package cern.nxcals.db;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by msobiesz on 08/08/17.
 */
public class ApplicationTest {
    private Application instance;

    @Before
    public void setUp() throws Exception {
        this.instance = new Application();
    }

    @Test
    public void shouldPerformUpdate() {
        Config config = ConfigFactory.load();
        this.instance.run(config);
    }

    @Test(expected = RuntimeException.class)
    public void shouldNotPerformUpdateDueToNonExistingChangeLogPath() {
        Config config = ConfigFactory.parseString("db.install.changelog.path=ala_ma_kota").
                withFallback(ConfigFactory.load());
        this.instance.run(config);
    }
}