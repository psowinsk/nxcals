package cern.nxcals.db;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import liquibase.Contexts;
import liquibase.LabelExpression;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import lombok.Builder;
import lombok.Data;
import org.apache.commons.codec.binary.Base64;

import java.sql.Connection;

/**
 * Created by msobiesz on 04/08/17.
 */
public class Application {

    public static void main(String[] args) {
        new Application().run(ConfigFactory.load());
    }

    public void run(Config config) {
        AppConfig appConfig = this.getAppConfigFrom(config);
        try (Connection connection = ConnectionFactory.forDriver(appConfig.getClassDriver())
                .getConnectionFor(appConfig.getUrl(), appConfig.getUser(), appConfig.getPassword())) {
            Database database = DatabaseFactory.getInstance()
                    .findCorrectDatabaseImplementation(new JdbcConnection(connection));
            if (appConfig.isDropAll()) {
                System.out.println("********* droping all ****************");
                dropAll(appConfig.getInstallChangeLogPath(), database);
            }
            if (appConfig.isInstallEnabled()) {
                System.out.println("********* installing all *************");
                performUpdate(appConfig.getInstallChangeLogPath(), database);
            }
            if (appConfig.isUpdateEnabled()) {
                System.out.println("********* updating all ***************");
                performUpdate(appConfig.getUpdateChangeLogPath(), database);
            }
            if (appConfig.isScriptEnabled() && !appConfig.getScriptChangeLogFile().isEmpty()) {
                System.out.println("********* script " + appConfig.getScriptChangeLogFile() + " ***************");
                performUpdate(appConfig.getScriptChangeLogFile(), database);
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private void dropAll(String pathToChangeLog, Database database) throws LiquibaseException {
        Liquibase liquibase = new Liquibase(pathToChangeLog, new ClassLoaderResourceAccessor(), database);
        liquibase.dropAll();
    }

    private void performUpdate(String pathToChangeLog, Database database) throws LiquibaseException {
        Liquibase liquibase = new Liquibase(pathToChangeLog, new ClassLoaderResourceAccessor(), database);
        liquibase.update(new Contexts(), new LabelExpression());
    }

    private AppConfig getAppConfigFrom(Config config) {
        boolean dropAll = config.hasPath("db.drop.all.before") ? config.getBoolean("db.drop.all.before") : false;
        return AppConfig.builder().user(config.getString("db.user"))
                .password(convertPasswordIfNeeded(config.getString("db.password"))).url(config.getString("db.url"))
                .installChangeLogPath(config.getString("db.install.changelog.path"))
                .updateChangeLogPath(config.getString("db.update.changelog.path"))
                .scriptChangeLogFile(config.getString("db.script.changelog.file"))
                .installEnabled(config.getBoolean("db.enable.install"))
                .updateEnabled(config.getBoolean("db.enable.update"))
                .scriptEnabled(config.getBoolean("db.enable.script"))
                .dropAll(dropAll)
                .classDriver(config.getString("db.driver.class")).
                        build();
    }

    private String convertPasswordIfNeeded(String password) {
        if (Base64.isBase64(password.getBytes())) {
            return new String(Base64.decodeBase64(password.getBytes()));
        }
        return password;
    }

    @Data
    @Builder
    private static class AppConfig {
        private final String user;
        private final String password;
        private final String url;
        private final String installChangeLogPath;
        private final String updateChangeLogPath;
        private final String scriptChangeLogFile;
        private final String classDriver;
        private final boolean installEnabled;
        private final boolean updateEnabled;
        private final boolean scriptEnabled;
        private final boolean dropAll;
    }
}
