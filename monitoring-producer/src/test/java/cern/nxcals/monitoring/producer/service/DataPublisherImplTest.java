package cern.nxcals.monitoring.producer.service;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.client.Publisher;
import cern.nxcals.client.Result;
import cern.nxcals.monitoring.producer.domain.ProducerConfiguration;
import cern.nxcals.monitoring.producer.domain.RecordConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by jwozniak on 02/10/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class DataPublisherImplTest {
    @Mock
    private Publisher<ImmutableData> ingestionPublisher;

    @Mock
    private ProducerConfiguration config;

    @Mock
    private RecordConfig recordConfig;

    @Test
    public void start() throws Exception {
        ValueCreator valueCreator = new ValueCreatorImpl();
        DataPublisherImpl dataPublisher = new DataPublisherImpl(valueCreator, config, (s) -> ingestionPublisher, 1, 1);

        Map<String, String> keys = new HashMap<>();
        keys.put("test1", "value1");

        when(recordConfig.getRecordKeys()).thenReturn(keys);
        when(recordConfig.getSystem()).thenReturn("TEST-SYSTEM");
        when(recordConfig.getTimestampKeyName()).thenReturn("testTimestamp");

        List<RecordConfig> recordConfigList = new ArrayList<>();
        recordConfigList.add(recordConfig);
        when(config.getRecords()).thenReturn(recordConfigList);

        when(ingestionPublisher.publishAsync(any())).thenReturn(new CompletableFuture<Result>());

        dataPublisher.start();

        TimeUnit.SECONDS.sleep(2);

        //should have 60 x num_of_params sent via ingestionPublisher
        verify(ingestionPublisher, times(60)).publishAsync(any());

    }

}