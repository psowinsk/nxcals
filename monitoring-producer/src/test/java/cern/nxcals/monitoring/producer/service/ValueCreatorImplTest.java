package cern.nxcals.monitoring.producer.service;

import cern.cmw.datax.ImmutableData;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertTrue;

/**
 * Created by jwozniak on 02/10/17.
 */
public class ValueCreatorImplTest {
    @Test
    public void createData() throws Exception {
        ValueCreatorImpl valueCreator = new ValueCreatorImpl();

        Map<String, String> keys = new HashMap<>();
        keys.put("test1", "value1");
        keys.put("test2", "value2");

        ImmutableData data = valueCreator.createData(keys, "timestampTest", 100L);

        assertTrue(data.getEntry("test1").get().equals("value1"));
        assertTrue(data.getEntry("test2").get().equals("value2"));
        assertTrue(data.getEntry("timestampTest").get().equals(100L));

    }

}