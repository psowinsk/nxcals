/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.monitoring.producer.config;

import cern.nxcals.monitoring.producer.domain.ProducerConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;

@EnableConfigurationProperties(ProducerConfiguration.class)
@Configuration
public class SpringConfig {
    @Bean
    public static ConversionService conversionService() {
        return new DefaultConversionService();
    }
}
