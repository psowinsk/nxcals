/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.monitoring.producer;

import cern.nxcals.monitoring.producer.service.DataPublisher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.context.ApplicationContext;

/**
 * Represents an application class that bootstraps and launches a Spring application from a Java main
 * method.
 */
@SpringBootApplication
@Slf4j
public class Application {
    static {
        System.setProperty("log4j.debug", "true");
    }

    public static void main(String[] args) throws Exception {
        try {
            SpringApplication app = new SpringApplication(Application.class);
            app.addListeners(new ApplicationPidFileWriter());
            ApplicationContext ctx = app.run();

            DataPublisher dataPublisher = ctx.getBean(DataPublisher.class);
            dataPublisher.start();
        } catch (Exception ex) {
            log.error("Exception while running publisher app", ex);
            System.exit(1);
        }

    }

}
