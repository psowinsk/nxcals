package cern.nxcals.monitoring.producer.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * Created by jwozniak on 30/09/17.
 */
@ConfigurationProperties("nxcals.producer")
@Data
@NoArgsConstructor
public class ProducerConfiguration {
    private List<RecordConfig> records;
}
