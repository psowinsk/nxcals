package cern.nxcals.monitoring.producer.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
public class RecordConfig {
    private Map<String, String> recordKeys;
    private String system;
    private String timestampKeyName;
}