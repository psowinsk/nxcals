package cern.nxcals.monitoring.producer.service;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.client.Publisher;
import cern.nxcals.client.PublisherFactory;
import cern.nxcals.client.Result;
import cern.nxcals.monitoring.producer.domain.ProducerConfiguration;
import cern.nxcals.monitoring.producer.domain.RecordConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

/**
 * Created by jwozniak on 03/11/16.
 */
@Service
@Slf4j
public class DataPublisherImpl implements DataPublisher {
    private final ValueCreator valueCreator;
    private final ProducerConfiguration config;
    private final ConcurrentHashMap<String, Publisher<ImmutableData>> publishers;
    private final Function<String, Publisher<ImmutableData>> publisherCreator;
    private final int firstValueSleepTime;
    private final int nextValuesSleepTime;

    protected DataPublisherImpl(ValueCreator valueCreator, ProducerConfiguration config,
            Function<String, Publisher<ImmutableData>> publisherCreator, int firstValueSleepTime,
            int nextValuesSleepTime) {
        this.valueCreator = Objects.requireNonNull(valueCreator);
        this.config = Objects.requireNonNull(config);
        this.publishers = new ConcurrentHashMap<>();
        this.publisherCreator = Objects.requireNonNull(publisherCreator);
        this.firstValueSleepTime = firstValueSleepTime;
        this.nextValuesSleepTime = nextValuesSleepTime;
    }

    @Autowired
    public DataPublisherImpl(ValueCreator provider, ProducerConfiguration config) {
        this(provider, config, DataPublisherImpl::createPublisher, 10000, 100);
    }

    @Override
    public void start() {
        log.info("Start publishing values");
        ScheduledExecutorService scheduledExecutorService = Executors
                .newScheduledThreadPool(config.getRecords().size());
        config.getRecords().forEach(record -> scheduledExecutorService
                .scheduleAtFixedRate(() -> sendValues(record), 0, 1, TimeUnit.MINUTES));
    }

    private void sendValues(RecordConfig recordConfig) {
        try {
            // remove the millis after the current, round minute
            long timeInMillis = (System.currentTimeMillis() / 60_000L) * 60_000L;
            //start 60 values every second starting from this minute.
            log.debug("Publishing values for record={} and timestamp={}", recordConfig, timeInMillis);
            for (int i = 0; i < 60; ++i) {

                //start adding 60 consecutive seconds to the current minute
                long timestamp = (timeInMillis + i * 1000) * 1_000_000;
                ImmutableData data = valueCreator
                        .createData(recordConfig.getRecordKeys(), recordConfig.getTimestampKeyName(), timestamp);

                Publisher<ImmutableData> publisher = publishers
                        .computeIfAbsent(recordConfig.getSystem(), this.publisherCreator);

                CompletableFuture<Result> future = publisher.publishAsync(data);
                future.whenComplete((result, throwable) -> {
                    if (throwable != null) {
                        log.error("Cannot send data with keys={}  for timestamp={}", recordConfig.getRecordKeys(),
                                timestamp, throwable);
                    }
                });
                //This sleep is needed to avoid messages order problem when schema changes
                if (i == 0) {
                    //We wait long only for the first message
                    TimeUnit.MILLISECONDS.sleep(this.firstValueSleepTime);
                } else {
                    TimeUnit.MILLISECONDS.sleep(this.nextValuesSleepTime);
                }
            }
        } catch (Exception ex) {
            log.error("Exception while publishing values", ex);
        }
    }

    private static Publisher<ImmutableData> createPublisher(String systemName) {

        return PublisherFactory.newInstance().createPublisher(systemName, Function.identity());

    }

}


