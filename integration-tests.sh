#!/usr/bin/env bash


VERSION=$(cat gradle.properties | grep currentVersion | awk 'BEGIN { FS="="} { print $2;}')
FILE=integration-tests-install.yml
INVENTORY=inventory/dev-$USER

INTEGRATION_TESTS_DIR=$HOME/nxcals_integration_tests

echo "**** Using inventory $INVENTORY with $FILE for integration tests version $VERSION with additional arguments: ${@:2} ****"
echo "If you want to cancel press CTRL+C now..."
sleep 1

cd ansible;
ansible-playbook -i $INVENTORY $FILE --vault-pass .pass -e "kerberos_keytab_directory=$INTEGRATION_TESTS_DIR" "${@:2}";

if [ ! $? -eq 0 ]; then
 exit $?
fi

cd ../integration-tests/


../gradlew clean integrationTest --info -Phadoop_cluster=dev -Dnxcals_namespace=nxcals_dev_${USER} -Dkerberos.principal=$USER -Dkerberos.keytab=$HOME/.keytab \
-Dservice.url=https://nxcals-${USER}1.cern.ch:19093,https://nxcals-${USER}2.cern.ch:19093


echo "Clean-up structure after integration tests"

cd ..
git checkout -- integration-tests/src/integration-test/resources/application.yml

rm -rf $INTEGRATION_TESTS_DIR

echo "Clean-up done"