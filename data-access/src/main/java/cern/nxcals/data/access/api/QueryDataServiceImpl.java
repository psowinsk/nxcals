/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.data.access.api;

import cern.nxcals.common.domain.EntityResources;
import cern.nxcals.common.domain.SchemaData;
import cern.nxcals.common.domain.SystemData;
import cern.nxcals.common.domain.VariableConfigData;
import cern.nxcals.common.domain.VariableData;
import cern.nxcals.common.utils.TimeUtils;
import cern.nxcals.data.access.builders.mapper.QueryBuilderMapper;
import cern.nxcals.service.client.api.internal.InternalEntitiesResourcesService;
import cern.nxcals.service.client.api.internal.InternalSystemService;
import cern.nxcals.service.client.api.internal.InternalVariableService;
import com.google.common.annotations.VisibleForTesting;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericRecordBuilder;
import org.apache.commons.collections.CollectionUtils;
import org.apache.spark.sql.types.DataTypes;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static cern.nxcals.common.SystemFields.NXC_ENTITY_ID;
import static cern.nxcals.common.SystemFields.NXC_EXTR_ENTITY_ID;
import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VALUE;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VARIABLE_NAME;
import static cern.nxcals.common.SystemFields.getAllSystemFieldNames;
import static cern.nxcals.data.access.Constants.ALIASES_KEY;
import static cern.nxcals.data.access.Constants.ALIASES_TYPE;
import static cern.nxcals.data.access.Constants.END_TIME_KEY;
import static cern.nxcals.data.access.Constants.FIELDS_KEY;
import static cern.nxcals.data.access.Constants.FIELDS_TYPE;
import static cern.nxcals.data.access.Constants.KEY_VALUES_KEY;
import static cern.nxcals.data.access.Constants.KEY_VALUES_TYPE;
import static cern.nxcals.data.access.Constants.START_TIME_KEY;
import static cern.nxcals.data.access.Constants.SYSTEM_KEY;
import static cern.nxcals.data.access.Constants.VARIABLE_KEY;

/**
 * Default implementation of {@link QueryDataService}. It is transforming user the request into
 * {@link InternalQueryData} which is used for building the Spark requests for both data sources
 * (HBase nad HDFS).
 */
class QueryDataServiceImpl implements QueryDataService {
    @VisibleForTesting
    static final String DATE_CORRECTNESS_ERROR_MESSAGE_FORMAT =
            "Provided timestamp for field %s correspond to a " + "date before 01/01/2000 ! Value=%s";
    @VisibleForTesting
    static final String SCHEMA_MISMATCH_ERROR_MESSAGE_FORMAT =
            "The field %s and value %s don't match the schema for " + "system %s";
    @VisibleForTesting
    static final String NO_SYSTEM_FOUND_ERROR_MESSAGE_FORMAT =
            "The NXCALS DataAccess format requires SYSTEM_KEY " + "to be declared in the query!";
    private static final Logger LOGGER = LoggerFactory.getLogger(QueryDataServiceImpl.class);
    private static final long MILLENNIUM_TIMESTAMP = TimeUtils
            .getNanosFromInstant(Instant.from(LocalDateTime.of(2000, 1, 1, 0, 0).atOffset(ZoneOffset.UTC)));
    private final ObjectMapper mapper = new ObjectMapper();
    private final InternalSystemService systemService;
    private final InternalVariableService variableService;
    private final InternalEntitiesResourcesService entitiesResourcesService;

    QueryDataServiceImpl(InternalSystemService systemService, InternalVariableService variableService,
            InternalEntitiesResourcesService entitiesResourcesService) {
        this.systemService = systemService;
        this.variableService = variableService;
        this.entitiesResourcesService = entitiesResourcesService;
    }

    private static void checkTimeFieldCorrectness(String fieldName, long timestamp) {
        if (timestamp < MILLENNIUM_TIMESTAMP) {
            throw new IllegalArgumentException(
                    String.format(DATE_CORRECTNESS_ERROR_MESSAGE_FORMAT, fieldName, timestamp));
        }
    }

    @Override
    public Set<InternalQueryData> loadQueryData(Map<String, String> query) {
        LOGGER.debug("Loading data from query={}", query);
        long startTime = getTimeFieldFromQueryMap(START_TIME_KEY, query);
        long endTime = getTimeFieldFromQueryMap(END_TIME_KEY, query);
        LOGGER.debug("Querying for interval [{},{}]", startTime, endTime);
        if (query.containsKey(VARIABLE_KEY)) {
            String variableName = query.get(VARIABLE_KEY);
            return getVariablesQueryData(variableName, startTime, endTime);
        } else {
            return getEntityQueryData(query, startTime, endTime);
        }
    }

    private Set<InternalQueryData> getEntityQueryData(Map<String, String> query, long startTime, long endTime) {
        LOGGER.debug("Start building entityQueryData for query={}", query);
        if (query.get(SYSTEM_KEY) == null) {
            throw new IllegalArgumentException(NO_SYSTEM_FOUND_ERROR_MESSAGE_FORMAT);
        }
        SystemData systemData = systemService.findByName(query.get(SYSTEM_KEY));
        Instant start = TimeUtils.getInstantFromNanos(startTime);
        Instant stop = TimeUtils.getInstantFromNanos(endTime);
        Collection<Map<String, Object>> queries = getEntityQueries(query, systemData);
        Map<String, List<String>> aliases = getAliases(query);
        List<String> fields = getFields(query);
        Set<EntityResources> entityResources = new HashSet<>();

        for (Map<String, Object> entityQuery : queries) {
            Set<EntityResources> entityResourcesSet = entitiesResourcesService
                    .findBySystemIdKeyValuesAndTimeWindow(systemData.getId(), entityQuery, startTime,
                            endTime);
            if (CollectionUtils.isEmpty(entityResourcesSet)) {
                LOGGER.warn("There is no entity registered for systemId={} and keyValues={}",
                        systemData.getId(), entityQuery);
            }
            entityResources.addAll(entityResourcesSet);
        }

        LOGGER.debug("Build query for Set<entityResourceData>={}, startTime={}, endTime={}", entityResources, startTime,
                endTime);

        return entityResources.stream().map(e -> new InternalQueryData(e, start, stop,
                getRequestedEntityFields(aliases, fields, e.getSystemData(), e.getSchemaData()), null))
                .collect(Collectors.toSet());
    }

    private List<String> getFields(Map<String, String> query) {
        String serializedFields = query.get(FIELDS_KEY);
        if (serializedFields == null) {
            return Collections.emptyList();
        } else {
            return QueryBuilderMapper.deserializeOrThrow(serializedFields, FIELDS_TYPE);
        }
    }

    private Map<String, List<String>> getAliases(Map<String, String> query) {
        String serializedAliases = query.get(ALIASES_KEY);
        if (serializedAliases == null) {
            return Collections.emptyMap();
        } else {
            return QueryBuilderMapper.deserializeOrThrow(serializedAliases, ALIASES_TYPE);
        }
    }

    private Collection<Map<String, Object>> getEntityQueries(Map<String, String> query, SystemData systemData) {
        Schema schema = new Schema.Parser().parse(systemData.getEntityKeyDefinitions());

        Map<String, Map<String, Object>> queries = QueryBuilderMapper
                .deserializeOrThrow(query.get(KEY_VALUES_KEY), KEY_VALUES_TYPE);

        for (Map<String, Object> entityQuery : queries.values()) {
            GenericRecordBuilder genericRecordBuilder = new GenericRecordBuilder(schema);
            for (Map.Entry<String, Object> entry : entityQuery.entrySet()) {
                String fieldName = entry.getKey();
                Object value = entry.getValue();
                try {
                    genericRecordBuilder.set(fieldName, value);
                } catch (Exception exception) {
                    throw new IllegalArgumentException(
                            String.format(SCHEMA_MISMATCH_ERROR_MESSAGE_FORMAT, fieldName, value,
                                    systemData.getName()), exception);
                }
            }
            genericRecordBuilder.build();
        }

        return queries.values();
    }

    private Set<DataAccessField> getRequestedEntityFields(Map<String, List<String>> aliases, List<String> fields,
            SystemData system, SchemaData schema) {
        Set<DataAccessField> resultFields = getSystemFields(system, false);
        resultFields.addAll(getAllFieldsFromEntitySchemas(schema, transposeAliasFields(aliases), fields));
        LOGGER.debug("Get set of requested addFields = {}", resultFields);
        return resultFields;
    }

    private Map<String, String> transposeAliasFields(Map<String, List<String>> aliasFields) {
        Map<String, String> fieldAlias = new LinkedHashMap<>();
        for (Map.Entry<String, List<String>> entry : aliasFields.entrySet()) {
            entry.getValue().forEach(value -> fieldAlias.put(value, entry.getKey()));
        }
        return fieldAlias;
    }

    private List<DataAccessField> getAllFieldsFromEntitySchemas(SchemaData schema, Map<String, String> fieldAlias,
            List<String> requestedFields) {
        if (schema == null) {
            return Collections.emptyList();
        }
        List<String> schemaFields = new Schema.Parser().parse(schema.getSchemaJson()).getFields().stream()
                .map(Schema.Field::name).filter(f -> !getAllSystemFieldNames().contains(f))
                .collect(Collectors.toList());

        if (!requestedFields.isEmpty()) {
            schemaFields = schemaFields.stream().filter(requestedFields::contains).collect(Collectors.toList());
        }

        if (!fieldAlias.isEmpty()) {
            List<DataAccessField> collect = new ArrayList<>();
            for (String field : getFieldsOrderedByAliasPreference(fieldAlias, schemaFields)) {
                // checks whether another field is already assigned endTime the same alias.
                // If yes, that means the previous field had higher priority and we will skip the alias of the current one
                if (collect.stream().noneMatch(f -> f.getAlias() != null && f.getAlias().equals(fieldAlias.get(field)))) {
                    collect.add(new DataAccessFieldBuilder(field).alias(fieldAlias.get(field)).build());
                } else {
                    collect.add(new DataAccessFieldBuilder(field).build());
                }
            }
            return collect;
        }
        return schemaFields.stream().map(f -> new DataAccessFieldBuilder(f).build()).collect(Collectors.toList());
    }

    private List<String> getFieldsOrderedByAliasPreference(Map<String, String> fieldAlias, List<String> collect) {
        List<String> res = new ArrayList<>(fieldAlias.keySet());
        collect.sort(Comparator.comparing(res::indexOf));
        return collect;
    }

    private Set<InternalQueryData> getVariablesQueryData(String variableName, long startTime, long endTime) {
        Objects.requireNonNull(variableName, "Variable name cannot be null!");
        VariableData variableData = variableService.findByVariableNameAndTimeWindow(variableName, startTime, endTime);
        if (variableData == null) {
            throw new IllegalArgumentException("There is no registered variable in NXCALS with name " + variableName);
        }
        if (variableData.getVariableConfigData() == null) {
            throw new IllegalArgumentException("The requested variable=" + variableData.getVariableName()
                    + " does not have any configuration set!");
        }
        LOGGER.debug("Start building query for variable={}", variableData.getVariableName());
        Set<InternalQueryData> queryData = new HashSet<>();
        for (VariableConfigData config : variableData.getVariableConfigData()) {
            queryData.addAll(createInternalQueryData(variableName, startTime, endTime, config));
        }
        return queryData;
    }

    private Set<InternalQueryData> createInternalQueryData(String variableName, long startTime, long endTime,
            VariableConfigData conf) {
        LOGGER.debug("Add query data for variableConfig={}", conf);
        Set<EntityResources> entityResources = this.entitiesResourcesService
                .findByEntityIdAndTimeWindow(conf.getEntityId(), startTime, endTime);
        if (CollectionUtils.isEmpty(entityResources)) {
            throw new IllegalStateException(
                    "There are no registered resources in NXCALS for the requested entity=" + conf.getEntityId()
                            + " in time window startTime=" + startTime + " endTime=" + endTime);
        }

        Instant start = TimeUtils.getInstantFromNanos(
                Long.max(conf.getValidFromStamp() == null ? startTime : conf.getValidFromStamp(), startTime));
        Instant stop = TimeUtils.getInstantFromNanos(
                Long.min(conf.getValidToStamp() == null ? endTime : conf.getValidToStamp(), endTime));
        Set<InternalQueryData> ret = new HashSet<>();
        for (EntityResources entityResource : entityResources) {
            ret.add(new InternalQueryData(entityResource, start, stop,
                    getVariableFields(conf, entityResource.getSystemData(), entityResource.getSchemaData()),
                    variableName));
        }
        return ret;
    }

    private Set<DataAccessField> getVariableFields(VariableConfigData variableConfig, SystemData system,
            SchemaData schema) {
        //Use Set to eliminate duplicates between systemFields and schemaFields
        Set<DataAccessField> fields = new HashSet<>();
        fields.addAll(getSystemFields(system, true));
        fields.add(getVariableNameField());
        if (variableConfig.getFieldName() != null) {
            fields.add(
                    new DataAccessFieldBuilder(variableConfig.getFieldName()).alias(NXC_EXTR_VALUE.getValue()).build());
        } else {
            fields.addAll(getAllFieldsFromEntitySchemas(schema, Collections.emptyMap(), Collections.emptyList()));
        }
        LOGGER.debug("Get set of Fields = {}", fields);
        return fields;
    }

    private DataAccessField getVariableNameField() {
        Schema stringSchema = new Schema.Parser().parse("[\"string\",\"null\"]");
        return new DataAccessFieldBuilder(NXC_EXTR_VARIABLE_NAME.getValue()).dataType(DataTypes.StringType)
                .fieldSchema(stringSchema).alias(NXC_EXTR_VARIABLE_NAME.getValue()).build();
    }

    private long getTimeFieldFromQueryMap(String key, Map<String, String> query) {
        long convertedTimeValue = Long.parseLong(query.get(key));
        checkTimeFieldCorrectness(key, convertedTimeValue);
        return convertedTimeValue;
    }

    private Set<DataAccessField> getSystemFields(SystemData system, boolean withAlias) {
        Set<DataAccessField> systemFields = new HashSet<>();
        systemFields
                .add(new DataAccessFieldBuilder(NXC_ENTITY_ID.getValue()).alias(NXC_EXTR_ENTITY_ID.getValue()).build());
        systemFields.add(getTimestampFieldFor(system, withAlias));
        LOGGER.debug("Using systemFields={}", systemFields);
        return systemFields;
    }

    private DataAccessField getTimestampFieldFor(SystemData system, boolean withAlias) {
        String fieldName = ServiceUtils.getTimestampField(system.getTimeKeyDefinitions());
        LOGGER.debug("Get timestamp field for system={}, field={}", system.getName(), fieldName);
        if (withAlias) {
            return new DataAccessFieldBuilder(fieldName).alias(NXC_EXTR_TIMESTAMP.getValue()).build();
        }
        return new DataAccessFieldBuilder(fieldName).build();
    }
}
