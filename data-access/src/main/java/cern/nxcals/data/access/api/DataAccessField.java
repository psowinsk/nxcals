/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.data.access.api;

import org.apache.avro.Schema;
import org.apache.spark.sql.types.DataType;

/**
 * Internal representation of requested field. It adds additional information used during the extraction process
 */
class DataAccessField {
    private final String fieldName;
    private final DataType dataType;
    private final Schema fieldSchema;
    private final String alias;

    DataAccessField(String fieldName, DataType dataType, Schema fieldSchema, String alias) {
        this.fieldName = fieldName;
        this.dataType = dataType;
        this.fieldSchema = fieldSchema;
        this.alias = alias;
    }

    //This should stay protected as there are people using it (in PM)
    protected String getFieldName() {
        return fieldName;
    }

    protected DataType getDataType() {
        return dataType;
    }

    protected Schema getFieldSchema() {
        return fieldSchema;
    }

    protected String getAlias() {
        return alias;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        DataAccessField that = (DataAccessField) o;

        return fieldName.equals(that.fieldName);
    }

    @Override
    public int hashCode() {
        return fieldName.hashCode();
    }

    @Override
    public String toString() {
        return "DataAccessField{" + "fieldName='" + fieldName + '\'' + ", dataType=" + dataType + ", fieldSchema="
                + fieldSchema + ", alias='" + alias + '\'' + '}';
    }
}
