/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.data.access.api;

import org.apache.commons.lang3.StringUtils;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.execution.datasources.hbase.HBaseTableCatalog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.TreeMap;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

/**
 * Implementation of {@link InternalDataAccessService} fot Hbase.
 * The responsibility of this class is to create the table catalog used by the Spark - Hbase connector
 * {@link org.apache.spark.sql.execution.datasources.DataSource} and obtain the data for the requested entities and
 * time window
 */
class HbaseDataAccessServiceImpl implements InternalDataAccessService<ResourcesBasedQuery<HbaseResource>> {
    private static final String PREFIX = "\"rowkey\":\"id\",\"columns\":{\"entityKey\":{\"cf\":\"rowkey\",\"col\":\"id\",\"type\":\"string\"},";
    private static final String SUFFIX = "}";
    private static final String HBASE_FORMAT = "org.apache.spark.sql.execution.datasources.hbase";

    private static final Logger LOGGER = LoggerFactory.getLogger(HbaseDataAccessServiceImpl.class);
    private static final String ENTITY_KEY = "entityKey";
    private static final String[] EMPTY_STRING = new String[0];
    private static final String KEY_DELIMITER = "__";
    private final SparkSession sparkSession;

    HbaseDataAccessServiceImpl(SparkSession sparkSession) {
        this.sparkSession = sparkSession;
    }

    @Override
    public Dataset<Row> getDataFor(ResourcesBasedQuery<HbaseResource> query) {

        Dataset<Row> resultSet = null;
        for (ResourceQuery<HbaseResource> resourceQuery : query.getResources()) {

            String[] cols = resourceQuery.getColumnsAsArray();
            Column conditions = createPredicate(resourceQuery);

            LOGGER.debug("Starting querying hbase from {} to {} with fields columns [{}] for entities [{}].",
                    resourceQuery.getStartTime(), resourceQuery.getEndTime(), cols, resourceQuery.getEntityIds());

            Dataset<Row> dataset;
            for (HbaseResource resource : resourceQuery.getResources()) {
                Map<String, String> catalog = tableCatalog(resource.getNamespace(),
                        resource.getTableName(), query.getFields());

                LOGGER.debug("Querying data for table {}", resource.getTableName());

                dataset = sparkSession.read().format(HBASE_FORMAT).options(catalog).load().selectExpr(cols);

                if (nonNull(conditions)) {
                    dataset = dataset.where(conditions);
                }

                if (nonNull(resultSet)) {
                    resultSet = resultSet.union(dataset);
                    LOGGER.debug("Performed union on resultSet.union(dataset) successfully!");
                } else {
                    resultSet = dataset;
                    LOGGER.debug("Assigned first dataset");
                }
            }
        }

        return nonNull(resultSet) ? resultSet : empty(query.getFields());
    }

    private Dataset<Row> empty(List<DataAccessField> fields) {
        return sparkSession.createDataFrame(new ArrayList<>(), SparkTypeUtils.getStructSchemaFor(fields));
    }

    private Column createPredicate(ResourceQuery<HbaseResource> resourceQuery) {
        Column conditions = null;
        for (Long entityId : resourceQuery.getEntityIds()) {
            String start = getKeyPredicateFor(entityId, resourceQuery.getStartTime());
            String end = getKeyPredicateFor(entityId, resourceQuery.getEndTime());
            if (nonNull(conditions)) {
                conditions = conditions.or(createFilteredColumn(start, end));
            } else {
                conditions = createFilteredColumn(start, end);
            }
        }
        return conditions;
    }

    protected String getKeyPredicateFor(long entityId, long nanos) {
        String predicate = String.format("%s__%s", entityId, Long.MAX_VALUE - nanos);
        LOGGER.debug("getKeyPredicateFor(entityId = {} ,timestamp = {}) returns = {}", entityId, nanos, predicate);
        return predicate;
    }

    private Column createFilteredColumn(String start, String end) {
        return new Column(ENTITY_KEY).geq(end).and(new Column(ENTITY_KEY).leq(start));
    }

    Map<String, String> tableCatalog(String namespace, String tableName, List<DataAccessField> fields) {
        if (StringUtils.isBlank(namespace)) {
            LOGGER.warn("The provided namespace is blank string, will use 'default' namespace instead!");
            namespace = "default";
        }

        validateFields(fields);

        LOGGER.debug("Create catalog for namespace = {} table = {} with fields = {})", namespace, tableName, fields);

        String header = catalogHeader(tableName, fields);

        Map<String, String> catalog = new LinkedHashMap<>();
        catalog.put(HBaseTableCatalog.tableCatalog(),
                String.format("{\"table\": {\"namespace\": \"%s\", \"name\":\"%s\"},%s}", namespace, tableName, header));

        for (DataAccessField field: fields) {
            String schemaName = schemaName(tableName, field);

            catalog.put(schemaName, field.getFieldSchema().toString());

            LOGGER.debug("Add schema for field \"{}\" to tablesCatalog: {} -> {}",
                    field.getFieldName(), schemaName, field.getFieldSchema().toString());
        }

        return catalog;
    }

    private String catalogHeader(String tableName, List<DataAccessField> fields) {
        StringJoiner joiner = new StringJoiner(",", PREFIX, SUFFIX);

        for (DataAccessField field: fields) {
            joiner.add(String.format("\"%s\":{\"cf\":\"data\",\"col\":\"%s\",\"avro\":\"%s\"}", field.getFieldName(),
                    field.getFieldName(), schemaName(tableName, field)));
        }

        return joiner.toString();
    }

    private String schemaName(String tableName, DataAccessField field) {
        return tableName + KEY_DELIMITER + field.getFieldName().toLowerCase();
    }

    private void validateFields(List<DataAccessField> fields) {
        List<String> errorFields = fields.stream()
                .filter(f -> isNull(f.getFieldSchema()))
                .map(DataAccessField::getFieldName)
                .collect(Collectors.toList());

        if (!errorFields.isEmpty()) {
            throw new IllegalStateException("There is no schema present for fields = " + errorFields);
        }
    }
}
