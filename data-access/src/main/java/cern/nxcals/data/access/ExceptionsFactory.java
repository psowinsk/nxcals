/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.data.access;

/**
 * Central point to common error message formats.
 *
 * @author timartin
 */
public final class ExceptionsFactory {

    private ExceptionsFactory() {
        /* Nothing to do here */
    }

    private static final String SERIALIZATION_ERROR_FORMAT = "Unable to serialize to JSON: [%s]";
    private static final String DESERIALIZATION_ERROR_FORMAT = "Unable to deserialize to JSON: [%s]";

    public static RuntimeException createSerializationException(Object objectToSerialize, Throwable cause) {
        return new RuntimeException(String.format(SERIALIZATION_ERROR_FORMAT, objectToSerialize), cause);
    }

    public static RuntimeException createDeserializationException(Object objectToDeserialize, Throwable cause) {
        return new RuntimeException(String.format(DESERIALIZATION_ERROR_FORMAT, objectToDeserialize), cause);
    }
}
