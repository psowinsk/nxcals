/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.data.access.api;

import org.apache.hadoop.fs.FileSystem;
import org.apache.spark.sql.SparkSession;

import java.io.IOException;
import java.io.UncheckedIOException;

/**
 * Factory class to createDataSetFor DataAccessService with requested SparkContext
 */
public final class DataAccessServiceFactory {

    private DataAccessServiceFactory() {
    }

    /**
     * Creates DataAccessService with given SparkSession
     *
     * @param sparkSession
     * @return {@link DataAccessService}
     */
    public static DataAccessService createDataAccessService(SparkSession sparkSession) {
        InternalDataAccessService<ResourcesBasedQuery<HbaseResource>> hbaseService = new HbaseDataAccessServiceImpl(
                sparkSession);
        InternalDataAccessService<HdfsResourceBasedQuery> hdfsService = new HdfsDataAccessServiceImpl(sparkSession,
                createFileSystem());
        return new DataAccessServiceImpl(hbaseService, hdfsService);
    }

    private static FileSystem createFileSystem() {
        org.apache.hadoop.conf.Configuration conf = new org.apache.hadoop.conf.Configuration();
        try {
            return FileSystem.get(conf);
        } catch (IOException e) {
            throw new UncheckedIOException("Cannot access hdfs filesystem", e);
        }
    }
}
