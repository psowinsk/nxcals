/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.data.access.api;

import cern.nxcals.common.SystemFields;
import org.apache.avro.Schema;
import org.apache.avro.Schema.Type;
import org.apache.spark.sql.types.DataTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static org.apache.avro.Schema.Type.BOOLEAN;
import static org.apache.avro.Schema.Type.BYTES;
import static org.apache.avro.Schema.Type.DOUBLE;
import static org.apache.avro.Schema.Type.FLOAT;
import static org.apache.avro.Schema.Type.INT;
import static org.apache.avro.Schema.Type.LONG;

/**
 * Created by ntsvetko on 11/29/16.
 */

final class TypeCompatibility {
    private static final Logger LOGGER = LoggerFactory.getLogger(TypeCompatibility.class);
    private static final Set<Type> numericTypes = new HashSet<>(Arrays.asList(INT, LONG, BYTES, BOOLEAN));
    private static final Set<Type> extendedNumericTypes = new HashSet<>(
            Arrays.asList(INT, LONG, BYTES, BOOLEAN, FLOAT, DOUBLE));

    private TypeCompatibility() {
    }

    static DataAccessField getPromotedFieldTypeWithAliases(DataAccessField field,
            Map<String, Set<Schema>> fieldSchemas) {
        Set<Schema> schemas = fieldSchemas.entrySet().stream().filter(e -> field.getAlias() == null ?
                field.getFieldName().equals(e.getKey()) :
                field.getAlias().equals(e.getKey())).map(Map.Entry::getValue).flatMap(Set::stream)
                .collect(Collectors.toSet());

        // We don't promote system fields if they have already type and schema set
        // The reason is that we don't have fields like NXCALS_VARIABLE_NAME in any schema and we pre-build them
        if (SystemFields.getAllSystemFieldNames().contains(field.getAlias()) && field.getDataType() != null
                && field.getFieldSchema() != null) {
            return field;
        }
        return getPromotedFieldTypeFor(field, schemas);
    }

    static DataAccessField getPromotedFieldTypeFor(DataAccessField field, Set<Schema> schemas) {
        Objects.requireNonNull(field, "DataAccessField is required for calling this method.");
        Schema anySchema = schemas.stream().findFirst().orElseThrow(() -> new IllegalArgumentException(
                "Provided collection of schemas is empty. Requested promotion for field=" + field));
        Set<Type> types = schemas.stream().map(TypeCompatibility::getSchemaTypeFor).distinct()
                .collect(Collectors.toSet());

        if (types.size() > 1) {
            if (numericTypes.containsAll(types)) {
                Schema longSchema = new Schema.Parser().parse("[\"long\",\"null\"]");
                LOGGER.debug("Promote field of types = {} to Long", types);
                return new DataAccessFieldBuilder(field.getFieldName()).dataType(DataTypes.LongType)
                        .fieldSchema(longSchema).alias(field.getAlias()).build();
            } else if (extendedNumericTypes.containsAll(types)) {
                Schema doubleSchema = new Schema.Parser().parse("[\"double\",\"null\"]");
                LOGGER.debug("Promote field of types = {} to Double", types);
                return new DataAccessFieldBuilder(field.getFieldName()).dataType(DataTypes.DoubleType)
                        .fieldSchema(doubleSchema).alias(field.getAlias()).build();
            } else {
                throw new UnsupportedOperationException("Unsupported type promotion {" + types.toString() + "}");
            }
        }
        return new DataAccessFieldBuilder(field.getFieldName()).dataType(SparkTypeUtils.getDataTypeFor(anySchema))
                .fieldSchema(anySchema).alias(field.getAlias()).build();

    }

    private static Type getSchemaTypeFor(Schema schema) {
        if (schema.getType().equals(Type.UNION)) {
            return getSchemaTypeFor(SparkTypeUtils.getSchemaUnionType(schema));
        }
        return schema.getType();
    }
}
