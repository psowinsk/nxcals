package cern.nxcals.data.access.api;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;
import java.util.Set;

@Getter
@AllArgsConstructor
public class ResourcesBasedQuery<T> {
    private final Set<ResourceQuery<T>> resources;
    private final List<DataAccessField> fields;
}
