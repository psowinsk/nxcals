package cern.nxcals.data.access.builders.fluent.stages;

import java.time.Duration;
import java.time.Instant;

public interface EndTimeStage<T> {
    DataContextStage<T> endTime(Instant endTime);

    DataContextStage<T> endTime(String endTimeUtc);

    DataContextStage<T> duration(Duration duration);
}
