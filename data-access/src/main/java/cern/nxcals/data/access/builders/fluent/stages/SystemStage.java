package cern.nxcals.data.access.builders.fluent.stages;

public interface SystemStage<T> {
    StartTimeStage<T> system(String system);
}
