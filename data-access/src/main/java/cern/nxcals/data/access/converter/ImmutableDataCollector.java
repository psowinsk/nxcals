/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.data.access.converter;

import cern.cmw.datax.DataBuilder;
import cern.cmw.datax.ImmutableData;
import cern.cmw.datax.ImmutableEntry;

import java.util.HashSet;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

/**
 * {@link Collector} implementation to allow easy manipulation and collection of {@link ImmutableData} with Java
 * {@link java.util.stream.Stream}s.
 *
 * @author timartin
 */

public class ImmutableDataCollector implements Collector<ImmutableEntry, DataBuilder, ImmutableData> {

    private static ImmutableDataCollector instance = null;

    /**
     * Gets or creates if needed a singleton instance of {@link ImmutableDataCollector}.
     *
     * @return a new isntance of {@link ImmutableDataCollector}.
     */
    public static final synchronized ImmutableDataCollector instance() {
        if (instance == null) {
            instance = new ImmutableDataCollector();
        }

        return instance;
    }

    private ImmutableDataCollector() {
        /* Functional class, so we can make it singleton */
    }

    @Override
    public Supplier<DataBuilder> supplier() {
        return ImmutableData::builder;
    }

    @Override
    public BiConsumer<DataBuilder, ImmutableEntry> accumulator() {
        return DataBuilder::add;
    }

    @Override
    public BinaryOperator<DataBuilder> combiner() {
        return (builder1, builder2) -> builder1.addAll(builder2.build().getEntries());
    }

    @Override
    public Function<DataBuilder, ImmutableData> finisher() {
        return DataBuilder::build;
    }

    @Override
    public Set<Characteristics> characteristics() {
        return new HashSet<>();
    }
}
