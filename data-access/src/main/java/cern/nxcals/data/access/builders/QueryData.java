/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.data.access.builders;

import cern.nxcals.common.utils.TimeUtils;
import cern.nxcals.data.access.api.DataAccessService;
import cern.nxcals.data.access.api.DataAccessServiceFactory;
import cern.nxcals.data.access.api.QueryDataService;
import cern.nxcals.data.access.api.QueryDataServiceFactory;

import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;

import static cern.nxcals.data.access.Constants.ALIASES_KEY;
import static cern.nxcals.data.access.Constants.ALIASES_TYPE;
import static cern.nxcals.data.access.Constants.END_TIME_KEY;
import static cern.nxcals.data.access.Constants.FIELDS_KEY;
import static cern.nxcals.data.access.Constants.FIELDS_TYPE;
import static cern.nxcals.data.access.Constants.KEY_VALUES_KEY;
import static cern.nxcals.data.access.Constants.KEY_VALUES_TYPE;
import static cern.nxcals.data.access.Constants.START_TIME_KEY;
import static cern.nxcals.data.access.builders.mapper.QueryBuilderMapper.deserializeOrThrow;
import static cern.nxcals.data.access.builders.mapper.QueryBuilderMapper.serializeOrThrow;
import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.requireNonNull;

/**
 * Builder that provides access to the creation of the querying {@link Map} to be used when querying the NXCALS
 * infrastructure.
 */
class QueryData {
    private Pair<String, String> id;
    private Instant startTime;
    private Instant endTime;

    private final SparkSession session;
    private final Map<String, Map<String, String>> entities = new HashMap<>();
    private final List<String> fields = new ArrayList<>();
    private final Map<String, List<String>> aliases = new HashMap<>();

    /**
     * Creates a new empty {@link QueryData}.
     * @param session the {@link SparkSession} to run the builder against.
     */
    QueryData(SparkSession session) {
        this.session = requireNonNull(session, "Spark session must not be null");
    }

    /**
     * Create a new {@link QueryData} with its fields set to the values provided by a given {@link Map}.
     *
     * @param session the {@link SparkSession} to run the builder against.
     * @param queryMap a {@link Map} with the mandatory querying fields.
     */
    QueryData(SparkSession session, String idName, Map<String, String> queryMap) {
        this(session);

        requireNonNull(queryMap, "QueryMap cannot be null!");

        setId(idName, queryMap.get(idName));

        setStartTime(toInstant(queryMap.get(START_TIME_KEY)));
        setEndTime(toInstant(queryMap.get(END_TIME_KEY)));

        if (queryMap.containsKey(KEY_VALUES_KEY)) {
            Map<String, Map<String, String>> entities = deserializeOrThrow(queryMap.get(KEY_VALUES_KEY),
                    KEY_VALUES_TYPE);

            entities.forEach(this::addEntity);
        }

        if (queryMap.containsKey(FIELDS_KEY)) {
            addFields(deserializeOrThrow(queryMap.get(FIELDS_KEY), FIELDS_TYPE));
        }

        if (queryMap.containsKey(ALIASES_KEY)) {
            addAliases(deserializeOrThrow(queryMap.get(ALIASES_KEY), ALIASES_TYPE));
        }
    }

    private Instant toInstant(String time) {
        return TimeUtils.getInstantFromNanos(Long.valueOf(time));
    }

    void setId(String name, String value) {
        requireNonNull(name, "IdName cannot be null!");
        requireNonNull(value, "IdValue cannot be null!");
        id = Pair.of(name, value);
    }

    /**
     * Sets the left inclusive bound of the search interval.
     *
     * @param startTime an Instant with the time and date.
     */
    void setStartTime(Instant startTime) {
        this.startTime = requireNonNull(startTime, "StartTime cannot be null!");;
    }

    /**
     * Sets the left inclusive bound of the search interval.
     *
     * @param startTimeUtc a String with the time and date in UTC time with the following format:
     *                     yyyy-MM-dd HH:mm:ss.nnnnnn.
     */
    void setStartTime(String startTimeUtc) {
        requireNonNull(startTimeUtc, "StartTimeUtc cannot be null!");
        setStartTime(TimeUtils.getInstantFromString(startTimeUtc));
    }

    /**
     * Sets the right inclusive bound of the search interval.
     *
     * @param endTime an Instant with the time and date.
     */
    void setEndTime(Instant endTime) {
        this.endTime = requireNonNull(endTime, "EndTime cannot be null!");;
    }

    /**
     * Sets the right inclusive bound of the search interval.
     *
     * @param endTimeUtc a String with the time and date in UTC time with the following format:
     *                   yyyy-MM-dd HH:mm:ss.nnnnnn.
     */
    void setEndTime(String endTimeUtc) {
        requireNonNull(endTimeUtc, "EndTimeUtc cannot be null!");
        setEndTime(TimeUtils.getInstantFromString(endTimeUtc));
    }

    /**
     * Adds all the key/value pairs in the provided map to the entities of the query. If any key already exists,
     * its value is replaced with the newly provided value.
     *
     * @param name the name of the entity to add
     * @param entityToAdd a {@link Map} with the key and values to be added.
     * @throws NullPointerException if the provided {@link Map} or any of its elements (keys or values) is null.
     */
    void addEntity(String name, Map<String, String> entityToAdd) {
        requireNonNull(name, "Entity name cannot be null!");
        requireNonNullElements(entityToAdd, "Entity keys nor value cannot be null!");
        entities.put(name, ImmutableMap.copyOf(entityToAdd));
    }

    /**
     * Removes the entity with the provided list.
     *
     * @param name the name of the entity.
     */
    void removeEntity(String name) {
        requireNonNull(name, "Entity name cannot be null!");
        entities.remove(name);
    }

    /**
     * Removes all the entities of the query.
     */
    void clearEntities() {
        entities.clear();
    }

    /**
     * Adds all the addFields in the provided {@link List} to the query.
     *
     * @param fieldsToAdd a {@link List} of {@link String}s with the name of the addFields.
     */
    void addFields(Collection<String> fieldsToAdd) {
        requireNonNull(fieldsToAdd, "FieldsToAdd cannot be null!");
        Predicate<String> contains = fields::contains;

        fieldsToAdd.stream()
                .filter(Objects::nonNull)
                .filter(contains.negate())
                .forEach(fields::add);
    }

    /**
     * Removes all the addFields in the provided {@link List} from the query.
     *
     * @param fieldsToRemove a {@link List} of {@link String}s with the addFields to be added.
     */
    void removeFields(List<String> fieldsToRemove) {
        requireNonNull(fieldsToRemove, "FieldsToRemove cannot be null!");
        fields.removeAll(fieldsToRemove);
    }

    /**
     * Clears all the addFields of the query.
     */
    void clearFields() {
        fields.clear();
    }

    /**
     * Adds the addAliases in the provided {@link Map} to the query. If any alias already exists, its value is replaced
     * with the newly provided value.
     *
     * @param aliasesToAdd a {@link Map} with the addAliases to e added.
     */
    void addAliases(Map<String, List<String>> aliasesToAdd) {
        requireNonNull(aliasesToAdd, "AliasesToAdd cannot be null!");
        requireNonNullElements(aliasesToAdd, "Aliases");
        aliases.putAll(aliasesToAdd);
    }

    /**
     * Removes all the addAliases in the provided {@link List} from the query.
     *
     * @param aliasesToRemove a {@link List} with the addAliases to remove.
     */
    void removeAliases(List<String> aliasesToRemove) {
        requireNonNull(aliasesToRemove, "AliasesToRemove cannot be null!");
        aliasesToRemove.forEach(aliases::remove);
    }



    /**
     * Clears all the addAliases of the query.
     */
    void clearAliases() {
        aliases.clear();
    }

    Instant getEndTime() {
        return endTime;
    }

    Instant getStartTime() {
        return startTime;
    }

    Map<String, Map<String, String>> getEntities() {
        return Collections.unmodifiableMap(entities);
    }

    List<String> getFields() {
        return Collections.unmodifiableList(fields);
    }

    Map<String, List<String>> getAliases() {
        return Collections.unmodifiableMap(aliases);
    }

    Map<String, String> toMap() {
        return ImmutableMap.<String, String>builder()
            .put(id.getKey(), id.getValue())
            .put(START_TIME_KEY, String.valueOf(TimeUtils.getNanosFromInstant(startTime)))
            .put(END_TIME_KEY, String.valueOf(TimeUtils.getNanosFromInstant(endTime)))
            .put(KEY_VALUES_KEY, serializeOrThrow(entities))
            .put(FIELDS_KEY, serializeOrThrow(fields))
            .put(ALIASES_KEY, serializeOrThrow(aliases))
            .build();
    }

    Dataset<Row> buildDataset() {
        DataAccessService dataAccessService = DataAccessServiceFactory.createDataAccessService(session);
        QueryDataService queryDataService = QueryDataServiceFactory.createDataService();

        return dataAccessService.createDataSetFor(queryDataService.loadQueryData(this.toMap()));
    }

    private static void requireNonNullElements(Map<String, ?> mapToCheck, String name) {
        checkArgument(!mapToCheck.containsKey(null), "Keys of " + name + " cannot be null!");
        checkArgument(!mapToCheck.containsValue(null), "Values of " + name + " cannot be null!");
    }
}