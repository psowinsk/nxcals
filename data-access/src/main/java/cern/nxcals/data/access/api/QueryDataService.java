/*
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN),All Rights Reserved.
 */

package cern.nxcals.data.access.api;

import java.util.Map;
import java.util.Set;

/**
 * This component is responsible for transforming the user request data into the internal representation
 * {@link InternalQueryData} that is used for accessing data from both data sources through the central
 * {@link DataAccessService} entry point.
 *
 * @author ntsvetko
 */
public interface QueryDataService {
    Set<InternalQueryData> loadQueryData(Map<String, String> query);
}
