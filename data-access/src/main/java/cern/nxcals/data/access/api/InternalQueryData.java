/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.data.access.api;

import cern.nxcals.common.domain.EntityResources;

import java.time.Instant;
import java.util.Set;

/**
 * Object that keep the user preferences while requesting for data
 */
class InternalQueryData {
    private final EntityResources entityData;
    private final Instant startTime;
    private final Instant endTime;
    private final Set<DataAccessField> fields;
    private final String variableName;

    InternalQueryData(EntityResources entityData, Instant startTime, Instant endTime,
            Set<DataAccessField> fields, String variableName) {
        this.entityData = entityData;
        this.startTime = startTime;
        this.endTime = endTime;
        this.fields = fields;
        this.variableName = variableName;
    }

    EntityResources getEntityData() {
        return entityData;
    }

    Instant getStartTime() {
        return startTime;
    }

    Instant getEndTime() {
        return endTime;
    }

    Set<DataAccessField> getFields() {
        return fields;
    }

    String getVariableName() {
        return variableName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        InternalQueryData that = (InternalQueryData) o;

        if (!entityData.equals(that.entityData))
            return false;
        if (!startTime.equals(that.startTime))
            return false;
        if (!endTime.equals(that.endTime))
            return false;
        if (fields != null ? !fields.equals(that.fields) : that.fields != null)
            return false;
        return variableName != null ? variableName.equals(that.variableName) : that.variableName == null;
    }

    @Override
    public int hashCode() {
        int result = entityData.hashCode();
        result = 31 * result + startTime.hashCode();
        result = 31 * result + endTime.hashCode();
        result = 31 * result + (fields != null ? fields.hashCode() : 0);
        result = 31 * result + (variableName != null ? variableName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "InternalQueryData{" + "entityData=" + entityData + ", startTime=" + startTime + ", endTime=" + endTime
                + ", fields=" + fields + ", variableName='" + variableName + '\'' + '}';
    }
}
