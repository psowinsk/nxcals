/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.data.access.api;

import cern.nxcals.service.client.api.internal.InternalEntitiesResourcesService;
import cern.nxcals.service.client.api.internal.InternalSystemService;
import cern.nxcals.service.client.api.internal.InternalVariableService;
import cern.nxcals.service.client.providers.InternalServiceClientFactory;

/**
 * Default implementation of {@link QueryDataService}. It is transforming user request into {@link InternalQueryData}
 * which is used for building Spark requests to both datasources (HBase nad HDFS).
 */
public final class QueryDataServiceFactory {
    private static final InternalSystemService systemService = InternalServiceClientFactory.createSystemService();
    private static final InternalVariableService variableService = InternalServiceClientFactory.createVariableService();
    private static final InternalEntitiesResourcesService entityService = InternalServiceClientFactory
            .createEntityResourceService();

    private QueryDataServiceFactory() {
    }

    public static QueryDataService createDataService() {
        return new QueryDataServiceImpl(systemService, variableService, entityService);
    }
}
