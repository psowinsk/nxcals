/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.data.access.api;

import cern.nxcals.common.domain.TimeWindow;
import lombok.RequiredArgsConstructor;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.Optional;


@RequiredArgsConstructor
class DataSourceTimeWindow {

    static final int HBASE_PERIOD_HOURS = 36;
    private final TimeWindow hdfsTimeWindow;
    private final TimeWindow hbaseTimeWindow;

    static DataSourceTimeWindow getTimeWindows(Instant twStartTime, Instant twEndTime) {
        return getTimeWindows(twStartTime, twEndTime, Clock.systemDefaultZone());
    }

    static DataSourceTimeWindow getTimeWindows(Instant twStartTime, Instant twEndTime, Clock clock) {
        if (twEndTime.isBefore(twStartTime)) {
            throw new IllegalArgumentException("EndTime is before StartTime! Wrong TimeWindow definition!");
        }

        Instant split = clock.instant().minus(Duration.ofHours(HBASE_PERIOD_HOURS));

        if (!split.isAfter(twStartTime)) {
            return new DataSourceTimeWindow(null, TimeWindow.between(twStartTime, twEndTime));
        }

        if (!split.isBefore(twEndTime)) {
            return new DataSourceTimeWindow(TimeWindow.between(twStartTime, twEndTime), null);
        }

        Instant openEnd = split.minus(Duration.ofNanos(1));
        return new DataSourceTimeWindow(TimeWindow.between(twStartTime, openEnd), TimeWindow.between(split, twEndTime));
    }

    Optional<TimeWindow> getHdfsTimeWindow() {
        return Optional.ofNullable(hdfsTimeWindow);
    }

    Optional<TimeWindow> getHbaseTimeWindow() {
        return Optional.ofNullable(hbaseTimeWindow);
    }
}
