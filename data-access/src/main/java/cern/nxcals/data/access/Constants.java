/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.data.access;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.type.TypeFactory;
import org.codehaus.jackson.type.JavaType;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Holder class for constants common to both frontend (builders) and backend (api) layers.
 */
public class Constants {

    private Constants() {
        /* Nothing to do here */
    }

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static final TypeFactory TYPE_FACTORY = OBJECT_MAPPER.getTypeFactory();
    private static final JavaType STRING_JAVA_TYPE = TYPE_FACTORY.constructType(String.class);

    public static final JavaType KEY_VALUES_TYPE = TYPE_FACTORY.constructMapType(HashMap.class,
            STRING_JAVA_TYPE, TYPE_FACTORY.constructMapType(HashMap.class, STRING_JAVA_TYPE, STRING_JAVA_TYPE));
    private static final JavaType ALIASES_VALUE_TYPE = TYPE_FACTORY
            .constructCollectionType(ArrayList.class, String.class);
    public static final JavaType ALIASES_TYPE = TYPE_FACTORY
            .constructMapType(HashMap.class, STRING_JAVA_TYPE, ALIASES_VALUE_TYPE);
    public static final JavaType FIELDS_TYPE = TYPE_FACTORY.constructCollectionType(ArrayList.class, STRING_JAVA_TYPE);

    public static final String SYSTEM_KEY = "system";
    public static final String START_TIME_KEY = "start_time";
    public static final String END_TIME_KEY = "end_time";
    public static final String KEY_VALUES_KEY = "key_values";
    public static final String FIELDS_KEY = "fields_key";
    public static final String ALIASES_KEY = "aliases_key";
    public static final String VARIABLE_KEY = "variable";
}