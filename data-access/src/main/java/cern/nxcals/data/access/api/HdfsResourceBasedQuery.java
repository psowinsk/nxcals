package cern.nxcals.data.access.api;

import java.net.URI;
import java.util.List;
import java.util.Set;

public class HdfsResourceBasedQuery extends ResourcesBasedQuery<URI> {

    private final String timestampField;

    public HdfsResourceBasedQuery(Set<ResourceQuery<URI>> resource, List<DataAccessField> fields,
            String timestampField) {
        super(resource, fields);
        this.timestampField = timestampField;
    }

    public String getTimestampField() {
        return timestampField;
    }
}
