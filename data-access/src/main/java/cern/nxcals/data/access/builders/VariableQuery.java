/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.data.access.builders;

import com.google.common.base.Preconditions;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import java.util.Objects;

import static cern.nxcals.data.access.Constants.VARIABLE_KEY;

public class VariableQuery {

    public static VariableStage builder(SparkSession session) {
        return new InternalBuilder(session);
    }

    public static Modifier modifier(SparkSession session, Map<String, String> queryMap) {
        return new Modifier(session, queryMap);
    }

    private VariableQuery() {
        /* Nothing to do here */
    }

    static class InternalBuilder
            implements VariableStartTimeStage, VariableEndTimeStage, VariableBuildQueryStage, VariableStage {

        private final QueryData data;

        private InternalBuilder(SparkSession session) {
            data = new QueryData(session);
        }

        @Override
        public VariableStartTimeStage variable(String name) {
            Objects.requireNonNull(name, "VariableName cannot be null!");
            data.setId(VARIABLE_KEY, name);
            return this;
        }

        @Override
        public VariableEndTimeStage startTime(Instant time) {
            data.setStartTime(time);
            return this;
        }

        @Override
        public VariableEndTimeStage startTime(String timeUtc) {
            data.setStartTime(timeUtc);
            return this;
        }

        @Override
        public VariableBuildQueryStage atTime(Instant timeStamp) {
            return startTime(timeStamp).endTime(timeStamp);
        }

        @Override
        public VariableBuildQueryStage atTime(String timestampString) {
            return startTime(timestampString).endTime(timestampString);
        }

        @Override
        public VariableBuildQueryStage endTime(Instant time) {
            data.setEndTime(time);
            return this;
        }

        @Override
        public VariableBuildQueryStage endTime(String timeUtc) {
            data.setEndTime(timeUtc);
            return this;
        }

        @Override
        public VariableBuildQueryStage duration(Duration duration) {
            Preconditions.checkArgument(!duration.isNegative(), "Query duration must be positive.");
            return endTime(data.getStartTime().plus(duration));
        }

        Map<String, String> toMap() {
            return data.toMap();
        }

        @Override
        public Dataset<Row> buildDataset() {
            return data.buildDataset();
        }

    }

    public static class Modifier {

        private final QueryData data;

        private Modifier(SparkSession session, Map<String, String> queryMap) {
            data = new QueryData(session, VARIABLE_KEY, queryMap);
        }

        public Modifier variable(String name) {
            data.setId(VARIABLE_KEY, name);
            return this;
        }

        public Modifier startTime(Instant time) {
            data.setStartTime(time);
            return this;
        }

        public Modifier startTime(String timeUtc) {
            data.setStartTime(timeUtc);
            return this;
        }

        public Modifier atTime(Instant timeStamp) {
            startTime(timeStamp).endTime(timeStamp);
            return this;
        }

        public Modifier atTime(String timestampString) {
            startTime(timestampString).endTime(timestampString);
            return this;
        }

        public Modifier endTime(Instant time) {
            data.setEndTime(time);
            return this;
        }

        public Modifier endTime(String timeUtc) {
            data.setEndTime(timeUtc);
            return this;
        }

        public Modifier duration(Duration duration) {
            Preconditions.checkArgument(!duration.isNegative(), "Query duration must be positive.");
            return endTime(data.getStartTime().plus(duration));
        }

        Map<String, String> toMap() {
            return data.toMap();
        }

        public Dataset<Row> buildDataset() {
            return data.buildDataset();
        }

    }

    public interface VariableStage {
        VariableStartTimeStage variable(String variableName);
    }

    public interface VariableStartTimeStage {
        VariableEndTimeStage startTime(Instant startTime);

        VariableEndTimeStage startTime(String startTimeUtc);

        VariableBuildQueryStage atTime(Instant timeStamp);
        VariableBuildQueryStage atTime(String timestampString);

    }
    public interface VariableEndTimeStage {
        VariableBuildQueryStage endTime(Instant endTime);

        VariableBuildQueryStage endTime(String endTimeUtc);
        VariableBuildQueryStage duration(Duration duration);

    }
    public interface VariableBuildQueryStage {
        Dataset<Row> buildDataset();
    }
}
