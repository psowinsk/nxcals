package cern.nxcals.data.access.builders.fluent.stages;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.Map;

public interface BuildStage<T> extends EntityStage<T> {
    Dataset<Row> buildDataset();
}
