package cern.nxcals.data.access.builders.fluent.stages;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface DataContextStage<T> extends EntityStage<T> {
    DataContextStage<T> fields(String... fields);

    DataContextStage<T> fields(Collection<String> fields);

    DataContextStage<T> fieldAliases(Map<String, List<String>> aliases);
}
