/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.data.access.api;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;
import java.util.function.IntFunction;
import java.util.stream.Collectors;

import static cern.nxcals.common.SystemFields.NXC_EXTR_ENTITY_ID;
import static java.util.stream.Collectors.toSet;

/**
 * Implementation of {@link InternalDataAccessService} for Hdfs.
 * The responsibility of this class is to obtain the HDFS data for the requested entities and time window.
 */
class HdfsDataAccessServiceImpl implements InternalDataAccessService<HdfsResourceBasedQuery> {
    private static final Logger LOGGER = LoggerFactory.getLogger(HdfsDataAccessServiceImpl.class);
    private static final String[] EMPTY_ARRAY = new String[0];
    private final SparkSession sparkSession;
    private final FileSystem fileSystem;

    HdfsDataAccessServiceImpl(SparkSession sparkSession, FileSystem fileSystem) {
        this.sparkSession = Objects.requireNonNull(sparkSession);
        this.fileSystem = Objects.requireNonNull(fileSystem);
    }

    @Override
    public Dataset<Row> getDataFor(HdfsResourceBasedQuery query) {
        String timestampField = getTimeStampFieldName(query.getTimestampField(), query.getFields());

        Dataset<Row> resultSet = null;

        for (ResourceQuery<URI> resourceQuery : query.getResources()) {
            String[] cols = resourceQuery.getColumnsAsArray();
            String[] paths = resourceQuery.getResources().stream()
                    .filter(this::pathExists)
                    .map(URI::toString)
                    .collect(Collectors.toSet())
                    .toArray(EMPTY_ARRAY);

            LOGGER.debug(
                    "Calls getHdfsDataFor entity_ids = {} , timestampField = {}, timeWindow (startTime = {} : endTime = {})"
                            + ",  fields = {}", resourceQuery.getEntityIds(), timestampField,
                    resourceQuery.getStartTime(), resourceQuery.getEndTime(), query.getFields());

            LOGGER.debug("Columns = {}", Arrays.toString(cols));
            if (paths.length > 0) {
                LOGGER.debug("Start loading parquet files");
                Dataset<Row> dataset = sparkSession.read().load(paths).selectExpr(cols);
                LOGGER.debug("Loaded parquet files");
                dataset = dataset.where(createPredicate(resourceQuery.getEntityIds(), timestampField,
                        resourceQuery.getStartTime(), resourceQuery.getEndTime()));
                LOGGER.debug("Filtered dataset by entity and timestamps");
                if (resultSet == null) {
                    resultSet = dataset;
                    LOGGER.debug("Assigned first dataset");

                } else {
                    resultSet = resultSet.union(dataset);
                    LOGGER.debug("Performed union on resultSet.union(dataset) successfully!");
                }
            }
        }

        if (resultSet == null) {
            return this.sparkSession
                    .createDataFrame(new ArrayList<>(), SparkTypeUtils.getStructSchemaFor(query.getFields()));
        } else {
            return resultSet;
        }
    }

    private String getTimeStampFieldName(String timestampField, Collection<DataAccessField> fields) {
        for (DataAccessField field : fields) {
            if (field.getFieldName().equals(timestampField) && field.getAlias() != null) {
                return field.getAlias();
            }
        }
        return timestampField;
    }

    private boolean pathExists(URI uri) {
        Path path = new Path(uri);
        // I assume that the returned path is of a form: /xyz/abc/*.parquet. Removing *.parquet and checking if this
        // path exists.
        try {
            LOGGER.debug("Checking paths start");
            boolean result = false;
            if (fileSystem.exists(path.getParent())) {
                RemoteIterator<LocatedFileStatus> remoteIterator = fileSystem
                        .listFiles(path.getParent(), false);
                result = remoteIterator.hasNext() && remoteIterator.next().getPath().getName().endsWith(".parquet");
            }
            LOGGER.debug("Checking paths finished");
            return result;
        } catch (IOException e) {
            throw new IllegalStateException("Cannot access hdfs file system to check for path " + path, e);
        }
    }

    private Column createPredicate(Set<Long> ids, String timestampField, long startTime, long endTime) {
        Column columnIdPredicate = null;

        for (long id : ids) {
            if (columnIdPredicate == null) {
                columnIdPredicate = new Column(NXC_EXTR_ENTITY_ID.getValue()).equalTo(id);
            } else {
                columnIdPredicate = columnIdPredicate.or(new Column(NXC_EXTR_ENTITY_ID.getValue()).equalTo(id));
            }
        }

        return columnIdPredicate.and(new Column(timestampField).geq(startTime))
                .and(new Column(timestampField).leq(endTime));
    }
}
