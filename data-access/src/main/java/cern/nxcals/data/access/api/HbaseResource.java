package cern.nxcals.data.access.api;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class HbaseResource {
    private final String tableName;
    private final String namespace;
}
