/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.data.access.api;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.util.Set;

/**
 * This service represents the single data access point. It provides access to methods that will be used by user APIs
 * for creating Spark datasets.
 */
public interface DataAccessService {
    Dataset<Row> createDataSetFor(Set<InternalQueryData> queryData);
}