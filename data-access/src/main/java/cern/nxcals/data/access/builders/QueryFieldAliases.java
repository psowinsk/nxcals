

package cern.nxcals.data.access.builders;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by ntsvetko on 5/3/17.
 */
public class QueryFieldAliases {

    public static FieldsStage alias(String alias) {
        return new Internal(alias);
    }

    public static class Internal implements FieldsStage, BuildStage {

        private static final String FIELD_NULL = "Field aliases cannot be built without fields specified and cannot contain nulls!";
        private static final String ALIAS_NULL = "Alias cannot be NULL!";

        Map<String, List<String>> aliasMap = new HashMap<>();

        String currentAlias;
        private Internal(String alias) {
            alias(alias);
        }

        @Override
        public FieldsStage alias(String alias) {
            currentAlias = checkNotNull(alias, ALIAS_NULL);
            return this;
        }

        @Override
        public BuildStage fields(String... fields) {
            List<String> fieldList = Lists.newArrayList(fields);

            checkArgument(!fieldList.isEmpty(), FIELD_NULL);
            checkArgument(!fieldList.contains(null), FIELD_NULL);

            aliasMap.put(currentAlias, fieldList);
            return this;
        }

        @Override
        public Map<String, List<String>> build() {
            return ImmutableMap.copyOf(aliasMap);
        }

    }

    public interface FieldsStage {
        BuildStage fields(String... fields);
    }

    public interface BuildStage {
        FieldsStage alias(String alias);

        Map<String, List<String>> build();
    }
}