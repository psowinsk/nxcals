/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.data.access.api;

import cern.nxcals.common.utils.IllegalCharacterConverter;
import org.apache.avro.Schema;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import java.util.List;
import java.util.stream.Collectors;

import static org.apache.spark.sql.types.DataTypes.BooleanType;
import static org.apache.spark.sql.types.DataTypes.DoubleType;
import static org.apache.spark.sql.types.DataTypes.FloatType;
import static org.apache.spark.sql.types.DataTypes.IntegerType;
import static org.apache.spark.sql.types.DataTypes.LongType;
import static org.apache.spark.sql.types.DataTypes.NullType;
import static org.apache.spark.sql.types.DataTypes.StringType;

final class SparkTypeUtils {
    private static final IllegalCharacterConverter ILLEGAL_CHARACTER_CONVERTER = IllegalCharacterConverter.get();

    private SparkTypeUtils() {
    }

    static DataType getDataTypeFor(Schema schema) {
        switch (schema.getType()) {
        case STRING:
            return StringType;
        case BYTES:
            return IntegerType;
        case INT:
            return IntegerType;
        case LONG:
            return LongType;
        case FLOAT:
            return FloatType;
        case DOUBLE:
            return DoubleType;
        case BOOLEAN:
            return BooleanType;
        case NULL:
            return NullType;
        case ARRAY:
            DataType type = getDataTypeFor(schema.getElementType());
            return DataTypes.createArrayType(type);
        case RECORD:
            return DataTypes.createStructType(createStructFields(schema));
        case UNION:
            return getDataTypeFor(getSchemaUnionType(schema));
        default:
            throw new IllegalArgumentException("Unknown schema type = " + schema.getType());
        }
    }

    static Schema getSchemaUnionType(Schema schema) {
        return schema.getTypes().stream().filter(s -> s.getType() != Schema.Type.NULL).findFirst().orElseThrow(
                () -> new IllegalArgumentException("Provided UNION schema is missing field type " + schema));
    }

    static StructType getStructSchemaFor(List<DataAccessField> fields) {
        StructType schema = new StructType();
        List<DataAccessField> dataSetFields = fields.stream().map(f -> f.getAlias() == null ?
                f :
                new DataAccessFieldBuilder(f.getAlias()).dataType(f.getDataType()).fieldSchema(f.getFieldSchema())
                        .build()).distinct().collect(Collectors.toList());

        for (DataAccessField f : dataSetFields) {
            String fieldName = f.getFieldName();
            if(ILLEGAL_CHARACTER_CONVERTER.isEncoded(f.getFieldName())) {
                fieldName = ILLEGAL_CHARACTER_CONVERTER.convertFromLegal(fieldName);
            }
            schema = schema.add(fieldName, f.getDataType(), true);
        }
        return schema;
    }

    private static List<StructField> createStructFields(Schema schema) {
        //Should we check for nullability?, currently I've set all endTime nullable (jwozniak)
        return schema.getFields().stream()
                .map(field -> DataTypes.createStructField(field.name(), getDataTypeFor(field.schema()), true))
                .collect(Collectors.toList());
    }
}
