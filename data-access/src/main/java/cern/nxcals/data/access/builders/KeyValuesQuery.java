/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.data.access.builders;

import cern.nxcals.data.access.builders.fluent.stages.BuildStage;
import cern.nxcals.data.access.builders.fluent.stages.SystemStage;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.time.Instant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static cern.nxcals.data.access.Constants.SYSTEM_KEY;
import static java.util.Objects.requireNonNull;

/**
 * Builder responsible for the definition of the base mandatory stages of the fluent API for querying generic data using
 * key/value pairs.
 *
 * @see QueryData
 */
public class KeyValuesQuery {

    public static SystemStage<KeyValueStage> builder(SparkSession session) {
        return new Internal(session);
    }

    public static Modifier modifier(SparkSession session, Map<String, String> queryMap) {
        return new Modifier(session, queryMap);
    }

    private KeyValuesQuery() {
        /* Nothing to do here */
    }

    static class Internal extends FluentQuery<KeyValueStage>
            implements KeyValueStage, OngoingKeyValueStage {

        private OngoingEntity ongoingEntity = null;

        //This should stay protected as there are people using it (in PM)
        private Internal(SparkSession session) {
            super(session);
        }

        @Override
        public KeyValueStage entity(String name) {
            preBuild();
            requireNonNull(name, "Entity name cannot be null!");
            ongoingEntity = new OngoingEntity(name);
            return this;
        }

        @Override
        protected void preBuild() {
            if (ongoingEntity != null) {
                addEntity(ongoingEntity.getName(), ongoingEntity.getKeyValues());
            }
        }

        @Override
        public BuildStage<KeyValueStage> keyValues(Map<String, String> keyValues) {
            ongoingEntity.getKeyValues().putAll(keyValues);
            return this;
        }

        @Override
        public OngoingKeyValueStage keyValue(String key, String value) {
            ongoingEntity.getKeyValues().put(key, value);
            return this;
        }
    }

    public static class Modifier {

        private final QueryData data;

        private Modifier(SparkSession session, Map<String, String> queryMap) {
            data = new QueryData(session, SYSTEM_KEY, queryMap);
        }

        public Modifier system(String system) {
            data.setId(SYSTEM_KEY, system);
            return this;
        }

        public Modifier startTime(Instant startTime) {
            data.setStartTime(startTime);
            return this;
        }

        public Modifier startTime(String startTimeUtc) {
            data.setStartTime(startTimeUtc);
            return this;
        }

        public Modifier endTime(Instant endTime) {
            data.setEndTime(endTime);
            return this;
        }

        public Modifier endTime(String endTimeUtc) {
            data.setEndTime(endTimeUtc);
            return this;
        }

        public Modifier atTime(Instant timeStamp) {
            return startTime(timeStamp).endTime(timeStamp);
        }

        public Modifier atTime(String timestampString) {
            return startTime(timestampString).endTime(timestampString);
        }

        public Modifier addFields(String... fields) {
            data.addFields(Arrays.asList(fields));
            return this;
        }

        public Modifier removeFields(String... fields) {
            data.removeFields(Arrays.asList(fields));
            return this;
        }

        public Modifier removeAllFields() {
            data.clearFields();
            return this;
        }

        public Modifier addAliases(Map<String, List<String>> aliases) {
            data.addAliases(aliases);
            return this;
        }

        public Modifier removeAliases(String... keys) {
            data.removeAliases(Arrays.asList(keys));
            return this;
        }

        public Modifier removeAllAliases() {
            data.clearAliases();
            return this;
        }

        public Modifier addEntity(Map<String, String> entity) {
            data.addEntity(UUID.randomUUID().toString(), entity);
            return this;
        }

        public Modifier addEntity(String name, Map<String, String> entity) {
            data.addEntity(name, entity);
            return this;
        }

        public Modifier removeEntity(String name) {
            data.removeEntity(name);
            return this;
        }

        public Modifier removeAllEntities() {
            data.clearEntities();
            return this;
        }

        Map<String, String> toMap() {
            return data.toMap();
        }

        public Dataset<Row> buildDataset() {
            return data.buildDataset();
        }
    }

    public interface OngoingKeyValueStage extends BuildStage<KeyValueStage> {
        OngoingKeyValueStage keyValue(String key, String value);
    }

    public interface KeyValueStage {
        BuildStage<KeyValueStage> keyValues(Map<String, String> keyValues);

        OngoingKeyValueStage keyValue(String key, String value);
    }

    private static class OngoingEntity {
        private final String name;
        private Map<String, String> keyValues = new HashMap<>();

        OngoingEntity(String name) {
            this.name = name;
        }

        String getName() {
            return name;
        }

        Map<String, String> getKeyValues() {
            return keyValues;
        }
    }
}