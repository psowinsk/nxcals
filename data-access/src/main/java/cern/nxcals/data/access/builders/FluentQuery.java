package cern.nxcals.data.access.builders;

import cern.nxcals.data.access.builders.fluent.stages.BuildStage;
import cern.nxcals.data.access.builders.fluent.stages.DataContextStage;
import cern.nxcals.data.access.builders.fluent.stages.EndTimeStage;
import cern.nxcals.data.access.builders.fluent.stages.EntityStage;
import cern.nxcals.data.access.builders.fluent.stages.StartTimeStage;
import cern.nxcals.data.access.builders.fluent.stages.SystemStage;
import com.google.common.base.Preconditions;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

import static cern.nxcals.data.access.Constants.SYSTEM_KEY;

abstract class FluentQuery<T> implements StartTimeStage<T>, EndTimeStage<T>,
        DataContextStage<T>, BuildStage<T>, EntityStage<T>, SystemStage<T> {

    private final QueryData data;

    FluentQuery(SparkSession session) {
        this.data = new QueryData(session);
    }

    @Override
    public StartTimeStage<T> system(String system) {
        data.setId(SYSTEM_KEY, system);
        return this;
    }

    @Override
    public EndTimeStage<T> startTime(Instant startTime) {
        data.setStartTime(startTime);
        return this;
    }

    @Override
    public EndTimeStage<T> startTime(String startTimeUtc) {
        data.setStartTime(startTimeUtc);
        return this;
    }

    @Override
    public DataContextStage<T> endTime(Instant endTime) {
        data.setEndTime(endTime);
        return this;
    }

    @Override
    public DataContextStage<T> endTime(String endTimeUtc) {
        data.setEndTime(endTimeUtc);
        return this;
    }

    @Override
    public DataContextStage<T> duration(Duration duration) {
        Objects.requireNonNull(duration, "Query duration cannot be null!");
        Preconditions.checkArgument(!duration.isNegative(), "Query duration must be positive.");

        return endTime(data.getStartTime().plus(duration));
    }

    @Override
    public DataContextStage<T> atTime(Instant timeStamp) {
        return startTime(timeStamp).endTime(timeStamp);
    }

    @Override
    public DataContextStage<T> atTime(String timestampString) {
        return startTime(timestampString).endTime(timestampString);
    }

    @Override
    public DataContextStage<T> fields(String... fields) {
        data.addFields(Arrays.asList(fields));
        return this;
    }

    @Override
    public DataContextStage<T> fields(Collection<String> fields) {
        if (fields != null) {
            data.addFields(fields);
        }
        return this;
    }

    @Override
    public DataContextStage<T> fieldAliases(Map<String, List<String>> aliases) {
        data.addAliases(aliases);
        return this;
    }

    void addEntity(String name, Map<String, String> entityToAdd) {
        data.addEntity(name, entityToAdd);
    }

    Map<String, String> toMap() {
        preBuild();
        return data.toMap();
    }

    @Override
    public Dataset<Row> buildDataset() {
        preBuild();
        return data.buildDataset();
    }

    public T entity() {
        return entity(UUID.randomUUID().toString());
    }

    protected abstract void preBuild();
}
