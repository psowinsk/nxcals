package cern.nxcals.data.access.builders.fluent.stages;

public interface EntityStage<T> {
    T entity();

    T entity(String name);
}
