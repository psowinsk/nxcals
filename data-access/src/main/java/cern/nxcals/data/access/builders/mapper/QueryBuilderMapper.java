/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.data.access.builders.mapper;

import cern.nxcals.data.access.ExceptionsFactory;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.JavaType;

import java.io.IOException;

/**
 * Utility class providing Serialization/Deserialization functionality to/from {@link String}
 */
public final class QueryBuilderMapper {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private QueryBuilderMapper() {
        /* Nothing to do here */
    }

    /**
     * Serializes any type of Java {@link Object} object to {@link String}.
     * The implementation is based on the {@link ObjectMapper}
     *
     * @param objectToSerialize {@link Object } to be serialized
     * @return String representation of the Java object
     */
    public static String serializeOrThrow(Object objectToSerialize) {
        try {
            return OBJECT_MAPPER.writeValueAsString(objectToSerialize);
        } catch (IOException exception) {
            throw ExceptionsFactory.createSerializationException(objectToSerialize, exception);
        }
    }

    /**
     * Deserializes {@link String} object provided with its {@link JavaType}.
     * The implementation is based on the {@link ObjectMapper}
     *
     * @param objectToDeserialize {@link String} representation of the object to be deserialized
     * @param javaType            {@link JavaType} specifying the deserializer to be used by Jackson
     * @return {@link Object} deserialized from its String representation
     */
    public static <T> T deserializeOrThrow(String objectToDeserialize, JavaType javaType) {
        try {
            return OBJECT_MAPPER.readValue(objectToDeserialize, javaType);
        } catch (IOException exception) {
            throw ExceptionsFactory.createDeserializationException(objectToDeserialize, exception);
        }
    }
}