package cern.nxcals.data.access.api;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;
import java.util.Set;

/**
 * Represents a set of resources to be fetched using a single load from the spark querying API.
 *
 * @param <T> the type of resource to be used.
 */
@Getter
@AllArgsConstructor
public class ResourceQuery<T> {
    private static final String[] EMPTY_ARRAY = new String[0];

    private final Set<T> resources;
    private final Set<Long> entityIds;
    private final List<String> columns;
    private final long startTime;
    private final long endTime;

    String[] getColumnsAsArray() {
        return this.getColumns().toArray(EMPTY_ARRAY);
    }
}