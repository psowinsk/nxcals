package cern.nxcals.data.access.builders.fluent.stages;

import java.time.Instant;

public interface StartTimeStage<T> {
    EndTimeStage<T> startTime(Instant startTime);

    EndTimeStage<T> startTime(String startTimeUtc);

    DataContextStage<T> atTime(Instant timeStamp);

    DataContextStage<T> atTime(String timestampString);
}
