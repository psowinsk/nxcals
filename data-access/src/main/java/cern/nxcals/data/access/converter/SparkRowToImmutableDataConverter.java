/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.data.access.converter;

import cern.cmw.data.DataFactory;
import cern.cmw.data.DiscreteFunction;
import cern.cmw.datax.ImmutableData;
import cern.cmw.datax.ImmutableEntry;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.ArrayType;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import scala.collection.mutable.WrappedArray;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Optional;

import static cern.nxcals.common.avro.SchemaConstants.ARRAY_DIMENSIONS_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.ARRAY_ELEMENTS_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.DDF_X_ARRAY_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.DDF_Y_ARRAY_FIELD_NAME;

/**
 * Class capable of converting spark {@link Row} objects into {@link ImmutableData} objects. <br>
 *
 * @author timartin
 * @author mamajews
 */
public class SparkRowToImmutableDataConverter {

    private static final String[] ARRAY_FIELD_NAMES = new String[] { ARRAY_ELEMENTS_FIELD_NAME,
            ARRAY_DIMENSIONS_FIELD_NAME };
    private static final String[] XY_ARRAY_NAMES = new String[] { DDF_X_ARRAY_FIELD_NAME, DDF_Y_ARRAY_FIELD_NAME };

    private SparkRowToImmutableDataConverter() {
        /* Functional static class */
    }

    /**
     * Converts a {@link Row} instance into an {@link ImmutableData} instance.
     *
     * @param source the instance to be converted
     * @return a new {@link ImmutableData} instance representing the same informtaion of the provided {@link Row}.
     */
    public static ImmutableData convert(Row source) {
        if (source == null) {
            return null;
        }
        return Arrays.stream(source.schema().fields())
                .map(field -> convertToEntry(source, field))
                .collect(ImmutableDataCollector.instance());
    }

    private static ImmutableEntry convertToEntry(Row source, StructField field) {
        DataType dataType = field.dataType();
        Object value = source.getAs(field.name());
        if (!(dataType instanceof StructType)) {
            return ImmutableEntry.of(field.name(), value);
        } else if (compareFields((StructType) dataType, ARRAY_FIELD_NAMES)) {
            return getImmutableEntryFromArray(field, (Row) value);
        } else if (compareFields((StructType) dataType, XY_ARRAY_NAMES)) {
            return ImmutableEntry.of(field.name(), getDiscreteFunctionFromRow((Row) value));
        } else {
            return ImmutableEntry.of(field.name(), convert((Row) value));
        }
    }

    private static ImmutableEntry getImmutableEntryFromArray(StructField field, Row value) {
        final StructField[] arrayFields = ((StructType) field.dataType()).fields();
        final Optional<StructField> optionalField = Arrays.stream(arrayFields)
                .filter(fld -> ARRAY_ELEMENTS_FIELD_NAME.equals(fld.name()))
                .findAny();

        if (optionalField.isPresent()) {
            DataType elementsType = ((ArrayType) optionalField.get().dataType()).elementType();

            Object elements = tryGetPrimitiveValue(value.getAs(ARRAY_ELEMENTS_FIELD_NAME));
            int[] dimensions = tryGetPrimitiveValue(value.getAs(ARRAY_DIMENSIONS_FIELD_NAME));

            if (elementsType instanceof StructType) {
                if (compareFields((StructType) elementsType, XY_ARRAY_NAMES)) {
                    elements = Arrays.stream((Row[]) elements)
                            .map(SparkRowToImmutableDataConverter::getDiscreteFunctionFromRow)
                            .toArray(length -> createArray(DiscreteFunction.class, length));
                } else {
                    elements = Arrays.stream((Row[]) elements)
                            .map(SparkRowToImmutableDataConverter::convert)
                            .toArray(length -> createArray(ImmutableData.class, length));
                }
            }
            return ImmutableEntry.of(field.name(), elements, dimensions);
        } else {
            throw new IllegalArgumentException("Not supported");
        }
    }

    private static DiscreteFunction getDiscreteFunctionFromRow(Row value) {
        double[] xArray = tryGetPrimitiveValue(value.getAs(DDF_X_ARRAY_FIELD_NAME));
        double[] yArray = tryGetPrimitiveValue(value.getAs(DDF_Y_ARRAY_FIELD_NAME));
        return DataFactory.createDiscreteFunction(xArray, yArray);
    }

    private static boolean compareFields(StructType structType, String[] fields) {
        return structType.length() == 2 && Arrays.equals(fields, structType.fieldNames());
    }

    private static <T> T[] createArray(Class<T> elementsClass, int length) {
        return (T[]) Array.newInstance(elementsClass, length);
    }

    @VisibleForTesting
    static <T> T tryGetPrimitiveValue(Object parameterValue) {
        Object toReturn = parameterValue;

        if (toReturn instanceof WrappedArray) {
            toReturn = ((WrappedArray) toReturn).array();
        }

        // This is needed because EntryType from ImmutableData does not accept arrays of boxed values so we must convert to their primitive counterpart!
        if (toReturn.getClass().isArray()) {
            Class<?> componentType = toReturn.getClass().getComponentType();
            if (Boolean.class.equals(componentType)) {
                toReturn = ArrayUtils.toPrimitive((Boolean[]) toReturn);
            } else if (Byte.class.equals(componentType)) {
                toReturn = ArrayUtils.toPrimitive((Byte[]) toReturn);
            } else {
                toReturn = ArrayUtils.toPrimitive(toReturn);

            }
        }

        return (T) toReturn;
    }
}
