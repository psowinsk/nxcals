package cern.nxcals.data.access.api;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

/**
 * Service providing access to the data.
 * Concrete implementations provide access to different sources of data.
 * The result set is return as {@link Dataset}
 */
public interface InternalDataAccessService<T> {
    Dataset<Row> getDataFor(T query);
}
