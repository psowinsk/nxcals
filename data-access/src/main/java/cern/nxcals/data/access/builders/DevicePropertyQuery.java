/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.data.access.builders;

import cern.nxcals.data.access.builders.fluent.stages.BuildStage;
import cern.nxcals.data.access.builders.fluent.stages.SystemStage;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static cern.nxcals.data.access.Constants.SYSTEM_KEY;
import static java.util.Objects.requireNonNull;

/**
 * Builder responsible for the definition of the base mandatory stages of the fluent API for querying using
 * device/property pairs.
 */
public class DevicePropertyQuery {

    public static SystemStage<DeviceStage> builder(SparkSession session) {
        return new Internal(session);
    }

    public static Modifier modifier(SparkSession session, Map<String, String> queryMap) {
        return new Modifier(session, queryMap);
    }

    private static final Pattern PARAMETER_PATTERN = Pattern.compile("^([.a-zA-Z0-9_-]+)/([.a-zA-Z0-9_-]+)$");
    @VisibleForTesting
    static final String DEVICE_KEY = "device";
    @VisibleForTesting
    static final String PROPERTY_KEY = "property";

    private DevicePropertyQuery() {
        /* Nothing to do here */
    }

    static class Internal extends FluentQuery<DeviceStage>
            implements DeviceStage, PropertyStage<DeviceStage> {

        private OngoingEntity ongoingEntity = null;

        Internal(SparkSession session) {
            super(session);
        }

        @Override
        public DeviceStage entity(String name) {
            preBuild();
            requireNonNull(name, "Entity name cannot be null!");
            ongoingEntity = new OngoingEntity(name);
            return this;
        }

        @Override
        protected void preBuild() {
            if (ongoingEntity != null) {
                addEntity(ongoingEntity.getName(),
                        toKeyValue(ongoingEntity.getDevice(), ongoingEntity.getProperty()));
                ongoingEntity = null;
            }
        }

        @Override
        public PropertyStage device(String device) {
            ongoingEntity.setDevice(requireNonNull(device));
            return this;
        }

        @Override
        public BuildStage<DeviceStage> property(String property) {
            ongoingEntity.setProperty(requireNonNull(property));
            return this;
        }

        @Override
        public BuildStage parameter(String parameter) {
            Pair<String, String> deviceProperty = extractDeviceProperty(parameter);
            return device(deviceProperty.getLeft()).property(deviceProperty.getRight());
        }
    }

    public static class Modifier {
        private final QueryData data;

        private Modifier(SparkSession session, Map<String, String> queryMap) {
            data = new QueryData(session, SYSTEM_KEY, queryMap);
        }

        public Modifier system(String system) {
            data.setId(SYSTEM_KEY, system);
            return this;
        }

        public Modifier startTime(Instant startTime) {
            data.setStartTime(startTime);
            return this;
        }

        public Modifier startTime(String startTimeUtc) {
            data.setStartTime(startTimeUtc);
            return this;
        }

        public Modifier endTime(Instant endTime) {
            data.setEndTime(endTime);
            return this;
        }

        public Modifier endTime(String endTimeUtc) {
            data.setEndTime(endTimeUtc);
            return this;
        }

        public Modifier atTime(Instant timeStamp) {
            return startTime(timeStamp).endTime(timeStamp);
        }

        public Modifier atTime(String timestampString) {
            return startTime(timestampString).endTime(timestampString);
        }

        public Modifier addFields(String... fields) {
            data.addFields(Arrays.asList(fields));
            return this;
        }

        public Modifier removeFields(String... fields) {
            data.removeFields(Arrays.asList(fields));
            return this;
        }

        public Modifier removeAllFields() {
            data.clearFields();
            return this;
        }

        public Modifier addAliases(Map<String, List<String>> aliases) {
            data.addAliases(aliases);
            return this;
        }

        public Modifier removeAliases(String... keys) {
            data.removeAliases(Arrays.asList(keys));
            return this;
        }

        public Modifier removeAllAliases() {
            data.clearAliases();
            return this;
        }

        Map<String, String> toMap() {
            return data.toMap();
        }

        public Dataset<Row> buildDataset() {
            return data.buildDataset();
        }

        public Modifier parameter(String parameter) {
            Pair<String, String> deviceProperty = extractDeviceProperty(parameter);
            return deviceProperty(deviceProperty.getLeft(), deviceProperty.getRight());
        }

        public Modifier deviceProperty(String device, String property) {
            data.addEntity(String.format("%s/%s", device, property), toKeyValue(device, property));
            return this;
        }
    }

    private static Map<String, String> toKeyValue(String device, String property) {
        return ImmutableMap.of(DEVICE_KEY, device, PROPERTY_KEY, property);
    }

    private static Pair<String, String> extractDeviceProperty(String parameter) {
        Matcher matcher = PARAMETER_PATTERN.matcher(requireNonNull(parameter));
        Preconditions.checkArgument(matcher.matches(),
                "Illegal parameter name, expected <device>/<property> got " + parameter);

        return Pair.of(matcher.group(1), matcher.group(2));
    }

    public interface DeviceStage {
        PropertyStage<DeviceStage> device(String device);

        BuildStage<DeviceStage> parameter(String parameter);
    }

    public interface PropertyStage<T> {
        BuildStage<T> property(String property);
    }

    private static class OngoingEntity {
        private final String name;
        private String device;
        private String property;

        OngoingEntity(String name) {
            this.name = name;
        }

        String getName() {
            return name;
        }

        String getDevice() {
            return device;
        }

        void setDevice(String device) {
            this.device = device;
        }

        String getProperty() {
            return property;
        }

        void setProperty(String property) {
            this.property = property;
        }
    }
}