/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.data.access.api;

import com.google.common.base.Preconditions;
import org.apache.avro.Schema;

import java.util.List;

class ServiceUtils {

    private ServiceUtils() {
    }

    /**
     * Obtains the name of the timestamp field from the system's time key definition schema
     *
     * @return The name of the field used to store the timestamp in the record
     */
    static String getTimestampField(String systemTimeKeyDefinition) {
        Schema timeDefinitionSchema = new Schema.Parser().parse(systemTimeKeyDefinition);
        List<Schema.Field> fields = timeDefinitionSchema.getFields();
        Preconditions
                .checkArgument(!fields.isEmpty(), "Cannot extract timestamp field. No fields in time key definition.");
        return fields.get(0).name();
    }
}
