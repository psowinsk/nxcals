/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.data.access.api;

import cern.nxcals.common.domain.EntityResources;
import cern.nxcals.common.domain.ResourceData;
import cern.nxcals.common.domain.SchemaData;
import cern.nxcals.common.domain.TimeWindow;
import cern.nxcals.common.utils.IllegalCharacterConverter;
import cern.nxcals.common.utils.TimeUtils;
import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.apache.avro.SchemaParseException;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static cern.nxcals.common.SystemFields.NXC_EXTR_VARIABLE_NAME;
import static java.util.stream.Collectors.toList;

/**
 * Implementation of {@link DataAccessService} that is responsible for acquiring the data from both sources (HDFS and
 * HBase) based on the requested time window (all the data for the last 2 DAYS is taken from HBase). It is also
 * responsible for taking the requested fields with proper data type analyzing and merging entity schemas where
 * possible.
 */
public class DataAccessServiceImpl implements DataAccessService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DataAccessServiceImpl.class);
    private final InternalDataAccessService<ResourcesBasedQuery<HbaseResource>> hbaseService;
    private final InternalDataAccessService<HdfsResourceBasedQuery> hdfsService;

    DataAccessServiceImpl(InternalDataAccessService<ResourcesBasedQuery<HbaseResource>> hbaseService,
            InternalDataAccessService<HdfsResourceBasedQuery> hdfsService) {
        this.hbaseService = Objects.requireNonNull(hbaseService);
        this.hdfsService = Objects.requireNonNull(hdfsService);
    }

    @Override
    public Dataset<Row> createDataSetFor(Set<InternalQueryData> queryData) {
        LOGGER.debug("Create dataset for queryData = {}", queryData);

        Collection<Dataset<Row>> resultSets = new ArrayList<>();

        if (!queryData.isEmpty()) {

            Instant startTime = startTime(queryData);
            Instant endTime = endTime(queryData);

            Set<SchemaData> schemas = queryData.stream()
                    .map(InternalQueryData::getEntityData)
                    .distinct()
                    .map(EntityResources::getSchemaData)
                    .collect(Collectors.toSet());

            Set<DataAccessField> requestedFields = queryData.stream()
                    .flatMap(t -> t.getFields().stream())
                    .collect(Collectors.toSet());

            DataSourceTimeWindow dsTimeWindows = DataSourceTimeWindow.getTimeWindows(startTime, endTime);

            LOGGER.debug("HBase time window = {}, HDFS time window = {}", dsTimeWindows.getHbaseTimeWindow(),
                    dsTimeWindows.getHdfsTimeWindow());

            List<DataAccessField> requiredFields = this.getRequiredSchemaFieldsFor(schemas, requestedFields);
            LOGGER.debug("Fields to be extracted = {}", requiredFields);

            dsTimeWindows.getHbaseTimeWindow().ifPresent(hbaseTimeWindow -> {
                Set<InternalQueryData> hbaseQueryData = getDataSourceQueryData(queryData, hbaseTimeWindow);

                ResourcesBasedQuery<HbaseResource> hbaseResourceBasedQuery = getResourceBasedQuery(hbaseQueryData,
                        requiredFields,
                        internalQueryData -> {
                            ResourceData resourcesData = internalQueryData.getEntityData().getResourcesData();
                            Set<HbaseResource> hbaseResources = new HashSet<>();
                            for (String tableName : resourcesData.getHbaseTableNames()) {
                                hbaseResources.add(new HbaseResource(tableName, resourcesData.getHbaseNamespace()));
                            }
                            return hbaseResources;
                        });

                resultSets.add(hbaseService.getDataFor(hbaseResourceBasedQuery));
            });

            dsTimeWindows.getHdfsTimeWindow().ifPresent(hdfsTimeWindow -> {
                Set<InternalQueryData> hdfsQueryData = getDataSourceQueryData(queryData, hdfsTimeWindow);

                ResourcesBasedQuery<URI> resourcesBasedQuery = getResourceBasedQuery(hdfsQueryData, requiredFields,
                        internalQueryData -> internalQueryData.getEntityData().getResourcesData().getHdfsPaths());

                String timestampField = ServiceUtils.getTimestampField(
                        queryData.iterator().next().getEntityData().getSystemData().getTimeKeyDefinitions());

                HdfsResourceBasedQuery hdfsResourceBasedQuery = new HdfsResourceBasedQuery(
                        resourcesBasedQuery.getResources(), resourcesBasedQuery.getFields(), timestampField);
                resultSets.add(hdfsService.getDataFor(hdfsResourceBasedQuery));
            });

        }
        return resultSets.stream().reduce(Dataset::union).orElseThrow(() -> new IllegalArgumentException(
                "No Spark datasets have been created providing the requested queryData=" + queryData));
    }

    private Instant endTime(Set<InternalQueryData> queryData) {
        return queryData.stream().map(InternalQueryData::getEndTime).max(Instant::compareTo).orElseThrow(
                () -> new IllegalArgumentException("EndTime is not set for the requested NXCALS DataAccess query"));
    }

    private Instant startTime(Set<InternalQueryData> queryData) {
        return queryData.stream().map(InternalQueryData::getStartTime).min(Instant::compareTo).orElseThrow(
                () -> new IllegalArgumentException("StartTime is not set for the requested NXCALS DataAccess query"));
    }

    private <T> ResourcesBasedQuery<T> getResourceBasedQuery(Set<InternalQueryData> queryData,
            List<DataAccessField> fields, Function<InternalQueryData, Set<T>> resourcesMapper) {
        Collection<List<InternalQueryData>> queriesPerResource = getQueriesPerResource(queryData);
        Set<ResourceQuery<T>> resourceQueries = new HashSet<>();

        for (List<InternalQueryData> value : queriesPerResource) {
            Instant startDate = Instant.MAX;
            Instant endDate = Instant.MIN;
            Set<T> resources = new HashSet<>();
            Set<Long> entityIds = new HashSet<>();
            List<String> columns = getColumnsFor(fields, value.get(0));

            for (InternalQueryData internalQueryData : value) {
                if (startDate.isAfter(internalQueryData.getStartTime())) {
                    startDate = internalQueryData.getStartTime();
                }
                if (endDate.isBefore(internalQueryData.getEndTime())) {
                    endDate = internalQueryData.getEndTime();
                }
                resources.addAll(resourcesMapper.apply(internalQueryData));
                entityIds.add(internalQueryData.getEntityData().getId());
            }

            resourceQueries.add(new ResourceQuery<>(resources, entityIds, columns,
                    TimeUtils.getNanosFromInstant(startDate),
                    TimeUtils.getNanosFromInstant(endDate)));
        }

        return new ResourcesBasedQuery<>(resourceQueries, fields);
    }

    private Collection<List<InternalQueryData>> getQueriesPerResource(Set<InternalQueryData> queryData) {
        Map<String, List<InternalQueryData>> queryByResources = new HashMap<>();
        for (InternalQueryData queryDatum : queryData) {
            String key = queryDatum.getEntityData().getPartitionData().getId() + "/" + queryDatum.getEntityData()
                    .getSchemaData().getId();
            queryByResources.compute(key, (k, v) -> {
                if (v == null) {
                    v = new ArrayList<>();
                }
                v.add(queryDatum);
                return v;
            });
        }
        return queryByResources.values();
    }

    private Set<InternalQueryData> getDataSourceQueryData(Set<InternalQueryData> queryData, TimeWindow timeWindow) {
        Instant twStartTime = timeWindow.getStartTime();
        Instant twEndTime = timeWindow.getEndTime();

        // Create a new Set of InternalQueryData that content only queries with intervals applicable for HDFS time window
        Set<InternalQueryData> dataSourceQueryData = queryData.stream()
                .map(query -> new InternalQueryData(query.getEntityData(),
                        query.getStartTime().compareTo(twStartTime) <= 0 ? twStartTime : query.getStartTime(),
                        query.getEndTime().compareTo(twEndTime) >= 0 ? twEndTime : query.getEndTime(),
                        query.getFields(),
                        query.getVariableName())).filter(q -> q.getStartTime().compareTo(q.getEndTime()) <= 0)
                .collect(Collectors.toSet());
        return dataSourceQueryData;
    }

    protected List<DataAccessField> getRequiredSchemaFieldsFor(Set<SchemaData> schemas, Set<DataAccessField> fields) {
        if (schemas.isEmpty()) {
            throw new IllegalArgumentException(
                    "There is no schema present for the requested entity! Please contact the Logging support !");

        }
        return getRequestedFieldsWithPromotedTypes(schemas, fields);
    }

    private List<DataAccessField> getRequestedFieldsWithPromotedTypes(Set<SchemaData> schemas,
            Set<DataAccessField> requestedFields) {

        LOGGER.debug("Check existence of the requested fields and type compatibility !");

        //Converts requested fields to leagal names
        Set<DataAccessField> legalRequestedFieldNames = convertToLeagalDataAccessFields(requestedFields);

        // Collects only requested fields to all the schemas and store list of schemas per field in a Map
        Map<String, Set<Schema>> fieldSchemas = getFieldSchemas(schemas, legalRequestedFieldNames);

        Set<DataAccessField> promotedFields = legalRequestedFieldNames.stream()
                .map(f -> TypeCompatibility.getPromotedFieldTypeWithAliases(f, fieldSchemas))
                .collect(Collectors.toSet());

        // Check for fields that were not present in any schema
        LOGGER.debug("Return verified set of requested fields");
        return this.checkFieldExistenceFor(legalRequestedFieldNames, sortedFieldsByAliases(promotedFields));
    }

    private Map<String, Set<Schema>> getFieldSchemas(Set<SchemaData> schemas,
            Set<DataAccessField> legalRequestedFieldNames) {
        Predicate<Schema.Field> isFieldRequestedLegal = field ->
                getLegalFieldsFor(field, legalRequestedFieldNames).count() > 0;

        return getFieldsFrom(schemas).filter(isFieldRequestedLegal).collect(Collectors
                .groupingBy(getLegalDataAccessFieldFrom(legalRequestedFieldNames),
                        Collectors.mapping(Schema.Field::schema, Collectors.toSet())));
    }

    private Function<Schema.Field, String> getLegalDataAccessFieldFrom(Set<DataAccessField> legalRequestedFieldNames) {
        return field -> getLegalFieldsFor(field, legalRequestedFieldNames)
                .map(f -> f.getAlias() == null ? f.getFieldName() : f.getAlias()).findFirst().orElseThrow(
                        () -> new IllegalArgumentException(
                                "Field " + field + " is not present in the legal fields list = "
                                        + legalRequestedFieldNames));
    }

    private Stream<DataAccessField> getLegalFieldsFor(Schema.Field field,
            Set<DataAccessField> legalRequestedFieldNames) {
        return legalRequestedFieldNames.stream().filter(f -> f.getFieldName().equals(field.name()));
    }

    private Stream<Schema.Field> getFieldsFrom(Set<SchemaData> schemas) {
        return schemas.stream().map(sd -> new Schema.Parser().parse(sd.getSchemaJson())).map(Schema::getFields)
                .flatMap(List::stream);
    }

    private Set<DataAccessField> convertToLeagalDataAccessFields(Set<DataAccessField> requestedFields) {
        return requestedFields.stream()
                .map(DataAccessServiceImpl::removeIllegalCharacters)
                .collect(Collectors.toSet());
    }

    private static DataAccessField removeIllegalCharacters(DataAccessField dataAccessField) {
        String fieldName = convertToLegalIfNecessary(dataAccessField.getFieldName());

        return new DataAccessFieldBuilder(fieldName)
                .dataType(dataAccessField.getDataType())
                .fieldSchema(dataAccessField.getFieldSchema())
                .alias(dataAccessField.getAlias()).build();
    }

    private List<DataAccessField> sortedFieldsByAliases(Set<DataAccessField> promotedFields) {
        return promotedFields.stream()
                .sorted(Comparator.comparing(f -> f.getAlias() == null ? f.getFieldName() : f.getAlias()))
                .collect(Collectors.toList());
    }

    private List<DataAccessField> checkFieldExistenceFor(Set<DataAccessField> requestedFields,
            List<DataAccessField> promotedFields) {
        Set<DataAccessField> missingFields = requestedFields.stream().filter(f -> !promotedFields.contains(f))
                .collect(Collectors.toSet());
        if (!missingFields.isEmpty()) {
            throw new IllegalArgumentException("Field(s) " + missingFields.toString()
                    + " is(are) not present in any schema for the requested entity and time window!");
        }
        return promotedFields;
    }

    private List<String> getColumnsFor(List<DataAccessField> fields, InternalQueryData query) {
        return fields.stream().map(field -> getColumn(field, query))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(Column::toString)
                .distinct()
                .collect(toList());
    }

    private Optional<Column> getColumn(DataAccessField field, InternalQueryData queryData) {
        // creates a VARIABLE_NAME column listing the name of the variable
        if (field.getFieldName().equals(NXC_EXTR_VARIABLE_NAME.getValue())) {
            return createSparkColumn(field, convertToStringDataValue(queryData.getVariableName()));
        }

        List<String> schemaFields = queryData.getFields().stream().map(DataAccessField::getFieldName).collect(toList());

        // creates a standart column if exists for the given schema
        if (schemaFields.contains(field.getFieldName())) {
            return createSparkColumn(field, null);
        }

        // creates column with NULL value if the field is not present in the given schema
        // checks whether such an alias exists for the given schema and creates an empty column ONLY if it doesn't exist
        if (queryData.getFields().stream().filter(ff -> ff.getAlias() != null && ff.getAlias().equals(field.getAlias()))
                .count() == 0) {
            return createSparkColumn(field, convertToStringDataValue(null));
        }

        // if the field is not present in the queryData but the alias exists we need to skip the column
        return Optional.empty();
    }

    private Optional<Column> createSparkColumn(DataAccessField field, String value) {
        IllegalCharacterConverter illegalCharacterConverter = IllegalCharacterConverter.get();
        String fieldAlias = convertFromLegalIfNecessary(field.getAlias());
        return Optional.of(new Column(value == null ? field.getFieldName() : value).cast(field.getDataType())
                .alias(fieldAlias == null ? convertFromLegalIfNecessary(field.getFieldName()) : fieldAlias));
    }

    private String convertToStringDataValue(String value) {
        return value == null ? "null" : "\"" + value + "\"";
    }

    private static String convertToLegalIfNecessary(String name) {
        if (name == null) {
            return null;
        }

        String toReturn = name;
        try {
            SchemaBuilder.record("test").fields().name(name).type().stringType().noDefault().endRecord();
        } catch (SchemaParseException exception) {
            toReturn = IllegalCharacterConverter.get().convertToLegal(name);
        }
        return toReturn;
    }

    private static String convertFromLegalIfNecessary(String name) {
        IllegalCharacterConverter illegalCharacterConverter = IllegalCharacterConverter.get();
        if (illegalCharacterConverter.isEncoded(name)) {
            return illegalCharacterConverter.convertFromLegal(name);
        }

        return name;
    }
}