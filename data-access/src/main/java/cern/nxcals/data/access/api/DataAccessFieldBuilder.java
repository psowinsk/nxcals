/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.data.access.api;

import org.apache.avro.Schema;
import org.apache.spark.sql.types.DataType;

/**
 * Builder for creating {@link DataAccessField}
 */
class DataAccessFieldBuilder {
    private final String fieldName;
    private DataType dataType;
    private Schema fieldSchema;
    private String alias;

    DataAccessFieldBuilder(String fieldName) {
        this.fieldName = fieldName;
    }

    DataAccessFieldBuilder alias(String alias) {
        this.alias = alias;
        return this;
    }

    DataAccessFieldBuilder dataType(DataType dataType) {
        this.dataType = dataType;
        return this;
    }

    DataAccessFieldBuilder fieldSchema(Schema fieldSchema) {
        this.fieldSchema = fieldSchema;
        return this;
    }

    DataAccessField build() {
        return new DataAccessField(this.fieldName, this.dataType, this.fieldSchema, this.alias);
    }
}
