/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.data.access;

import cern.nxcals.common.domain.EntityHistoryData;
import cern.nxcals.common.domain.EntityResources;
import cern.nxcals.common.domain.PartitionData;
import cern.nxcals.common.domain.ResourceData;
import cern.nxcals.common.domain.SchemaData;
import cern.nxcals.common.domain.SystemData;
import cern.nxcals.common.domain.VariableConfigData;
import cern.nxcals.common.domain.VariableData;
import cern.nxcals.common.utils.TimeUtils;
import com.google.common.collect.Sets;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Ignore;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import static cern.nxcals.common.utils.KeyValuesUtils.convertKeyValuesStringIntoMap;

@Ignore
public abstract class BaseTest {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static final long VERSION = 0l;

    protected EntityResources getTestEntityData(SystemData systemData) {
        //schema1 is missing "string_field" that exists in schema2
        String schema1 = "{\"type\":\"record\",\"name\":\"data0\",\"namespace\":\"cern.nxcals\",\"fields\":"
                + "[{\"name\":\"__sys_nxcals_system_id__\",\"type\":\"long\"},"
                + "{\"name\":\"__sys_nxcals_entity_id__\",\"type\":\"long\"},"
                + "{\"name\":\"__sys_nxcals_partition_id__\",\"type\":\"long\"},"
                + "{\"name\":\"__sys_nxcals_schema_id__\",\"type\":\"long\"},"
                + "{\"name\":\"__sys_nxcals_timestamp__\",\"type\":\"long\"},"
                + "{\"name\":\"__record_timestamp__\",\"type\":\"long\"},"
                + "{\"name\":\"__record_version__\",\"type\":\"long\"},"
                + "{\"name\":\"acqStamp\",\"type\":[\"long\",\"null\"]}," + "{\"name\":\"class\",\"type\":\"string\"},"
                + "{\"name\":\"int_field\",\"type\":[\"int\",\"null\"]},"
                + "{\"name\":\"long_field\",\"type\":[\"long\",\"null\"]},"
                + "{\"name\":\"float_field\",\"type\":[\"float\",\"null\"]},"
                + "{\"name\":\"double_field\",\"type\":[\"double\",\"null\"]},"
                + "{\"name\":\"array_field\",\"type\":[{\"type\":\"array\",\"items\":[\"float\",\"null\"]},\"null\"]},"
                + "{\"name\":\"array2D_field\",\"type\":[{\"type\":\"record\",\"name\":\"float_array_2d\","
                + "\"fields\":[{\"name\":\"elements\",\"type\":[{\"type\":\"array\",\"items\":[\"float\",\"null\"]},\"null\"]},{\"name\":\"rowCount\",\"type\":[\"int\",\"null\"]},{\"name\":\"columnCount\",\"type\":[\"int\",\"null\"]}]},\"null\"]}"
                + "]}";

        String schema2 = "{\"type\":\"record\",\"name\":\"data0\",\"namespace\":\"cern.nxcals\",\"fields\":"
                + "[{\"name\":\"__sys_nxcals_system_id__\",\"type\":\"long\"},"
                + "{\"name\":\"__sys_nxcals_entity_id__\",\"type\":\"long\"},"
                + "{\"name\":\"__sys_nxcals_partition_id__\",\"type\":\"long\"},"
                + "{\"name\":\"__sys_nxcals_schema_id__\",\"type\":\"long\"},"
                + "{\"name\":\"__sys_nxcals_timestamp__\",\"type\":\"long\"},"
                + "{\"name\":\"__record_timestamp__\",\"type\":\"long\"},"
                + "{\"name\":\"__record_version__\",\"type\":\"long\"},"
                + "{\"name\":\"acqStamp\",\"type\":[\"long\",\"null\"]}," + "{\"name\":\"class\",\"type\":\"string\"},"
                + "{\"name\":\"int_field\",\"type\":[\"int\"]},"
                + "{\"name\":\"long_field\",\"type\":[\"long\",\"null\"]},"
                + "{\"name\":\"float_field\",\"type\":[\"float\",\"null\"]},"
                + "{\"name\":\"double_field\",\"type\":[\"double\",\"null\"]},"
                + "{\"name\":\"string_field\",\"type\":[\"string\",\"null\"]},"
                + "{\"name\":\"array_field\",\"type\":[{\"type\":\"array\",\"items\":[\"float\",\"null\"]},\"null\"]},"
                + "{\"name\":\"array2D_field\",\"type\":[{\"type\":\"record\",\"name\":\"float_array_2d\","
                + "\"fields\":[{\"name\":\"elements\",\"type\":[{\"type\":\"array\",\"items\":[\"float\",\"null\"]},\"null\"]},{\"name\":\"rowCount\",\"type\":[\"int\",\"null\"]},{\"name\":\"columnCount\",\"type\":[\"int\",\"null\"]}]},\"null\"]}"
                + "]}";

        PartitionData partitionData = PartitionData.builder()
                .id(2L)
                .keyValues(convertKeyValuesStringIntoMap("{\"class\": \"ClassName\", \"property\": \"Acquisition\"}"))
                .build();
        SchemaData schemaData1 = SchemaData.builder().id(3L).schema(schema1).build();
        SchemaData schemaData2 = SchemaData.builder().id(4L).schema(schema2).build();
        EntityHistoryData ehData1 = EntityHistoryData.builder().id(1L).schemaData(schemaData1)
                .partitionData(partitionData)
                .validFromStamp(TimeUtils.getNanosFromInstant(Instant.now().minus(10, ChronoUnit.DAYS)))
                .validToStamp(TimeUtils.getNanosFromInstant(Instant.now().minus(3, ChronoUnit.DAYS)))
                .build();

        EntityHistoryData ehData2 = EntityHistoryData.builder().id(2L)
                .schemaData(schemaData2).partitionData(partitionData)
                .validFromStamp(TimeUtils.getNanosFromInstant(Instant.now().minus(3, ChronoUnit.DAYS)))
                .build();
        SortedSet<EntityHistoryData> entityHistoryData = new TreeSet<>();
        entityHistoryData.add(ehData1);
        entityHistoryData.add(ehData2);

        try {
            return EntityResources.builder().id(1L)
                    .entityKeyValues("{\"device\": \"Test_Device\", \"property\": \"Acquisition\"}")
                    .systemData(systemData).partitionData(partitionData).schemaData(schemaData2)
                    .resourceData(getTestResourceData()).build();
        } catch (URISyntaxException exception) {
            throw new RuntimeException("Wrong URI for default test entity", exception);
        }
    }

    protected Set<EntityResources> getTestEntitiesResources(SystemData systemData) {
        return Sets.newHashSet(getTestEntityData(systemData));
    }

    protected SystemData getTestDevPropSystemData() {
        long systemId = 1L;
        String systemName = "TEST_SYSTEM";
        String devicePropertySchema =
                "{\"type\":\"record\",\"name\":\"CmwKey\",\"namespace\":\"cern.nxcals\",\"fields\":"
                        + "[{\"name\":\"device\",\"type\":\"string\"},{\"name\":\"property\",\"type\":\"string\"}]}";
        String partitionKeyDefs = "{\"type\":\"record\",\"name\":\"CmwPartition\",\"namespace\":\"cern.nxcals\""
                + ",\"fields\":[{\"name\":\"class\",\"type\":\"string\"},{\"name\":\"property\",\"type\":\"string\"}]}";
        String timeKeyDefs = "{\"type\":\"record\",\"name\":\"CmwPT\",\"namespace\":\"cern.nxcals\",\"fields\":"
                + "[{\"name\":\"__record_timestamp__\",\"type\":\"long\"}]}";
        String recordVersionKeyDefs = "{\"type\":\"record\",\"name\":\"CmwRecordVersion\",\"namespace\""
                + ":\"cern.nxcals\",\"fields\":[{\"name\":\"__record_version__\",\"type\":\"long\"}]}";

        return SystemData.builder().id(systemId).name(systemName).entityKeyDefinitions(devicePropertySchema)
                .partitionKeyDefinitions(partitionKeyDefs)
                .timeKeyDefinitions(timeKeyDefs).recordVersionKeyDefinitions(recordVersionKeyDefs).build();
    }

    protected SystemData getTestElementSystemData() {
        long systemId = 2L;
        String systemName = "TEST_SYSTEM";
        String elementEntityKeySchema = "{\"type\":\"record\",\"name\":\"Test\",\"namespace\":\"cern.nxcals\","
                + "\"fields\":[{\"name\":\"element\",\"type\":\"string\"}]}";
        String partitionKeyDefs = "{\"type\":\"record\",\"name\":\"CmwPartition\",\"namespace\":\"cern.nxcals\""
                + ",\"fields\":[{\"name\":\"class\",\"type\":\"string\"},{\"name\":\"property\",\"type\":\"string\"}]}";
        String timeKeyDefs = "{\"type\":\"record\",\"name\":\"CmwPT\",\"namespace\":\"cern.nxcals\",\"fields\":"
                + "[{\"name\":\"__record_timestamp__\",\"type\":\"long\"}]}";
        String recordVersionKeyDefs = "{\"type\":\"record\",\"name\":\"CmwRecordVersion\",\"namespace\""
                + ":\"cern.nxcals\",\"fields\":[{\"name\":\"__record_version__\",\"type\":\"long\"}]}";

        return SystemData.builder().id(systemId).name(systemName).entityKeyDefinitions(elementEntityKeySchema)
                .partitionKeyDefinitions(partitionKeyDefs).timeKeyDefinitions(timeKeyDefs)
                .recordVersionKeyDefinitions(recordVersionKeyDefs).build();
    }

    protected VariableData getTestVariableDataFullEntity() {
        SortedSet<VariableConfigData> varConfData = new TreeSet<>();
        varConfData
                .add(VariableConfigData.builder().entityId(getTestEntityData(getTestDevPropSystemData()).getId())
                        .build());

        return VariableData.builder().name(toString()).description("Description")
                .creationTimeUtc(TimeUtils.getNanosFromInstant(Instant.now()))
                .variableConfigData(varConfData).build();
    }

    protected VariableData getTestVariableDataWithField() {
        SortedSet<VariableConfigData> varConfData = new TreeSet<>();
        varConfData
                .add(VariableConfigData.builder().entityId(getTestEntityData(getTestDevPropSystemData()).getId())
                        .fieldName("field1").build());

        return VariableData.builder().name(toString()).description("Description")
                .creationTimeUtc(TimeUtils.getNanosFromInstant(Instant.now()))
                .variableConfigData(varConfData).build();
    }

    protected ResourceData getTestResourceData() throws URISyntaxException {
        Set<URI> hdfsPaths = new HashSet<>();
        Set<String> hbaseTables = new HashSet<>();

        hdfsPaths.add(new URI("/nxcals/data/1/2/4/2016-11-11/*.parquet"));
        hdfsPaths.add(new URI("/nxcals/data/1/2/4/2016-11-12/*.parquet"));
        hdfsPaths.add(new URI("/nxcals/data/1/2/4/2016-11-13/*.parquet"));

        hbaseTables.add("1__2__4");

        return ResourceData.builder()
                .hdfsPaths(hdfsPaths)
                .hbaseNamespace("default")
                .hbaseTableNames(hbaseTables)
                .build();
    }

    protected String getKeyValuesValueFor(Object keyValues) throws IOException {
        return OBJECT_MAPPER.writeValueAsString(keyValues);
    }
}
