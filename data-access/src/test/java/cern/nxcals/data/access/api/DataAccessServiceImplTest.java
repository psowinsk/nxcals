/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.data.access.api;

import cern.nxcals.common.domain.EntityResources;
import cern.nxcals.common.domain.SchemaData;
import cern.nxcals.data.access.BaseTest;
import org.apache.avro.Schema;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.DataTypes;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DataAccessServiceImplTest extends BaseTest {

    @Mock
    private InternalDataAccessService hdfsDataAccessServiceMock;
    @Mock
    private InternalDataAccessService hbaseDataAccessServiceMock;
    @Mock
    private Dataset<Row> datasetMock;
    @Mock
    private InternalQueryData queryDataMock;
    @InjectMocks
    private DataAccessServiceImpl dataAccessService;

    private Set<InternalQueryData> setQueryDataMock;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        //given
        EntityResources testEntityData = getTestEntityData(getTestDevPropSystemData());
        this.dataAccessService = new DataAccessServiceImpl(hbaseDataAccessServiceMock, hdfsDataAccessServiceMock);

        Set<DataAccessField> fields = new HashSet<>();
        fields.add(new DataAccessFieldBuilder("__record_timestamp__").alias(NXC_EXTR_TIMESTAMP.getValue()).build());

        //when
        when(queryDataMock.getEntityData()).thenReturn(testEntityData);
        when(queryDataMock.getFields()).thenReturn(fields);
        when(queryDataMock.getVariableName()).thenReturn(null);

        setQueryDataMock = new HashSet<>();
        setQueryDataMock.add(queryDataMock);

        when(hdfsDataAccessServiceMock.getDataFor(any())).thenReturn(datasetMock);
        when(hbaseDataAccessServiceMock.getDataFor(any())).thenReturn(datasetMock);
        when(datasetMock.union(eq(datasetMock))).thenReturn(datasetMock);
    }

    @Test
    public void shouldCreateDatasetFromBothSourcesWithMissingEntityHist() {
        //when
        when(queryDataMock.getEntityData()).thenReturn(getTestEntityData(getTestDevPropSystemData()));
        when(queryDataMock.getStartTime()).thenReturn(Instant.now().minus(2, ChronoUnit.DAYS));
        when(queryDataMock.getEndTime()).thenReturn(Instant.now());
        Dataset<Row> resultSet = this.dataAccessService.createDataSetFor(setQueryDataMock);

        //then
        Assert.assertNotNull(resultSet);
        Assert.assertEquals(datasetMock, resultSet);
        verify(datasetMock, times(1)).union(datasetMock);
    }

    @Test
    public void shouldCreateDatasetFromBothSources() {
        //when
        when(queryDataMock.getStartTime()).thenReturn(Instant.now().minus(2, ChronoUnit.DAYS));
        when(queryDataMock.getEndTime()).thenReturn(Instant.now());
        Dataset<Row> resultSet = dataAccessService.createDataSetFor(setQueryDataMock);

        //then
        Assert.assertNotNull(resultSet);
        Assert.assertEquals(datasetMock, resultSet);
        verify(datasetMock, times(1)).union(datasetMock);
    }

    @Test
    public void shouldCreateHdfsDatasetWithoutSpecifiedFields() {
        //when
        when(queryDataMock.getFields()).thenReturn(Collections.emptySet());
        when(queryDataMock.getStartTime()).thenReturn(Instant.now().minus(4, ChronoUnit.DAYS));
        when(queryDataMock.getEndTime()).thenReturn(Instant.now().minus(2, ChronoUnit.DAYS));
        Dataset<Row> resultSet = this.dataAccessService.createDataSetFor(setQueryDataMock);

        //then
        Assert.assertNotNull(resultSet);
        Assert.assertEquals(datasetMock, resultSet);
        verify(datasetMock, times(0)).union(datasetMock);
    }

    @Test
    public void shouldCreateHdfsDatasetForVariable() {
        //when
        when(queryDataMock.getVariableName()).thenReturn("Varaible");
        when(queryDataMock.getStartTime()).thenReturn(Instant.now().minus(4, ChronoUnit.DAYS));
        when(queryDataMock.getEndTime()).thenReturn(Instant.now().minus(2, ChronoUnit.DAYS));
        Dataset<Row> resultSet = this.dataAccessService.createDataSetFor(setQueryDataMock);

        //then
        Assert.assertNotNull(resultSet);
        Assert.assertEquals(datasetMock, resultSet);
        verify(datasetMock, times(0)).union(datasetMock);
    }

    @Test
    public void shouldCreateHbaseDataset() {
        //when
        when(queryDataMock.getStartTime()).thenReturn(Instant.now().minus(1, ChronoUnit.DAYS));
        when(queryDataMock.getEndTime()).thenReturn(Instant.now());
        Dataset<Row> resultSet = this.dataAccessService.createDataSetFor(setQueryDataMock);

        //then
        Assert.assertNotNull(resultSet);
        Assert.assertEquals(datasetMock, resultSet);
        verify(datasetMock, times(0)).union(datasetMock);
    }

    @Test
    public void shouldCreateHbaseDatasetWithoutSpecifiedFields() {
        //when
        when(queryDataMock.getFields()).thenReturn(Collections.emptySet());
        when(queryDataMock.getStartTime()).thenReturn(Instant.now().minus(1, ChronoUnit.DAYS));
        when(queryDataMock.getEndTime()).thenReturn(Instant.now());
        Dataset<Row> resultSet = this.dataAccessService.createDataSetFor(setQueryDataMock);

        //then
        Assert.assertNotNull(resultSet);
        Assert.assertEquals(datasetMock, resultSet);
        verify(datasetMock, times(0)).union(datasetMock);
    }

    @Test
    public void shouldCreateHbaseDatasetForVariable() {
        //when
        when(queryDataMock.getVariableName()).thenReturn("Varaible");
        when(queryDataMock.getStartTime()).thenReturn(Instant.now().minus(1, ChronoUnit.DAYS));
        when(queryDataMock.getEndTime()).thenReturn(Instant.now());
        Dataset<Row> resultSet = this.dataAccessService.createDataSetFor(setQueryDataMock);

        //then
        Assert.assertNotNull(resultSet);
        Assert.assertEquals(datasetMock, resultSet);
        verify(datasetMock, times(0)).union(datasetMock);
    }

    @Test
    public void shouldCreateDatasetForGivenFieldsFromBothSources() {
        //given
        Set<DataAccessField> fields = new HashSet<>();
        fields.add(new DataAccessFieldBuilder("__record_timestamp__").alias(NXC_EXTR_TIMESTAMP.getValue()).build());
        fields.add(new DataAccessFieldBuilder("string_field").build());
        fields.add(new DataAccessFieldBuilder("array_field").build());
        fields.add(new DataAccessFieldBuilder("array2D_field").build());

        //when
        when(queryDataMock.getFields()).thenReturn(fields);
        when(queryDataMock.getStartTime()).thenReturn(Instant.now().minus(2, ChronoUnit.DAYS));
        when(queryDataMock.getEndTime()).thenReturn(Instant.now());
        Dataset<Row> resultSet = this.dataAccessService.createDataSetFor(setQueryDataMock);

        //then
        Assert.assertNotNull(resultSet);
        Assert.assertEquals(datasetMock, resultSet);
        verify(datasetMock, times(1)).union(datasetMock);
    }

    @Test
    public void shouldCreateDatasetForGivenFieldsFromHDFS() {
        //given
        Set<DataAccessField> fields = new HashSet<>();
        fields.add(new DataAccessFieldBuilder("__record_timestamp__").alias(NXC_EXTR_TIMESTAMP.getValue()).build());
        fields.add(new DataAccessFieldBuilder("string_field").build());
        fields.add(new DataAccessFieldBuilder("array_field").build());
        fields.add(new DataAccessFieldBuilder("array2D_field").build());

        //when
        when(queryDataMock.getFields()).thenReturn(fields);
        when(queryDataMock.getStartTime()).thenReturn(Instant.now().minus(4, ChronoUnit.DAYS));
        when(queryDataMock.getEndTime()).thenReturn(Instant.now().minus(2, ChronoUnit.DAYS));
        Dataset<Row> resultSet = this.dataAccessService.createDataSetFor(setQueryDataMock);

        //then
        Assert.assertNotNull(resultSet);
        Assert.assertEquals(datasetMock, resultSet);
        verify(datasetMock, times(0)).union(datasetMock);
    }

    @Test
    public void shouldCreateDatasetForGivenFieldsFromHBase() {
        //given
        Set<DataAccessField> fields = new HashSet<>();
        fields.add(new DataAccessFieldBuilder("__record_timestamp__").alias(NXC_EXTR_TIMESTAMP.getValue()).build());
        fields.add(new DataAccessFieldBuilder("string_field").build());
        fields.add(new DataAccessFieldBuilder("array_field").build());
        fields.add(new DataAccessFieldBuilder("array2D_field").build());

        //when
        when(queryDataMock.getFields()).thenReturn(fields);
        when(queryDataMock.getStartTime()).thenReturn(Instant.now().minus(1, ChronoUnit.DAYS));
        when(queryDataMock.getEndTime()).thenReturn(Instant.now());
        Dataset<Row> resultSet = this.dataAccessService.createDataSetFor(setQueryDataMock);

        //then
        Assert.assertNotNull(resultSet);
        Assert.assertEquals(datasetMock, resultSet);
        verify(datasetMock, times(0)).union(datasetMock);
    }

    @Test
    public void shouldGetRequiredSchemaFields() {
        //given
        EntityResources entityData = this.getTestEntityData(getTestDevPropSystemData());
        String timestampField = "__record_timestamp__";
        Instant startTime = Instant.now().minus(1, ChronoUnit.DAYS);
        Instant endTime = Instant.now();
        Set<DataAccessField> requestedFields = new HashSet<>();
        requestedFields.add(new DataAccessFieldBuilder("int_field").build());
        requestedFields.add(new DataAccessFieldBuilder("long_field").build());
        requestedFields.add(new DataAccessFieldBuilder("float_field").build());
        requestedFields.add(new DataAccessFieldBuilder("double_field").build());
        requestedFields.add(new DataAccessFieldBuilder("string_field").build());
        requestedFields.add(new DataAccessFieldBuilder("array_field").build());
        requestedFields.add(new DataAccessFieldBuilder("array2D_field").build());
        requestedFields.add(new DataAccessFieldBuilder(timestampField).alias(NXC_EXTR_TIMESTAMP.getValue()).build());

        Schema array_schema = new Schema.Parser()
                .parse("[{\"type\":\"array\",\"items\":[\"float\",\"null\"]},\"null\"]");

        Schema array2D_schema = new Schema.Parser()
                .parse("[{\"type\":\"record\",\"name\":\"float_array_2d\",\"namespace\":\"cern.nxcals\","
                        + "\"fields\":[{\"name\":\"elements\",\"type\":[{\"type\":\"array\",\"items\":[\"float\",\"null\"]},\"null\"]},"
                        + "{\"name\":\"rowCount\",\"type\":[\"int\",\"null\"]},{\"name\":\"columnCount\",\"type\":[\"int\",\"null\"]}]},\"null\"]");

        Set<SchemaData> schemas = new HashSet<>();
        schemas.add(entityData.getSchemaData());

        //when
        List<DataAccessField> requiredSchemaFields = this.dataAccessService
                .getRequiredSchemaFieldsFor(schemas, requestedFields);

        //then
        Assert.assertNotNull(requiredSchemaFields);
        Assert.assertFalse(requiredSchemaFields.isEmpty());
        Assert.assertEquals(8, requiredSchemaFields.size());

        //assert primitives
        Assert.assertEquals(1, requiredSchemaFields.stream().filter(f -> f.getFieldName().equals("int_field")).count());
        Assert.assertEquals(DataTypes.IntegerType,
                requiredSchemaFields.stream().filter(f -> f.getFieldName().equals("int_field"))
                        .map(DataAccessField::getDataType).findFirst().get());
        Assert.assertEquals(1,
                requiredSchemaFields.stream().filter(f -> f.getFieldName().equals("long_field")).count());
        Assert.assertEquals(DataTypes.LongType,
                requiredSchemaFields.stream().filter(f -> f.getFieldName().equals("long_field"))
                        .map(DataAccessField::getDataType).findFirst().get());
        Assert.assertEquals(1,
                requiredSchemaFields.stream().filter(f -> f.getFieldName().equals("float_field")).count());
        Assert.assertEquals(DataTypes.FloatType,
                requiredSchemaFields.stream().filter(f -> f.getFieldName().equals("float_field"))
                        .map(DataAccessField::getDataType).findFirst().get());
        Assert.assertEquals(1,
                requiredSchemaFields.stream().filter(f -> f.getFieldName().equals("double_field")).count());
        Assert.assertEquals(DataTypes.DoubleType,
                requiredSchemaFields.stream().filter(f -> f.getFieldName().equals("double_field"))
                        .map(DataAccessField::getDataType).findFirst().get());
        Assert.assertEquals(1,
                requiredSchemaFields.stream().filter(f -> f.getFieldName().equals("string_field")).count());
        Assert.assertEquals(DataTypes.StringType,
                requiredSchemaFields.stream().filter(f -> f.getFieldName().equals("string_field"))
                        .map(DataAccessField::getDataType).findFirst().get());

        //assert array
        Assert.assertEquals(1,
                requiredSchemaFields.stream().filter(f -> f.getFieldName().equals("array_field")).count());
        Assert.assertEquals(DataTypes.createArrayType(DataTypes.FloatType),
                requiredSchemaFields.stream().filter(f -> f.getFieldName().equals("array_field"))
                        .map(DataAccessField::getDataType).findFirst().get());
        Assert.assertEquals(1, requiredSchemaFields.stream()
                .filter(f -> f.getFieldName().equals("array_field") && f.getFieldSchema() != null).count());
        Assert.assertEquals(array_schema,
                requiredSchemaFields.stream().filter(f -> f.getFieldName().equals("array_field"))
                        .map(DataAccessField::getFieldSchema).findFirst().get());

        //assert array2D
        Assert.assertEquals(1,
                requiredSchemaFields.stream().filter(f -> f.getFieldName().equals("array2D_field")).count());
        Assert.assertEquals(SparkTypeUtils.getDataTypeFor(array2D_schema),
                requiredSchemaFields.stream().filter(f -> f.getFieldName().equals("array2D_field"))
                        .map(DataAccessField::getDataType).findFirst().get());
        Assert.assertEquals(1, requiredSchemaFields.stream()
                .filter(f -> f.getFieldName().equals("array2D_field") && f.getFieldSchema() != null).count());
        Assert.assertEquals(array2D_schema,
                requiredSchemaFields.stream().filter(f -> f.getFieldName().equals("array2D_field"))
                        .map(DataAccessField::getFieldSchema).findFirst().get());

        //assert meta fields
        Assert.assertEquals(1,
                requiredSchemaFields.stream().filter(f -> f.getFieldName().equals(timestampField)).count());
        Assert.assertEquals(1,
                requiredSchemaFields.stream().filter(f -> NXC_EXTR_TIMESTAMP.getValue().equals(f.getAlias())).count());
    }

    @Test
    public void shouldGetCurrentSchemaFromEntityWithoutHistory() {
        //given
        Set<DataAccessField> fields = new HashSet<>();
        fields.add(new DataAccessFieldBuilder("__record_timestamp__").alias(NXC_EXTR_TIMESTAMP.getValue()).build());
        fields.add(new DataAccessFieldBuilder("string_field").build());
        fields.add(new DataAccessFieldBuilder("array_field").build());
        fields.add(new DataAccessFieldBuilder("array2D_field").build());

        //when
        when(queryDataMock.getEntityData()).thenReturn(getTestEntityData(getTestDevPropSystemData()));
        when(queryDataMock.getStartTime()).thenReturn(Instant.now().minus(15, ChronoUnit.DAYS));
        when(queryDataMock.getEndTime()).thenReturn(Instant.now().minus(13, ChronoUnit.DAYS));
        when(queryDataMock.getFields()).thenReturn(fields);
        Dataset<Row> resultSet = this.dataAccessService.createDataSetFor(setQueryDataMock);

        //then
        Assert.assertNotNull(resultSet);
        Assert.assertEquals(datasetMock, resultSet);
        verify(datasetMock, times(0)).union(datasetMock);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailRequestingNonExistingField() {
        //given
        Set<DataAccessField> fields = new HashSet<>();
        fields.add(new DataAccessFieldBuilder("non-existing_field").build());

        //when
        when(queryDataMock.getEntityData()).thenReturn(getTestEntityData(getTestDevPropSystemData()));
        when(queryDataMock.getStartTime()).thenReturn(Instant.now().minus(2, ChronoUnit.DAYS));
        when(queryDataMock.getEndTime()).thenReturn(Instant.now());
        when(queryDataMock.getFields()).thenReturn(fields);

        Dataset<Row> resultSet = this.dataAccessService.createDataSetFor(setQueryDataMock);
    }

}
