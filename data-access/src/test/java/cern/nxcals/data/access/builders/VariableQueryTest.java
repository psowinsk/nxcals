/*
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN),All Rights Reserved.
 */

package cern.nxcals.data.access.builders;

import cern.nxcals.common.utils.TimeUtils;
import org.apache.spark.sql.SparkSession;
import org.junit.Assert;
import org.junit.Test;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Map;

import static cern.nxcals.data.access.Constants.VARIABLE_KEY;
import static cern.nxcals.data.access.builders.QueryAssertions.assertEndTime;
import static cern.nxcals.data.access.builders.QueryAssertions.assertStartTime;
import static cern.nxcals.data.access.Constants.END_TIME_KEY;
import static cern.nxcals.data.access.Constants.START_TIME_KEY;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

/**
 * Test suite for {@link VariableQuery}.
 */
public class VariableQueryTest {

    private static final String VARIABLE_NAME = "VARIABLE_NAME";
    private static final Instant TIME_1 = Instant.now();
    private static final Instant TIME_2 = TIME_1.plus(1, ChronoUnit.DAYS);

    private Map<String, String> query = toMap(VariableQuery.builder(mock(SparkSession.class))
            .variable(VARIABLE_NAME)
            .startTime(TIME_1)
            .endTime(TIME_2));

    private static Map<String, String> toMap(VariableQuery.VariableBuildQueryStage builder) {
        return ((VariableQuery.InternalBuilder) builder).toMap();
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowWhenVariableIsNull() {
        builder(null);
    }

    @Test
    public void shouldCreateVariableQuery() {
        //when
        Map<String, String> query = toMap(builder(VARIABLE_NAME).startTime(TIME_1).endTime(TIME_2));

        //then
        Assert.assertNotNull(query);
        Assert.assertEquals(6, query.size());
        Assert.assertEquals(VARIABLE_NAME, query.get(VARIABLE_KEY));
        assertStartTime(TIME_1, query);
        assertEndTime(TIME_2, query);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowWhenModifyWithNull() {
        modifier(null);
    }

    @Test
    public void shouldModifyVariableName() {
        //given
        String newVariableName = "NEW_VARIABLE";

        //when
        Map<String, String> resultQuery = modifier(query).variable(newVariableName).toMap();

        //then
        Assert.assertNotNull(resultQuery);
        assertEquals(VARIABLE_NAME, query.get(VARIABLE_KEY));
        assertEquals(newVariableName, resultQuery.get(VARIABLE_KEY));
        assertEquals(query.get(START_TIME_KEY), resultQuery.get(START_TIME_KEY));
        assertEquals(query.get(END_TIME_KEY), resultQuery.get(END_TIME_KEY));
    }

    @Test
    public void shouldModifyStartTime() {
        //given
        Instant newStartTime = TIME_1.minus(1, ChronoUnit.DAYS);

        //when
        Map<String, String> resultQuery = modifier(query).startTime(newStartTime).toMap();

        //then
        Assert.assertNotNull(resultQuery);
        assertEquals(query.get(VARIABLE_KEY), resultQuery.get(VARIABLE_KEY));
        assertEquals(String.valueOf(TimeUtils.getNanosFromInstant(TIME_1)), query.get(START_TIME_KEY));
        assertEquals(String.valueOf(TimeUtils.getNanosFromInstant(newStartTime)), resultQuery.get(START_TIME_KEY));
        assertEquals(query.get(END_TIME_KEY), resultQuery.get(END_TIME_KEY));
    }

    @Test
    public void shouldModifyEndTime() {
        //given
        Instant newEndTime = TIME_2.minus(1, ChronoUnit.DAYS);

        //when
        Map<String, String> resultQuery = modifier(query).endTime(newEndTime).toMap();

        //then
        Assert.assertNotNull(resultQuery);
        assertEquals(query.get(VARIABLE_KEY), resultQuery.get(VARIABLE_KEY));
        assertEquals(String.valueOf(TimeUtils.getNanosFromInstant(TIME_2)), query.get(END_TIME_KEY));
        assertEquals(String.valueOf(TimeUtils.getNanosFromInstant(newEndTime)), resultQuery.get(END_TIME_KEY));
        assertEquals(query.get(START_TIME_KEY), resultQuery.get(START_TIME_KEY));
    }

    private VariableQuery.VariableStartTimeStage builder(String variable) {
        return VariableQuery.builder(mock(SparkSession.class)).variable(variable);
    }

    private VariableQuery.Modifier modifier(Map<String, String> query) {
        return VariableQuery.modifier(mock(SparkSession.class), query);
    }
}
