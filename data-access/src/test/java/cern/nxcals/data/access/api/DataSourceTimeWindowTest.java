/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.data.access.api;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;

import static org.mockito.Mockito.when;

/**
 * Created by ntsvetko on 12/1/16.
 */
public class DataSourceTimeWindowTest {

    @Mock
    private Clock clockMock;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        when(this.clockMock.getZone()).thenReturn(ZoneId.systemDefault());
    }

    @Test
    public void shouldGetDataBothSourceTimeWindows() {
        //given
        Instant startTime = Instant.ofEpochSecond(2 * 60);
        Instant endTime = Instant.ofEpochSecond(4 * 60);

        Instant now = Instant.ofEpochSecond(3 * 60 + DataSourceTimeWindow.HBASE_PERIOD_HOURS * 3600);
        when(this.clockMock.instant()).thenReturn(now);

        //when
        DataSourceTimeWindow dsTimeWindow = DataSourceTimeWindow.getTimeWindows(startTime, endTime, clockMock);

        //then
        Assert.assertNotNull(dsTimeWindow);
        Assert.assertEquals(dsTimeWindow.getHdfsTimeWindow().get().getStartTime(), startTime);
        Assert.assertEquals(dsTimeWindow.getHbaseTimeWindow().get().getEndTime(), endTime);
        Assert.assertEquals(dsTimeWindow.getHdfsTimeWindow().get().getEndTime(),
                dsTimeWindow.getHbaseTimeWindow().get().getStartTime().minus(Duration.ofNanos(1)));
        Assert.assertTrue(dsTimeWindow.getHdfsTimeWindow().get().getStartTime()
                .compareTo(dsTimeWindow.getHdfsTimeWindow().get().getEndTime()) < 0);
        Assert.assertTrue(dsTimeWindow.getHbaseTimeWindow().get().getStartTime()
                .compareTo(dsTimeWindow.getHbaseTimeWindow().get().getEndTime()) < 0);
    }

    @Test
    public void shouldGetOnlyHdfsSourceTimeWindow() {
        //given
        Instant startTime = Instant.ofEpochSecond(2 * 60);
        Instant endTime = Instant.ofEpochSecond(4 * 60);

        Instant now = Instant.ofEpochSecond(5 * 60 + DataSourceTimeWindow.HBASE_PERIOD_HOURS * 3600);
        when(this.clockMock.instant()).thenReturn(now);

        //when
        DataSourceTimeWindow dsTimeWindow = DataSourceTimeWindow.getTimeWindows(startTime, endTime, clockMock);

        //then
        Assert.assertNotNull(dsTimeWindow);
        Assert.assertFalse(dsTimeWindow.getHbaseTimeWindow().isPresent());
        Assert.assertEquals(dsTimeWindow.getHdfsTimeWindow().get().getStartTime(), startTime);
        Assert.assertEquals(dsTimeWindow.getHdfsTimeWindow().get().getEndTime(), endTime);
        Assert.assertTrue(dsTimeWindow.getHdfsTimeWindow().get().getStartTime()
                .compareTo(dsTimeWindow.getHdfsTimeWindow().get().getEndTime()) < 0);
    }

    @Test
    public void shouldGetOnlyHbaseSourceTimeWindow() {
        //given
        Instant startTime = Instant.ofEpochSecond(2 * 60);
        Instant endTime = Instant.ofEpochSecond(4 * 60);

        Instant now = Instant.ofEpochSecond(1 * 60 + DataSourceTimeWindow.HBASE_PERIOD_HOURS * 3600);
        when(this.clockMock.instant()).thenReturn(now);

        //when
        DataSourceTimeWindow dsTimeWindow = DataSourceTimeWindow.getTimeWindows(startTime, endTime, clockMock);

        //then
        Assert.assertNotNull(dsTimeWindow);
        Assert.assertFalse(dsTimeWindow.getHdfsTimeWindow().isPresent());
        Assert.assertEquals(dsTimeWindow.getHbaseTimeWindow().get().getStartTime(), startTime);
        Assert.assertEquals(dsTimeWindow.getHbaseTimeWindow().get().getEndTime(), endTime);
        Assert.assertTrue(dsTimeWindow.getHbaseTimeWindow().get().getStartTime()
                .compareTo(dsTimeWindow.getHbaseTimeWindow().get().getEndTime()) < 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAcceptWrongTimeWindow() {
        //given
        Instant startTime = Instant.ofEpochSecond(3 * 60);
        Instant endTime = Instant.ofEpochSecond(2 * 60);

        //when
        DataSourceTimeWindow dsTimeWindow = DataSourceTimeWindow.getTimeWindows(startTime, endTime);
    }
}
