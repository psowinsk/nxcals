/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.data.access.api;

import cern.nxcals.common.domain.EntityResources;
import cern.nxcals.common.domain.SchemaData;
import cern.nxcals.common.utils.TimeUtils;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.DataTypes;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class HdfsDataAccessServiceImplTest extends BaseServiceTest {

    @Mock
    private InternalQueryData queryDataMock;
    private Set<InternalQueryData> setQueryDataMock;

    @Mock
    private Map<InternalQueryData, List<String>> columns;

    @Test
    public void shouldGetHdfsDataFor() throws URISyntaxException, IOException {
        // GIVEN
        String timestampField = "__timestamp_field__";
        SchemaData schema = getTestEntityData(getTestDevPropSystemData()).getSchemaData();
        long startTime = TimeUtils.getNanosFromInstant(Instant.now().minus(4, ChronoUnit.DAYS));
        long endTime = TimeUtils.getNanosFromInstant(Instant.now().minus(1, ChronoUnit.DAYS));
        List<DataAccessField> fields = new ArrayList<>();
        fields.add(new DataAccessFieldBuilder(timestampField).dataType(DataTypes.StringType)
                .alias(NXC_EXTR_TIMESTAMP.getValue()).build());
        fields.add(new DataAccessFieldBuilder("string_field").dataType(DataTypes.StringType).build());
        fields.add(new DataAccessFieldBuilder("array_field").dataType(DataTypes.createArrayType(DataTypes.FloatType))
                .build());

        // WHEN
        EntityResources entityResources = getTestEntityData(getTestDevPropSystemData());

        for (URI uri : entityResources.getResourcesData().getHdfsPaths()) {
            RemoteIterator remoteIterator = mock(RemoteIterator.class);
            Path path = new Path(uri);
            LocatedFileStatus fileStatus = mock(LocatedFileStatus.class);
            Path concreteFilePath = mock(Path.class);
            when(fileSystemMock.exists(path.getParent())).thenReturn(true);
            when(remoteIterator.hasNext()).thenReturn(true);
            when(remoteIterator.next()).thenReturn(locatedFileStatusMock);
            when(locatedFileStatusMock.getPath()).thenReturn(concreteFilePath);
            when(concreteFilePath.getName()).thenReturn("/test.parquet");
            when(fileSystemMock.listFiles(path.getParent(), false)).thenReturn(remoteIterator);
        }

        ResourceQuery<URI> resourceQuery = mock(ResourceQuery.class);
        when(resourceQuery.getStartTime()).thenReturn(startTime);
        when(resourceQuery.getEndTime()).thenReturn(endTime);
        when(resourceQuery.getEntityIds()).thenReturn(Collections.singleton(entityResources.getId()));
        when(resourceQuery.getResources()).thenReturn(entityResources.getResourcesData().getHdfsPaths());
        when(resourceQuery.getColumns()).thenReturn(Collections.emptyList());

        HdfsResourceBasedQuery hdfsResourceBasedQuery = mock(HdfsResourceBasedQuery.class);
        when(hdfsResourceBasedQuery.getFields()).thenReturn(fields);
        when(hdfsResourceBasedQuery.getResources()).thenReturn(Collections.singleton(resourceQuery));
        when(hdfsResourceBasedQuery.getTimestampField()).thenReturn(timestampField);

        Dataset<Row> ds = hdfsDataAccessService.getDataFor(hdfsResourceBasedQuery);

        //then
        Assert.assertNotNull(ds);
        Assert.assertEquals(datasetMock, ds);
        //should not union now
        verify(datasetMock, times(0)).union(datasetMock);
    }

}
