/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.data.access.builders;

import cern.nxcals.common.utils.TimeUtils;
import cern.nxcals.data.access.builders.mapper.QueryBuilderMapper;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Assert;

import java.io.IOException;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import static cern.nxcals.data.access.Constants.ALIASES_KEY;
import static cern.nxcals.data.access.Constants.ALIASES_TYPE;
import static cern.nxcals.data.access.Constants.END_TIME_KEY;
import static cern.nxcals.data.access.Constants.FIELDS_KEY;
import static cern.nxcals.data.access.Constants.KEY_VALUES_KEY;
import static cern.nxcals.data.access.Constants.KEY_VALUES_TYPE;
import static cern.nxcals.data.access.Constants.START_TIME_KEY;
import static org.junit.Assert.assertTrue;

/**
 * Class with common checks used through several builders tests.
 *
 * @author timartin
 */
public final class QueryAssertions {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private QueryAssertions() {
        /* Nothing to do here */
    }

    public static final void assertFields(String[] fields, Map<String, String> queryMap) {
        Assert.assertEquals(QueryBuilderMapper.serializeOrThrow(fields), queryMap.get(FIELDS_KEY));
    }

    public static final void assertAliases(Map<String, String> queryMap, Predicate<Map<String, List<String>>> predicate)
            throws IOException {
        Map<String, List<String>> aliases = OBJECT_MAPPER.readValue(queryMap.get(ALIASES_KEY), ALIASES_TYPE);
        assertTrue(predicate.test(aliases));
    }

    public static final void assertKeyValues(Map<String, String> queryMap,
            Predicate<Map<String, Map<String, String>>> predicate)
            throws IOException {
        Map<String, Map<String, String>> keyValues = OBJECT_MAPPER
                .readValue(queryMap.get(KEY_VALUES_KEY), KEY_VALUES_TYPE);
        assertTrue(predicate.test(keyValues));
    }

    public static final void assertStartTime(Instant expectedInstant, Map<String, String> queryMap) {
        String expectedString = String.valueOf(TimeUtils.getNanosFromInstant(expectedInstant));
        Assert.assertEquals(expectedString, queryMap.get(START_TIME_KEY));
    }

    public static final void assertEndTime(Instant expectedInstant, Map<String, String> queryMap) {
        String expectedString = String.valueOf(TimeUtils.getNanosFromInstant(expectedInstant));
        Assert.assertEquals(expectedString, queryMap.get(END_TIME_KEY));
    }
}
