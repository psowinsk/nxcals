/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.data.access.api;

import cern.nxcals.service.client.api.internal.InternalEntityService;
import org.apache.spark.sql.SparkSession;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

/**
 * Created by ntsvetko on 12/5/16.
 */
public class DataAccessServiceFactoryTest {

    @Mock
    private InternalEntityService entityService;

    @Mock
    private SparkSession sparkSession;

    @InjectMocks
    private DataAccessServiceFactory dataAccessServiceFactory;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldCreateDataAccessService() {
        //when
        DataAccessService dataAccessService = dataAccessServiceFactory.createDataAccessService(sparkSession);

        //then
        Assert.assertNotNull(dataAccessService);
    }
}
