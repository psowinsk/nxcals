/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.data.access.api;

import cern.nxcals.data.access.BaseTest;
import org.apache.avro.Schema;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ntsvetko on 11/30/16.
 */
public class SparkTypeUtilsTest extends BaseTest {

    @Test
    public void shouldGetTypeFor() {
        //given
        String intSchema = "[\"int\"]";
        String longSchema = "[\"long\"]";
        String floatSchema = "[\"float\"]";
        String doubleSchema = "[\"double\"]";
        String stringSchema = "[\"string\"]";
        String bytesSchema = "[\"bytes\"]";
        String booleanSchema = "[\"boolean\"]";
        String unionSchema = "[\"double\",\"null\"]";
        String arraySchema = "[{\"type\":\"array\",\"items\":[\"float\",\"null\"]},\"null\"]";
        String recordSchema = "[{\"type\":\"record\",\"name\":\"float_array_2d\",\"namespace\":\"cern.nxcals\"," +
                "\"fields\":[{\"name\":\"elements\",\"type\":[{\"type\":\"array\",\"items\":[\"float\",\"null\"]},\"null\"]},"
                +
                "{\"name\":\"rowCount\",\"type\":[\"int\",\"null\"]},{\"name\":\"columnCount\",\"type\":[\"int\",\"null\"]}]},\"null\"]";

        List<StructField> recordFields = new ArrayList<>();
        recordFields.add(DataTypes.createStructField("elements", DataTypes.createArrayType(DataTypes.FloatType), true));
        recordFields.add(DataTypes.createStructField("rowCount", DataTypes.IntegerType, true));
        recordFields.add(DataTypes.createStructField("columnCount", DataTypes.IntegerType, true));
        DataType expectedRecordType = DataTypes.createStructType(recordFields);

        //when
        DataType intType = SparkTypeUtils.getDataTypeFor(new Schema.Parser().parse(intSchema));
        DataType longType = SparkTypeUtils.getDataTypeFor(new Schema.Parser().parse(longSchema));
        DataType floatType = SparkTypeUtils.getDataTypeFor(new Schema.Parser().parse(floatSchema));
        DataType doubleType = SparkTypeUtils.getDataTypeFor(new Schema.Parser().parse(doubleSchema));
        DataType stringType = SparkTypeUtils.getDataTypeFor(new Schema.Parser().parse(stringSchema));
        DataType bytesType = SparkTypeUtils.getDataTypeFor(new Schema.Parser().parse(bytesSchema));
        DataType booleanType = SparkTypeUtils.getDataTypeFor(new Schema.Parser().parse(booleanSchema));
        DataType unionType = SparkTypeUtils.getDataTypeFor(new Schema.Parser().parse(unionSchema));
        DataType arrayType = SparkTypeUtils.getDataTypeFor(new Schema.Parser().parse(arraySchema));
        DataType recordType = SparkTypeUtils.getDataTypeFor(new Schema.Parser().parse(recordSchema));

        //then
        Assert.assertNotNull(intType);
        Assert.assertEquals(DataTypes.IntegerType, intType);
        Assert.assertNotNull(longType);
        Assert.assertEquals(DataTypes.LongType, longType);
        Assert.assertNotNull(floatType);
        Assert.assertEquals(DataTypes.FloatType, floatType);
        Assert.assertNotNull(doubleType);
        Assert.assertEquals(DataTypes.DoubleType, doubleType);
        Assert.assertNotNull(stringType);
        Assert.assertEquals(DataTypes.StringType, stringType);
        Assert.assertNotNull(bytesType);
        Assert.assertEquals(DataTypes.IntegerType, bytesType);
        Assert.assertNotNull(booleanType);
        Assert.assertEquals(DataTypes.BooleanType, booleanType);
        Assert.assertNotNull(unionType);
        Assert.assertEquals(DataTypes.DoubleType, unionType);
        Assert.assertNotNull(arrayType);
        Assert.assertEquals(DataTypes.createArrayType(DataTypes.FloatType), arrayType);
        Assert.assertNotNull(recordSchema);
        Assert.assertEquals(expectedRecordType, recordType);
    }

    @Test(expected = RuntimeException.class)
    public void shouldFailForUnknownType() {
        //given
        Schema intSchema = new Schema.Parser().parse("[{ \"type\": \"enum\"," +
                "  \"name\": \"Suit\", \"symbols\" : [\"TEST1\", \"TEST2\"]}]");

        //when
        DataType intType = SparkTypeUtils.getDataTypeFor(intSchema);
    }

}
