/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.data.access.api;

import cern.nxcals.common.domain.ResourceData;
import cern.nxcals.common.domain.TimeWindow;
import cern.nxcals.data.access.BaseTest;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.DataFrameReader;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.StructType;
import org.junit.Before;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.net.URISyntaxException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.anyVararg;
import static org.mockito.Mockito.when;

/**
 * Test suite that work as a base to all Spark component dependent tests.
 *
 * @author ntsvetko
 */
public abstract class BaseServiceTest extends BaseTest {

    @Mock
    protected SparkSession sparkSessionMock;
    @Mock
    protected DataFrameReader readerMock;
    @Mock
    protected Column columnMock;
    @Mock
    protected FileSystem fileSystemMock;
    @Mock
    protected Dataset<Row> datasetMock;
    @Mock
    protected RemoteIterator remoteIteratorMock;
    @Mock
    protected LocatedFileStatus locatedFileStatusMock;
    @Mock
    protected Path pathMock;

    @InjectMocks
    protected HdfsDataAccessServiceImpl hdfsDataAccessService;

    @InjectMocks
    protected HbaseDataAccessServiceImpl hbaseDataAccessService;

    @Before
    public void init() throws URISyntaxException {
        MockitoAnnotations.initMocks(this);
        this.hdfsDataAccessService = new HdfsDataAccessServiceImpl(sparkSessionMock, fileSystemMock);
        this.hbaseDataAccessService = new HbaseDataAccessServiceImpl(sparkSessionMock);

        ResourceData resourceData = this.getTestResourceData();

        when(sparkSessionMock.read()).thenReturn(readerMock);
        when(sparkSessionMock.createDataFrame(anyList(), any(StructType.class))).thenReturn(datasetMock);
        when(readerMock.format(anyString())).thenReturn(readerMock);
        when(readerMock.options(anyMap())).thenReturn(readerMock);
        when(readerMock.load((String[]) anyVararg())).thenReturn(datasetMock);
        when(readerMock.load()).thenReturn(datasetMock);
        when(datasetMock.select((Column[]) anyVararg())).thenReturn(datasetMock);
        when(datasetMock.selectExpr((String[]) anyVararg())).thenReturn(datasetMock);
        when(datasetMock.where(any(Column.class))).thenReturn(datasetMock);
        when(datasetMock.col(anyString())).thenReturn(columnMock);
        when(columnMock.isin(anyLong())).thenReturn(columnMock);
        when(columnMock.geq(anyLong())).thenReturn(columnMock);
        when(columnMock.$less(anyLong())).thenReturn(columnMock);
        when(columnMock.and(any(Column.class))).thenReturn(columnMock);
        when(columnMock.between(any(), any())).thenReturn(columnMock);
        when(columnMock.leq(any())).thenReturn(columnMock);
        when(columnMock.equalTo(any())).thenReturn(columnMock);

        when(datasetMock.union(datasetMock)).thenReturn(datasetMock);

    }

    protected TimeWindow getTestTimeWindow() {
        Instant startTime = Instant.now().minus(4, ChronoUnit.DAYS);
        Instant endTime = Instant.now().minus(1, ChronoUnit.DAYS);
        return TimeWindow.between(startTime, endTime);
    }

}
