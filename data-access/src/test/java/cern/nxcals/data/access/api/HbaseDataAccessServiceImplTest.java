/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.data.access.api;

import cern.nxcals.common.domain.EntityResources;
import cern.nxcals.common.utils.TimeUtils;
import com.google.common.collect.ImmutableSet;
import org.apache.avro.Schema;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.DataTypes;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static cern.nxcals.common.SystemFields.NXC_ENTITY_ID;
import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static java.util.stream.Collectors.toSet;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class HbaseDataAccessServiceImplTest extends BaseServiceTest {
    @Mock
    private InternalQueryData queryDataMock;
    private Set<InternalQueryData> setQueryDataMock;

    @Mock
    private Map<InternalQueryData, List<String>> columns;

    @Test
    public void shouldGetHbaseSingleData() {
        // given
        String timestampField = "__timestamp_field__";
        List<String> hbaseColumns = Arrays
                .asList(NXC_ENTITY_ID.getValue(), NXC_EXTR_TIMESTAMP.getValue(), "string_field"
                        , "array_field", "array2D_field");

        long timeInNanos = TimeUtils.getNanosFromInstant(Instant.ofEpochSecond(10 * 60).minus(1, ChronoUnit.DAYS));

        List<DataAccessField> fields = defaultFields(timestampField);

        // when
        EntityResources entityResources = getTestEntityData(getTestDevPropSystemData());

        ResourceQuery<HbaseResource> resourceQuery = mockResourceQuery(hbaseColumns, timeInNanos, entityResources);

        ResourcesBasedQuery<HbaseResource> resourcesBasedQuery = mock(ResourcesBasedQuery.class);
        when(resourcesBasedQuery.getFields()).thenReturn(fields);
        when(resourcesBasedQuery.getResources()).thenReturn(Collections.singleton(resourceQuery));

        Dataset<Row> ds = hbaseDataAccessService.getDataFor(resourcesBasedQuery);

        //then
        Assert.assertNotNull(ds);
        Assert.assertEquals(datasetMock, ds);
        verify(datasetMock, never()).union(datasetMock);
    }

    @Test
    public void shouldGetHbaseMultiData() {
        // given
        String timestampField = "__timestamp_field__";
        List<String> hbaseColumns = Arrays
                .asList(NXC_ENTITY_ID.getValue(), NXC_EXTR_TIMESTAMP.getValue(), "string_field"
                        , "array_field", "array2D_field");

        long timeInNanos = TimeUtils.getNanosFromInstant(Instant.ofEpochSecond(10 * 60).minus(1, ChronoUnit.DAYS));

        List<DataAccessField> fields = defaultFields(timestampField);

        // when
        EntityResources entityResources = getTestEntityData(getTestDevPropSystemData());

        ResourceQuery<HbaseResource> resourceQuery1 = mockResourceQuery(hbaseColumns, timeInNanos, entityResources);
        ResourceQuery<HbaseResource> resourceQuery2 = mockResourceQuery(hbaseColumns, timeInNanos, entityResources);

        ResourcesBasedQuery<HbaseResource> resourcesBasedQuery = mock(ResourcesBasedQuery.class);
        when(resourcesBasedQuery.getFields()).thenReturn(fields);
        when(resourcesBasedQuery.getResources()).thenReturn(ImmutableSet.of(resourceQuery1, resourceQuery2));

        Dataset<Row> ds = hbaseDataAccessService.getDataFor(resourcesBasedQuery);

        //then
        Assert.assertNotNull(ds);
        Assert.assertEquals(datasetMock, ds);
        verify(datasetMock, times(1)).union(datasetMock);
    }

    private List<DataAccessField> defaultFields(String timestampField) {
        Schema array2dSchema = new Schema.Parser()
                .parse("[{\"type\":\"record\",\"name\":\"float_array_2d\",\"fields\":[{\"name\":\"elements\",\"type\""
                        + ":[{\"type\":\"array\",\"items\":[\"float\",\"null\"]},\"null\"]},{\"name\":\"rowCount\",\"type\":[\"int\",\"null\"]},\n"
                        + "{\"name\":\"columnCount\",\"type\":[\"int\",\"null\"]}]},\"null\"]");

        List<DataAccessField> fields = new ArrayList<>();
        fields.add(new DataAccessFieldBuilder(timestampField).dataType(DataTypes.StringType)
                .fieldSchema(new Schema.Parser().parse("[\"string\"]")).alias(NXC_EXTR_TIMESTAMP.getValue()).build());
        fields.add(new DataAccessFieldBuilder("string_field").dataType(DataTypes.StringType)
                .fieldSchema(new Schema.Parser().parse("[\"string\",\"null\"]")).build());
        fields.add(new DataAccessFieldBuilder("array_field").dataType(DataTypes.createArrayType(DataTypes.IntegerType))
                .fieldSchema(new Schema.Parser().parse("[{\"type\":\"array\",\"items\":[\"int\",\"null\"]},\"null\"]"))
                .build());
        fields.add(new DataAccessFieldBuilder("array2D_field").dataType(SparkTypeUtils.getDataTypeFor(array2dSchema))
                .fieldSchema(array2dSchema).build());
        return fields;
    }

    private ResourceQuery<HbaseResource> mockResourceQuery(List<String> hbaseColumns, long timeInNanos,
            EntityResources entityResources) {
        ResourceQuery<HbaseResource> resourceQuery = mock(ResourceQuery.class);
        when(resourceQuery.getColumns()).thenReturn(hbaseColumns);
        when(resourceQuery.getStartTime()).thenReturn(timeInNanos);
        when(resourceQuery.getEndTime()).thenReturn(timeInNanos);
        when(resourceQuery.getEntityIds()).thenReturn(Collections.singleton(entityResources.getId()));

        Set<HbaseResource> resources = entityResources.getResourcesData().getHbaseTableNames().stream()
                .map(tableName -> new HbaseResource(tableName, entityResources.getResourcesData().getHbaseNamespace()))
                .collect(toSet());

        when(resourceQuery.getResources()).thenReturn(resources);
        return resourceQuery;
    }


    @Test
    public void shouldBuildHbaseKeyPredicate() {
        //given
        long entityId = 123L;
        long timestamp = 987L;

        //when
        String result = hbaseDataAccessService.getKeyPredicateFor(entityId, timestamp);

        //then
        String expected = entityId + "__" + (Long.MAX_VALUE - timestamp);
        Assert.assertNotNull(result);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void shouldBuildTableCatalog() {
        //given
        String namespace = "default";
        String tableName = "1__2__1";

        Schema array2dSchema = new Schema.Parser()
                .parse("[{\"type\":\"record\",\"name\":\"float_array_2d\",\"fields\":[{\"name\":\"elements\",\"type\""
                        + ":[{\"type\":\"array\",\"items\":[\"float\",\"null\"]},\"null\"]},{\"name\":\"rowCount\",\"type\":[\"int\",\"null\"]},\n"
                        + "{\"name\":\"columnCount\",\"type\":[\"int\",\"null\"]}]},\"null\"]");

        String expectedCatalog = "{\"table\": {\"namespace\": \"default\", \"name\":\"1__2__1\"},"
                + "\"rowkey\":\"id\",\"columns\":{\"entityKey\":{\"cf\":\"rowkey\",\"col\":\"id\",\"type\":\"string\"},"
                + "\"array_field\":{\"cf\":\"data\",\"col\":\"array_field\",\"avro\":\"1__2__1__array_field\"},"
                + "\"array2D_field\":{\"cf\":\"data\",\"col\":\"array2D_field\",\"avro\":\"1__2__1__array2d_field\"},"
                + "\"string_field\":{\"cf\":\"data\",\"col\":\"string_field\",\"avro\":\"1__2__1__string_field\"}}}";

        String expectedComplexTypeSchema = "[{\"type\":\"record\",\"name\":\"float_array_2d\","
                + "\"fields\":[{\"name\":\"elements\",\"type\":[{\"type\":\"array\",\"items\":[\"float\",\"null\"]},\"null\"]},"
                + "{\"name\":\"rowCount\",\"type\":[\"int\",\"null\"]},"
                + "{\"name\":\"columnCount\",\"type\":[\"int\",\"null\"]}]},\"null\"]";

        List<DataAccessField> fields = new ArrayList<>();
        fields.add(new DataAccessFieldBuilder("array_field").dataType(DataTypes.createArrayType(DataTypes.IntegerType))
                .fieldSchema(new Schema.Parser().parse("[{\"type\":\"array\",\"items\":[\"int\",\"null\"]},\"null\"]"))
                .build());
        fields.add(new DataAccessFieldBuilder("array2D_field").dataType(SparkTypeUtils.getDataTypeFor(array2dSchema))
                .fieldSchema(array2dSchema).build());
        fields.add(new DataAccessFieldBuilder("string_field").dataType(DataTypes.StringType)
                .fieldSchema(new Schema.Parser().parse("[\"string\",\"null\"]")).build());
        //when
        Map<String, String> resultMap = hbaseDataAccessService.tableCatalog(namespace, tableName, fields);

        //then
        Assert.assertFalse(resultMap.isEmpty());
        Assert.assertEquals(4, resultMap.size());
        Assert.assertNotNull(resultMap.get("catalog"));
        Assert.assertEquals(expectedCatalog, resultMap.get("catalog"));
        Assert.assertNotNull(resultMap.get("1__2__1__array2d_field"));
        Assert.assertEquals(expectedComplexTypeSchema, resultMap.get("1__2__1__array2d_field"));
    }
}
