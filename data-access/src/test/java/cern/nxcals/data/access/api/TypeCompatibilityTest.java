/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.data.access.api;

import cern.nxcals.data.access.BaseTest;
import org.apache.avro.Schema;
import org.apache.spark.sql.types.DataTypes;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by ntsvetko on 11/30/16.
 */
public class TypeCompatibilityTest extends BaseTest {
    private final Schema intSchema = new Schema.Parser().parse("\"int\"");
    private final Schema doubleSchema = new Schema.Parser().parse("\"double\"");
    private final Schema doubleUnionSchema = new Schema.Parser().parse("[\"double\",\"null\"]");
    private final Schema intUnionSchema = new Schema.Parser().parse("[\"int\",\"null\"]");
    private final Schema bytesUnionSchema = new Schema.Parser().parse("[\"bytes\",\"null\"]");
    private final Schema longUnionSchema = new Schema.Parser().parse("[\"long\",\"null\"]");
    private final Schema booleanUnionSchema = new Schema.Parser().parse("[\"boolean\",\"null\"]");
    private final Schema floatUnionSchema = new Schema.Parser().parse("[\"float\",\"null\"]");
    private final Schema arrayUnionSchema = new Schema.Parser()
            .parse("[{\"type\":\"array\",\"items\":[\"float\",\"null\"]},\"null\"]");

    @Test(expected = RuntimeException.class)
    public void shouldFailPromotingComplexTypes() {
        //given
        DataAccessField field = new DataAccessFieldBuilder("field").build();

        Set<Schema> schemas = new HashSet<>();
        schemas.add(intUnionSchema);
        schemas.add(arrayUnionSchema);
        schemas.add(longUnionSchema);
        schemas.add(booleanUnionSchema);
        schemas.add(floatUnionSchema);

        //when
        DataAccessField dataAccessField = TypeCompatibility.getPromotedFieldTypeFor(field, schemas);
    }

    @Test
    public void shouldGetPromotedFieldToLongType() {
        //given
        DataAccessField field = new DataAccessFieldBuilder("field").build();

        Set<Schema> schemas = new HashSet<>();
        schemas.add(intUnionSchema);
        schemas.add(bytesUnionSchema);
        schemas.add(longUnionSchema);
        schemas.add(booleanUnionSchema);

        //when
        DataAccessField dataAccessField = TypeCompatibility.getPromotedFieldTypeFor(field, schemas);

        //then
        Assert.assertNotNull(dataAccessField);
        Assert.assertEquals(DataTypes.LongType, dataAccessField.getDataType());
        Assert.assertEquals(longUnionSchema, dataAccessField.getFieldSchema());
    }

    @Test
    public void shouldGetPromotedFieldToDoubleType() {
        //given
        DataAccessField field = new DataAccessFieldBuilder("field").build();

        Set<Schema> schemas = new HashSet<>();
        schemas.add(intUnionSchema);
        schemas.add(doubleSchema);
        schemas.add(longUnionSchema);
        schemas.add(booleanUnionSchema);
        schemas.add(floatUnionSchema);

        //when
        DataAccessField dataAccessField = TypeCompatibility.getPromotedFieldTypeFor(field, schemas);

        //then
        Assert.assertNotNull(dataAccessField);
        Assert.assertEquals(dataAccessField.getDataType(), DataTypes.DoubleType);
        Assert.assertEquals(doubleUnionSchema, dataAccessField.getFieldSchema());
    }

    @Test
    public void shouldNotPromoteFieldsWithSameType() {
        //given
        DataAccessField field = new DataAccessFieldBuilder("field").build();

        Set<Schema> schemas = new HashSet<>();
        schemas.add(intSchema);
        schemas.add(intUnionSchema);

        //when
        DataAccessField dataAccessField = TypeCompatibility.getPromotedFieldTypeFor(field, schemas);

        //then
        Assert.assertNotNull(dataAccessField);
        Assert.assertEquals(dataAccessField.getDataType(), DataTypes.IntegerType);
    }
}
