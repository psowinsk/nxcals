/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.data.access.builders;

import cern.nxcals.data.access.builders.fluent.stages.BuildStage;
import cern.nxcals.data.access.builders.fluent.stages.StartTimeStage;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.spark.sql.SparkSession;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cern.nxcals.data.access.Constants.SYSTEM_KEY;
import static cern.nxcals.data.access.builders.QueryAssertions.assertAliases;
import static cern.nxcals.data.access.builders.QueryAssertions.assertEndTime;
import static cern.nxcals.data.access.builders.QueryAssertions.assertFields;
import static cern.nxcals.data.access.builders.QueryAssertions.assertKeyValues;
import static cern.nxcals.data.access.builders.QueryAssertions.assertStartTime;
import static org.mockito.Mockito.mock;

/**
 * Test suite for {@link KeyValuesQuery}.
 */
@RunWith(JUnitParamsRunner.class)
public class KeyValuesQueryTest {
    private static final String SYSTEM_1 = "CMW";
    private static final String SYSTEM_2 = "PM";

    private static final String KEY_1 = "KEY_1";
    private static final String KEY_2 = "KEY_2";
    private static final String KEY_3 = "KEY_3";
    private static final String VALUE_1 = "VALUE_1";
    private static final String VALUE_2 = "VALUE_2";
    private static final String VALUE_3 = "VALUE_3";
    private static final Map<String, String> KEY_VALUES = ImmutableMap.of(KEY_1, VALUE_1, KEY_2, VALUE_2);

    private static final Instant TIME_1 = Instant.ofEpochSecond(1505313230, 123456789);
    private static final Instant TIME_2 = Instant.ofEpochSecond(1505316830, 123456789);
    private static final Instant TIME_3 = Instant.ofEpochSecond(1505316830, 123423453);
    private static final String TIME_1_STRING = "2017-09-13 14:33:50.123456789";
    private static final String TIME_2_STRING = "2017-09-13 15:33:50.123456789";
    private static final String TIME_3_STRING = "2017-09-13 15:33:50.123423453";

    private static final String FIELD_1 = "FIELD_1";
    private static final String FIELD_2 = "FIELD_2";
    private static final String FIELD_3 = "FIELD_3";
    private static final String FIELD_4 = "FIELD_4";
    private static final String[] FIELDS = new String[] { FIELD_1, FIELD_2, FIELD_3 };

    private static final String ALIAS_1 = "ALIAS_1";
    private static final String ALIAS_2 = "ALIAS_2";
    private static final String ALIAS_3 = "ALIAS_3";

    private static final Map<String, List<String>> ALIASES = QueryFieldAliases
            .alias(ALIAS_1).fields(FIELD_1, FIELD_2)
            .alias(ALIAS_2).fields(FIELD_3).build();

    private static final Map<String, String> QUERY_MAP = toMap(KeyValuesQuery.builder(mock(SparkSession.class))
            .system(SYSTEM_1)
            .startTime(TIME_1)
            .endTime(TIME_2)
            .fields(FIELDS)
            .fieldAliases(ALIASES)
            .entity().keyValues(KEY_VALUES));

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    private static Map<String, String> toMap(BuildStage<?> builder) {
        return ((KeyValuesQuery.Internal) builder).toMap();
    }

    @Test
    public void shouldThrowWhenCreatingBuilderWithSystemToNull() {
        expectedException.expect(NullPointerException.class);
        builder(null);
    }

    @Test
    public void shouldCreateQueryFromStringTimeDates() {
        //when
        Map<String, String> query = toMap(builder(SYSTEM_1)
                .startTime(TIME_1_STRING)
                .endTime(TIME_2_STRING)
                .entity().keyValue(KEY_1, VALUE_1));

        //then
        assertStartTime(TIME_1, query);
        assertEndTime(TIME_2, query);
    }

    @Test
    @Parameters(method = "wrongDateTimeStringFormats")
    public void shouldFailForInvalidStartTimeFormat(String dateTimeString,
            Class<? extends Throwable> expectedExceptionClass) {
        expectedException.expect(expectedExceptionClass);
        builder(SYSTEM_1).startTime(dateTimeString);
    }

    @Test
    @Parameters(method = "wrongDateTimeStringFormats")
    public void shouldFailForInvalidEndTimeFormat(String dateTimeString,
            Class<? extends Throwable> expectedExceptionClass) {
        expectedException.expect(expectedExceptionClass);

        //when
        builder(SYSTEM_1).startTime(TIME_1).endTime(dateTimeString);
    }

    @Test
    public void shouldCreateQueryWithStartInstantAndDuration() {
        //when
        Map<String, String> query = toMap(builder(SYSTEM_1)
                .startTime(TIME_1)
                .duration(Duration.between(TIME_1, TIME_2))
                .entity().keyValue(KEY_1, VALUE_1));

        //then
        assertStartTime(TIME_1, query);
        assertEndTime(TIME_2, query);
    }

    @Test
    public void shouldFailForNegativeDuration() {
        expectedException.expect(RuntimeException.class);
        builder(SYSTEM_1)
                .startTime(TIME_1)
                .duration(Duration.ofSeconds(-1));
    }

    @Test
    public void shouldCreateQueryWithSpecificTimestampInstant() {
        //when
        Map<String, String> query = toMap(builder(SYSTEM_1)
                .atTime(TIME_1)
                .entity().keyValue(KEY_1, VALUE_1));

        //then
        assertStartTime(TIME_1, query);
        assertEndTime(TIME_1, query);
    }

    @Test
    public void shouldCreateQueryWithSpecificTimestampString() {
        //when
        Map<String, String> query = toMap(builder(SYSTEM_1)
                .atTime(TIME_1_STRING)
                .entity().keyValue(KEY_1, VALUE_1));

        //then
        assertStartTime(TIME_1, query);
        assertEndTime(TIME_1, query);
    }

    @Test
    @Parameters(method = "wrongDateTimeStringFormats")
    public void shouldFailForInvalidTimestampString(String dateTimeString,
            Class<? extends Throwable> expectedExceptionClass) {
        expectedException.expect(expectedExceptionClass);

        //when
        builder(SYSTEM_1)
                .atTime(dateTimeString)
                .entity().keyValue(KEY_1, VALUE_1);
    }

    @Test
    public void shouldCreateKeyValueQuery() throws IOException {
        //when
        Map<String, String> query = toMap(builder(SYSTEM_1)
                .startTime(TIME_1)
                .endTime(TIME_2)
                .entity().keyValue(KEY_1, VALUE_1));

        //then
        Assert.assertNotNull(query);
        Assert.assertEquals(6, query.size());
        Assert.assertEquals(SYSTEM_1, query.get(SYSTEM_KEY));
        assertKeyValues(query, map -> map.containsValue(ImmutableMap.of(KEY_1, VALUE_1)));
        assertStartTime(TIME_1, query);
        assertEndTime(TIME_2, query);
    }

    @Test
    public void shouldCreateKeyValueQueryWithFields() throws IOException {
        //when
        Map<String, String> query = toMap(builder(SYSTEM_1)
                .startTime(TIME_1)
                .endTime(TIME_2)
                .fields(FIELDS)
                .entity().keyValue(KEY_1, VALUE_1).keyValue(KEY_2, VALUE_2));

        //then
        Assert.assertNotNull(query);
        Assert.assertEquals(6, query.size());
        Assert.assertEquals(SYSTEM_1, query.get(SYSTEM_KEY));
        assertKeyValues(query, map -> map.containsValue(ImmutableMap.of(KEY_1, VALUE_1, KEY_2, VALUE_2)));
        assertStartTime(TIME_1, query);
        assertEndTime(TIME_2, query);
        assertFields(FIELDS, query);
    }

    @Test
    public void shouldCreateKeyValuesQueryWithFields() throws IOException {
        //when
        Map<String, String> query = toMap(builder(SYSTEM_1)
                .startTime(TIME_1)
                .endTime(TIME_2)
                .fields(FIELDS)
                .entity().keyValues(KEY_VALUES));

        //then
        Assert.assertNotNull(query);
        Assert.assertEquals(6, query.size());
        Assert.assertEquals(SYSTEM_1, query.get(SYSTEM_KEY));
        assertKeyValues(query, map -> map.containsValue(KEY_VALUES));
        assertStartTime(TIME_1, query);
        assertEndTime(TIME_2, query);
        assertFields(FIELDS, query);
    }

    @Test
    public void shouldCreateKeyValuesQueryWithFieldAliases() throws IOException {
        //given
        String alias1 = "alias1";
        String alias2 = "alias2";

        Map<String, List<String>> aliases = QueryFieldAliases.alias(alias1).fields(FIELDS[0], FIELDS[1])
                .alias(alias2).fields(FIELDS[2]).build();

        //when
        Map<String, String> query = toMap(builder(SYSTEM_1)
                .startTime(TIME_1)
                .endTime(TIME_2)
                .fields(FIELDS)
                .fieldAliases(aliases)
                .entity().keyValues(KEY_VALUES));

        //then
        Assert.assertNotNull(query);
        Assert.assertEquals(6, query.size());
        Assert.assertEquals(SYSTEM_1, query.get(SYSTEM_KEY));
        assertKeyValues(query, map -> map.containsValue(KEY_VALUES));
        assertStartTime(TIME_1, query);
        assertEndTime(TIME_2, query);
        assertFields(FIELDS, query);
        assertAliases(query, aliases::equals);
    }

    @Test
    public void shouldThrowWhenCreatedModifyWithNull() {
        expectedException.expect(NullPointerException.class);
        modifier(null);
    }

    @Test
    public void shouldModifySystem() {
        Map<String, String> modifiedQuery = modifier(QUERY_MAP).system(SYSTEM_2).toMap();
        Assert.assertEquals(SYSTEM_1, QUERY_MAP.get(SYSTEM_KEY));
        Assert.assertEquals(SYSTEM_2, modifiedQuery.get(SYSTEM_KEY));
    }

    @Test
    public void shouldThrowWhenModifySystemToNull() {
        expectedException.expect(NullPointerException.class);
        modifier(QUERY_MAP).system(null);
    }

    @Test
    public void shouldModifyStartTimeWithInstant() {
        Map<String, String> modifiedQuery = modifier(QUERY_MAP).startTime(TIME_2).toMap();
        assertStartTime(TIME_1, QUERY_MAP);
        assertStartTime(TIME_2, modifiedQuery);
    }

    @Test
    public void shouldThrowWhenModifyStartDateWithNullInstant() {
        expectedException.expect(NullPointerException.class);
        modifier(QUERY_MAP).startTime((Instant) null);
    }

    @Test
    public void shouldModifyStartTimeWithString() {
        Map<String, String> modifiedQuery = modifier(QUERY_MAP).startTime(TIME_2_STRING).toMap();
        assertStartTime(TIME_1, QUERY_MAP);
        assertStartTime(TIME_2, modifiedQuery);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowWhenModifyStartDateWithNullString() {
        modifier(QUERY_MAP).startTime((String) null);
    }

    @Test
    public void shouldThrowWhenModifyStartDateWithInvalidFormatString() {
        expectedException.expect(DateTimeParseException.class);
        modifier(QUERY_MAP).startTime("");
    }

    @Test
    public void shouldModifyEndTimeWithInstant() {
        Map<String, String> modifiedQuery = modifier(QUERY_MAP).endTime(TIME_1).toMap();
        assertEndTime(TIME_2, QUERY_MAP);
        assertEndTime(TIME_1, modifiedQuery);
    }

    @Test
    public void shouldThrowWhenModifyEndDateWithNullInstant() {
        expectedException.expect(NullPointerException.class);
        modifier(QUERY_MAP).endTime((Instant) null);
    }

    @Test
    public void shouldModifyEndTimeWithString() {
        Map<String, String> modifiedQuery = modifier(QUERY_MAP).endTime(TIME_1_STRING).toMap();
        assertEndTime(TIME_2, QUERY_MAP);
        assertEndTime(TIME_1, modifiedQuery);
    }

    @Test
    public void shouldThrowWhenModifyEndDateWithNullString() {
        expectedException.expect(NullPointerException.class);
        modifier(QUERY_MAP).endTime((String) null);
    }

    @Test
    public void shouldThrowWhenModifyEndDateWithInvalidFormatString() {
        expectedException.expect(DateTimeParseException.class);
        modifier(QUERY_MAP).endTime("");
    }

    @Test
    public void shouldModifyDatesWithAtTimeWithInstant() {
        Map<String, String> modifiedQuery = modifier(QUERY_MAP).atTime(TIME_3).toMap();
        assertStartTime(TIME_1, QUERY_MAP);
        assertEndTime(TIME_2, QUERY_MAP);
        assertStartTime(TIME_3, modifiedQuery);
        assertEndTime(TIME_3, modifiedQuery);
    }

    @Test
    public void shouldThrowWhenModifyDatesWithAtTimeWithNullInstant() {
        expectedException.expect(NullPointerException.class);
        modifier(QUERY_MAP).atTime((Instant) null);
    }

    @Test
    public void shouldModifyDatesWithAtTimeWithString() {
        Map<String, String> modifiedQuery = modifier(QUERY_MAP).atTime(TIME_3_STRING).toMap();
        assertStartTime(TIME_1, QUERY_MAP);
        assertEndTime(TIME_2, QUERY_MAP);
        assertStartTime(TIME_3, modifiedQuery);
        assertEndTime(TIME_3, modifiedQuery);
    }

    @Test
    public void shouldThrowWhenModifyDatesWithAtTimeWithNullString() {
        expectedException.expect(NullPointerException.class);
        modifier(QUERY_MAP).atTime((String) null);
    }

    @Test
    public void shouldThrowWhenModifyDatesWithAtTimeWithInvalidFormatString() {
        expectedException.expect(DateTimeParseException.class);
        modifier(QUERY_MAP).atTime("");
    }

    @Test
    public void shouldAddFieldWhenNonExistent() {
        Map<String, String> modifiedQuery = modifier(QUERY_MAP).addFields(FIELD_4).toMap();
        assertFields(FIELDS, QUERY_MAP);
        assertFields(ArrayUtils.add(FIELDS, FIELD_4), modifiedQuery);
    }

    @Test
    public void shouldIgnoreFieldWhenAddingExistent() {
        Map<String, String> modifiedQuery = modifier(QUERY_MAP).addFields(FIELD_1, FIELD_2).toMap();
        assertFields(FIELDS, QUERY_MAP);
        assertFields(FIELDS, modifiedQuery);
    }

    @Test
    public void shouldAddNothingWhenAddingNullFields() {
        Map<String, String> modifiedQuery = modifier(QUERY_MAP)
                .addFields(new String[] { null }).toMap();
        assertFields(FIELDS, QUERY_MAP);
        assertFields(FIELDS, modifiedQuery);
    }

    @Test
    public void shouldRemoveFieldWhenPresent() {
        Map<String, String> modifiedQuery = modifier(QUERY_MAP).removeFields(FIELD_1, FIELD_3).toMap();
        assertFields(FIELDS, QUERY_MAP);
        assertFields(new String[] { FIELD_2 }, modifiedQuery);
    }

    @Test
    public void shouldIgnoreFieldWhenRemovingNonExistent() {
        Map<String, String> modifiedQuery = modifier(QUERY_MAP).removeFields(FIELD_4).toMap();
        assertFields(FIELDS, QUERY_MAP);
        assertFields(FIELDS, modifiedQuery);
    }

    @Test
    public void shouldRemoveAllFields() {
        Map<String, String> modifiedQuery = modifier(QUERY_MAP).removeAllFields().toMap();
        assertFields(new String[0], modifiedQuery);
    }

    @Test
    public void shouldAddEntity() throws IOException {
        Map<String, String> modifiedQuery = modifier(QUERY_MAP)
                .addEntity("entity_name", ImmutableMap.of(KEY_3, VALUE_3))
                .toMap();
        assertKeyValues(modifiedQuery, map -> map.containsKey("entity_name") &&
                map.containsValue(ImmutableMap.of(KEY_3, VALUE_3)));
    }

    @Test
    public void shouldThrowWhenAddingEntityWithNullMap() {
        expectedException.expect(NullPointerException.class);
        modifier(QUERY_MAP).addEntity("entity_name", null);
    }

    @Test
    public void shouldThrowWhenAddingEntityWithNullName() {
        expectedException.expect(NullPointerException.class);
        modifier(QUERY_MAP).addEntity(null, ImmutableMap.of("key", "value"));
    }

    @Test
    public void shouldRemoveAllEntities() throws IOException {
        Map<String, String> modifiedQuery = modifier(QUERY_MAP).removeAllEntities().toMap();
        assertKeyValues(modifiedQuery, Map::isEmpty);
    }

    @Test
    public void shouldAddAliasWhenAddingNonExistent() throws IOException {
        Map<String, List<String>> aliasMap = QueryFieldAliases.alias(ALIAS_3).fields(FIELD_4).build();
        Map<String, String> modifiedQuery = modifier(QUERY_MAP).addAliases(aliasMap).toMap();
        assertAliases(modifiedQuery, QueryFieldAliases
                .alias(ALIAS_1).fields(FIELD_1, FIELD_2)
                .alias(ALIAS_2).fields(FIELD_3)
                .alias(ALIAS_3).fields(FIELD_4).build()::equals);
    }

    @Test
    public void shouldReplaceAliasWhenAddingExistent() throws IOException {
        Map<String, List<String>> aliasMap = QueryFieldAliases.alias(ALIAS_1).fields(FIELD_1).build();
        Map<String, String> modifiedQuery = modifier(QUERY_MAP).addAliases(aliasMap).toMap();
        assertAliases(modifiedQuery, QueryFieldAliases
                .alias(ALIAS_1).fields(FIELD_1)
                .alias(ALIAS_2).fields(FIELD_3).build()::equals);
    }

    @Test
    public void shouldThrowWhenAddingAliasesWithNullMap() {
        expectedException.expect(NullPointerException.class);
        modifier(QUERY_MAP).addAliases(null);
    }

    @Test
    public void shouldThrowWhenAddingAliasesWithNullKey() {
        expectedException.expect(IllegalArgumentException.class);
        Map<String, List<String>> aliasMap = new HashMap<>();
        aliasMap.put(null, ImmutableList.of());
        modifier(QUERY_MAP).addAliases(aliasMap);
    }

    @Test
    public void shouldThrowWhenAddingAliasesWithNullValue() {
        expectedException.expect(IllegalArgumentException.class);
        Map<String, List<String>> aliasMap = new HashMap<>();
        aliasMap.put(ALIAS_1, null);
        modifier(QUERY_MAP).addAliases(aliasMap);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowWhenRemovingAliasesWithNullArray() {
        modifier(QUERY_MAP).removeAliases((String[]) null);
    }

    @Test
    public void shouldRemoveAliasesWhenPresent() throws IOException {
        Map<String, String> modifiedQuery = modifier(QUERY_MAP).removeAliases(ALIAS_1).toMap();
        assertAliases(modifiedQuery, QueryFieldAliases.alias(ALIAS_2).fields(FIELD_3).build()::equals);
    }

    @Test
    public void shouldIgnoreAliasesWhenRemovingNonExistent() throws IOException {
        Map<String, String> modifiedQuery = modifier(QUERY_MAP).removeAliases(ALIAS_3).toMap();
        assertAliases(modifiedQuery, ALIASES::equals);
    }

    @Test
    public void shouldRemoveAllAliases() throws IOException {
        Map<String, String> modifiedQuery = modifier(QUERY_MAP).removeAllAliases().toMap();
        assertAliases(modifiedQuery, Map::isEmpty);
    }

    public Object[][] wrongDateTimeStringFormats() {
        return new Object[][] {
                new Object[] { null, NullPointerException.class },
                new Object[] { "", DateTimeParseException.class },
                new Object[] { "SOMETHING", DateTimeParseException.class },
                new Object[] { "2017-09-13", DateTimeParseException.class },
                new Object[] { "14:33:50", DateTimeParseException.class },
                new Object[] { "2017-09-13 14:33:50", DateTimeParseException.class },
                new Object[] { "2017-09-13T14:33:50.123456789", DateTimeParseException.class }
        };
    }

    private StartTimeStage<KeyValuesQuery.KeyValueStage> builder(String system) {
        return KeyValuesQuery.builder(mock(SparkSession.class)).system(system);
    }

    private KeyValuesQuery.Modifier modifier(Map<String, String> query) {
        return KeyValuesQuery.modifier(mock(SparkSession.class), query);
    }
}
