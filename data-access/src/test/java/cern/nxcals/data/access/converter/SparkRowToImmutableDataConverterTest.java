package cern.nxcals.data.access.converter;

import cern.cmw.data.DataFactory;
import cern.cmw.data.DiscreteFunction;
import cern.cmw.datax.EntryType;
import cern.cmw.datax.ImmutableData;
import cern.cmw.datax.ImmutableEntry;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.Test;
import org.junit.runner.RunWith;
import scala.collection.mutable.WrappedArray;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

import static cern.nxcals.common.avro.SchemaConstants.ARRAY_DIMENSIONS_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.ARRAY_ELEMENTS_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.DDF_X_ARRAY_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.DDF_Y_ARRAY_FIELD_NAME;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

/**
 * Test suite for {@link SparkRowToImmutableDataConverter}.
 *
 * @author timartin
 */
@RunWith(JUnitParamsRunner.class)
public class SparkRowToImmutableDataConverterTest {

    private static final String TEST_NAME = "TEST_NAME";

    @Test
    public void testConvertNull() {
        assertThat(SparkRowToImmutableDataConverter.convert(null)).isNull();
    }

    @Test
    public void testEmptyRow() {
        Row row = mock(Row.class);
        StructType mockedStructType = mock(StructType.class);
        doReturn(mockedStructType).when(row).schema();
        doReturn(new StructField[0]).when(mockedStructType).fields();
        ImmutableData immutableData = SparkRowToImmutableDataConverter.convert(row);
        assertThat(immutableData.getEntries()).isEmpty();
    }

    @Test
    @Parameters(method = "allTypes")
    public void testDifferentDataTypes(Object value, DataType dataType, EntryType entryType) {
        Row row = mock(Row.class);
        mockRow(row, value, dataType, TEST_NAME);

        ImmutableData immutableData = SparkRowToImmutableDataConverter.convert(row);

        ImmutableEntry entry = immutableData.getEntry(TEST_NAME);
        assertThat(entry.get()).isEqualTo(value);
        assertThat(entry.getType()).isEqualTo(entryType);
    }

    @Test
    @Parameters(method = "allArrayTypes")
    public void testDifferentArrayDataTypes(Object value, DataType dataType, EntryType entryType) {
        Row row = mock(Row.class);
        mockArrayValue(row, value, dataType, TEST_NAME);

        ImmutableData immutableData = SparkRowToImmutableDataConverter.convert(row);

        ImmutableEntry entry = immutableData.getEntry(TEST_NAME);
        assertThat(entry.get()).isEqualTo(value);
        assertThat(entry.getType()).isEqualTo(entryType);
    }

    @Test
    public void testDiscreteFunctionalType() {
        Row row = mock(Row.class);
        DiscreteFunction discreteFunction = DataFactory
                .createDiscreteFunction(new double[] { 1, 2 }, new double[] { 3, 4 });
        mockDiscreteFunctionValue(row, discreteFunction, TEST_NAME);

        ImmutableData immutableData = SparkRowToImmutableDataConverter.convert(row);

        ImmutableEntry entry = immutableData.getEntry(TEST_NAME);
        assertThat(entry.get()).isEqualTo(discreteFunction);
        assertThat(entry.getType()).isEqualTo(EntryType.DISCRETE_FUNCTION);
    }

    @Test
    public void testDiscreteFunctionalArrayType() {
        Row row = mock(Row.class);
        DiscreteFunction[] discreteFunctions = new DiscreteFunction[] {
                DataFactory.createDiscreteFunction(new double[] { 0, 1 }, new double[] { 2, 3 }),
                DataFactory.createDiscreteFunction(new double[] { 4, 5 }, new double[] { 6, 7 })
        };
        mockDiscreteFunctionArrayValue(row, discreteFunctions, TEST_NAME);

        ImmutableData immutableData = SparkRowToImmutableDataConverter.convert(row);

        ImmutableEntry entry = immutableData.getEntry(TEST_NAME);
        assertThat(entry.get()).isEqualTo(discreteFunctions);
        assertThat(entry.getType()).isEqualTo(EntryType.DISCRETE_FUNCTION_ARRAY);
    }

    @Test
    @Parameters(method = "allTypes")
    public void testImmutableDataWithScalars(Object value, DataType dataType, EntryType entryType) {
        Row row = mock(Row.class);
        Row nestedRow = mock(Row.class);
        ImmutableData immutableData = ImmutableData.builder()
                .add(TEST_NAME, ImmutableData.builder()
                        .add(TEST_NAME, value)
                        .build())
                .build();

        mockRow(nestedRow, value, dataType, TEST_NAME);
        mockRow(row, nestedRow, new StructType(new StructField[] { new StructField(TEST_NAME, dataType, true, null) }),
                TEST_NAME);

        ImmutableData convertedImmutableData = SparkRowToImmutableDataConverter.convert(row);

        assertThat(convertedImmutableData).isEqualTo(immutableData);
    }

    @Test
    @Parameters(method = "allArrayTypes")
    public void testImmutableDataWithArrays(Object value, DataType dataType, EntryType entryType) {
        Row row = mock(Row.class);
        Row nestedRow = mock(Row.class);
        ImmutableData immutableData = ImmutableData.builder()
                .add(TEST_NAME, ImmutableData.builder()
                        .add(TEST_NAME, SparkRowToImmutableDataConverter.tryGetPrimitiveValue(value))
                        .build())
                .build();

        mockArrayValue(nestedRow, value, dataType, TEST_NAME);
        mockRow(row, nestedRow, new StructType(new StructField[] { new StructField(TEST_NAME, dataType, true, null) }),
                TEST_NAME);

        ImmutableData convertedImmutableData = SparkRowToImmutableDataConverter.convert(row);

        assertThat(convertedImmutableData).isEqualTo(immutableData);
    }

    @Test
    @Parameters(method = "allTypes")
    public void testImmutableDataWithMultiLevelNesting(Object value, DataType dataType, EntryType entryType) {
        Row row = mock(Row.class);
        Row nestedRow = mock(Row.class);
        Row nestedNestedRow = mock(Row.class);
        ImmutableData immutableData = ImmutableData.builder()
                .add(TEST_NAME, ImmutableData.builder()
                        .add(TEST_NAME, ImmutableData.builder()
                                .add(TEST_NAME, SparkRowToImmutableDataConverter.tryGetPrimitiveValue(value))
                                .build())
                        .build())
                .build();

        mockRow(nestedNestedRow, value, dataType, TEST_NAME);
        StructType nestedStructType = new StructType(
                new StructField[] { new StructField(TEST_NAME, dataType, true, null) });
        mockRow(nestedRow, nestedNestedRow, nestedStructType, TEST_NAME);
        StructType structType = new StructType(
                new StructField[] { new StructField(TEST_NAME, nestedStructType, true, null) });
        mockRow(row, nestedRow, structType, TEST_NAME);

        ImmutableData convertedImmutableData = SparkRowToImmutableDataConverter.convert(row);

        assertThat(convertedImmutableData).isEqualTo(immutableData);
    }

    @Test
    @Parameters(method = "allArrayTypes")
    public void testImmutableDataWithMultiLevelNestingArray(Object value, DataType dataType, EntryType entryType) {
        Row row = mock(Row.class);
        Row nestedRow = mock(Row.class);
        Row nestedNestedRow = mock(Row.class);
        ImmutableData immutableData = ImmutableData.builder()
                .add(TEST_NAME, ImmutableData.builder()
                        .add(TEST_NAME, ImmutableData.builder()
                                .add(TEST_NAME, SparkRowToImmutableDataConverter.tryGetPrimitiveValue(value))
                                .build())
                        .build())
                .build();

        mockArrayValue(nestedNestedRow, value, dataType, TEST_NAME);
        StructType nestedStructType = new StructType(
                new StructField[] { new StructField(TEST_NAME, dataType, true, null) });
        mockRow(nestedRow, nestedNestedRow, nestedStructType, TEST_NAME);
        StructType structType = new StructType(
                new StructField[] { new StructField(TEST_NAME, nestedStructType, true, null) });
        mockRow(row, nestedRow, structType, TEST_NAME);

        ImmutableData convertedImmutableData = SparkRowToImmutableDataConverter.convert(row);

        assertThat(convertedImmutableData).isEqualTo(immutableData);
    }

    private void mockDiscreteFunctionArrayValue(Row source, DiscreteFunction[] discreteFunctions, String name) {
        final List<Row> elements = Arrays.stream(discreteFunctions).map(discreteFunction -> {
            Row nestedMockedRow = mock(Row.class);
            WrappedArray mockedWrappedXArray = mock(WrappedArray.class);
            WrappedArray mockedWrappedYArray = mock(WrappedArray.class);
            doReturn(mockedWrappedXArray).when(nestedMockedRow).getAs(DDF_X_ARRAY_FIELD_NAME);
            doReturn(mockedWrappedYArray).when(nestedMockedRow).getAs(DDF_Y_ARRAY_FIELD_NAME);
            doReturn(discreteFunction.getXArray()).when(mockedWrappedXArray).array();
            doReturn(discreteFunction.getYArray()).when(mockedWrappedYArray).array();
            mockSchema(nestedMockedRow, createDiscreteFunctionSchema(), name);
            return nestedMockedRow;
        }).collect(toList());

        mockArrayValue(source, elements.toArray(new Row[elements.size()]), createDiscreteFunctionSchema(), TEST_NAME);
    }

    private Row mockDiscreteFunctionValue(Row source, DiscreteFunction discreteFunction, String name) {
        Row mockedRow = mock(Row.class);
        WrappedArray mockedWrappedXArray = mock(WrappedArray.class);
        WrappedArray mockedWrappedYArray = mock(WrappedArray.class);
        doReturn(mockedRow).when(source).getAs(name);
        doReturn(mockedWrappedXArray).when(mockedRow).getAs(DDF_X_ARRAY_FIELD_NAME);
        doReturn(mockedWrappedYArray).when(mockedRow).getAs(DDF_Y_ARRAY_FIELD_NAME);
        doReturn(discreteFunction.getXArray()).when(mockedWrappedXArray).array();
        doReturn(discreteFunction.getYArray()).when(mockedWrappedYArray).array();
        mockSchema(source, createDiscreteFunctionSchema(), name);
        return mockedRow;
    }

    private StructType createDiscreteFunctionSchema() {
        StructField xArrayField = new StructField(DDF_X_ARRAY_FIELD_NAME,
                DataTypes.createArrayType(DataTypes.DoubleType), true, null);
        StructField yArrayField = new StructField(DDF_Y_ARRAY_FIELD_NAME,
                DataTypes.createArrayType(DataTypes.DoubleType), true, null);
        return new StructType(new StructField[] { xArrayField, yArrayField });
    }

    private Row mockArrayValue(Row source, Object value, DataType dataType, String name) {
        Row mockedRow = mock(Row.class);
        WrappedArray elementsWrappedArray = mock(WrappedArray.class);
        WrappedArray dimensionsWrappedArray = mock(WrappedArray.class);

        doReturn(mockedRow).when(source).getAs(name);
        doReturn(value).when(elementsWrappedArray).array();
        doReturn(new Integer[] { Array.getLength(value) }).when(dimensionsWrappedArray).array();
        doReturn(elementsWrappedArray).when(mockedRow).getAs(ARRAY_ELEMENTS_FIELD_NAME);
        doReturn(dimensionsWrappedArray).when(mockedRow).getAs(ARRAY_DIMENSIONS_FIELD_NAME);
        mockSchema(source, createArraySchema(dataType), name);

        return mockedRow;
    }

    private StructType createArraySchema(DataType dataType) {
        StructField elementsField = new StructField(ARRAY_ELEMENTS_FIELD_NAME, DataTypes.createArrayType(dataType),
                true, null);
        StructField dimensionsField = new StructField(ARRAY_DIMENSIONS_FIELD_NAME,
                DataTypes.createArrayType(DataTypes.IntegerType), true, null);
        return new StructType(new StructField[] { elementsField, dimensionsField });
    }

    private void mockRow(Row source, Object value, DataType dataType, String name) {
        doReturn(value).when(source).getAs(name);
        mockSchema(source, dataType, name);
    }

    private StructField mockSchema(Row source, DataType dataType, String name) {
        StructType mockedStructType = mock(StructType.class);
        StructField structField = new StructField(name, dataType, true, null);
        doReturn(new StructField[] { structField }).when(mockedStructType).fields();
        doReturn(mockedStructType).when(source).schema();
        return structField;
    }

    public Object[][] allTypes() {
        return new Object[][] {
                new Object[] { new Integer(2), DataTypes.IntegerType, EntryType.INT32 },
                new Object[] { 2, DataTypes.IntegerType, EntryType.INT32 },
                new Object[] { new Double(2), DataTypes.DoubleType, EntryType.DOUBLE },
                new Object[] { 2D, DataTypes.DoubleType, EntryType.DOUBLE },
                new Object[] { new Long(2), DataTypes.LongType, EntryType.INT64 },
                new Object[] { 2L, DataTypes.LongType, EntryType.INT64 },
                new Object[] { new Float(2), DataTypes.FloatType, EntryType.FLOAT },
                new Object[] { 2F, DataTypes.FloatType, EntryType.FLOAT },
                new Object[] { new Boolean(true), DataTypes.BooleanType, EntryType.BOOL },
                new Object[] { true, DataTypes.BooleanType, EntryType.BOOL },
                new Object[] { new Byte((byte) 'a'), DataTypes.ByteType, EntryType.INT8 },
                new Object[] { (byte) 'a', DataTypes.ByteType, EntryType.INT8 },
                new Object[] { "string", DataTypes.StringType, EntryType.STRING }
        };
    }

    public Object[][] allArrayTypes() {
        return new Object[][] {
                new Object[] { new Integer[] { 2, 3 }, DataTypes.IntegerType, EntryType.INT32_ARRAY },
                new Object[] { new int[] { 2, 3 }, DataTypes.IntegerType, EntryType.INT32_ARRAY },
                new Object[] { new Long[] { 2L, 3L }, DataTypes.LongType, EntryType.INT64_ARRAY },
                new Object[] { new long[] { 2L, 3L }, DataTypes.LongType, EntryType.INT64_ARRAY },
                new Object[] { new Float[] { 2F, 3F }, DataTypes.FloatType, EntryType.FLOAT_ARRAY },
                new Object[] { new float[] { 2F, 3F }, DataTypes.FloatType, EntryType.FLOAT_ARRAY },
                new Object[] { new Boolean[] { true, false }, DataTypes.BooleanType, EntryType.BOOL_ARRAY },
                new Object[] { new boolean[] { true, false }, DataTypes.BooleanType, EntryType.BOOL_ARRAY },
                new Object[] { new Byte[] { 'a', 'b' }, DataTypes.ByteType, EntryType.INT8_ARRAY },
                new Object[] { new byte[] { 'a', 'b' }, DataTypes.ByteType, EntryType.INT8_ARRAY },
                new Object[] { new String[] { "1", "2" }, DataTypes.StringType, EntryType.STRING_ARRAY }
        };
    }
}
