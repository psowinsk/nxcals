package cern.nxcals.data.access.builders;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Test suite for {@link QueryFieldAliases}.
 *
 * @author ntsvetko
 */
public class QueryFieldAliasesTest {

    @Test
    public void shouldGetAliasWithFields() {
        //given
        String alias = "test_alias";
        String field1 = "Field1";
        String field2 = "Field2";

        //when
        Map<String, List<String>> result = QueryFieldAliases.alias(alias).fields(field1, field2).build();

        //then
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());
        Assert.assertEquals(Arrays.asList(field1, field2), result.get(alias));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCreateAliasWithoutFields() {
        //given
        String alias = "alias";

        //when
        QueryFieldAliases.alias(alias).fields().build();
    }

    @Test(expected = NullPointerException.class)
    public void shouldNotCreateWithEmptyAlias() {
        //given
        String field1 = "Field1";

        //when
        QueryFieldAliases.alias(null).fields(field1).build();
    }

    @Test(expected = NullPointerException.class)
    public void shouldNotCreateWithEmptyAliasInSubsequentCalls() {
        //given
        String alias1 = "alias1";
        String field1 = "Field1";

        String alias2 = null;
        String field2 = "field2";

        //when
        QueryFieldAliases
                .alias(alias1).fields(field1)
                .alias(alias2).fields(field2)
                .build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCreateWithNullFieldsInSubsequentCalls() {
        //given
        String alias1 = "alias1";
        String field1 = "Field1";

        String alias2 = "alias2";
        String field2 = null;

        //when
        QueryFieldAliases
                .alias(alias1).fields(field1)
                .alias(alias2).fields(field2)
                .build();
    }


    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCreateWithEmptyFieldsInSubsequentCalls() {
        //given
        String alias1 = "alias1";
        String field1 = "Field1";

        String alias2 = "alias2";

        //when
        QueryFieldAliases
                .alias(alias1).fields(field1)
                .alias(alias2).fields()
                .build();
    }
}
