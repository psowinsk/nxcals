/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.data.access.api;

import cern.nxcals.common.domain.EntityResources;
import cern.nxcals.common.domain.SystemData;
import cern.nxcals.common.utils.TimeUtils;
import cern.nxcals.data.access.BaseTest;
import cern.nxcals.service.client.api.internal.InternalEntitiesResourcesService;
import cern.nxcals.service.client.api.internal.InternalSystemService;
import cern.nxcals.service.client.api.internal.InternalVariableService;
import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import static cern.nxcals.data.access.Constants.ALIASES_KEY;
import static cern.nxcals.data.access.Constants.END_TIME_KEY;
import static cern.nxcals.data.access.Constants.KEY_VALUES_KEY;
import static cern.nxcals.data.access.Constants.START_TIME_KEY;
import static cern.nxcals.data.access.Constants.SYSTEM_KEY;
import static cern.nxcals.data.access.Constants.VARIABLE_KEY;
import static cern.nxcals.data.access.api.QueryDataServiceImpl.DATE_CORRECTNESS_ERROR_MESSAGE_FORMAT;
import static cern.nxcals.data.access.api.QueryDataServiceImpl.NO_SYSTEM_FOUND_ERROR_MESSAGE_FORMAT;
import static cern.nxcals.data.access.api.QueryDataServiceImpl.SCHEMA_MISMATCH_ERROR_MESSAGE_FORMAT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;

public class QueryDataServiceImplTest extends BaseTest {

    private static final String FIELD_NAME = "newField";
    private static final String FIELD_VALUE = "fieldValue";
    private static final Instant TIME_BEFORE_YEAR_2000 = Instant.ofEpochSecond(915148800);
    private static final Instant TIME_1 = Instant.now();
    private static final Instant TIME_2 = TIME_1.plus(1, ChronoUnit.DAYS);

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Mock
    private InternalSystemService systemServiceMock;
    @Mock
    private InternalVariableService variableService;
    @Mock
    private InternalEntitiesResourcesService entityService;
    @InjectMocks
    private QueryDataServiceImpl queryDataService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldCreateQueryDataFromDevPropKeyValueBuilder() throws IOException {
        //given
        SystemData systemData = getTestDevPropSystemData();
        Set<EntityResources> entityResources = getTestEntitiesResources(systemData);

        String deviceName = "DEVICE";
        String propertyName = "PROPERTY";
        String startTime = String.valueOf(TimeUtils.getNanosFromInstant(TIME_1));
        String endTime = String.valueOf(TimeUtils.getNanosFromInstant(TIME_2));

        Map<String, String> query = new HashMap<>();
        query.put(SYSTEM_KEY, systemData.getName());
        query.put(KEY_VALUES_KEY,
                getKeyValuesValueFor(ImmutableMap.of("entity_name", ImmutableMap.of("device", deviceName, "property",
                        propertyName))));
        query.put(START_TIME_KEY, startTime);
        query.put(END_TIME_KEY, endTime);

        //when
        Mockito.when(systemServiceMock.findByName(systemData.getName())).thenReturn(systemData);
        Mockito.when(entityService.findBySystemIdKeyValuesAndTimeWindow(anyLong(), anyMap(), anyLong(), anyLong()))
                .thenReturn(entityResources);

        Set<InternalQueryData> queryData = queryDataService.loadQueryData(query);

        //then
        Assert.assertNotNull(queryData);
        assertEquals(1, queryData.size());
        InternalQueryData internalQueryData = queryData.iterator().next();
        assertEquals(internalQueryData.getEntityData(), entityResources.stream().findFirst().get());
        assertEquals(internalQueryData.getEntityData().getSystemData(), systemData);
        assertEquals(internalQueryData.getStartTime(), TIME_1);
        assertEquals(internalQueryData.getEndTime(), TIME_2);
        assertNull(internalQueryData.getVariableName());
        // expect endTime have one field (NXCALS_TIMESTAMP) added by the process
        assertNotNull(internalQueryData.getFields());
    }

    @Test
    public void shouldCreateQueryDataWithAliasesFromDevPropKeyValueBuilder() throws IOException {
        //given
        SystemData systemData = getTestDevPropSystemData();
        Set<EntityResources> entityResources = getTestEntitiesResources(systemData);

        String deviceName = "DEVICE";
        String propertyName = "PROPERTY";
        String startTime = String.valueOf(TimeUtils.getNanosFromInstant(TIME_1));
        String endTime = String.valueOf(TimeUtils.getNanosFromInstant(TIME_2));
        String alias = "{\"alias\":[\"int_field\",\"long_field\"]}";

        Map<String, String> query = new HashMap<>();
        query.put(SYSTEM_KEY, systemData.getName());
        query.put(KEY_VALUES_KEY, getKeyValuesValueFor(
                ImmutableMap.of("entity_name", ImmutableMap.of("device", deviceName, "property", propertyName))));
        query.put(START_TIME_KEY, startTime);
        query.put(END_TIME_KEY, endTime);
        query.put(ALIASES_KEY, alias);

        //when
        Mockito.when(systemServiceMock.findByName(systemData.getName())).thenReturn(systemData);
        Mockito.when(entityService.findBySystemIdKeyValuesAndTimeWindow(anyLong(), anyMap(), anyLong(), anyLong()))
                .thenReturn(entityResources);

        Set<InternalQueryData> queryData = queryDataService.loadQueryData(query);

        //then
        Assert.assertNotNull(queryData);
        assertEquals(1, queryData.size());
        InternalQueryData internalQueryData = queryData.iterator().next();
        assertEquals(internalQueryData.getEntityData(), entityResources.stream().findFirst().get());
        assertEquals(internalQueryData.getEntityData().getSystemData(), systemData);
        assertEquals(internalQueryData.getStartTime(), TIME_1);
        assertEquals(internalQueryData.getEndTime(), TIME_2);
        assertNull(internalQueryData.getVariableName());
        assertEquals(12, internalQueryData.getFields().size());

        // If more than one field specified to use a given alias exist in the same schema, then we use the field with the
        // highest priority based on the position in the fields list
        assertTrue(internalQueryData.getFields().stream()
                .map(DataAccessField::getAlias)
                .filter(Objects::nonNull)
                .anyMatch("alias"::equals));

        assertTrue(internalQueryData.getFields().stream()
                .filter(field -> Objects.nonNull(field.getAlias()))
                .filter(field -> "alias".equals(field.getAlias()))
                .anyMatch(field -> "int_field".equals(field.getFieldName())));
    }

    @Test
    public void shouldCreateQueryDataFromVariablesBuilder() {
        //given
        SystemData systemData = getTestDevPropSystemData();
        Set<EntityResources> entityResources = getTestEntitiesResources(systemData);

        String variableName = "VARIABLE";
        String startTime = String.valueOf(TimeUtils.getNanosFromInstant(TIME_1));
        String endTime = String.valueOf(TimeUtils.getNanosFromInstant(TIME_2));

        Map<String, String> query = new HashMap<>();
        query.put(SYSTEM_KEY, systemData.getName());
        query.put(VARIABLE_KEY, variableName);
        query.put(START_TIME_KEY, startTime);
        query.put(END_TIME_KEY, endTime);

        //then
        Mockito.when(systemServiceMock.findByName(systemData.getName())).thenReturn(systemData);
        Mockito.when(entityService.findByEntityIdAndTimeWindow(eq(1L), eq(Long.valueOf(startTime)),
                eq(Long.valueOf(endTime)))).thenReturn(entityResources);
        Mockito.when(variableService.findByVariableNameAndTimeWindow(eq(variableName), eq(Long.valueOf(startTime)),
                eq(Long.valueOf(endTime)))).thenReturn(getTestVariableDataFullEntity());
        Set<InternalQueryData> queryData = queryDataService.loadQueryData(query);

        //then
        Assert.assertNotNull(queryData);
        assertEquals(1, queryData.size());
        InternalQueryData internalQueryData = queryData.iterator().next();
        assertEquals(internalQueryData.getEntityData(), entityResources.stream().findFirst().get());
        assertEquals(internalQueryData.getEntityData().getSystemData(), systemData);
        assertEquals(internalQueryData.getStartTime(), TIME_1);
        assertEquals(internalQueryData.getEndTime(), TIME_2);
        assertNotNull(internalQueryData.getVariableName());
    }

    @Test
    public void shouldCreateQueryDataFromVariablesBuilderWithField() {
        //given
        SystemData systemData = getTestDevPropSystemData();
        Set<EntityResources> entityResources = getTestEntitiesResources(systemData);

        String variableName = "VARIABLE";
        String startTime = String.valueOf(TimeUtils.getNanosFromInstant(TIME_1));
        String endTime = String.valueOf(TimeUtils.getNanosFromInstant(TIME_2));

        Map<String, String> query = new HashMap<>();
        query.put(SYSTEM_KEY, systemData.getName());
        query.put(VARIABLE_KEY, variableName);
        query.put(START_TIME_KEY, startTime);
        query.put(END_TIME_KEY, endTime);

        //then
        Mockito.when(systemServiceMock.findByName(systemData.getName())).thenReturn(systemData);
        Mockito.when(entityService.findByEntityIdAndTimeWindow(eq(1L), eq(Long.valueOf(startTime)),
                eq(Long.valueOf(endTime)))).thenReturn(entityResources);
        Mockito.when(variableService.findByVariableNameAndTimeWindow(eq(variableName), eq(Long.valueOf(startTime)),
                eq(Long.valueOf(endTime)))).thenReturn(getTestVariableDataWithField());
        Set<InternalQueryData> queryData = queryDataService.loadQueryData(query);

        //then
        Assert.assertNotNull(queryData);
        assertEquals(1, queryData.size());
        InternalQueryData internalQueryData = queryData.iterator().next();
        assertEquals(internalQueryData.getEntityData(), entityResources.stream().findFirst().get());
        assertEquals(internalQueryData.getEntityData().getSystemData(), systemData);
        assertEquals(internalQueryData.getStartTime(), TIME_1);
        assertEquals(internalQueryData.getEndTime(), TIME_2);
        assertNotNull(internalQueryData.getVariableName());
        // expect endTime have one field (NXCALS_TIMESTAMP) added by the process
        assertEquals(4, internalQueryData.getFields().size());
    }

    @Test
    public void shouldCreateQueryDataFromKeyValueBuilder() throws IOException {
        //given
        SystemData systemData = getTestElementSystemData();
        Set<EntityResources> entityResources = getTestEntitiesResources(systemData);

        String elementName = "element";
        String startTime = String.valueOf(TimeUtils.getNanosFromInstant(TIME_1));
        String endTime = String.valueOf(TimeUtils.getNanosFromInstant(TIME_2));

        Map<String, String> query = new HashMap<>();
        query.put(SYSTEM_KEY, systemData.getName());
        query.put(KEY_VALUES_KEY,
                getKeyValuesValueFor(ImmutableMap.of("entity_name", ImmutableMap.of(elementName, elementName))));
        query.put(START_TIME_KEY, startTime);
        query.put(END_TIME_KEY, endTime);
        query.put(elementName, elementName);

        //when
        Mockito.when(systemServiceMock.findByName(systemData.getName())).thenReturn(systemData);
        Mockito.when(entityService.findBySystemIdKeyValuesAndTimeWindow(anyLong(), anyMap(), anyLong(), anyLong()))
                .thenReturn(entityResources);

        Set<InternalQueryData> queryData = queryDataService.loadQueryData(query);

        //then
        Assert.assertNotNull(queryData);
        assertEquals(1, queryData.size());
        InternalQueryData internalQueryData = queryData.iterator().next();
        assertEquals(internalQueryData.getEntityData(), entityResources.stream().findFirst().get());
        assertEquals(internalQueryData.getEntityData().getSystemData(), systemData);
        assertEquals(internalQueryData.getStartTime(), TIME_1);
        assertEquals(internalQueryData.getEndTime(), TIME_2);
        assertNull(internalQueryData.getVariableName());
        // expect to have one field (NXCALS_TIMESTAMP) added by the process
        assertNotNull(internalQueryData.getFields());
    }

    @Test
    public void shouldNotCreateQueryDataFromNonEpochTimestampInNanos() {
        //given
        SystemData systemData = getTestElementSystemData();
        Set<EntityResources> entityResources = getTestEntitiesResources(systemData);

        String startTime = String.valueOf(TimeUtils.getNanosFromInstant(TIME_BEFORE_YEAR_2000));
        String endTime = String.valueOf(TimeUtils.getNanosFromInstant(TIME_1));

        Map<String, String> query = new HashMap<>();
        query.put(SYSTEM_KEY, systemData.getName());
        query.put(START_TIME_KEY, startTime);
        query.put(END_TIME_KEY, endTime);

        expectedException.expect(IllegalArgumentException.class);
        expectedException
                .expectMessage(String.format(DATE_CORRECTNESS_ERROR_MESSAGE_FORMAT, START_TIME_KEY, startTime));

        //when
        Mockito.when(systemServiceMock.findByName(systemData.getName())).thenReturn(systemData);
        Mockito.when(entityService.findBySystemIdKeyValuesAndTimeWindow(anyLong(), anyMap(), anyLong(), anyLong()))
                .thenReturn(entityResources);

        Set<InternalQueryData> queryData = queryDataService.loadQueryData(query);
    }

    @Test
    public void shouldNotCreateQueryDataFromValueNotConvertableToLong() {
        //given
        SystemData systemData = getTestElementSystemData();
        Set<EntityResources> entityResources = getTestEntitiesResources(systemData);

        String startTime = "timestamp";
        String endTime = String.valueOf(TimeUtils.getNanosFromInstant(TIME_1));

        Map<String, String> query = new HashMap<>();
        query.put(SYSTEM_KEY, systemData.getName());
        query.put(START_TIME_KEY, startTime);
        query.put(END_TIME_KEY, endTime);

        expectedException.expect(NumberFormatException.class);
        expectedException.expectMessage("For input string: \"timestamp\"");

        //when
        Mockito.when(systemServiceMock.findByName(systemData.getName())).thenReturn(systemData);
        Mockito.when(entityService.findBySystemIdKeyValuesAndTimeWindow(anyLong(), anyMap(), anyLong(), anyLong()))
                .thenReturn(entityResources);

        Set<InternalQueryData> queryData = queryDataService.loadQueryData(query);
    }

    @Test
    public void shouldNotCreateQueryDataFromNonMatchingSystem() throws IOException {
        //given
        SystemData systemData = getTestElementSystemData();
        Set<EntityResources> entityResources = getTestEntitiesResources(systemData);

        String startTime = String.valueOf(TimeUtils.getNanosFromInstant(TIME_1));
        String endTime = String.valueOf(TimeUtils.getNanosFromInstant(TIME_2));

        Map<String, String> query = new HashMap<>();
        query.put(SYSTEM_KEY, systemData.getName());
        query.put(START_TIME_KEY, startTime);
        query.put(END_TIME_KEY, endTime);
        query.put(KEY_VALUES_KEY,
                getKeyValuesValueFor(ImmutableMap.of("entity_name", ImmutableMap.of(FIELD_NAME, FIELD_VALUE))));

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(String.format(SCHEMA_MISMATCH_ERROR_MESSAGE_FORMAT, FIELD_NAME, FIELD_VALUE,
                systemData.getName()));

        //when
        Mockito.when(systemServiceMock.findByName(systemData.getName())).thenReturn(systemData);
        Mockito.when(entityService.findBySystemIdKeyValuesAndTimeWindow(anyLong(), anyMap(), anyLong(), anyLong()))
                .thenReturn(entityResources);

        Set<InternalQueryData> queryData = queryDataService.loadQueryData(query);

    }

    @Test
    public void shouldNotCreateQueryDataWithoutSystemName() {
        //given
        SystemData systemData = getTestElementSystemData();
        Set<EntityResources> entityResources = getTestEntitiesResources(systemData);

        String startTime = String.valueOf(TimeUtils.getNanosFromInstant(TIME_1));
        String endTime = String.valueOf(TimeUtils.getNanosFromInstant(TIME_2));

        Map<String, String> query = new HashMap<>();
        query.put(START_TIME_KEY, startTime);
        query.put(END_TIME_KEY, endTime);

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(NO_SYSTEM_FOUND_ERROR_MESSAGE_FORMAT);

        //when
        Mockito.when(systemServiceMock.findByName(systemData.getName())).thenReturn(systemData);
        Mockito.when(entityService.findBySystemIdKeyValuesAndTimeWindow(anyLong(), anyMap(), anyLong(), anyLong()))
                .thenReturn(entityResources);

        Set<InternalQueryData> queryData = queryDataService.loadQueryData(query);
    }

    @Test
    public void shouldNotCreateQueryDataWithoutStartTime() {
        //given
        SystemData systemData = getTestElementSystemData();
        Set<EntityResources> entityResources = getTestEntitiesResources(systemData);

        String endTime = String.valueOf(TimeUtils.getNanosFromInstant(TIME_1));

        Map<String, String> query = new HashMap<>();
        query.put(SYSTEM_KEY, systemData.getName());
        query.put(END_TIME_KEY, endTime);

        expectedException.expect(NumberFormatException.class);
        expectedException.expectMessage("null");

        //when
        Mockito.when(systemServiceMock.findByName(systemData.getName())).thenReturn(systemData);
        Mockito.when(entityService.findBySystemIdKeyValuesAndTimeWindow(anyLong(), anyMap(), anyLong(), anyLong()))
                .thenReturn(entityResources);

        Set<InternalQueryData> queryData = queryDataService.loadQueryData(query);
    }

    @Test
    public void shouldNotCreateQueryDataWithoutEndTime() {
        //given
        SystemData systemData = getTestElementSystemData();
        Set<EntityResources> entityResources = getTestEntitiesResources(systemData);
        String startTime = String.valueOf(TimeUtils.getNanosFromInstant(TIME_1));

        Map<String, String> query = new HashMap<>();
        query.put(SYSTEM_KEY, systemData.getName());
        query.put(START_TIME_KEY, startTime);

        expectedException.expect(NumberFormatException.class);
        expectedException.expectMessage("null");

        //when
        Mockito.when(systemServiceMock.findByName(systemData.getName())).thenReturn(systemData);
        Mockito.when(entityService.findBySystemIdKeyValuesAndTimeWindow(anyLong(), anyMap(), anyLong(), anyLong()))
                .thenReturn(entityResources);

        Set<InternalQueryData> queryData = queryDataService.loadQueryData(query);
    }
}