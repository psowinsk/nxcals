package cern.nxcals.data.access.builders;

import cern.nxcals.data.access.builders.fluent.stages.BuildStage;
import cern.nxcals.data.access.builders.fluent.stages.StartTimeStage;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.spark.sql.SparkSession;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cern.nxcals.data.access.builders.DevicePropertyQuery.DEVICE_KEY;
import static cern.nxcals.data.access.builders.DevicePropertyQuery.PROPERTY_KEY;
import static cern.nxcals.data.access.builders.QueryAssertions.assertAliases;
import static cern.nxcals.data.access.builders.QueryAssertions.assertEndTime;
import static cern.nxcals.data.access.builders.QueryAssertions.assertFields;
import static cern.nxcals.data.access.builders.QueryAssertions.assertKeyValues;
import static cern.nxcals.data.access.builders.QueryAssertions.assertStartTime;

import static org.mockito.Mockito.mock;

/**
 * Test suite for {@link DevicePropertyQuery}.
 */
@RunWith(JUnitParamsRunner.class)
public class DevicePropertyQueryTest {
    private static final String SYSTEM = "SYSTEM";
    private static final String DEVICE_1 = "DEVICE_1.SAMPLE";
    private static final String DEVICE_2 = "DEVICE_2.SAMPLE";
    private static final String PROPERTY_1 = "PROPERTY_1.SAMPLE";
    private static final String PROPERTY_2 = "PROPERTY_2.SAMPLE";
    private static final String PARAMETER_1 = DEVICE_1 + "/" + PROPERTY_1;
    private static final String PARAMETER_2 = DEVICE_2 + "/" + PROPERTY_2;
    private static final Instant TIME_1 = Instant.now();
    private static final Instant TIME_2 = Instant.now();

    private static final String FIELD_1 = "FIELD_1";
    private static final String FIELD_2 = "FIELD_2";
    private static final String FIELD_3 = "FIELD_3";
    private static final String FIELD_4 = "FIELD_4";
    private static final String[] FIELDS = new String[] { FIELD_1, FIELD_2, FIELD_3 };

    private static final String ALIAS_1 = "ALIAS_1";
    private static final String ALIAS_2 = "ALIAS_2";
    private static final String ALIAS_3 = "ALIAS_3";

    private static final Map<String, List<String>> ALIASES = QueryFieldAliases
            .alias(ALIAS_1).fields(FIELD_1, FIELD_2)
            .alias(ALIAS_2).fields(FIELD_3).build();

    private static final Map<String, String> KEY_VALUES = ImmutableMap
            .of(DEVICE_KEY, DEVICE_1, PROPERTY_KEY, PROPERTY_1);

    private static final Map<String, String> QUERY_MAP = toMap(DevicePropertyQuery.builder(mock(SparkSession.class))
            .system(SYSTEM)
            .startTime(TIME_1).endTime(TIME_2)
            .fields(FIELDS)
            .fieldAliases(ALIASES)
            .entity()
            .parameter(PARAMETER_1));

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private static Map<String, String> toMap(BuildStage<?> builder) {
        return ((DevicePropertyQuery.Internal) builder).toMap();
    }

    @Test
    public void shouldThrowWhenSystemIsNull() {
        expectedException.expect(NullPointerException.class);
        builder(null);
    }

    @Test
    public void shouldThrowWhenParameterIsNull() {
        expectedException.expect(NullPointerException.class);
        builder(SYSTEM)
                .startTime(TIME_1)
                .endTime(TIME_2)
                .entity()
                .parameter(null);
    }

    @Test
    @Parameters(method = "wrongParameterFormattedStrings")
    public void shouldThrowWhenParameterHasWrongFormat(String parameter) {
        expectedException.expect(IllegalArgumentException.class);
        builder(SYSTEM)
                .startTime(TIME_1)
                .endTime(TIME_2)
                .entity()
                .parameter(parameter);
    }

    @Test
    public void shouldThrowWhenDeviceIsNull() {
        expectedException.expect(NullPointerException.class);
        builder(SYSTEM)
                .startTime(TIME_1)
                .endTime(TIME_2)
                .entity()
                .device(null);
    }

    @Test
    public void shouldThrowWhenPropertyIsNull() {
        expectedException.expect(NullPointerException.class);
        builder(SYSTEM)
                .startTime(TIME_1)
                .endTime(TIME_2)
                .entity()
                .device(DEVICE_1).property(null);
    }

    @Test
    public void shouldCreateDevicePropertyQuery() throws IOException {
        //when
        Map<String, String> query = toMap(builder(SYSTEM)
                .startTime(TIME_1)
                .endTime(TIME_2)
                .entity()
                .device(DEVICE_1).property(PROPERTY_1));

        //then
        Assert.assertNotNull(query);
        Assert.assertEquals(6, query.size());
        assertKeyValues(query, map -> map.containsValue(KEY_VALUES));
        assertStartTime(TIME_1, query);
        assertEndTime(TIME_2, query);
    }

    @Test
    public void shouldThrowWhenModifyWithNull() {
        expectedException.expect(NullPointerException.class);
        modifier(null);
    }

    @Test
    public void shouldThrowWhenModifyDeviceWithNull() {
        expectedException.expect(NullPointerException.class);
        modifier(QUERY_MAP).deviceProperty(null, PROPERTY_1);
    }

    @Test
    public void shouldModifyDevice() throws IOException {
        Map<String, String> modifiedQuery = modifier(QUERY_MAP)
                .deviceProperty(DEVICE_2, PROPERTY_1)
                .toMap();

        assertKeyValues(modifiedQuery,
                map -> map.containsValue(ImmutableMap.of(DEVICE_KEY, DEVICE_2, PROPERTY_KEY, PROPERTY_1)));
    }

    @Test
    public void shouldThrowWhenModifyPropertyWithNull() {
        expectedException.expect(NullPointerException.class);
        modifier(QUERY_MAP).deviceProperty(DEVICE_1, null);
    }

    @Test
    @Parameters(method = "wrongParameterFormattedStrings")
    public void shouldThrowWhenModifyParameterWithWrongFormat(String parameter) {
        expectedException.expect(IllegalArgumentException.class);
        modifier(QUERY_MAP).parameter(parameter);
    }

    @Test
    public void shouldModifyProperty() throws IOException {
        Map<String, String> modifiedQuery = modifier(QUERY_MAP)
                .deviceProperty(DEVICE_1, PROPERTY_2).toMap();

        assertKeyValues(modifiedQuery,
                map -> map.containsValue(ImmutableMap.of(DEVICE_KEY, DEVICE_1, PROPERTY_KEY, PROPERTY_2)));
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowWhenModifyParameterWithNull() {
        modifier(QUERY_MAP).parameter(null);
    }

    @Test
    public void shouldModifyParameter() throws IOException {
        Map<String, String> modifiedQuery = modifier(QUERY_MAP).parameter(PARAMETER_2).toMap();
        assertKeyValues(modifiedQuery,
                map -> map.containsValue(ImmutableMap.of(DEVICE_KEY, DEVICE_2, PROPERTY_KEY, PROPERTY_2)));

    }

    @Test
    public void shouldAddFieldWhenNonExistent() {
        Map<String, String> modifiedQuery = modifier(QUERY_MAP).addFields(FIELD_4).toMap();
        assertFields(FIELDS, QUERY_MAP);
        assertFields(ArrayUtils.add(FIELDS, FIELD_4), modifiedQuery);
    }

    @Test
    public void shouldIgnoreFieldWhenAddingExistent() {
        Map<String, String> modifiedQuery = modifier(QUERY_MAP).addFields(FIELD_1, FIELD_2).toMap();
        assertFields(FIELDS, QUERY_MAP);
        assertFields(FIELDS, modifiedQuery);
    }

    @Test
    public void shouldAddNothingWhenAddingNullFields() {
        Map<String, String> modifiedQuery = modifier(QUERY_MAP)
                .addFields(new String[] { null }).toMap();
        assertFields(FIELDS, QUERY_MAP);
        assertFields(FIELDS, modifiedQuery);
    }

    @Test
    public void shouldRemoveFieldWhenPresent() {
        Map<String, String> modifiedQuery = modifier(QUERY_MAP).removeFields(FIELD_1, FIELD_3).toMap();
        assertFields(FIELDS, QUERY_MAP);
        assertFields(new String[] { FIELD_2 }, modifiedQuery);
    }

    @Test
    public void shouldIgnoreFieldWhenRemovingNonExistent() {
        Map<String, String> modifiedQuery = modifier(QUERY_MAP).removeFields(FIELD_4).toMap();
        assertFields(FIELDS, QUERY_MAP);
        assertFields(FIELDS, modifiedQuery);
    }

    @Test
    public void shouldRemoveAllFields() {
        Map<String, String> modifiedQuery = modifier(QUERY_MAP).removeAllFields().toMap();
        assertFields(new String[0], modifiedQuery);
    }

    @Test
    public void shouldAddAliasWhenAddingNonExistent() throws IOException {
        Map<String, List<String>> aliasMap = QueryFieldAliases.alias(ALIAS_3).fields(FIELD_4).build();
        Map<String, String> modifiedQuery = modifier(QUERY_MAP).addAliases(aliasMap).toMap();
        assertAliases(modifiedQuery, QueryFieldAliases
                .alias(ALIAS_1).fields(FIELD_1, FIELD_2)
                .alias(ALIAS_2).fields(FIELD_3)
                .alias(ALIAS_3).fields(FIELD_4).build()::equals);
    }

    @Test
    public void shouldReplaceAliasWhenAddingExistent() throws IOException {
        Map<String, List<String>> aliasMap = QueryFieldAliases.alias(ALIAS_1).fields(FIELD_1).build();
        Map<String, String> modifiedQuery = modifier(QUERY_MAP).addAliases(aliasMap).toMap();
        assertAliases(modifiedQuery, QueryFieldAliases
                .alias(ALIAS_1).fields(FIELD_1)
                .alias(ALIAS_2).fields(FIELD_3).build()::equals);
    }

    @Test
    public void shouldThrowWhenAddingAliasesWithNullMap() {
        expectedException.expect(NullPointerException.class);
        modifier(QUERY_MAP).addAliases(null);
    }

    @Test
    public void shouldThrowWhenAddingAliasesWithNullKey() {
        expectedException.expect(IllegalArgumentException.class);
        Map<String, List<String>> aliasMap = new HashMap<>();
        aliasMap.put(null, ImmutableList.of());
        modifier(QUERY_MAP).addAliases(aliasMap);
    }

    @Test
    public void shouldThrowWhenAddingAliasesWithNullValue() {
        expectedException.expect(IllegalArgumentException.class);
        Map<String, List<String>> aliasMap = new HashMap<>();
        aliasMap.put(ALIAS_1, null);
        modifier(QUERY_MAP).addAliases(aliasMap);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowWhenRemovingAliasesWithNullArray() {
        modifier(QUERY_MAP).removeAliases((String[]) null);
    }

    @Test
    public void shouldRemoveAliasesWhenPresent() throws IOException {
        Map<String, String> modifiedQuery = modifier(QUERY_MAP).removeAliases(ALIAS_1).toMap();
        assertAliases(modifiedQuery, QueryFieldAliases.alias(ALIAS_2).fields(FIELD_3).build()::equals);
    }

    @Test
    public void shouldIgnoreAliasesWhenRemovingNonExistent() throws IOException {
        Map<String, String> modifiedQuery = modifier(QUERY_MAP).removeAliases(ALIAS_3).toMap();
        assertAliases(modifiedQuery, ALIASES::equals);
    }

    @Test
    public void shouldRemoveAllAliases() throws IOException {
        Map<String, String> modifiedQuery = modifier(QUERY_MAP).removeAllAliases().toMap();
        assertAliases(modifiedQuery, Map::isEmpty);
    }

    public Object[] wrongParameterFormattedStrings() {
        return new Object[] {
                DEVICE_1 + "#" + PROPERTY_1,
                DEVICE_1 + "#/" + PROPERTY_1,
                DEVICE_1 + "/#" + PROPERTY_1,
                DEVICE_1 + "$/" + PROPERTY_1,
                DEVICE_1 + "/$" + PROPERTY_1,
                DEVICE_1 + "*/*" + PROPERTY_1,
                DEVICE_1 + "/*" + PROPERTY_1,

                "/" + PROPERTY_1,
                DEVICE_1 + "/",
                DEVICE_1 + "/" + PROPERTY_1 + "/",
                "/" + DEVICE_1 + "/" + PROPERTY_1
        };
    }

    private StartTimeStage<DevicePropertyQuery.DeviceStage> builder(String system) {
        return DevicePropertyQuery.builder(mock(SparkSession.class)).system(system);
    }

    private DevicePropertyQuery.Modifier modifier(Map<String, String> query) {
        return DevicePropertyQuery.modifier(mock(SparkSession.class), query);
    }
}