#!/bin/bash
echo "Shows all the issue that are in develop but not in master yet"
echo 
for issue in $(git log --oneline origin/develop --not origin/master | sed -e 's/NXCALS/\nNXCALS/g' | grep NXCALS | awk 'match($0,/(NXCALS-[0-9]*)/,a) {print a[1]}' | sort | uniq); do
  echo $issue $(curl -s -XGET https://issues.cern.ch/rest/api/2/issue/$issue?fields=summary | json_reformat | grep summary | awk 'BEGIN { FS="\"summary\": " }; {print $2}')
done
echo
echo "All comments with no correct issue number"
echo
git log --oneline origin/develop --not origin/master | grep -v NXCALS | grep -v "JENKINS:" | sort | uniq
