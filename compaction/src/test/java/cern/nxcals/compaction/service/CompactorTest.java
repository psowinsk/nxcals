package cern.nxcals.compaction.service;

import cern.nxcals.compaction.processor.FolderProcessor;
import cern.nxcals.compaction.processor.ProcessingStatus;
import org.apache.hadoop.fs.Path;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Created by jwozniak on 21/10/16.
 */
@RunWith(MockitoJUnitRunner.class)
public class CompactorTest {

    @Mock
    FolderFinder finder;

    ExecutorService executor;
    List<String> pathStrings;
    List<Path> paths;

    @Before
    public void setup() {
        executor = Executors.newSingleThreadExecutor();
        pathStrings = new ArrayList<>();
        pathStrings.add("/usr/acclog/staging/CMW/1/2/3/2016-10-01");
        pathStrings.add("/usr/acclog/staging/CMW/1/2/3/2016-10-02");
        pathStrings.add("/usr/acclog/staging/CMW/1/2/3/2016-10-03");
        paths = pathStrings.stream().map(str -> new Path(str)).collect(Collectors.toList());

    }

    @Test
    public void runSuccessful() throws Exception {
        //given
        List<FolderProcessor> processors = createProcessors();

        when(finder.findFolders(pathStrings)).thenReturn(paths);
        Compactor compactor = new Compactor(processors, finder, executor, 1, pathStrings.toArray(new String[0]));
        //when
        ProcessingStatus status = compactor.run(path -> true, Long.MAX_VALUE);
        assertEquals(9, status.getFinishedTasksCount());
        assertEquals(15, status.getFileCount());
        assertEquals(1200, status.getByteCount());

    }

    @Test
    public void runWithTaskFailureOfSecondTask() throws Exception {
        //given
        List<FolderProcessor> processors = createProcessorsWithFailureOfSecondTask();

        when(finder.findFolders(pathStrings)).thenReturn(paths);
        Compactor compactor = new Compactor(processors, finder, executor, 1, pathStrings.toArray(new String[0]));
        //when
        ProcessingStatus status = compactor.run(path -> true, Long.MAX_VALUE);
        assertEquals(3, status.getFinishedTasksCount());
        assertEquals(6, status.getFileCount());
        assertEquals(300, status.getByteCount());
        assertEquals(3, status.getExceptions().size());

    }

    @Test
    public void runWithTaskFailureOfFirstTask() throws Exception {
        //given
        List<FolderProcessor> processors = createProcessorsWithFailureOfFirstTask();

        when(finder.findFolders(pathStrings)).thenReturn(paths);
        Compactor compactor = new Compactor(processors, finder, executor, 1, pathStrings.toArray(new String[0]));
        //when
        ProcessingStatus status = compactor.run(path -> true, Long.MAX_VALUE);
        assertEquals(0, status.getFinishedTasksCount());
        assertEquals(0, status.getFileCount());
        assertEquals(0, status.getByteCount());
        assertEquals(3, status.getExceptions().size());

    }

    private List<FolderProcessor> createProcessorsWithFailureOfFirstTask() {
        List<FolderProcessor> processors = new ArrayList<>();
        processors.add(path -> {
            throw new RuntimeException("Failure");
        });
        processors.add(path -> new ProcessingStatus(1, 2, 100));
        return processors;
    }

    private List<FolderProcessor> createProcessorsWithFailureOfSecondTask() {
        List<FolderProcessor> processors = new ArrayList<>();
        processors.add(path -> new ProcessingStatus(1, 2, 100));
        processors.add(path -> {
            throw new RuntimeException("Failure");
        });
        return processors;
    }

    private List<FolderProcessor> createProcessors() {
        List<FolderProcessor> processors = new ArrayList<>();
        processors.add(path -> new ProcessingStatus(1, 2, 100));
        processors.add(path -> new ProcessingStatus(2, 3, 300));
        return processors;
    }

}