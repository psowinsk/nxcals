/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.compaction.config;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SQLContext;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ConfigurationProperties(prefix = "spark")
@Profile("dev")
public class SparkConfigurationDev {

    @Bean(name = "sparkCtx")
    public SQLContext createSpark() {

        //@formatter:off
        SparkConf conf = new SparkConf()
                .setAppName("Compactor-test")
                .setMaster("local[*]");
        //@formatter:on
        JavaSparkContext sc = new JavaSparkContext(conf);
        SQLContext sqlContext = new SQLContext(sc);
        sqlContext.setConf("spark.sql.parquet.compression.codec", "snappy");
        return sqlContext;
    }

}
