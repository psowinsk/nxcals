/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.compaction.processor;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@Ignore
public abstract class BaseFolderProcessorTest {

    static {
        System.setProperty("log4j.debug", "true");
        System.setProperty("log4j.configuration", "compaction-log4j2.xml-bak");
    }

    protected Path basePath = new Path(System.getProperty("user.dir") + "/staging/CMW/1/1/1/2016-03-21");

    @Mock
    protected FileSystem fileSystem;

    @Mock
    protected RemoteIterator<LocatedFileStatus> iteratorForBase;

    protected LocatedFileStatus avroFiles[] = new LocatedFileStatus[4];
    protected LocatedFileStatus smallParquetFiles[] = new LocatedFileStatus[4];
    protected LocatedFileStatus bigParquetFiles[] = new LocatedFileStatus[4];

    protected void initFiles(LocatedFileStatus files[], String base, String suffix, long size) {
        for (int i = 0; i < files.length; ++i) {
            files[i] = mock(LocatedFileStatus.class);
            when(files[i].getPath()).thenReturn(new Path(base + Path.SEPARATOR + "data" + i + suffix));
            when(files[i].isFile()).thenReturn(true);
            //            when(files[i].isDirectory()).thenReturn(false);
            when(files[i].getLen()).thenReturn(size);

        }

    }

    @Before
    public void setup() throws IOException {

        when(fileSystem.listFiles(basePath, false)).thenReturn(iteratorForBase);
        initFiles(avroFiles, basePath.toString(), ".avro", 1000L);
        initFiles(smallParquetFiles, basePath.toString(), "-small.parquet", 1000L);
        initFiles(bigParquetFiles, basePath.toString(), "-big.parquet", 5000L);

        when(iteratorForBase.hasNext()).thenReturn(true, true, true, true, true, true, true, true, true, true, true,
                true, false);

        when(iteratorForBase.next()).thenReturn(avroFiles[0], avroFiles[1], avroFiles[2], avroFiles[3],
                smallParquetFiles[0], smallParquetFiles[1], smallParquetFiles[2], smallParquetFiles[3],
                bigParquetFiles[0], bigParquetFiles[1], bigParquetFiles[2], bigParquetFiles[3]);

        when(fileSystem.exists(any(Path.class))).thenReturn(true);
    }

}
