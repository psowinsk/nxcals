/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.compaction.processor;

import cern.nxcals.common.utils.HdfsFileUtils;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.ArgumentMatchers;
import org.mockito.verification.VerificationMode;

import java.io.IOException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.StringJoiner;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MoveStageFilesProcessorTest extends BaseFolderProcessorTest {

    private void verifyMove(LocatedFileStatus[] files, VerificationMode mode) {
        Stream.of(files).forEach(file -> {
            try {
                ArgumentMatcher<Path> matcher = new ArgumentMatcher<Path>() {
                    private Path fromPath;

                    {
                        fromPath = file.getPath();
                    }

                    @Override
                    public boolean matches(Path toPath) {
                        String date = fromPath.getParent().getName();

                        String fromPathMatch = fromPath.toString().replaceFirst("staging/CMW", "data")
                                .replaceFirst(date, expandDateToPath(date));

                        return toPath.toString().equals(fromPathMatch);
                    }

                };

                verify(fileSystem, mode).rename(ArgumentMatchers.eq(file.getPath()), ArgumentMatchers.argThat(matcher));
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                fail("Cannot verify move");
            }
        });
    }

    private String expandDateToPath(String dateString) {
        StringJoiner datePathJoiner = new StringJoiner(Path.SEPARATOR);
        String[] splitDate = dateString.split("-");
        for (String unit : splitDate) {
            String normalizedUnit = unit.matches("^0.+$") ? unit.substring(1) : unit;
            datePathJoiner.add(normalizedUnit);
        }
        return datePathJoiner.toString();
    }

    @Test
    public void testRunOldDir() throws IOException {
        //this value should not matter as in the old dir we move all parquet files. 
        Path out = new Path(basePath.toString().replaceAll("staging/CMW", "data"));
        long maxParquetSizeToProcess = 1100L;
        MoveFilesProcessorImpl job = new MoveFilesProcessorImpl(fileSystem, maxParquetSizeToProcess);
        ProcessingStatus output = job.run(basePath);

        assertEquals(8, output.getFinishedTasksCount());
        assertEquals(8, output.getFileCount());

        verifyMove(smallParquetFiles, times(1));
        verifyMove(bigParquetFiles, times(1));
        verifyMove(avroFiles, never());
        verify(fileSystem, never()).mkdirs(out);
    }

    @Test
    public void testCurrentDir() throws IOException {
        //should only move the big files.
        ZonedDateTime dateTime = ZonedDateTime.of(2016, 10, 24, 2, 1, 0, 0, ZoneId.of("UTC"));
        basePath = new Path(System.getProperty("user.dir") + "/staging/CMW/1/1/1/" + dateTime
                .format(HdfsFileUtils.STAGE_DIRECTORY_DATE_FORMATTER));
        //must call again because the basePath changed...
        setup();

        long maxParquetSizeToProcess = 1100L;
        MoveFilesProcessorImpl job = new MoveFilesProcessorImpl(fileSystem, maxParquetSizeToProcess);
        //set the execution time
        job.setTime(dateTime);
        ProcessingStatus output = job.run(basePath);

        assertEquals(4, output.getFinishedTasksCount());
        assertEquals(4, output.getFileCount());

        verifyMove(smallParquetFiles, never());
        verifyMove(bigParquetFiles, times(1));
        verifyMove(avroFiles, never());

    }

    @Test
    public void testCreateDir() throws IOException {
        //should create dir if does not exists.
        String date = basePath.getName();

        Path out = new Path(basePath.toString().replaceAll("staging/CMW", "data")
                .replaceFirst(date, expandDateToPath(date)));
        when(fileSystem.exists(out)).thenReturn(false);

        long maxParquetSizeToProcess = 1100L;
        MoveFilesProcessorImpl job = new MoveFilesProcessorImpl(fileSystem, maxParquetSizeToProcess);
        ProcessingStatus output = job.run(basePath);

        assertEquals(8, output.getFinishedTasksCount());
        assertEquals(8, output.getFileCount());

        verifyMove(smallParquetFiles, times(1));
        verifyMove(bigParquetFiles, times(1));
        verifyMove(avroFiles, never());
        verify(fileSystem, times(8)).mkdirs(out);
    }

}
