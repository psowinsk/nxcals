/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.compaction.processor;

import cern.nxcals.common.domain.SystemData;
import cern.nxcals.compaction.SparkContextStoppedRuntimeException;
import cern.nxcals.service.client.api.internal.InternalSystemService;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.spark.SparkContext;
import org.apache.spark.sql.DataFrameReader;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.ArgumentMatchers;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.verification.VerificationMode;

import java.io.IOException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SparkCompactorProcessorTest extends BaseFolderProcessorTest {

    private String entityKeyDefSchema =
            "{\"type\":\"record\",\"name\":\"CmwKey\",\"namespace\":\"cern.cals\",\"fields\":[{\"name\":\"device\",\"type\":\"string\"},{\"name\":\"property\",\"type\":\"string\"}]}";
    private String partitionKeyDefSchema =
            "{\"type\":\"record\",\"name\":\"CmwPartition\",\"namespace\":\"cern.cals\",\"fields\":[{\"name\":\"class\",\"type\":\"string\"},{\"name\":\"property\",\"type\":\"string\"}]}";

    private String recordVersionKeyDefSchemaNullable =
            "{\"type\":\"record\",\"name\":\"RecordVersion\",\"namespace\":\"cern.cals\",\"fields\":\n"
                    + "[{\"name\":\"record_version1\",\"type\":[\"string\",\"null\"]},{\"name\":\"record_version2\",\"type\":[\"int\",\"null\"]}]}";

    private String recordVersionKeyDefSchema =
            "{\"type\":\"record\",\"name\":\"RecordVersion\",\"namespace\":\"cern.cals\",\"fields\":[{\"name\":\"record_version1\",\"type\":\"string\"},{\"name\":\"record_version2\",\"type\":\"int\"}]}";

    private String timeKeyDefSchema =
            "{\"type\":\"record\",\"name\":\"CmwPT\",\"namespace\":\"cern.cals\",\"fields\":[{\"name\":\"_p_t_\",\"type\":\"long\"}]}";

    @Mock
    private SparkSession sparkSession;

    @Mock
    private SparkContext sparkContext;

    @Mock
    private RemoteIterator<LocatedFileStatus> iteratorForOutput;

    @Mock
    private DataFrameReader reader;

    @Mock
    private Dataset<Row> dataFrame;

    private LocatedFileStatus outputParquetFiles[] = new LocatedFileStatus[4];
    @Mock
    private SparkCompactorProcessorImpl.DataFrameWriter dataFrameWriter;

    @Mock
    private InternalSystemService systemService;

    private static final long MAX_OUTPUT_FILE_SIZE = 512 * 1024 * 1024L;

    @Override
    @Before
    public void setup() throws IOException {
        super.setup();

        ArgumentMatcher<Path> matcher = (path) ->
                path.toString().startsWith(basePath.toString() + Path.SEPARATOR + "compaction-");

        when(fileSystem.listFiles(ArgumentMatchers.argThat(matcher), Matchers.eq(false))).thenReturn(iteratorForOutput);
        initFiles(outputParquetFiles, basePath.toString() + Path.SEPARATOR + "compaction-", "-out.parquet", 5000L);
        //this is important for batches as it should return one file for each batch. 
        when(iteratorForOutput.hasNext()).thenReturn(true, false, true, false, true, false, true, false);
        when(iteratorForOutput.next()).thenReturn(outputParquetFiles[0], outputParquetFiles[1], outputParquetFiles[2],
                outputParquetFiles[3]);

        when(sparkSession.read()).thenReturn(reader);
        when(reader.format(ArgumentMatchers.anyString())).thenReturn(reader);
        //        when(reader.load(ArgumentMatchers.anyString())).thenReturn(dataFrame);
        when(reader.load((String[]) ArgumentMatchers.anyVararg())).thenReturn(dataFrame);

        when(dataFrame.union(any(Dataset.class))).thenReturn(dataFrame);
        when(dataFrame.dropDuplicates(any(new String[0].getClass()))).thenReturn(dataFrame);
        when(dataFrame.repartition(ArgumentMatchers.anyInt())).thenReturn(dataFrame);
        when(systemService.findById(1L)).thenReturn(
                SystemData.builder().id(1).name("CMW").entityKeyDefinitions(entityKeyDefSchema)
                        .partitionKeyDefinitions(partitionKeyDefSchema)
                        .timeKeyDefinitions(timeKeyDefSchema)
                        .recordVersionKeyDefinitions(recordVersionKeyDefSchemaNullable).build());

        when(sparkSession.sparkContext()).thenReturn(sparkContext);
        when(sparkContext.isStopped()).thenReturn(false);

        System.err.println(reader);
    }

    private void verifyLoadingAndDeletion(LocatedFileStatus files[], VerificationMode mode) {
        String[] fileArr = Stream.of(files).map(file -> file.getPath().toString()).collect(Collectors.toList())
                .toArray(new String[] {});

        verify(reader, mode).load(fileArr);

        Stream.of(files).forEach(file -> {
            try {
                verify(fileSystem, mode).delete(file.getPath(), true);
            } catch (Exception e) {
                e.printStackTrace();
                fail("Cannot verify loading and deletion");
            }
        });
    }

    private void verifyMove(LocatedFileStatus[] files, VerificationMode mode) {

        Stream.of(files).forEach(file -> {
            try {
                ArgumentMatcher<Path> matcher = new ArgumentMatcher<Path>() {
                    private Path fromPath;

                    {
                        fromPath = file.getPath();
                    }

                    @Override
                    public boolean matches(Path toPath) {
                        return toPath.toString().equals(fromPath.toString().replaceFirst("compaction-\\/", ""));
                    }

                };

                verify(fileSystem, mode).rename(ArgumentMatchers.eq(file.getPath()), ArgumentMatchers.argThat(matcher));
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                fail("Cannot verify move");
            }
        });

    }

    @Test(expected = SparkContextStoppedRuntimeException.class)
    public void testSparkContextClosed() {
        when(sparkContext.isStopped()).thenReturn(true);
        long maxSizeFilesBatch = 2000L;
        SparkCompactorProcessorImpl job = new SparkCompactorProcessorImpl(fileSystem, sparkSession, maxSizeFilesBatch,
                MAX_OUTPUT_FILE_SIZE, systemService);
        job.setDataFrameWriter(dataFrameWriter);
        ProcessingStatus output = job.run(basePath);
    }

    /**
     * Uses 3 batches.
     *
     * @throws IOException
     */
    @Test
    public void testRun1_4Batches() throws IOException {

        long maxSizeFilesBatch = 2000L;
        SparkCompactorProcessorImpl job = new SparkCompactorProcessorImpl(fileSystem, sparkSession, maxSizeFilesBatch,
                MAX_OUTPUT_FILE_SIZE, systemService);
        job.setDataFrameWriter(dataFrameWriter);
        ProcessingStatus output = job.run(basePath);

        assertEquals(4, output.getFinishedTasksCount());
        assertEquals(8, output.getFileCount());

        verify(dataFrameWriter, times(4)).write(
                Matchers.startsWith(basePath.toString() + Path.SEPARATOR + "compaction-"), Matchers.eq(dataFrame));
        verify(dataFrame, times(4)).repartition(1);
        verifyLoadingAndDeletion(new LocatedFileStatus[] { avroFiles[0], avroFiles[1] }, times(1));
        verifyLoadingAndDeletion(new LocatedFileStatus[] { avroFiles[2], avroFiles[3] }, times(1));
        verifyLoadingAndDeletion(new LocatedFileStatus[] { smallParquetFiles[0], smallParquetFiles[1] }, times(1));
        verifyLoadingAndDeletion(new LocatedFileStatus[] { smallParquetFiles[2], smallParquetFiles[3] }, times(1));
        verifyLoadingAndDeletion(bigParquetFiles, never());
        verifyMove(new LocatedFileStatus[] { outputParquetFiles[0], outputParquetFiles[1], outputParquetFiles[2],
                outputParquetFiles[3] }, times(1));
    }

    /**
     * Should take all in one batch.
     *
     * @throws IOException
     */
    @Test
    public void testRun2_1Batch_Repartition28Files() throws IOException {

        long maxSizeFilesBatch = 100000L; // but total size is 4 x 1000 + 4 x 1000 + 4 x 5000 = 28,000.
        long maxOutputSize = 1000L; //for repartition into 28 files
        SparkCompactorProcessorImpl job = new SparkCompactorProcessorImpl(fileSystem, sparkSession, maxSizeFilesBatch,
                maxOutputSize, systemService);

        job.setDataFrameWriter(dataFrameWriter);
        ProcessingStatus output = job.run(basePath);

        assertEquals(1, output.getFinishedTasksCount());
        assertEquals(12, output.getFileCount());

        verify(dataFrame, times(1)).repartition(28);

        verify(dataFrameWriter, times(1)).write(
                Matchers.startsWith(basePath.toString() + Path.SEPARATOR + "compaction-"), Matchers.eq(dataFrame));

        verifyLoadingAndDeletion(new LocatedFileStatus[] {
                avroFiles[0], avroFiles[1], avroFiles[2], avroFiles[3] }, times(1));

        verifyLoadingAndDeletion(new LocatedFileStatus[] {
                smallParquetFiles[0], smallParquetFiles[1], smallParquetFiles[2], smallParquetFiles[3],
                bigParquetFiles[0], bigParquetFiles[1], bigParquetFiles[2], bigParquetFiles[3]
        }, times(1));

        verifyMove(new LocatedFileStatus[] { outputParquetFiles[0] }, times(1));

    }

    /**
     * Should take all in 2 batches, should not touch parquet files.
     *
     * testRun3Batch2OnlyAvro was here, but it didn't make sense after redesigning compactor to always consider
     * all parquet files in the directory
     */

    /**
     * Should take all in 2 batches, no avro files.
     *
     * @throws IOException
     */
    @Test
    public void testRun4NoAvroFiles() throws IOException {
        when(iteratorForBase.hasNext()).thenReturn(true, true, true, true, true, true, true,
                true, false);

        when(iteratorForBase.next())
                .thenReturn(smallParquetFiles[0], smallParquetFiles[1], smallParquetFiles[2], smallParquetFiles[3],
                        bigParquetFiles[0], bigParquetFiles[1], bigParquetFiles[2], bigParquetFiles[3]);

        long maxSizeFilesBatch = 2000L;
        SparkCompactorProcessorImpl job = new SparkCompactorProcessorImpl(fileSystem, sparkSession, maxSizeFilesBatch,
                MAX_OUTPUT_FILE_SIZE, systemService);
        job.setDataFrameWriter(dataFrameWriter);
        ProcessingStatus output = job.run(basePath);

        assertEquals(2, output.getFinishedTasksCount());
        assertEquals(4, output.getFileCount());

        verify(dataFrameWriter, times(2)).write(
                Matchers.startsWith(basePath.toString() + Path.SEPARATOR + "compaction-"), Matchers.eq(dataFrame));

        verifyLoadingAndDeletion(avroFiles, never());
        verifyLoadingAndDeletion(new LocatedFileStatus[] { smallParquetFiles[0], smallParquetFiles[1] }, times(1));
        verifyLoadingAndDeletion(new LocatedFileStatus[] { smallParquetFiles[2], smallParquetFiles[3] }, times(1));
        verifyLoadingAndDeletion(bigParquetFiles, never());
        verifyMove(new LocatedFileStatus[] { outputParquetFiles[0], outputParquetFiles[1] }, times(1));
    }

    /**
     * Should take all in 1 batches, only 1 avro file.
     *
     * @throws IOException
     */
    @Test
    public void testRun5OneAvroFile() throws IOException {
        when(iteratorForBase.hasNext()).thenReturn(true, false);
        when(iteratorForBase.next()).thenReturn(avroFiles[0]);

        long maxSizeParquetFile = 500L;
        long maxSizeFilesBatch = 2000L;
        SparkCompactorProcessorImpl job = new SparkCompactorProcessorImpl(fileSystem, sparkSession, maxSizeFilesBatch,
                MAX_OUTPUT_FILE_SIZE, systemService);
        job.setDataFrameWriter(dataFrameWriter);
        ProcessingStatus output = job.run(basePath);

        assertEquals(1, output.getFinishedTasksCount());
        assertEquals(1, output.getFileCount());

        verify(dataFrameWriter, times(1)).write(
                Matchers.startsWith(basePath.toString() + Path.SEPARATOR + "compaction-"), Matchers.eq(dataFrame));

        verifyLoadingAndDeletion(new LocatedFileStatus[] { avroFiles[0] }, times(1));
        verifyLoadingAndDeletion(smallParquetFiles, never());
        verifyLoadingAndDeletion(bigParquetFiles, never());
        verifyMove(new LocatedFileStatus[] { outputParquetFiles[0] }, times(1));
    }

    /**
     * Should do nothing, only 1 parquet file.
     *
     * @throws IOException
     */
    @Test
    public void testRun6OneParquetFile() throws IOException {
        when(iteratorForBase.hasNext()).thenReturn(true, false);
        when(iteratorForBase.next()).thenReturn(smallParquetFiles[0]);

        long maxSizeParquetFile = 2000L;
        long maxSizeFilesBatch = 2000L;
        SparkCompactorProcessorImpl job = new SparkCompactorProcessorImpl(fileSystem, sparkSession, maxSizeFilesBatch,
                MAX_OUTPUT_FILE_SIZE, systemService);
        job.setDataFrameWriter(dataFrameWriter);
        ProcessingStatus output = job.run(basePath);

        assertEquals(0, output.getFinishedTasksCount());

        verify(dataFrameWriter, times(0)).write(
                Matchers.startsWith(basePath.toString() + Path.SEPARATOR + "compaction-"), Matchers.eq(dataFrame));

        verifyLoadingAndDeletion(avroFiles, never());
        verifyLoadingAndDeletion(smallParquetFiles, never());
        verifyLoadingAndDeletion(bigParquetFiles, never());
        verifyMove(outputParquetFiles, never());
    }

    /**
     * Should take 1 batch,  2 parquet files.
     *
     * @throws IOException
     */
    @Test
    public void testRun7TwoParquetFiles() throws IOException {
        when(iteratorForBase.hasNext()).thenReturn(true, true, false);
        when(iteratorForBase.next()).thenReturn(smallParquetFiles[0], smallParquetFiles[1]);

        long maxSizeParquetFile = 2000L;
        long maxSizeFilesBatch = 2000L;
        SparkCompactorProcessorImpl job = new SparkCompactorProcessorImpl(fileSystem, sparkSession, maxSizeFilesBatch,
                MAX_OUTPUT_FILE_SIZE, systemService);
        job.setDataFrameWriter(dataFrameWriter);
        ProcessingStatus output = job.run(basePath);

        assertEquals(1, output.getFinishedTasksCount());
        assertEquals(2, output.getFileCount());

        verify(dataFrameWriter, times(1)).write(
                Matchers.startsWith(basePath.toString() + Path.SEPARATOR + "compaction-"), Matchers.eq(dataFrame));

        verifyLoadingAndDeletion(avroFiles, never());
        verifyLoadingAndDeletion(new LocatedFileStatus[] { smallParquetFiles[0], smallParquetFiles[1] }, times(1));
        verifyLoadingAndDeletion(bigParquetFiles, never());
        verifyMove(new LocatedFileStatus[] { outputParquetFiles[0] }, times(1));
    }

    /**
     * Should take 2 batches, 1 avro,  2 parquet files.
     *
     * @throws IOException
     */
    @Test
    public void testRun8OneAvroOneParquet() throws IOException {
        when(iteratorForBase.hasNext()).thenReturn(true, true, true, false);
        when(iteratorForBase.next()).thenReturn(smallParquetFiles[0], smallParquetFiles[1], avroFiles[0]);

        long maxSizeParquetFile = 2000L;
        long maxSizeFilesBatch = 2000L;
        SparkCompactorProcessorImpl job = new SparkCompactorProcessorImpl(fileSystem, sparkSession, maxSizeFilesBatch,
                MAX_OUTPUT_FILE_SIZE, systemService);
        job.setDataFrameWriter(dataFrameWriter);
        ProcessingStatus output = job.run(basePath);

        assertEquals(2, output.getFinishedTasksCount());
        assertEquals(3, output.getFileCount());

        verify(dataFrameWriter, times(2)).write(
                Matchers.startsWith(basePath.toString() + Path.SEPARATOR + "compaction-"), Matchers.eq(dataFrame));

        verifyLoadingAndDeletion(new LocatedFileStatus[] { avroFiles[0] }, times(1));
        verifyLoadingAndDeletion(new LocatedFileStatus[] { smallParquetFiles[0], smallParquetFiles[1] }, times(1));
        verifyLoadingAndDeletion(bigParquetFiles, never());
        verifyMove(new LocatedFileStatus[] { outputParquetFiles[0], outputParquetFiles[1] }, times(1));
    }

    /**
     * Should do nothing, no files.
     *
     * @throws IOException
     */
    @Test
    public void testRun9NoFiles() throws IOException {
        when(iteratorForBase.hasNext()).thenReturn(false);
        long maxSizeFilesBatch = 2000L;
        SparkCompactorProcessorImpl job = new SparkCompactorProcessorImpl(fileSystem, sparkSession, maxSizeFilesBatch,
                MAX_OUTPUT_FILE_SIZE, systemService);
        job.setDataFrameWriter(dataFrameWriter);
        ProcessingStatus output = job.run(basePath);

        assertEquals(0, output.getFinishedTasksCount());

        verify(dataFrameWriter, never()).write(
                Matchers.startsWith(basePath.toString() + Path.SEPARATOR + "compaction-"), Matchers.eq(dataFrame));

        verifyLoadingAndDeletion(avroFiles, never());
        verifyLoadingAndDeletion(smallParquetFiles, never());
        verifyLoadingAndDeletion(bigParquetFiles, never());
        verifyMove(outputParquetFiles, never());
    }

    /**
     * Should take 3 batches, but there is no output files produced.
     *
     * @throws IOException
     */
    @Test(expected = SparkCompactionRuntimeException.class)
    public void testRun10NoOutputFiles() throws IOException {
        when(iteratorForOutput.hasNext()).thenReturn(false);
        long maxSizeFilesBatch = 2000L;
        SparkCompactorProcessorImpl job = new SparkCompactorProcessorImpl(fileSystem, sparkSession, maxSizeFilesBatch,
                MAX_OUTPUT_FILE_SIZE, systemService);
        job.setDataFrameWriter(dataFrameWriter);
        ProcessingStatus output = job.run(basePath);

    }

}
