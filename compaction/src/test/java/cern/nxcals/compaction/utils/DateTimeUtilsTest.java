package cern.nxcals.compaction.utils;

import org.junit.Test;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by jwozniak on 20/10/16.
 */
public class DateTimeUtilsTest {
    @Test
    public void isDateBeforeDateTimeAfter2HoursAfterMidnight() throws Exception {
        //given
        ZonedDateTime currentTime = ZonedDateTime
                .parse("2016-10-19T02:00:01Z[UTC]", DateTimeFormatter.ISO_ZONED_DATE_TIME);

        //when/then
        assertTrue(DateTimeUtils.isDateBeforeDateTime("2016-10-17",
                currentTime.minusHours(26))); //ok to process this dir, it is 2 days old
        assertTrue(DateTimeUtils.isDateBeforeDateTime("2016-10-18", currentTime
                .minusHours(26))); //ok to process this dir as it is 2 hours after we should finish writing into it
        assertFalse(
                DateTimeUtils.isDateBeforeDateTime("2016-10-19", currentTime.minusHours(26))); //too early to process.
    }

    @Test
    public void isDateBeforeDateTimeBefore2HoursAfterMidnight() throws Exception {
        //given
        ZonedDateTime currentTime = ZonedDateTime
                .parse("2016-10-19T01:59:59Z[UTC]", DateTimeFormatter.ISO_ZONED_DATE_TIME);

        //when/then
        assertTrue(DateTimeUtils
                .isDateBeforeDateTime("2016-10-17", currentTime.minusHours(26))); //ok we can process this directory
        assertFalse(DateTimeUtils.isDateBeforeDateTime("2016-10-18",
                currentTime.minusHours(26))); // it is too early to process this directory
        assertFalse(DateTimeUtils
                .isDateBeforeDateTime("2016-10-19", currentTime.minusHours(26))); // to early to process the current day
    }
}