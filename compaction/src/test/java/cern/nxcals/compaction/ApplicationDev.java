/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.compaction;

import cern.nxcals.compaction.metrics.MetricsExporter;
import cern.nxcals.compaction.processor.ProcessingStatus;
import cern.nxcals.compaction.service.Compactor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.metrics.GaugeService;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
@EnableAutoConfiguration
public class ApplicationDev {
    static {
        System.setProperty("org.apache.logging.log4j.simplelog.StatusLogger.level", "TRACE");
        System.setProperty("logging.config", "classpath:compaction-log4j2.yml");

        System.setProperty("compactor.sleepTime", "5");

        //        System.setProperty("kerberos.principal", "acclog");
        //        System.setProperty("kerberos.keytab", "/opt/jwozniak/.keytab-acclog");
        System.setProperty("javax.persistence.validation.mode", "NONE");
        System.setProperty("java.security.krb5.realm", "CERN.CH");
        System.setProperty("java.security.krb5.kdc", "cerndc.cern.ch");
        System.setProperty("java.security.krb5.conf", "dev/null");
        //System.setProperty("service.url","http://cs-ccr-dev3:19093");
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) throws Exception {

        try {

            ApplicationContext ctx = SpringApplication.run(ApplicationDev.class);
            Compactor compactor = ctx.getBean(Compactor.class);
            GaugeService metrics = ctx.getBean(GaugeService.class);

            runCompactor(compactor, metrics);
            //Filter paths that are not from today
            //            compactor.run((path) -> {
            //                //this predicate is tested at the task execution time, so we take always new time
            //                ZonedDateTime edgeTime = ZonedDateTime.now(ZoneId.of("UTC")).minusHours(26); //we give 2 additional hours for processing of previous day
            //                return DateTimeUtils.isDateBeforeDateTime(path.getName(),edgeTime);
            //            }, 100);
        } catch (Throwable e) {
            e.printStackTrace();
            LOGGER.error("Exception while running compaction app", e);
        } finally {
            //has to exit to allow for the next run from outside scrip
            System.exit(0);
        }


        /*
         * System.out.println("Let's inspect the beans provided by Spring Boot:");
         * 
         * String[] beanNames = ctx.getBeanDefinitionNames(); Arrays.sort(beanNames); for (String beanName : beanNames)
         * { System.out.println(beanName); }
         */

        // String initPath = "/tmp/cals/MDB4";

    }

    private static void runCompactor(Compactor compactor, GaugeService metricsService) throws InterruptedException {
        Instant start = Instant.now();

        while (Duration.between(start, Instant.now()).toMillis() <= 30_000) {
            // we don't filter paths here...
            ProcessingStatus status = compactor.run(p -> true, 200);
            MetricsExporter.export(metricsService, status);
            TimeUnit.SECONDS.sleep(2);
        }

        System.err.println("!!! Finished ALL !!!");

    }

}
