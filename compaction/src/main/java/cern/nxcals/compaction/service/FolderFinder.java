/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.compaction.service;

import cern.nxcals.common.utils.HdfsFileUtils;
import cern.nxcals.common.utils.HdfsFileUtils.SearchType;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Processes recursively all the folders from a given path that match the *.avro or *.parquet pattern of which the directory is a date in format YYYY-mm-DD.
 * The date on the directory must be older than the current time - time window from a current time in order to be processed.
 */
@Service
public class FolderFinder {

    private static final Logger LOGGER = LoggerFactory.getLogger(FolderFinder.class);

    @Autowired
    private FileSystem fileSystem;

    public List<Path> findFolders(List<String> initialPaths) {
        long start = System.currentTimeMillis();
        List<Path> out = initialPaths.stream().map(Path::new).flatMap(this::convert).collect(Collectors.toList());
        LOGGER.info("Number of pathStrings found {} in {} ms", out.size(),
                Long.valueOf((System.currentTimeMillis() - start)));
        return out;
    }

    private Stream<Path> convert(Path path) {
        try {
            return HdfsFileUtils
                    .findFilesInPath(
                            fileSystem,
                            path,
                            SearchType.RECURSIVE,
                            file -> HdfsFileUtils.isDataFile(file)
                                    && file.getPath().getParent().getName().matches("\\d{4}-\\d{2}-\\d{2}")).stream()
                    .map(file -> file.getPath().getParent()).distinct();
        } catch (Exception ex) {
            LOGGER.error("Cannot process path {}", path, ex);
            return Stream.empty();
        }

    }

}
