/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.compaction.service;

import cern.nxcals.compaction.SparkContextStoppedRuntimeException;
import cern.nxcals.compaction.processor.FolderProcessor;
import cern.nxcals.compaction.processor.ProcessingStatus;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.hadoop.fs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * A main class that is used to run the whole compaction process.
 */

@Service
@Slf4j
public class Compactor {

    private final long sleepTime;

    private final FolderFinder finder;
    private final ExecutorService exec;

    private final List<String> paths;

    private final List<FolderProcessor> processors;

    @Autowired
    public Compactor(List<FolderProcessor> processors, FolderFinder finder, ExecutorService exec,
            @Value("${compactor.sleepTime}") long sleepTime, @Value("${compactor.paths}") String[] paths) {
        this.processors = Objects.requireNonNull(processors);
        this.finder = Objects.requireNonNull(finder);
        this.exec = Objects.requireNonNull(exec);
        Objects.requireNonNull(paths);
        this.paths = Arrays.asList(paths);
        log.info("Processing pathStrings {}", this.paths);
        if (this.paths.isEmpty()) {
            throw new IllegalArgumentException("Please provide pathStrings to run the compaction");
        }
        this.sleepTime = sleepTime;
    }

    /**
     * Runs the compaction based on the set pathStrings via ApplicationArguments in the constructor.
     * This method accepts optional Predicate to additionally filter out Paths that do not match it.
     *
     * @param pathFilterPredicate - a predicate to filter out certain pathStrings. Can be used to remove pathStrings not older than X days.
     */
    public ProcessingStatus run(Predicate<Path> pathFilterPredicate, long maxTasks) {
        Objects.requireNonNull(pathFilterPredicate);

        long startTime = System.currentTimeMillis();

        List<Path> jobs = finder.findFolders(paths).stream()
                .sorted()
                .limit(maxTasks)
                .collect(Collectors.toList());
        List<Future<ProcessingStatus>> futures = jobs.stream()
                .map(path -> exec.submit(() -> {
                    try {
                        if (!pathFilterPredicate.test(path)) {
                            log.info("Nothing to do here, path {} filtered out by predicate", path);
                            return new ProcessingStatus();
                        }
                        log.info("Started compaction for path {}", path);
                        final ProcessingStatus pathProcessingStatus = getTotalProcessingStatusForPath(path);
                        log.info("Finished compaction for path {} in {}, status {}", path,
                                DurationFormatUtils.formatDurationHMS(System.currentTimeMillis() - startTime),
                                pathProcessingStatus);
                        return pathProcessingStatus;
                    } catch (SparkContextStoppedRuntimeException ex) {
                        log.error("Fatal exception in processing path, spark context closed " + path, ex);
                        throw ex;
                    } catch (Exception ex) {
                        log.error("Exception in processing path " + path, ex);
                        return new ProcessingStatus(ex);
                    }
                })).collect(Collectors.toList());

        while (!allCompleted(futures)) {
            log.info("Waiting for the {} jobs, finished {}, elapsed time {}, processed total {}", futures.size(),
                    countFinished(futures),
                    DurationFormatUtils.formatDurationHMS(System.currentTimeMillis() - startTime),
                    sumFinished(futures));
            try {
                TimeUnit.SECONDS.sleep(sleepTime);
            } catch (Exception e) {
                log.error("Sleep interrupted", e);
            }
        }

        log.info("All {} jobs finished, elapsed time {}, total {}", futures.size(),
                DurationFormatUtils.formatDurationHMS(System.currentTimeMillis() - startTime), sumFinished(futures));
        return futures.stream().map(future -> {
            try {
                return future.get();
            } catch (InterruptedException | ExecutionException e) {
                throw new IllegalStateException(e);
            }
        }).reduce(ProcessingStatus::merge).orElseGet(ProcessingStatus::new);
    }

    private ProcessingStatus getTotalProcessingStatusForPath(Path path) {
        ProcessingStatus total = new ProcessingStatus();
        for (FolderProcessor process : processors) {
            try {
                total = ProcessingStatus.merge(total, process.run(path));
            } catch (Exception ex) {
                log.error("Exception in task " + process + " in processing path " + path, ex);
                //we stop processing if exception happened in one of the tasks, the other tasks will not be executed.
                return ProcessingStatus.merge(total, new ProcessingStatus(ex));
            }
        }
        return total;
    }

    private long countFinished(List<Future<ProcessingStatus>> futures) {
        return futures.stream().filter(Future::isDone).count();
    }

    private ProcessingStatus sumFinished(List<Future<ProcessingStatus>> futures) {
        return futures.stream().filter(Future::isDone).map(future -> {
            try {
                return future.get();
            } catch (InterruptedException | ExecutionException e) {
                throw new IllegalStateException(e);
            }
        }).reduce(ProcessingStatus::merge).orElseGet(ProcessingStatus::new);
    }

    private static boolean allCompleted(List<Future<ProcessingStatus>> futures) {
        return futures.isEmpty() || futures.stream().allMatch(Future::isDone);

    }

}
