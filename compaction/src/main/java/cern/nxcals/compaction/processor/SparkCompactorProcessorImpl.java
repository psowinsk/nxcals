/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.compaction.processor;

import cern.nxcals.common.domain.SystemData;
import cern.nxcals.common.utils.HdfsFileUtils;
import cern.nxcals.common.utils.HdfsFileUtils.SearchType;
import cern.nxcals.common.utils.HdfsPathDecoder;
import cern.nxcals.compaction.SparkContextStoppedRuntimeException;
import cern.nxcals.service.client.api.internal.InternalSystemService;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Schema;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Compacts the avro and parquet files in the directory.
 * The process can be done in batches to avoid blocking Spark for a
 * long period of time.
 * <p>
 * Please note that in order to avoid duplicates we have to do everything in 1 batch for the directory that is not updated anymore (older than 1 day)
 * This means that if the process fails in the middle of operation there will be next deduplication that actually will fix the duplicated files.
 * </p>
 *
 * @author jwozniak
 */

@Component
@Order(value = 1)
@Slf4j
public class SparkCompactorProcessorImpl extends FolderProcessorBaseImpl {

    private enum FileTypeFormat {
        AVRO("com.databricks.spark.avro"), PARQUET("parquet"), INVALID(null);
        private String fileFormat;

        FileTypeFormat(String format) {
            this.fileFormat = format;
        }

        String getFormat() {
            return this.fileFormat;
        }
    }

    private static final Predicate<LocatedFileStatus> avroPredicate = HdfsFileUtils::isAvroDataFile;
    private static final Predicate<LocatedFileStatus> parquetPredicate = HdfsFileUtils::isParquetDataFile;
    private final long maxOutputFileSize;
    private final SparkSession ctx;
    private final long maxFileBatchSize;
    private final InternalSystemService systemService;

    /**
     * Cannot mock spark {@link org.apache.spark.sql.DataFrameWriter} as this class is final. So I just extract this
     * save operation to another interface.
     *
     * @author jwozniak
     */
    interface DataFrameWriter {
        void write(String path, Dataset<Row> df);
    }

    /**
     * This is only for testing to inject dummy mockito writer.
     *
     * @param writer
     */
    void setDataFrameWriter(DataFrameWriter writer) {
        this.writer = writer;
    }

    private DataFrameWriter writer = (String path, Dataset<Row> df) -> df.write().mode(SaveMode.Append).parquet(path);

    /**
     *
     */
    @Autowired
    public SparkCompactorProcessorImpl(FileSystem fileSystem, SparkSession ctx,
            @Value("${compactor.maxFileBatchSize}") long maxFileBatchSize,
            @Value("${compactor.maxOutputFileSize}") long maxOutputFileSize,
            InternalSystemService systemService) {
        super("compact-files", fileSystem);
        this.ctx = Objects.requireNonNull(ctx);
        this.maxFileBatchSize = maxFileBatchSize;
        this.maxOutputFileSize = maxOutputFileSize;
        this.systemService = Objects.requireNonNull(systemService);

    }

    @Override
    protected ProcessingStatus runInternally(Path baseLocation) {
        checkSparkContext();
        String tempOperationPath =
                baseLocation.toString() + Path.SEPARATOR + "compaction-" + System.currentTimeMillis();
        Optional<ProcessingStatus> report = findAndProcessFiles(tempOperationPath, baseLocation);
        if (report.isPresent()) {
            deleteFile(new Path(tempOperationPath), true);
        }
        return report.orElse(new ProcessingStatus());
    }

    private void checkSparkContext() {
        if (this.ctx.sparkContext().isStopped()) {
            throw new SparkContextStoppedRuntimeException("The Spark context is closed!");
        }
    }

    /**
     * @param tempOperationPath - the path were the operation is taking place
     */
    private Optional<ProcessingStatus> findAndProcessFiles(String tempOperationPath, Path baseLocation) {
        long start = System.currentTimeMillis();

        List<LocatedFileStatus> filesToProcess = HdfsFileUtils.findFilesInPath(getFileSystem(), baseLocation,
                SearchType.NONRECURSIVE, parquetPredicate.or(avroPredicate));

        log.debug("Found {} files in directory {} ", filesToProcess.size(), baseLocation);

        if (!shouldProcessFiles(filesToProcess)) {
            // we don't process empty list of just one parquet file.
            return Optional.empty();
        }

        Map<Integer, List<LocatedFileStatus>> batches = filesToProcess.stream().collect(
                Collectors.groupingBy(new Function<LocatedFileStatus, Integer>() {
                    private long count = 0;
                    private int key = 0;

                    @Override
                    public Integer apply(LocatedFileStatus value) {
                        this.count += value.getLen();
                        if (this.count > maxFileBatchSize) {
                            this.count = value.getLen();
                            return ++key;
                        }
                        return key;
                    }
                }));

        log.debug("Start processing file batches {} in directory {} ", batches.size(), baseLocation);

        //batching might result in batches of 1 files that is parquet, those should not be processed, thus filtering again here...
        Optional<ProcessingStatus> ret = batches.values().stream().filter(this::shouldProcessFiles)
                .map(list -> processFilesBatch(tempOperationPath, list, baseLocation)).reduce(ProcessingStatus::merge);

        if (log.isDebugEnabled()) {
            log.debug("Finish processing file batches in directory {} in {} sec", baseLocation,
                    (System.currentTimeMillis() - start) / 1000);

        }
        return ret;
    }

    private boolean shouldProcessFiles(List<LocatedFileStatus> filesToProcess) {
        return !(filesToProcess.isEmpty() || (filesToProcess.size() == 1 && HdfsFileUtils
                .isParquetDataFile(filesToProcess.get(0))));
    }

    private ProcessingStatus processFilesBatch(String tempOperationPath, List<LocatedFileStatus> files,
            Path baseLocation) {
        try {
            long start = System.currentTimeMillis();
            long totalBytes = files.stream().mapToLong(LocatedFileStatus::getLen).sum();
            if (log.isDebugEnabled()) {
                log.debug(
                        "******  Start processing batch in directory {} for batch of {} files, total size {} MB, files like {} ",
                        tempOperationPath, files.size(),
                        String.format("%.3f", totalBytes / (1024 * 1024.0)),
                        files.stream().limit(3).collect(Collectors.toList())
                );
            }

            compactFiles(tempOperationPath, files, totalBytes, baseLocation);
            List<Path> output = moveParquetFilesToBase(tempOperationPath, baseLocation);
            if (!output.isEmpty()) {
                deleteFiles(files);
            } else {
                throw new SparkCompactionRuntimeException("The compaction batch in " + tempOperationPath
                        + " did not produce any output files for input ");
            }
            if (log.isDebugEnabled()) {
                log.debug("******  Finish processing batch of {} files totalSize {} MB in {} sec in directory {}",
                        files.size(),
                        String.format("%.3f", totalBytes / (1024 * 1024.0)),
                        DurationFormatUtils.formatDurationHMS(System.currentTimeMillis() - start), baseLocation);
            }
            return new ProcessingStatus(files.size(), totalBytes);
        } catch (Exception ex) {
            log.error("Error processing in directory {} for batch of {} files {}", tempOperationPath, files.size(),
                    files.stream().limit(3).collect(Collectors.toList()), ex);
            throw new SparkCompactionRuntimeException(
                    "Error processing in directory " + tempOperationPath + " for batch of " + files.size() + " files",
                    ex);
        }
    }

    private void compactFiles(String targetPath, List<LocatedFileStatus> files, long totalBytes, Path baseLocation) {
        try {

            int nbOfPartitions = getNbOfPartitions(totalBytes);

            log.info("Start compacting in {} with {} target output files", targetPath, nbOfPartitions);
            List<Dataset<Row>> frames = convertToDataFrames(files, baseLocation);
            log.info("Converted to {} datasets {} files in {}", frames.size(), files.size(), targetPath);
            frames.stream()
                    //.peek(DataFrame::printSchema)
                    .reduce(Dataset::union)
                    .ifPresent(df -> deduplicateAndWrite(df, targetPath, nbOfPartitions, baseLocation));

            log.info("Finish compacting in {}", targetPath);
            //
        } catch (Exception e) {
            throw new SparkCompactionRuntimeException("Failed to compact in " + baseLocation + " files "
                    + files.stream().map(LocatedFileStatus::getPath).limit(3).collect(Collectors.toList()), e);
        }
    }

    private void deduplicateAndWrite(Dataset<Row> df, String targetPath, int nbOfPartitions, Path baseLocation) {
        String[] deduplicateColumns = findDeduplicationColumnNames(baseLocation);
        Dataset<Row> deduplicated = df;
        if (deduplicateColumns.length > 0) {
            if (log.isDebugEnabled()) {
                log.debug("Deduplicating using columns {} records in path {}", Arrays.asList(deduplicateColumns),
                        targetPath);
            }
            deduplicated = df.dropDuplicates(deduplicateColumns);
        }
        log.debug("Writing & repartitioning into {} partitions records in path {}", nbOfPartitions, targetPath);
        this.writer.write(targetPath, deduplicated.repartition(nbOfPartitions));

    }

    private int getNbOfPartitions(long totalBytes) {
        if (totalBytes > maxOutputFileSize) {
            long partNb = totalBytes / maxOutputFileSize;
            if (partNb <= Integer.MAX_VALUE) {
                return (int) partNb;
            } else {
                return Integer.MAX_VALUE;
            }
        } else {
            return 1;
        }

    }

    /**
     * @param fromPath
     */
    private List<Path> moveParquetFilesToBase(String fromPath, Path baseLocation) {

        return HdfsFileUtils
                .findFilesInPath(getFileSystem(), new Path(fromPath), SearchType.NONRECURSIVE,
                        HdfsFileUtils::isParquetDataFile).stream().map(file -> {
                    String fileName = file.getPath().getName();
                    Path newPath = new Path(baseLocation.toString() + Path.SEPARATOR + fileName);
                    try {
                        getFileSystem().rename(file.getPath(), newPath);
                        return newPath;
                    } catch (Exception e) {
                        throw new SparkCompactionRuntimeException(
                                "Cannot move file from " + file.getPath() + " to " + newPath, e);
                    }
                }).collect(Collectors.toList());

    }

    private void deleteFile(Path path, boolean recursive) {
        try {
            if (getFileSystem().exists(path)) {
                getFileSystem().delete(path, recursive);
            }
        } catch (IOException e) {
            log.error("Cannot delete file " + path, e);
        }
    }

    private void deleteFiles(List<LocatedFileStatus> files) {
        files.forEach(file -> deleteFile(file.getPath(), true));
    }

    private List<Dataset<Row>> convertToDataFrames(List<LocatedFileStatus> files, Path baseLocation) {
        //split by type, avro, parquet, invalid
        Map<FileTypeFormat, List<LocatedFileStatus>> filesByType = files.stream().collect(
                Collectors.groupingBy(file -> {
                    if (HdfsFileUtils.isAvroDataFile(file)) {
                        return FileTypeFormat.AVRO;
                    } else if (HdfsFileUtils.isParquetDataFile(file)) {
                        return FileTypeFormat.PARQUET;
                    } else {
                        return FileTypeFormat.INVALID;
                    }
                }));

        filesByType.entrySet().forEach(entry ->
                log.debug("Found {} files of type {} in path {}", entry.getValue().size(), entry.getKey(),
                        baseLocation));

        if (filesByType.containsKey(FileTypeFormat.INVALID)) {
            log.warn("Ignoring invalid (not avro/parquet) files found {} in path {}",
                    filesByType.get(FileTypeFormat.INVALID), baseLocation);
            filesByType.remove(FileTypeFormat.INVALID);
        }

        return filesByType.entrySet().stream().map(entry -> {
            List<String> paths = entry.getValue().stream().map(file -> file.getPath().toString())
                    .collect(Collectors.toList());
            String[] pathsArr = paths.toArray(new String[] {});
            return ctx.read().format(entry.getKey().fileFormat).load(pathsArr);
        }).collect(Collectors.toList());

    }

    /**
     * Finds the columns by getting the system data (all system schemas defined for this system). The system id is found from the path.
     *
     * @param path - it is assumed that the path will contain the system and it will have the following structure at the end:
     *             /systemId/partitionId/schemaId/date and the 4th element from the end can be taken as the system.
     * @return
     */
    private String[] findDeduplicationColumnNames(Path path) {
        long systemId = HdfsPathDecoder.findSystemIdFrom(Paths.get(path.toString()));
        log.debug("Finding SystemData for system id {}", systemId);
        //this is cached at the systemService
        SystemData systemData = systemService.findById(systemId);
        return getDeduplicationColumnsFromSystemData(systemData);
    }

    private static String[] getDeduplicationColumnsFromSystemData(SystemData systemData) {
        Set<String> columns = new HashSet<>();
        columns.addAll(getFieldNames(systemData.getEntityKeyDefinitions()));
        columns.addAll(getFieldNames(systemData.getTimeKeyDefinitions()));
        if (systemData.getRecordVersionKeyDefinitions() != null) {
            columns.addAll(getFieldNames(systemData.getRecordVersionKeyDefinitions()));
        }
        return columns.toArray(new String[] {});
    }

    private static Collection<? extends String> getFieldNames(String schema) {
        return new Schema.Parser().parse(schema).getFields().stream().map(Schema.Field::name)
                .collect(Collectors.toSet());
    }

}
