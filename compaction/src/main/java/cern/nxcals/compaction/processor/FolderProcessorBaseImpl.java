/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.compaction.processor;

import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * Base class for all the jobs. Holds the FileSystem & reports about the processor execution.
 */
@Slf4j
public abstract class FolderProcessorBaseImpl implements FolderProcessor {

    private final FileSystem fileSystem;
    private final String name;
    private ZonedDateTime execTime;

    /**
     */
    public FolderProcessorBaseImpl(String name, FileSystem fileSystem) {
        super();
        this.name = Objects.requireNonNull(name);
        this.fileSystem = Objects.requireNonNull(fileSystem);
    }

    protected FileSystem getFileSystem() {
        return fileSystem;
    }

    @Override
    public ProcessingStatus run(Path baseLocation) {
        try {
            long start = System.currentTimeMillis();
            log.info("#####  Start {} in {}", name, baseLocation);
            ProcessingStatus report = runInternally(baseLocation);
            if (report.getFinishedTasksCount() > 0) {
                log.info("#####  Finish {} in {} total {} in {} sec", name, baseLocation, report,
                        (System.currentTimeMillis() - start) / 1000);
            } else {
                log.info("#####  Nothing to do for {} in {}", name, baseLocation);
            }
            return report;

        } catch (Exception ex) {
            log.error("#####  Failed to execute {} in {}", name, baseLocation, ex);
            throw ex;
        }
    }

    protected ZonedDateTime getTime() {
        if (execTime == null) {
            return ZonedDateTime.now(ZoneId.of("UTC"));
        } else {
            return execTime;
        }
    }

    void setTime(ZonedDateTime time) {
        this.execTime = time;
    }

    protected abstract ProcessingStatus runInternally(Path baseLocation);

    @Override
    public String toString() {
        return name;
    }

}
