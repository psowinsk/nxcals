package cern.nxcals.compaction.processor;

import org.apache.hadoop.fs.Path;

/**
 * Created by jwozniak on 21/10/16.
 */
public interface FolderProcessor {
    ProcessingStatus run(Path baseLocation);
}
