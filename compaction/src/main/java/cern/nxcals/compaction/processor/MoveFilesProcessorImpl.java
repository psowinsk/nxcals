/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.compaction.processor;

import cern.nxcals.common.utils.HdfsFileUtils;
import cern.nxcals.common.utils.HdfsFileUtils.SearchType;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.function.Predicate;

/**
 * Moves the files in staging directory to the final destination.
 */
@Component
@Order(value = 2)
@Slf4j
public class MoveFilesProcessorImpl extends FolderProcessorBaseImpl {

    static final String STAGING_REGEXP = "/staging/.+?/";
    static final String DATA_FOLDER = "/data/";

    private static final long AVOID_STAGED_DATA_COMPACTION_HOURS = 2;

    private final Predicate<LocatedFileStatus> smallFilePredicate;

    @Autowired
    public MoveFilesProcessorImpl(FileSystem fileSystem,
            @Value("${compactor.maxSmallParquetFileSize}") long maxParquetSizeToProcess) {
        super("move-stage-files", fileSystem);
        this.smallFilePredicate = (LocatedFileStatus file) -> file.getLen() <= maxParquetSizeToProcess;
    }

    private ProcessingStatus copyToFinalDestination(Path stagingFilePath) {
        Objects.requireNonNull(stagingFilePath, "Path to staged file should not be null!");
        try {
            Path finalFilePath = constructFilePathWithExpandedDate(stagingFilePath);
            if (!getFileSystem().exists(finalFilePath.getParent())) {
                log.debug("Creating directory {}", finalFilePath.getParent());
                getFileSystem().mkdirs(finalFilePath.getParent());
            }
            log.debug("Moving stage file {} to final file {}", stagingFilePath, finalFilePath);
            getFileSystem().rename(stagingFilePath, finalFilePath);
            return new ProcessingStatus(1, 0);

        } catch (IOException ex) {
            log.error("Cannot move file {} to final destination", stagingFilePath, ex);
            throw new UncheckedIOException("Exception moving file " + stagingFilePath + " to final destination", ex);
        }

    }

    @Override
    protected ProcessingStatus runInternally(Path baseLocation) {
        Predicate<LocatedFileStatus> parquetPredicate = HdfsFileUtils::isParquetDataFile;
        // give 2 hours for processing when on the edge between days.
        ZonedDateTime currentCompactionDateTime = getTime().minusHours(AVOID_STAGED_DATA_COMPACTION_HOURS);
        log.debug("Start moving files from base dir {} to final destination, current date {}", baseLocation
                .getName(), currentCompactionDateTime);
        if (currentCompactionDateTime.format(HdfsFileUtils.STAGE_DIRECTORY_DATE_FORMATTER)
                .equals(baseLocation.getName())) {
            // We are in the directory currently being written with avro files.
            // We have to copy over only the big parquet files
            parquetPredicate = parquetPredicate.and(smallFilePredicate.negate());
            log.debug("Copying only big parquet files from current (being still written) directory");
        } else {
            // We are in some old directory, lets copy all parquet files to the final destination
            log.debug("Copying all parquet files from current (old) directory");
        }

        return HdfsFileUtils
                .findFilesInPath(getFileSystem(), baseLocation, SearchType.NONRECURSIVE, parquetPredicate).stream()
                .map(FileStatus::getPath)
                .map(this::copyToFinalDestination)
                .reduce(ProcessingStatus::merge)
                .orElseGet(ProcessingStatus::new);
    }

    private Path constructFilePathWithExpandedDate(final Path filePath) {
        Objects.requireNonNull(filePath, "File path can not be null!");

        String fileName = filePath.getName();
        Path datedDirectoryPath = filePath.getParent();

        StringJoiner pathJoiner = new StringJoiner(Path.SEPARATOR);
        pathJoiner.add(datedDirectoryPath.getParent().toString())
                .add(HdfsFileUtils.expandDateToNestedPaths(datedDirectoryPath.getName()))
                .add(fileName);

        return new Path(pathJoiner.toString().replaceAll(STAGING_REGEXP, DATA_FOLDER));
    }

}
