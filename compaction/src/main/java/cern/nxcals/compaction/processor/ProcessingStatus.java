package cern.nxcals.compaction.processor;

import org.apache.commons.lang.exception.ExceptionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author jwozniak
 */
public class ProcessingStatus {
    private final long nbOfTasks;
    private final long byteCount;
    private final long fileCount;
    private List<Throwable> exceptions;

    public ProcessingStatus() {
        this(0, 0, 0);
    }

    public ProcessingStatus(long fileCount, long processedBytes) {
        this(1, fileCount, processedBytes);
    }

    public ProcessingStatus(long nbOfJobs, long fileCount, long processedBytes) {
        this.nbOfTasks = nbOfJobs;
        this.fileCount = fileCount;
        this.byteCount = processedBytes;
        this.exceptions = new ArrayList<>();
    }

    public ProcessingStatus(Throwable ex) {
        this(0, 0, 0);
        this.exceptions.add(ex);
    }

    private ProcessingStatus(long nbOfJobs, long fileCount, long processedBytes, List<Throwable> exceptions) {
        this(nbOfJobs, fileCount, processedBytes);
        this.exceptions = exceptions;
    }

    public static ProcessingStatus merge(ProcessingStatus first, ProcessingStatus second) {
        List<Throwable> exceptions = new ArrayList<>();
        exceptions.addAll(first.exceptions);
        exceptions.addAll(second.exceptions);
        return new ProcessingStatus(first.nbOfTasks + second.nbOfTasks, first.fileCount + second.fileCount,
                first.byteCount
                        + second.byteCount, exceptions);
    }

    public long getFinishedTasksCount() {
        return nbOfTasks;
    }

    public long getByteCount() {
        return byteCount;
    }

    public long getFileCount() {
        return fileCount;
    }

    public List<Throwable> getExceptions() {
        return Collections.unmodifiableList(exceptions);
    }

    @Override
    public String toString() {
        StringBuilder output = new StringBuilder("ProcessingStatus: total tasks " + nbOfTasks + " fileCount " +
                fileCount + " totalSize " + String.format("%.3f", byteCount / (1024 * 1024.0)) +
                " MB ");
        if (!exceptions.isEmpty()) {
            output.append("\n");
        }
        for (Throwable ex : exceptions) {
            output.append(" Exception: \n" + ExceptionUtils.getFullStackTrace(ex));
        }

        return output.toString();
    }

}
