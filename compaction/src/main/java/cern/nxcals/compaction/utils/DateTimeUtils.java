package cern.nxcals.compaction.utils;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by jwozniak on 20/10/16.
 */
public class DateTimeUtils {
    private DateTimeUtils() {
        //no instance
    }

    /**
     * Parses the date that has to be in the format of yyyy-MM-dd adding UTC zone (Z) and time 00:00 and checks if the obtained ZonedDateTime is before the given edgeTime.
     *
     * @param date
     * @param edgeTime
     * @return
     */
    public static boolean isDateBeforeDateTime(String date, ZonedDateTime edgeTime) {
        ZonedDateTime folderDateTime = ZonedDateTime
                .parse(date + "T00:00Z[UTC]", DateTimeFormatter.ISO_ZONED_DATE_TIME);
        return folderDateTime.isBefore(edgeTime);
    }
}
