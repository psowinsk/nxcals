/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.compaction;

import cern.nxcals.compaction.metrics.MetricsExporter;
import cern.nxcals.compaction.processor.ProcessingStatus;
import cern.nxcals.compaction.service.Compactor;
import cern.nxcals.service.client.api.internal.InternalCompactionService;
import cern.nxcals.service.client.providers.InternalServiceClientFactory;
import org.apache.spark.sql.SparkSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.metrics.GaugeService;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.context.ApplicationContext;

import java.util.concurrent.TimeUnit;

@SpringBootApplication
public class Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        try {

            SpringApplication app = new SpringApplication(Application.class);
            app.addListeners(new ApplicationPidFileWriter());
            ApplicationContext ctx = app.run();

            SparkSession sparkSession = ctx.getBean(SparkSession.class);
            GaugeService metricsService = ctx.getBean(GaugeService.class);
            Compactor compactor = ctx.getBean(Compactor.class);

            InternalCompactionService compactionService = InternalServiceClientFactory.createCompactionService();

            runCompactor(compactor, metricsService, sparkSession, compactionService);

        } catch (Exception e) {
            LOGGER.error("Exception while running compaction app", e);
            System.exit(1);
        } finally {
            //has to exit to allow further runs with external bash script.
            System.exit(0);
        }
    }

    private static void runCompactor(Compactor compactor, GaugeService metricsService, SparkSession sparkSession,
            InternalCompactionService compactionService) throws InterruptedException {
        while (true) { //testing running in an endless loop - lets see if Kerberos will be fine this time.
            if (sparkSession.sparkContext().isStopped()) {
                throw new IllegalStateException("SparkContext is closed, cannot proceed further");
            }
            ProcessingStatus status = compactor
                    .run(path -> compactionService.shouldCompact(path.toString()), Long.MAX_VALUE);
            MetricsExporter.export(metricsService, status);

            TimeUnit.MINUTES.sleep(1);
        }
    }
}
