package cern.nxcals.compaction;

/**
 * Thrown by the processor when the context is stopped (https://issues.cern.ch/browse/CALS-4931)
 * Created by jwozniak on 02/01/17.
 */
public class SparkContextStoppedRuntimeException extends RuntimeException {

    public SparkContextStoppedRuntimeException() {
    }

    public SparkContextStoppedRuntimeException(String message) {
        super(message);
    }

    public SparkContextStoppedRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public SparkContextStoppedRuntimeException(Throwable cause) {
        super(cause);
    }

    public SparkContextStoppedRuntimeException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
