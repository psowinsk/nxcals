/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.compaction.metrics;

import cern.nxcals.compaction.processor.ProcessingStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.actuate.metrics.GaugeService;

/**
 * Created by jwozniak on 17/11/16.
 */
@Slf4j
public class MetricsExporter {
    private MetricsExporter() {
    }

    public static void export(GaugeService service, ProcessingStatus status) {
        try {
            service.submit("nxcals.compaction.fileCount", status.getFileCount());
            service.submit("nxcals.compaction.byteCount", status.getByteCount());
            service.submit("nxcals.compaction.taskCount", status.getFinishedTasksCount());
            service.submit("nxcals.compaction.errorCount", status.getExceptions().size());
        } catch (Exception e) {
            log.error("Cannot export metrics", e);
        }
    }

}
