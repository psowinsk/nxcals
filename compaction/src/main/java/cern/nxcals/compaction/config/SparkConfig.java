/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.compaction.config;

import cern.nxcals.common.config.SparkPropertiesConfig;
import cern.nxcals.common.spark.SparkUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.SparkSession;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class SparkConfig {

    @Bean
    @DependsOn("kerberos")
    public SparkConf createSparkConf(SparkPropertiesConfig config) {
        return SparkUtils.createSparkConf(config);
    }

    @Bean
    @DependsOn({ "kerberos", "leader" })
    public SparkSession createSparkSession(SparkConf conf) {
        //FIXME - currently there are problems with
        return SparkSession.builder().config(conf)./*enableHiveSupport().*/
                //FIXME - to be understood what is this directory really???
                        config("spark.sql.warehouse.dir", "/tmp/nxcals/spark/warehouse-" + System.currentTimeMillis()).
                        getOrCreate();
    }

    @ConfigurationProperties(prefix = "spark")
    @Bean
    public SparkPropertiesConfig createSparkPropertiesConfig() {
        return new SparkPropertiesConfig();
    }
}


