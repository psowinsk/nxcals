/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.compaction.config;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.leader.LeaderLatch;
import org.apache.curator.framework.recipes.leader.LeaderLatchListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by jwozniak on 12/13/16.
 */
@Configuration
public class LeaderConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(LeaderConfig.class);

    @Bean(name = "leader", destroyMethod = "close")
    public LeaderLatch waitForLeader(CuratorFramework cf,
            @Value("${compactor.zookeeper.leaderPath}") String electionPath) throws Exception {
        LOGGER.info("**** STARTING LEADERSHIP LATCH ****");
        LeaderLatch leaderLatch = new LeaderLatch(cf, electionPath);
        LOGGER.info("**** AWAITING LEADERSHIP...****");
        leaderLatch.addListener(new LeaderLatchListener() {

            @Override
            public void isLeader() {
                /*
                 * Empty method implementation here, since the information about
                 * acquired leadership is provided outside of the listener's scope.
                 */
            }

            @Override
            public void notLeader() {
                LOGGER.warn("**** LOST LEADERSHIP...****");
                LOGGER.info("Exiting the process to keep safe, will be restarted");
                //System.exit() is not reliable as it calls the hooks which can block (in case of no Zookeeper they will).
                Runtime.getRuntime().halt(-1);
            }
        });
        leaderLatch.start();
        leaderLatch.await();

        LOGGER.info("**** GOT LEADERSHIP...****");

        return leaderLatch;
    }
}
