/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.compaction.config;

import cern.nxcals.common.security.KerberosRelogin;
import cern.nxcals.service.client.api.internal.InternalSystemService;
import cern.nxcals.service.client.providers.InternalServiceClientFactory;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.FileSystem;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Slf4j
@Configuration
public class SpringConfig {
    private static final int THREADS = 5;

    @Bean(name = "kerberos", initMethod = "start")
    public KerberosRelogin kerberosRelogin(@Value("${kerberos.principal}") String principal,
            @Value("${kerberos.keytab}") String keytab,
            @Value("${kerberos.relogin}") boolean enableRelogin) {
        return new KerberosRelogin(principal, keytab, enableRelogin);
    }

    @Bean(name = "queue")
    public LinkedBlockingQueue<Runnable> createQueue() {
        return new LinkedBlockingQueue<>();
    }

    @Bean(name = "executor")
    public ExecutorService createExecutor(LinkedBlockingQueue<Runnable> queue) {

        return new ThreadPoolExecutor(THREADS, THREADS, 0L, TimeUnit.MILLISECONDS, queue);

    }

    @Bean(name = "fileSystem")
    @DependsOn("kerberos")
    public FileSystem createFileSystem() {
        log.info("Creating HDFS FileSystem");
        org.apache.hadoop.conf.Configuration conf = new org.apache.hadoop.conf.Configuration();
        try {
            return FileSystem.get(conf);
        } catch (IOException e) {
            throw new UncheckedIOException("Cannot access filesystem", e);
        }
    }

    @Bean(name = "systemService")
    public InternalSystemService createSystemService() {
        return InternalServiceClientFactory.createSystemService();
    }
}
