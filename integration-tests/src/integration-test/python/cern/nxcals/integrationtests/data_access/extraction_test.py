from cern.nxcals.pyquery.builders.queryfactory import QueryFactory
from collections import Counter
from datetime import datetime, timedelta
from pyspark.sql.functions import col, pandas_udf, PandasUDFType

from . import PySparkIntegrationTest


class ShouldExtractCorrectNumberOfRecordsFromOneDat(PySparkIntegrationTest):
    def runTest(self):
        time_format = "%Y-%m-%d %H:%M:%S.%f"
        one_day_before = datetime.utcnow().replace(hour=0, minute=0, second=0, microsecond=0) - timedelta(days=1)
        two_days_before = one_day_before - timedelta(days=1)

        df = QueryFactory.key_values_query_builder("MOCK-SYSTEM") \
            .start_time(two_days_before.strftime(time_format)) \
            .end_time((one_day_before - timedelta(seconds=1)).strftime(time_format)) \
            .entity() \
            .key_value('device', 'NXCALS_MONITORING_DEV6') \
            .entity() \
            .key_value('device', 'NXCALS_MONITORING_DEV7') \
            .build_dataset(self.ss)
        if self.extract_since < two_days_before:
            self.assertEqual(df.count(), 172800)


class ShouldProperlySerializeForeachAndDoSimpleFiltering(PySparkIntegrationTest):
    def runTest(self):
        time_format = "%Y-%m-%d %H:%M:%S.%f"
        one_day_before = datetime.utcnow().replace(hour=0, minute=0, second=0, microsecond=0) - timedelta(days=1)
        two_days_before = one_day_before - timedelta(days=1)
        expected_array_size = 5
        df = QueryFactory.key_values_query_builder("MOCK-SYSTEM") \
            .start_time(two_days_before.strftime(time_format)) \
            .end_time((one_day_before - timedelta(seconds=1)).strftime(time_format)) \
            .entity() \
            .key_value('device', 'NXCALS_MONITORING_DEV6') \
            .build_dataset(self.ss)

        def test_function(value):
            if value != expected_array_size:
                raise ValueError('Test failed due to incorrect value. Expected size is: {}, actual is: {}'.format(
                    expected_array_size, value))

        if self.extract_since < two_days_before:
            # It seems that python serialization cannot handle assert methods and in some environments code below will fail.
            # df.foreach(lambda row: self.assertEqual(len(list(filter(lambda element: element % 2, row.byteArrayField.elements))), 5))
            df.foreach(
                lambda row: test_function(len(list(filter(lambda element: element % 2, row.byteArrayField.elements)))))


class ShouldCorrectlyExecuteSparkAggregationFunctions(PySparkIntegrationTest):
    def runTest(self):
        time_format = "%Y-%m-%d %H:%M:%S.%f"
        one_day_before = datetime.utcnow().replace(hour=0, minute=0, second=0, microsecond=0) - timedelta(days=1)
        one_day_30_min_before = one_day_before - timedelta(minutes=30)
        df1 = QueryFactory.key_values_query_builder("MOCK-SYSTEM") \
            .start_time(one_day_30_min_before.strftime(time_format)) \
            .duration(timedelta(minutes=30)) \
            .entity() \
            .key_value('device', 'NXCALS_MONITORING_DEV6') \
            .build_dataset(self.ss).alias('df1')

        df2 = QueryFactory.key_values_query_builder("MOCK-SYSTEM") \
            .start_time(one_day_30_min_before.strftime(time_format)) \
            .duration(timedelta(minutes=30)) \
            .entity() \
            .key_value('device', 'NXCALS_MONITORING_DEV7') \
            .build_dataset(self.ss).alias('df2')

        if self.extract_since < one_day_30_min_before:
            for res in df2.join(df1, col('df1.shortField1') == col('df2.shortField1')).groupBy(
                    col('df2.byteField1')).avg('df1.longField2').collect():
                self.assertEqual(res['avg(longField2)'], 1)


class ShouldReturnMatchingResultForFilterAndQuery(PySparkIntegrationTest):
    def runTest(self):
        time_format = "%Y-%m-%d %H:%M:%S.%f"
        one_day_before = datetime.utcnow().replace(hour=0, minute=0, second=0, microsecond=0) - timedelta(days=1)
        one_day_30_min_before = one_day_before - timedelta(minutes=30)
        df = QueryFactory.key_values_query_builder("MOCK-SYSTEM") \
            .start_time(one_day_30_min_before.strftime(time_format)) \
            .end_time((one_day_before - timedelta(seconds=1)).strftime(time_format)) \
            .entity() \
            .key_value('device', 'NXCALS_MONITORING_DEV6') \
            .build_dataset(self.ss)

        if self.extract_since < one_day_30_min_before:
            collection1 = df.filter(df.floatField1 < 0.5).select('boolField').collect()
            collection2 = df.toPandas().query('floatField1 < 0.5')['boolField'].values.tolist()
            self.assertEqual(Counter([x[0] for x in collection1]), Counter(collection2))


class ShouldProperlyExecuteComplexQuery(PySparkIntegrationTest):
    def runTest(self):
        time_format = "%Y-%m-%d %H:%M:%S.%f"
        one_day_before = datetime.utcnow().replace(hour=0, minute=0, second=0, microsecond=0) - timedelta(days=1)
        one_day_1_min_before = one_day_before - timedelta(minutes=1)
        df = QueryFactory.key_values_query_builder("MOCK-SYSTEM") \
            .start_time(one_day_1_min_before.strftime(time_format)) \
            .end_time((one_day_before - timedelta(seconds=1)).strftime(time_format)) \
            .entity() \
            .key_value('device', 'NXCALS_MONITORING_DEV6') \
            .build_dataset(self.ss)

        @pandas_udf("floatField1 double, sum_abs_intField1 long", PandasUDFType.GROUPED_MAP)  # doctest: +SKIP
        def normalize(pdf):
            sum_abs_intField1 = pdf.sum_abs_intField1
            return pdf.assign(sum_abs_intField1=(sum_abs_intField1 - sum_abs_intField1.mean()))

        if self.extract_since < one_day_1_min_before:
            avgs = df.groupBy().avg()
            new_columns = [col.replace('avg(', '').replace(')', '') for col in avgs.columns]
            result = avgs.toDF(*new_columns) \
                .selectExpr("floatField1 * 2", "floatField1", "abs(intField1)") \
                .withColumnRenamed("abs(intField1)", "abs_intField1") \
                .cube("(floatField1 * 2)", "floatField1") \
                .sum("abs_intField1") \
                .withColumnRenamed("sum(abs_intField1)", "sum_abs_intField1") \
                .orderBy("(floatField1 * 2)", "floatField1") \
                .drop("(floatField1 * 2)") \
                .groupBy("floatField1") \
                .apply(normalize) \
                .collect()
            for r in result:
                self.assertEqual(getattr(r, 'sum_abs_intField1'), 0)
