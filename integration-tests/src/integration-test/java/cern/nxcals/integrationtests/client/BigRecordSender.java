package cern.nxcals.integrationtests.client;

import cern.cmw.datax.DataBuilder;
import cern.cmw.datax.ImmutableData;
import cern.nxcals.client.Publisher;
import cern.nxcals.client.PublisherFactory;
import cern.nxcals.client.Result;
import com.google.common.collect.ImmutableList;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

/**
 * Created by wjurasz on 30/03/17.
 */
public class BigRecordSender {
    static {
        System.setProperty("org.apache.logging.log4j.simplelog.StatusLogger.level", "TRACE");
        System.setProperty("logging.config", "classpath:log4j2.yml");
        System.setProperty("service.url", "http://nxcals-wjurasz1:19093");
        System.setProperty("kafka.producer.bootstrap.servers",
                "nile-kafka-nxcals-test-01.cern.ch:9092,nile-kafka-nxcals-test-02.cern.ch:9092,nile-kafka-nxcals-test-03.cern.ch:9092,"
                        + "nile-kafka-nxcals-test-04.cern.ch:9092,nile-kafka-nxcals-test-05.cern.ch:9092");
        System.setProperty("kafka.producer.batch.size", "500000000");
        System.setProperty("java.security.krb5.conf", "/etc/krb5.conf");
        System.setProperty("java.security.auth.login.config", "/opt/wjurasz/jaas2.conf");
        System.setProperty("javax.security.auth.useSubjectCredsOnly", "false");

        System.setProperty("kafka.producer.security.protocol", "SASL_PLAINTEXT");
        System.setProperty("kafka.producer.sasl.mechanism", "GSSAPI");
        System.setProperty("kafka.producer.sasl.kerberos.service.name", "kafka");
        System.setProperty("kafka.producer.max.request.size", "600000000");
        System.setProperty("kafka.producer.buffer.memory", "30000000");
        System.setProperty("kafka.producer.batch.size", "1638400");

    }

    private static final List<String> topics = ImmutableList.<String>builder()
            .add("NXCALS_TOPIC_1", "NXCALS_TOPIC_2", "NXCALS_TOPIC_3", "NXCALS_TOPIC_4", "NXCALS_TOPIC_5",
                    "NXCALS_TOPIC_6", "NXCALS_TOPIC_7", "NXCALS_TOPIC_8", "NXCALS_TOPIC_9", "NXCALS_TOPIC_10",
                    "NXCALS_TOPIC_11", "NXCALS_TOPIC_12", "NXCALS_TOPIC_13", "NXCALS_TOPIC_14", "NXCALS_TOPIC_15",
                    "NXCALS_TOPIC_16", "NXCALS_TOPIC_17", "NXCALS_TOPIC_18", "NXCALS_TOPIC_19", "NXCALS_TOPIC_20",
                    "NXCALS_TOPIC_21", "NXCALS_TOPIC_22", "NXCALS_TOPIC_23", "NXCALS_TOPIC_24", "NXCALS_TOPIC_25",
                    "NXCALS_TOPIC_26", "NXCALS_TOPIC_27", "NXCALS_TOPIC_28", "NXCALS_TOPIC_29", "NXCALS_TOPIC_30",
                    "NXCALS_TOPIC_31", "NXCALS_TOPIC_32", "NXCALS_TOPIC_33", "NXCALS_TOPIC_34", "NXCALS_TOPIC_35",
                    "NXCALS_TOPIC_36", "NXCALS_TOPIC_37", "NXCALS_TOPIC_38", "NXCALS_TOPIC_39", "NXCALS_TOPIC_40",
                    "NXCALS_TOPIC_41", "NXCALS_TOPIC_42", "NXCALS_TOPIC_43", "NXCALS_TOPIC_44", "NXCALS_TOPIC_45",
                    "NXCALS_TOPIC_46", "NXCALS_TOPIC_47", "NXCALS_TOPIC_48", "NXCALS_TOPIC_49", "NXCALS_TOPIC_50",
                    "NXCALS_TOPIC_51", "NXCALS_TOPIC_52", "NXCALS_TOPIC_53", "NXCALS_TOPIC_54", "NXCALS_TOPIC_55",
                    "NXCALS_TOPIC_56", "NXCALS_TOPIC_57", "NXCALS_TOPIC_58", "NXCALS_TOPIC_59", "NXCALS_TOPIC_60",
                    "NXCALS_TOPIC_61", "NXCALS_TOPIC_62", "NXCALS_TOPIC_63", "NXCALS_TOPIC_64", "NXCALS_TOPIC_65",
                    "NXCALS_TOPIC_66", "NXCALS_TOPIC_67", "NXCALS_TOPIC_68", "NXCALS_TOPIC_69", "NXCALS_TOPIC_70",
                    "NXCALS_TOPIC_71", "NXCALS_TOPIC_72", "NXCALS_TOPIC_73", "NXCALS_TOPIC_74", "NXCALS_TOPIC_75",
                    "NXCALS_TOPIC_76", "NXCALS_TOPIC_77", "NXCALS_TOPIC_78", "NXCALS_TOPIC_79", "NXCALS_TOPIC_80",
                    "NXCALS_TOPIC_81", "NXCALS_TOPIC_82", "NXCALS_TOPIC_83", "NXCALS_TOPIC_84", "NXCALS_TOPIC_85",
                    "NXCALS_TOPIC_86", "NXCALS_TOPIC_87", "NXCALS_TOPIC_88", "NXCALS_TOPIC_89", "NXCALS_TOPIC_90",
                    "NXCALS_TOPIC_91", "NXCALS_TOPIC_92", "NXCALS_TOPIC_93", "NXCALS_TOPIC_94", "NXCALS_TOPIC_95",
                    "NXCALS_TOPIC_96", "NXCALS_TOPIC_97", "NXCALS_TOPIC_98", "NXCALS_TOPIC_99", "NXCALS_TOPIC_100")
            .build();

    private static final Logger LOGGER = LoggerFactory.getLogger(BigRecordSender.class);
    private int[] sizes = { 1209, 3474, 9683, 879, 5052, 6248, 5790, 7816, 5647, 4121, 7836, 3254, 5636, 9930, 2214,
            1022, 9023, 7197, 7128, 3260, 1903, 4510, 899, 674, 5768, 4972, 911, 1121, 8544, 4828, 9841, 723, 3944,
            4584, 5550, 4677, 3962, 3752, 6059, 6537, 1404, 8040, 3177, 7565, 6894, 599, 235, 6142, 1945, 3213, 8066,
            9200, 898, 3168, 7427, 8663, 7653, 80, 2680, 1569, 1584, 6603, 6850, 6590, 8062, 2368, 2048, 2989, 6241,
            7994, 5488, 8467, 4434, 2814, 5125, 6896, 2712, 50, 4682, 2452, 5841, 9296, 3838, 7668, 6173, 3268, 5543,
            5859, 3171, 2945, 4571, 4605, 6591, 3217, 8200, 9075, 1370, 8490, 1144, 2468, 787, 3073, 8579, 6078, 7051,
            195, 1042, 2815, 530, 5151, 5277, 2574, 6316, 8234, 5853, 4245, 7012, 338, 6774, 2093, 5038, 296, 8083,
            8461, 3144, 1190, 5391, 9382, 6905, 7940, 10, 8659, 7343, 6156, 2389, 3922, 962, 8493, 1621, 8817, 8757,
            6264, 8009, 6198, 2730, 3624, 3097, 6051, 7004, 3276, 5043, 1256, 2826, 8574, 2624, 1605, 4346, 8892, 9720,
            5047, 1182, 7061, 7323, 6012, 343, 5896, 2715, 9834, 90, 8786, 9578, 1764, 2829, 6206, 8554, 6824, 7410,
            2455, 9795, 8801, 9388, 7018, 9571, 8612, 9737, 875, 5714, 4733, 9663, 9595, 6248, 9179, 9633, 9306, 6616,
            4468, 8065, 4138, 1658, 8981, 29272, 92585, 70401, 30573, 91670, 89306, 76449, 84603, 81571, 83670, 28398,
            43834, 19186, 33117, 17204, 38200, 33341, 79089, 59830, 44439, 342323, 516292, 261163, 648315, 942322 };

    private List<Publisher<ImmutableData>> publishers = new ArrayList<>();
    private final RandomByteDataGenerator dataGenerator = new RandomByteDataGenerator();

    private ArrayBlockingQueue<Runnable> queue;

    private ExecutorService createExecutor() {
        ThreadFactoryBuilder threadFactoryBuilder = new ThreadFactoryBuilder();
        threadFactoryBuilder.setNameFormat("NXCALS-Ingestion-Executor-%d");
        queue = new ArrayBlockingQueue<>(100);
        ThreadPoolExecutor executor = new ThreadPoolExecutor(1, 10, 1L, TimeUnit.MINUTES, queue,
                threadFactoryBuilder.build());
        return executor;
    }

    private ImmutableData getImmutableData(int messageSize, int dimension) {
        DataBuilder builder = ImmutableData.builder();
        builder.add("device", "wjuraszDevice");
        builder.add("property", "wjuraszProperty");
        builder.add("class", "wjuraszClass");
        builder.add("__record_version__", 0L);
        builder.add("__record_timestamp__", System.currentTimeMillis() * 1000_000);

        if (dimension == 1) {
            builder.add("data", dataGenerator.generateOneDimensionData(messageSize), 1);
        } else {
            builder.add("data", dataGenerator.generateTwoDimensionData(messageSize), 2);
        }

        return builder.build();
    }

    public static void main(String args[]) {

        BigRecordSender bigRecordSender = new BigRecordSender();
        //        for (String topic : topics) {
        ExecutorService executorService = bigRecordSender.createExecutor();
        bigRecordSender.publishers.add(PublisherFactory.newInstance().createPublisher("TEST-CMW", Function.identity()));
        //        }

        int numberOfThreads = 1;
        ExecutorService publishingExecutorService = Executors.newFixedThreadPool(numberOfThreads);
        for (int i = 0; i < numberOfThreads; i++) {
            publishingExecutorService.execute(() -> {
                while (true) {
                    ImmutableData data = bigRecordSender.getImmutableData(
                            bigRecordSender.sizes[ThreadLocalRandom.current().nextInt(bigRecordSender.sizes.length)],
                            1);
                    try {
                        CompletableFuture<Result> future = bigRecordSender.publishers
                                .get(ThreadLocalRandom.current().nextInt(bigRecordSender.publishers.size()))
                                .publishAsync(data, executorService);
                        future.whenComplete((Result n, Throwable ex) -> {
                            if (ex == null) {
                                LOGGER.info("Record sent");
                            } else {
                                LOGGER.error("Error sending record", ex);
                            }
                        });
                    } catch (Exception ex) {
                        LOGGER.error("Cannot publish record", ex);
                    }

                    LOGGER.info("Record added for sending, queue size={}", bigRecordSender.queue.size());
                    try {
                        TimeUnit.MILLISECONDS.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

    }
}
