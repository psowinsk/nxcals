/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.integrationtests;

import cern.nxcals.common.domain.EntityData;
import cern.nxcals.common.domain.SystemData;
import cern.nxcals.service.client.api.EntityService;
import cern.nxcals.service.client.api.SystemService;
import cern.nxcals.service.client.api.internal.InternalEntityService;
import cern.nxcals.service.client.domain.KeyValues;
import cern.nxcals.service.client.domain.impl.SimpleKeyValues;
import cern.nxcals.service.client.providers.InternalServiceClientFactory;
import cern.nxcals.service.client.providers.ServiceClientFactory;
import com.google.common.collect.ImmutableMap;

import java.util.Map;
import java.util.Random;

/**
 * This test can be launched against a service to check if it does not have corrupted records.
 * We used to have them in the sense of entities that had incorrect partitions assigned.
 * The sQL to be executed on the meta-data db to verify (should return no rows):
 * select e.entity_id, e.REC_VERSION, eh.valid_from_stamp, e.key_values as ekeys, p.key_values as pkeys, e.partition_id, eh.VALID_TO_STAMP, eh.partition_id, p2.key_values from
 * ENTITIES_HIST eh join entities e on eh.entity_id = e.entity_id
 * join partitions p on eh.PARTITION_ID = p.PARTITION_ID
 * join partitions p2 on e.partition_id = p2.partition_id
 * where
 * REGEXP_SUBSTR(e.key_values, 'property\":.*\"\}') != REGEXP_SUBSTR(p.key_values, 'property\":.*\"\}')
 * order by eh.VALID_FROM_STAMP, e.entity_id;
 * Created by jwozniak on 08/11/17.
 */
public class CorruptionDemo {
    static {
        String user = System.getProperty("user.name");
        //System.setProperty("service.url", "https://nxcals-" + user + "1.cern.ch:19093,https://nxcals-" + user + "2.cern.ch:19093,https://nxcals-" + user + "3.cern.ch:19093");
        System.setProperty("service.url", "https://nxcals-" + user + "1.cern.ch:19093");
//        System.setProperty("service.url", "http://cs-ccr-dev3.cern.ch:29093");
        System.setProperty("kerberos.principal", "jwozniak");
        System.setProperty("kerberos.keytab", "/opt/jwozniak/.keytab");

        //        try {
//            System.setProperty("service.url", "https://" + InetAddress.getLocalHost().getHostName() + ":29093"); //Do not use localhost here.
//        } catch (UnknownHostException e) {
//            throw new RuntimeException("Cannot acquire hostname programmatically, provide the name full name of localhost");
//        }
//        System.setProperty("kerberos.principal", "mock-system-user"); //Name of the user. To change it go to {@link TEST_PRINCIPAL} variable.
//        //Do not set this variable to your username if you have Kerberos ticket cached,
//        //this will cause conflicts during authentication with local KDC instance.
//        System.setProperty("kerberos.keytab", ".service.keytab"); //This keytab is created automatically. It contains local service principle
//        //and user principle (with username as above)
//        System.setProperty("java.security.krb5.conf", "build/LocalKdc/krb5.conf"); //This conf is created automatically.
//        //It contains configuration necessary to run local instance of KDC.
//
//        System.setProperty("javax.net.ssl.trustStore", "selfsigned.jks");
//        System.setProperty("javax.net.ssl.trustStorePassword", "ALA123");
    }

    SystemService systemService = ServiceClientFactory.createSystemService();
    SystemData systemData = systemService.findByName("TEST-CMW");

    EntityService entityService = ServiceClientFactory.createEntityService();
    InternalEntityService internalEntityService =
            InternalServiceClientFactory.createEntityService();

    Random rand = new Random();

    public static void main(String[] args) {
        CorruptionDemo demo = new CorruptionDemo();
        //        demo.test();
        demo.run(200, 200, 200);
    }

    private void saveEntity(int devId, int classId) {
        String property = "\"property\": \"prop" + classId + "\"";
        Map<String, Object> entityKey = ImmutableMap.of("device", "corr_dev" + devId + "\", " + property);
        Map<String, Object> partitionKey = ImmutableMap.of("class", "corr_devClass\", " + property);

        KeyValues entityKeyValues = new SimpleKeyValues("", entityKey);
        KeyValues partitionKeyValues = new SimpleKeyValues(null, partitionKey);

        EntityData entityData1 = internalEntityService.findOrCreateEntityFor(
                systemData.getId(),
                entityKeyValues,
                partitionKeyValues,
                "corr_brokenSchema1", //this does not matter
                System.currentTimeMillis() * 1_000_000L);

        internalEntityService.extendEntityFirstHistoryDataFor(entityData1.getId(), "corr_brokenSchema" + devId, 0);

        //        EntityData entityData2 = entityService.findBySystemIdAndKeyValues(systemData.getId(),
//                entityKey); //finding just in case this matters... No prove that it does...
//        String thName = Thread.currentThread().getName();
//        if (!entityData1.getPartitionData().getKeyValues().equals(partitionKey)) {
//            System.err.println(
//                    "[" + thName + "] Entity partition corrupted for " + entityKey + " id=" + entityData1.getId()
//                            + " entityPartition=" + entityData1.getPartitionData() + " expected=" + partitionKey);
//            System.exit(-1);
//        } else {
//            //           System.out.println("[" + thName + "] Data ok for " + entityKey + " partition=" + partitionKey);
//        }

    }

    private void run(int devNum, int classNum) {
        while (true) {
            try {
                int dev = rand.nextInt(devNum);
                int clazz = rand.nextInt(classNum);
                saveEntity(dev, clazz);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void run(int threadNum, int devNum, int classNum) {
        for (int i = 0; i < threadNum; ++i) {
            Thread th = new Thread(() -> {
                this.run(devNum, classNum);
            });
            th.start();
        }
    }

    private void test() {
        Map<String, Object> keyValues = ImmutableMap.of("not_existent", "not_existent");
        EntityData entityData2 = entityService.findBySystemIdAndKeyValues(systemData.getId(), keyValues);
        System.err.println(entityData2);
    }
}
