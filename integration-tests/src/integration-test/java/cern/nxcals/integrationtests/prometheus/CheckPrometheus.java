package cern.nxcals.integrationtests.prometheus;

import cern.nxcals.integrationtests.config.PrometheusConfig;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.SpringBootDependencyInjectionTestExecutionListener;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by jwozniak on 25/03/17.
 */

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { PrometheusConfig.class })
@TestPropertySource("classpath:application.yml")

//jwozniak - due to a bug (mockito in spring boot, https://github.com/spring-projects/spring-boot/issues/6520) this is needed.
//Otherwise there is an exception when spring boot tries to reset mocks that actually are not used here.
@TestExecutionListeners(listeners = { SpringBootDependencyInjectionTestExecutionListener.class })
public class CheckPrometheus {

    @Autowired
    private Prometheus prometheus;

    @Test
    public void shouldHaveNoFiringAlertsInPrometheus() {

        JsonObject result = prometheus.alertsFiring();
        JsonArray alerts = result.getAsJsonObject("data").getAsJsonArray("result");
        Assert.assertEquals("Should have 0 alerts firing in prometheus", 0, alerts.size());
    }

    //
    @Test
    public void shouldHaveNoNXCALSMonitoringParametersMetricsErrors() {
        JsonObject result = prometheus.metrics("{__name__=~\"^nxcals_monitoring_dataloss_.*\"}");
        JsonArray metrics = result.getAsJsonObject("data").getAsJsonArray("result");
        Assert.assertTrue("Should have metrics nxcals_monitoring name", metrics.size() > 0);
        metrics.forEach(elt -> {
            JsonObject metric = (JsonObject) elt;
            Assert.assertEquals(
                    "Metric " + metric.getAsJsonObject("metric").get("__name__").getAsString() + " should be 0", "0",
                    metric.getAsJsonArray("value").get(1).getAsString());
        });
    }

}
