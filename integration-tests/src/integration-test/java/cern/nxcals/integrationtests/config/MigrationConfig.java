/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.integrationtests.config;

import cern.nxcals.common.security.KerberosRelogin;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Import;

import java.io.IOException;
import java.io.UncheckedIOException;

/**
 * Configuration class for the {@link cern.nxcals.integrationtests.migration.MigrationTest} to prepare the {@link org.apache.spark.sql.SparkSession}
 * and HDFS {@link FileSystem} initialization
 *
 * @author ntsvetko
 */
@Configuration
@Import(cern.nxcals.common.config.SparkContext.class)
public class MigrationConfig {
    @Value(value = "${kerberos.principal}")
    private String principal;

    @Value(value = "${kerberos.keytab}")
    private String keytabLocation;

    @Value(value = "${kerberos.relogin}")
    private String enableRelogin;

    @Value(value = "${hdfs.data.paths}")
    private String hdfsDataPaths;

    @Bean
    public KerberosRelogin kerberos() {
        KerberosRelogin kerberos = new KerberosRelogin(principal, keytabLocation, Boolean.valueOf(enableRelogin));
        kerberos.start();
        return kerberos;
    }

    @Bean
    public Path hdfsDataPaths() {
        return new Path(hdfsDataPaths);
    }

    @Bean
    @DependsOn("kerberos")
    public FileSystem createFileSystem() {
        org.apache.hadoop.conf.Configuration conf = new org.apache.hadoop.conf.Configuration();
        try {
            return FileSystem.get(conf);
        } catch (IOException e) {
            throw new UncheckedIOException("Cannot access filesystem", e);
        }
    }
}