/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.integrationtests.service;

import cern.nxcals.common.domain.EntityData;
import cern.nxcals.common.domain.EntityResources;
import cern.nxcals.common.utils.TimeUtils;
import cern.nxcals.service.client.domain.KeyValues;
import cern.nxcals.service.client.domain.impl.SimpleKeyValues;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Set;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class EntitiesResourcesServiceTest extends AbstractTest {

    @Test
    public void shouldFindEntitiesResources() {
        KeyValues entityKeyValues = new SimpleKeyValues("", ENTITY_KEY_VALUES_RESOURCES_TEST);
        KeyValues partitionKeyValues = new SimpleKeyValues("", PARTITION_KEY_VALUES);

        EntityData entityData1 = internalEntityService.findOrCreateEntityFor(systemData.getId(), entityKeyValues,
                partitionKeyValues, SCHEMA, RECORD_TIMESTAMP);

        Set<EntityResources> resourceData = entitiesResourcesService
                .findBySystemIdKeyValuesAndTimeWindow(systemData.getId(), entityData1.getEntityKeyValues(),
                        TimeUtils.getNanosFromInstant(Instant.now().minus(10, ChronoUnit.DAYS)),
                        TimeUtils.getNanosFromInstant(Instant.now()));
        assertEquals(1, resourceData.size());

        //FIXME: Use assertJ (timartin 27/11/2017)
        EntityResources entityResources = resourceData.iterator().next();
        assertEquals(1, entityResources.getResourcesData().getHbaseTableNames().size());
        System.err.println(entityResources.getResourcesData().getHdfsPaths());
        //        FIXME - why this return 11 paths for HDFS while the current date should not be there but in HBASE -jwozniak
        assertEquals(11, entityResources.getResourcesData().getHdfsPaths().size());
    }
}
