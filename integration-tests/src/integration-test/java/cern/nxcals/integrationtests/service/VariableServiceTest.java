/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.integrationtests.service;

import cern.nxcals.common.domain.EntityData;
import cern.nxcals.common.domain.VariableConfigData;
import cern.nxcals.common.domain.VariableData;
import cern.nxcals.common.utils.TimeUtils;
import cern.nxcals.service.client.domain.KeyValues;
import cern.nxcals.service.client.domain.impl.SimpleKeyValues;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.time.Instant;
import java.util.SortedSet;
import java.util.TreeSet;

import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(JUnit4.class)
public class VariableServiceTest extends AbstractTest {

    @Test
    public void shouldCreateAndFindVariable() {
        //lets first find some existing entity for which we will create variable
        KeyValues entityKeyValues = new SimpleKeyValues("", ENTITY_KEY_VALUES_VARIABLE_TEST);
        KeyValues partitionKeyValues = new SimpleKeyValues("", PARTITION_KEY_VALUES);
        EntityData entityData = internalEntityService
                .findOrCreateEntityFor(systemData.getId(), entityKeyValues,
                        partitionKeyValues, "brokenSchema1", RECORD_TIMESTAMP);

        assertNotNull(entityData);

        VariableConfigData varConfData1 = VariableConfigData.builder().entityId(entityData.getId()).build();
        SortedSet<VariableConfigData> varConfSet = new TreeSet<>();
        varConfSet.add(varConfData1);
        VariableData varData = VariableData.builder().name(variableName).description("Description")
                .creationTimeUtc(TimeUtils.getNanosFromInstant(Instant.now())).variableConfigData(varConfSet).build();
        VariableData variableData = variableService.registerOrUpdateVariableFor(varData);
        assertNotNull(variableData);
        assertEquals(variableName, variableData.getVariableName());

        VariableData foundVariable = variableService.findByVariableName(variableName);
        assertEquals(variableName, foundVariable.getVariableName());

        VariableData foundVariable2 = variableService.findByVariableNameAndTimeWindow(variableName, 100, 200);
        assertEquals(variableName, foundVariable2.getVariableName());
    }

    @Test
    public void shouldNotFindVariable() {
        String nonExistentVariable = "NoSuchVariable" + System.currentTimeMillis();
        VariableData foundVariable = variableService.findByVariableName(nonExistentVariable);
        assertNull(foundVariable);

        VariableData foundVariable2 = variableService.findByVariableNameAndTimeWindow(nonExistentVariable, 100, 200);
        assertNull(foundVariable2);
    }

}
