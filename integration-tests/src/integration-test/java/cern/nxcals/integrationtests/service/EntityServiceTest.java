/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.integrationtests.service;

import cern.nxcals.common.domain.EntityData;
import cern.nxcals.service.client.domain.KeyValues;
import cern.nxcals.service.client.domain.impl.SimpleKeyValues;
import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by jwozniak on 02/07/17.
 */

@RunWith(JUnit4.class)
public class EntityServiceTest extends AbstractTest {

    @Test
    public void shouldCreateAndFindEntity() {

        KeyValues entityKeyValues = new SimpleKeyValues("", ENTITY_KEY_VALUES);
        KeyValues partitionKeyValues = new SimpleKeyValues("", PARTITION_KEY_VALUES);

        EntityData entityData1 = internalEntityService.findOrCreateEntityFor(systemData.getId(), entityKeyValues,
                partitionKeyValues, "brokenSchema1", RECORD_TIMESTAMP);

        EntityData entityData2 = internalEntityService.findOrCreateEntityFor(systemData.getId(), entityKeyValues,
                partitionKeyValues, "brokenSchema1", RECORD_TIMESTAMP);

        Assert.assertEquals(1, entityData1.getEntityHistoryData().size());
        Assert.assertEquals(1, entityData2.getEntityHistoryData().size());

        EntityData entityData4 = internalEntityService
                .findBySystemIdAndKeyValues(systemData.getId(), ENTITY_KEY_VALUES);
        assertNotNull(entityData4);

        assertEquals(entityData1.getEntityKeyValues(), entityData4.getEntityKeyValues());

        EntityData entityData5 = internalEntityService
                .findByEntityIdAndTimeWindow(entityData4.getId(), 0, RECORD_TIMESTAMP);

        assertNotNull(entityData5);
    }

    @Test
    public void shouldCreateAndFindEntityUsingIds() {

        KeyValues entityKeyValues = new SimpleKeyValues("", ENTITY_KEY_VALUES);
        KeyValues partitionKeyValues = new SimpleKeyValues("", PARTITION_KEY_VALUES);

        Long nextTimestamp  = RECORD_TIMESTAMP + 100;
        String nextSchema ="brokenSchema2";
        EntityData entityData1 = internalEntityService.findOrCreateEntityFor(mockSystemData.getId(), entityKeyValues,
                partitionKeyValues, "brokenSchema1", RECORD_TIMESTAMP);

        Assert.assertEquals(1, entityData1.getEntityHistoryData().size());
        assertEquals(Long.valueOf(RECORD_TIMESTAMP), entityData1.getFirstEntityHistoryData().getValidFromStamp());

        EntityData entityData2 = internalEntityService.findOrCreateEntityFor(mockSystemData.getId(), entityData1.getId(),
                entityData1.getPartitionData().getId(), nextSchema, nextTimestamp);

        Assert.assertEquals(1, entityData2.getEntityHistoryData().size());
        assertEquals(nextTimestamp, entityData2.getFirstEntityHistoryData().getValidFromStamp());
        assertEquals(nextSchema, entityData2.getFirstEntityHistoryData().getSchemaData().getSchemaJson());

    }

    @Test
    public void shouldNotFindEntity() {
        Map<String, Object> keyValues = ImmutableMap.of("device", "%$#SHOULD_NOT_BE_FOUND");
        EntityData entityData3 = entityService.findBySystemIdAndKeyValues(systemData.getId(), keyValues);
        assertNull(entityData3);
    }

    @Test
    public void shouldLockUntil() {

        KeyValues entityKeyValues = new SimpleKeyValues("", ENTITY_KEY_VALUES);
        KeyValues partitionKeyValues = new SimpleKeyValues("", PARTITION_KEY_VALUES);

        Long nextTimestamp = RECORD_TIMESTAMP + 100;
        String nextSchema = "brokenSchema2";
        EntityData entityData1 = internalEntityService.findOrCreateEntityFor(mockSystemData.getId(), entityKeyValues,
                partitionKeyValues, "brokenSchema1", RECORD_TIMESTAMP);
        long time = System.currentTimeMillis() * 1_000_000;
        EntityData newEntityData = EntityData.builder(entityData1).lockUntil(time).build();
        List<EntityData> output = internalEntityService.updateEntities(Collections.singletonList(newEntityData));

        EntityData found = internalEntityService.findById(newEntityData.getId());
        assertEquals(time, (long) found.getLockUntilEpochNanos());

    }

}
