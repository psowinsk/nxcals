/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.integrationtests.service;

import cern.nxcals.common.domain.SystemData;
import cern.nxcals.integrationtests.ServiceProvider;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Ignore;

import java.util.Map;

/**
 * Those tests need variable that will point to the service location -Dservice.url=xxx.
 */
@Ignore
class AbstractTest extends ServiceProvider {

    private static final String RANDOM_STRING = RandomStringUtils.randomAscii(64);

    static final Map<String, Object> ENTITY_KEY_VALUES = ImmutableMap
            .of("device", "device_value" + RANDOM_STRING);
    static final Map<String, Object> ENTITY_KEY_VALUES_RESOURCES_TEST = ImmutableMap
            .of("device", "device_value_resources_test" + RANDOM_STRING);
    static final Map<String, Object> ENTITY_KEY_VALUES_SCHEMA_TEST = ImmutableMap
            .of("device", "device_value_schema_test" + RANDOM_STRING);
    static final Map<String, Object> ENTITY_KEY_VALUES_VARIABLE_TEST = ImmutableMap
            .of("device", "device_value_variable_test" + RANDOM_STRING);
    static final Map<String, Object> PARTITION_KEY_VALUES = ImmutableMap
            .of("specification", "devClass1" + RANDOM_STRING);

    static final SystemData mockSystemData = systemService.findByName("MOCK-SYSTEM");

    static final String SCHEMA = "TEST_SCHEMA" + RANDOM_STRING;
    static final String MOCK_SYSTEM_NAME = "MOCK-SYSTEM";
    static final SystemData systemData = systemService.findByName(MOCK_SYSTEM_NAME);

    static final long RECORD_TIMESTAMP = 1476789831111222334L;
    static final long MOCK_SYSTEM_ID = 0; //This is no longer -100, we should use 0 for MOCK-SYSTEM

    static String variableName = "VARIABLE_NAME" + RANDOM_STRING;
}
