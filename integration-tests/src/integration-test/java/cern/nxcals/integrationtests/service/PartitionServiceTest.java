/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.integrationtests.service;

import cern.nxcals.common.domain.PartitionData;
import cern.nxcals.service.client.domain.KeyValues;
import cern.nxcals.service.client.domain.impl.SimpleKeyValues;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by jwozniak on 02/07/17.
 */
@RunWith(JUnit4.class)
public class PartitionServiceTest extends AbstractTest {

    @Test
    public void shouldFindPartition() {
        KeyValues entityKeyValues = new SimpleKeyValues("", ENTITY_KEY_VALUES);
        KeyValues partitionKeyValues = new SimpleKeyValues("", PARTITION_KEY_VALUES);
        internalEntityService
                .findOrCreateEntityFor(systemData.getId(), entityKeyValues, partitionKeyValues, "brokenSchema1",
                        RECORD_TIMESTAMP);

        PartitionData partitionData = internalPartitionService
                .findBySystemIdAndKeyValues(systemData.getId(), PARTITION_KEY_VALUES);
        assertNotNull(partitionData);

        assertEquals(PARTITION_KEY_VALUES, partitionData.getKeyValues());
    }

    @Test
    public void shouldNotFindPartition() {
        PartitionData partitionData = internalPartitionService
                .findBySystemIdAndKeyValues(systemData.getId(),
                        ImmutableMap.of("specification", "SHOULD_NOT_BE_FOUND_PARTITION_KEY_VALUES"));
        assertNull(partitionData);
    }

}
