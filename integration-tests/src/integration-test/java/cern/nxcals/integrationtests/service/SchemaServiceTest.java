/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.integrationtests.service;

import cern.nxcals.common.domain.EntityData;
import cern.nxcals.common.domain.SchemaData;
import cern.nxcals.service.client.domain.KeyValues;
import cern.nxcals.service.client.domain.impl.SimpleKeyValues;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class SchemaServiceTest extends AbstractTest {
    @Test
    public void shouldFindSchema() {
        KeyValues entityKeyValues = new SimpleKeyValues("", ENTITY_KEY_VALUES_SCHEMA_TEST);
        KeyValues partitionKeyValues = new SimpleKeyValues(null, PARTITION_KEY_VALUES);
        EntityData entityData = internalEntityService.findOrCreateEntityFor(systemData.getId(),
                entityKeyValues, partitionKeyValues, SCHEMA, RECORD_TIMESTAMP);
        assertNotNull(entityData);
        SchemaData schemaData = internalSchemaService.findById(entityData.getSchemaData().getId());
        assertNotNull(schemaData);

        assertEquals(SCHEMA, schemaData.getSchemaJson());
    }
}
