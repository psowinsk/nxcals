package cern.nxcals.integrationtests.service;

import cern.nxcals.common.domain.SystemData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by jwozniak on 02/07/17.
 */
@RunWith(JUnit4.class)
public class SystemServiceTest extends AbstractTest {

    @Test
    public void shouldFindSystemById() {
        SystemData systemData = internalSystemService.findById(MOCK_SYSTEM_ID);

        assertNotNull(systemData);
        assertEquals(MOCK_SYSTEM_ID, systemData.getId());
    }

    @Test
    public void shouldFindSystemByName() {
        SystemData systemData = systemService.findByName(MOCK_SYSTEM_NAME);
        assertNotNull(systemData);
        assertEquals(MOCK_SYSTEM_NAME, systemData.getName());
    }

    @Test
    public void shouldNotFindSystemById() {
        SystemData systemData = internalSystemService.findById(MOCK_SYSTEM_ID - 10000);
        assertNull(systemData);
    }

    @Test
    public void shouldNotFindSystemByName() {
        SystemData systemData = systemService.findByName(MOCK_SYSTEM_NAME + System.currentTimeMillis());
        assertNull(systemData);
    }
}
