package cern.nxcals.integrationtests;

import cern.nxcals.service.client.api.EntitiesResourcesService;
import cern.nxcals.service.client.api.EntityService;
import cern.nxcals.service.client.api.SystemService;
import cern.nxcals.service.client.api.VariableService;
import cern.nxcals.service.client.api.internal.InternalEntityService;
import cern.nxcals.service.client.api.internal.InternalPartitionService;
import cern.nxcals.service.client.api.internal.InternalSchemaService;
import cern.nxcals.service.client.api.internal.InternalSystemService;
import cern.nxcals.service.client.providers.InternalServiceClientFactory;
import cern.nxcals.service.client.providers.ServiceClientFactory;

public class ServiceProvider {
    protected static final SystemService systemService = ServiceClientFactory.createSystemService();
    protected static final InternalSystemService internalSystemService = InternalServiceClientFactory
            .createSystemService();
    protected static final EntityService entityService = ServiceClientFactory.createEntityService();
    protected static final InternalEntityService internalEntityService = InternalServiceClientFactory
            .createEntityService();
    protected static final InternalPartitionService internalPartitionService = InternalServiceClientFactory
            .createPartitionService();
    protected static final VariableService variableService = ServiceClientFactory.createVariableService();
    protected static final InternalSchemaService internalSchemaService = InternalServiceClientFactory
            .createSchemaService();
    protected static final EntitiesResourcesService entitiesResourcesService = ServiceClientFactory
            .createEntityResourceService();
}
