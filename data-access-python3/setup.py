import argparse
import sys
from setuptools import find_packages, setup

version_argument = '--nxcals-version'

parser = argparse.ArgumentParser()
parser.add_argument(version_argument, required=True, help='Version which will be visible in .whl file')
args, _ = parser.parse_known_args()

# This is because setup() also parses command line and complains about unrecognized options
sys.argv.remove(version_argument + '=' + args.nxcals_version)

# Package meta-data.
NAME = 'nxcals-data-access-python3'
EMAIL = 'acclog@cern.ch'

setup(
    name=NAME,
    version=args.nxcals_version,
    description='Python data extraction API for NXCALS system',
    author_email=EMAIL,
    packages=find_packages(exclude=('tests',)),
    include_package_data=True,
    test_suite="tests",
    install_requires=['typing'],
    classifiers=[
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6'
    ],
)
