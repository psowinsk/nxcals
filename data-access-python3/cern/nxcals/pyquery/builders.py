from typing import Dict

def get_builders_package(spark):
    return spark._jvm.cern.nxcals.data.access.builders

class KeyValuesQuery:

    @staticmethod
    def builder(spark):
        return get_builders_package(spark).KeyValuesQuery.builder(spark._jsparkSession)

    @staticmethod
    def modifier(spark, query: Dict[str, str]):
        return get_builders_package(spark).KeyValuesQuery.modifier(spark._jsparkSession, query)

class DevicePropertyQuery:

    @staticmethod
    def builder(spark):
        return get_builders_package(spark).DevicePropertyQuery.builder(spark._jsparkSession)

    @staticmethod
    def modifier(spark, query: Dict[str, str]):
        return get_builders_package(spark).DevicePropertyQuery.modifier(spark._jsparkSession, query)

class VariableQuery:

    @staticmethod
    def builder(spark):
        return get_builders_package(spark).VariableQuery.builder(spark._jsparkSession)

    @staticmethod
    def modifier(spark, query: Dict[str, str]):
        return get_builders_package(spark).VariableQuery.modifier(spark._jsparkSession, query)
