/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.data.access.demo;

import cern.nxcals.data.access.builders.KeyValuesQuery;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;

import java.util.List;

@Import(cern.nxcals.common.config.SparkContext.class)
@SpringBootApplication
public class Application {

    static {
        System.setProperty("org.apache.logging.log4j.simplelog.StatusLogger.level", "TRACE");
        //        System.setProperty("logging.config", "classpath:log4j2.yml");

        String user = System.getProperty("user.name");
        //Kerberos is a must!
        System.setProperty("kerberos.keytab", "/opt/" + user + "/.keytab-acclog");
        System.setProperty("kerberos.principal", "acclog");

        //TEST Env
        //System.setProperty("service.url", "http://nxcals-test4.cern.ch:19093,http://nxcals-test5.cern.ch:19093,http://nxcals-test6.cern.ch:19093");
        //TESTBED
        //System.setProperty("service.url", "http://cs-ccr-nxcals9.cern.ch:19093,http://cs-ccr-nxcals10.cern.ch:19093,http://cs-ccr-nxcals11.cern.ch:19093");
        //PRO
        System.setProperty("service.url", "http://cs-ccr-nxcals6.cern.ch:19093,http://cs-ccr-nxcals7.cern.ch:19093,http://cs-ccr-nxcals8.cern.ch:19093");

    }

    /*
    2018-03-14 07:35:11.507 [ERROR] [pool-17-thread-1] ProcessorImpl - ERROR: Entity=mock-system_nxcals_monitoring_dev6 check=byHour
    condition= var data = dataSet.takeAsList(10); dataSet.count() == 3600
    getStartTime=2018-03-13T09:00:00Z getEndTime=2018-03-13T09:59:59.999999999Z failed with message=Expected 3600 by hour but got: 3660	nxcals-test1.cern.ch
2018-03-14 07:35:25	2018-03-14 07:35:24.363 [ERROR] [pool-17-thread-1] ProcessorImpl - ERROR: Entity=mock-system_nxcals_monitoring_dev6 check=byHour condition= var data = dataSet.takeAsList(10); dataSet.count() == 3600 getStartTime=2018-03-13T17:00:00Z getEndTime=2018-03-13T17:59:59.999999999Z failed with message=Expected 3600 by hour but got: 3660
     */
    public static void main(String[] args) {

        ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);

        // Create SparkSession
        SparkSession sparkSession = context.getBean(SparkSession.class);
//        sparkSession.sparkContext().hadoopConfiguration().set("parquet.task.side.metadata", "true");
//        sparkSession.read()
//                .load("/project/nxcals/nxcals_pro/data/2/186/632/2018-03-03/*.parquet")
//                .sort("__sys_nxcals_entity_id__", "__record_timestamp__").repartition(1)
//                .write().mode(SaveMode.Overwrite)
//                .parquet("/project/nxcals/nxcals_pro/data/2/186/632/2018-03-03/sorted/");
//
//        System.out.println("Finished!");
//        System.exit(0);
//        dataset.printSchema();
//        System.out.println(dataset.count());
        for(int i = 0 ; i < 100000000; i++) {
            long startTime = System.currentTimeMillis();
            Dataset<Row> ds = KeyValuesQuery.builder(sparkSession)
                    .system("CMW")
                    .startTime("2018-03-03 00:00:00.0")
                    .endTime("2018-03-03 23:59:59.999999999")
                    .fields("longArrayField")
//                    .fields("doubleField")
                    .entity()
                    .keyValue("device", "NXCALS_DEV_6")
                    .keyValue("property", "Logging")
                    .entity()
                    .keyValue("device", "NXCALS_DEV_5")
                    .keyValue("property", "Logging")
                    .buildDataset();

            System.out.println("Building Dataset took " +(System.currentTimeMillis()-startTime) + " ms");

            //        ds.printSchema();
            long beforeCount = System.currentTimeMillis();
            //long count = ds.count();
            //System.out.println("Count=" + count  + " took " + (System.currentTimeMillis()-beforeCount) + " ms");

            long beforeTake = System.currentTimeMillis();
//            List<Row> rows = ds.takeAsList(10);
            //ds.select("longArrayField.elements").show();
            List<Row> rows = ds.collectAsList();
            System.out.println("Take " + rows.size() +" took " + (System.currentTimeMillis()-beforeTake) + " ms");

            System.out.println("Total Execution took " + (System.currentTimeMillis()-startTime) + " ms");
            System.out.println("---------------------------------------------------------");
            //        ds.show(10);
            //        ds.createOrReplaceTempView("myview");
        }
//        Dataset<Row> result  = sparkSession.sql("select * from myview where `special-character` = 'test'");
//        Dataset<Row> result = sparkSession.sql("Select timestamp as `test-test`, count(*) from myview group by timestamp having count(*) > 1");

//        System.out.println("More than one timestamp");
//        result.show(10);
//
//        Dataset<Row> res2 = sparkSession.sql("select device as `test-test` from myview where timestamp = 1520933103000000000");
//        res2.createOrReplaceTempView("v2");
//
//        Dataset<Row> sql = sparkSession.sql("Select * from v2 where `test-test` = 'test'");
//        sql.show(10);
    }
}