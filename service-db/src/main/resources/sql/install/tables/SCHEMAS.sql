--------------------------------------------------------
--  DDL for Table SCHEMAS
--------------------------------------------------------

CREATE TABLE SCHEMAS 
(	
    SCHEMA_ID NUMBER,
	SCHEMA_CONTENT_HASH VARCHAR2(512 BYTE),
	SCHEMA_CONTENT CLOB
) ;
