/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.client;

import avro.shaded.com.google.common.collect.ImmutableMap;
import cern.cmw.data.DataFactory;
import cern.cmw.data.DiscreteFunction;
import cern.cmw.datax.DataBuilder;
import cern.cmw.datax.ImmutableData;
import cern.cmw.datax.ImmutableEntry;
import cern.cmw.datax.enumeration.EnumDefinition;
import cern.cmw.datax.enumeration.EnumFactory;
import cern.cmw.datax.enumeration.EnumValue;
import cern.cmw.datax.enumeration.EnumValueSet;
import cern.nxcals.common.SystemFields;
import cern.nxcals.common.avro.BytesToGenericRecordDecoder;
import cern.nxcals.common.converters.TimeConverter;
import cern.nxcals.common.converters.TimeConverterImpl;
import cern.nxcals.common.domain.EntityData;
import cern.nxcals.common.domain.EntityHistoryData;
import cern.nxcals.common.domain.PartitionData;
import cern.nxcals.common.domain.SchemaData;
import cern.nxcals.common.domain.SystemData;
import cern.nxcals.service.client.api.internal.InternalSchemaService;
import cern.nxcals.service.client.domain.KeyValues;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.SortedSet;
import java.util.stream.Collectors;

import static cern.cmw.datax.EntryType.STRING;
import static cern.nxcals.common.Schemas.ENTITY_ID;
import static cern.nxcals.common.Schemas.PARTITION_ID;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DataServiceEncoderImplTest extends Base {
    @Mock
    private InternalSchemaService schemaProvider;
    @Mock
    private RecordData recordData;
    @Mock
    private EntityData entityData;
    @Mock
    private PartitionData partitionData;
    @Mock
    private SystemData systemData;

    private TimeConverter timeConverter = new TimeConverterImpl();

    private static boolean checkType(Schema schema, String field, String type) {
        return schema.getField(field).schema().getTypes().stream().map(Schema::getName).anyMatch(type::equals);
    }

    @Before
    public void setUp() throws Exception {
        when(recordData.getEntityData()).thenReturn(entityData);
        when(entityData.getId()).thenReturn(10L);
        when(entityData.getPartitionData()).thenReturn(partitionData);
        when(entityData.getSystemData()).thenReturn(systemData);
        when(systemData.getId()).thenReturn(100L);
        when(partitionData.getId()).thenReturn(1000L);
    }

    @Test
    public void testEncodeEntityKeyValues() {
        // given
        DataServiceEncoderImpl serializer = new DataServiceEncoderImpl(entityKeyDefSchema, partitionKeyDefSchema,
                timeKeyDefSchema, recordVersionKeyDefSchemaNullable, timeConverter);
        ImmutableData data = createTestCmwData(10);

        // when
        KeyValues cachedRequest = serializer.encodeEntityKeyValues(data);

        // then
        assertNotNull(cachedRequest);
        assertNotNull(cachedRequest.getId());
        assertNotNull(cachedRequest.getKeyValues());

    }

    @Test
    public void testEncodePartitionKeyValues() {
        // given
        DataServiceEncoderImpl serializer = new DataServiceEncoderImpl(entityKeyDefSchema, partitionKeyDefSchema,
                timeKeyDefSchema, recordVersionKeyDefSchemaNullable, timeConverter);
        ImmutableData data = createTestCmwData(10);

        // when
        KeyValues partitionKeyValues = serializer.encodePartitionKeyValues(data);

        // then
        assertNotNull(partitionKeyValues);
        assertNull(partitionKeyValues.getId());
        assertNotNull(partitionKeyValues.getKeyValues());
    }

    /**
     * Test method for {@link DataServiceEncoderImpl#apply(RecordData)} .
     */
    @Test
    public void testEncodeSchemaAndSerialize() {
        // given
        DataServiceEncoderImpl serializer = new DataServiceEncoderImpl(entityKeyDefSchema, partitionKeyDefSchema,
                timeKeyDefSchema, recordVersionKeyDefSchema, timeConverter);
        ImmutableData data = createTestCmwData(10);
        String recordSchemaStr = serializer.encodeRecordFieldDefinitions(data);

        when(recordData.getData()).thenReturn(data);
        SchemaData schemaData = SchemaData.builder().id(1).schema(recordSchemaStr).build();
        SortedSet<EntityHistoryData> histSetMock = mock(SortedSet.class);
        when(entityData.getEntityHistoryData()).thenReturn(histSetMock);
        EntityHistoryData entityHistoryDataMock = mock(EntityHistoryData.class);
        when(histSetMock.first()).thenReturn(entityHistoryDataMock);
        when(entityHistoryDataMock.getSchemaData()).thenReturn(schemaData);
        // when
        byte[] output = serializer.apply(recordData);
        GenericRecord record = deserialize(output, schemaData);

        // System.err.println(record);
        // then
        assertNotNull(record);

        //check for the system fields
        assertEquals(1L, record.get(SystemFields.NXC_SCHEMA_ID.getValue()));
        assertEquals(10L, record.get(SystemFields.NXC_ENTITY_ID.getValue()));
        assertEquals(100L, record.get(SystemFields.NXC_SYSTEM_ID.getValue()));
        assertEquals(1000L, record.get(SystemFields.NXC_PARTITION_ID.getValue()));
        assertTrue((Long) record.get(SystemFields.NXC_TIMESTAMP.getValue()) <= System.currentTimeMillis() * 1_000_000);

        assertEquals("dev1", record.get("device").toString());
        assertEquals("class1", record.get("class").toString());
        assertEquals("prop1", record.get("property").toString());
        assertEquals(123456789L, record.get("_p_t_"));
        assertEquals("ver1", record.get("record_version1").toString());
        assertEquals(10, record.get("record_version2"));

        //check for all data fields
        System.out.println(record);
        verifyFields(data, record.getSchema());

    }

    private void verifyFields(ImmutableData data, Schema schema) {
        for (ImmutableEntry entry : data.getEntries()) {
            assertNotNull(schema.getField(entry.getName()));
        }
    }

    @Test
    public void testEncodeSchemaWithNullRecordVersion() {
        // given
        DataServiceEncoderImpl encoder = new DataServiceEncoderImpl(entityKeyDefSchema, partitionKeyDefSchema,
                timeKeyDefSchema, null, timeConverter);
        ImmutableData data = createTestCmwData(null);

        // when
        String recordSchemaStr = encoder.encodeRecordFieldDefinitions(data);
        assertNotNull(recordSchemaStr);
        Schema recordSchema = new Schema.Parser().parse(recordSchemaStr);

        // then
        System.out.println(recordSchemaStr);

        assertTrue(checkType(recordSchema, "boolField", "boolean"));
        assertTrue(checkType(recordSchema, "longField1", "long"));
        assertTrue(checkType(recordSchema, "shortField1", "int")); // short maps to int in Avro
        assertTrue(checkType(recordSchema, "doubleField", "double"));

        assertTrue(checkType(recordSchema, "boolField", "boolean"));
        assertTrue(checkType(recordSchema, "longField1", "long"));
        assertTrue(checkType(recordSchema, "shortField1", "int")); // short maps to int in Avro
        assertTrue(checkType(recordSchema, "doubleField", "double"));
        //check for all data fields
        verifyFields(data, recordSchema);
    }

    @Test
    public void testEncodeSchemaWithNullableRecordVersion() {
        // given
        DataServiceEncoderImpl encoder = new DataServiceEncoderImpl(entityKeyDefSchema, partitionKeyDefSchema,
                timeKeyDefSchema, recordVersionKeyDefSchemaNullable, timeConverter);
        ImmutableData data = createTestCmwData(1);
        DataBuilder builder = ImmutableData.builder();
        builder.addAll(data.getEntries());
        builder.add(ImmutableEntry.ofNull("record_version1", STRING));

        // when
        String recordSchemaStr = encoder.encodeRecordFieldDefinitions(data);
        assertNotNull(recordSchemaStr);
        Schema recordSchema = new Schema.Parser().parse(recordSchemaStr);

        // then
        System.out.println(recordSchemaStr);

        assertTrue(checkType(recordSchema, "boolField", "boolean"));
        assertTrue(checkType(recordSchema, "longField1", "long"));
        assertTrue(checkType(recordSchema, "shortField1", "int")); // short maps to int in Avro
        assertTrue(checkType(recordSchema, "doubleField", "double"));

        assertTrue(checkType(recordSchema, "boolField", "boolean"));
        assertTrue(checkType(recordSchema, "longField1", "long"));
        assertTrue(checkType(recordSchema, "shortField1", "int")); // short maps to int in Avro
        assertTrue(checkType(recordSchema, "doubleField", "double"));

        //check for all data fields
        verifyFields(data, recordSchema);
    }

    @Test(expected = IllegalRecordRuntimeException.class)
    public void testEncodeSchemaWithMissingRecordVersionField() {
        // given
        DataServiceEncoderImpl encoder = new DataServiceEncoderImpl(entityKeyDefSchema, partitionKeyDefSchema,
                timeKeyDefSchema, recordVersionKeyDefSchemaNullable, timeConverter);
        ImmutableData data = createTestCmwData(null);

        // when
        encoder.encodeRecordFieldDefinitions(data);

    }

    @Test
    public void testEncodeSchemaWithRecordVersion() {
        // given
        DataServiceEncoderImpl encoder = new DataServiceEncoderImpl(entityKeyDefSchema, partitionKeyDefSchema,
                timeKeyDefSchema, recordVersionKeyDefSchemaNullable, timeConverter);
        ImmutableData data = createTestCmwData(20);

        // when
        String recordSchemaStr = encoder.encodeRecordFieldDefinitions(data);
        assertNotNull(recordSchemaStr);
        Schema recordSchema = new Schema.Parser().parse(recordSchemaStr);

        // then
        System.out.println(recordSchemaStr);
        assertTrue(checkType(recordSchema, "boolField", "boolean"));
        assertTrue(checkType(recordSchema, "longField1", "long"));
        assertTrue(checkType(recordSchema, "shortField1", "int")); // short maps to int in Avro
        assertTrue(checkType(recordSchema, "doubleField", "double"));
        assertTrue(checkType(recordSchema, "record_version1", "string"));
        assertTrue(checkType(recordSchema, "record_version2", "int"));

        //check for all data fields
        verifyFields(data, recordSchema);

    }

    @Test(expected = UnsupportedOperationException.class)
    public void testUnsupportedMultiArrayOfDifferentData() {
        // given
        DataServiceEncoderImpl encoder = new DataServiceEncoderImpl(entityKeyDefSchema, partitionKeyDefSchema,
                timeKeyDefSchema, recordVersionKeyDefSchemaNullable, timeConverter);
        DataBuilder builder1 = ImmutableData.builder();
        builder1.add("field1", true);
        DataBuilder builder2 = ImmutableData.builder();
        builder2.add("field2", 1);

        DataBuilder builder = ImmutableData.builder();
        builder.add("multiarrayOfData",
                new ImmutableData[] { builder1.build(), builder2.build(), builder1.build(), builder2.build() }, 2, 2);

        encoder.encodeRecordFieldDefinitions(builder.build());
        // then
        // exception

    }

    @Test
    public void testEncodeSchemaWithNoRecordVersionSchema() {
        // given
        DataServiceEncoderImpl encoder = new DataServiceEncoderImpl(entityKeyDefSchema, partitionKeyDefSchema,
                timeKeyDefSchema, null, timeConverter);
        ImmutableData data = createTestCmwData(null);

        // when
        String recordSchemaStr = encoder.encodeRecordFieldDefinitions(data);
        assertNotNull(recordSchemaStr);
        Schema recordSchema = new Schema.Parser().parse(recordSchemaStr);

        // then
        System.out.println(recordSchemaStr);
        assertTrue(checkType(recordSchema, "boolField", "boolean"));
        assertTrue(checkType(recordSchema, "longField1", "long"));
        assertTrue(checkType(recordSchema, "shortField1", "int")); // short maps to int in Avro
        assertTrue(checkType(recordSchema, "doubleField", "double"));
        //check for all data fields
        verifyFields(data, recordSchema);

    }

    @Test
    public void testEntityIdAndPartitionIdPassedFromRecord() {
        // given
        DataServiceEncoderImpl encoder = new DataServiceEncoderImpl(entityKeyDefSchema, partitionKeyDefSchema,
                timeKeyDefSchema, null, timeConverter);
        ImmutableData data = createTestCmwData(null, 1L, 2L);

        // when
        String recordSchemaStr = encoder.encodeRecordFieldDefinitions(data);
        assertNotNull(recordSchemaStr);
        Schema recordSchema = new Schema.Parser().parse(recordSchemaStr);

        // then
        assertEquals(ENTITY_ID.getSchema(), recordSchema.getField(ENTITY_ID.getFieldName()).schema());
        assertEquals(PARTITION_ID.getSchema(), recordSchema.getField(PARTITION_ID.getFieldName()).schema());
        //check for all data fields
        verifyFields(data, recordSchema);

    }

    @Test(expected = IllegalRecordRuntimeException.class)
    public void testNoFieldsInNestedRecord() {
        // given
        DataServiceEncoderImpl encoder = new DataServiceEncoderImpl(entityKeyDefSchema, partitionKeyDefSchema,
                timeKeyDefSchema, null, timeConverter);
        ImmutableData data = createTestCmwData(null, null, null, false);

        // when
        String recordSchemaStr = encoder.encodeRecordFieldDefinitions(data);
        assertNotNull(recordSchemaStr);
        Schema recordSchema = new Schema.Parser().parse(recordSchemaStr);

        // then
        //check for all data fields

    }

    @Test(expected = UnsupportedOperationException.class)
    public void testUnsupportedArrayOfDifferentData() {
        // given
        DataServiceEncoderImpl encoder = new DataServiceEncoderImpl(entityKeyDefSchema, partitionKeyDefSchema,
                timeKeyDefSchema, recordVersionKeyDefSchemaNullable, timeConverter);
        DataBuilder builder1 = ImmutableData.builder();
        builder1.add("field1", true);
        DataBuilder builder2 = ImmutableData.builder();
        builder2.add("field2", 1);

        DataBuilder builder = ImmutableData.builder();
        builder.add("arrayOfData",
                new ImmutableData[] { builder1.build(), builder2.build(), builder1.build(), builder2.build() });

        // when
        encoder.encodeRecordFieldDefinitions(builder.build());

        // then
        // exception

    }

    @Test(expected = UnsupportedOperationException.class)
    public void testUnsupportedArray2DOfDifferentData() {
        // given
        DataServiceEncoderImpl encoder = new DataServiceEncoderImpl(entityKeyDefSchema, partitionKeyDefSchema,
                timeKeyDefSchema, recordVersionKeyDefSchemaNullable, timeConverter);
        DataBuilder builder1 = ImmutableData.builder();
        builder1.add("field1", true);
        DataBuilder builder2 = ImmutableData.builder();
        builder2.add("field2", 1);

        DataBuilder builder = ImmutableData.builder();
        builder.add("arrayOfData",
                new ImmutableData[] { builder1.build(), builder2.build(), builder1.build(), builder2.build() }, 1, 2);
        // when
        encoder.encodeRecordFieldDefinitions(builder.build());

        // then
        // exception

    }

    @Test(expected = RuntimeException.class)
    public void testSerializeNoSystemFields() {
        DataServiceEncoderImpl serializer = new DataServiceEncoderImpl(entityKeyDefSchema, partitionKeyDefSchema,
                timeKeyDefSchema, recordVersionKeyDefSchema, timeConverter);
        DataBuilder builder = ImmutableData.builder();
        builder.add("field", 1);
        serializer.encodeEntityKeyValues(builder.build());
    }

    @Test
    public void testEncodeTimeValue() {
        // given
        DataServiceEncoderImpl serializer = new DataServiceEncoderImpl(entityKeyDefSchema, partitionKeyDefSchema,
                timeKeyDefSchema, recordVersionKeyDefSchemaNullable, timeConverter);
        ImmutableData data = createTestCmwData(10);
        // when
        Long timeKeyValue = serializer.encodeTimeKeyValues(data);

        // then
        assertNotNull(timeKeyValue);
        Assert.assertEquals(123456789L, timeKeyValue, 0);

    }

    @Test
    public void testEnums() {
        // given
        DataServiceEncoderImpl serializer = new DataServiceEncoderImpl(entityKeyDefSchema, partitionKeyDefSchema,
                timeKeyDefSchema, recordVersionKeyDefSchemaNullable, timeConverter);
        ImmutableData data = createTestCmwData(1);

        DataBuilder builder = ImmutableData.builder();
        builder.addAll(data.getEntries());
        // EnumValue
        Map<String, Long> symbols = new ImmutableMap.Builder<String, Long>().put("ala", 1L).put("ma", 2L)
                .put("kota", 3L).build();
        EnumDefinition def = EnumFactory.createDefinition("symbols", symbols);

        // EnumValue[]
        EnumValue[] values = new EnumValue[3];
        values[0] = def.valueOf("ala");
        values[1] = def.valueOf("ma");
        values[2] = def.valueOf("kota");

        // EnumValueSet
        EnumValueSet enumSet = EnumFactory.createEnumSet(def, values);
        builder.add("enumValue", def.valueOf("ala"));
        builder.add("enumValues", values);
        builder.add("enumValueSet", enumSet);

        data = builder.build();
        String schema = serializer.encodeRecordFieldDefinitions(data);
        assertNotNull(schema);

        String recordSchemaStr = serializer.encodeRecordFieldDefinitions(data);
        when(recordData.getData()).thenReturn(data);
        SchemaData schemaData = SchemaData.builder().id(1).schema(recordSchemaStr).build();
        SortedSet<EntityHistoryData> histSetMock = mock(SortedSet.class);
        when(entityData.getEntityHistoryData()).thenReturn(histSetMock);
        EntityHistoryData entityHistoryDataMock = mock(EntityHistoryData.class);
        when(histSetMock.first()).thenReturn(entityHistoryDataMock);
        when(entityHistoryDataMock.getSchemaData()).thenReturn(schemaData);

        byte[] output = serializer.apply(recordData);
        GenericRecord record = deserialize(output, schemaData);
        assertNotNull(record);

        String value = record.get("enumValue").toString();
        assertEquals("ala", value);

        GenericData.Record enumValues = (GenericData.Record) record.get("enumValues");
        assertNotNull(enumValues);
        Object elements = enumValues.get("elements");
        assertNotNull(elements);
        assertEquals(Arrays.stream(values).map(EnumValue::getName).collect(Collectors.toSet()),
                Arrays.stream(((GenericData.Array) elements).toArray()).map(Object::toString)
                        .collect(Collectors.toSet()));
        Object dims = enumValues.get("dimensions");
        assertNotNull(dims);
        assertEquals(Collections.singleton(String.valueOf(values.length)),
                Arrays.stream(((GenericData.Array) dims).toArray()).map(Object::toString).collect(Collectors.toSet()));

        GenericData.Record enumValueSet = (GenericData.Record) record.get("enumValueSet");
        assertNotNull(enumValueSet);
        elements = enumValueSet.get("elements");
        assertNotNull(elements);
        assertEquals(Arrays.stream(enumSet.toArray()).map(EnumValue::getName).collect(Collectors.toSet()),
                Arrays.stream(((GenericData.Array) elements).toArray()).map(Object::toString)
                        .collect(Collectors.toSet()));
        dims = enumValueSet.get("dimensions");
        assertNotNull(dims);
        assertEquals(Collections.singleton(String.valueOf(enumSet.toArray().length)),
                Arrays.stream(((GenericData.Array) dims).toArray()).map(Object::toString).collect(Collectors.toSet()));
    }

    private ImmutableData createTestCmwData(Integer recordVersion) {
        return createTestCmwData(recordVersion, null, null, true);
    }

    private ImmutableData createTestCmwData(Integer recordVersion, Long entityId, Long partitionId) {
        return createTestCmwData(recordVersion, entityId, partitionId, true);
    }

    private ImmutableData createTestCmwData(Integer recordVersion, Long entityId, Long partitionId, boolean addFields) {

        DataBuilder builder = ImmutableData.builder();

        if (entityId != null) {
            builder.add(SystemFields.NXC_ENTITY_ID.getValue(), entityId);
        }

        if (partitionId != null) {
            builder.add(SystemFields.NXC_PARTITION_ID.getValue(), entityId);
        }

        builder.add("device", "dev1");
        builder.add("property", "prop1");
        builder.add("class", "class1");
        builder.add("_p_t_", 123456789L);

        builder.add("boolField", true);
        builder.add("byteField1", (byte) 2);
        builder.add("shortField1", (short) 1);
        builder.add("intField1", 1);
        builder.add("longField1", 1L);
        builder.add("longField2", 1L);
        builder.add("floatField1", 1.0f);
        builder.add("doubleField", 1.0d);
        builder.add("stringField1", "string value");

        builder.add("boolArrayField", new boolean[] { true, false, true });
        builder.add("byteArrayField", new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
        builder.add("shortArrayField", new short[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
        builder.add("intArrayField", new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
        builder.add("longArrayField", new long[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
        builder.add("floatArrayField", new float[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
        builder.add("doubleArrayField", new double[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
        builder.add("stingArrayField", new String[] { "aaa", "bbb" });

        builder.add("boolArray2DField1", new boolean[] { true, false }, 1, 2);
        builder.add("byteArray2DField2", new byte[] { 1, 2 }, 1, 2);
        builder.add("shortArray2DField2", new short[] { 1, 2 }, 1, 2);
        builder.add("intArray2DField2", new int[] { 1, 2 }, 1, 2);
        builder.add("longArray2DField2", new long[] { 1, 2 }, 1, 2);
        builder.add("doubleArray2DField2", new double[] { 1, 2 }, 1, 2);
        builder.add("floatArray2DField2", new float[] { 1, 2 }, 1, 2);
        builder.add("stringArray2DField2", new String[] { "aaaa", "bbbb" }, 1, 2);

        builder.add("boolMultiArrayField1", new boolean[] { true, false, true, false }, 2, 2);
        builder.add("byteMultiArrayField1", new byte[] { 1, 2, 3, 4 }, 2, 2);
        builder.add("shortMultiArrayField1", new short[] { 1, 2, 3, 4 }, 2, 2);
        builder.add("intMultiArrayField1", new int[] { 1, 2, 3, 4 }, 2, 2);
        builder.add("longMultiArrayField1", new long[] { 1, 2, 3, 4 }, 2, 2);
        builder.add("doubleMultiArrayField1", new double[] { 1, 2, 3, 4 }, 2, 2);
        builder.add("floatMultiArrayField1", new float[] { 1, 2, 3, 4 }, 2, 2);
        builder.add("stringMultiArrayField1", new String[] { "aaaa", "bbbb", "cccc", "dddd" }, 2, 2);
        builder.add("stringMultiArrayField2", new String[] { "aaaa", "bbbb", "cccc", "dddd" }, 2, 2);

        builder.add("ddfField1",
                DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0 }, new double[] { 1.0, 2.0 }));
        builder.add("ddfField2",
                DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0 }, new double[] { 1.0, 2.0 }));

        builder.add("ddfArrayField1", new DiscreteFunction[] {
                DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0 }, new double[] { 1.0, 2.0 }),
                DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0, 3.0 }, new double[] { 1.0, 2.0, 3.0 }) });

        builder.add("ddfArrayField2", new DiscreteFunction[] {
                DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0 }, new double[] { 1.0, 2.0 }),
                DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0, 3.0 }, new double[] { 1.0, 2.0, 3.0 }) });

        builder.add("ddfMultiArrayField", new DiscreteFunction[] {
                        DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0 }, new double[] { 1.0, 2.0 }),
                        DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0, 3.0 }, new double[] { 1.0, 2.0, 3.0 }),
                        DataFactory.createDiscreteFunction(new double[] { 1.1, 2.1, 3.1 }, new double[] { 1.2, 2.2, 3.2 }) }, 1,
                1, 1);

        DataBuilder secondNestedBuilder = ImmutableData.builder();
        secondNestedBuilder.add("longField1", 1L);
        secondNestedBuilder.add("discreteFunctionField",
                DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0 }, new double[] { 1.0, 2.0 }));
        secondNestedBuilder.add("multiArrayLong", new short[] { 1, 2, 3, 4 }, 2, 2);

        DataBuilder nestedBuilder = ImmutableData.builder();
        if (addFields) {
            nestedBuilder.add("boolField", true);
            nestedBuilder.add("longField", 1L);
            // here nothing happens, the record version is ignored.
            nestedBuilder.add("record_version1", "ver1");
            nestedBuilder.add("record_version2", 12);
            nestedBuilder.add("boolArray2DField2", new boolean[] { true, false }, 1, 2);
            nestedBuilder.add("byteArrayField", new byte[] { 1, 2, 3, 4 });
            nestedBuilder.add("dataField", secondNestedBuilder.build());
        }
        builder.add("dataField", nestedBuilder.build());

        if (recordVersion != null) {
            builder.add("record_version1", "ver1");
            builder.add("record_version2", recordVersion);
        }
        return builder.build();
    }

    private static GenericRecord deserialize(byte[] bytes, SchemaData schemaData) {
        BytesToGenericRecordDecoder decoder = new BytesToGenericRecordDecoder(schemaId -> schemaData);
        return decoder.apply(bytes);
    }
}
