package cern.nxcals.client;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.common.domain.SystemData;
import cern.nxcals.service.client.api.SystemService;
import cern.nxcals.service.client.api.internal.InternalEntityService;
import org.apache.kafka.clients.producer.Producer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

/**
 * Created by jwozniak on 21/12/16.
 */
@RunWith(MockitoJUnitRunner.class)
public class PublisherFactoryTest extends Base {

    private PublisherFactory factory;
    @Mock
    private InternalEntityService entityService;
    @Mock
    private SystemService systemService;
    @Mock
    private SystemData systemData;

    @Mock
    private Producer<byte[], byte[]> kafkaProducer;

    @Before
    public void setUp() throws Exception {
        when(systemService.findByName("TEST")).thenReturn(systemData);
        when(systemData.getRecordVersionKeyDefinitions()).thenReturn(recordVersionKeyDefSchema.toString());
        when(systemData.getEntityKeyDefinitions()).thenReturn(entityKeyDefSchema.toString());
        when(systemData.getPartitionKeyDefinitions()).thenReturn(partitionKeyDefSchema.toString());
        when(systemData.getTimeKeyDefinitions()).thenReturn(timeKeyDefSchema.toString());
        when(systemData.getName()).thenReturn("TEST");
        factory = new PublisherFactory(entityService, systemService, () -> kafkaProducer);
    }

    @Test
    public void shouldCreatePublisher() throws Exception {
        Publisher<Map<String, Object>> publisher = factory
                .createPublisher("TEST", v -> ImmutableData.builder().build());
        assertNotNull(publisher);
    }

    @Test(expected = NullPointerException.class)
    public void shouldNotCreatePublisherWithNullSystem() throws Exception {
        Publisher<Map<String, Object>> publisher = factory.createPublisher(null, v -> ImmutableData.builder().build());
        assertNotNull(publisher);
    }

    @Test(expected = NullPointerException.class)
    public void shouldNotCreatePublisherWithNullFunction() throws Exception {
        Publisher<Map<String, Object>> publisher = factory.createPublisher("TEST", null);
        assertNotNull(publisher);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCreatePublisherWithUnknownSystem() throws Exception {
        when(systemService.findByName("UNKNOWN")).thenReturn(null);
        Publisher<Map<String, Object>> publisher = factory
                .createPublisher("UNKNOWN", v -> ImmutableData.builder().build());

    }

}