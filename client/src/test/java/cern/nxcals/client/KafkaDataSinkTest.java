/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.client;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.common.domain.EntityData;
import cern.nxcals.common.domain.PartitionData;
import cern.nxcals.common.domain.SchemaData;
import com.google.common.collect.ImmutableMap;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.PartitionInfo;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by jwozniak on 19/12/16.
 */
@RunWith(MockitoJUnitRunner.class)
public class KafkaDataSinkTest {
    private static final byte[] VALUE = new byte[0];
    private static final int MAX_RECORD_SIZE = 2 * 1024 * 1024;

    private KafkaDataSink kafkaSink;
    private Producer<byte[], byte[]> producer;
    private static final String topicName = "TEST-TOPIC";
    private PartitionInfo part1;
    private PartitionInfo part2;
    private PartitionInfo part3;

    @Before
    public void setup() {
        part1 = mock(PartitionInfo.class);
        part2 = mock(PartitionInfo.class);
        part3 = mock(PartitionInfo.class);

        List<PartitionInfo> parts = new ArrayList<>();
        parts.add(part1);
        parts.add(part2);
        parts.add(part3);

        producer = mock(Producer.class);
        when(producer.partitionsFor(topicName)).thenReturn(parts);

        this.kafkaSink = new KafkaDataSink(topicName, producer, (RecordData data) -> VALUE, MAX_RECORD_SIZE);
    }

    @Test
    public void shouldNotPublishTooBigRecord() {

        this.kafkaSink = new KafkaDataSink(topicName, producer, (RecordData data) -> new byte[MAX_RECORD_SIZE + 1],
                MAX_RECORD_SIZE);
        EntityData entityData = mock(EntityData.class);
        when(entityData.getId()).thenReturn(1L);
        when(entityData.getEntityKeyValues()).thenReturn(ImmutableMap.of("entityKeyValues", "entityKeyValues"));
        ImmutableData data = mock(ImmutableData.class);
        RecordData recordData = new RecordData(entityData, data);

        kafkaSink.send(recordData, (r, e) -> {
            Assert.assertNotNull(e);
            Assert.assertNull(r);
            Assert.assertTrue(e instanceof RecordTooBigRuntimeException);
        });
    }

    @Test
    public void shouldPublish() {
        RecordData data = mock(RecordData.class);
        EntityData entityData = mock(EntityData.class);
        SchemaData schemaData = mock(SchemaData.class);
        PartitionData partitionData = mock(PartitionData.class);

        when(data.getEntityData()).thenReturn(entityData);
        when(entityData.getSchemaData()).thenReturn(schemaData);
        when(entityData.getPartitionData()).thenReturn(partitionData);
        when(entityData.getId()).thenReturn(Long.valueOf(1L));
        when(partitionData.getId()).thenReturn(10L);
        when(schemaData.getId()).thenReturn(100L);

        when(producer.send(any(ProducerRecord.class), any(Callback.class))).thenAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                ProducerRecord<byte[], byte[]> record = (ProducerRecord<byte[], byte[]>) args[0];
                Callback callback = (Callback) args[1];
                Assert.assertEquals(record.topic(), topicName);
                Assert.assertEquals(record.value(), VALUE);
                //compare the byte arrays, same as entityData.getId() to bytes
                byte[] recordKey = Long.valueOf(1L).toString().getBytes();
                for (int i = 0; i < recordKey.length; ++i) {
                    Assert.assertEquals(record.key()[i], recordKey[i]);
                }

                callback.onCompletion(null, null);
                return null;
            }
        });

        kafkaSink.send(data, (r, e) -> {
            Assert.assertNotNull(r);
            Assert.assertNull(e);
        });
        verify(this.producer, times(1)).send(any(ProducerRecord.class), any(Callback.class));
    }

    @Test
    public void shouldCloseProducerOnClose() throws IOException {
        this.kafkaSink.close();
        verify(this.producer, times(1)).close();
    }
}
