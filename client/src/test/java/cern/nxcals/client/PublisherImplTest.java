/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.client;

import cern.cmw.datax.EntryType;
import cern.cmw.datax.ImmutableData;
import cern.cmw.datax.ImmutableEntry;
import cern.nxcals.common.SystemFields;
import cern.nxcals.common.domain.EntityData;
import cern.nxcals.service.client.api.internal.InternalEntityService;
import cern.nxcals.service.client.domain.KeyValues;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.function.Function;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyMap;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by jwozniak on 20/12/16.
 */
public class PublisherImplTest {
    private static final Executor CURRENT_THREAD_EXECUTOR = Runnable::run;
    private static final Map<String, Object> ENTITY_KEY_VALUES = ImmutableMap.of("entity", "value1");
    private static final Map<String, Object> PARTITION_KEY_VALUES = ImmutableMap.of("partition", "value");
    private static final String SCHEMA = "SCHEMA";
    private static KeyValues entityKeyValues;
    private static KeyValues partitionKeyValues;
    private Function<Map<String, Object>, ImmutableData> converter;
    private InternalEntityService entityService;
    private DataServiceEncoderImpl encoder;
    private DataSink<RecordData, DefaultCallback> dataSink;
    private Publisher<Map<String, Object>> publisher;
    private ImmutableData cmwData;
    private EntityData entityData;
    private ImmutableEntry entityIdEntry;
    private ImmutableEntry partitionIdEntry;
    private Map<String, Object> data;

    private static long SYSTEM_ID = 1L;
    private static long ENTITY_ID = 1L;
    private static long PARTITION_ID = 2L;
    private static long RECORD_TIMESTAMP = 100L;

    @Before
    public void setUp() {

        entityKeyValues = mock(KeyValues.class);
        when(entityKeyValues.getId()).thenReturn("");
        when(entityKeyValues.getKeyValues()).thenReturn(ENTITY_KEY_VALUES);

        partitionKeyValues = mock(KeyValues.class);
        when(partitionKeyValues.getId()).thenReturn(null);
        when(partitionKeyValues.getKeyValues()).thenReturn(PARTITION_KEY_VALUES);

        data = new HashMap<>();
        data.put("field1", 1L);
        data.put("field2", 2L);

        converter = mock(Function.class);
        entityService = mock(InternalEntityService.class);
        encoder = mock(DataServiceEncoderImpl.class);
        dataSink = mock(DataSink.class);
        cmwData = mock(ImmutableData.class);
        entityData = mock(EntityData.class);
        entityIdEntry = mock(ImmutableEntry.class);
        partitionIdEntry = mock(ImmutableEntry.class);
        when(converter.apply(anyMap())).thenReturn(cmwData);
        when(cmwData.getEntryCount()).thenReturn(10);

        when(encoder.encodeEntityKeyValues(cmwData)).thenReturn(entityKeyValues);
        when(encoder.encodePartitionKeyValues(cmwData)).thenReturn(partitionKeyValues);
        when(encoder.encodeRecordFieldDefinitions(cmwData)).thenReturn(SCHEMA);
        when(encoder.encodeTimeKeyValues(cmwData)).thenReturn(Long.valueOf(100L));
        when(entityService.findOrCreateEntityFor(1L, entityKeyValues, partitionKeyValues, SCHEMA, 100L))
                .thenReturn(entityData);
        publisher = new PublisherImpl<>(1, converter, entityService, encoder, dataSink);
    }

    @Test
    public void shouldCloseSinkOnClose() throws Exception {
        publisher.close();
        verify(dataSink, times(1)).close();
    }

    @Test
    public void shouldPublish() {
        Map<String, Object> data = new HashMap<>();
        data.put("field1", Long.valueOf(1L));
        data.put("field2", Long.valueOf(2L));
        CompletableFuture<Result> future = publisher.publishAsync(data, CURRENT_THREAD_EXECUTOR);

        assertNotNull(future);
        future.handle((o, e) -> {
            assertNotNull(o);
            assertNull(e);
            return o;
        });
        verify(dataSink, times(1)).send(any(RecordData.class), any(DefaultCallback.class));

    }

    @Test
    public void shouldPublishWithEntityIdAndPartitionId() {
        data.put(SystemFields.NXC_ENTITY_ID.getValue(), ENTITY_ID);
        data.put(SystemFields.NXC_PARTITION_ID.getValue(), PARTITION_ID);
        when(cmwData.getEntry(SystemFields.NXC_ENTITY_ID.getValue())).thenReturn(entityIdEntry);
        when(cmwData.getEntry(SystemFields.NXC_PARTITION_ID.getValue())).thenReturn(partitionIdEntry);
        when(entityIdEntry.getAs(EntryType.INT64)).thenReturn(ENTITY_ID);
        when(partitionIdEntry.getAs(EntryType.INT64)).thenReturn(PARTITION_ID);
        when(entityService.findOrCreateEntityFor(SYSTEM_ID, ENTITY_ID, PARTITION_ID, SCHEMA, RECORD_TIMESTAMP))
                .thenReturn(entityData);

        CompletableFuture<Result> future = publisher.publishAsync(data,CURRENT_THREAD_EXECUTOR);

        assertNotNull(future);
        future.exceptionally((exception)-> {exception.printStackTrace(); return null;});
        assertFalse(future.isCompletedExceptionally());

        verify(dataSink, times(1)).send(any(RecordData.class), any(DefaultCallback.class));

    }

    @Test
    public void shouldFailToPublishWithEntityIdAndMissingPartitionId() {
        data.put(SystemFields.NXC_ENTITY_ID.getValue(), ENTITY_ID);
        when(cmwData.getEntry(SystemFields.NXC_ENTITY_ID.getValue())).thenReturn(entityIdEntry);
        when(entityIdEntry.getAs(EntryType.INT64)).thenReturn(ENTITY_ID);
        when(entityService.findOrCreateEntityFor(SYSTEM_ID, ENTITY_ID, PARTITION_ID, SCHEMA, RECORD_TIMESTAMP))
                .thenReturn(entityData);

        CompletableFuture<Result> future = publisher.publishAsync(data,CURRENT_THREAD_EXECUTOR);

        assertNotNull(future);
        future.exceptionally((exception)-> {
            assertEquals(IllegalRecordRuntimeException.class, exception.getClass());
            return null;
        });
        assertTrue(future.isCompletedExceptionally());
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowOnPublishNull() {
        Map<String, Object> data = null;
        publisher.publish(data);
    }

    @Test
    public void shouldReturnExceptionFutureOnInvalidRecordTimestamp() {

        when(encoder.encodeTimeKeyValues(cmwData)).thenReturn(Long.valueOf(0));

        CompletableFuture<Result> future = publisher.publishAsync(data, CURRENT_THREAD_EXECUTOR);
        assertNotNull(future);

        assertTrue(future.isCompletedExceptionally());
        future.handle((val, ex) -> {
            assertEquals(ex.getClass(), IllegalRecordRuntimeException.class);
            return val;
        });

        verify(dataSink, times(0)).send(any(RecordData.class), any(DefaultCallback.class));
    }

    @Test
    public void shouldReturnExceptionFutureOnRecordFieldsMaxSizeExceeded() {
        when(cmwData.getEntryCount()).thenReturn(10000);

        CompletableFuture<Result> future = publisher.publishAsync(data, CURRENT_THREAD_EXECUTOR);
        assertNotNull(future);

        assertTrue(future.isCompletedExceptionally());
        future.handle((val, ex) -> {
            assertEquals(ex.getClass(), IllegalRecordRuntimeException.class);
            return val;
        });

        verify(dataSink, times(0)).send(any(RecordData.class), any(DefaultCallback.class));
    }

    @Test
    public void shouldReturnExceptionFutureOnInternalConverterException() {

        when(converter.apply(anyMap())).thenThrow(new NullPointerException());

        CompletableFuture<Result> future = publisher.publishAsync(data, CURRENT_THREAD_EXECUTOR);
        assertNotNull(future);

        assertTrue(future.isCompletedExceptionally());
        verify(dataSink, times(0)).send(any(RecordData.class), any(DefaultCallback.class));
    }

    @Test
    public void shouldReturnExceptionFutureOnInternalEncoderException() {

        when(encoder.encodeEntityKeyValues(cmwData)).thenThrow(new NullPointerException());

        CompletableFuture<Result> future = publisher.publishAsync(data, CURRENT_THREAD_EXECUTOR);
        assertNotNull(future);

        assertTrue(future.isCompletedExceptionally());
        verify(dataSink, times(0)).send(any(RecordData.class), any(DefaultCallback.class));
    }

    @Test
    public void shouldReturnExceptionFutureOnInternalEntityServiceException() {

        when(entityService.findOrCreateEntityFor(SYSTEM_ID, entityKeyValues, partitionKeyValues, SCHEMA, 100L))
                .thenThrow(new NullPointerException());
        CompletableFuture<Result> future = publisher.publishAsync(data, CURRENT_THREAD_EXECUTOR);
        assertNotNull(future);

        assertTrue(future.isCompletedExceptionally());
        verify(dataSink, times(0)).send(any(RecordData.class), any(DefaultCallback.class));
    }

    @Test
    public void shouldReturnExceptionFutureOnInternalSinkException() {

        doThrow(new NullPointerException()).when(dataSink).send(any(RecordData.class), any(DefaultCallback.class));

        CompletableFuture<Result> future = publisher.publishAsync(data, CURRENT_THREAD_EXECUTOR);
        assertNotNull(future);

        assertTrue(future.isCompletedExceptionally());
        verify(dataSink, times(1)).send(any(RecordData.class), any(DefaultCallback.class));
    }
}