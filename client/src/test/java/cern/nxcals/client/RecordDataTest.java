package cern.nxcals.client;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.common.domain.EntityData;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

/**
 * Created by jwozniak on 20/12/16.
 */
public class RecordDataTest {
    @Test
    public void shouldCorrectlyCreateRecordData() throws Exception {
        EntityData entityData = mock(EntityData.class);
        ImmutableData data1 = mock(ImmutableData.class);
        RecordData data = new RecordData(entityData, data1);
        assertEquals(data.getEntityData(), entityData);
        assertEquals(data.getData(), data1);
    }

}