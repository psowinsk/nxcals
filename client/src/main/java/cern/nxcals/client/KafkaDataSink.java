/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.client;

import cern.cmw.data.Data;
import cern.nxcals.common.domain.EntityData;
import com.google.common.base.Charsets;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.PartitionInfo;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * This class uses the @see Serializer to convert the @see {@link Data} into bytes to be sent to Kafka @see
 * {@link Publisher}.
 */
@Slf4j
class KafkaDataSink implements DataSink<RecordData, DefaultCallback> {
    private static final String RECORD_TOO_BIG_MSG_PATTERN = "Record for entity={0} with id={1,number,#} too big, size={2,number,#}";
    private static final Result DUMMY_RESULT = new Result() {
    };
    private final int maxRecordSize;
    private final Producer<byte[], byte[]> kafkaProducer;
    private final Function<RecordData, byte[]> serializer;
    private final String topicName;
    private final PartitionInfo[] partitions;

    KafkaDataSink(String topicName, Producer<byte[], byte[]> producer, Function<RecordData, byte[]> serializer,
            int maxRecordSize) {
        this.topicName = Objects.requireNonNull(topicName);
        this.kafkaProducer = Objects.requireNonNull(producer);
        this.serializer = Objects.requireNonNull(serializer);
        this.partitions = getPartitions(producer, topicName);
        this.maxRecordSize = maxRecordSize;

    }

    private static PartitionInfo[] getPartitions(Producer<byte[], byte[]> producer, String topicName) {
        return producer.partitionsFor(topicName).toArray(new PartitionInfo[] {});
    }

    @Override
    public void close() throws IOException {
        this.kafkaProducer.close();
    }

    @Override
    public void send(RecordData record, DefaultCallback callback) {
        Objects.requireNonNull(callback);
        try {
            byte[] bytes = this.serializer.apply(record);
            EntityData entityData = record.getEntityData();
            if (bytes.length > this.maxRecordSize) {
                String msg = MessageFormat
                        .format(RECORD_TOO_BIG_MSG_PATTERN, entityData.getEntityKeyValues(), entityData.getId(),
                                bytes.length);
                callback.accept(null, new RecordTooBigRuntimeException(msg));
                return;
            }
            sendToKafka(record.getEntityData().getId(), bytes, getPartitionNumber(record),
                    (recordMetadata, exception) -> callback.accept(DUMMY_RESULT, exception));
        } catch (Exception exception) {
            callback.accept(null, exception);
        }
    }

    /**
     * This generates the Kafka topic partition number based on NXCALS partition id + schema id.
     * It aims at using the same partition for data that are stored in the same directory to avoid
     * creating many files (as many as Kafka partitions used for a given directory in hdfs).
     */
    private Integer getPartitionNumber(RecordData record) {
        int index = (int) (
                (record.getEntityData().getPartitionData().getId() + record.getEntityData().getSchemaData().getId())
                        % partitions.length);
        return partitions[index].partition();
    }

    private void sendToKafka(Long recordKey, byte[] bytes, Integer partitionNumber,
            BiConsumer<RecordMetadata, Exception> callback) {
        log.trace("Sending to kafka entityId={}, size={}, kafkaPartition={}", recordKey, bytes.length, partitionNumber);
        this.kafkaProducer
                .send(new ProducerRecord<>(this.topicName, partitionNumber,
                                recordKey.toString().getBytes(Charsets.UTF_8), bytes),
                        callback::accept);
    }

}