/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.client;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.common.domain.EntityData;
import lombok.Data;
import lombok.NonNull;

/**
 * Represents the internal record meta information and the data needed to process it in the NXCALS system.
 *
 * @author jwozniak
 */
@Data
class RecordData {
    @NonNull
    private final EntityData entityData;
    @NonNull
    private final ImmutableData data;
}