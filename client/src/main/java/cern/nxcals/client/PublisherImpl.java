/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.client;

import cern.cmw.data.Data;
import cern.cmw.datax.EntryType;
import cern.cmw.datax.ImmutableData;
import cern.cmw.datax.ImmutableEntry;
import cern.nxcals.common.SystemFields;
import cern.nxcals.common.domain.EntityData;
import cern.nxcals.common.utils.Tuple2;
import cern.nxcals.service.client.api.internal.InternalEntityService;
import cern.nxcals.service.client.domain.KeyValues;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Function;

import static java.util.Objects.requireNonNull;

/**
 * Default implementation of {@link Publisher}. The responsibility of this class is to use the @see {@code conventer} to
 * convert the user data into @see {@link Data} and to obtain the necessary meta information for the record. Later on
 * the processing of the record is passed over to the @link Sink.
 */
@Slf4j
class PublisherImpl<V> implements Publisher<V> {
    private static final Executor CURRENT_THREAD_EXECUTOR = Runnable::run;
    private static final int MAX_FIELDS = 1000;
    private final Function<V, ImmutableData> converter;
    private final InternalEntityService entityService;
    private final DataSink<RecordData, DefaultCallback> sink;
    private final long systemId;
    private final DataServiceEncoder<KeyValues, KeyValues, String, Long> encoder;

    PublisherImpl(long systemId, Function<V, ImmutableData> converter, InternalEntityService entityService,
            DataServiceEncoder<KeyValues, KeyValues, String, Long> encoder,
            DataSink<RecordData, DefaultCallback> dataSink) {
        this.systemId = systemId;
        this.entityService = requireNonNull(entityService);
        this.converter = requireNonNull(converter);
        this.sink = requireNonNull(dataSink);
        this.encoder = requireNonNull(encoder);
    }

    @Override
    public void close() throws IOException {
        this.sink.close();
    }

    @Override
    public Result publish(V value) {
        return this.publishAsync(value, CURRENT_THREAD_EXECUTOR).join();
    }

    @Override
    public CompletableFuture<Result> publishAsync(V value) {
        return this.publishAsync(value, ForkJoinPool.commonPool());
    }

    @Override
    public CompletableFuture<Result> publishAsync(V value, Executor executor) {
        requireNonNull(executor);
        // should this be validated in the caller thread or asynchronously?
        requireNonNull(value, "Input value must not be null");
        CompletableFuture<Result> result = new CompletableFuture<>();
        executor.execute(() -> {
            try {
                Tuple2<RecordData, Exception> recordData = convertToRecord(value);
                if (recordData.getRight() != null) {
                    result.completeExceptionally(recordData.getRight());
                    return;
                }
                this.sink.send(recordData.getLeft(), (res, except) -> {

                    if (except != null) {
                        result.completeExceptionally(except);
                    } else {
                        result.complete(res);
                    }
                });
            } catch (Exception exception) {
                result.completeExceptionally(exception);
            }
        });
        return result;
    }

    private Tuple2<RecordData, Exception> convertToRecord(V value) {
        ImmutableData data = this.converter.apply(value);
        return this.createRecordData(data);
    }

    private Tuple2<RecordData, Exception> createRecordData(ImmutableData record) {
        if (record.getEntryCount() > MAX_FIELDS) {
            return new Tuple2<>(null, new IllegalRecordRuntimeException(
                    "Record has " + record.getEntryCount() + " fields with limit of " + MAX_FIELDS
                            + ". This large number of fields cannot be processes with Spark at extraction. Please "
                            + "re-model your data."));

        }

        KeyValues cachedRequest = encoder.encodeEntityKeyValues(record);

        Long timestamp = this.encoder.encodeTimeKeyValues(record);
        if (timestamp == null || timestamp <= 0) {
            return new Tuple2<>(null, new IllegalRecordRuntimeException(
                    "Illegal record timestamp for entityKey=" + cachedRequest + " system=" + this.systemId
                            + " timestamp=" + timestamp));
        }

        EntityData entityData = findEntityData(record, cachedRequest, timestamp);
        return new Tuple2<>(new RecordData(entityData, record), null);
    }

    private EntityData findEntityData(ImmutableData record, KeyValues entityCachedRequest, Long timestamp) {
        Long entityId;
        Long partitionId = null;
        //This is like that on purpose to avoid getting both ids when the entityId == null already.
        //We don't check in that case the partitionId, it is ignored (for performance reasons)
        if ((entityId = getId(record, SystemFields.NXC_ENTITY_ID.getValue())) != null) {
            partitionId = getId(record, SystemFields.NXC_PARTITION_ID.getValue());
        }

        if (entityId != null && partitionId != null) {
            //Encoding schema here to check if the required partition fields are ok
            return entityService.findOrCreateEntityFor(this.systemId, entityId, partitionId,
                    this.encoder.encodeRecordFieldDefinitions(record), timestamp);
        } else if (entityId == null) {
            return entityService.findOrCreateEntityFor(this.systemId, entityCachedRequest,
                    this.encoder.encodePartitionKeyValues(record), this.encoder.encodeRecordFieldDefinitions(record),
                    timestamp);
        } else {
            throw new IllegalRecordRuntimeException("Must have both entityId and partitionId set in a record for " +
                    "entityKey=" + entityCachedRequest.getKeyValues() + " system=" + this.systemId + " timestamp="
                    + timestamp);
        }
    }

    private Long getId(ImmutableData record, String fieldName) {
        ImmutableEntry entry = record.getEntry(fieldName);
        return entry != null ? entry.getAs(EntryType.INT64) : null;
    }

}

