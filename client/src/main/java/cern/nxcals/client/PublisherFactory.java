/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.client;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.common.converters.TimeConverterImpl;
import cern.nxcals.common.domain.SystemData;
import cern.nxcals.common.utils.ConfigHolder;
import cern.nxcals.service.client.api.SystemService;
import cern.nxcals.service.client.api.internal.InternalEntityService;
import cern.nxcals.service.client.providers.InternalServiceClientFactory;
import com.google.common.annotations.VisibleForTesting;
import com.typesafe.config.Config;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Schema;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;

import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.function.Supplier;

import static cern.nxcals.common.utils.ConfigHolder.getProperty;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toMap;

/**
 * The main entry point to the NXCALS service. Creates the @see {@link Publisher} instance for a given system name and
 * data converting function.
 */
@Slf4j
public final class PublisherFactory {
    private static final int DEFAULT_MAX_RECORD_SIZE = 10 * 1024 * 1024; //10MB
    private static final Supplier<Producer<byte[], byte[]>> DEFAULT_KAFKA_PRODUCER_SUPPLIER = () -> {
        Map<String, Object> props = getKafkaConfig();
        log.debug("Creating KafkaProducer with config={}", props);
        return new KafkaProducer<>(props);
    };

    private final Supplier<Producer<byte[], byte[]>> kafkaProducerSupplier;
    private InternalEntityService entityService;
    private SystemService systemService;

    private PublisherFactory() {
        this(InternalServiceClientFactory.createEntityService(), InternalServiceClientFactory.createSystemService(),
                DEFAULT_KAFKA_PRODUCER_SUPPLIER);
    }

    @VisibleForTesting
    PublisherFactory(InternalEntityService entityService, SystemService systemService,
            Supplier<org.apache.kafka.clients.producer.Producer<byte[], byte[]>> kafkaProducerSupplier) {
        this.entityService = requireNonNull(entityService);
        this.systemService = requireNonNull(systemService);
        this.kafkaProducerSupplier = requireNonNull(kafkaProducerSupplier);
    }

    private static Map<String, Object> getKafkaConfig() {
        Config config = ConfigHolder.getConfig();
        Config rawProps = config.getConfig("kafka.producer");
        if (rawProps.isEmpty()) {
            throw new IllegalStateException("Cannot find kafka producer properties");
        }
        return rawProps.entrySet().stream().collect(toMap(Entry::getKey, entry -> entry.getValue().unwrapped()));
    }

    /**
     * Creating the publisher from the system name and conversion function.
     *
     * @param systemName         Name of system to publish.
     * @param convertingFunction Function used to convert data.
     * @param <V>                Type of data to publish.
     * @return Publisher which can publish data of type {@code <V>}
     */
    public <V> Publisher<V> createPublisher(String systemName, Function<V, ImmutableData> convertingFunction) {
        requireNonNull(systemName, "System name cannot be null");
        requireNonNull(convertingFunction, "Converting function cannot be null");
        SystemData systemData = systemService.findByName(systemName);
        if (systemData == null) {
            throw new IllegalArgumentException("Unknown system: " + systemName);
        }
        DataServiceEncoderImpl serviceEncoder = createDataToAvroServiceEncoder(systemData);
        DataSink<RecordData, DefaultCallback> dataSink = createInternalDataSink(systemData, serviceEncoder);
        return new PublisherImpl<V>(systemData.getId(), convertingFunction, entityService, serviceEncoder, dataSink);
    }

    private DataServiceEncoderImpl createDataToAvroServiceEncoder(SystemData systemData) {
        String recordVersionSchemaStr = systemData.getRecordVersionKeyDefinitions();
        Schema recordVersionSchema = null;
        if (recordVersionSchemaStr != null) {
            recordVersionSchema = new Schema.Parser().parse(recordVersionSchemaStr);
        }
        return new DataServiceEncoderImpl(new Schema.Parser().parse(systemData.getEntityKeyDefinitions()),
                new Schema.Parser().parse(systemData.getPartitionKeyDefinitions()),
                new Schema.Parser().parse(systemData.getTimeKeyDefinitions()), recordVersionSchema,
                new TimeConverterImpl());
    }

    private DataSink<RecordData, DefaultCallback> createInternalDataSink(SystemData systemData,
            Function<RecordData, byte[]> converter) {
        String topicName = getProperty("publisher.topic_name", systemData.getName());
        int maxRecordSize = getProperty("publisher.max_record_size", DEFAULT_MAX_RECORD_SIZE);
        return new KafkaDataSink(topicName, kafkaProducerSupplier.get(), converter, maxRecordSize);
    }

    /**
     * Factory method.
     *
     * @return New instance of {@link PublisherFactory}
     */
    public static PublisherFactory newInstance() {
        return new PublisherFactory();
    }

}
