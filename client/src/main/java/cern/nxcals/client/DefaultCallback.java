package cern.nxcals.client;

/**
 * Default implementation of {@link Callback} interface.
 */
interface DefaultCallback extends Callback<Result, Exception> {
}
