/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.client;

import avro.shaded.com.google.common.primitives.Booleans;
import cern.cmw.data.DiscreteFunction;
import cern.cmw.datax.EntryType;
import cern.cmw.datax.ImmutableData;
import cern.cmw.datax.ImmutableEntry;
import cern.cmw.datax.enumeration.EnumValue;
import cern.cmw.datax.enumeration.EnumValueSet;
import cern.nxcals.common.avro.GenericRecordToBytesEncoder;
import cern.nxcals.common.converters.TimeConverter;
import cern.nxcals.common.domain.SchemaData;
import cern.nxcals.common.utils.AvroUtils;
import cern.nxcals.common.utils.IllegalCharacterConverter;
import cern.nxcals.service.client.domain.KeyValues;
import cern.nxcals.service.client.domain.impl.GenericRecordKeyValues;
import cern.nxcals.service.client.domain.impl.SimpleKeyValues;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.primitives.Doubles;
import com.google.common.primitives.Floats;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;
import org.apache.avro.Schema;
import org.apache.avro.Schema.Type;
import org.apache.avro.SchemaBuilder;
import org.apache.avro.SchemaBuilder.FieldAssembler;
import org.apache.avro.SchemaParseException;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static cern.cmw.datax.EntryType.BOOL;
import static cern.cmw.datax.EntryType.BOOL_ARRAY;
import static cern.cmw.datax.EntryType.DATA;
import static cern.cmw.datax.EntryType.DATA_ARRAY;
import static cern.cmw.datax.EntryType.DISCRETE_FUNCTION;
import static cern.cmw.datax.EntryType.DISCRETE_FUNCTION_ARRAY;
import static cern.cmw.datax.EntryType.DOUBLE;
import static cern.cmw.datax.EntryType.DOUBLE_ARRAY;
import static cern.cmw.datax.EntryType.ENUM;
import static cern.cmw.datax.EntryType.ENUM_ARRAY;
import static cern.cmw.datax.EntryType.ENUM_SET;
import static cern.cmw.datax.EntryType.FLOAT;
import static cern.cmw.datax.EntryType.FLOAT_ARRAY;
import static cern.cmw.datax.EntryType.INT16;
import static cern.cmw.datax.EntryType.INT16_ARRAY;
import static cern.cmw.datax.EntryType.INT32;
import static cern.cmw.datax.EntryType.INT32_ARRAY;
import static cern.cmw.datax.EntryType.INT64;
import static cern.cmw.datax.EntryType.INT64_ARRAY;
import static cern.cmw.datax.EntryType.INT8;
import static cern.cmw.datax.EntryType.INT8_ARRAY;
import static cern.cmw.datax.EntryType.STRING;
import static cern.cmw.datax.EntryType.STRING_ARRAY;
import static cern.nxcals.common.Schemas.ENTITY_ID;
import static cern.nxcals.common.Schemas.PARTITION_ID;
import static cern.nxcals.common.Schemas.SCHEMA_ID;
import static cern.nxcals.common.Schemas.SYSTEM_ID;
import static cern.nxcals.common.Schemas.TIMESTAMP;
import static cern.nxcals.common.avro.SchemaConstants.ARRAY_DIMENSIONS_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.ARRAY_ELEMENTS_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.BOOLEAN_MULTI_ARRAY_SCHEMA_NAME;
import static cern.nxcals.common.avro.SchemaConstants.DDF_X_ARRAY_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.DDF_Y_ARRAY_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.DF_MULTI_ARRAY_SCHEMA_NAME;
import static cern.nxcals.common.avro.SchemaConstants.DOUBLE_MULTI_ARRAY_SCHEMA_NAME;
import static cern.nxcals.common.avro.SchemaConstants.FLOAT_MULTI_ARRAY_SCHEMA_NAME;
import static cern.nxcals.common.avro.SchemaConstants.INT_MULTI_ARRAY_SCHEMA_NAME;
import static cern.nxcals.common.avro.SchemaConstants.LONG_MULTI_ARRAY_SCHEMA_NAME;
import static cern.nxcals.common.avro.SchemaConstants.RECORD_NAMESPACE;
import static cern.nxcals.common.avro.SchemaConstants.RECORD_NAME_PREFIX;
import static cern.nxcals.common.avro.SchemaConstants.STRING_MULTI_ARRAY_SCHEMA_NAME;
import static java.util.Arrays.asList;
import static java.util.Comparator.comparing;

/**
 * Understands the specific interpretation of the dynamic keys stored in the service and converts Avro records & schemas
 * to strings. It also converts from RecordData to avro bytes. It is crazily ugly code, left like that for the moment,
 * maybe somebody will be brave enough to make it a bit more clear... (jwozniak)
 */
public class DataServiceEncoderImpl
        implements DataServiceEncoder<KeyValues, KeyValues, String, Long>, Function<RecordData, byte[]> {
    /**
     * All Data related possible schema types definitions.
     */
    //@formatter:off
    private static final Schema booleanFieldSchemaNullable = SchemaBuilder.nullable().booleanType();
    private static final Schema intFieldSchemaNullable = SchemaBuilder.nullable().intType();
    private static final Schema longFieldSchemaNullable = SchemaBuilder.nullable().longType();
    private static final Schema floatFieldSchemaNullable = SchemaBuilder.nullable().floatType();
    private static final Schema doubleFieldSchemaNullable = SchemaBuilder.nullable().doubleType();
    private static final Schema stringFieldSchemaNullable = SchemaBuilder.nullable().stringType();
    private static final Schema booleanArrayFieldSchemaNullable = SchemaBuilder.nullable().array()
            .items(SchemaBuilder.builder().booleanType());
    private static final Schema intArrayFieldSchemaNullable = SchemaBuilder.nullable().array()
            .items(SchemaBuilder.builder().intType());
    private static final Schema longArrayFieldSchemaNullable = SchemaBuilder.nullable().array()
            .items(SchemaBuilder.builder().longType());
    private static final Schema floatArrayFieldSchemaNullable = SchemaBuilder.nullable().array()
            .items(SchemaBuilder.builder().floatType());
    private static final Schema doubleArrayFieldSchemaNullable = SchemaBuilder.nullable().array()
            .items(SchemaBuilder.builder().doubleType());
    private static final Schema stringArrayFieldSchemaNullable = SchemaBuilder.nullable().array()
            .items(stringFieldSchemaNullable);
    private static final Schema ddfFieldSchema = createDiscreteFunctionSchemaType();
    private static final Schema ddfFieldSchemaNullable = SchemaBuilder.nullable().type(ddfFieldSchema);
    private static final Schema ddfArrayFieldSchema = SchemaBuilder.array().items(ddfFieldSchema);
    private static final Schema ddfArrayFieldSchemaNullable = SchemaBuilder.nullable().type(ddfArrayFieldSchema);
    private static final Schema booleanMultiArrayFieldSchema = createMultiArraySchemaType(
            BOOLEAN_MULTI_ARRAY_SCHEMA_NAME,
            booleanArrayFieldSchemaNullable);
    private static final Schema intMultiArrayFieldSchema = createMultiArraySchemaType(INT_MULTI_ARRAY_SCHEMA_NAME,
            intArrayFieldSchemaNullable);
    private static final Schema longMultiArrayFieldSchema = createMultiArraySchemaType(LONG_MULTI_ARRAY_SCHEMA_NAME,
            longArrayFieldSchemaNullable);
    private static final Schema floatMultiArrayFieldSchema = createMultiArraySchemaType(FLOAT_MULTI_ARRAY_SCHEMA_NAME,
            floatArrayFieldSchemaNullable);
    private static final Schema doubleMultiArrayFieldSchema = createMultiArraySchemaType(DOUBLE_MULTI_ARRAY_SCHEMA_NAME,
            doubleArrayFieldSchemaNullable);
    private static final Schema stringMultiArrayFieldSchema = createMultiArraySchemaType(STRING_MULTI_ARRAY_SCHEMA_NAME,
            stringArrayFieldSchemaNullable);
    private static final Schema ddfMultiArrayFieldSchema = createMultiArraySchemaType(DF_MULTI_ARRAY_SCHEMA_NAME,
            ddfArrayFieldSchemaNullable);
    private static final Schema booleanMultiArrayFieldSchemaNullable = SchemaBuilder.nullable()
            .type(booleanMultiArrayFieldSchema);
    private static final Schema intMultiArrayFieldSchemaNullable = SchemaBuilder.nullable()
            .type(intMultiArrayFieldSchema);
    private static final Schema longMultiArrayFieldSchemaNullable = SchemaBuilder.nullable()
            .type(longMultiArrayFieldSchema);
    private static final Schema floatMultiArrayFieldSchemaNullable = SchemaBuilder.nullable()
            .type(floatMultiArrayFieldSchema);
    private static final Schema doubleMultiArrayFieldSchemaNullable = SchemaBuilder.nullable()
            .type(doubleMultiArrayFieldSchema);
    private static final Schema stringMultiArrayFieldSchemaNullable = SchemaBuilder.nullable()
            .type(stringMultiArrayFieldSchema);
    private static final Schema ddfMultiArrayFieldSchemaNullable = SchemaBuilder.nullable()
            .type(ddfMultiArrayFieldSchema);

    private static final Map<EntryType<?>, Schema> TYPE_TO_SCHEMA_MAP = new ImmutableMap.Builder<EntryType<?>, Schema>()
            .put(BOOL, booleanFieldSchemaNullable)
            .put(BOOL_ARRAY, booleanMultiArrayFieldSchemaNullable)
            .put(INT8, intFieldSchemaNullable)
            .put(INT16, intFieldSchemaNullable)
            .put(INT32, intFieldSchemaNullable)
            .put(INT8_ARRAY, intMultiArrayFieldSchemaNullable)
            .put(INT16_ARRAY, intMultiArrayFieldSchemaNullable)
            .put(INT32_ARRAY, intMultiArrayFieldSchemaNullable)
            .put(INT64, longFieldSchemaNullable)
            .put(INT64_ARRAY, longMultiArrayFieldSchemaNullable)
            .put(FLOAT, floatFieldSchemaNullable)
            .put(FLOAT_ARRAY, floatMultiArrayFieldSchemaNullable)
            .put(DOUBLE, doubleFieldSchemaNullable)
            .put(DOUBLE_ARRAY, doubleMultiArrayFieldSchemaNullable)
            .put(STRING, stringFieldSchemaNullable)
            .put(STRING_ARRAY, stringMultiArrayFieldSchemaNullable)
            .put(DISCRETE_FUNCTION, ddfFieldSchemaNullable)
            .put(DISCRETE_FUNCTION_ARRAY, ddfMultiArrayFieldSchemaNullable)
            .put(ENUM, stringFieldSchemaNullable)
            .put(ENUM_SET, stringMultiArrayFieldSchemaNullable)
            .put(ENUM_ARRAY, stringMultiArrayFieldSchemaNullable)
            .build();
    public static final String ILLEGAL_CHARACTER_ERROR_PREFIX = "Illegal character in:";

    private final ConcurrentHashMap<Long, Schema> schemaCache = new ConcurrentHashMap<>();

    //@formatter:on
    private final Schema entityKeySchema;
    private final Schema partitionKeySchema;
    private final Schema timeKeyRecordSchema;
    private final Map<String, Schema.Field> specialFieldsSchemaMap;
    private final int specialFieldsCount;
    private TimeConverter timeConverter;

    /**
     * Constructor that builds encoder based on supplied schemas.
     *
     * @param entityKeyDefs          Entity schema definition.
     * @param partitionKeyDefs       Partition schema definition.
     * @param timeKeyDefs            Time schema definition.
     * @param recordVersionKeySchema Record version schema definition.
     * @param timeConverter          Timestamp converter from generic record.
     */
    @SuppressWarnings("WeakerAccess")
    public DataServiceEncoderImpl(Schema entityKeyDefs, Schema partitionKeyDefs, Schema timeKeyDefs,
            Schema recordVersionKeySchema, TimeConverter timeConverter) {
        this.timeConverter = timeConverter;
        this.entityKeySchema = Objects.requireNonNull(entityKeyDefs);
        this.partitionKeySchema = Objects.requireNonNull(partitionKeyDefs);
        this.timeKeyRecordSchema = Objects.requireNonNull(timeKeyDefs);

        // this schema is a merge of all special fields and its fields override the field from the record (for
        // non-nullability)
        this.specialFieldsSchemaMap = Collections.unmodifiableMap(
                Stream.of(this.entityKeySchema, this.partitionKeySchema, recordVersionKeySchema,
                        this.timeKeyRecordSchema).reduce(AvroUtils::mergeSchemas)
                        .orElseThrow(() -> new IllegalStateException("Schema is empty?")).getFields().stream()
                        .collect(Collectors.toMap(Schema.Field::name, Function.identity())));
        this.specialFieldsCount = this.specialFieldsSchemaMap.size();

    }

    private static Schema getSchemaFromNullableUnion(Schema schema) {
        Type type = schema.getType();
        if (Type.UNION.equals(type)) {
            for (Schema sch : schema.getTypes()) {
                if (!Type.NULL.equals(sch.getType())) {
                    // return the first non NULL type from this UNION, expects to be a union of only 2 types, normal +
                    // NULL.
                    return sch;
                }
            }
            throw new IllegalArgumentException("There is no not NULL type in this schema " + schema.toString());
        } else {
            // not an union, just return it
            return schema;
        }
    }

    private static Schema getRecordSchemaFromNullableUnion(Schema schema) {
        Schema output = getSchemaFromNullableUnion(schema);
        if (Type.RECORD.equals(output.getType())) {
            return output;
        } else {
            throw new IllegalArgumentException(
                    "This schema is not an expected RECORD or UNION with RECORD type: " + schema.toString());
        }
    }

    @Override
    public Long encodeTimeKeyValues(ImmutableData record) {
        return this.timeConverter.convert(encodeKeyValuesOrThrow(this.timeKeyRecordSchema, record));
    }

    /*
     * It has to iterate over the fields from the Data record as the schema contains here more fields (our system
     * fields) that are not present in the record.
     */
    private static GenericRecord createDataGenericRecord(Schema avroSchema, ImmutableData record) {
        GenericRecord genericRecord = new GenericData.Record(avroSchema);
        for (ImmutableEntry entry : record.getEntries()) {
            String fieldName = IllegalCharacterConverter.get().getLegalIfCached(entry.getName());
            try {
                genericRecord.put(fieldName, createGenericRecordFieldValue(entry, fieldName, avroSchema));
            } catch (SchemaParseException exception) {
                if (exception.getMessage().startsWith(ILLEGAL_CHARACTER_ERROR_PREFIX)) {
                    fieldName = IllegalCharacterConverter.get().convertToLegal(fieldName);
                    genericRecord.put(fieldName, createGenericRecordFieldValue(entry, fieldName, avroSchema));
                } else {
                    throw exception;
                }
            }
        }
        return genericRecord;
    }

    /*
     * Used to create a value for the GenericRecord field from the cmw Data Entry.
     */
    private static Object createGenericRecordFieldValue(ImmutableEntry cmwDataEntry, String fieldName,
            Schema avroSchema) {
        Object value = cmwDataEntry.get();
        if (value == null) {
            return null;
        }
        EntryType<?> type = cmwDataEntry.getType();
        if (isScalar(cmwDataEntry)) {
            return handleScalars(cmwDataEntry, fieldName, avroSchema, value, type);
        }
        return handleArrays(cmwDataEntry, value, type);

    }

    @SuppressWarnings({ "squid:S1698" }) //== instead of equals
    private static Object handleScalars(ImmutableEntry cmwDataEntry, String fieldName, Schema avroSchema, Object value,
            EntryType<?> type) {
        if (type == INT8) {
            return Byte.toUnsignedInt(cmwDataEntry.getAs(INT8));
        } else if (type == INT16) {
            return Short.toUnsignedInt(cmwDataEntry.getAs(INT16));
        } else if (type == DATA) {
            return createDataGenericRecord(getRecordSchemaFromNullableUnion(avroSchema.getField(fieldName).schema()),
                    cmwDataEntry.getAs(DATA));
        } else if (type == DISCRETE_FUNCTION) {
            return createDiscreteFunctionGenericRecord(ddfFieldSchema, cmwDataEntry.getAs(DISCRETE_FUNCTION));
        } else if (type == ENUM) {
            return cmwDataEntry.getAs(ENUM).getName();
        } else if (type == ENUM_SET) {
            EnumValueSet set = cmwDataEntry.getAs(ENUM_SET);
            return handleEnumCollection(set.toArray());
        }
        return value;
    }

    private static Object handleEnumCollection(EnumValue[] array) {
        List<String> values = new LinkedList<>();
        for (EnumValue val : array) {
            values.add(val.getName());
        }
        return createMultiArrayGenericRecord(stringMultiArrayFieldSchema, values, new int[] { values.size() });
    }

    @SuppressWarnings({ "squid:MethodCyclomaticComplexity", "squid:S1698" })//== instead of equals, method complexity
    private static Object handleArrays(ImmutableEntry cmwDataEntry, Object value, EntryType<?> type) {
        if (type == DATA_ARRAY) {
            // this must be built dynamically - should we have array of the same types or different???
            throw new IllegalRecordRuntimeException("DataType not supported: DATA_ARRAY");
        }
        Collection<?> elements;
        Schema multiArraySchema;
        if (type == BOOL_ARRAY) {
            elements = Booleans.asList((boolean[]) value);
            multiArraySchema = booleanMultiArrayFieldSchema;
        } else if (type == INT8_ARRAY) {
            elements = convertToInt((byte[]) value);
            multiArraySchema = intMultiArrayFieldSchema;
        } else if (type == INT16_ARRAY) {
            elements = convertToInt((short[]) value);
            multiArraySchema = intMultiArrayFieldSchema;
        } else if (type == INT32_ARRAY) {
            elements = Ints.asList((int[]) value);
            multiArraySchema = intMultiArrayFieldSchema;
        } else if (type == INT64_ARRAY) {
            elements = Longs.asList((long[]) value);
            multiArraySchema = longMultiArrayFieldSchema;
        } else if (type == FLOAT_ARRAY) {
            elements = Floats.asList((float[]) value);
            multiArraySchema = floatMultiArrayFieldSchema;
        } else if (type == DOUBLE_ARRAY) {
            elements = Doubles.asList((double[]) value);
            multiArraySchema = doubleMultiArrayFieldSchema;
        } else if (type == DISCRETE_FUNCTION_ARRAY) {
            elements = createArrayOfDiscreteFunctionGenericRecord(ddfFieldSchema, asList((DiscreteFunction[]) value));
            multiArraySchema = ddfMultiArrayFieldSchema;
        } else if (type == STRING_ARRAY) {
            elements = asList((String[]) value);
            multiArraySchema = stringMultiArrayFieldSchema;
        } else if (type == ENUM_ARRAY) {
            return handleEnumCollection(cmwDataEntry.getAs(ENUM_ARRAY));
        } else {
            throw new IllegalRecordRuntimeException(
                    MessageFormat.format("DataType {0} not supported for arrays ", type));
        }
        return createMultiArrayGenericRecord(multiArraySchema, elements, cmwDataEntry.getDims());
    }

    private static Collection<Integer> convertToInt(byte[] bytes) {
        int[] ret = new int[bytes.length];
        for (int i = 0; i < bytes.length; i++) {
            ret[i] = bytes[i];
        }
        return Ints.asList(ret);
    }

    private static Collection<Integer> convertToInt(short[] shorts) {
        int[] ret = new int[shorts.length];
        for (int i = 0; i < shorts.length; i++) {
            ret[i] = shorts[i];
        }
        return Ints.asList(ret);
    }

    private static boolean isScalar(ImmutableEntry entry) {
        int[] dims = entry.getDims();
        return dims == null || dims.length == 0;
    }

    private static List<Object> createArrayOfDiscreteFunctionGenericRecord(Schema internalSchema,
            Collection<DiscreteFunction> entries) {
        List<Object> ret = new ArrayList<>(entries.size());
        for (DiscreteFunction df : entries) {
            ret.add(createDiscreteFunctionGenericRecord(internalSchema, df));
        }
        return ret;
    }

    private static GenericRecord createMultiArrayGenericRecord(Schema internalSchema, Collection<?> records,
            int[] dims) {
        GenericRecord record = new GenericData.Record(internalSchema);
        record.put(ARRAY_ELEMENTS_FIELD_NAME, records);
        record.put(ARRAY_DIMENSIONS_FIELD_NAME, Ints.asList(dims));
        return record;
    }

    private static Object createDiscreteFunctionGenericRecord(Schema internalSchema,
            DiscreteFunction discreteFunction) {
        GenericRecord record = new GenericData.Record(internalSchema);
        record.put(DDF_X_ARRAY_FIELD_NAME, Doubles.asList(discreteFunction.getXArray()));
        record.put(DDF_Y_ARRAY_FIELD_NAME, Doubles.asList(discreteFunction.getYArray()));
        return record;
    }

    private static GenericRecord encodeKeyValuesOrThrow(Schema schema, ImmutableData data) {
        GenericRecord genericRecord = new GenericData.Record(schema);
        for (Schema.Field field : schema.getFields()) {
            String cmwDataFieldName = field.name(); // I assume that we won't allow illegal characters in the names of
            // the system def keys.
            ImmutableEntry entry = data.getEntry(cmwDataFieldName);
            if (entry != null) {
                genericRecord.put(cmwDataFieldName, createGenericRecordFieldValue(entry, cmwDataFieldName, schema));
            } else {
                throw new IllegalRecordRuntimeException(
                        "Record [" + data + "] does not contain expected field [" + cmwDataFieldName + "]");
            }
        }

        return genericRecord;
    }

    private static Schema createMultiArraySchemaType(String typeName, Schema elementType) {
        return SchemaBuilder.record(typeName).namespace(RECORD_NAMESPACE).fields().name(ARRAY_ELEMENTS_FIELD_NAME)
                .type(elementType).noDefault().name(ARRAY_DIMENSIONS_FIELD_NAME).type(intArrayFieldSchemaNullable)
                .noDefault().endRecord();
    }

    private static Schema createDiscreteFunctionSchemaType() {
        return SchemaBuilder.record("discrete_function").namespace(RECORD_NAMESPACE).fields()
                .name(DDF_X_ARRAY_FIELD_NAME).type(doubleArrayFieldSchemaNullable).noDefault()
                .name(DDF_Y_ARRAY_FIELD_NAME).type(doubleArrayFieldSchemaNullable).noDefault().endRecord();
    }

    @Override
    public byte[] apply(RecordData record) {
        GenericRecord genericRecord = this.convert(record);
        return GenericRecordToBytesEncoder.convertToBytes(genericRecord);
    }

    @Override
    public KeyValues encodeEntityKeyValues(ImmutableData data) {
        return new GenericRecordKeyValues(encodeKeyValuesOrThrow(entityKeySchema, data));
    }

    @Override
    public KeyValues encodePartitionKeyValues(ImmutableData data) {
        GenericRecord genericRecord = encodeKeyValuesOrThrow(partitionKeySchema, data);
        Map<String, Object> keyValuesBasedOnGenericRecord = getKeyValuesBasedOnGenericRecord(genericRecord);
        return new SimpleKeyValues(null, keyValuesBasedOnGenericRecord);
    }

    private static Map<String, Object> getKeyValuesBasedOnGenericRecord(GenericRecord record) {
        Map<String, Object> keyValues = Maps.newHashMap();
        Schema schema = record.getSchema();

        for (Schema.Field field : schema.getFields()) {
            String fieldName = field.name();
            keyValues.put(fieldName, record.get(fieldName));
        }

        return keyValues;
    }

    /**
     * Creates a Schema from a given CMW Data record.
     */
    @Override
    public String encodeRecordFieldDefinitions(ImmutableData record) {
        FieldAssembler<Schema> fieldAssembler = SchemaBuilder.builder().record(RECORD_NAME_PREFIX + 0)
                .namespace(RECORD_NAMESPACE).fields();
        // add system fields
        fieldAssembler.name(SYSTEM_ID.getFieldName()).type(SYSTEM_ID.getSchema()).noDefault();
        fieldAssembler.name(ENTITY_ID.getFieldName()).type(ENTITY_ID.getSchema()).noDefault();
        fieldAssembler.name(PARTITION_ID.getFieldName()).type(PARTITION_ID.getSchema()).noDefault();
        fieldAssembler.name(SCHEMA_ID.getFieldName()).type(SCHEMA_ID.getSchema()).noDefault();
        fieldAssembler.name(TIMESTAMP.getFieldName()).type(TIMESTAMP.getSchema()).noDefault();
        // add fields from Data
        this.addFieldsFromData(fieldAssembler, record, 0);

        return fieldAssembler.endRecord().toString();
    }

    private GenericRecord convert(RecordData record) {
        SchemaData schemaData = record.getEntityData().getEntityHistoryData().first().getSchemaData();
        Schema schema = this.createOrFindSchema(schemaData);
        GenericRecord genericRecord = createDataGenericRecord(schema, record.getData());
        genericRecord.put(SYSTEM_ID.getFieldName(), record.getEntityData().getSystemData().getId());
        genericRecord.put(ENTITY_ID.getFieldName(), record.getEntityData().getId());
        genericRecord.put(PARTITION_ID.getFieldName(), record.getEntityData().getPartitionData().getId());
        genericRecord.put(SCHEMA_ID.getFieldName(), schemaData.getId());
        genericRecord.put(TIMESTAMP.getFieldName(), System.currentTimeMillis() * 1_000_000); // we store in nanosecond
        // resolution to be
        // compatible with the data
        // timestamp
        return genericRecord;
    }

    /**
     * Creates the schema for the record based on the metadata. It assumes that the schema is correctly returned by the
     * schema provider.
     */
    private Schema createOrFindSchema(SchemaData schemaData) {
        return this.schemaCache.computeIfAbsent(schemaData.getId(),
                schemaKey -> new Schema.Parser().parse(schemaData.getSchemaJson()));

    }

    // Adds fields from Data to a FieldAssembler
    // Checks if all the special fields on level 0 are found. The check is very simple and based on the number of fields found.
    // It is based on the assumption that we cannot have twice the same field name in CMW Data.
    private void addFieldsFromData(FieldAssembler<Schema> fieldAssembler, ImmutableData record, int recordLevel) {
        if (recordLevel > 0 && record.getEntryCount() == 0) {
            throw new IllegalRecordRuntimeException("There are no fields in nested record");
        }
        //This is a simple check if all the special fields are found.
        int fieldsCount = 0;
        // sort needed to create the same schema every time for the same record field definitions.
        List<ImmutableEntry> entries = new ArrayList<>(record.getEntries());
        entries.sort(comparing(ImmutableEntry::getName));

        for (ImmutableEntry entry : entries) {
            String fieldName = IllegalCharacterConverter.get().getLegalIfCached(entry.getName());
            if (recordLevel == 0 && (fieldName.equals(ENTITY_ID.getFieldName()) || fieldName
                    .equals(PARTITION_ID.getFieldName()))) {
                //We don't process further ENTITY_ID and PARTITION_ID fields at 0 level if passed from the client in the record.
                //They are already added to the field assembler in the calling method.
                continue;
            }
            Schema fieldSchema;
            Schema.Field field;
            if (recordLevel == 0 && (field = this.specialFieldsSchemaMap.get(fieldName)) != null) {
                // Do not generate anything for special fields at 0 level (main record level).
                // The schema for this special field should be the one defined in the system.
                fieldSchema = field.schema();
                fieldsCount++;
            } else {
                //Generate the field schema based on the Entry definition.
                fieldSchema = this.getSchemaForEntry(entry, recordLevel);
            }

            try {
                fieldAssembler.name(fieldName).type(fieldSchema).noDefault();
            } catch (SchemaParseException exception) {
                if (exception.getMessage().startsWith(ILLEGAL_CHARACTER_ERROR_PREFIX)) {
                    fieldName = IllegalCharacterConverter.get().convertToLegal(fieldName);
                    fieldAssembler.name(fieldName).type(fieldSchema).noDefault();
                } else {
                    throw exception;
                }
            }
        }

        throwIfDataDoesNotContainAllRequiredFields(recordLevel, fieldsCount, record);
    }

    private void throwIfDataDoesNotContainAllRequiredFields(int recordLevel, int fieldsCount, ImmutableData record) {
        if (recordLevel == 0 && fieldsCount != this.specialFieldsCount) {
            throw new IllegalRecordRuntimeException(
                    "Data record " + record + " does not contain all required (system defined) fields "
                            + this.specialFieldsSchemaMap.keySet());
        }
    }

    /**
     * Creates the schema for a given @see {@link ImmutableEntry} object with all the fields nullable by default.
     */
    @SuppressWarnings("squid:S1698") //== instead of equals
    private Schema getSchemaForEntry(ImmutableEntry cmwDataEntry, int recordLevel) {
        EntryType<?> type = cmwDataEntry.getType();
        Schema schema;
        if (type == DATA) {
            int nestedRecordLevel = recordLevel + 1;
            FieldAssembler<Schema> fieldAssembler = SchemaBuilder.nullable()
                    .record(RECORD_NAME_PREFIX + nestedRecordLevel).namespace(RECORD_NAMESPACE).fields();
            // this is built dynamically
            this.addFieldsFromData(fieldAssembler, cmwDataEntry.getAs(DATA), nestedRecordLevel);
            schema = fieldAssembler.endRecord();
        } else {
            schema = TYPE_TO_SCHEMA_MAP.get(type);
        }

        if (schema == null) {
            throw new UnsupportedOperationException("Unknown DataType: " + type);
        }

        return schema;
    }
}
