nxcals.monitoring.grok:
  metrics:
    domain: "nxcals.monitoring.grok"
  basePath: "{{install_dir}}"
  fileGlob: "**/log*/{application,monit,grafana,server,zookeeper,gobblin-current,filebeat,logstash*}.log"
  depth: 5
  searchEvery: 120
  searchEveryTimeUnit: "SECONDS"
  rules:

    - name: errorsGeneric
      filePatternsInclude: [".*"]
      filePatternsExclude: [".*/monit.log", ".*filebeat.*", ".*datasource.*", ".*reader.*", ".*etl.*", ".*logstash-metrics.*"]
      linePatterns: [".*ERROR.*"]
      initScript: '
          var metricName = metricNamePrefix + "___" + ruleConfig.getName();
          counterService.decrement(metricName);
          counterService.increment(metricName);
      '
#      Patterns can be used from array in an arbitrary expression
      conditionScript: '
        if(patterns[0].matcher(line).matches()) {
          var metricName = metricNamePrefix + "___" + ruleConfig.getName();
          log.debug("Incrementing the counter for {}", metricName);
          counterService.increment(metricName);
        }
      '
    - name: errorsMonitoringReader
      filePatternsInclude: [".*reader.*"]
      filePatternsExclude:
      linePatterns: [".*ERROR.*", ".*ProcessorImpl - Errors found.*", ".*ProcessorImpl - ERROR: Entity.*"]
      initScript: '
        var metricName = metricNamePrefix + "___" + ruleConfig.getName();
        counterService.decrement(metricName);
        counterService.increment(metricName);
      '
#      Or we can use included "firstMatcher" predicate that will match the line if the first pattern matches and all others do not match
      conditionScript: '
        if(firstMatcher.test(line)) {
          var metricName = metricNamePrefix + "___" + ruleConfig.getName();
          log.debug("Incrementing the counter for {}", metricName);
          counterService.increment(metricName);
        }
      '

#   Isolate 'monit' logfile path in order the catch only monit specific loggers and
#   and avoid accidental pick-up from other modules that contain the 'monit' literal (like monitoring-x modules)
    - name: errorsMonit
      filePatternsInclude: [".*/monit.log"]
      filePatternsExclude:
      linePatterns: [".*error.*", ".*ssl.*",".*SSL.*", ".*uptime test failed.*"]
      initScript: '
        var metricName = metricNamePrefix + "___" + ruleConfig.getName();
        counterService.decrement(metricName);
        counterService.increment(metricName);
      '
      conditionScript: '
        if(firstMatcher.test(line)) {
          var metricName = metricNamePrefix + "___" + ruleConfig.getName();
          log.debug("Incrementing the counter for {}", metricName);
          counterService.increment(metricName);
        }
      '
    - name: errorsFilebeat
      filePatternsInclude: [".*filebeat.*"]
      filePatternsExclude:
      linePatterns: [".* ERR .*", ".* ERR .*data/registry.*"]
      initScript: '
        var metricName = metricNamePrefix + "___" + ruleConfig.getName();
        counterService.decrement(metricName);
        counterService.increment(metricName);
      '
      conditionScript: '
        if(firstMatcher.test(line)) {
          var metricName = metricNamePrefix + "___" + ruleConfig.getName();
          log.debug("Incrementing the counter for {}", metricName);
          counterService.increment(metricName);
        }
      '


    - name: errorsETL
      filePatternsInclude: [".*etl.*"]
      filePatternsExclude:
      linePatterns: [".* ERROR .*", ".*Pulling partition ([a-zA-Z-_]+):([0-9]+) from .* range=([0-9]+).*"]
      initScript: '
        var metricName = metricNamePrefix + "___" + ruleConfig.getName();
        counterService.decrement(metricName);
        counterService.increment(metricName);
      '
      conditionScript: '
        if(patterns[0].matcher(line).matches()) {
             var metricName = metricNamePrefix + "___" + ruleConfig.getName();
             log.debug("Incrementing the counter for {}", metricName);
             counterService.increment(metricName);
        }

        var matcher = patterns[1].matcher(line);
        if(matcher.matches()) {
             var gname = metricName+ "_gauge_" + matcher.group(1) + "_" + matcher.group(2);
             var value = java.lang.Double.parseDouble(matcher.group(3));
             log.debug("Setting the gauge for {} to {}", gname, value );
             gaugeService.submit(gname,value);
        }
      '

    - name: logstashNode
      filePatternsInclude: [".*logstash-metrics.*"]
      filePatternsExclude:
      linePatterns: [".*\"duration_in_millis\":([0-9]+).*", ".*\"in\":([0-9]+).*", ".*\"filtered\":([0-9]+).*", ".*\"out\":([0-9]+).*",
      ".*\"heap_used_in_bytes\":([0-9]+).*", ".*\"open_file_descriptors\":([0-9]+).*",
      ".*\"gc\":.*\"young\":.*collection_time_in_millis\":([0-9]+).*", ".*\"gc\":.*\"young\":.*collection_count\":([0-9]+).*"]
      initScript: '
        var metricNameDurationIn = ruleConfig.getName() + "PipelineDurationInMillis_gauge";
        gaugeService.submit(metricNameDurationIn, 0);

        var metricNameIn = ruleConfig.getName() + "PipelineIn_gauge";
        gaugeService.submit(metricNameIn, 0);

        var metricNameFiltered = ruleConfig.getName() + "PipelineFiltered_gauge";
        gaugeService.submit(metricNameFiltered, 0);

        var metricNameOut = ruleConfig.getName() + "PipelineOut_gauge";
        gaugeService.submit(metricNameOut, 0);

        var metricNameHeapUsedBytes = ruleConfig.getName() + "JvmHeapUsedBytes_gauge";
        gaugeService.submit(metricNameHeapUsedBytes, 0);

        var metricNameOpenFileDescriptors = ruleConfig.getName() + "ProcessOpenFileDescriptors_gauge";
        gaugeService.submit(metricNameOpenFileDescriptors, 0);

        var metricNameGcYoungTime = ruleConfig.getName() + "JvmGcYoungTime_gauge";
        gaugeService.submit(metricNameGcYoungTime, 0);

        var metricNameGcYoungCount = ruleConfig.getName() + "JvmGcYoungCount_gauge";
        gaugeService.submit(metricNameGcYoungCount, 0);

      '
      conditionScript: '
        var matcherDurationIn = patterns[0].matcher(line);
        if(matcherDurationIn.matches()) {
          log.debug("Setting the gauge for {}", metricNameDurationIn);
          gaugeService.submit(metricNameDurationIn, java.lang.Double.parseDouble(matcherDurationIn.group(1)));
        }

        var matcherIn = patterns[1].matcher(line);
        if(matcherIn.matches()) {
          log.debug("Setting the gauge for {}", metricNameIn);
          gaugeService.submit(metricNameIn, java.lang.Double.parseDouble(matcherIn.group(1)));
        }

        var matcherFiltered = patterns[2].matcher(line);
        if(matcherFiltered.matches()) {
          log.debug("Setting the gauge for {}", metricNameFiltered);
          gaugeService.submit(metricNameFiltered, java.lang.Double.parseDouble(matcherFiltered.group(1)));
        }

        var matcherOut = patterns[3].matcher(line);
        if(matcherOut.matches()) {
          log.debug("Setting the gauge for {}", metricNameOut);
          gaugeService.submit(metricNameOut, java.lang.Double.parseDouble(matcherOut.group(1)));
        }

        var matcherHeapUsedBytes = patterns[4].matcher(line);
        if(matcherHeapUsedBytes.matches()) {
          log.debug("Setting the gauge for {}", metricNameHeapUsedBytes);
          gaugeService.submit(metricNameHeapUsedBytes, java.lang.Long.parseLong(matcherHeapUsedBytes.group(1)));
        }

        var matcherOpenFileDescriptors = patterns[5].matcher(line);
        if(matcherOpenFileDescriptors.matches()) {
          log.debug("Setting the gauge for {}", metricNameOpenFileDescriptors);
          gaugeService.submit(metricNameOpenFileDescriptors, java.lang.Double.parseDouble(matcherOpenFileDescriptors.group(1)));
        }

        var matcherGcYoungTime = patterns[6].matcher(line);
        if(matcherGcYoungTime.matches()) {
          log.debug("Setting the gauge for {}", metricNameGcYoungTime);
          gaugeService.submit(metricNameGcYoungTime, java.lang.Double.parseDouble(matcherGcYoungTime.group(1)));
        }

        var matcherGcYoungCount = patterns[7].matcher(line);
        if(matcherGcYoungCount.matches()) {
          log.debug("Setting the gauge for {}", metricNameGcYoungCount);
          gaugeService.submit(metricNameGcYoungCount, java.lang.Double.parseDouble(matcherGcYoungCount.group(1)));
        }
      '

spring.profiles.active: prod

---
spring:
  profiles: prod
---
spring:
  profiles: dev
