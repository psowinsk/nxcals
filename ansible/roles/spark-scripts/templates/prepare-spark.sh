#!/usr/bin/env bash

#This has to be empty, we use jar with hadoop config
export HADOOP_CONF_DIR=

echo "Using NXCALS version={{version}} repo={{repository}} spark={{spark_module_name}}"

hash spark-shell --version 2>/dev/null || {
    echo "spark-shell command not available in PATH=$PATH";
    SPARK_DIR=$MYDIR/{{spark_module_name}}
    if [ ! -d "$SPARK_DIR" ]; then
        echo "Downloading {{spark_module_name}}"
        curl -s -o $MYDIR/spark.tgz "{{spark_download_url}}"
        tar -xf $MYDIR/spark.tgz -C $MYDIR
        rm -rf $MYDIR/spark.tgz
    fi
    export SPARK_SHELL=$SPARK_DIR/bin/spark-shell
    export PYSPARK_SHELL=$SPARK_DIR/bin/pyspark
    echo "Using spark-shell from $SPARK_SHELL"
}

if [ -z "$SPARK_SHELL" ]; then
    export SPARK_SHELL=spark-shell
fi

if [ -z "$PYSPARK_SHELL" ]; then
    export PYSPARK_SHELL=pyspark
fi

if [ ! -f "{{hadoop_config_jar}}" ]; then
    echo "Getting hadoop config {{hadoop_config_artefact}}/{{version}}/{{hadoop_config_jar}}"
    curl -s -o $MYDIR/{{hadoop_config_jar}} \
        "{{artifactory_address}}/{{repository}}/cern/nxcals/{{hadoop_config_artefact}}/{{version}}/{{hadoop_config_jar}}"
fi

if [ ! -f "nxcals-data-access-python-{{version}}.zip" ]; then
    echo "Getting nxcals-data-access-python-{{version}}.zip"
    curl -s -o $MYDIR/nxcals-data-access-python-{{version}}.zip \
        "{{artifactory_address}}/{{repository}}/cern/nxcals/nxcals-data-access-python/{{version}}/nxcals-data-access-python-{{version}}.zip"
fi

