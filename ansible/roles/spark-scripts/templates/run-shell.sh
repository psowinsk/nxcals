#!/usr/bin/env bash



if [[ "$SPARK_COMMAND" == *pyspark ]]; then
    PYFILES="--py-files $MYDIR/nxcals-data-access-python-{{version}}.zip"
fi

${SPARK_COMMAND} \
--packages "cern.nxcals:nxcals-data-access:{{version}}" \
--repositories "{{spark_script_repositories}}"  \
--driver-java-options '{{service_url_java_opt}} {{cert_java_opt}}' \
--driver-class-path $MYDIR/{{hadoop_config_jar}} \
--conf spark.yarn.appMasterEnv.JAVA_HOME={{hadoop_java_home}} \
--conf spark.executorEnv.JAVA_HOME={{hadoop_java_home}} \
--conf spark.yarn.jars='hdfs://{{hadoop_hdfs_lib_dir}}/spark-{{spark_version}}/*.jar' \
--conf spark.executor.extraClassPath=/usr/lib/hadoop/lib/native \
--conf spark.yarn.am.extraClassPath=/usr/lib/hadoop/lib/native \
--conf spark.sql.caseSensitive=true $PYFILES "${@:1}"