#!/bin/bash

ID={% for server in zookeeper_hosts %}
{% if server.host is defined %}
{% if server.host == inventory_hostname %}
{{ server.id }}
{% endif %}
{% else %}
{% if server == inventory_hostname %}
{{ loop.index }}
{% endif %}
{% endif %}
{% endfor %}

export KAFKA_HEAP_OPTS="{{ kafka_memory }}"
export KAFKA_OPTS="$KAFKA_OPTS {{ java_opts }}"

bin/kafka-server-start.sh -daemon config/server.properties --override broker.id=$ID

sleep 5
PIDS=$(ps ax | grep -i 'kafka\.Kafka' | grep java | grep -v grep | awk '{print $1}')
echo $PIDS > application.pid

