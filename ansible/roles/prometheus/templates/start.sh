#!/bin/bash

nohup ./prometheus \
 -storage.local.path={{install_dir}}/data-prometheus \
 -config.file=config/prometheus.yml -web.listen-address :{{prometheus_port}} \
 -alertmanager.url=http://localhost:{{alertmanager_port}} \
 -storage.local.retention {{prometheus_local_retention_time}} \
 -log.format "logger:stdout" 2>&1 > log/application.log &

echo $! > application.pid
 
