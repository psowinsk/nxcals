#!/usr/bin/env bash

VERSION=$(cat gradle.properties | grep currentVersion | awk 'BEGIN { FS="="} { print $2;}')

if [ $# -eq 1 ] ; then
    FILE=$(basename $1)
else
    FILE=nxcals-full-install-with-sudo.yml
fi

INVENTORY=inventory/dev-$USER
echo "**** Using inventory $INVENTORY with $FILE for install version $VERSION with additional arguments: ${@:2} ****"
echo "If you want to cancel press CTRL+C now..."
sleep 3

./gradlew clean test install

if [ ! $? -eq 0 ]; then
 exit $?
fi

cd ansible;
ansible-playbook -i $INVENTORY $FILE --vault-pass .pass -e nxcals_version=$VERSION "${@:2}";
cd ..
