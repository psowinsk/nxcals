package cern.nxcals.monitoring.reader.domain;

import lombok.Data;
import lombok.NonNull;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.time.Instant;

/**
 * Created by jwozniak on 12/01/17.
 */
@Data
public class ExtractionResultImpl implements ExtractionResult {
    @NonNull
    private final Instant startTime;
    @NonNull
    private final Instant endTime;
    @NonNull
    private final Dataset<Row> dataset;
}
