package cern.nxcals.monitoring.reader.service;

/**
 * Created by jwozniak on 12/01/17.
 */
public interface Processor {
    public void process();
}
