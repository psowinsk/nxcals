package cern.nxcals.monitoring.reader;

import cern.nxcals.monitoring.reader.domain.CheckConfig;
import cern.nxcals.monitoring.reader.domain.EntityConfig;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * Created by jwozniak on 14/01/17.
 */
public interface ConfigurationProvider {
    List<CheckConfig> getChecks();

    List<EntityConfig> getEntities();

    Optional<CheckConfig> getByName(String name);

    TimeUnit getPeriodTimeUnit();

    long getPeriod();
}
