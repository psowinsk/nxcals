/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.monitoring.reader.config;

import cern.nxcals.common.security.KerberosRelogin;
import cern.nxcals.data.access.api.DataAccessService;
import cern.nxcals.data.access.api.DataAccessServiceFactory;
import cern.nxcals.monitoring.reader.domain.ReaderConfiguration;
import org.apache.spark.sql.SparkSession;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@EnableConfigurationProperties(ReaderConfiguration.class)
@Configuration
public class SpringConfig {


    @Bean(name = "kerberos", initMethod = "start")
    public KerberosRelogin kerberosRelogin(@Value("${kerberos.principal}") String principal,
                                           @Value("${kerberos.keytab}") String keytab,
                                           @Value("${kerberos.relogin}") boolean enableRelogin) {
        return new KerberosRelogin(principal, keytab, enableRelogin);
    }

    @Bean
    public static ConversionService conversionService() {
        return new DefaultConversionService();
    }

    @Bean
    @DependsOn("kerberos")
    public DataAccessService dataAccessService(SparkSession session) {
        return DataAccessServiceFactory.createDataAccessService(session);

        // Create DevicepropertyBuilder object that will build the object containing the information for device/property
        // you want to getQuery
    }

    @Bean
    public ScriptEngine scriptEngine() {
        return new ScriptEngineManager().getEngineByName("nashorn");
    }

    @Bean
    public ScheduledExecutorService scheduledExecutorService() {
       return Executors.newScheduledThreadPool(1);
    }


}
