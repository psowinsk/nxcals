package cern.nxcals.monitoring.reader.domain;

import cern.nxcals.monitoring.reader.ConfigurationProvider;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * Created by jwozniak on 14/01/17.
 */
@ConfigurationProperties("nxcals.reader")
@Data
@NoArgsConstructor
public class ReaderConfiguration implements ConfigurationProvider {

    private List<EntityConfig> entities;
    private List<CheckConfig> checks;
    private TimeUnit periodTimeUnit;
    private long period;


    @Override
    public synchronized Optional<CheckConfig> getByName(String name) {
        return checks.stream().filter(ch -> ch.getName().equals(name)).findFirst();
    }



}
