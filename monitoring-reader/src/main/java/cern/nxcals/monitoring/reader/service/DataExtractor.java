package cern.nxcals.monitoring.reader.service;

import cern.nxcals.monitoring.reader.domain.CheckConfig;
import cern.nxcals.monitoring.reader.domain.EntityConfig;
import cern.nxcals.monitoring.reader.domain.ExtractionResult;

import java.util.List;

/**
 * Created by jwozniak on 12/01/17.
 */
public interface DataExtractor {
    List<ExtractionResult> extract(EntityConfig entityConfig, CheckConfig checkConfig);
}
