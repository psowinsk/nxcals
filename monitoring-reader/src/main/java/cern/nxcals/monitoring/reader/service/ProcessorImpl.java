package cern.nxcals.monitoring.reader.service;

import cern.nxcals.monitoring.reader.ConfigurationProvider;
import cern.nxcals.monitoring.reader.domain.CheckConfig;
import cern.nxcals.monitoring.reader.domain.EntityConfig;
import cern.nxcals.monitoring.reader.domain.ExtractionResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.actuate.metrics.GaugeService;
import org.springframework.stereotype.Service;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Created by jwozniak on 12/01/17.
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class ProcessorImpl implements Processor {

    private final ConfigurationProvider configProvider;
    private final DataExtractor extractor;
    private final GaugeService gaugeService;
    private final ScriptEngine engine;
    private final ScheduledExecutorService scheduledExecutorService;


    @Override

    public void process() {
        scheduledExecutorService.scheduleAtFixedRate(this::runInThread, 0, configProvider.getPeriod(), configProvider.getPeriodTimeUnit());
    }

    @SuppressWarnings("squid:S1181")// catching Throwable is bad but we want this thread to never stop.
    private void runInThread() {
        try {
            long start = System.currentTimeMillis();
            if(log.isInfoEnabled()) {
                log.info("Start processing entities {} ", configProvider.getEntities());
            }
            for (EntityConfig entityConfig : configProvider.getEntities()) {
                processEntity(entityConfig);
            }
            log.info("Finished processing entities in {} seconds", String.format("%.2f", (System.currentTimeMillis()-start)/1000.0));
        } catch (Throwable ex) {
            log.error("Exception while processing entities", ex);
        }
    }

    private void processEntity(EntityConfig entityConfig) {
        for (String checkRef : entityConfig.getCheckRefs()) {
            Optional<CheckConfig> configOptional = this.configProvider.getByName(checkRef);
            if (configOptional.isPresent()) {
                processEntityWithCheck(entityConfig, configOptional.get());
            } else {
                if(log.isErrorEnabled()) {
                    log.error("Entity {} - no such check: {} ", entityConfig.getName(), checkRef);
                }
                throw new IllegalStateException("Entity " + entityConfig.getName() + " - no such check: " + checkRef);
            }
        }

    }

    private void processEntityWithCheck(EntityConfig entityConfig, CheckConfig checkConfig) {
        try {
            long errorCount = verifyExtractionResults(entityConfig, checkConfig,
                    extractor.extract(entityConfig, checkConfig));
            publishStatistics(entityConfig, checkConfig, errorCount);
        } catch (Exception ex) {
            if(log.isErrorEnabled()) {
                log.error("Exception while processing entity={} check={}", entityConfig.getEntityKeys(),
                        checkConfig.getName(), ex);
            }
            publishStatistics(entityConfig, checkConfig, 1);
        }
    }

    private void publishStatistics(EntityConfig entityConfig, CheckConfig checkConfig, long errorCount) {
        String name = (entityConfig.getName() + "_" + checkConfig.getName()).toLowerCase();
        if (errorCount > 0) {
            log.error("Errors found for {} errorsCount={}", name, errorCount);
        } else {
            log.info("All checks OK for {}", name);
        }
        this.gaugeService.submit(name, errorCount);
    }

    private boolean executeCondition(EntityConfig entityConfig, CheckConfig checkConfig, ExtractionResult result) {
        try {
            Bindings bindings = engine.createBindings();
            bindings.put("dataSet", result.getDataset());

            Boolean eval = (Boolean) engine.eval(checkConfig.getCondition(), bindings);

            if (eval) {
                if(log.isInfoEnabled()) {
                    log.info("OK: Entity={} check={} condition={} getStartTime={} getEndTime={} evaluation=true",
                            entityConfig.getName(), checkConfig.getName(), checkConfig.getCondition(), result.getStartTime(),
                            result.getEndTime());
                }
            } else {
                if(log.isErrorEnabled()) {
                    log.error("ERROR: Entity={} check={}  condition={} getStartTime={} getEndTime={} failed with message={}",
                            entityConfig.getName(), checkConfig.getName(), checkConfig.getCondition(), result.getStartTime(),
                            result.getEndTime(), engine.eval(checkConfig.getDebug(), bindings));
                }
            }
            return eval;
        } catch (Exception e) {
            if(log.isErrorEnabled()) {
                log.error("Failed to execute entity={} check={} condition={}", entityConfig.getName(),
                        checkConfig.getName(), checkConfig.getCondition(), e);
            }
            return false;
        }
    }

    private long verifyExtractionResults(EntityConfig entityConfig, CheckConfig checkConfig,
            List<ExtractionResult> results) {
        return results.parallelStream().map(result -> executeCondition(entityConfig, checkConfig, result)).filter(val -> !val)
                .count();

    }

}
