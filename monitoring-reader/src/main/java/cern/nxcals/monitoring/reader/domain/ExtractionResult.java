package cern.nxcals.monitoring.reader.domain;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.time.Instant;

/**
 * Created by jwozniak on 12/01/17.
 */
public interface ExtractionResult {
    Instant getStartTime();
    Instant getEndTime();
    Dataset<Row> getDataset();
}
