package cern.nxcals.monitoring.reader.service;

import cern.nxcals.data.access.builders.KeyValuesQuery;
import cern.nxcals.monitoring.reader.domain.CheckConfig;
import cern.nxcals.monitoring.reader.domain.EntityConfig;
import cern.nxcals.monitoring.reader.domain.ExtractionResult;
import cern.nxcals.monitoring.reader.domain.ExtractionResultImpl;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAmount;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.time.format.DateTimeFormatter.ofPattern;

/**
 * Created by jwozniak on 04/11/16.
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class DataExtractorImpl implements DataExtractor {
    static final DateTimeFormatter FORMATTER = ofPattern("yyyy-MM-dd HH:mm:ss");
    private Instant extractSince = Instant.EPOCH;

    @NonNull
    private final SparkSession session;

    @NonNull
    private final Clock clock;

    @NonNull
    private DatasetBuilder builder;

    @Autowired
    DataExtractorImpl(SparkSession sparkSession) {
        this(sparkSession, Clock.systemDefaultZone(), new SimpleDatasetBuilder());
    }

    @Override
    public List<ExtractionResult> extract(EntityConfig entityConfig, CheckConfig checkConfig) {
        Instant baseTime = this.clock.instant()
                .minus(checkConfig.getStartTime(), checkConfig.getTimeUnit())
                .truncatedTo(checkConfig.getTimeUnit());

        TemporalAmount step = Duration.of(1, checkConfig.getTimeUnit());

        Instant startTime = baseTime.minus(step);
        Instant endTime = baseTime.minus(1, ChronoUnit.NANOS);

        List<ExtractionResult> ret = new ArrayList<>((int) checkConfig.getTimeSlots());
        for (long i = 0; i < checkConfig.getTimeSlots(); i++) {
            startTime = startTime.plus(step);
            endTime = endTime.plus(step);

            if (startTime.isBefore(this.extractSince)) {
                if(log.isInfoEnabled()) {
                    String name = entityConfig.getName() + "_" + checkConfig.getName();
                    log.info("Data checking/extraction of [{}] skipped due to start time {} older than configuration "
                            + "extraction time: {}, ", name.toLowerCase(), startTime, this.extractSince);
                }
            } else {
                Dataset<Row> dataset = builder.buildDataset(entityConfig.getSystem(),
                        startTime,
                        endTime,
                        checkConfig.getFields(),
                        entityConfig.getEntityKeys(), session);

                ret.add(new ExtractionResultImpl(startTime, endTime, dataset));
            }
        }
        return ret;
    }

    /**
     * Sets the date to start the extraction getStartTime
     *
     * @param date - in format yyyy-mm-dd hh:mm:ss
     */
    @Value(value = "${nxcals.reader.extract.since:1970-01-01 00:00:00}")
    public void setExtractSince(String date) {
        this.extractSince = FORMATTER.withZone(this.clock.getZone()).parse(date, Instant::from);
        if(log.isInfoEnabled()) {
            log.info("Set extract since to {}", FORMATTER.withZone(this.clock.getZone()).format(this.extractSince));
        }
    }

    LocalDateTime getExtractSince() {
        return LocalDateTime.ofInstant(this.extractSince, this.clock.getZone());
    }

    private static class SimpleDatasetBuilder implements DatasetBuilder {
        @Override
        public Dataset<Row> buildDataset(String system, Instant start, Instant end, List<String> fields,
                Map<String, String> entityKeys, SparkSession session) {
            return KeyValuesQuery.builder(session)
                    .system(system)
                    .startTime(start)
                    .endTime(end)
                    .fields(fields)
                    .entity()
                    .keyValues(entityKeys)
                    .buildDataset();
        }
    }

    public interface DatasetBuilder {
        Dataset<Row> buildDataset(String system, Instant start, Instant end, List<String> fields, Map<String, String> entityKeys, SparkSession session);
    }
}
