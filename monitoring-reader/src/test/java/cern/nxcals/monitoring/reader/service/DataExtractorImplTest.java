package cern.nxcals.monitoring.reader.service;

import cern.nxcals.monitoring.reader.domain.CheckConfig;
import cern.nxcals.monitoring.reader.domain.EntityConfig;
import cern.nxcals.monitoring.reader.domain.ExtractionResult;
import org.apache.spark.sql.DataFrameReader;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static cern.nxcals.monitoring.reader.service.DataExtractorImpl.FORMATTER;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.time.temporal.ChronoUnit.HOURS;
import static java.time.temporal.ChronoUnit.MINUTES;
import static java.time.temporal.ChronoUnit.SECONDS;
import static java.util.Collections.singletonMap;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by msobiesz on 05/09/17.
 */
public class DataExtractorImplTest {
    private static final DateTimeFormatter FORMATTER_WITH_ZONE = FORMATTER.withZone(ZoneId.systemDefault());

    private DataExtractorImpl instance;
    private EntityConfig eConfig;

    @Mock
    private Clock clockMock;

    @Mock
    private Dataset<Row> dataset;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        DataExtractorImpl.DatasetBuilder builder = mock(DataExtractorImpl.DatasetBuilder.class);
        when(builder.buildDataset(any(String.class), any(Instant.class), any(Instant.class), anyList(), anyMap(), any(SparkSession.class)))
                .thenReturn(this.dataset);

        when(this.clockMock.getZone()).thenReturn(ZoneId.systemDefault());

        this.instance = new DataExtractorImpl(mock(SparkSession.class), this.clockMock, builder);
        this.eConfig = getEntityConfig();
    }

    @Test(expected = DateTimeParseException.class)
    public void shouldNotParseWrongExtractTime() {
        this.instance.setExtractSince("1970/11/11 12:00");
    }

    @Test
    public void shouldParseCorrectExtractTime() {
        LocalDateTime now = LocalDateTime.now().truncatedTo(SECONDS);
        this.instance.setExtractSince(now.format(FORMATTER));
        LocalDateTime extractSince = this.instance.getExtractSince();
        Assert.assertEquals(now, extractSince);
    }

    @Test
    public void shouldExtractDataForMinutesButNotHours() {
        Instant now = Instant.ofEpochSecond(2 * 60 * 60);
        when(this.clockMock.instant()).thenReturn(now);
        this.instance.setExtractSince(FORMATTER_WITH_ZONE.format(now.minus(30, MINUTES)));

        CheckConfig cConfig;
        List<ExtractionResult> data;

        cConfig = checkConfigMock(60, 60, MINUTES);
        data = extract(cConfig);
        assertSize(data, 30);

        cConfig = checkConfigMock(1, 2, HOURS);
        data = extract(cConfig);
        assertEmpty(data);

        cConfig = checkConfigMock(1, 2, DAYS);
        data = extract(cConfig);
        assertEmpty(data);
    }

    @Test
    public void shouldExtractDataForMinutesAndHoursButNotDays() {
        Instant now = Instant.ofEpochSecond(5 * 60 * 60);
        when(this.clockMock.instant()).thenReturn(now);
        this.instance.setExtractSince(FORMATTER_WITH_ZONE.format(now.minus(2, HOURS)));

        CheckConfig cConfig;
        List<ExtractionResult> data;

        cConfig = checkConfigMock(2 * 60, 3 * 60, MINUTES);
        data = extract(cConfig);
        int size = 60;
        assertSize(data, size);

        cConfig = checkConfigMock(2 * 1, 3 * 1, HOURS);
        data = extract(cConfig);
        assertSize(data, 1);

        cConfig = checkConfigMock(1, 2, DAYS);
        data = extract(cConfig);
        assertEmpty(data);
    }

    @Test
    public void shouldExtractDataForMinutesAndHoursAndDays() {
        Instant now = Instant.ofEpochSecond(5 * 24 * 60 * 60);
        when(this.clockMock.instant()).thenReturn(now);
        this.instance.setExtractSince(FORMATTER_WITH_ZONE.format(now.minus(2, DAYS)));

        CheckConfig cConfig;
        List<ExtractionResult> data;

        cConfig = checkConfigMock(2 * 24 * 60L, 3 * 24 * 60L, MINUTES);
        data = extract(cConfig);
        assertSize(data, 24 * 60);

        cConfig = checkConfigMock(2 * 24, 3 * 24, HOURS);
        data = extract(cConfig);
        assertSize(data, 1 * 24);

        cConfig = checkConfigMock(2, 3, DAYS);
        data = extract(cConfig);
        assertSize(data, 1);
    }

    @Test
    public void shouldNotExtractDataForMinutesAndHoursAndDays() {
        Instant now = Instant.ofEpochSecond(5 * 24 * 60 * 60);
        when(this.clockMock.instant()).thenReturn(now);
        this.instance.setExtractSince(FORMATTER_WITH_ZONE.format(now));

        CheckConfig cConfig;
        List<ExtractionResult> data;

        cConfig = checkConfigMock(2 * 24 * 60L, 3 * 24 * 60L, MINUTES);
        data = extract(cConfig);
        assertEmpty(data);

        cConfig = checkConfigMock(2 * 24, 3 * 24, HOURS);
        data = extract(cConfig);
        assertEmpty(data);

        cConfig = checkConfigMock(2, 3, DAYS);
        data = extract(cConfig);
        assertEmpty(data);
    }

    private EntityConfig getEntityConfig() {
        EntityConfig eConfig = mock(EntityConfig.class);
        when(eConfig.getSystem()).thenReturn("CMW");
        when(eConfig.getEntityKeys()).thenReturn(singletonMap("a", "b"));
        return eConfig;
    }

    private CheckConfig checkConfigMock(long timeSlots, long startTime, ChronoUnit unit) {
        CheckConfig cConfig = mock(CheckConfig.class);
        when(cConfig.getStartTime()).thenReturn(startTime);
        when(cConfig.getTimeUnit()).thenReturn(unit);
        when(cConfig.getTimeSlots()).thenReturn(timeSlots);
        return cConfig;
    }

    private void assertEmpty(List<ExtractionResult> data) {
        Assert.assertNotNull(data);
        Assert.assertTrue(data.isEmpty());
    }

    private void assertSize(List<ExtractionResult> data, long size) {
        Assert.assertNotNull(data);
        Assert.assertEquals(data.size(), size);
    }

    private List<ExtractionResult> extract(CheckConfig cConfig) {
        return this.instance.extract(eConfig, cConfig);
    }
}