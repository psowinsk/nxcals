package cern.nxcals.monitoring.reader.domain;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ReaderConfigurationTest {

    @Mock
    private CheckConfig checkConfig;

    @Test
    public void shouldFindByName() {
        //given
        ReaderConfiguration configuration = new ReaderConfiguration();
        configuration.setChecks(Collections.singletonList(checkConfig));
        when(checkConfig.getName()).thenReturn("test");
        //when
        CheckConfig retConfig = configuration.getByName("test").orElseThrow(()->new IllegalStateException("Cannot find check config"));

        //then
        assertEquals(checkConfig,retConfig);

    }
}