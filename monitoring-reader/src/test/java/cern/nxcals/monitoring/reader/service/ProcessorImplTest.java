package cern.nxcals.monitoring.reader.service;

import cern.nxcals.monitoring.reader.ConfigurationProvider;
import cern.nxcals.monitoring.reader.domain.CheckConfig;
import cern.nxcals.monitoring.reader.domain.EntityConfig;
import cern.nxcals.monitoring.reader.domain.ExtractionResult;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.actuate.metrics.GaugeService;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptException;
import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("squid:S2925" ) //Thread.sleep() required to test
public class ProcessorImplTest {
    @Mock
    private ConfigurationProvider configurationProvider;
    @Mock
    private EntityConfig entityConfig;
    @Mock
    private CheckConfig checkConfig;
    @Mock
    private DataExtractor dataExtractor;
    @Mock
    private ExtractionResult extractionResult;
    @Mock
    private GaugeService gaugeService;
    @Mock
    private ScriptEngine scriptEngine;
    @Mock
    private Dataset<Row> dataset;
    @Mock
    private Bindings bindings;
    @Mock
    private ScheduledExecutorService scheduledExecutorService;
    private static final Long PERIOD = 10L;

    @Before
    public void setup() throws ScriptException  {
        when(configurationProvider.getPeriod()).thenReturn(PERIOD);
        when(configurationProvider.getPeriodTimeUnit()).thenReturn(TimeUnit.SECONDS);
        when(configurationProvider.getEntities()).thenReturn(Collections.singletonList(entityConfig));
        when(entityConfig.getCheckRefs()).thenReturn(Collections.singletonList("REF"));
        when(configurationProvider.getByName("REF")).thenReturn(Optional.of(checkConfig));
        when(dataExtractor.extract(entityConfig,checkConfig)).thenReturn(Collections.singletonList(extractionResult));
        when(extractionResult.getDataset()).thenReturn(dataset);
        when(scriptEngine.createBindings()).thenReturn(bindings);
        when(checkConfig.getCondition()).thenReturn("cond");
        when(scriptEngine.eval("cond", bindings)).thenReturn(Boolean.TRUE);
        when(entityConfig.getName()).thenReturn("entity");
        when(checkConfig.getName()).thenReturn("config");
    }
    @Test
    public void shouldScheduleRun() {
        //given
        ProcessorImpl processor = new ProcessorImpl(configurationProvider, dataExtractor,gaugeService, scriptEngine,scheduledExecutorService );
        when(configurationProvider.getPeriod()).thenReturn(PERIOD);
        when(configurationProvider.getPeriodTimeUnit()).thenReturn(TimeUnit.SECONDS);
        //when
        processor.process();

        //then
        verify(scheduledExecutorService).scheduleAtFixedRate(any(),eq(0L),eq(PERIOD), eq(TimeUnit.SECONDS));


    }

    @Test
    public void processOk() throws InterruptedException {
        //given
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        ProcessorImpl processor = new ProcessorImpl(configurationProvider, dataExtractor,gaugeService, scriptEngine, executor);

        //when
        processor.process();
        Thread.sleep(1000);
        //then
        verify(dataExtractor,atLeastOnce()).extract(entityConfig,checkConfig);
        verify(gaugeService,atLeastOnce()).submit("entity_config", 0.0d);


    }

    @Test
    public void processError() throws InterruptedException, ScriptException {
        //given
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        ProcessorImpl processor = new ProcessorImpl(configurationProvider, dataExtractor,gaugeService, scriptEngine, executor);
        when(dataExtractor.extract(entityConfig,checkConfig)).thenThrow(new RuntimeException());

        //when
        processor.process();
        Thread.sleep(1000);
        //then
        verify(dataExtractor,atLeastOnce()).extract(entityConfig,checkConfig);
        verify(gaugeService,atLeastOnce()).submit("entity_config", 1.0d);


    }

    @Test
    public void processScriptError() throws InterruptedException, ScriptException {
        //given
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        ProcessorImpl processor = new ProcessorImpl(configurationProvider, dataExtractor,gaugeService, scriptEngine, executor);
        when(scriptEngine.eval("cond", bindings)).thenThrow(new RuntimeException());


        //when
        processor.process();
        Thread.sleep(1000);
        //then
        verify(dataExtractor,atLeastOnce()).extract(entityConfig,checkConfig);
        verify(gaugeService,atLeastOnce()).submit("entity_config", 1.0d);


    }

}