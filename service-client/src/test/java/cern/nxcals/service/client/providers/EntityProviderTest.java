/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.client.providers;

import cern.nxcals.common.domain.EntityData;
import cern.nxcals.common.domain.EntityHistoryData;
import cern.nxcals.common.domain.FindOrCreateEntityRequest;
import cern.nxcals.common.domain.PartitionData;
import cern.nxcals.common.domain.SchemaData;
import cern.nxcals.common.domain.SystemData;
import cern.nxcals.service.client.DataConflictRuntimeException;
import cern.nxcals.service.client.domain.KeyValues;
import cern.nxcals.service.client.providers.feign.EntityClient;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Marcin Sobieszek
 * @author jwozniak
 * @date Jul 22, 2016 4:33:06 PM
 */
@RunWith(MockitoJUnitRunner.class)
public class EntityProviderTest extends AbstractProviderTest {

    private EntityProvider entityProvider;

    @Mock
    private EntityClient httpClient;

    @Before
    public void setup() {
        this.entityProvider = new EntityProvider(httpClient);
        reset(httpClient);
    }

    private EntityData createEntityData(long id, SortedSet<EntityHistoryData> histData) {
        SystemData systemData = SystemData.builder().id(1)
                .name(SYSTEM_NAME)
                .entityKeyDefinitions(ENTITY_SCHEMA.toString())
                .partitionKeyDefinitions(PARTITION_SCHEMA.toString())
                .timeKeyDefinitions(TIME_KEY_SCHEMA.toString())
                .build();

        PartitionData partitionData = PartitionData.builder()
                .id(1)
                .keyValues(PARTITION_KEY_VALUES)
                .build();

        SchemaData schemaData = SchemaData.builder()
                .id(1)
                .schema(ENTITY_SCHEMA.toString())
                .build();

        return EntityData.builder(id, ENTITY_KEY_VALUES, systemData, partitionData, schemaData, histData, null, 0L)
                .build();
    }

    private EntityData createEntityDataWithHistory(long id, Map<String, Object> partitionKeyValues,
            List<String> schemas) {
        return createEntityDataWithHistoryWithTimeDiff(id, partitionKeyValues, schemas, 1);
    }

    private EntityData createEntityDataWithHistoryWithTimeDiff(long id, Map<String, Object> partitionKeyValues,
            List<String> schemas,
            long histTimeDifference) {

        SortedSet<EntityHistoryData> histData = new TreeSet<>();
        for (int i = 0; i < schemas.size(); i++) {
            histData.add(EntityHistoryData.builder().id(i)
                    .schemaData(SchemaData.builder().id(0).schema(schemas.get(i)).build())
                    .partitionData(PartitionData.builder().id(0).keyValues(partitionKeyValues).build())
                    .validFromStamp(Long.valueOf(i * histTimeDifference))
                    .validToStamp(i == schemas.size() - 1 ? null : Long.valueOf((i + 1) * histTimeDifference))
                    .build());
        }

        return createEntityData(id, histData);
    }

    @Test
    public void shouldNotObtainEntityDataForNonExistingEntityKeyValues() {
        when(this.httpClient.findBySystemIdAndKeyValues(SYSTEM_ID, ENTITY_KEY_VALUES)).thenReturn(null);
        EntityData data = this.entityProvider.findBySystemIdAndKeyValues(SYSTEM_ID, ENTITY_KEY_VALUES);
        assertNull(data);
        verify(this.httpClient, times(1)).findBySystemIdAndKeyValues(SYSTEM_ID, ENTITY_KEY_VALUES);
    }

    @Test
    public void shouldNotObtainEntityDataForExistingEntityKeyValuesFromCache() {
        EntityData keyData = createEntityData(0, Collections.emptySortedSet());
        when(this.httpClient.findBySystemIdAndKeyValues(SYSTEM_ID, ENTITY_KEY_VALUES)).thenReturn(keyData);

        for (int i = 0; i < 10; i++) {
            EntityData data = this.entityProvider.findBySystemIdAndKeyValues(SYSTEM_ID, ENTITY_KEY_VALUES);
            assertNotNull(data);
            assertEquals(keyData, data);
        }
        Mockito.verify(this.httpClient, times(10)).findBySystemIdAndKeyValues(SYSTEM_ID, ENTITY_KEY_VALUES);
    }

    @Test
    public void shouldExtendEntityHistoryDataForMigration() {

        // given
        final long ENTITYID = 100;
        List<String> schemas = Arrays.asList(SCHEMA1.toString(), SCHEMA1.toString());
        long histTimeDifference = 1;
        long firstTimestamp = 10;

        SortedSet<EntityHistoryData> histData1 = new TreeSet<>();  // unmodified history
        SortedSet<EntityHistoryData> histData2 = new TreeSet<>();  // history with migration

        histData2.add(EntityHistoryData.builder().id(0)
                .schemaData(SchemaData.builder().id(0).schema(SCHEMA2.toString()).build())
                .partitionData(PartitionData.builder().id(0).keyValues(PARTITION_KEY_VALUES).build())
                .validFromStamp(0L).validToStamp(10L).build());

        for (int i = 0; i < schemas.size(); i++) {
            EntityHistoryData entityHistoryData = EntityHistoryData.builder().id(i + 1)
                    .schemaData(SchemaData.builder().id(0).schema(schemas.get(i)).build())
                    .partitionData(PartitionData.builder().id(0).keyValues(PARTITION_KEY_VALUES).build())
                    .validFromStamp(Long.valueOf(i * histTimeDifference + firstTimestamp))
                    .validToStamp(i == schemas.size() - 1 ?
                            null :
                            Long.valueOf((i + 1) * histTimeDifference) + firstTimestamp)
                    .build();

            histData1.add(entityHistoryData);
            histData2.add(entityHistoryData);
        }

        EntityData keyData1 = createEntityData(ENTITYID, histData1);
        EntityData keyData2 = createEntityData(ENTITYID, histData2);

        // when
        when(this.httpClient.extendEntityFirstHistoryDataFor(ENTITYID, 0L, SCHEMA2.toString())).thenReturn(keyData2);

        EntityData data = this.entityProvider.extendEntityFirstHistoryDataFor(ENTITYID, SCHEMA2.toString(), 0L);

        // then
        assertNotNull(data);
        assertEquals(keyData2, data);
    }

    @Test
    public void shouldCreateEntityDataForExistingKeyValuesAndSchemaFromCache() {
        EntityData keyData = createEntityDataWithHistory(0, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA1.toString(), SCHEMA2.toString()));
        FindOrCreateEntityRequest findOrCreateEntityRequest = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES)
                .partitionKeyValues(PARTITION_KEY_VALUES)
                .schema(SCHEMA1.toString())
                .build();
        when(httpClient.findOrCreateEntityFor(SYSTEM_ID, RECORD_TIME, findOrCreateEntityRequest)).thenReturn(keyData);

        KeyValues entityKeyValues = mock(KeyValues.class);
        when(entityKeyValues.getId()).thenReturn("");
        when(entityKeyValues.getKeyValues()).thenReturn(ENTITY_KEY_VALUES);

        KeyValues partitionKeyValues = mock(KeyValues.class);
        when(partitionKeyValues.getKeyValues()).thenReturn(PARTITION_KEY_VALUES);

        for (int i = 0; i < 10; i++) {
            EntityData data = entityProvider
                    .findOrCreateEntityFor(SYSTEM_ID, entityKeyValues, partitionKeyValues, SCHEMA1.toString(),
                            RECORD_TIME);
            assertNotNull(data);
            assertEquals(keyData, data);
        }
        verify(httpClient, times(1)).findOrCreateEntityFor(SYSTEM_ID, RECORD_TIME, findOrCreateEntityRequest);
    }

    @Test
    public void shouldCreateDifferentEntityDataForExistingKeyValuesAndDifferentSchemaFromCache() {
        // given
        EntityData keyData = createEntityDataWithHistory(0, PARTITION_KEY_VALUES, Arrays.asList(SCHEMA1.toString()));
        EntityData keyData1 = createEntityDataWithHistory(1, PARTITION_KEY_VALUES, Arrays.asList(SCHEMA2.toString()));

        FindOrCreateEntityRequest findOrCreateEntityRequest1 = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES)
                .partitionKeyValues(PARTITION_KEY_VALUES)
                .schema(SCHEMA1.toString())
                .build();
        FindOrCreateEntityRequest findOrCreateEntityRequest2 = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES1)
                .partitionKeyValues(PARTITION_KEY_VALUES)
                .schema(SCHEMA2.toString())
                .build();

        when(httpClient.findOrCreateEntityFor(SYSTEM_ID, RECORD_TIME, findOrCreateEntityRequest1)).thenReturn(keyData);
        when(httpClient.findOrCreateEntityFor(SYSTEM_ID, RECORD_TIME, findOrCreateEntityRequest2)).thenReturn(keyData1);

        KeyValues entityKeyValues = mock(KeyValues.class);
        when(entityKeyValues.getId()).thenReturn("");
        when(entityKeyValues.getKeyValues()).thenReturn(ENTITY_KEY_VALUES);

        KeyValues entityKeyValues1 = mock(KeyValues.class);
        when(entityKeyValues1.getId()).thenReturn("1");
        when(entityKeyValues1.getKeyValues()).thenReturn(ENTITY_KEY_VALUES1);

        KeyValues partitionKeyValues = mock(KeyValues.class);
        when(partitionKeyValues.getKeyValues()).thenReturn(PARTITION_KEY_VALUES);

        // when
        EntityData data0 = entityProvider.findOrCreateEntityFor(SYSTEM_ID, entityKeyValues, partitionKeyValues,
                SCHEMA1.toString(), RECORD_TIME);
        EntityData data1 = entityProvider.findOrCreateEntityFor(SYSTEM_ID, entityKeyValues, partitionKeyValues,
                SCHEMA1.toString(), RECORD_TIME);
        EntityData data2 = entityProvider.findOrCreateEntityFor(SYSTEM_ID, entityKeyValues1, partitionKeyValues,
                SCHEMA2.toString(), RECORD_TIME);
        EntityData data3 = entityProvider.findOrCreateEntityFor(SYSTEM_ID, entityKeyValues1, partitionKeyValues,
                SCHEMA2.toString(), RECORD_TIME);

        // then
        assertNotNull(data0);
        assertNotNull(data1);
        assertNotNull(data2);
        assertNotNull(data3);

        assertEquals(keyData, data0);
        assertEquals(data0, data1);
        assertEquals(keyData1, data2);
        assertEquals(data2, data3);

        verify(httpClient, times(1)).findOrCreateEntityFor(SYSTEM_ID, RECORD_TIME,
                findOrCreateEntityRequest1);
        verify(httpClient, times(1)).findOrCreateEntityFor(SYSTEM_ID, RECORD_TIME,
                findOrCreateEntityRequest2);
    }

    @Test
    public void shouldCreateDifferentEntityDataForExistingKeyValuesAndDifferentPartitionFromCache() {
        // given
        EntityData keyData = createEntityDataWithHistory(0, PARTITION_KEY_VALUES, Arrays.asList(SCHEMA1.toString()));
        EntityData keyData1 = createEntityDataWithHistory(1, PARTITION_KEY_VALUES1, Arrays.asList(SCHEMA1.toString()));

        FindOrCreateEntityRequest findOrCreateEntityRequest1 = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES)
                .partitionKeyValues(PARTITION_KEY_VALUES)
                .schema(SCHEMA1.toString())
                .build();
        FindOrCreateEntityRequest findOrCreateEntityRequest2 = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES1)
                .partitionKeyValues(PARTITION_KEY_VALUES1)
                .schema(SCHEMA1.toString())
                .build();

        when(httpClient.findOrCreateEntityFor(SYSTEM_ID, RECORD_TIME, findOrCreateEntityRequest1)).thenReturn(keyData);
        when(httpClient.findOrCreateEntityFor(SYSTEM_ID, RECORD_TIME, findOrCreateEntityRequest2)).thenReturn(keyData1);

        KeyValues entityKeyValues = mock(KeyValues.class);
        when(entityKeyValues.getId()).thenReturn("");
        when(entityKeyValues.getKeyValues()).thenReturn(ENTITY_KEY_VALUES);

        KeyValues entityKeyValues1 = mock(KeyValues.class);
        when(entityKeyValues1.getId()).thenReturn("1");
        when(entityKeyValues1.getKeyValues()).thenReturn(ENTITY_KEY_VALUES1);

        KeyValues partitionKeyValues = mock(KeyValues.class);
        when(partitionKeyValues.getKeyValues()).thenReturn(PARTITION_KEY_VALUES);

        KeyValues partitionKeyValues1 = mock(KeyValues.class);
        when(partitionKeyValues1.getKeyValues()).thenReturn(PARTITION_KEY_VALUES1);

        // when
        EntityData data0 = entityProvider.findOrCreateEntityFor(SYSTEM_ID, entityKeyValues, partitionKeyValues,
                SCHEMA1.toString(), RECORD_TIME);
        EntityData data1 = entityProvider.findOrCreateEntityFor(SYSTEM_ID, entityKeyValues, partitionKeyValues,
                SCHEMA1.toString(), RECORD_TIME);

        EntityData data2 = entityProvider.findOrCreateEntityFor(SYSTEM_ID, entityKeyValues1, partitionKeyValues1,
                SCHEMA1.toString(), RECORD_TIME);
        EntityData data3 = entityProvider.findOrCreateEntityFor(SYSTEM_ID, entityKeyValues1, partitionKeyValues1,
                SCHEMA1.toString(), RECORD_TIME);

        // then
        assertNotNull(data0);
        assertNotNull(data1);
        assertNotNull(data2);
        assertNotNull(data3);

        assertEquals(keyData, data0);
        assertEquals(data0, data1);
        assertEquals(keyData1, data2);
        assertEquals(data2, data3);

        verify(httpClient, times(1)).findOrCreateEntityFor(SYSTEM_ID, RECORD_TIME, findOrCreateEntityRequest1);
        verify(httpClient, times(1)).findOrCreateEntityFor(SYSTEM_ID, RECORD_TIME, findOrCreateEntityRequest2);
    }

    @Test(expected = IllegalStateException.class)
    public void shouldThrowOnHistoryRewriteWithWrongPartition() {
        // given
        EntityData keyData = createEntityDataWithHistory(0, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA2.toString(), SCHEMA3.toString()));

        FindOrCreateEntityRequest findOrCreateEntityRequest = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES)
                .partitionKeyValues(PARTITION_KEY_VALUES)
                .schema(SCHEMA2.toString())
                .build();

        when(httpClient.findOrCreateEntityFor(SYSTEM_ID, RECORD_TIME, findOrCreateEntityRequest)).thenReturn(keyData);

        KeyValues entityKeyValues = mock(KeyValues.class);
        when(entityKeyValues.getId()).thenReturn("");
        when(entityKeyValues.getKeyValues()).thenReturn(ENTITY_KEY_VALUES);

        KeyValues partitionKeyValues = mock(KeyValues.class);
        when(partitionKeyValues.getKeyValues()).thenReturn(PARTITION_KEY_VALUES);

        KeyValues partitionKeyValues1 = mock(KeyValues.class);
        when(partitionKeyValues1.getKeyValues()).thenReturn(PARTITION_KEY_VALUES1);

        // when
        entityProvider.findOrCreateEntityFor(SYSTEM_ID, entityKeyValues, partitionKeyValues, SCHEMA2.toString(),
                RECORD_TIME);

        entityProvider.findOrCreateEntityFor(SYSTEM_ID, entityKeyValues, partitionKeyValues1, SCHEMA2.toString(),
                RECORD_TIME);
    }

    @Test(expected = IllegalStateException.class)
    public void shouldThrowOnHistoryRewriteWithWrongSchema() {
        // given
        EntityData keyData = createEntityDataWithHistory(0, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA2.toString(), SCHEMA3.toString()));

        FindOrCreateEntityRequest findOrCreateEntityRequest = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES)
                .partitionKeyValues(PARTITION_KEY_VALUES)
                .schema(SCHEMA2.toString())
                .build();

        when(httpClient.findOrCreateEntityFor(SYSTEM_ID, RECORD_TIME, findOrCreateEntityRequest)).thenReturn(keyData);

        KeyValues entityKeyValues = mock(KeyValues.class);
        when(entityKeyValues.getId()).thenReturn("");
        when(entityKeyValues.getKeyValues()).thenReturn(ENTITY_KEY_VALUES);

        KeyValues partitionKeyValues = mock(KeyValues.class);
        when(partitionKeyValues.getKeyValues()).thenReturn(PARTITION_KEY_VALUES);

        // when
        entityProvider.findOrCreateEntityFor(SYSTEM_ID, entityKeyValues, partitionKeyValues, SCHEMA2.toString(),
                RECORD_TIME);

        entityProvider.findOrCreateEntityFor(SYSTEM_ID, entityKeyValues, partitionKeyValues, SCHEMA1.toString(),
                RECORD_TIME);
    }

    @Test
    public void shouldThrowOnHistoryRewriteWithWrongSchemaAndUpdateCache() {
        // given
        EntityData keyData = createEntityDataWithHistory(0, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA2.toString(), SCHEMA3.toString()));

        FindOrCreateEntityRequest findOrCreateEntityRequest1 = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES).
                        partitionKeyValues(PARTITION_KEY_VALUES)
                .schema(SCHEMA2.toString())
                .build();
        FindOrCreateEntityRequest findOrCreateEntityRequest2 = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES)
                .partitionKeyValues(PARTITION_KEY_VALUES)
                .schema(SCHEMA1.toString())
                .build();

        when(httpClient.findOrCreateEntityFor(SYSTEM_ID, RECORD_TIME, findOrCreateEntityRequest1)).thenReturn(keyData);

        //this one says Data Conflict for this record.
        when(httpClient.findOrCreateEntityFor(SYSTEM_ID, RECORD_TIME + 10, findOrCreateEntityRequest2))
                .thenThrow(new DataConflictRuntimeException());

        KeyValues entityKeyValues = mock(KeyValues.class);
        when(entityKeyValues.getId()).thenReturn("");
        when(entityKeyValues.getKeyValues()).thenReturn(ENTITY_KEY_VALUES);

        KeyValues partitionKeyValues = mock(KeyValues.class);
        when(partitionKeyValues.getKeyValues()).thenReturn(PARTITION_KEY_VALUES);

        // when
        entityProvider.findOrCreateEntityFor(SYSTEM_ID, entityKeyValues, partitionKeyValues,
                SCHEMA2.toString(), RECORD_TIME);

        //thats a wrong call.

        boolean exceptionThrow = false;

        try {
            //this should not be able to verify the record without calling the service which should trow an exception.
            //Under this condition the entityProvider should call the service to find the most recent state of this entity.
            entityProvider.findOrCreateEntityFor(SYSTEM_ID, entityKeyValues, partitionKeyValues,
                    SCHEMA1.toString(), RECORD_TIME + 10);
        } catch (IllegalStateException e) {
            exceptionThrow = true;
        }

        assertThat(exceptionThrow).isTrue();

        // then
        verify(httpClient, times(2)).findBySystemIdAndKeyValues(SYSTEM_ID, ENTITY_KEY_VALUES);

    }

    @Test
    public void shouldAcceptNewSchemaWithOneHistoricalValue() {
        //given
        EntityData keyData1 = createEntityDataWithHistoryWithTimeDiff(0, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA1.toString()),
                100);
        EntityData keyData2 = createEntityDataWithHistoryWithTimeDiff(0, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA1.toString(), SCHEMA2.toString()), 100);

        final long recordTimestamp = 100;

        FindOrCreateEntityRequest findOrCreateEntityRequest1 = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES)
                .partitionKeyValues(PARTITION_KEY_VALUES)
                .schema(SCHEMA1.toString())
                .build();
        FindOrCreateEntityRequest findOrCreateEntityRequest2 = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES)
                .partitionKeyValues(PARTITION_KEY_VALUES)
                .schema(SCHEMA2.toString())
                .build();

        when(httpClient.findOrCreateEntityFor(SYSTEM_ID, RECORD_TIME, findOrCreateEntityRequest1)).thenReturn(keyData1);

        when(httpClient.findOrCreateEntityFor(SYSTEM_ID, recordTimestamp, findOrCreateEntityRequest2))
                .thenReturn(keyData2);

        KeyValues entityKeyValues = mock(KeyValues.class);
        when(entityKeyValues.getId()).thenReturn("");
        when(entityKeyValues.getKeyValues()).thenReturn(ENTITY_KEY_VALUES);

        KeyValues partitionKeyValues = mock(KeyValues.class);
        when(partitionKeyValues.getKeyValues()).thenReturn(PARTITION_KEY_VALUES);

        //ask first for existing one to add to the cache
        entityProvider.findOrCreateEntityFor(SYSTEM_ID, entityKeyValues, partitionKeyValues,
                SCHEMA1.toString(), RECORD_TIME);

        // when
        EntityData data0 = entityProvider.findOrCreateEntityFor(SYSTEM_ID, entityKeyValues, partitionKeyValues,
                SCHEMA2.toString(), recordTimestamp);

        //then
        assertEquals(data0, keyData2);
    }

    @Test
    public void shouldCreateSameEntityDataForExistingKeyValuesAndDifferentSchemaFromCache() {
        // given
        long secondRecordTimestamp = RECORD_TIME + 1;
        EntityData keyData = createEntityDataWithHistory(0, PARTITION_KEY_VALUES, Arrays.asList(SCHEMA1.toString()));
        EntityData keyData1 = createEntityDataWithHistory(0, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA1.toString(), SCHEMA2.toString()));

        FindOrCreateEntityRequest findOrCreateEntityRequest1 = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES)
                .partitionKeyValues(PARTITION_KEY_VALUES)
                .schema(SCHEMA1.toString())
                .build();
        FindOrCreateEntityRequest findOrCreateEntityRequest2 = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES)
                .partitionKeyValues(PARTITION_KEY_VALUES)
                .schema(SCHEMA2.toString())
                .build();

        when(httpClient.findOrCreateEntityFor(SYSTEM_ID, RECORD_TIME, findOrCreateEntityRequest1)).thenReturn(keyData);
        when(httpClient.findOrCreateEntityFor(SYSTEM_ID, secondRecordTimestamp, findOrCreateEntityRequest2))
                .thenReturn(keyData1);

        KeyValues cachedRequest = mock(KeyValues.class);
        when(cachedRequest.getId()).thenReturn("");
        when(cachedRequest.getKeyValues()).thenReturn(ENTITY_KEY_VALUES);

        KeyValues partitionKeyValues = mock(KeyValues.class);
        when(partitionKeyValues.getKeyValues()).thenReturn(PARTITION_KEY_VALUES);

        // when
        EntityData data0 = entityProvider.findOrCreateEntityFor(SYSTEM_ID, cachedRequest, partitionKeyValues,
                SCHEMA1.toString(), RECORD_TIME);

        EntityData data2 = entityProvider.findOrCreateEntityFor(SYSTEM_ID, cachedRequest, partitionKeyValues,
                SCHEMA2.toString(), secondRecordTimestamp);

        EntityData data1 = entityProvider.findOrCreateEntityFor(SYSTEM_ID, cachedRequest, partitionKeyValues,
                SCHEMA1.toString(), RECORD_TIME);

        EntityData data3 = entityProvider.findOrCreateEntityFor(SYSTEM_ID, cachedRequest, partitionKeyValues,
                SCHEMA2.toString(), secondRecordTimestamp);

        // then
        assertNotNull(data0);
        assertNotNull(data1);
        assertNotNull(data2);
        assertNotNull(data3);

        assertEquals(keyData, data0);
        assertEquals(data0, data1);
        assertEquals(keyData1, data2);
        assertEquals(data2, data3);

        verify(httpClient, times(1)).findOrCreateEntityFor(SYSTEM_ID, RECORD_TIME, findOrCreateEntityRequest1);
        verify(httpClient, times(1))
                .findOrCreateEntityFor(SYSTEM_ID, secondRecordTimestamp, findOrCreateEntityRequest2);
    }

    @Test
    public void shouldAcceptNewSchemaWithOneHistoricalValueWithIds() {
        //given
        EntityData keyData1 = createEntityDataWithHistoryWithTimeDiff(0, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA1.toString()),
                100);
        EntityData keyData2 = createEntityDataWithHistoryWithTimeDiff(0, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA1.toString(), SCHEMA2.toString()), 100);

        final long recordTimestamp = 100;

        when(this.httpClient.findByEntityIdAndTimeWindow(keyData1.getId(), RECORD_TIME, RECORD_TIME))
                .thenReturn(keyData1);

        when(this.httpClient.findOrCreateEntityFor(SYSTEM_ID, keyData1.getId(), keyData1.getPartitionData().getId(),
                recordTimestamp, SCHEMA2.toString())).thenReturn(keyData2);

        //ask first for existing one to add to the cache
        this.entityProvider.findOrCreateEntityFor(SYSTEM_ID, keyData1.getId(), keyData1.getPartitionData().getId(),
                SCHEMA1.toString(), RECORD_TIME);

        // when
        EntityData data0 = this.entityProvider
                .findOrCreateEntityFor(SYSTEM_ID, keyData1.getId(), keyData1.getPartitionData().getId(),
                        SCHEMA2.toString(), recordTimestamp);

        //then
        assertEquals(data0, keyData2);

    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowOnEntityNotFound() {
        //given
        EntityData keyData1 = createEntityDataWithHistoryWithTimeDiff(0, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA1.toString()),
                100);

        final long recordTimestamp = 100;

        when(this.httpClient.findByEntityIdAndTimeWindow(keyData1.getId(), RECORD_TIME, RECORD_TIME)).thenReturn(null);

        //ask first for existing one to add to the cache
        this.entityProvider.findOrCreateEntityFor(SYSTEM_ID, keyData1.getId(), keyData1.getPartitionData().getId(),
                SCHEMA1.toString(), RECORD_TIME);

        // when
        this.entityProvider.findOrCreateEntityFor(SYSTEM_ID, keyData1.getId(), keyData1.getPartitionData().getId(),
                SCHEMA2.toString(), recordTimestamp);
    }

    @Test(expected = IllegalStateException.class)
    public void shouldThrowOnHistoryRewrite() {
        //given
        EntityData keyData1 = createEntityDataWithHistoryWithTimeDiff(0, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA1.toString(), SCHEMA2.toString()),
                100);

        final long recordTimestamp = 100;

        when(this.httpClient.findByEntityIdAndTimeWindow(keyData1.getId(), RECORD_TIME, RECORD_TIME))
                .thenReturn(keyData1);

        //ask first for existing one to add to the cache
        this.entityProvider.findOrCreateEntityFor(SYSTEM_ID, keyData1.getId(), keyData1.getPartitionData().getId(),
                SCHEMA1.toString(), RECORD_TIME);

        // when
        this.entityProvider.findOrCreateEntityFor(SYSTEM_ID, keyData1.getId(), 10000L, SCHEMA3.toString(), RECORD_TIME);
    }

    @Test
    public void shouldFindBySystemIdKeyValuesAndTimeWindow() {
        //given
        EntityData keyData = createEntityDataWithHistory(0, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA1.toString(), SCHEMA2.toString(), SCHEMA3.toString()));

        //when
        when(httpClient
                .findBySystemIdKeyValuesAndTimeWindow(SYSTEM_ID, ENTITY_KEY_VALUES, RECORD_TIME, RECORD_TIME + 3))
                .thenReturn(keyData);
        EntityData data = entityProvider
                .findBySystemIdKeyValuesAndTimeWindow(SYSTEM_ID, ENTITY_KEY_VALUES, RECORD_TIME, RECORD_TIME + 3);

        //then
        assertNotNull(data);
        assertEquals(3, data.getEntityHistoryData().size());
    }

    @Test
    public void shouldUpdateEntityNameBasedOnListOfEntityData() {
        //given
        EntityData entityData = createEntityDataWithHistory(0, PARTITION_KEY_VALUES,
                Collections.singletonList(SCHEMA1.toString()));

        List<EntityData> inputEntityDataList = Lists.newArrayList();
        inputEntityDataList.add(entityData);
        List<EntityData> entityDataList = Lists.newArrayList();
        EntityData entityDataMock = mock(EntityData.class);
        entityDataList.add(entityDataMock);

        //when
        when(httpClient.updateEntities(any(List.class))).thenReturn(entityDataList);
        List<EntityData> entityDataReturnedList = entityProvider.updateEntities(inputEntityDataList);

        //then
        assertThat(entityDataReturnedList)
                .hasSize(1)
                .contains(entityDataMock);
    }

    @Test
    public void shouldFindEntityById() {
        //given
        EntityData keyData = createEntityDataWithHistory(0, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA1.toString(), SCHEMA2.toString(), SCHEMA3.toString()));

        //when
        when(httpClient.findById(0)).thenReturn(keyData);

        EntityData data = entityProvider.findById(0);

        //then
        assertNotNull(data);
        assertEquals(0, data.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenFindAllEntitiesByIdInWithNullList() {
        entityProvider.findAllByIdIn(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenFindAllEntitiesByIdInWithEmptyList() {
        entityProvider.findAllByIdIn(Collections.emptyList());
    }

    @Test
    public void shouldFindAllEntitiesByIdInList() {
        //given
        EntityData keyData = createEntityDataWithHistory(0, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA1.toString(), SCHEMA2.toString(), SCHEMA3.toString()));

        EntityData otherKeyData = createEntityDataWithHistory(1, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA1.toString(), SCHEMA2.toString(), SCHEMA3.toString()));

        List<Long> requestedEntityIds = Arrays.asList(keyData.getId(), otherKeyData.getId());
        //when
        when(httpClient.findAllByIdIn(requestedEntityIds)).thenReturn(Arrays.asList(keyData, otherKeyData));

        List<EntityData> fetchedEntities = entityProvider.findAllByIdIn(requestedEntityIds);

        //then
        assertNotNull(fetchedEntities);
        assertEquals(2, fetchedEntities.size());
    }
}
