/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.client.demo;

import cern.nxcals.common.domain.EntityData;
import cern.nxcals.common.domain.SystemData;
import cern.nxcals.common.domain.VariableConfigData;
import cern.nxcals.common.domain.VariableData;
import cern.nxcals.common.utils.TimeUtils;
import cern.nxcals.service.client.api.EntitiesResourcesService;
import cern.nxcals.service.client.api.EntityService;
import cern.nxcals.service.client.api.VariableService;
import cern.nxcals.service.client.api.internal.InternalEntityService;
import cern.nxcals.service.client.api.internal.InternalPartitionService;
import cern.nxcals.service.client.api.internal.InternalSchemaService;
import cern.nxcals.service.client.api.internal.InternalSystemService;
import cern.nxcals.service.client.domain.KeyValues;
import cern.nxcals.service.client.domain.impl.SimpleKeyValues;
import cern.nxcals.service.client.providers.InternalServiceClientFactory;
import cern.nxcals.service.client.providers.ServiceClientFactory;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang.RandomStringUtils;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.Instant;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ClientDemo {
    //    protected static final SystemService systemService = ServiceClientFactory.createSystemService();
    //    protected static final InternalSystemService internalSystemService = InternalServiceClientFactory
    //            .createSystemService();
    //    protected static final EntityService entityService = ServiceClientFactory.createEntityService();
    //    protected static final InternalEntityService internalEntityService = InternalServiceClientFactory
    //            .createEntityService();
    //    protected static final InternalPartitionService internalPartitionService = InternalServiceClientFactory
    //            .createPartitionService();
    //    protected static final VariableService variableService = ServiceClientFactory.createVariableService();
    //    protected static final InternalSchemaService internalSchemaService = InternalServiceClientFactory
    //            .createSchemaService();
    //    protected static final EntitiesResourcesService entitiesResourcesService = ServiceClientFactory
    //            .createEntityResourceService();
    static {
        try {
            System.setProperty("service.url", "https://" + InetAddress.getLocalHost().getHostName() + ":19093");
        } catch (UnknownHostException exception) {
            throw new RuntimeException(
                    "Cannot acquire hostname programmatically, provide the name full name of localhost");
        }

        System.setProperty("kerberos.principal", "mock-system-user");
        System.setProperty("kerberos.keytab", ".service.keytab");

        System.setProperty("java.security.krb5.conf", "build/LocalKdc/krb5.conf");

        System.setProperty("javax.net.ssl.trustStore", "selfsigned.jks");
        System.setProperty("javax.net.ssl.trustStorePassword", "123456");
    }

    protected static final InternalSystemService internalSystemService = InternalServiceClientFactory
            .createSystemService();
    protected static final EntityService entityService = ServiceClientFactory.createEntityService();
    protected static final InternalEntityService internalEntityService = InternalServiceClientFactory
            .createEntityService();
    protected static final InternalPartitionService internalPartitionService = InternalServiceClientFactory
            .createPartitionService();
    protected static final VariableService variableService = ServiceClientFactory.createVariableService();
    protected static final InternalSchemaService internalSchemaService = InternalServiceClientFactory
            .createSchemaService();
    protected static final EntitiesResourcesService entitiesResourcesService = ServiceClientFactory
            .createEntityResourceService();

    private static final String RANDOM_STRING = RandomStringUtils.randomAscii(64);

    static final Map<String, Object> ENTITY_KEY_VALUES = ImmutableMap
            .of("device", "device_value" + RANDOM_STRING);
    static final Map<String, Object> ENTITY_KEY_VALUES_RESOURCES_TEST = ImmutableMap
            .of("device", "device_value_resources_test" + RANDOM_STRING);
    static final Map<String, Object> ENTITY_KEY_VALUES_SCHEMA_TEST = ImmutableMap
            .of("device", "device_value_schema_test" + RANDOM_STRING);
    static final Map<String, Object> ENTITY_KEY_VALUES_VARIABLE_TEST = ImmutableMap
            .of("device", "device_value_variable_test" + RANDOM_STRING);
    static final Map<String, Object> PARTITION_KEY_VALUES = ImmutableMap
            .of("specification", "devClass1" + RANDOM_STRING);

    static final String SCHEMA = "TEST_SCHEMA" + RANDOM_STRING;
    static final String MOCK_SYSTEM_NAME = "MOCK-SYSTEM";
    static InternalSystemService systemService = InternalServiceClientFactory.createSystemService();
    static final SystemData systemData = systemService.findByName(MOCK_SYSTEM_NAME);

    static final long RECORD_TIMESTAMP = 1476789831111222334L;
    static final long MOCK_SYSTEM_ID = -100;

    static String variableName = "VARIABLE_NAME" + RANDOM_STRING;

    public static void main(String[] args) {
        //lets first find some existing entity for which we will create variable

        KeyValues entityKeyValues = new SimpleKeyValues("", ENTITY_KEY_VALUES_VARIABLE_TEST);
        KeyValues partitionKeyValues = new SimpleKeyValues(null, PARTITION_KEY_VALUES);

        EntityData entityData = internalEntityService
                .findOrCreateEntityFor(systemData.getId(), entityKeyValues,
                        partitionKeyValues, "brokenSchema1", RECORD_TIMESTAMP);

        assertNotNull(entityData);

        VariableConfigData varConfData1 = VariableConfigData.builder().entityId(entityData.getId()).build();
        SortedSet<VariableConfigData> varConfSet = new TreeSet<>();
        varConfSet.add(varConfData1);
        VariableData varData = VariableData.builder().name(variableName).description("Description")
                .creationTimeUtc(TimeUtils.getNanosFromInstant(Instant.now())).variableConfigData(varConfSet).build();
        VariableData variableData = variableService.registerOrUpdateVariableFor(varData);
        assertNotNull(variableData);
        assertEquals(variableName, variableData.getVariableName());

        VariableData foundVariable = variableService.findByVariableName(variableName);
        assertEquals(variableName, foundVariable.getVariableName());

        VariableData foundVariable2 = variableService.findByVariableNameAndTimeWindow(variableName, 100, 200);
        assertEquals(variableName, foundVariable2.getVariableName());
    }
}
