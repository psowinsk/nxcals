package cern.nxcals.service.client.security;

import com.google.common.collect.ImmutableMap;
import feign.Client;
import feign.Request;
import feign.Response;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.minikdc.MiniKdc;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Properties;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * Created by wjurasz on 13.09.17.
 */
@Slf4j
@RunWith(MockitoJUnitRunner.class)
public class KerberosAwareClientTest {

    private static String KEYTAB_PATH = "test.keytab";
    private static String HOST;
    private static String SERVICE_PRINCIPAL;
    private static MiniKdc kdc;
    private static File workDir;
    private static Properties conf;
    private static String user = System.getProperty("user.name");

    static {
        System.setProperty("kerberos.principal", user);
        System.setProperty("kerberos.keytab", KEYTAB_PATH);
        try {
            HOST = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException ex) {
            log.error("Could not get localhost name.", ex);
        }
        SERVICE_PRINCIPAL = "HTTPS/" + HOST;
    }

    @Mock
    private Client client;

    @Before
    public void startMiniKdc() throws Exception {
        createTestDir();
        createMiniKdcConf();

        kdc = new MiniKdc(conf, workDir);
        kdc.start();
        kdc.createPrincipal(new File(KEYTAB_PATH), user, SERVICE_PRINCIPAL);
    }

    @Test
    public void shouldExecuteRequest() throws IOException {
        Request request = Request.create("GET", "https://" + HOST + ":8080/some/path", new HashMap<>(), null, null);
        Response response = Response.builder().status(200).headers(new HashMap<>()).body(new byte[0]).request(request)
                .build();
        when(client.execute(any(Request.class), any(Request.Options.class))).thenReturn(response);
        KerberosAwareClient kerberosAwareClient = KerberosAwareClient.builder()
                .setServiceType("HTTPS")
                .setKeytabLocation(KEYTAB_PATH)
                .setUserPrincipal(user + "@CERN.CH")
                .setFeignClient(client)
                .setLoginOptions(ImmutableMap.of("useTicketCache", "false", "renewTGT", "false"))
                .build();
        Response execute = kerberosAwareClient.execute(request, new Request.Options());
        assertEquals(execute, response);

    }

    @After
    public void stopMiniKdc() {
        if (kdc != null) {
            kdc.stop();
        }
        File keytab = new File(KEYTAB_PATH);
        if (keytab.exists()) {
            keytab.delete();
        }
    }

    private static void createTestDir() {
        workDir = new File(java.lang.System.getProperty("test.dir", "build"));
        if (!workDir.exists()) {
            workDir.mkdirs();
        }
    }

    private static void createMiniKdcConf() {
        conf = MiniKdc.createConf();
        conf.setProperty("org.name", "CERN");
        conf.setProperty("org.domain", "CH");
    }

}
