/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.client.providers;

import cern.nxcals.common.domain.VariableConfigData;
import cern.nxcals.common.domain.VariableData;
import cern.nxcals.service.client.providers.feign.VariableClient;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.SortedSet;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class VariableProviderTest {

    private static final String TEST_VAR_NAME = "NAME";
    private static final String TEST_VAR_SEARCH_EXPRESSION = "TEST123";
    private static final String TEST_VAR_DESCRIPTION = "Description...";
    private VariableProvider variableProvider;

    @Mock
    private VariableClient httpService;

    @Before
    public void setup() {
        this.variableProvider = new VariableProvider(httpService);
        reset(httpService);
    }

    private VariableData createVariableData(String varName) {
        return createVariableData(varName, TEST_VAR_DESCRIPTION, null, null);
    }

    private VariableData createVariableData(String variableName, String description, Long creationTimeUtc,
            SortedSet<VariableConfigData> variableConfigData) {
        return VariableData.builder().name(variableName).description(description).creationTimeUtc(creationTimeUtc)
                .variableConfigData(variableConfigData).build();
    }

    @Test
    public void shouldGetData() {
        String variableName = TEST_VAR_NAME;
        VariableData varData = this.createVariableData(variableName);
        when(variableProvider.findByVariableName(variableName)).thenReturn(varData);

        assertNotNull(variableProvider.findByVariableName(variableName));
        System.out.println(varData.toString());
    }

    @Test
    public void shouldCreateData() {
        String variableName = TEST_VAR_NAME;
        VariableData variableData = createVariableData(variableName);
        when(variableProvider.registerOrUpdateVariableFor(variableData)).thenReturn(variableData);
        when(variableProvider.findByVariableName(variableName)).thenReturn(variableData);

        VariableData data1 = variableProvider.registerOrUpdateVariableFor(variableData);
        VariableData data2 = variableProvider.findByVariableName(variableName);
        assertEquals(data1, data2);
    }

    @Test
    public void shouldGetDataByNameRegex() {
        VariableData variableData = createVariableData(TEST_VAR_SEARCH_EXPRESSION);
        when(variableProvider.findByNameLike(TEST_VAR_SEARCH_EXPRESSION)).thenReturn(Lists.newArrayList(variableData));

        List<VariableData> returnedVariables = variableProvider.findByNameLike(TEST_VAR_SEARCH_EXPRESSION);

        assertThat(returnedVariables).containsExactly(variableData);
    }

    @Test
    public void shouldGetDataByDescriptionRegex() {
        VariableData variableData = createVariableData(TEST_VAR_NAME);
        when(variableProvider.findByDescriptionLike(TEST_VAR_SEARCH_EXPRESSION))
                .thenReturn(Lists.newArrayList(variableData));

        List<VariableData> returnedVariables = variableProvider.findByDescriptionLike(TEST_VAR_SEARCH_EXPRESSION);

        assertThat(returnedVariables).containsExactly(variableData);
    }

}
