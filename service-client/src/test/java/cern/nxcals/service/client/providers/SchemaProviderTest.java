/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.client.providers;

import cern.nxcals.common.domain.SchemaData;
import cern.nxcals.service.client.providers.feign.SchemaClient;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Marcin Sobieszek
 * @author jwozniak
 * @date Jul 22, 2016 11:39:57 AM
 */
@RunWith(MockitoJUnitRunner.class)
public class SchemaProviderTest extends AbstractProviderTest {
    private SchemaProvider schemaProvider;

    @Mock
    private SchemaClient httpService;

    @Before
    public void setup() {
        this.schemaProvider = new SchemaProvider(httpService);
        reset(httpService);
    }

    @Test
    public void shouldNotObtainSchemaForNonExistingId() {
        when(this.httpService.findById(-1)).thenReturn(null);

        SchemaData data = this.schemaProvider.findById(-1);

        assertNull(data);
        verify(this.httpService, times(1)).findById(-1);
    }

    @Test
    public void shouldGetSchemaDataById() {
        when(httpService.findById(1)).thenReturn(SchemaData.builder().id(1).schema(SCHEMA1.toString()).build());
        SchemaData data = schemaProvider.findById(1);
        Assert.assertEquals(1, data.getId());
        Assert.assertEquals(SCHEMA1.toString(), data.getSchemaJson());
        data = schemaProvider.findById(1);
        Assert.assertEquals(1, data.getId());
        Assert.assertEquals(SCHEMA1.toString(), data.getSchemaJson());
        Mockito.verify(httpService, times(1)).findById(1);

        Mockito.reset(httpService);
        Mockito.when(httpService.findById(2))
                .thenReturn(SchemaData.builder().id(2).schema(SCHEMA2.toString()).build());
        data = schemaProvider.findById(2);
        Assert.assertEquals(2, data.getId());
        Assert.assertEquals(SCHEMA2.toString(), data.getSchemaJson());
        data = schemaProvider.findById(2);
        Assert.assertEquals(2, data.getId());
        Assert.assertEquals(SCHEMA2.toString(), data.getSchemaJson());
        Mockito.verify(httpService, times(1)).findById(2);
    }

}
