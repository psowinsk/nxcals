package cern.nxcals.service.client.demo;

import cern.nxcals.service.client.api.internal.InternalCompactionService;
import cern.nxcals.service.client.providers.InternalServiceClientFactory;

/**
 * Created by ntsvetko on 7/28/17.
 */
public class CompactionServiceDemo {
    static {
        String USER = System.getProperty("user.name");
        String USER_HOME = System.getProperty("user.home");
        System.setProperty("service.url", "https://nxcals-" + USER + "1.cern.ch:19093");
        System.setProperty("kerberos.principal", USER);
        System.setProperty("kerberos.keytab", USER_HOME + "/.keytab");

        System.setProperty("javax.net.ssl.trustStore", "selfsigned.jks");
        // write your store password in the next parameter
        System.setProperty("javax.net.ssl.trustStorePassword", "<your_password>");
    }

    public static void main(String[] args) {
        InternalCompactionService compactionService = InternalServiceClientFactory.createCompactionService();
        String path = "/project/nxcals/nxcals_dev_ntsvetko/staging/CMW/2/3/4/2017-08-30";

        boolean result = compactionService.shouldCompact(path);

        System.out.println(result);
    }
}
