/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.client.providers;

import cern.nxcals.common.domain.PartitionData;
import cern.nxcals.service.client.providers.feign.PartitionClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static cern.nxcals.service.client.providers.DomainTestConstants.PARTITION_KEY_VALUES;
import static cern.nxcals.service.client.providers.DomainTestConstants.SYSTEM_ID;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(MockitoJUnitRunner.class)
public class PartitionProviderTest {
    private PartitionProvider partitionProvider;

    @Mock
    private PartitionClient httpService;

    @Before
    public void setup() {
        this.partitionProvider = new PartitionProvider(httpService);
        reset(httpService);
    }

    @Test
    public void shouldNotObtainPartitionDataForNonExistingPartition() {
        when(httpService.findBySystemIdAndKeyValues(SYSTEM_ID, PARTITION_KEY_VALUES)).thenReturn(null);
        PartitionData data = partitionProvider.findBySystemIdAndKeyValues(SYSTEM_ID, PARTITION_KEY_VALUES);
        assertNull(data);
        data = partitionProvider.findBySystemIdAndKeyValues(SYSTEM_ID, PARTITION_KEY_VALUES);
        assertNull(data);
        verify(httpService, times(2)).findBySystemIdAndKeyValues(SYSTEM_ID, PARTITION_KEY_VALUES);
    }

    @Test
    public void shouldObtainPartitionCachedDataForExistingPart() {
        PartitionData partData = PartitionData.builder().id(0).keyValues(PARTITION_KEY_VALUES).build();
        when(this.httpService.findBySystemIdAndKeyValues(SYSTEM_ID, PARTITION_KEY_VALUES)).thenReturn(partData);

        for (int i = 0; i < 10; i++) {
            PartitionData data = this.partitionProvider.findBySystemIdAndKeyValues(SYSTEM_ID, PARTITION_KEY_VALUES);
            assertNotNull(data);
            assertEquals(partData, data);

            data = this.partitionProvider.findBySystemIdAndKeyValues(SYSTEM_ID, PARTITION_KEY_VALUES);
            assertNotNull(data);
            assertEquals(partData, data);
        }
        verify(this.httpService, times(1)).findBySystemIdAndKeyValues(SYSTEM_ID, PARTITION_KEY_VALUES);
    }
}
