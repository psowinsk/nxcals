/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.client.demo;

import cern.nxcals.common.domain.EntityData;
import cern.nxcals.common.domain.SystemData;
import cern.nxcals.service.client.api.SystemService;
import cern.nxcals.service.client.api.internal.InternalEntityService;
import cern.nxcals.service.client.domain.KeyValues;
import cern.nxcals.service.client.domain.impl.SimpleKeyValues;
import cern.nxcals.service.client.providers.InternalServiceClientFactory;
import cern.nxcals.service.client.providers.ServiceClientFactory;

import java.util.concurrent.TimeUnit;

import static cern.nxcals.service.client.providers.DomainTestConstants.ENTITY_KEY_VALUES;
import static cern.nxcals.service.client.providers.DomainTestConstants.PARTITION_KEY_VALUES;
import static cern.nxcals.service.client.providers.TestSchemas.RECORD_VERSION_SCHEMA;

public class LoadBalanceDemo {
    static {
        String USER = System.getProperty("user.name");
        String USER_HOME = System.getProperty("user.home");
        System.setProperty("service.url", "http://nxcals-" + USER + "1:19093,http://nxcals-" + USER + "2:19093");
        System.setProperty("kerberos.principal", USER);
        System.setProperty("kerberos.keytab", USER_HOME + "/.keytab");
    }

    public static void main(String[] args) throws Exception {
        SystemService systemService = ServiceClientFactory.createSystemService();
        SystemData systemData = systemService.findByName("MOCK-SYSTEM");
        System.out.println(systemData);

        InternalEntityService entityService = InternalServiceClientFactory.createEntityService();

        KeyValues entityKeyValues = new SimpleKeyValues("", ENTITY_KEY_VALUES);
        KeyValues partitionKeyValues = new SimpleKeyValues(null, PARTITION_KEY_VALUES);

        //Please stop one of the services (on one machine to see if the balancing works)
        for (int i = 0; i < 120; i++) {
            EntityData entityData1 = entityService.findOrCreateEntityFor(systemData.getId(), entityKeyValues,
                    partitionKeyValues, RECORD_VERSION_SCHEMA.toString(), System.currentTimeMillis() * 1000_000);
            System.out.println("EntityId=" + entityData1.getId());
            TimeUnit.SECONDS.sleep(3);
        }

    }
}
