package cern.nxcals.service.client.providers;

/**
 * Created by jwozniak on 05/02/17.
 */
public class ClientDev {
    static {
        String USER = System.getProperty("user.name");
        String USER_HOME = System.getProperty("user.home");
        System.setProperty("service.url", "http://nxcals-" + USER + "1.cern.ch:19093");
        System.setProperty("kerberos.principal", USER);
        System.setProperty("kerberos.keytab", USER_HOME + "/.keytab");
        //        System.setProperty("javax.net.ssl.trustStore", "selfsigned.jks");
        //        System.setProperty("javax.net.ssl.trustStorePassword", "ALA123");
        //        System.setProperty("javax.net.ssl.keyStore", "selfsigned.jks");
        //        System.setProperty("javax.net.ssl.keyStorePassword", "ALA123");
    }
    //    static {
    //        //for localhost testing only
    //        javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
    //                new javax.net.ssl.HostnameVerifier(){
    //                    public boolean verify(String hostname,
    //                                          javax.net.ssl.SSLSession sslSession) {
    //                        System.err.println("Hostname" + hostname);
    //                        return true;
    //                    }
    //                });
    //        //javax.net.ssl.HttpsURLConnection.se
    //    }

    private static final String SCHEMA = "{\"device\": \"LARGER\", \"property\": \"intensityMarkerBct4\"}, partition={\"class\": \"SPS.Larger\", \"property\": \"intensityMarkerBct4\"}, schema={\"type\":\"record\",\"name\":\"data0\",\"namespace\":\"cern.nxcals\",\"fields\":[{\"name\":\"__sys_nxcals_system_id__\",\"type\":\"long\"},{\"name\":\"__sys_nxcals_entity_id__\",\"type\":\"long\"},{\"name\":\"__sys_nxcals_partition_id__\",\"type\":\"long\"},{\"name\":\"__sys_nxcals_schema_id__\",\"type\":\"long\"},{\"name\":\"__sys_nxcals_timestamp__\",\"type\":\"long\"},{\"name\":\"SPSX2QBCTX2QBeforeBeamDump\",\"type\":[\"double\",\"null\"]},{\"name\":\"SPSX2QBCTX2QBeforeBeamDumpX2Qedit\",\"type\":[\"double\",\"null\"]},{\"name\":\"SPSX2QBCTX2QEndFlatBottom\",\"type\":[\"double\",\"null\"]},{\"name\":\"SPSX2QBCTX2QEndFlatBottomX2Qedit\",\"type\":[\"double\",\"null\"]},{\"name\":\"SPSX2QBCTX2QInjection\",\"type\":[\"double\",\"null\"]},{\"name\":\"SPSX2QBCTX2QInjectionX2Q1\",\"type\":[\"double\",\"null\"]},{\"name\":\"SPSX2QBCTX2QInjectionX2Qedit\",\"type\":[\"double\",\"null\"]},{\"name\":\"SPSX2QBCTX2QInjectionX2QeditX2Q1\",\"type\":[\"double\",\"null\"]},{\"name\":\"SPSX2QBCTX2QInjectionX2QeditX2Qlast\",\"type\":[\"double\",\"null\"]},{\"name\":\"SPSX2QBCTX2QStartFlatTop\",\"type\":[\"double\",\"null\"]},{\"name\":\"SPSX2QBCTX2QStartFlatTopX2Qedit\",\"type\":[\"double\",\"null\"]},{\"name\":\"SPSX2QBCTX2QT1\",\"type\":[\"double\",\"null\"]},{\"name\":\"SPSX2QBCTX2QT1X2Qedit\",\"type\":[\"double\",\"null\"]},{\"name\":\"SPSX2QBCTX2QT2\",\"type\":[\"double\",\"null\"]},{\"name\":\"SPSX2QBCTX2QT2X2Qedit\",\"type\":[\"double\",\"null\"]},{\"name\":\"SPSX2QBCTX2QT3\",\"type\":[\"double\",\"null\"]},{\"name\":\"SPSX2QBCTX2QT3X2Qedit\",\"type\":[\"double\",\"null\"]},{\"name\":\"SPSX2QBCTX2QT4\",\"type\":[\"double\",\"null\"]},{\"name\":\"SPSX2QBCTX2QT4X2Qedit\",\"type\":[\"double\",\"null\"]},{\"name\":\"TOTAL_INJ\",\"type\":[\"double\",\"null\"]},{\"name\":\"__nxcals_timestamp__\",\"type\":[\"long\",\"null\"]},{\"name\":\"__record_timestamp__\",\"type\":\"long\"},{\"name\":\"__record_version__\",\"type\":\"long\"},{\"name\":\"acqDesc\",\"type\":[\"string\",\"null\"]},{\"name\":\"acqMsg\",\"type\":[\"string\",\"null\"]},{\"name\":\"acqStamp\",\"type\":[\"long\",\"null\"]},{\"name\":\"acqState\",\"type\":[\"int\",\"null\"]},{\"name\":\"acqTime\",\"type\":[\"string\",\"null\"]},{\"name\":\"beamID\",\"type\":[\"int\",\"null\"]},{\"name\":\"class\",\"type\":\"string\"},{\"name\":\"cycleTime\",\"type\":[\"string\",\"null\"]},{\"name\":\"cyclestamp\",\"type\":[\"long\",\"null\"]},{\"name\":\"device\",\"type\":\"string\"},{\"name\":\"deviceName\",\"type\":[\"string\",\"null\"]},{\"name\":\"dumpCycleTime\",\"type\":[\"int\",\"null\"]},{\"name\":\"dumpInt\",\"type\":[\"float\",\"null\"]},{\"name\":\"injectedArray\",\"type\":[{\"type\":\"array\",\"items\":[\"double\",\"null\"]},\"null\"]},{\"name\":\"lossAtFB\",\"type\":[\"double\",\"null\"]},{\"name\":\"measStamp\",\"type\":[{\"type\":\"array\",\"items\":[\"float\",\"null\"]},\"null\"]},{\"name\":\"measStamp_unit\",\"type\":[\"int\",\"null\"]},{\"name\":\"measStamp_unitExponent\",\"type\":[\"float\",\"null\"]},{\"name\":\"nbOfMeas\",\"type\":[\"int\",\"null\"]},{\"name\":\"observables\",\"type\":[\"int\",\"null\"]},{\"name\":\"propType\",\"type\":[\"int\",\"null\"]},{\"name\":\"property\",\"type\":\"string\"},{\"name\":\"samplingTime\",\"type\":[\"int\",\"null\"]},{\"name\":\"sbfCycleTime\",\"type\":[\"int\",\"null\"]},{\"name\":\"sbfIntensity\",\"type\":[\"float\",\"null\"]},{\"name\":\"selector\",\"type\":[\"string\",\"null\"]},{\"name\":\"slowExtInt\",\"type\":[\"float\",\"null\"]},{\"name\":\"superCycleNb\",\"type\":[\"int\",\"null\"]},{\"name\":\"totalIntensity\",\"type\":[{\"type\":\"array\",\"items\":[\"float\",\"null\"]},\"null\"]},{\"name\":\"totalIntensity_unit\",\"type\":[\"int\",\"null\"]},{\"name\":\"totalIntensity_unitExponent\",\"type\":[\"float\",\"null\"]}]}";
    private static final String ENTITY = "{\"device\": \"LARGER\", \"property\": \"intensityMarkerBct4\"}";
    private static final String PARTITION = "{\"class\": \"SPS.Larger\", \"property\": \"intensityMarkerBct4\"}";

    public static void main(String[] args) {
        //        System.err.println("Time:" + System.currentTimeMillis());
        //
        //        EntityService entityService = ServiceClientFactory.createEntityService();
        //
        //        EntityData entityData = entityService.findBySystemIdAndKeyValues(2, "{\"device\": \"LARGER\", \"property\": \"intensityMarkerBct3\"}");
        //
        //        entityData.getEntityHistoryData().forEach(hist-> {
        //            Instant from = TimeUtils.getInstantFromNanos(hist.getValidFromStamp());
        //            Instant to = hist.getValidToStamp() != null ? TimeUtils.getInstantFromNanos(hist.getValidToStamp()) : null;
        //            System.out.println(from + " - " + to);
        //        } );
        System.err.println(ServiceClientFactory.createSystemService().findByName("CMW"));

    }
}
