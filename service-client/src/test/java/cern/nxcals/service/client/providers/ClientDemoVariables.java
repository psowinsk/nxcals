package cern.nxcals.service.client.providers;

import cern.nxcals.common.domain.VariableConfigData;
import cern.nxcals.common.domain.VariableData;
import cern.nxcals.common.utils.TimeUtils;
import cern.nxcals.service.client.api.VariableService;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Created by ntsvetko on 3/10/17.
 */
public class ClientDemoVariables {
    static {
        String user = System.getProperty("user.name");
        System.setProperty("service.url", "http://nxcals-" + user + "1:19093");
    }

    public static VariableConfigData createVariableConfigData(long entityId, String fieldName,
            Long validFromStamp, Long validToStamp) {
        return VariableConfigData.builder().entityId(entityId).fieldName(fieldName).validToStamp(validToStamp)
                .validFromStamp(validFromStamp).build();
    }

    private static VariableData createVariableData(String variableName, String description, Long creationTimeUtc,
            SortedSet<VariableConfigData> variableConfigData) {
        return VariableData.builder().name(variableName).description(description).creationTimeUtc(creationTimeUtc)
                .variableConfigData(variableConfigData).build();
    }

    public static void main(String[] args) {

        VariableService variableService = ServiceClientFactory.createVariableService();
        VariableData variableData = variableService.findByVariableName("TestVariable");
        System.out.println("FOUND variable: " + variableData.toString());

        long t1 = TimeUtils.getNanosFromInstant(Instant.now().minus(5, ChronoUnit.DAYS));
        long t2 = TimeUtils.getNanosFromInstant(Instant.now().minus(2, ChronoUnit.DAYS));

        VariableConfigData varConfData1 = createVariableConfigData(500011, "field3", null, t1);
        VariableConfigData varConfData2 = createVariableConfigData(500011, "field2", t1, t2);
        VariableConfigData varConfData3 = createVariableConfigData(500011, "field4", t2, null);

        SortedSet<VariableConfigData> varConfSet = new TreeSet<>();
        varConfSet.add(varConfData1);
        varConfSet.add(varConfData2);
        varConfSet.add(varConfData3);

        VariableData varData = createVariableData(
                "TestVariable4",
                "Description",
                TimeUtils.getNanosFromInstant(Instant.now()),
                varConfSet);
        VariableData var1 = variableService.registerOrUpdateVariableFor(varData);
        System.out.println("Updated variable data: " + var1);

        long tNow = TimeUtils.getNanosFromInstant(Instant.now());

        VariableData var2 =
                variableService.findByVariableNameAndTimeWindow(
                        "TestVariable4", t2, TimeUtils.getNanosFromInstant(Instant.now()));
        System.err.println("Found in time window (" + t2 + ", " + tNow + "):" + var2.toString());

        VariableData testVariable = variableService.findByVariableName("TEST_DEV6");

        long splitTime = TimeUtils.getNanosFromInstant(Instant.now().minus(3, ChronoUnit.HOURS));

        SortedSet<VariableConfigData> newConfig = new TreeSet<>();
        VariableConfigData firstConf = testVariable.getVariableConfigData().first();
        newConfig.add(createVariableConfigData(
                firstConf.getEntityId(), firstConf.getFieldName(), null, splitTime));
        newConfig.add(createVariableConfigData(firstConf.getEntityId(), null, splitTime, null));

        VariableData newVariable = createVariableData(
                testVariable.getVariableName(),
                testVariable.getDescription(),
                TimeUtils.getNanosFromInstant(Instant.now()),
                newConfig);

        VariableData var5 = variableService.registerOrUpdateVariableFor(newVariable);

        System.err.println(var5.toString());
    }
}
