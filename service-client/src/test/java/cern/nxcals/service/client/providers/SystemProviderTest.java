/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.client.providers;

import cern.nxcals.common.domain.SystemData;
import cern.nxcals.service.client.providers.feign.SystemClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Marcin Sobieszek
 * @date Jul 22, 2016 4:13:37 PM
 */
@RunWith(MockitoJUnitRunner.class)
public class SystemProviderTest extends AbstractProviderTest {
    private SystemProvider systemProvider;

    @Mock
    private SystemClient httpService;

    @Before
    public void setup() {
        this.systemProvider = new SystemProvider(httpService);
        reset(httpService);
    }

    @Test
    public void shouldNotObtainSystemForNonExistingName() {
        when(this.httpService.findByName(SYSTEM_NAME)).thenReturn(null);
        SystemData data = this.systemProvider.findByName(SYSTEM_NAME);
        assertNull(data);
        verify(this.httpService, times(1)).findByName(SYSTEM_NAME);
    }

    @Test
    public void shouldObtainSystemForExistingName() {

        for (int i = 0; i < 10; i++) {
            String name = "test" + i;
            SystemData identity = SystemData.builder().id(i).name(name).entityKeyDefinitions(SCHEMA1.toString())
                    .partitionKeyDefinitions(PARTITION_SCHEMA.toString()).timeKeyDefinitions(TIME_KEY_SCHEMA.toString())
                    .build();
            when(this.httpService.findByName(name)).thenReturn(identity);
            SystemData data = this.systemProvider.findByName(name);
            assertNotNull(data);
            assertEquals(identity, data);

            data = this.systemProvider.findByName(name);
            assertNotNull(data);
            assertEquals(identity, data);
            verify(this.httpService, times(1)).findByName(name);

        }
    }
}
