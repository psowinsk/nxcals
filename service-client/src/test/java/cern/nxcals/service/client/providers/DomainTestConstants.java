/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.client.providers;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

import static cern.nxcals.service.client.providers.TestSchemas.ENTITY_DOUBLE_SCHEMA_KEY;
import static cern.nxcals.service.client.providers.TestSchemas.ENTITY_STRING_SCHEMA_KEY;
import static cern.nxcals.service.client.providers.TestSchemas.ENTITY_STRING_SCHEMA_KEY_1;
import static cern.nxcals.service.client.providers.TestSchemas.PARTITION_DOUBLE_SCHEMA_KEY;
import static cern.nxcals.service.client.providers.TestSchemas.PARTITION_STRING_SCHEMA_KEY;
import static cern.nxcals.service.client.providers.TestSchemas.PARTITION_STRING_SCHEMA_KEY_1;

public class DomainTestConstants {

    static final long SYSTEM_ID = 10;
    static final String SYSTEM_NAME = "SYSTEM_NAME";

    public static final Map<String, Object> PARTITION_KEY_VALUES = ImmutableMap
            .of(PARTITION_STRING_SCHEMA_KEY, "string", PARTITION_DOUBLE_SCHEMA_KEY, 2d);

    static final Map<String, Object> PARTITION_KEY_VALUES_1 = ImmutableMap
            .of(PARTITION_STRING_SCHEMA_KEY_1, "string");

    public static final Map<String, Object> ENTITY_KEY_VALUES = ImmutableMap
            .of(ENTITY_STRING_SCHEMA_KEY, "string", ENTITY_DOUBLE_SCHEMA_KEY, 2d);

    static final Map<String, Object> ENTITY_KEY_VALUES_1 = ImmutableMap
            .of(ENTITY_STRING_SCHEMA_KEY_1, "string");
    static final long ENTITY_ID = 40;

}
