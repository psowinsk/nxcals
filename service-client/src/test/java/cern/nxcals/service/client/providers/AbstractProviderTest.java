/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.client.providers;

import com.google.common.collect.ImmutableMap;
import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.junit.Ignore;

import java.util.Map;

/**
 * @author Marcin Sobieszek
 * @date Jul 22, 2016 4:14:41 PM
 */
@Ignore
public abstract class AbstractProviderTest {

    protected static final Schema DATA_SCHEMA = SchemaBuilder.record("test").fields()
            .name("name").type().stringType().noDefault()
            .name("number").type().intType().noDefault()
            .name("flag").type().booleanType().noDefault()
            .endRecord();

    protected static final Schema PARTITION_SCHEMA = SchemaBuilder.record("test").fields()
            .name("name").type().stringType().noDefault().endRecord();

    protected static final Schema ENTITY_SCHEMA = SchemaBuilder.record("test").fields()
            .name("number").type().intType().noDefault().endRecord();

    protected static final Schema TIME_KEY_SCHEMA = SchemaBuilder.record("test").fields()
            .name("timestamp").type().longType().noDefault().endRecord();

    protected static final long SYSTEM_ID = -1;
    protected static final String SYSTEM_NAME = "TEST_SYSTEM";
    protected static final Map<String, Object> ENTITY_KEY_VALUES = ImmutableMap.of("value", "1");
    protected static final Map<String, Object> ENTITY_KEY_VALUES1 = ImmutableMap.of("value", "2");
    protected static final Map<String, Object> PARTITION_KEY_VALUES = ImmutableMap.of("value", "3");
    protected static final Map<String, Object> PARTITION_KEY_VALUES1 = ImmutableMap.of("value", "4");
    protected static final long RECORD_TIME = 0;

    protected static final Schema SCHEMA1 = SchemaBuilder.record("schema1").fields()
            .name("schema1").type().stringType().noDefault().endRecord();
    protected static final Schema SCHEMA2 = SchemaBuilder.record("schema2").fields()
            .name("schema2").type().stringType().noDefault().endRecord();
    protected static final Schema SCHEMA3 = SchemaBuilder.record("schema3").fields()
            .name("schema3").type().stringType().noDefault().endRecord();

}
