/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.client.domain.impl;

import cern.nxcals.service.client.domain.KeyValues;

import java.util.HashMap;
import java.util.Map;

/**
 * Simple implementation of {@link KeyValues} that received both the id and key values as parameters.
 */
public class SimpleKeyValues extends AbstractKeyValues {

    private final String id;
    private final Map<String, Object> keyValues;

    /**
     * Creates an id and {@link Map} of keyValue tuple.
     * @param id id of object.
     * @param keyValues KeyValues of object.
     */
    public SimpleKeyValues(String id, Map<String, Object> keyValues) {
        this.id = id;
        this.keyValues = new HashMap<>(keyValues);
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Map<String, Object> getKeyValues() {
        return new HashMap<>(keyValues);
    }
}
