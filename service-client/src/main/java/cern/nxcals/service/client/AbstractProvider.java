/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.client;

import cern.nxcals.common.concurrent.AutoCloseableLock;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.google.common.util.concurrent.Striped;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.locks.Lock;

/**
 * Abstract class for data providers.
 *
 * @param <S> Id type.
 * @param <P> Key type.
 * @param <K> Data type.
 * @param <C> Client type.
 */
public abstract class AbstractProvider<S, P, K, C> {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractProvider.class);
    private final Striped<Lock> locks = Striped.lazyWeakLock(1000);
    private final Table<S, P, K> cache = HashBasedTable.create();
    protected final C httpClient;

    protected abstract K createDataForCache(S id, P value);

    protected AbstractProvider(C httpClient) {
        this.httpClient = httpClient;
    }

    private AutoCloseableLock getLockFor(String name) {
        if (StringUtils.isEmpty(name)) {
            throw new RuntimeException("Name must not be null");
        }
        return AutoCloseableLock.getFor(this.locks.get(name));
    }

    protected K getDataFromCache(S system, P key) {
        try (AutoCloseableLock lock = this.getLockFor(this.getHashFrom(system, key))) {
            K data = this.getCache().get(system, key);
            if (data != null) {
                LOGGER.debug("Found data in cache for system {} and key {}", system, key);
                return data;
            }
            LOGGER.debug("Could not find data in cache for system {} and key {}, calling service", system, key);
            data = this.createDataForCache(system, key);
            if (data == null) {
                return null;
            }
            this.getCache().put(system, key, data);
            return data;
        }
    }

    private Table<S, P, K> getCache() {
        return this.cache;
    }

    private String getHashFrom(S system, P records) {
        return String.valueOf(system) + String.valueOf(records);
    }

    protected C getHttpClient() {
        return this.httpClient;
    }
}
