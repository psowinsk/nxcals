/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.client.providers.feign;

import cern.nxcals.common.domain.EntityData;
import cern.nxcals.common.domain.FindOrCreateEntityRequest;
import feign.Body;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

import java.util.List;
import java.util.Map;

import static cern.nxcals.common.web.Endpoints.ENTITIES;
import static cern.nxcals.common.web.Endpoints.ENTITY_EXTEND_FIRST_HISTORY;
import static cern.nxcals.common.web.Endpoints.ENTITY_UPDATE;
import static cern.nxcals.common.web.HttpHeaders.ACCEPT_APPLICATION_JSON;
import static cern.nxcals.common.web.HttpHeaders.CONTENT_TYPE_APPLICATION_JSON;
import static cern.nxcals.common.web.HttpVerbs.GET;
import static cern.nxcals.common.web.HttpVerbs.POST;
import static cern.nxcals.common.web.HttpVerbs.PUT;

/**
 * Feign declarative service interface for consuming EntityService.
 */
//Methods are self-descriptive
@SuppressWarnings("squid:UndocumentedApi")
public interface EntityClient {
    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(POST + ENTITIES
            + "?systemId={systemId}")
    EntityData findBySystemIdAndKeyValues(
            @Param("systemId") long systemId,
            Map<String, Object> entityKeyValues
    );

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(POST + ENTITIES
            + "?systemId={systemId}&"
            + "startTime={startTime}&"
            + "endTime={endTime}")
    EntityData findBySystemIdKeyValuesAndTimeWindow(
            @Param("systemId") long systemId,
            Map<String, Object> entityKeyValues,
            @Param("startTime") long startTime,
            @Param("endTime") long endTime
    );

    @Headers(ACCEPT_APPLICATION_JSON)
    @RequestLine(GET + ENTITIES
            + "?keyValuesExpression={keyValuesExpression}")
    List<EntityData> findByKeyValuesLike(
            @Param("keyValuesExpression") String keyValuesExpression
    );

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(PUT + ENTITIES
            + "?systemId={systemId}&"
            + "recordTimestamp={recordTimestamp}")
    EntityData findOrCreateEntityFor(
            @Param("systemId") long systemId,
            @Param("recordTimestamp") long recordTimestamp,
            FindOrCreateEntityRequest findOrCreateEntityRequest
    );

    @Headers({ ACCEPT_APPLICATION_JSON })
    @Body("{schema}")
    @RequestLine(PUT + ENTITY_EXTEND_FIRST_HISTORY
            + "?entityId={entityId}&"
            + "from={from}")
    EntityData extendEntityFirstHistoryDataFor(
            @Param("entityId") long entityId,
            @Param("from") long from,
            @Param("schema") String schema
    );

    @Headers({ ACCEPT_APPLICATION_JSON })
    @Body("{schema}")
    @RequestLine(PUT + ENTITIES
            + "?systemId={systemId}"
            + "&entityId={entityId}"
            + "&partitionId={partitionId}"
            + "&recordTimestamp={recordTimestamp}")
    EntityData findOrCreateEntityFor(
            @Param("systemId") long systemId,
            @Param("entityId") long entityId,
            @Param("partitionId") long partitionId,
            @Param("recordTimestamp") long recordTimestamp,
            @Param("schema") String schema);

    @Headers(ACCEPT_APPLICATION_JSON)
    @RequestLine(GET + ENTITIES
            + "?entityId={entityId}&"
            + "startTime={startTime}&"
            + "endTime={endTime}")
    EntityData findByEntityIdAndTimeWindow(
            @Param("entityId") long entityId,
            @Param("startTime") long startTime,
            @Param("endTime") long endTime
    );

    @Headers({ CONTENT_TYPE_APPLICATION_JSON, ACCEPT_APPLICATION_JSON })
    @RequestLine(PUT + ENTITY_UPDATE)
    List<EntityData> updateEntities(
            List<EntityData> entityDataList
    );

    @Headers(ACCEPT_APPLICATION_JSON)
    @RequestLine(GET + ENTITIES + "/{entityId}")
    EntityData findById(
            @Param("entityId") long entityId
    );

    @Headers(ACCEPT_APPLICATION_JSON)
    @RequestLine(GET + ENTITIES
            + "?ids={entityIds}")
    List<EntityData> findAllByIdIn(
            @Param("entityIds") List<Long> entityIds
    );
}