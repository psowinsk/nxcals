package cern.nxcals.service.client.security;

/**
 * Paths in configuration enclosed in enum.
 *
 * @author wjurasz
 */
public enum PropertiesKeys {
    SERVICE_URL_CONFIG("service.url"),
    USER_PRINCIPAL_PATH("kerberos.principal"),
    USER_KEYTAB_PATH_CONFIG("kerberos.keytab");

    private String pathInProperties;

    PropertiesKeys(String pathInProperties) {
        this.pathInProperties = pathInProperties;
    }

    public String getPathInProperties() {
        return pathInProperties;
    }
}
