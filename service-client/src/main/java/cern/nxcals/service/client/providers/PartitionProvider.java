/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.client.providers;

import cern.nxcals.common.domain.PartitionData;
import cern.nxcals.service.client.AbstractProvider;
import cern.nxcals.service.client.api.internal.InternalPartitionService;
import cern.nxcals.service.client.providers.feign.PartitionClient;

import java.util.Map;

/**
 * @author Marcin Sobieszek
 * @date Jul 21, 2016 4:46:52 PM
 */
class PartitionProvider extends AbstractProvider<Long, Map<String, Object>, PartitionData, PartitionClient>
        implements InternalPartitionService {

    PartitionProvider(PartitionClient httpClient) {
        super(httpClient);
    }

    /**
     * @param systemId
     * @param partitionKeyValues
     * @return Returning null because for now there is nothing in exposed PartitionProvider (and you
     * have to override the method) but could be in a future.
     */
    @Override
    protected PartitionData createDataForCache(Long systemId, Map<String, Object> partitionKeyValues) {
        return this.getHttpClient().findBySystemIdAndKeyValues(systemId, partitionKeyValues);
    }

    @Override
    public PartitionData findBySystemIdAndKeyValues(long systemId, Map<String, Object> partitionKeyValues) {
        return super.getDataFromCache(systemId, partitionKeyValues);
    }
}
