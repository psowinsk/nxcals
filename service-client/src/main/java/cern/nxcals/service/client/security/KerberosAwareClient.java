package cern.nxcals.service.client.security;

import feign.Client;
import feign.Request;
import feign.Response;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.ietf.jgss.GSSContext;
import org.ietf.jgss.GSSCredential;
import org.ietf.jgss.GSSException;
import org.ietf.jgss.GSSManager;
import org.ietf.jgss.GSSName;
import org.ietf.jgss.Oid;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;
import javax.security.auth.Subject;
import javax.security.auth.login.AppConfigurationEntry;
import javax.security.auth.login.Configuration;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.security.Principal;
import java.security.PrivilegedAction;
import java.util.Base64;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Nxcals Implementation of feign {@link Client}.
 * It adds Kerberos token to the HTTP request header.
 *
 * @author wjurasz
 */
@Slf4j
public class KerberosAwareClient implements Client {

    //SPNEGO (Simple and Protected GSSAPI Negotiation Mechanism) is a mechanism used to negotiate security protocol.
    //In our case we are using it to establish Kerberos based connection.
    //This oid correspond to SPNEGO mechanism.
    private static final String SPNEGO_OID_VERSION = "1.3.6.1.5.5.2";
    private final Oid spnegoOid;
    private final String keytabLocation;
    private final String userPrincipal;
    private final String serviceType;
    private final Client client;
    private final ClientLoginConfig loginConfig;

    private KerberosAwareClient(String keytabLocation, String userPrincipal, Map<String, Object> loginOptions,
            String serviceType, Client client) {
        try {
            this.spnegoOid = new Oid(SPNEGO_OID_VERSION);
        } catch (GSSException gssException) {
            log.error("Error while creating Oid: ");
            throw new KerberosAuthenticationException("Error while creating Oid: " + SPNEGO_OID_VERSION, gssException);
        }
        this.client = client;
        this.serviceType = serviceType;
        this.keytabLocation = keytabLocation;
        this.userPrincipal = userPrincipal;
        this.loginConfig = new ClientLoginConfig(keytabLocation, userPrincipal, loginOptions);
        log.debug("KeytabLocation: {}, UserPrincipal: {}", this.keytabLocation, this.userPrincipal);
    }

    @Override
    public Response execute(Request request, Request.Options options) throws IOException {
        log.debug("Kerberos execute start");
        Subject subject = kerberosLogin();
        GSSManager manager = GSSManager.getInstance();
        String token = Subject.doAs(subject, (PrivilegedAction<String>) () -> {
            try {

                //There should be only one, if there is more all of them should be valid for service communication.
                Optional<Principal> principalOptional = subject.getPrincipals().stream().findFirst();
                if (!principalOptional.isPresent()) {
                    throw new KerberosAuthenticationException(
                            "Error while acquiring ticket for service communication. No user principal found");
                }
                //Create GSSName object from username used in client-service communication (it's a wrapper on user principal)
                GSSName clientName = manager.createName(principalOptional.get().getName(), GSSName.NT_USER_NAME);

                //Associate clientName with its credentials. Client is initiating connection .
                GSSCredential clientCred = manager
                        .createCredential(clientName, GSSCredential.INDEFINITE_LIFETIME, spnegoOid,
                                GSSCredential.INITIATE_ONLY);

                //Create GSSName object from service name used in client-service communication (it's a wrapper on service principal)
                GSSName serverName = manager.createName(serviceType + "@" + URI.create(request.url()).getHost(),
                        GSSName.NT_HOSTBASED_SERVICE);

                log.info("Trying to reach service host: {}", URI.create(request.url()).getHost());

                //Create context for client-service connection
                GSSContext context = manager
                        .createContext(serverName, spnegoOid, clientCred, GSSContext.DEFAULT_LIFETIME);

                //Client validates service, service validates client. Both sides of connection checks if request is valid.
                context.requestMutualAuth(true);
                //We are not encrypting the message, SSL takes care of that.
                context.requestConf(false);
                //Create checksum for message
                context.requestInteg(true);

                byte[] outToken = context.initSecContext(new byte[0], 0, 0);
                String encode = new String(Base64.getEncoder().encode(outToken), StandardCharsets.UTF_8);
                encode = encode.replace("\n", "");
                context.dispose();
                return encode;
            } catch (GSSException gssException) {
                log.error("Error while acquiring ticket for service communication ", gssException);
                throw new KerberosAuthenticationException("Error while acquiring ticket for service communication", gssException);
            }
        });
        Map<String, Collection<String>> headers = new HashMap<>(request.headers());
        headers.put("Authorization", Collections.singletonList("Negotiate " + token));
        Request requestWithAuthToken = Request
                .create(request.method(), request.url(), headers, request.body(), request.charset());
        log.debug("Kerberos execute end");
        Response response = client.execute(requestWithAuthToken, options);
        log.debug("Kerberos execute after response");
        return response;
    }

    /**
     * Static method to get {@link Builder} instance.
     *
     * @return new Builder.
     */
    public static Builder builder() {
        return new Builder();
    }

    private Subject kerberosLogin() {
        try {
            //This cannot be static instance. Here Subject is create with all arguments empty, but they are added in LoginContext.
            //In LoginContext static configuration is provided so in general outcome of such code should be the same every time.
            //But there exist a possibility to override those properties with System arguments (for example user principal can change).
            //So to maintain some level of genericness, those should be created on each request.
            Subject sub = new Subject(false, Collections.emptySet(), Collections.emptySet(), Collections.emptySet());
            LoginContext lc = new LoginContext("", sub, null, this.loginConfig);
            lc.login();
            return lc.getSubject();
        } catch (LoginException loginException) {
            log.error("Couldn't log in to Kerberos", loginException);
            throw new KerberosAuthenticationException("Couldn't log in to Kerberos", loginException);
        }
    }

    private static class ClientLoginConfig extends Configuration {

        private final String keyTabLocation;
        private final String userPrincipal;
        private final Map<String, Object> loginOptions;

        private ClientLoginConfig(String keyTabLocation, String userPrincipal, Map<String, Object> loginOptions) {
            super();
            this.keyTabLocation = keyTabLocation;
            this.userPrincipal = userPrincipal;
            this.loginOptions = loginOptions;
        }

        @Override
        public AppConfigurationEntry[] getAppConfigurationEntry(String name) {

            Map<String, Object> options = new HashMap<>();
            //We do not support password auth.
            options.put("doNotPrompt", "true");
            //If it is possible, use cached ticket.
            options.put("useTicketCache", "true");
            //In case there are some changes coming from System properties, we want to refresh config.
            options.put("refreshKrb5Config", "true");
            //If the ticket expires, renew it.
            options.put("renewTGT", "true");
            // LoginModule retrieves the username and password from the module's shared
            // state using "javax.security.auth.login.name" and "javax.security.auth.login.password" as the respective keys.
            // The retrieved values are used for authentication.
            options.put("useFirstPass", "true");

            //If the principal is provided use it, otherwise java will try to acquire one from cache.
            if (StringUtils.isNotEmpty(this.userPrincipal)) {
                options.put("principal", this.userPrincipal);
            }

            //If the cached ticket is not valid or it does not exist, new one will be acquired from KDC using keyTab.
            if (StringUtils.isNotEmpty(keyTabLocation)) {
                options.put("useKeyTab", "true");
                options.put("keyTab", this.keyTabLocation);
            }

            if (loginOptions != null) {
                options.putAll(loginOptions);
            }

            return new AppConfigurationEntry[] {
                    new AppConfigurationEntry("com.sun.security.auth.module.Krb5LoginModule",
                            AppConfigurationEntry.LoginModuleControlFlag.REQUIRED, options) };
        }
    }

    /**
     * Builder for {@link KerberosAwareClient}. All fields are optional.
     * However if {@param keytabLocation} or {@param userPrincipal} is not set, only cached kerberos ticket can be used.
     */
    public static class Builder {
        private String keytabLocation = null;
        private String userPrincipal = null;
        private Map<String, Object> loginOptions = null;
        private SSLSocketFactory sslContextFactory = null;
        private HostnameVerifier hostnameVerifier = null;
        private String serviceType = null;
        private Client client = null;

        private Builder() {
        }

        public Builder setKeytabLocation(String keytabLocation) {
            this.keytabLocation = keytabLocation;
            return this;
        }

        public Builder setUserPrincipal(String userPrincipal) {
            this.userPrincipal = userPrincipal;
            return this;
        }

        public Builder setLoginOptions(Map<String, Object> loginOptions) {
            if (loginOptions != null) {
                this.loginOptions = new HashMap<>(loginOptions);
            }
            return this;
        }

        public Builder setSslContextFactory(SSLSocketFactory sslContextFactory) {
            this.sslContextFactory = sslContextFactory;
            return this;
        }

        public Builder setHostnameVerifier(HostnameVerifier hostnameVerifier) {
            this.hostnameVerifier = hostnameVerifier;
            return this;
        }

        public Builder setServiceType(String serviceType) {
            this.serviceType = serviceType;
            return this;
        }

        public Builder setFeignClient(Client client) {
            this.client = client;
            return this;
        }

        /**
         * Builds {@link KerberosAwareClient}.
         *
         * @return new instance of {@link KerberosAwareClient}.
         */
        public KerberosAwareClient build() {
            if (client == null) {
                client = new Client.Default(sslContextFactory, hostnameVerifier);
            }
            return new KerberosAwareClient(keytabLocation, userPrincipal, loginOptions, serviceType, client);
        }
    }

}
