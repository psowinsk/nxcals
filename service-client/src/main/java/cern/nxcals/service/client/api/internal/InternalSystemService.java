package cern.nxcals.service.client.api.internal;

import cern.nxcals.common.domain.SystemData;
import cern.nxcals.service.client.api.SystemService;
import feign.Param;
import feign.RequestLine;

public interface InternalSystemService extends SystemService {
    @RequestLine("GET /systems/search/findById?id={id}")
    SystemData findById(@Param("id") long systemId);
}
