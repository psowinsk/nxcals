/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.client.providers;

import cern.nxcals.common.domain.SystemData;
import cern.nxcals.service.client.AbstractProvider;
import cern.nxcals.service.client.api.internal.InternalSystemService;
import cern.nxcals.service.client.providers.feign.SystemClient;
import lombok.NonNull;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Implementation of the SystemService using HTTP calls.
 *
 * @author Marcin Sobieszek
 * @author jwozniak
 * @date Jul 21, 2016 4:49:58 PM
 */
class SystemProvider extends AbstractProvider<String, String, SystemData, SystemClient>
        implements InternalSystemService {
    //Caching both should not be a problem as we will just have a couple of systems.
    private final ConcurrentHashMap<String, SystemData> cacheByName = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<Long, SystemData> cacheById = new ConcurrentHashMap<>();

    SystemProvider(SystemClient httpClient) {
        super(httpClient);
    }

    /**
     * This method is actually not used. We just use the ConcurrentHashMap for caching.
     * Necessary to have it as it is abstract in the base class.
     *
     * @param system
     * @param value
     * @return
     */
    @Override
    protected SystemData createDataForCache(String system, String value) {
        return null;
    }

    /* Finds system by name.
     * @param name - system name
     * @return
     */
    @Override
    public SystemData findByName(@NonNull String name) {
        return cacheByName.computeIfAbsent(name, key -> this.getHttpClient().findByName(key));
    }

    @Override
    public SystemData findById(long systemId) {
        return cacheById.computeIfAbsent(systemId, id -> this.getHttpClient().findById(id));
    }
}
