package cern.nxcals.service.client.security;

public class KerberosAuthenticationException extends RuntimeException {

    public KerberosAuthenticationException(String message) {
        super(message);
    }

    public KerberosAuthenticationException(String message, Throwable cause) {
        super(message, cause);
    }

}
