/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.client.domain.impl;

import cern.nxcals.service.client.domain.KeyValues;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Abstract implementation of {@link KeyValues} that enforces the correct behaviour.
 */
public abstract class AbstractKeyValues implements KeyValues {

    private Integer hashcode;

    @Override
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        AbstractKeyValues that = (AbstractKeyValues) obj;

        if (getId() == null) {
            return that.getId() == null;
        }

        return getId().equals(that.getId());
    }

    @Override
    public final int hashCode() {
        if (hashcode == null) {
            hashcode = new HashCodeBuilder(17, 37)
                    .append(getId())
                    .toHashCode();
        }
        return hashcode;
    }
}
