/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.client.providers;

import cern.nxcals.service.client.api.EntitiesResourcesService;
import cern.nxcals.service.client.api.EntityService;
import cern.nxcals.service.client.api.SchemaService;
import cern.nxcals.service.client.api.SystemService;
import cern.nxcals.service.client.api.VariableService;

/**
 * @author Marcin Sobieszek
 * @author jwozniak
 * @date Jul 21, 2016 4:38:07 PM
 */

public final class ServiceClientFactory {
    private static final SystemService systemService =
            new SystemProvider(ClientFactory.createSystemService());
    private static final SchemaService schemaService =
            new SchemaProvider(ClientFactory.createSchemaSerivce());
    private static final EntityService entityService =
            new EntityProvider(ClientFactory.createEntityService());
    private static final EntitiesResourcesService resourceService =
            new EntitiesResourcesProvider(ClientFactory.createResourceService());
    private static final VariableService variableService =
            new VariableProvider(ClientFactory.createVariableService());

    /**
     * @return Only instance of {@link SchemaService}.
     */
    public static SchemaService createSchemaService() {
        return schemaService;
    }

    /**
     * @return Only instance of {@link EntityService}.
     */
    public static EntityService createEntityService() {
        return entityService;
    }

    /**
     * @return Only instance of {@link SystemService}.
     */
    public static SystemService createSystemService() {
        return systemService;
    }

    /**
     * @return Only instance of {@link EntitiesResourcesService}.
     */
    public static EntitiesResourcesService createEntityResourceService() {
        return resourceService;
    }

    /**
     * @return Only instance of {@link VariableService}.
     */
    public static VariableService createVariableService() {
        return variableService;
    }

    private ServiceClientFactory() {
    }
}
