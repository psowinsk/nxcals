/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.client.providers;

import cern.nxcals.service.client.AbstractClientFactory;
import cern.nxcals.service.client.providers.feign.CompactionClient;
import cern.nxcals.service.client.providers.feign.EntitiyResourcesClient;
import cern.nxcals.service.client.providers.feign.EntityClient;
import cern.nxcals.service.client.providers.feign.PartitionClient;
import cern.nxcals.service.client.providers.feign.SchemaClient;
import cern.nxcals.service.client.providers.feign.SystemClient;
import cern.nxcals.service.client.providers.feign.VariableClient;

import java.lang.reflect.Proxy;

/**
 * A factory class that creates services for all meta-data access points using Netflix Feign + Ribbon.
 */
class ClientFactory extends AbstractClientFactory {
    private static final ClientFactory INSTANCE = new ClientFactory();
    private final PartitionClient partitionClient;
    private final SystemClient systemClient;
    private final SchemaClient schemaClient;
    private final EntityClient entityClient;
    private final EntitiyResourcesClient resourceClient;
    private final VariableClient variableService;
    private final CompactionClient compactionService;

    private ClientFactory() {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        this.partitionClient = createNotFoundRuntimeExceptionProxy(PartitionClient.class, classLoader);
        this.systemClient = createNotFoundRuntimeExceptionProxy(SystemClient.class, classLoader);
        this.schemaClient = createNotFoundRuntimeExceptionProxy(SchemaClient.class, classLoader);
        this.entityClient = createNotFoundRuntimeExceptionProxy(EntityClient.class, classLoader);
        this.resourceClient = createNotFoundRuntimeExceptionProxy(EntitiyResourcesClient.class, classLoader);
        this.variableService = createNotFoundRuntimeExceptionProxy(VariableClient.class, classLoader);
        this.compactionService = createNotFoundRuntimeExceptionProxy(CompactionClient.class, classLoader);
    }

    /**
     * Suppressing because for our use cases it will always succeed
     */
    @SuppressWarnings("unchecked")
    private <T> T createNotFoundRuntimeExceptionProxy(Class<T> clazz, ClassLoader classLoader) {
        return (T) Proxy.newProxyInstance(classLoader, new Class[] { clazz },
                new NotFoundRuntimeExceptionInterceptor<>(createServiceFor(clazz)));
    }

    static SchemaClient createSchemaSerivce() {
        return INSTANCE.schemaClient;
    }

    static EntityClient createEntityService() {
        return INSTANCE.entityClient;
    }

    static PartitionClient createPartitionService() {
        return INSTANCE.partitionClient;
    }

    static SystemClient createSystemService() {
        return INSTANCE.systemClient;
    }

    static EntitiyResourcesClient createResourceService() {
        return INSTANCE.resourceClient;
    }

    static VariableClient createVariableService() {
        return INSTANCE.variableService;
    }

    static CompactionClient createCompactionService() {
        return INSTANCE.compactionService;
    }
}
