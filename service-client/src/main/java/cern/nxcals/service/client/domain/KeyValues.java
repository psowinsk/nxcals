/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.client.domain;

import java.util.Map;

/**
 * Wrapper that provides a way to identify quickly if the key values are identical or not.
 */
public interface KeyValues {

    /**
     * A unique identifier of that represents the provided key values. For the same key values the same identifier must be generated.
     *
     * @return a {@link String}
     */
    String getId();

    /**
     * The key values that identify the entity.
     *
     * @return a {@link Map} of with the key values.
     */
    Map<String, Object> getKeyValues();
}
