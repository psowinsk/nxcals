/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.client.providers;

import cern.nxcals.common.domain.VariableData;
import cern.nxcals.service.client.AbstractProvider;
import cern.nxcals.service.client.api.internal.InternalVariableService;
import cern.nxcals.service.client.providers.feign.VariableClient;
import lombok.NonNull;

import java.util.List;

/**
 * Implementation of {@link cern.nxcals.service.client.api.VariableService} for getting {@link VariableData} objects
 */
class VariableProvider extends AbstractProvider<Long, String, VariableData, VariableClient>
        implements InternalVariableService {

    VariableProvider(VariableClient httpClient) {
        super(httpClient);
    }

    @Override
    public VariableData findByVariableName(@NonNull String variableName) {
        return getHttpClient().findByVariableName(variableName);
    }

    @Override
    public VariableData findByVariableNameAndTimeWindow(@NonNull String variableName, long startTime, long endTime) {
        return getHttpClient().findByVariableNameAndTimeWindow(variableName, startTime, endTime);
    }

    @Override
    public List<VariableData> findByNameLike(@NonNull String nameExpression) {
        return getHttpClient().findByNameLike(nameExpression);
    }

    @Override
    public List<VariableData> findByDescriptionLike(@NonNull String descriptionExpression) {
        return getHttpClient().findByDescriptionLike(descriptionExpression);
    }

    @Override
    public VariableData registerOrUpdateVariableFor(@NonNull VariableData variableData) {
        return getHttpClient().registerOrUpdateVariableFor(variableData);
    }

    /**
     * @param id
     * @param value
     * @return No caching implemented for VariableProvider
     */
    @Override
    protected VariableData createDataForCache(Long id, String value) {
        return null;
    }
}
