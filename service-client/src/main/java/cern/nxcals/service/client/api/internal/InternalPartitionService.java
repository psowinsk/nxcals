/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.client.api.internal;

import cern.nxcals.common.domain.PartitionData;
import cern.nxcals.service.client.api.PartitionService;

import java.util.Map;

public interface InternalPartitionService extends PartitionService {
    PartitionData findBySystemIdAndKeyValues(long systemId, Map<String, Object> partitionKeyValues);
}
