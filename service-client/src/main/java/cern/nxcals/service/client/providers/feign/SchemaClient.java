/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.client.providers.feign;

import cern.nxcals.common.domain.SchemaData;
import feign.Param;
import feign.RequestLine;

import static cern.nxcals.common.web.Endpoints.SCHEMA_WITH_ID;
import static cern.nxcals.common.web.HttpVerbs.GET;

/**
 * Feing declarative service interface for consuming Schema service.
 */
//Methods are self-descriptive
@SuppressWarnings("squid:UndocumentedApi")
public interface SchemaClient {
    @RequestLine(GET + SCHEMA_WITH_ID)
    SchemaData findById(@Param("schemaId") long schemaId);
}
