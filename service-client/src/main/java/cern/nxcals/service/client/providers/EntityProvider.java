/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.client.providers;

import cern.nxcals.common.domain.EntityData;
import cern.nxcals.common.domain.EntityHistoryData;
import cern.nxcals.common.domain.FindOrCreateEntityRequest;
import cern.nxcals.common.domain.PartitionData;
import cern.nxcals.common.utils.TimeUtils;
import cern.nxcals.service.client.AbstractProvider;
import cern.nxcals.service.client.DataConflictRuntimeException;
import cern.nxcals.service.client.api.internal.InternalEntityService;
import cern.nxcals.service.client.domain.KeyValues;
import cern.nxcals.service.client.providers.feign.EntityClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;
import java.util.function.Supplier;

/**
 * Implementation of the {@link cern.nxcals.service.client.AbstractProvider} for getting {@link EntityDataImpl} objects
 *
 * @author Marcin Sobieszek
 * @author jwozniak
 * @date Jul 21, 2016 4:40:15 PM
 */
class EntityProvider extends AbstractProvider<Long, Map<String, Object>, EntityData, EntityClient>
        implements InternalEntityService {

    private static final BiFunction<Map<String, Object>, PartitionData, Boolean> KEY_VALUE_MATCHER =
            (partitionKeyValues, partitionData) -> partitionData.getKeyValues().equals(partitionKeyValues);
    private static final Logger LOGGER = LoggerFactory.getLogger(EntityProvider.class);
    private final ConcurrentHashMap<String, EntityData> entityCacheByKeyValues = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<Long, EntityData> entityCacheById = new ConcurrentHashMap<>();
    private BiFunction<Long, PartitionData, Boolean> idMatcher = (partitionId, partitionData) -> partitionData.getId()
            == partitionId;

    EntityProvider(EntityClient httpService) {
        super(httpService);
    }

    @Override
    public EntityData findBySystemIdAndKeyValues(long system, Map<String, Object> entityKeyValues) {
        return serviceFindEntityFor(system, entityKeyValues);
    }

    @Override
    protected EntityData createDataForCache(Long systemId, Map<String, Object> entityKeyValues) {
        return getHttpClient().findBySystemIdAndKeyValues(systemId, entityKeyValues);
    }

    @Override
    public EntityData findOrCreateEntityFor(long systemId, Long entityId, Long partitionId, String schema,
            Long recordTimestamp) {
        try {
            return internalFindOrCreateEntityFor(systemId, entityId, partitionId, schema, recordTimestamp);
        } catch (IllegalStateException | IllegalArgumentException exception) {
            throw exception; //this one we don't retry.
        } catch (Exception ex) {
            //FIXME - try to find appropriate exception types for this retry.
            //Not all are fine but only related to record version, and violation of schema & partition keys.
            //History rewrite failure will always fail for the second time so we should not retry.
            LOGGER.warn(
                    "Retrying - another client has already created an entity for system {} entity {}  partition {} schema {}",
                    systemId, entityId, partitionId, schema, ex);
            return internalFindOrCreateEntityFor(systemId, entityId, partitionId, schema, recordTimestamp);
        }

    }

    private EntityData internalFindOrCreateEntityFor(long systemId, Long entityId, Long partitionId, String schema,
            Long timestamp) {
        //this is only finding
        EntityData entityData = getEntityData(entityId, entityCacheById,
                () -> serviceFindEntityFor(entityId, timestamp), false);

        if (entityData == null) {
            throw new IllegalArgumentException(String.format("Entity with id=%1$d not found", entityId));
        }

        if (isRecordDefinitionFoundInCachedHistory(entityData, partitionId, idMatcher, schema, timestamp)) {
            //the check method will throw exception on history rewrite attempt
            return entityData;
        } else {
            //call the service to change the history (add entry for this timestamp) and update the local cache (forcing call to the server)
            return getEntityData(entityId, entityCacheById,
                    () -> serviceFindOrCreateEntityFor(systemId, entityId, partitionId, schema, timestamp), true);
        }
    }

    private EntityData serviceFindOrCreateEntityFor(long systemId, Long entityId, Long partitionId, String schema,
            Long timestamp) {
        LOGGER.debug(
                "Calling a remote service to updateEntityHistory for system={}, entityId={} partitionId={} timestamp={}",
                systemId, entityId, partitionId, TimeUtils.getInstantFromNanos(timestamp));
        return getHttpClient().findOrCreateEntityFor(systemId, entityId, partitionId, timestamp, schema);
    }

    @Override
    public EntityData findOrCreateEntityFor(long systemId, KeyValues entityKeyValues, KeyValues partitionKeyValues,
            String schema, long recordTimestamp) {
        try {
            return internalFindOrCreateEntityFor(systemId, entityKeyValues, partitionKeyValues, schema,
                    recordTimestamp);
        } catch (IllegalStateException exception) {
            throw exception; //this one we don't retry.
        } catch (Exception ex) {
            //FIXME - try to find appropriate exception types for this retry.
            //Not all are fine but only related to record version, and violation of schema & partition keys.
            //History rewrite failure will always fail for the second time so we should not retry.
            LOGGER.warn(
                    "Retrying - another client has already created an entity for system {} entity {}  partition {} schema {}",
                    systemId, entityKeyValues.getKeyValues(), partitionKeyValues, schema, ex);
            return internalFindOrCreateEntityFor(systemId, entityKeyValues, partitionKeyValues, schema,
                    recordTimestamp);
        }

    }

    private EntityData internalFindOrCreateEntityFor(long systemId, KeyValues entityKeyValues,
            KeyValues partitionKeyValues,
            String schema, long recordTimestamp) {
        //We only cache by entityKeyValues as this code is always for the same system.
        //If the cache does not contain the entity this first call always hits the remote service in order to put the entity into the cache.
        //It does not try to create the entity with the given input parameters as the data in this call might be wrong (like history rewrite),
        //that will lead to exception from the service
        //side and finally to a situation where we keep calling the service for always wrong data.
        //This might slow down the ingestion and happened after the restart of the datasources for empty cache and invalid first record (jwozniak)
        //Entity can be not found, so we create it.
        //        try (AutoCloseableLock lock = getLockFor(entityKeyValues)) { // using stripe locking to avoid multiple compute/computeIfAbsent.

        // THIS IS A TRY WITH LESS LOCKING IMPLEMENTATION. BEWARE THAT put ALSO SYNCHRONIZES...

        //this is only finding
        EntityData entityData = getEntityData(entityKeyValues.getId(), entityCacheByKeyValues,
                () -> serviceFindEntityFor(systemId, entityKeyValues.getKeyValues()), false);

        if (entityData == null || !isRecordDefinitionFoundInCachedHistory(entityData, partitionKeyValues.getKeyValues(),
                KEY_VALUE_MATCHER, schema, recordTimestamp)) {
            //This is a new record (null) or data is ok as isRecordDefinitionFoundInCachedHistory will throw an exception for history rewrite.
            //This call is synchronized on the key by the ConcurrentHashMap on the key but it has to go the remote service to add data to history.
            try {
                //this is finding or creating
                return getEntityData(entityKeyValues.getId(), entityCacheByKeyValues,
                        () -> serviceFindOrCreateEntityFor(systemId, entityKeyValues.getKeyValues(),
                                partitionKeyValues.getKeyValues(), schema,
                                recordTimestamp), true);

            } catch (DataConflictRuntimeException exception) {
                //We have a data conflict for this entity. The cache needs to be updated with the recent state of this entity to avoid hitting the service.
                LOGGER.warn("Data conflict for system={}, entity={}, partition={}, schema={}, timestamp={}", systemId,
                        entityKeyValues.getKeyValues(), partitionKeyValues, schema, recordTimestamp);

                //this is only finding
                getEntityData(entityKeyValues.getId(), entityCacheByKeyValues, () -> serviceFindEntityFor(systemId,
                        entityKeyValues.getKeyValues()), true);

                throw new IllegalStateException(MessageFormat
                        .format(("Data conflict detected, schema or partition history rewrite error, for systemId={0," +
                                        "number,#}, entityKey={1}, partitionKey={2},recordTimestamp={3}, schema={4}")
                                ,
                                systemId, entityKeyValues.getKeyValues(), partitionKeyValues,
                                TimeUtils.getInstantFromNanos(recordTimestamp), schema), exception);
            }
        } else {
            return entityData;
        }
    }

    @Override
    public EntityData extendEntityFirstHistoryDataFor(long entityId, String schema, long from) {
        return serviceExtendEntityHistoryDataFor(entityId, schema, from);
    }

    @Override
    public EntityData findBySystemIdKeyValuesAndTimeWindow(long systemId, Map<String, Object> entityKeyValues,
            long startTime,
            long endTime) {
        LOGGER.debug("Calling a remote service to findEntityFor system={}, entityKey={} startTime={} endTime={}",
                systemId, entityKeyValues, TimeUtils.getInstantFromNanos(startTime),
                TimeUtils.getInstantFromNanos(endTime));
        return getHttpClient().findBySystemIdKeyValuesAndTimeWindow(systemId, entityKeyValues, startTime, endTime);
    }

    @Override
    public EntityData findByEntityIdAndTimeWindow(long entityId, long startTime, long endTime) {
        LOGGER.debug("Calling a remote service to findEntityFor entityId={}, startTime={}, endTime={}", entityId,
                startTime, endTime);
        return getHttpClient().findByEntityIdAndTimeWindow(entityId, startTime, endTime);
    }

    @Override
    public List<EntityData> findByKeyValuesLike(String keyValuesExpression) {
        return getHttpClient().findByKeyValuesLike(keyValuesExpression);
    }

    @Override
    public List<EntityData> updateEntities(List<EntityData> entityDataList) {
        return getHttpClient().updateEntities(entityDataList);
    }

    @Override
    public EntityData findById(long entityId) {
        return getHttpClient().findById(entityId);
    }

    @Override
    public List<EntityData> findAllByIdIn(List<Long> entityIds) {
        if (entityIds == null || entityIds.isEmpty()) {
            throw new IllegalArgumentException("Received null or empty collection of entity IDs!");
        }
        return getHttpClient().findAllByIdIn(entityIds);
    }

    /**
     * Uses supplier to fill in the cache.
     *
     * @param key
     * @param supplier
     * @param force    a flag that indicates whether to force or not, the the supplier usage
     * @return
     */
    private <T> EntityData getEntityData(T key, ConcurrentHashMap<T, EntityData> cache,
            Supplier<EntityData> supplier, boolean force) {
        EntityData entityData = null;
        if (force) {
            entityData = supplier.get();
            putIfNotNull(key, cache, entityData);
        } else {
            entityData = cache.get(key);
            if (entityData == null) {
                entityData = supplier.get();
                putIfNotNull(key, cache, entityData);
            }
        }
        return entityData;
    }

    private <T> void putIfNotNull(T key, ConcurrentHashMap<T, EntityData> cache, EntityData entityData) {
        if (entityData != null) {
            cache.put(key, entityData);
        }
    }

    private EntityData serviceFindEntityFor(long systemId, Map<String, Object> entityKeyValues) {
        LOGGER.debug("Calling a remote service to findEntityFor system={}, entityKey={}", systemId, entityKeyValues);
        return getHttpClient().findBySystemIdAndKeyValues(systemId, entityKeyValues);
    }

    private EntityData serviceFindEntityFor(long id, long timestamp) {
        LOGGER.debug("Calling a remote service to findEntityFor entityId={} timestamp={}", id, timestamp);
        return getHttpClient().findByEntityIdAndTimeWindow(id, timestamp, timestamp);
    }

    protected EntityData serviceFindOrCreateEntityFor(long systemId, Map<String, Object> entityKeyValues,
            Map<String, Object> partitionKeyValues, String schema, long recordTimestamp) {
        LOGGER.debug(
                "Calling a remote service to findOrCreateEntityFor system={}, entityKey={}, partitionKey={}, schema={}, timestamp={}",
                systemId, entityKeyValues, partitionKeyValues, schema, recordTimestamp);
        FindOrCreateEntityRequest findOrCreateEntityRequest = FindOrCreateEntityRequest.builder()
                .entityKeyValues(entityKeyValues)
                .partitionKeyValues(partitionKeyValues)
                .schema(schema)
                .build();
        return getHttpClient()
                .findOrCreateEntityFor(systemId, recordTimestamp, findOrCreateEntityRequest);
    }

    private EntityData serviceExtendEntityHistoryDataFor(long entityId, String schema, long from) {
        return getHttpClient().extendEntityFirstHistoryDataFor(entityId, from, schema);
    }

    private <T> Boolean isRecordDefinitionFoundInCachedHistory(EntityData entityData, T partitionIdentifier,
            BiFunction<T, PartitionData, Boolean> partitionMatcher,
            String schema, long recordTimestamp) {
        //made iterative for performance, streams are slower
        Set<EntityHistoryData> histData = entityData.getEntityHistoryData();
        if (histData == null || histData.isEmpty()) {
            return false;
        }

        for (EntityHistoryData hist : histData) {
            if (hist.getValidFromStamp() <= recordTimestamp && (hist.getValidToStamp() == null
                    || hist.getValidToStamp() > recordTimestamp)) {
                //Found the history entry for this recordTimestamp
                if (partitionMatcher.apply(partitionIdentifier, hist.getPartitionData()) && hist.getSchemaData()
                        .getSchemaJson().equals(schema)) {
                    //Schema & partition match
                    return true;
                } else if (hist.getValidToStamp() != null) {
                    //The historical record is closed from both ends and this timestamp is inside it but
                    //the schema or partition do not match - this is wrong, we don't allow to rewrite history with different schema or partition.
                    //This is prevented already on the client side to avoid hitting the server with many calls like that for wrong data
                    LOGGER.warn("History rewrite error for entity {}: schema {} or partition {} do not match in found "
                                    + "history entry {} for recordTime {}", entityData, schema, partitionIdentifier, hist,
                            recordTimestamp);

                    throw new IllegalStateException(MessageFormat
                            .format("Data conflict detected, schema or partition history rewrite error, "
                                            + "for entityId={0,number,#}, entityKey={1}, partition={2} system={3} ,"
                                            + "recordTimestamp={4}, historyId={5,number,#}, validFrom={6}, validTo={7}",
                                    entityData.getId(), entityData.getEntityKeyValues(),
                                    partitionIdentifier,
                                    entityData.getSystemData().getName(),
                                    TimeUtils.getInstantFromNanos(recordTimestamp), hist.getId(),
                                    TimeUtils.getInstantFromNanos(hist.getValidFromStamp()),
                                    TimeUtils.getInstantFromNanos(hist.getValidToStamp())));
                }
            }
        }
        return false;
    }
}
