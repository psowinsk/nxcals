package cern.nxcals.service.client.api.internal;

import cern.nxcals.service.client.api.VariableService;

public interface InternalVariableService extends VariableService {
}
