/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.client.api;

import cern.nxcals.common.domain.EntityData;

import java.util.List;
import java.util.Map;

/**
 * Public access to NXCALS entities.
 */
//FIXME: VERY IMPORTANT, this class is the public entry for the service, MUST BE DOCUMENTED (timartin 27/11/2017)
public interface EntityService {

    EntityData findBySystemIdAndKeyValues(long systemId, Map<String, Object> entityKeyValues);

    EntityData findBySystemIdKeyValuesAndTimeWindow(long systemId, Map<String, Object> entityKeyValues, long startTime,
            long endTime);

    List<EntityData> findByKeyValuesLike(String keyValuesExpression);
}
