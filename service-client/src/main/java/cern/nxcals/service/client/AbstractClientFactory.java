/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.client;

import cern.nxcals.common.exceptions.NotFoundRuntimeException;
import cern.nxcals.common.utils.ConfigHolder;
import cern.nxcals.service.client.security.KerberosAwareClient;
import cern.nxcals.service.client.security.PropertiesKeys;
import com.google.common.io.CharStreams;
import com.typesafe.config.Config;
import feign.Feign;
import feign.Logger;
import feign.Response;
import feign.codec.ErrorDecoder;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.ribbon.RibbonClient;
import feign.slf4j.Slf4jLogger;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.stream.Collectors;

import static cern.nxcals.common.utils.ConfigHolder.getConfig;
import static cern.nxcals.common.utils.ConfigHolder.getConfigIfPresent;

/**
 * Abstract base class for Feign clients. Knows how to configure the whole chain of serialization and security.
 */
@Slf4j
public class AbstractClientFactory {
    //FIXME: No need to store the servive url or passing around the SERVICE_URL_CONFIG as now things are based on system property.
    private final String serviceUrl;
    private static final String SERVICE_URL_CONFIG = "service.url";

    protected AbstractClientFactory() {
        serviceUrl = createServiceURL(SERVICE_URL_CONFIG);
    }

    protected <T> T createServiceFor(Class<T> clazz) {
        return Feign.builder()
                .encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder())
                .client(RibbonClient.builder().delegate(createKerberosClientDelegate()).build())
                .logger(new Slf4jLogger())
                .logLevel(Logger.Level.BASIC)
                .errorDecoder(new ServiceClientErrorDecoder())
                .decode404()
                .target(clazz, serviceUrl);
    }

    private KerberosAwareClient createKerberosClientDelegate() {
        return KerberosAwareClient.builder()
                .setKeytabLocation(ConfigHolder
                        .getProperty(PropertiesKeys.USER_KEYTAB_PATH_CONFIG.getPathInProperties(), null))
                .setUserPrincipal(ConfigHolder
                        .getProperty(PropertiesKeys.USER_PRINCIPAL_PATH.getPathInProperties(), null))
                .setLoginOptions(getKerberosLoginOptions())
                .setServiceType("HTTPS")
                .build();
    }

    private static class ServiceClientErrorDecoder implements ErrorDecoder {
        @Override
        public Exception decode(String methodKey, Response response) {
            String body;
            try (InputStreamReader reader = new InputStreamReader(response.body().asInputStream(),
                    StandardCharsets.UTF_8.name())) {
                body = CharStreams.toString(reader);
            } catch (IOException exception) {
                log.warn("Cannot read response body for {}", methodKey, exception);
                body = "Cannot read response body for " + methodKey;
            }
            if (response.status() == HttpStatus.SC_CONFLICT) {
                log.warn("Conflict for method={}. Reason={}, Body={} status code={}",
                        methodKey, response.reason(), body, response.status());
                return new DataConflictRuntimeException(String
                        .format("Conflict detected for method=%s, reason=%s, status=%s", methodKey,
                                body, response.status()));
            } else if (response.status() == HttpStatus.SC_NOT_FOUND && body.startsWith("404 Resource Not Found.")) {
                return new NotFoundRuntimeException(body);
            } else {
                log.error("Data cannot be retrieved for method={}. Reason={}, Body={}, status code={}", methodKey,
                        response.reason(), body, response.status());
                return new RuntimeException(String.format(
                        "Unsuccessful call to a remote service for method=%s, reason=%s, body=%s, status = %s",
                        methodKey, response.reason(), body, response.status()));
            }
        }
    }

    private String createServiceURL(String variableName) {
        Config config = getConfig();
        if (!config.hasPath(variableName)) {
            throw new IllegalStateException("Cannot find schema service url for " + variableName);
        }
        String url = config.getString(variableName);
        System.setProperty("nxcals-service.ribbon.listOfServers", url);
        return "https://nxcals-service";
    }

    private static Map<String, Object> getKerberosLoginOptions() {
        return getConfigIfPresent("kerberos.login")
                .entrySet()
                .stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        entry -> entry.getValue().unwrapped()));
    }
}