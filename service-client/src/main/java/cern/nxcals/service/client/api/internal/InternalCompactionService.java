package cern.nxcals.service.client.api.internal;

public interface InternalCompactionService {
    boolean shouldCompact(String path);
}
