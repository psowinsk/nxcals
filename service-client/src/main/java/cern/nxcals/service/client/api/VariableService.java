/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.client.api;

import cern.nxcals.common.domain.VariableData;

import java.util.List;

/**
 * Public access to NXCALS variables.
 * Created by ntsvetko on 1/16/17.
 */
//Methods are self-descriptive
@SuppressWarnings("squid:UndocumentedApi")
public interface VariableService {

    VariableData findByVariableName(String variableName);

    VariableData findByVariableNameAndTimeWindow(String variableName, long startTime, long endTime);

    List<VariableData> findByNameLike(String nameExpression);

    List<VariableData> findByDescriptionLike(String descriptionExpression);

    VariableData registerOrUpdateVariableFor(VariableData variableData);
}
