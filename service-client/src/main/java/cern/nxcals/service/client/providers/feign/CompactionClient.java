/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.client.providers.feign;

import feign.Body;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

import static cern.nxcals.common.web.Endpoints.COMPACTION_SHOULD_COMPACT;
import static cern.nxcals.common.web.HttpVerbs.POST;

/**
 * Feing declarative service interface for consuming Compaction service..
 */
//Methods are self-descriptive
@SuppressWarnings("squid:UndocumentedApi")
public interface CompactionClient {

    @Headers({ "Content-Type: application/json" })
    @RequestLine(POST + COMPACTION_SHOULD_COMPACT)
    @Body("{path}")
    boolean shouldCompact(@Param("path") String path);
}
