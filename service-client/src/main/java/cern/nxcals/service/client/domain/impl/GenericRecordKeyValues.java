/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.client.domain.impl;

import cern.nxcals.service.client.domain.KeyValues;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericRecord;

import java.util.HashMap;
import java.util.Map;

/**
 * Implementation of {@link KeyValues} based on the Avro API.
 */
public class GenericRecordKeyValues extends AbstractKeyValues {

    private final String id;
    private final Map<String, Object> keyValues;

    /**
     * Creates an id and {@link Map} of keyValues from a {@link GenericRecord}.
     *
     * @param genericRecord the source of the key values.
     */
    public GenericRecordKeyValues(GenericRecord genericRecord) {
        Map<String, Object> keyValues = new HashMap<>();
        Schema schema = genericRecord.getSchema();
        StringBuilder idStringBuilder = new StringBuilder();

        for (Schema.Field field : schema.getFields()) {
            String fieldName = field.name();
            Object fieldValue = genericRecord.get(fieldName);
            keyValues.put(fieldName, fieldValue);
            idStringBuilder.append(field).append(fieldValue);
        }

        id = idStringBuilder.toString();
        this.keyValues = keyValues;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Map<String, Object> getKeyValues() {
        return new HashMap<>(keyValues);
    }
}
