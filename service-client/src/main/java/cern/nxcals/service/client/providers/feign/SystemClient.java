/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.client.providers.feign;

import cern.nxcals.common.domain.SystemData;
import feign.Param;
import feign.RequestLine;

import static cern.nxcals.common.web.Endpoints.SYSTEMS;
import static cern.nxcals.common.web.Endpoints.SYSTEM_WITH_ID;
import static cern.nxcals.common.web.HttpVerbs.GET;

/**
 * Feing declarative service interface for consuming System service..
 */
//Methods are self-descriptive
@SuppressWarnings("squid:UndocumentedApi")
public interface SystemClient {
    @RequestLine(GET + SYSTEM_WITH_ID)
    SystemData findById(@Param("id") long systemId);

    @RequestLine(GET + SYSTEMS + "?name={name}")
    SystemData findByName(@Param("name") String name);
}
