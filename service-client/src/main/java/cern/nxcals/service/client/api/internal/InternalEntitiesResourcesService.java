package cern.nxcals.service.client.api.internal;

import cern.nxcals.service.client.api.EntitiesResourcesService;

public interface InternalEntitiesResourcesService extends EntitiesResourcesService {
}
