/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.client.providers;

import cern.nxcals.service.client.api.internal.InternalCompactionService;
import cern.nxcals.service.client.api.internal.InternalEntitiesResourcesService;
import cern.nxcals.service.client.api.internal.InternalEntityService;
import cern.nxcals.service.client.api.internal.InternalPartitionService;
import cern.nxcals.service.client.api.internal.InternalSchemaService;
import cern.nxcals.service.client.api.internal.InternalSystemService;
import cern.nxcals.service.client.api.internal.InternalVariableService;

/**
 * WARNING: THIS IS NOT A PUBLIC API! INTERNAL USE ONLY, SUBJECT TO CHANGE AT ANY MOMENT, Please use ServiceClientFactory for public API.
 *
 * @author Marcin Sobieszek
 * @author jwozniak
 * @date Jul 21, 2016 4:38:07 PM
 */
//Methods are self-descriptive
@SuppressWarnings("squid:UndocumentedApi")
public final class InternalServiceClientFactory {
    private static final InternalPartitionService partitionService =
            new PartitionProvider(ClientFactory.createPartitionService());
    private static final InternalSystemService systemService =
            new SystemProvider(ClientFactory.createSystemService());
    private static final InternalSchemaService schemaService =
            new SchemaProvider(ClientFactory.createSchemaSerivce());
    private static final InternalEntityService entityService =
            new EntityProvider(ClientFactory.createEntityService());
    private static final InternalEntitiesResourcesService resourceService =
            new EntitiesResourcesProvider(ClientFactory.createResourceService());
    private static final InternalVariableService variableService =
            new VariableProvider(ClientFactory.createVariableService());
    private static final InternalCompactionService compactionService =
            new CompactionProvider(ClientFactory.createCompactionService());

    public static InternalSchemaService createSchemaService() {
        return schemaService;
    }

    public static InternalEntityService createEntityService() {
        return entityService;
    }

    public static InternalPartitionService createPartitionService() {
        return partitionService;
    }

    public static InternalSystemService createSystemService() {
        return systemService;
    }

    public static InternalEntitiesResourcesService createEntityResourceService() {
        return resourceService;
    }

    public static InternalVariableService createVariableService() {
        return variableService;
    }

    public static InternalCompactionService createCompactionService() {
        return compactionService;
    }

    private InternalServiceClientFactory() {
    }
}
