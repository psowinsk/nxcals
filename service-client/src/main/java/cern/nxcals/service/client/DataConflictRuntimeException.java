package cern.nxcals.service.client;

/**
 * This exception is thrown when there is a data conflict detected, like a history rewrite.
 * Created by jwozniak on 01/02/17.
 */
public class DataConflictRuntimeException extends RuntimeException {

    @SuppressWarnings("squid:UndocumentedApi")
    public DataConflictRuntimeException() {
    }

    @SuppressWarnings("squid:UndocumentedApi")
    public DataConflictRuntimeException(String message) {
        super(message);
    }

}
