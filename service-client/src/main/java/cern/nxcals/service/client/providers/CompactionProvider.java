package cern.nxcals.service.client.providers;

import cern.nxcals.service.client.api.internal.InternalCompactionService;
import cern.nxcals.service.client.providers.feign.CompactionClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author ntsvetko
 */
@Slf4j
@RequiredArgsConstructor
class CompactionProvider implements InternalCompactionService {

    private final CompactionClient compactionClient;

    @Override
    public boolean shouldCompact(String path) {
        return this.compactionClient.shouldCompact(path);
    }
}