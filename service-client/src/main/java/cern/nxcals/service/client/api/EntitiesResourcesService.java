/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.client.api;

import cern.nxcals.common.domain.EntityResources;

import java.util.Map;
import java.util.Set;

/**
 * Created by ntsvetko on 4/25/17.
 */
//Methods are self-descriptive
@SuppressWarnings("squid:UndocumentedApi")
public interface EntitiesResourcesService {
    Set<EntityResources> findBySystemIdKeyValuesAndTimeWindow(long systemId, Map<String, Object> entityKeyValues,
            long startTime, long endTime);

    Set<EntityResources> findByEntityIdAndTimeWindow(long entityId, long startTime, long endTime);
}


