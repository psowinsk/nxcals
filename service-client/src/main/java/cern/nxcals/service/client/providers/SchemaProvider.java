/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.client.providers;

import cern.nxcals.common.domain.SchemaData;
import cern.nxcals.service.client.AbstractProvider;
import cern.nxcals.service.client.api.internal.InternalSchemaService;
import cern.nxcals.service.client.providers.feign.SchemaClient;

class SchemaProvider extends AbstractProvider<Long, String, SchemaData, SchemaClient> implements InternalSchemaService {

    @Override
    protected SchemaData createDataForCache(Long schemaId, String key) {
        return this.getHttpClient().findById(schemaId);
    }

    SchemaProvider(SchemaClient httpClient) {
        super(httpClient);
    }

    @Override
    public SchemaData findById(long schemaId) {
        return this.getDataFromCache(schemaId, String.valueOf(schemaId));
    }
}
