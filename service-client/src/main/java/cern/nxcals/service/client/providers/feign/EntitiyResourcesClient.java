/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.client.providers.feign;

import cern.nxcals.common.domain.EntityResources;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

import java.util.Map;
import java.util.Set;

import static cern.nxcals.common.web.Endpoints.RESOURCES;
import static cern.nxcals.common.web.HttpVerbs.GET;
import static cern.nxcals.common.web.HttpVerbs.POST;

/**
 * Feing declarative service interface for consuming Entity Resources service.
 */
//Methods are self-descriptive
@SuppressWarnings("squid:UndocumentedApi")
public interface EntitiyResourcesClient {
    @Headers("Content-Type: application/json")
    @RequestLine(POST + RESOURCES
            + "?systemId={systemId}"
            + "&startTime={startTime}"
            + "&endTime={endTime}")
    Set<EntityResources> findBySystemIdKeyValuesAndTimeWindow(
            @Param("systemId") long systemId,
            Map<String, Object> entityKeyValues,
            @Param("startTime") long startTime,
            @Param("endTime") long endTime);

    @RequestLine(GET + RESOURCES
            + "?entityId={entityId}"
            + "&startTime={startTime}"
            + "&endTime={endTime}")
    Set<EntityResources> findByEntityIdAndTimeWindow(
            @Param("entityId") long entityId,
            @Param("startTime") long startTime,
            @Param("endTime") long endTime);
}
