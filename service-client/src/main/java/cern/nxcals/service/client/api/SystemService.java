/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.client.api;

import cern.nxcals.common.domain.SystemData;

/**
 * SystemData provider Service interface.
 *
 * @author jwozniak
 */
//Methods are self-descriptive
@SuppressWarnings("squid:UndocumentedApi")
public interface SystemService {
    SystemData findByName(String name);
}
