package cern.nxcals.service.client.api.internal;

import cern.nxcals.common.domain.SchemaData;
import cern.nxcals.service.client.api.SchemaService;

public interface InternalSchemaService extends SchemaService {
    SchemaData findById(long schemaId);
}
