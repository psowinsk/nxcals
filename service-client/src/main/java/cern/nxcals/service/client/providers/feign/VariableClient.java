/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.client.providers.feign;

import cern.nxcals.common.domain.VariableData;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

import java.util.List;

import static cern.nxcals.common.web.Endpoints.VARIABLES;
import static cern.nxcals.common.web.Endpoints.VARIABLE_REGISTER_OR_UPDATE;
import static cern.nxcals.common.web.HttpHeaders.ACCEPT_APPLICATION_JSON;
import static cern.nxcals.common.web.HttpHeaders.CONTENT_TYPE_APPLICATION_JSON;
import static cern.nxcals.common.web.HttpVerbs.GET;
import static cern.nxcals.common.web.HttpVerbs.PUT;

/**
 * Feing declarative service interface for consuming Variable service.
 */
//Methods are self-descriptive
@SuppressWarnings("squid:UndocumentedApi")
public interface VariableClient {

    @RequestLine(GET + VARIABLES
            + "?name={name}")
    VariableData findByVariableName(@Param("name") String name);

    @RequestLine(GET + VARIABLES
            + "?nameExpression={nameExpression}")
    List<VariableData> findByNameLike(@Param("nameExpression") String nameExpression);

    @RequestLine(GET + VARIABLES
            + "?descriptionExpression={descriptionExpression}")
    List<VariableData> findByDescriptionLike(
            @Param("descriptionExpression") String descriptionExpression);

    @RequestLine(GET + VARIABLES
            + "?name={name}"
            + "&startTime={startTime}"
            + "&endTime={endTime}")
    VariableData findByVariableNameAndTimeWindow(
            @Param("name") String variableName,
            @Param("startTime") long startTime,
            @Param("endTime") long endTime);

    // fixme should be private (may be public in future)
    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(PUT + VARIABLE_REGISTER_OR_UPDATE)
    VariableData registerOrUpdateVariableFor(VariableData variableData);
}
