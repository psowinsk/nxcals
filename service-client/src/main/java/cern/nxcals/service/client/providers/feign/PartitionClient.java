/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.client.providers.feign;

import cern.nxcals.common.domain.PartitionData;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

import java.util.Map;

import static cern.nxcals.common.web.Endpoints.PARTITIONS;
import static cern.nxcals.common.web.HttpHeaders.CONTENT_TYPE_APPLICATION_JSON;
import static cern.nxcals.common.web.HttpVerbs.POST;

/**
 * Feing declarative service interface for consuming Partition service.
 */
//Methods are self-descriptive
@SuppressWarnings("squid:UndocumentedApi")
public interface PartitionClient {
    @Headers(CONTENT_TYPE_APPLICATION_JSON)
    @RequestLine(POST + PARTITIONS
            + "?systemId={systemId}")
    PartitionData findBySystemIdAndKeyValues(@Param("systemId") long systemId, Map<String, Object> partitionKeyValues);
}
