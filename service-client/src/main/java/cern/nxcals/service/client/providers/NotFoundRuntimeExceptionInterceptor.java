package cern.nxcals.service.client.providers;

import cern.nxcals.common.exceptions.NotFoundRuntimeException;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Interceptor that handles {@link NotFoundRuntimeException} by returning null value.
 *
 * @param <T> Object that perform actual invocation.
 */
public class NotFoundRuntimeExceptionInterceptor<T> implements InvocationHandler {
    private final T delegate;

    NotFoundRuntimeExceptionInterceptor(T delegate) {
        this.delegate = delegate;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        try {
            return method.invoke(delegate, args);
        } catch (InvocationTargetException exception) {
            if (exception.getCause() instanceof NotFoundRuntimeException) {
                return null;
            }
            throw exception.getCause();
        }
    }
}
