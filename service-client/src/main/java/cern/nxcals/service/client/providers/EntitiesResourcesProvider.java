/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.client.providers;

import cern.nxcals.common.domain.EntityResources;
import cern.nxcals.service.client.api.internal.InternalEntitiesResourcesService;
import cern.nxcals.service.client.providers.feign.EntitiyResourcesClient;
import lombok.RequiredArgsConstructor;

import java.util.Map;
import java.util.Set;

@RequiredArgsConstructor
class EntitiesResourcesProvider implements InternalEntitiesResourcesService {
    private final EntitiyResourcesClient httpService;

    @Override
    public Set<EntityResources> findBySystemIdKeyValuesAndTimeWindow(long systemId,
            Map<String, Object> entityKeyValues, long startTime, long endTime) {
        return httpService.findBySystemIdKeyValuesAndTimeWindow(systemId, entityKeyValues, startTime, endTime);
    }

    @Override
    public Set<EntityResources> findByEntityIdAndTimeWindow(long entityId, long startTime, long endTime) {
        return httpService.findByEntityIdAndTimeWindow(entityId, startTime, endTime);
    }
}
