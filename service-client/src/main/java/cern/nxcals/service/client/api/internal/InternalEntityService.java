/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.client.api.internal;

import cern.nxcals.common.domain.EntityData;
import cern.nxcals.service.client.api.EntityService;
import cern.nxcals.service.client.domain.KeyValues;

import java.util.List;

public interface InternalEntityService extends EntityService {
    /**
     * Finds or creates an entity for given ids. In this case only the history is sometimes created (depending on the passed timestamp).
     *
     * @param systemId
     * @param entityId
     * @param partitionId
     * @param schema
     * @param timestamp
     * @return
     */
    EntityData findOrCreateEntityFor(long systemId, Long entityId, Long partitionId, String schema, Long timestamp);

    EntityData findOrCreateEntityFor(long systemId, KeyValues entityCachedRequest,
            KeyValues partitionKeyValues, String recordFieldDefinitions, long recordTimestamp);

    /**
     * Extends entity's history for a given schema in a given time window
     *
     * @param entityId entity's identifier
     * @param schema   entity's schema to be used in the given time window
     * @param from     start of the time window
     * @return modified {@link EntityData}
     */
    EntityData extendEntityFirstHistoryDataFor(long entityId, String schema, long from);

    EntityData findByEntityIdAndTimeWindow(long entityId, long startTime, long endTime);

    /**
     * Updates a list of entities.
     *
     * @param entityDataList a {@link List} of {@link EntityData}s that identify the entities to be updated.
     *                       We only allow change of the keyValues and lock/unlock for operation.
     * @return a {@link List} of updated entities.
     */
    List<EntityData> updateEntities(List<EntityData> entityDataList);

    /**
     * Fetches an entity based on the provided entity identification number.
     * The fetched entity's history data is adjusted to contain <b>only</b> the latest registered entry.
     *
     * @param entityId a number that corresponds to the target entity's id
     * @return the requested {@link EntityData} instance, queried by the provided id
     */
    EntityData findById(long entityId);

    /**
     * Fetches a {@link List<EntityData>} that contains all found (if any) {@link EntityData} instances, based on
     * the provided collection of entity identification numbers.
     * Each fetched entity's history data is adjusted to contain <b>only</b> the latest registered entry.
     *
     * @param entityIds a {@link List<Long>} of ids that correspond to the target entities
     * @return a {@link List<EntityData>} that contains all target entities queried by the provided ids,
     * or an empty list, if nothing was found.
     */
    List<EntityData> findAllByIdIn(List<Long> entityIds);

}