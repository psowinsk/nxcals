package cern.nxcals.common.utils;

import com.typesafe.config.Config;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/* *
 * @author jwozniak
 * @date Aug 04, 2016 8:07 AM
 */
public class ConfigHolderTest {
    @Test
    public void referenceConfigShouldBeLoadedCorrectly() {
        Config config = ConfigHolder.getConfig();
        assertEquals(100, config.getInt("test.test2.value"));
    }

    @Test
    public void shouldGetConfigIfPresent() {
        assertEquals(1,ConfigHolder.getConfigIfPresent("test").entrySet().size());
        assertEquals(0,ConfigHolder.getConfigIfPresent("test111").entrySet().size());
    }

    @Test
    public void shouldGetValue() {
        assertEquals(100,(int)ConfigHolder.getProperty("test.test2.value",200));
    }

    @Test
    public void shouldGetEmptyValue() {
        assertEquals(200,(int)ConfigHolder.getProperty("test.empty",200));
    }


}