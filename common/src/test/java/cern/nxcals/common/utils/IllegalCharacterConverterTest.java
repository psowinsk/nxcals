package cern.nxcals.common.utils;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.apache.avro.SchemaBuilder;
import org.apache.avro.SchemaParseException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static java.lang.String.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(JUnitParamsRunner.class)
public class IllegalCharacterConverterTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    Map<String, String> namesConvertedToLegalCache = new ConcurrentHashMap<>();
    Map<String, String> namesConvertedFromLegalCache = new ConcurrentHashMap<>();

    private static final String LEGAL_TEST_BASE_NAME = "test";
    private IllegalCharacterConverter illegalCharacterConverter;

    @Before
    public void setup() {
        illegalCharacterConverter = new IllegalCharacterConverter(namesConvertedToLegalCache,
                namesConvertedFromLegalCache);
    }

    @Test
    @Parameters(method = "fieldNames")
    public void testEncodingDecodingFieldNames(String nameToEncode) {
        String encodedName = illegalCharacterConverter.convertToLegal(nameToEncode);
        assertTrue(encodedName.startsWith(IllegalCharacterConverter.PREFIX));
        assertAvroParsingException(encodedName, false);
        String originalName = illegalCharacterConverter.convertFromLegal(encodedName);
        assertEquals(nameToEncode, originalName);
    }

    @Test
    public void testEncodingNameWithPrefix() {
        final String nameToEncode = IllegalCharacterConverter.PREFIX + LEGAL_TEST_BASE_NAME;
        expectedException.expect(IllegalCharacterConverter.FieldNameException.class);
        expectedException.expectMessage(format(IllegalCharacterConverter.PREFIX_EXISTS_ERROR, nameToEncode));
        illegalCharacterConverter.convertToLegal(nameToEncode);
    }

    @Test
    @Parameters(method = "nonDecodableFieldNames")
    public void testDecodeIllegalValues(String toDecode) {
        expectedException.expect(IllegalCharacterConverter.FieldNameException.class);
        expectedException.expectMessage(format(IllegalCharacterConverter.CANNOT_DECODE_FIELD_NAME_ERROR, toDecode));
        illegalCharacterConverter.convertFromLegal(toDecode);
    }

    @Test
    public void testCachedToLegalValue() {
        assertFalse(namesConvertedToLegalCache.containsKey(LEGAL_TEST_BASE_NAME));
        String convertedValue = illegalCharacterConverter.convertToLegal(LEGAL_TEST_BASE_NAME);
        assertEquals(convertedValue, namesConvertedToLegalCache.get(LEGAL_TEST_BASE_NAME));
        String cachedResult = "cachedResult";
        namesConvertedToLegalCache.put(LEGAL_TEST_BASE_NAME, cachedResult);
        assertEquals(cachedResult, illegalCharacterConverter.convertToLegal(LEGAL_TEST_BASE_NAME));
    }

    @Test
    public void testCachedFromLegalValue() {
        String encodedName = illegalCharacterConverter.convertToLegal(LEGAL_TEST_BASE_NAME);
        assertFalse(namesConvertedFromLegalCache.containsKey(encodedName));
        String convertedValue = illegalCharacterConverter.convertFromLegal(encodedName);
        assertNotNull(convertedValue);
        String cachedResult = "cachedResult";
        namesConvertedFromLegalCache.put(encodedName, cachedResult);
        assertEquals(cachedResult, illegalCharacterConverter.convertFromLegal(encodedName));
    }

    @Test
    public void testGetLegalIfCachedWithCachedValue() {
        String cachedValue = "cachedValue";
        namesConvertedToLegalCache.put(LEGAL_TEST_BASE_NAME, cachedValue);
        assertEquals(cachedValue, illegalCharacterConverter.getLegalIfCached(LEGAL_TEST_BASE_NAME));
    }

    @Test
    public void testGetLegalIfCachedWithNoCachedValue() {
        assertEquals(LEGAL_TEST_BASE_NAME, illegalCharacterConverter.getLegalIfCached(LEGAL_TEST_BASE_NAME));
    }

    @Test
    public void testSingleton() {
        IllegalCharacterConverter singleton = IllegalCharacterConverter.get();
        assertNotNull(singleton);
        assertEquals(singleton, IllegalCharacterConverter.get());
    }

    private void assertAvroParsingException(String name, boolean wasThrown) {
        boolean parsingExceptionThrown = false;
        try {
            SchemaBuilder.record("test").fields().name(name).type().stringType().noDefault().endRecord();
        } catch (SchemaParseException exception) {
            if (exception.getMessage().startsWith("Illegal character in:") ||
                    exception.getMessage().startsWith("Illegal initial character:")) {
                parsingExceptionThrown = true;
            }
        }

        assertEquals(wasThrown, parsingExceptionThrown);
    }

    public Object[][] fieldNames() {
        return new Object[][] {
                new Object[] { ":" + LEGAL_TEST_BASE_NAME },
                new Object[] { LEGAL_TEST_BASE_NAME + ":" },
                new Object[] { LEGAL_TEST_BASE_NAME + ":" + LEGAL_TEST_BASE_NAME },
                new Object[] { ":" + LEGAL_TEST_BASE_NAME + ":" },
                new Object[] { ":" },
                new Object[] { "::" },
                new Object[] { "LegalName" },
                new Object[] { "12345" },    // No padding
                new Object[] { "123456" },   // 6 padding
                new Object[] { "1234567" },  // 4 padding
                new Object[] { "12345678" }, // 3 padding
                new Object[] { "123456789" } // 1 padding
        };
    }

    public Object[][] nonDecodableFieldNames() {
        return new Object[][] {
                new Object[] { "Test" },
                new Object[] { "__NX_Test" },
                new Object[] { "__NX_5" }
        };
    }
}
