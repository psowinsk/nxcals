package cern.nxcals.common.utils;

import org.junit.Assert;
import org.junit.Test;

public class HdfsFileUtilsTest {

    @Test
    public void shouldCreateNestedDirectoriesOutOfProvidedDate() {
        String dateToNestedPaths = HdfsFileUtils.expandDateToNestedPaths("2018-04-10");

        Assert.assertEquals("2018/4/10", dateToNestedPaths);
    }

}
