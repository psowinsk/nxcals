package cern.nxcals.common.utils;

import org.junit.Assert;
import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by ntsvetko on 7/28/17.
 */
public class HdfsPathDecoderTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailForInvalidDateInThePath() {
        //given
        Path path = Paths.get("/system_dir1/system_dir2/1/2/3/2017");

        //when
        HdfsPathDecoder.verify(path);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailForInvalidNumberOfIdsInThePath() {
        //given
        Path path = Paths.get("/system_dir1/system_dir2/1/2017-01-01");

        //when
        HdfsPathDecoder.verify(path);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailForInvalidConversionToIDInThePath() {
        //given
        Path path = Paths.get("/system_dir1/system_dir2/1a/2/3/2017-01-01");

        //when
        HdfsPathDecoder.verify(path);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailForTooShortPath() {
        //given
        Path path = Paths.get("/2/3/2017-01-01");

        //when
        HdfsPathDecoder.verify(path);
    }

    @Test
    public void shouldNotFailForCorrectPath() {
        //given
        Path path = Paths.get("/system_dir1/system_dir2/1/2/3/2017-01-01");

        //when
        HdfsPathDecoder.verify(path);
    }

    @Test
    public void shouldFindSystemIdFromPath() {
        //given
        Path path = Paths.get("/system_dir1/system_dir2/1/2/3/2017-01-01");

        //when
        long systemId = HdfsPathDecoder.findSystemIdFrom(path);

        //then
        Assert.assertNotNull(systemId);
        Assert.assertEquals(1L, systemId);
    }

    @Test
    public void shouldFindPartitionIdFromPath() {
        //given
        Path path = Paths.get("/system_dir1/system_dir2/1/2/3/2017-01-01");

        //when
        long partitionId = HdfsPathDecoder.findPartitionIdFrom(path);

        //then
        Assert.assertNotNull(partitionId);
        Assert.assertEquals(2L, partitionId);
    }

    @Test
    public void shouldFindSchemaIdFromPath() {
        //given
        Path path = Paths.get("/system_dir1/system_dir2/1/2/3/2017-01-01");

        //when
        long schemaId = HdfsPathDecoder.findSchemaIdFrom(path);

        //then
        Assert.assertNotNull(schemaId);
        Assert.assertEquals(3L, schemaId);
    }

    @Test
    public void shouldFindDateFromPath() {
        //given
        Path path = Paths.get("/system_dir1/system_dir2/1/2/3/2017-01-28");

        //when
        String date = HdfsPathDecoder.findDateFrom(path);

        //then
        Assert.assertNotNull(date);
        Assert.assertEquals("2017-01-28", date);
    }
}
