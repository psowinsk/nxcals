/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.common;

import cern.nxcals.common.utils.AvroUtils;
import org.apache.avro.Schema;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

import static org.apache.avro.Schema.Type.STRING;
import static org.apache.avro.Schema.create;
import static org.apache.avro.SchemaBuilder.record;

/**
 * @author Marcin Sobieszek
 * @date Jul 13, 2016 4:14:46 PM
 */
public class AvroUtilsTest {

    @Test
    public void shouldReturnFirstSchemaWhenSecondIsNull() {
        Schema schema = record("TEST").namespace("ns").fields().name("class").type(create(STRING)).noDefault()
                .name("property").type(create(STRING)).noDefault().endRecord();
        Schema merged = AvroUtils.mergeSchemas(schema, null);
        Assert.assertEquals(schema, merged);
    }

    @Test
    public void shouldReturnSecondSchemaWhenFirstIsNull() {
        Schema schema = record("TEST").namespace("ns").fields().name("class").type(create(STRING)).noDefault()
                .name("property").type(create(STRING)).noDefault().endRecord();
        Schema merged = AvroUtils.mergeSchemas(null, schema);
        Assert.assertEquals(schema, merged);
    }

    @Test
    public void shouldMergeSchemas() {
        Schema oldSchema = record("TEST").namespace("ns").fields().name("class").type(create(STRING)).noDefault()
                .name("property").type(create(STRING)).noDefault().endRecord();
        Schema newSchema = record("TEST").fields().name("classVersion").type(create(STRING)).noDefault().endRecord();

        Schema merged = AvroUtils.mergeSchemas(oldSchema, newSchema);
        for (String name : Arrays.asList("class", "property", "classVersion")) {
            Assert.assertNotNull(merged.getField(name));
        }
    }

}
