/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringJoiner;
import java.util.function.Predicate;

import static java.lang.String.format;

@Slf4j
public class HdfsFileUtils {

    public static final DateTimeFormatter STAGE_DIRECTORY_DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd")
            .withZone(ZoneId.of("UTC"));

    public enum SearchType {
        RECURSIVE(true), NONRECURSIVE(false);

        private boolean value;

        SearchType(boolean value) {
            this.value = value;
        }

        public boolean getValue() {
            return this.value;
        }
    }

    /**
     * Provides status information about all files located under an HDFS directory path.
     *
     * @param fileSystem          the Hadoop filesystem
     * @param path                the {@link Path} instance that represents the directory path
     * @param type                a {@link SearchType} instance that dictates the type (recursive or not) of the search action
     * @param fileStatusPredicate a {@link Predicate} to tests against the fetched files
     * @return a {@link List} of all available {@link FileStatus} instances, located under the given path,
     * that their status, match the provided search predicate
     */
    public static List<LocatedFileStatus> findFilesInPath(FileSystem fileSystem, Path path, SearchType type,
            Predicate<LocatedFileStatus> fileStatusPredicate) {
        log.info("Searching files for path {}", path);
        List<LocatedFileStatus> fileStatuses = new ArrayList<>();
        try {
            if (!fileSystem.exists(path)) {
                log.info("No such path {}", path);
                return Collections.emptyList();
            }

            RemoteIterator<LocatedFileStatus> iterator = fileSystem.listFiles(path, type.getValue());
            while (iterator.hasNext()) {
                LocatedFileStatus stat = iterator.next();
                if (fileStatusPredicate.test(stat)) {
                    fileStatuses.add(stat);
                }
            }
            log.debug("Found status information for {} filtered files under HDFS directory: {}",
                    fileStatuses.size(), path);
            return fileStatuses;
        } catch (IOException ex) {
            throw new UncheckedIOException(format("Cannot list files in base path: %s", path), ex);
        }
    }

    /**
     * Finds files in the base path directory that match the predicate.
     */
    public static List<LocatedFileStatus> findDirsAndFilesInPath(FileSystem fileSystem, Path path, SearchType type,
            Predicate<LocatedFileStatus> predicate) {
        log.info("Searching dirs for path: {}", path);
        List<LocatedFileStatus> output = new ArrayList<>();
        try {
            if (fileSystem.exists(path)) {
                RemoteIterator<LocatedFileStatus> iterator = fileSystem.listLocatedStatus(path);
                while (iterator.hasNext()) {
                    LocatedFileStatus stat = iterator.next();
                    if (predicate.test(stat)) {
                        output.add(stat);
                    }

                    if (stat.isDirectory() && SearchType.RECURSIVE.equals(type)) {
                        output.addAll(findDirsAndFilesInPath(fileSystem, stat.getPath(), type, predicate));
                    }
                }
                return output;
            } else {
                log.info("No such path {}", path);
                return Collections.emptyList();
            }
        } catch (IOException ex) {
            throw new UncheckedIOException(format("Cannot list files in base path: %s", path), ex);
        }
    }

    /**
     * Finds files in the base path directory that match the predicate.
     */
    public static List<LocatedFileStatus> findFilesInPath(FileSystem fileSystem, Path path, SearchType type) {
        return findFilesInPath(fileSystem, path, type, file -> true);
    }

    /**
     * @param stat
     * @return
     */
    public static boolean isDataFile(LocatedFileStatus stat) {
        return isAvroDataFile(stat) || isParquetDataFile(stat);
    }

    public static boolean isAvroDataFile(LocatedFileStatus stat) {
        return stat.isFile() && stat.getPath().getName().endsWith(".avro");
    }

    public static boolean isParquetDataFile(LocatedFileStatus stat) {
        return stat.isFile() && stat.getPath().getName().endsWith(".parquet");
    }

    /**
     * Expands a provided date instance to a nested directory layout that derives out of it's chronological units.
     * The constructed directory layout with a pattern like {@code year/month/dayOfMonth} ex. {@code 2018/2/25},
     * where each 'slash' is representing a directory separator.
     *
     * @param date the {@link String} instance that represents a given date
     * @return a {@link String} representation of the date-derived nested directory layout.
     */
    public static String expandDateToNestedPaths(String date) {
        if (StringUtils.isBlank(date)) {
            throw new IllegalArgumentException("Provided date cannot be a null or blank string!");
        }
        TemporalAccessor directoryDate = STAGE_DIRECTORY_DATE_FORMATTER.parse(date);

        return new StringJoiner(Path.SEPARATOR)
                .add(Integer.toString(directoryDate.get(ChronoField.YEAR)))
                .add(Integer.toString(directoryDate.get(ChronoField.MONTH_OF_YEAR)))
                .add(Integer.toString(directoryDate.get(ChronoField.DAY_OF_MONTH))).toString();
    }

}
