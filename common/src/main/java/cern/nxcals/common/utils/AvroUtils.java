/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.common.utils;

import org.apache.avro.Schema;
import org.apache.avro.Schema.Field;
import org.apache.avro.generic.GenericRecord;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static org.apache.avro.Schema.Type.RECORD;

/**
 * Utility class for Avro stuff.
 *
 * @author Marcin Sobieszek
 * @date Jul 7, 2016 11:19:16 AM
 */
public final class AvroUtils {
    private AvroUtils() {
        throw new IllegalStateException("No instances allowed");
    }

    /**
     * Checks if the given schema definition is present with non-empty fields in the given record
     */
    public static boolean isChildSchemaCoveredWithNonEmptyValues(GenericRecord record, Schema schema) {
        for (Field f : schema.getFields()) {
            if (record.get(f.name()) == null) {
                return false;
            }
        }
        return true;
    }

    /**
     * <p>
     * Merges two schemas. The schemas must be compatible which means they can contain fields with the same names as
     * long as their definition is also the same.
     * </p>
     * Uses {@code newSchema} meta data for the merged schema.
     */
    public static Schema mergeSchemas(Schema oldSchema, Schema newSchema) {
        if (oldSchema == null) {
            return newSchema;
        }
        if (newSchema == null) {
            return oldSchema;
        }

        if (!(RECORD.equals(oldSchema.getType()) && RECORD.equals(newSchema.getType()))) {
            return newSchema;
        }

        verifyCompatibility(oldSchema, newSchema);
        List<Field> fields = new ArrayList<>(oldSchema.getFields());
        fields.addAll(newSchema.getFields());
        fields = copyFieldsFrom(fields.stream());

        Schema schema = Schema.createRecord(newSchema.getName(), newSchema.getDoc(), newSchema.getNamespace(),
                newSchema.isError());
        schema.setFields(fields);
        return schema;
    }

    /**
     * Verifies if the given schemas contain compatible fields, that is if both schema contain a field with the same
     * name its definitions must be the same.
     */
    private static void verifyCompatibility(Schema oldSchema, Schema newSchema) {
        for (Field f : oldSchema.getFields()) {
            Field field = newSchema.getField(f.name());
            if (field == null) {
                continue;
            }
            if (!f.equals(field)) {
                throw new IllegalStateException("Conflict between schemas detected with field " + f + " and field " + field);
            }
        }
    }

    private static List<Field> copyFieldsFrom(Stream<Field> fields) {
        return fields.distinct().map(f -> new Field(f.name(), f.schema(), f.doc(), f.defaultVal())).collect(toList());
    }
}
