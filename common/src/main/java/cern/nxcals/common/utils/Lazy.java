/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.common.utils;

import java.util.Objects;
import java.util.function.Supplier;

/**
 * Lazily evaluates the variable it holds.
 *
 * @author Marcin Sobieszek
 * @date Jul 28, 2016 4:20:28 PM
 */
public class Lazy<T> implements Supplier<T> {
    private Supplier<T> supplier;
    private T value;

    public Lazy(Supplier<T> newSupplier) {
        this.supplier = Objects.requireNonNull(newSupplier);
    }

    @Override
    public T get() {
        if (this.value == null) {
            synchronized (this) {
                if (this.value == null) {
                    this.value = this.supplier.get();
                    // do not need to hold the reference any more
                    this.supplier = null;
                }
            }
        }
        return this.value;
    }
}
