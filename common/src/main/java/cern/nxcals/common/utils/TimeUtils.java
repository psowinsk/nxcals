/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.common.utils;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * Utility class to provide methods for conversion between different time formats
 *
 * @author ntsvetko
 * @author timartin
 */
public final class TimeUtils {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.n");

    private TimeUtils() {
        /* Static class */
    }

    /**
     * Converts a long with the number of nanoseconds since epoch into its {@link Instant} counterpart.
     *
     * @param timeInNanos the number of seconds since epoch.
     * @return a new {@link Instant}.
     */
    public static final Instant getInstantFromNanos(long timeInNanos) {
        return Instant.ofEpochSecond(TimeUnit.NANOSECONDS.toSeconds(timeInNanos), (int) (timeInNanos % 1000000000));
    }

    /**
     * Converts an {@link Instant} into a long with the number of nanoseconds since epoch.
     *
     * @param instantTime the {@link Instant} to be converter.
     * @return a long with the number of nanoseconds since epoch.
     */
    public static final long getNanosFromInstant(Instant instantTime) {
        Objects.requireNonNull(instantTime, "Cannot convert a null instant");
        return TimeUnit.SECONDS.toNanos(instantTime.getEpochSecond()) + instantTime.getNano();
    }

    /**
     * Converts a {@link String} into its {@link Instant} counterpart.
     *
     * @param timeString a string with the following format: yyyy-MM-dd HH:mm:ss.nnnnnnnnn
     * @return a new instance of {@link Instant}.
     */
    public static final Instant getInstantFromString(String timeString) {
        return LocalDateTime.parse(timeString, FORMATTER).toInstant(ZoneOffset.UTC);
    }

    /**
     * Converts an {@link Instant} into its {@link String} counterpart.
     *
     * @param instant the instant to be converted.
     * @return a {@link String} with the following format: yyyy-MM-dd HH:mm:ss.nnnnnnnnn
     */
    public static final String getStringFromInstant(Instant instant) {
        return FORMATTER.format(instant);
    }
}
