/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.common.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericRecordBuilder;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;
import java.util.Map;

import static java.lang.String.format;

public class KeyValuesUtils {

    private KeyValuesUtils() {
        //Should not be initialized
    }

    private static final String ERROR_MISSING_KEY_IN_KEY_VALUES = "There is not field %s in key values but it was defined in key values definition";
    private static final String ERROR_MISMATCHED_NUMBER_OF_KEYS = "Number of fields in key values definition must match key values";
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public static String convertMapIntoAvroSchemaString(Map<String, Object> entityKeyValuesMap,
            String keyValuesSchema) {
        Schema avroSchema = new Schema.Parser().parse(keyValuesSchema);
        GenericRecordBuilder genericRecordBuilder = new GenericRecordBuilder(avroSchema);
        List<Schema.Field> fields = avroSchema.getFields();
        if (fields.size() != entityKeyValuesMap.size()) {
            throw new IllegalArgumentException(ERROR_MISMATCHED_NUMBER_OF_KEYS);
        }

        for (Schema.Field field : fields) {
            String fieldName = field.name();
            if (!entityKeyValuesMap.containsKey(fieldName)) {
                throw new IllegalArgumentException(format(ERROR_MISSING_KEY_IN_KEY_VALUES, fieldName));
            }
            genericRecordBuilder.set(field, entityKeyValuesMap.get(fieldName));
        }

        return genericRecordBuilder.build().toString();
    }

    public static Map<String, Object> convertKeyValuesStringIntoMap(String keyValues) {
        Map<String, Object> parsedKeyValues;
        try {
            parsedKeyValues = OBJECT_MAPPER.readValue(keyValues, new TypeReference<Map<String, Object>>() {
            });
        } catch (IOException exception) {
            throw new UncheckedIOException("Cannot deserialize from json", exception);
        }
        return parsedKeyValues;
    }
}
