package cern.nxcals.common.utils;

import java.io.File;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.regex.Pattern;

/**
 * Utility class providing methods for extracting information from NXCALS HDFS path
 *
 * @author ntsvetko
 */
public abstract class HdfsPathDecoder {


    private HdfsPathDecoder() {
        throw new IllegalStateException("HdfsPathDecoder class");
    }

    /**
     * Finds systemId from provided NXCALS HDFS path
     *
     * @param path HDFS path
     * @return systemId
     */
    public static long findSystemIdFrom(Path path) {
        verify(path);
        return Long.parseLong(
                path./*schemaId*/getParent()./*partitionId*/getParent()./*systemId*/getParent().getFileName()
                        .toString());
    }

    /**
     * Finds partitionId from provided NXCALS HDFS path
     *
     * @param path HDFS path
     * @return partitioId
     */
    public static long findPartitionIdFrom(Path path) {
        verify(path);
        return Long.parseLong(path./*schemaId*/getParent()./*partitionId*/getParent().getFileName().toString());
    }

    /**
     * Finds schemaId from provided NXCALS HDFS path
     *
     * @param path HDFS path
     * @return schemaId
     */
    public static long findSchemaIdFrom(Path path) {
        verify(path);
        return Long.parseLong(path./*schemaId*/getParent().getFileName().toString());
    }

    /**
     * Finds date in a format "yyyy-MM-dd" from provided NXCALS HDFS path
     *
     * @param path HDFS path
     * @return String representing date in a format "yyyy-MM-dd"
     */
    public static String findDateFrom(Path path) {
        verify(path);
        return path./*date*/getFileName().toString();
    }

    /**
     * Checks whether the tail of the provided NXCALS HDFS path respects the pattern: /systemId/partitionId/schemaId/yyyy-MM-dd
     */
    public static void verify(Path path) {
        try {
            verifyDateFormat(path);
            verifyIdDirectories(path);
        } catch (IllegalArgumentException | ParseException e) {
            throw new IllegalArgumentException("The tested path is not a valid NXCALS directory PATH=" + path);
        }
    }

    private static void verifyDateFormat(Path path) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        formatter.parse(path.getFileName().toString());
    }

    private static void verifyIdDirectories(Path path) {
        String[] pathDirs = path.toString().split(File.separator);
        Pattern datePattern = Pattern.compile("^\\d{4}-\\d{2}-\\d{2}$");

        // Everything before the last 4 directories is the system path to the NXCALS data directory
        int numSystemDirs = pathDirs.length - 4;

        // Skips system directories and filter out date directories
        long numIds = Arrays.stream(pathDirs).skip(numSystemDirs).filter(v -> !datePattern.matcher(v).matches())
                .mapToLong(Long::valueOf).count();

        // Expect to have 3 ID directories in the path
        if (numIds != 3) {
            throw new IllegalArgumentException("The number of ID directories in the path is not equal to 3 !");
        }
    }
}
