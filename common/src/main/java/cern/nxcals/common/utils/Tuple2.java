package cern.nxcals.common.utils;

import lombok.Data;

@Data
public class Tuple2<L, R> {
    private final L left;
    private final R right;
}
