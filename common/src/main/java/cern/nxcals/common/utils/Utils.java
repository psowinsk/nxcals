/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.common.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.binary.Base64;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * @author Marcin Sobieszek
 * @date Jul 28, 2016 4:55:10 PM
 */
public final class Utils {
    private static final ObjectMapper mapper = new ObjectMapper();

    private Utils() {
        throw new IllegalStateException("No instances allowed");
    }

    public static String encodeUrl(String param) {
        return encode(() -> URLEncoder.encode(param, UTF_8.name()));
    }

    public static String encodeWithBase64(String param) {
        return encode(() -> Base64.encodeBase64String(param.getBytes(UTF_8.name())));
    }

    public static String decodeBase64(String encodedValue) {
        return encode(() -> new String(Base64.decodeBase64(encodedValue.getBytes(UTF_8.name()))));
    }

    private static <T> T encode(ExceptionAwareSupplier<T> sup) {
        try {
            return sup.get();
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }

    }

    @FunctionalInterface
    private static interface ExceptionAwareSupplier<T> {
        T get() throws UnsupportedEncodingException;
    }

    public static Map<String, String> convertJsonToMap(String json) {
        try {
            return mapper.readValue(json, new TypeReference<Map<String, String>>() {/**/
            });
        } catch (Exception e) {
            throw new IllegalArgumentException("Exception while converting json to Map<String,String> json=" + json, e);
        }

    }

    /**
     * @param entityKeyValues
     * @return
     */
    public static String convertToJson(Map<String, String> entityKeyValues) {
        try {
            return mapper.writeValueAsString(entityKeyValues);
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException("Exception while converting Map<String,String> to json=" + entityKeyValues, e);
        }
    }
}
