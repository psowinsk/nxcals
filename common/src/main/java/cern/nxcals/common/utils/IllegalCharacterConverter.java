/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.common.utils;

import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.codec.binary.Base32;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static java.lang.String.format;

/**
 * Class that converts name to comply with the avro field name convention. <br>
 * Previously converted values are cached for faster conversions of the same name.
 */
public final class IllegalCharacterConverter {

    @VisibleForTesting
    static final String PREFIX = "__NX_";
    @VisibleForTesting
    static final String CANNOT_DECODE_FIELD_NAME_ERROR = "Cannot decode field name [%s]";
    @VisibleForTesting
    static final String PREFIX_EXISTS_ERROR = "Field name [%s] cannot start with " + PREFIX;
    private static final int PREFIX_LENGTH = PREFIX.length();

    private static IllegalCharacterConverter INSTANCE;

    private final Map<String, String> namesConvertedToLegalCache;
    private final Map<String, String> namesConvertedFromLegalCache;
    private final Base32 base32 = new Base32();

    public static IllegalCharacterConverter get() {
        if (INSTANCE == null) {
            INSTANCE = new IllegalCharacterConverter(10);
        }
        return INSTANCE;
    }

    private IllegalCharacterConverter(int cacheInitialSize) {
        this(new ConcurrentHashMap<>(cacheInitialSize), new ConcurrentHashMap<>(cacheInitialSize));
    }

    @VisibleForTesting
    IllegalCharacterConverter(Map<String, String> namesConvertedToLegalCache,
            Map<String, String> namesConvertedFromLegalCache) {
        this.namesConvertedFromLegalCache = namesConvertedFromLegalCache;
        this.namesConvertedToLegalCache = namesConvertedToLegalCache;
    }

    public String convertFromLegal(String name) {
        try {
            return namesConvertedFromLegalCache.computeIfAbsent(name, toConvert -> internalConvertFromLegal(toConvert));
        } catch (NumberFormatException exception) {
            throw new FieldNameException(format(CANNOT_DECODE_FIELD_NAME_ERROR, name));
        }
    }

    public String convertToLegal(String name) {
        if(name.startsWith(PREFIX)) {
            throw new FieldNameException(format(PREFIX_EXISTS_ERROR, name));
        }
        return namesConvertedToLegalCache.computeIfAbsent(name, toConvert -> internalConvertToLegal(toConvert));
    }

    public String getLegalIfCached(String toConvert) {
        return namesConvertedToLegalCache.getOrDefault(toConvert, toConvert);
    }

    /**
     * Converts any name into an avro field legal name based on Base32 encoding removing the trailing =. </br>
     * For signaling that the name is encoded a tag is added to the beginning of the encoded name, followed by the a
     * single digit with the length of the padding followed then by the encoded name. </br>
     * See RFC 4648 section 6 (https://tools.ietf.org/html/rfc4648#section-6)
     *
     * @param toEncode the string to encode
     * @return a String with the format {@link IllegalCharacterConverter#PREFIX} + number of trailing '=' characters + encoded string
     */
    private String internalConvertToLegal(String toEncode) {
        String encodedString = base32.encodeAsString(toEncode.getBytes());
        int paddingLength = StringUtils.countMatches(encodedString, "=");
        return PREFIX + paddingLength + encodedString.replace("=", "");
    }

    private String internalConvertFromLegal(String toDecode) {
        if (!toDecode.startsWith(PREFIX) || toDecode.length() <= PREFIX_LENGTH + 1) {
            throw new FieldNameException(format(CANNOT_DECODE_FIELD_NAME_ERROR, toDecode));
        }

        int paddingLength = Integer.valueOf(toDecode.substring(PREFIX_LENGTH, PREFIX_LENGTH + 1));
        String stringToDecode = StringUtils.rightPad(toDecode.substring(PREFIX_LENGTH + 1), paddingLength, "=");
        return new String(base32.decode(stringToDecode));
    }

    public static boolean isEncoded(String toTest) {
        return toTest != null && toTest.startsWith(PREFIX);
    }

    public static class FieldNameException extends RuntimeException {
        public FieldNameException(String message) {
            super(message);
        }
    }
}
