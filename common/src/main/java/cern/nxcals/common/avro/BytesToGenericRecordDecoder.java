/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.common.avro;

import cern.nxcals.common.domain.SchemaData;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData.Record;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.ByteBuffer;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

/**
 * Decodes array of bytes into Avro record. The first byte from the array we consider as a sort of check sum. The next 4
 * bytes contain schema id. The rest is message body.
 *
 * @author Marcin Sobieszek
 * @date Jul 14, 2016 4:04:18 PM
 */
public class BytesToGenericRecordDecoder implements Function<byte[], GenericRecord> {
    private static final Logger LOGGER = LoggerFactory.getLogger(BytesToGenericRecordDecoder.class);
    private static final byte MAGIC_BYTE = 0x0;
    private final Function<Long, SchemaData> schemaProvider;
    // FIXME - we should see if this caching is ok here. baseSchemaCache holds the schemas for the client record.
    // (jwozniak)
    // Remarks (msobiesz) - it's duplication of what is cached in the schema provider
    private final ConcurrentHashMap<Long, Schema> schemaCache = new ConcurrentHashMap<>();

    public BytesToGenericRecordDecoder(Function<Long, SchemaData> schemaProvider) {
        this.schemaProvider = Objects.requireNonNull(schemaProvider);
    }

    @Override
    public GenericRecord apply(byte[] data) {
        ByteBuffer buffer = this.getByteBuffer(data);
        long schemaId = buffer.getLong();

        Schema schema = this.schemaCache.computeIfAbsent(schemaId,
                schemaKey -> new Schema.Parser().parse(this.schemaProvider.apply(schemaId).getSchemaJson()));

        if (schema == null) {
            throw new IllegalArgumentException("Unknown schema data for id " + schemaId);
        }
        return this.decodeData(buffer, schema);
    }

    private GenericRecord decodeData(ByteBuffer buffer, Schema schema) {
        Decoder decoder = DecoderFactory.get().binaryDecoder(buffer.array(), buffer.position() - buffer.arrayOffset(),
                buffer.limit() - 9, null);
        GenericDatumReader<Record> rr = new GenericDatumReader<>(schema);
        try {
            return rr.read(null, decoder);
        } catch (IOException e) {
            LOGGER.error("Error while decoding record", e);
            throw new UncheckedIOException(e);
        }
    }

    private ByteBuffer getByteBuffer(byte[] data) {
        ByteBuffer buffer = ByteBuffer.wrap(data);
        if (buffer.get() != MAGIC_BYTE) {
            throw new IllegalArgumentException("Unknown magic byte!");
        }
        return buffer;
    }

}
