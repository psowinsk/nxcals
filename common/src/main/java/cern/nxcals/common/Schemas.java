/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.common;

import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;

import static cern.nxcals.common.SystemFields.NXC_ENTITY_ID;
import static cern.nxcals.common.SystemFields.NXC_PARTITION_ID;
import static cern.nxcals.common.SystemFields.NXC_SCHEMA_ID;
import static cern.nxcals.common.SystemFields.NXC_SYSTEM_ID;
import static cern.nxcals.common.SystemFields.NXC_TIMESTAMP;

/**
 * @author Marcin Sobieszek
 * @date Jul 14, 2016 11:53:10 AM
 */
public enum Schemas {
    /**
     * @formatter:off
     */
    SYSTEM_ID(SchemaBuilder.builder().longType(), NXC_SYSTEM_ID.getValue()),

    ENTITY_ID(SchemaBuilder.builder().longType(), NXC_ENTITY_ID.getValue()),

    PARTITION_ID(SchemaBuilder.builder().longType(), NXC_PARTITION_ID.getValue()),

    SCHEMA_ID(SchemaBuilder.builder().longType(), NXC_SCHEMA_ID.getValue()),

    TIMESTAMP(SchemaBuilder.builder().longType(), NXC_TIMESTAMP.getValue());

    /**
     * @formatter:on
     */

    private final Schema schema;
    private final String fieldName;

    Schemas(Schema newSchema, String newFieldName) {
        this.schema = newSchema;
        this.fieldName = newFieldName;
    }

    public Schema getSchema() {
        return this.schema;
    }

    public String getFieldName() {
        return this.fieldName;
    }

}
