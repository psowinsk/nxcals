/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.common.web;

/**
 * This class is to provide constants that corresponds to service endpoints.
 */
public final class Endpoints {

    private Endpoints() {
        //Nothing to do here
    }

    public static final String ENTITIES = "/entities";

    public static final String ENTITY_EXTEND_FIRST_HISTORY = ENTITIES + "extendFirstHistoryData";

    public static final String ENTITY_UPDATE = ENTITIES + "/update";

    public static final String COMPACTION = "/compaction";

    public static final String COMPACTION_SHOULD_COMPACT = COMPACTION + "/shouldCompact";

    public static final String RESOURCES = "/resources";

    public static final String PARTITIONS = "/partitions";

    public static final String SCHEMAS = "/schemas";

    public static final String SCHEMA_WITH_ID = SCHEMAS + "/{schemaId}";

    public static final String SYSTEMS = "/systems";

    public static final String SYSTEM_WITH_ID = SYSTEMS + "/{id}";

    public static final String VARIABLES = "/variables";

    public static final String VARIABLE_REGISTER_OR_UPDATE = VARIABLES + "/registerOrUpdateVariableFor";
}
