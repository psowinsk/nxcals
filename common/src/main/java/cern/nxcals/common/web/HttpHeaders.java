/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.common.web;

/**
 * Constants with the HTTP headers.
 */
public class HttpHeaders {
    public static final String APPLICATION_JSON_DATA_FORMAT = "application/json";
    public static final String ACCEPT_APPLICATION_JSON = "Accept: " + APPLICATION_JSON_DATA_FORMAT;
    public static final String CONTENT_TYPE_APPLICATION_JSON = "Content-Type: " + APPLICATION_JSON_DATA_FORMAT;
}
