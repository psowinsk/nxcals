/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.common.concurrent;

import java.util.Objects;
import java.util.concurrent.locks.Lock;

/**
 * @author Marcin Sobieszek
 * @date Jul 21, 2016 5:01:41 PM
 */
public class AutoCloseableLock implements AutoCloseable {
    private final Lock lock;

    private AutoCloseableLock(Lock newLock) {
        this.lock = Objects.requireNonNull(newLock);
    }

    public static AutoCloseableLock getFor(Lock lock) {
        AutoCloseableLock ret = new AutoCloseableLock(lock);
        ret.lock.lock();
        return ret;
    }

    @Override
    public void close() {
        this.lock.unlock();
    }

}
