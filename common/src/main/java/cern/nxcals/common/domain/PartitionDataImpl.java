/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.common.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Value;

import javax.validation.constraints.NotEmpty;
import java.util.HashMap;
import java.util.Map;

@JsonDeserialize(builder = PartitionData.Builder.class)
@Value
class PartitionDataImpl implements PartitionData {

    private final long id;
    @NotEmpty
    private final Map<String, Object> keyValues;

    public Map<String, Object> getKeyValues() {
        return new HashMap<>(keyValues);
    }

}
