package cern.nxcals.common.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(as = EntityHistoryDataImpl.class)
public interface EntityHistoryData {
    long getId();

    SchemaData getSchemaData();

    PartitionData getPartitionData();

    Long getValidFromStamp();

    Long getValidToStamp();

    final class Builder {
        private long id;
        private SchemaData schemaData;
        private PartitionData partitionData;
        private Long validFromStamp;
        private Long validToStamp;

        private Builder() {
        }

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder schemaData(SchemaData schemaData) {
            this.schemaData = schemaData;
            return this;
        }

        public Builder partitionData(PartitionData partitionData) {
            this.partitionData = partitionData;
            return this;
        }

        public Builder validFromStamp(Long validFromStamp) {
            this.validFromStamp = validFromStamp;
            return this;
        }

        public Builder validToStamp(Long validToStamp) {
            this.validToStamp = validToStamp;
            return this;
        }

        public EntityHistoryData build() {
            return new EntityHistoryDataImpl(id, schemaData, partitionData, validFromStamp, validToStamp);
        }
    }

    static Builder builder() {
        return new Builder();
    }
}
