package cern.nxcals.common.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.net.URI;
import java.util.Collections;
import java.util.Set;

@JsonDeserialize(as = ResourceDataImpl.class)
public interface ResourceData {
    Set<URI> getHdfsPaths();

    String getHbaseNamespace();

    Set<String> getHbaseTableNames();

    static ResourceData createHdfsResourceData(Set<URI> paths) {
        return new ResourceDataImpl(paths, Collections.emptySet());
    }

    static ResourceData createHbaseResourceData(Set<String> hbaseTableNames) {
        return new ResourceDataImpl(Collections.emptySet(), hbaseTableNames);
    }

    static ResourceData createHbaseResourceData(String hbaseNamespace, Set<String> hbaseTableNames) {
        return new ResourceDataImpl(Collections.emptySet(), hbaseNamespace, hbaseTableNames);
    }

    static ResourceData createEmpty() {
        return new ResourceDataImpl(Collections.emptySet(), Collections.emptySet());
    }

    final class Builder {
        private Set<URI> hdfsPaths;
        private String hbaseNamespace;
        private Set<String> hbaseTableNames;

        private Builder() {
        }

        public Builder hdfsPaths(Set<URI> hdfsPaths) {
            this.hdfsPaths = hdfsPaths;
            return this;
        }

        public Builder hbaseNamespace(String hbaseNamespace) {
            this.hbaseNamespace = hbaseNamespace;
            return this;
        }

        public Builder hbaseTableNames(Set<String> hbaseTableNames) {
            this.hbaseTableNames = hbaseTableNames;
            return this;
        }

        public ResourceData build() {
            return new ResourceDataImpl(hdfsPaths, hbaseNamespace, hbaseTableNames);
        }
    }

    static Builder builder() {
        return new Builder();
    }
}
