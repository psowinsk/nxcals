package cern.nxcals.common.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(as = SystemDataImpl.class)
public interface SystemData {
    long getId();

    String getName();

    String getEntityKeyDefinitions();

    String getTimeKeyDefinitions();

    String getPartitionKeyDefinitions();

    String getRecordVersionKeyDefinitions();

    final class Builder {
        private long id;
        private String name;
        private String entityKeyDefinitions;
        private String timeKeyDefinitions;
        private String partitionKeyDefinitions;
        private String recordVersionKeyDefinitions;

        private Builder() {
        }

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder entityKeyDefinitions(String entityKeyDefinitions) {
            this.entityKeyDefinitions = entityKeyDefinitions;
            return this;
        }

        public Builder timeKeyDefinitions(String timeKeyDefinitions) {
            this.timeKeyDefinitions = timeKeyDefinitions;
            return this;
        }

        public Builder partitionKeyDefinitions(String partitionKeyDefinitions) {
            this.partitionKeyDefinitions = partitionKeyDefinitions;
            return this;
        }

        public Builder recordVersionKeyDefinitions(String recordVersionKeyDefinitions) {
            this.recordVersionKeyDefinitions = recordVersionKeyDefinitions;
            return this;
        }

        public SystemData build() {
            return new SystemDataImpl(id, name, entityKeyDefinitions, partitionKeyDefinitions, timeKeyDefinitions,
                    recordVersionKeyDefinitions);
        }
    }

    static Builder builder() {
        return new Builder();
    }
}
