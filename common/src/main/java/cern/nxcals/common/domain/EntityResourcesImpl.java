/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.common.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

/**
 * Created by ntsvetko on 4/25/17.
 */

@Value
class EntityResourcesImpl implements EntityResources {

    private final long id;
    private final String entityKeyValues;
    private final SystemData systemData;
    private final PartitionData partitionData;
    private final SchemaData schemaData;
    private final ResourceData resourcesData;

    @JsonCreator
    EntityResourcesImpl(@JsonProperty("id") long id, @JsonProperty("entityKeyValues") String entityKeyValues,
            @JsonProperty("systemData") SystemData systemData,
            @JsonProperty("partitionData") PartitionData partitionData,
            @JsonProperty("schemaData") SchemaData schemaData,
            @JsonProperty("resourcesData") ResourceData resourcesData) {
        this.id = id;
        this.entityKeyValues = entityKeyValues;
        this.systemData = systemData;
        this.partitionData = partitionData;
        this.schemaData = schemaData;
        this.resourcesData = resourcesData;
    }

}
