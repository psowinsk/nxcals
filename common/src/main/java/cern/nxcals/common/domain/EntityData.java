/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.common.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.NonNull;

import java.util.Map;
import java.util.SortedSet;

@JsonDeserialize(as = EntityDataImpl.class)
public interface EntityData {
    long getId();

    long getRecVersion();

    Map<String, Object> getEntityKeyValues();

    SchemaData getSchemaData();

    SystemData getSystemData();

    PartitionData getPartitionData();

    SortedSet<EntityHistoryData> getEntityHistoryData();

    @JsonIgnore
    EntityHistoryData getFirstEntityHistoryData();

    Long getLockUntilEpochNanos();

    final class Builder {
        @NonNull
        private final EntityData entityData;

        private Map<String, Object> entityKeyValues;

        private Long lockUntilEpochNanos;

        private Builder(EntityData entityData) {
            this.entityData = entityData;
            this.lockUntilEpochNanos = entityData.getLockUntilEpochNanos();
            this.entityKeyValues = entityData.getEntityKeyValues();
        }

        /**
         * Overwrites the existing set of key-values for the target entity with the provided ones.
         * NOTE: This is not currently used but kept for future rename functionality.
         *
         * @param entityKeyValues a {@link Map} instance that contains the new set of key-value pairs
         * @return the {@link Builder} instance itself, this time updated to contain entity key values
         */
        public Builder withNewEntityKeyValues(Map<String, Object> entityKeyValues) {
            this.entityKeyValues = entityKeyValues;
            return this;
        }

        /**
         * Registers a lock that will disallow operations from being applied on the target entity,
         * until the pass of timestamp, defined by the provided number of nanoseconds since epoch.
         *
         * @param lockUntilEpochNanos the number of nanos since epoch, that represents
         *                            the expiration timestamp of the lock
         * @return the {@link Builder} instance itself, this time updated to contain the lock action
         */
        public Builder lockUntil(long lockUntilEpochNanos) {
            this.lockUntilEpochNanos = lockUntilEpochNanos;
            return this;
        }

        /**
         * Removes the lock on entity to mark it as available for operation actions.
         *
         * @return the {@link Builder} instance itself, this time updated to contain the unlock action
         */
        public Builder unlock() {
            lockUntilEpochNanos = null;
            return this;
        }

        public EntityData build() {
            return new EntityDataImpl(entityData.getId(), entityKeyValues, entityData.getSystemData(),
                    entityData.getPartitionData(), entityData.getSchemaData(), entityData.getEntityHistoryData(),
                    lockUntilEpochNanos, entityData.getRecVersion());
        }
    }

    static Builder builder(EntityData entityData) {
        return new Builder(entityData);
    }

    static Builder builder(long id, Map<String, Object> entityKeyValues,
            SystemData systemData, PartitionData partitionData,
            SchemaData schemaData, SortedSet<EntityHistoryData> entityHistoryData,
            Long lockUntilEpochNanos, long recVersion) {
        return new Builder(new EntityDataImpl(id, entityKeyValues, systemData, partitionData,
                schemaData, entityHistoryData,
                lockUntilEpochNanos, recVersion));
    }

}
