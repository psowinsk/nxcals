package cern.nxcals.common.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(as = VariableConfigDataImpl.class)
public interface VariableConfigData extends Comparable<VariableConfigData> {
    long getEntityId();

    String getFieldName();

    Long getValidFromStamp();

    Long getValidToStamp();

    final class Builder {
        private long entityId;
        private String fieldName;
        private Long validFromStamp;
        private Long validToStamp;

        private Builder() {
        }

        public Builder entityId(long entityId) {
            this.entityId = entityId;
            return this;
        }

        public Builder fieldName(String fieldName) {
            this.fieldName = fieldName;
            return this;
        }

        public Builder validFromStamp(Long validFromStamp) {
            this.validFromStamp = validFromStamp;
            return this;
        }

        public Builder validToStamp(Long validToStamp) {
            this.validToStamp = validToStamp;
            return this;
        }

        public VariableConfigData build() {
            return new VariableConfigDataImpl(entityId, fieldName, validFromStamp, validToStamp);
        }
    }

    static Builder builder() {
        return new Builder();
    }

}
