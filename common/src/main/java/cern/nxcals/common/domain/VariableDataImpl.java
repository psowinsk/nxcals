/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.common.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.util.SortedSet;

/**
 * Created by ntsvetko on 1/12/17.
 */

@Value
class VariableDataImpl implements VariableData {

    private final String variableName;
    private final String description;
    private final Long creationTimeUtc;

    @JsonManagedReference
    private SortedSet<VariableConfigData> variableConfigData;

    @JsonCreator
    VariableDataImpl(@JsonProperty("variableName") String variableName,
            @JsonProperty("description") String description,
            @JsonProperty("creationTimeUtc") Long creationTimeUtc,
            @JsonProperty("variableConfigData") SortedSet<VariableConfigData> variableConfigData) {
        this.variableName = variableName;
        this.description = description;
        this.creationTimeUtc = creationTimeUtc;
        this.variableConfigData = variableConfigData;
    }

    @Override
    public SortedSet<VariableConfigData> getVariableConfigData() {
        return variableConfigData;
    }
}
