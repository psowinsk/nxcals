package cern.nxcals.common.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(as = SchemaDataImpl.class)
public interface SchemaData {
    long getId();

    String getSchemaJson();

    final class Builder {
        private long id;
        private String schemaJson;

        private Builder() {
        }

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder schema(String schemaJson) {
            this.schemaJson = schemaJson;
            return this;
        }

        public SchemaData build() {
            return new SchemaDataImpl(id, schemaJson);
        }

    }

    static Builder builder() {
        return new Builder();
    }

}
