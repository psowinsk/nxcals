/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.common.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Value;
import org.apache.commons.lang.StringUtils;

import java.net.URI;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;

/**
 * Represents an instance of {@link ResourceData} that contains information about
 * the persistence locations of available resources.
 */
@Value
class ResourceDataImpl implements ResourceData {
    private final Set<URI> hdfsPaths;

    private static final String HBASE_NAMESPACE_DEFAULT = "default";

    @Getter
    private final String hbaseNamespace;

    private final Set<String> hbaseTableNames;

    @JsonCreator
    ResourceDataImpl(@JsonProperty("hdfsPaths") Set<URI> hdfsPaths,
            @JsonProperty("hbaseNamespace") String hbaseNamespace,
            @JsonProperty("hbaseTableNames") Set<String> hbaseTableNames) {
        this.hdfsPaths = Objects.requireNonNull(hdfsPaths);
        this.hbaseNamespace = StringUtils.defaultIfBlank(hbaseNamespace, HBASE_NAMESPACE_DEFAULT);
        this.hbaseTableNames = Objects.requireNonNull(hbaseTableNames);
    }

    ResourceDataImpl(Set<URI> hdfsPaths, Set<String> hbaseTableNames) {
        this(hdfsPaths, HBASE_NAMESPACE_DEFAULT, hbaseTableNames);
    }

    @Override
    public Set<URI> getHdfsPaths() {
        return Collections.unmodifiableSet(hdfsPaths);
    }

    @Override
    public Set<String> getHbaseTableNames() {
        return Collections.unmodifiableSet(hbaseTableNames);
    }

}
