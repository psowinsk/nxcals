/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.common.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NonNull;
import lombok.Value;

/**
 * @author Marcin Sobieszek
 * @author jwozniak
 * @date Jul 11, 2016 5:09:05 PM
 */

@Value
class SchemaDataImpl implements SchemaData {

    private final long id;
    private final String schemaJson;

    SchemaDataImpl(@JsonProperty("id") long id, @NonNull @JsonProperty("schemaJson") String schema) {
        this.id = id;
        this.schemaJson = schema;
    }

}
