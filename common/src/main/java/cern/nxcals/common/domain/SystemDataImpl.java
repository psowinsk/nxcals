/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.common.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.Setter;
import lombok.Value;

/**
 * Remarks (msobiesz)
 * <ul>
 * <li>Why we don't have the definitions stored as avro.Schema? Looks like it would simplify a lot of things</li>
 * </ul>
 */

@Value
class SystemDataImpl implements SystemData {

    private final long id;
    private final String name;
    private final String entityKeyDefinitions;
    private final String timeKeyDefinitions;
    private final String partitionKeyDefinitions;
    @Setter(AccessLevel.NONE)
    private String recordVersionKeyDefinitions;

    SystemDataImpl(@JsonProperty("id") long id, @NonNull @JsonProperty("name") String name,
            @NonNull @JsonProperty("entityKeyDefinitions") String entityKeyDefinitions,
            @NonNull @JsonProperty("partitionKeyDefinitions") String partitionKeyDefinitions,
            @NonNull @JsonProperty("timeKeyDefinitions") String timeKeyDefinitions,
            @JsonProperty("recordVersionKeyDefinitions") String recordVersionKeyDefs) {
        this.id = id;
        this.name = name;
        this.entityKeyDefinitions = entityKeyDefinitions;
        this.timeKeyDefinitions = timeKeyDefinitions;
        this.partitionKeyDefinitions = partitionKeyDefinitions;
        //can be null if system did not define recordVersion definitions
        this.recordVersionKeyDefinitions = recordVersionKeyDefs;
    }
}
