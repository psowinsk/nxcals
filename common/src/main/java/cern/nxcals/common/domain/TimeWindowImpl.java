/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.common.domain;

import lombok.AllArgsConstructor;
import lombok.Value;

import javax.validation.constraints.NotNull;
import java.time.Instant;

/**
 * @author ntsvetko
 */
@Value
@AllArgsConstructor
class TimeWindowImpl implements TimeWindow {
    @NotNull
    private final Instant startTime;
    @NotNull
    private final Instant endTime;
}
