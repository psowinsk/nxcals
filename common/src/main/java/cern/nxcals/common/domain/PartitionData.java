/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.common.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

@JsonDeserialize(as = PartitionDataImpl.class)
public interface PartitionData {
    long getId();

    Map<String, Object> getKeyValues();

    @JsonPOJOBuilder(withPrefix = "")
    final class Builder {

        private long id;
        private Map<String, Object> keyValues;

        private Builder() {
        }

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder keyValues(Map<String, Object> keyValues) {
            this.keyValues = ImmutableMap.copyOf(keyValues);
            return this;
        }

        public PartitionData build() {
            return new PartitionDataImpl(id, keyValues);
        }
    }

    static Builder builder() {
        return new Builder();
    }

}
