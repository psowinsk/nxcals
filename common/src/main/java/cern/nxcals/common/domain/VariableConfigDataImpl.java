package cern.nxcals.common.domain;

import cern.nxcals.common.utils.TimeUtils;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.time.Instant;
import java.util.Comparator;

/**
 * Created by ntsvetko on 1/12/17.
 */

@Value
class VariableConfigDataImpl implements VariableConfigData {

    private final long entityId;
    private final String fieldName;
    private final Long validFromStamp;
    private final Long validToStamp;

    @JsonCreator
    VariableConfigDataImpl(@JsonProperty("entityId") long entityId,
            @JsonProperty("fieldName") String fieldName,
            @JsonProperty("validFromStamp") Long validFromStamp,
            @JsonProperty("validToStamp") Long validToStamp) {
        this.entityId = entityId;
        this.fieldName = fieldName;
        this.validFromStamp = validFromStamp;
        this.validToStamp = validToStamp;
    }

    @Override
    public int compareTo(VariableConfigData o) {
        return Comparator.<Instant>reverseOrder().compare(
                this.validFromStamp == null ? Instant.EPOCH : TimeUtils.getInstantFromNanos(this.validFromStamp),
                o.getValidFromStamp() == null ? Instant.EPOCH : TimeUtils.getInstantFromNanos(o.getValidFromStamp()));
    }
}
