/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.common.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 * DTO for the findOrCreateEntity request endpoint
 */
@JsonDeserialize(as = FindOrCreateEntityRequestImpl.class)
public interface FindOrCreateEntityRequest {

    /**
     * Gets the key values that identify the entity.
     *
     * @return a {@link Map} of {@link String}s to {@link Object}s.
     */
    Map<String, Object> getEntityKeyValues();

    /**
     * Gets the key values that identify the partition of the entity.
     *
     * @return a {@link Map} of {@link String}s to {@link Object}s.
     */
    Map<String, Object> getPartitionKeyValues();

    /**
     * Gets the string json representation of the full schema of the data sent.
     *
     * @return a json formatted {@link String}.
     */
    String getSchema();

    @JsonPOJOBuilder(withPrefix = "")
    final class Builder {

        private Map<String, Object> entityKeyValues;

        private Map<String, Object> partitionKeyValues;

        private String schema;

        private Builder() {
        }

        /* Collection setters are using immutable-maps to provide immutability */

        public FindOrCreateEntityRequestImpl.Builder entityKeyValues(Map<String, Object> entityKeyValues) {
            this.entityKeyValues = ImmutableMap.copyOf(entityKeyValues);
            return this;
        }

        public FindOrCreateEntityRequestImpl.Builder partitionKeyValues(Map<String, Object> partitionKeyValues) {
            this.partitionKeyValues = ImmutableMap.copyOf(partitionKeyValues);
            return this;
        }

        public FindOrCreateEntityRequestImpl.Builder schema(String schema) {
            this.schema = schema;
            return this;
        }

        public FindOrCreateEntityRequestImpl build() {
            return new FindOrCreateEntityRequestImpl(entityKeyValues, partitionKeyValues, schema);
        }
    }

    static Builder builder() {
        return new Builder();
    }
}
