package cern.nxcals.common.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Value;

import javax.validation.constraints.NotEmpty;
import java.util.HashMap;
import java.util.Map;

/**
 * DTO for the findOrCreateEntity request endpoint
 */
@Value
@JsonDeserialize(builder = FindOrCreateEntityRequest.Builder.class)
final class FindOrCreateEntityRequestImpl implements FindOrCreateEntityRequest {
    @NotEmpty
    private final Map<String, Object> entityKeyValues;
    @NotEmpty
    private final Map<String, Object> partitionKeyValues;
    @NotEmpty
    private final String schema;

    public Map<String, Object> getEntityKeyValues() {
        return new HashMap<>(entityKeyValues);
    }

    public Map<String, Object> getPartitionKeyValues() {
        return new HashMap<>(partitionKeyValues);
    }

}