package cern.nxcals.common.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.SortedSet;

@JsonDeserialize(as = VariableDataImpl.class)
public interface VariableData {
    String getVariableName();

    String getDescription();

    Long getCreationTimeUtc();

    SortedSet<VariableConfigData> getVariableConfigData();

    final class Builder {
        private String variableName;
        private String description;
        private Long creationTimeUtc;
        private SortedSet<VariableConfigData> variableConfigData;

        private Builder() {
        }

        public Builder name(String variableName) {
            this.variableName = variableName;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder creationTimeUtc(Long creationTimeUtc) {
            this.creationTimeUtc = creationTimeUtc;
            return this;
        }

        public Builder variableConfigData(SortedSet<VariableConfigData> variableConfigData) {
            this.variableConfigData = variableConfigData;
            return this;
        }

        public VariableData build() {
            return new VariableDataImpl(variableName, description, creationTimeUtc, variableConfigData);
        }

    }

    static Builder builder() {
        return new Builder();
    }

}
