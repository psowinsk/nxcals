/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.common.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.util.Comparator;
import java.util.Objects;

/**
 * Represents the REST EntityHist information
 *
 * @author ntsvetko
 */

@Value
class EntityHistoryDataImpl implements EntityHistoryData, Comparable<EntityHistoryData> {

    private final long id;
    private final SchemaData schemaData;
    private final PartitionData partitionData;
    private final Long validFromStamp;
    private final Long validToStamp;

    EntityHistoryDataImpl(@JsonProperty("id") long id,
            @JsonProperty("schemaData") SchemaData schemaData,
            @JsonProperty("partitionData") PartitionData partitionData,
            @JsonProperty("validFromStamp") Long validFromStamp,
            @JsonProperty("validToStamp") Long validToStamp) {

        this.id = id;
        this.schemaData = Objects.requireNonNull(schemaData);
        this.partitionData = Objects.requireNonNull(partitionData);
        this.validFromStamp = Objects.requireNonNull(validFromStamp);
        //this can be null
        this.validToStamp = validToStamp;
    }

    @Override
    public int compareTo(EntityHistoryData o) {
        return Comparator.<Long>reverseOrder().compare(this.validFromStamp, o.getValidFromStamp());
    }

}
