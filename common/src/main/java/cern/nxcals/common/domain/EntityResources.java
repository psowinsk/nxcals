package cern.nxcals.common.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(as = EntityResourcesImpl.class)
public interface EntityResources {
    long getId();

    String getEntityKeyValues();

    SchemaData getSchemaData();

    SystemData getSystemData();

    PartitionData getPartitionData();

    ResourceData getResourcesData();

    final class Builder {
        private long id;
        private String entityKeyValues;
        private SystemData systemData;
        private PartitionData partitionData;
        private SchemaData schemaData;
        private ResourceData resourceData;

        private Builder() {
        }

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder entityKeyValues(String entityKeyValues) {
            this.entityKeyValues = entityKeyValues;
            return this;
        }

        public Builder systemData(SystemData systemData) {
            this.systemData = systemData;
            return this;
        }

        public Builder partitionData(PartitionData partitionData) {
            this.partitionData = partitionData;
            return this;
        }

        public Builder schemaData(SchemaData schemaData) {
            this.schemaData = schemaData;
            return this;
        }

        public Builder resourceData(ResourceData resourceData) {
            this.resourceData = resourceData;
            return this;
        }

        public EntityResources build() {
            return new EntityResourcesImpl(id, entityKeyValues, systemData, partitionData, schemaData, resourceData);
        }
    }

    static Builder builder() {
        return new Builder();
    }
}
