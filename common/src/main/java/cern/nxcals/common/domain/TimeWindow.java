package cern.nxcals.common.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.time.Instant;

@JsonDeserialize(as = TimeWindowImpl.class)
public interface TimeWindow {
    Instant getStartTime();

    Instant getEndTime();

    static TimeWindow between(Instant startTime, Instant endTime) {
        if (startTime.isAfter(endTime)) {
            throw new IllegalArgumentException("startTime is after endTime");
        }

        return new TimeWindowImpl(startTime, endTime);
    }
}
