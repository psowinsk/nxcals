/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.common.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.Value;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.SortedSet;

/**
 * Represents the REST Entity information (DTO).
 */
@Value
@EqualsAndHashCode(exclude = "entityHistoryData")
class EntityDataImpl implements EntityData {

    private final long recVersion;
    private final long id;
    @NonNull
    private final Map<String, Object> entityKeyValues;
    @NonNull
    private final SchemaData schemaData;
    @NonNull
    private final SystemData systemData;
    @NonNull
    private final PartitionData partitionData;

    private final Long lockUntilEpochNanos;

    @JsonManagedReference
    private final SortedSet<EntityHistoryData> entityHistoryData;

    EntityDataImpl(@JsonProperty("id") long id,
            @JsonProperty("entityKeyValues") Map<String, Object> entityKeyValues,
            @JsonProperty("systemData") SystemData systemData,
            @JsonProperty("partitionData") PartitionData partitionData,
            @JsonProperty("schemaData") SchemaData schemaData,
            @JsonProperty("entityHistoryData") SortedSet<EntityHistoryData> entityHistoryData,
            @JsonProperty(value = "lockUntilEpochNanos") Long lockUntilEpochNanos,
            @JsonProperty("version") long recVersion) {
        this.id = id;
        this.entityKeyValues = Objects.requireNonNull(entityKeyValues);
        this.systemData = Objects.requireNonNull(systemData);
        this.partitionData = Objects.requireNonNull(partitionData);
        this.schemaData = Objects.requireNonNull(schemaData);
        this.entityHistoryData = entityHistoryData;
        this.lockUntilEpochNanos = lockUntilEpochNanos;
        this.recVersion = recVersion;
    }

    @Override
    public SortedSet<EntityHistoryData> getEntityHistoryData() {
        return Collections.unmodifiableSortedSet(entityHistoryData);
    }

    @Override
    @JsonIgnore
    public EntityHistoryData getFirstEntityHistoryData() {
        return entityHistoryData.last();
    }

}
