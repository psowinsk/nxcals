/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.common;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Marcin Sobieszek
 * @date Jul 7, 2016 5:13:09 PM
 */
public enum SystemFields {
    /**
     * @formatter:off
     */
    NXC_SYSTEM_ID("__sys_nxcals_system_id__"),
    NXC_PARTITION_ID("__sys_nxcals_partition_id__"),
    NXC_ENTITY_ID("__sys_nxcals_entity_id__"),
    NXC_SCHEMA_ID("__sys_nxcals_schema_id__"),
    NXC_TIMESTAMP("__sys_nxcals_timestamp__"),

    // reserved system fields used for extraction
    NXC_EXTR_VARIABLE_NAME("nxcals_variable_name"),
    NXC_EXTR_TIMESTAMP("nxcals_timestamp"),
    NXC_EXTR_VALUE("nxcals_value"),
    NXC_EXTR_ENTITY_ID("nxcals_entity_id");
    /**
     * @formatter:on
     */

    private final String value;

    SystemFields(String newValue) {
        this.value = newValue;
    }

    public String getValue() {
        return this.value;
    }

    public static Set<String> getAllSystemFieldNames() {
        return Arrays.stream(SystemFields.values()).map(SystemFields::getValue).collect(Collectors.toSet());
    }

}
