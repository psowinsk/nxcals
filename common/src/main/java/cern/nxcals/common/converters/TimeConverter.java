/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.common.converters;

import org.apache.avro.generic.GenericRecord;

public interface TimeConverter {
    Long convert(GenericRecord record);
}
