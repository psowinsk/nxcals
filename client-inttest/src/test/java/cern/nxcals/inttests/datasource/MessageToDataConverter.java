/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.inttests.datasource;

import cern.nxcals.inttests.datamodel.Message;
import cern.cmw.data.Data;
import cern.cmw.data.DataFactory;

import java.util.function.Function;

/**
 * @author Krzysztof Nawara
 * @author jwozniak
 * @date Jul 26, 2016 9:55:08 PM
 */
class MessageToDataConverter implements Function<Message, Data> {

    @Override
    public Data apply(Message mesg) {
        Data record = DataFactory.createData();
        record.append(MessageSchema.DATA_FIELD_NAME, mesg.getData());
        record.append(MessageSchema.KEY_FIELD_NAME, mesg.getKey());
        record.append(MessageSchema.PARTITION_FIELD_NAME, mesg.getPartition());
        record.append(MessageSchema.TIMESTAMP_FIELD_NAME, mesg.getTimestamp());
        return record;
    }
}
