/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.inttests.datasource;

import cern.nxcals.inttests.datamodel.Message;
import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;

/**
 * @author Krzysztof Nawara
 * @date Aug 04, 2016 11:52 AM
 */
public class MessageSchema {
    public static final String KEY_FIELD_NAME = "key";
    public static final String PARTITION_FIELD_NAME = "partition";
    public static final String TIMESTAMP_FIELD_NAME = "timestamp";
    public static final String DATA_FIELD_NAME = "data";

    public static final Schema KEY_SCHEMA = SchemaBuilder.record("key").fields()
            .requiredString(KEY_FIELD_NAME)
            .endRecord();

    public static final Schema PARTITION_SCHEMA = SchemaBuilder.record("partiton").fields()
            .requiredString(PARTITION_FIELD_NAME)
            .endRecord();

    public static final Schema TIME_SCHEMA = SchemaBuilder.record("timestamp").fields()
            .requiredString(TIMESTAMP_FIELD_NAME)
            .endRecord();

    public static final Schema DATA_SCHEMA = SchemaBuilder.record("data").fields()
            .requiredString(KEY_FIELD_NAME)
            .requiredString(PARTITION_FIELD_NAME)
            .requiredString(TIMESTAMP_FIELD_NAME)
            .requiredString(DATA_FIELD_NAME)
            .endRecord();

    public static GenericRecord messageToAvro(Schema s, Message m) {
        GenericRecord dataRecord = new GenericData.Record(s);
        dataRecord.put(KEY_FIELD_NAME, m.getKey());
        dataRecord.put(PARTITION_FIELD_NAME, m.getPartition());
        dataRecord.put(TIMESTAMP_FIELD_NAME, m.getTimestamp());
        dataRecord.put(DATA_FIELD_NAME, m.getData());
        return dataRecord;
    }

    public static Message avroToMessage(GenericRecord r) {
        String key = r.get(KEY_FIELD_NAME).toString();
        String partition = r.get(PARTITION_FIELD_NAME).toString();
        String timestamp = r.get(TIMESTAMP_FIELD_NAME).toString();
        String data = r.get(DATA_FIELD_NAME).toString();
        return new Message(key, partition, timestamp, data);
    }
}
