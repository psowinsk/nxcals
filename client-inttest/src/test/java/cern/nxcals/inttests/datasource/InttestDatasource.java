/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.inttests.datasource;

import cern.nxcals.client.Publisher;
import cern.nxcals.client.PublisherFactory;
import cern.nxcals.inttests.datamodel.Message;

import java.util.Set;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

/**
 * Class is responsible for sending messages to NXCALS (synchronously)
 *
 * @author Krzysztof Nawara
 * @date Aug 11, 2016 3:03 PM
 */
public class InttestDatasource {
    public static final String SYSTEM_NAME = "INTTEST";

    /**
     * Setups DefaultCalsProducer, attempts to send messages, then closes the producer.
     * Messages are sent synchronously (producer.close() waits until all messages are sent).
     * However, I wasn't really precise when using the verb 'sent' - to ensure that they were actually sent (and
     * some error wasn't returned), you have to examine set of returned Futures.
     */
    public static Set<Future<Void>> sendMessagesToSystem(Set<Message> messagesToSend) throws Exception {
        final MessageToDataConverter serializer = new MessageToDataConverter();
        Publisher<Message> producer = PublisherFactory.createPublisher(SYSTEM_NAME, serializer);

        Set<Future<Void>> results = messagesToSend.stream()
                .map(producer::publish)
                .collect(Collectors.toSet());

        producer.close();

        return results;
    }
}
