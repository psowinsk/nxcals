/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.inttests.kafka;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.io.IOException;
import java.util.stream.Collectors;

/**
 * This class is responsible for reading back records from Kafka. To do that, before sending messages
 * to NXCALS we have to mark the offset because previous test might have failed, leaving some unconsumed messages.
 *
 * @author Krzysztof Nawara
 * @date Aug 11, 2016 3:30 PM
 */
public class InttestKafkaClient implements Closeable {
    private static final Logger LOGGER = LoggerFactory.getLogger(InttestKafkaClient.class);
    private final KafkaConsumer<byte[], byte[]> consumer;

    public InttestKafkaClient(Collection<String> topicsToSubscribeTo) {
        consumer = new KafkaConsumer<>(getConsumerConfig());
        consumer.subscribe(topicsToSubscribeTo);
    }

    public void markOffset() {
        LOGGER.debug("Marking offset");
        /* with empty list as argument should seek for all parititons that we subscribed to */
        consumer.seekToEnd(Collections.emptyList());
        /* but seeking is lazy, so we need to call poll */
        consumer.poll(200);
		/* we don't use autocommit, so I think this is required */
        consumer.commitSync();
        LOGGER.debug("Offset marked");
    }

    public Map<String, Set<ConsumerRecord<byte[], byte[]>>> retrieveRecords(int amountExpected, long timeoutMillis) {
        LOGGER.debug("Started: polling messages from Kafka");

        int polledMessagesCount = 0;
        Map<String, Set<ConsumerRecord<byte[], byte[]>>> retrievedRecords = new HashMap<>();
        long stopTime = System.currentTimeMillis() + timeoutMillis;
        while (polledMessagesCount < amountExpected && System.currentTimeMillis() < stopTime) {
            try {
                ConsumerRecords<byte[], byte[]> data = consumer.poll(200);
                consumer.commitSync();

                polledMessagesCount += data.count();
                data.forEach(r -> {
                    LOGGER.trace("Received record: {}", Objects.toString(r));
                    retrievedRecords.compute(r.topic(), (k, v) -> {
                        if (v == null) {
                            HashSet<ConsumerRecord<byte[], byte[]>> list = new HashSet<>();
                            list.add(r);
                            return list;
                        } else {
                            v.add(r);
                            return v;
                        }
                    });
                });
            } catch (Exception e) {
                LOGGER.error("Kafka consumer thrown exception during message retrieval (below). " +
                        "Ignoring and waiting for timeout.", e);
            }
        }

        LOGGER.debug("Ended: polling messages from Kafka. Polled {} messages", polledMessagesCount);

        return retrievedRecords;
    }

    @Override
    public void close() throws IOException {
        consumer.close();
    }

    private static Map<String, Object> getConsumerConfig() {
        Config config = ConfigFactory.load();

        return config.getConfig("kafka.consumer").entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, cv -> cv.getValue().unwrapped()));
    }
}
