/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.inttests.ingestionapi;

import cern.nxcals.inttests.datamodel.Message;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.util.Arrays.asList;

/**
 * @author Krzysztof Nawara
 * @date Aug 03, 2016 3:43 PM
 */
public class TestMessageSet {
    public static final List<String> TEST_PARTITIONS = asList("partition1", "partition2");
    public static final int TEST_MESSAGES_PER_PARTITION = 2;
    public static final int MESSAGES_TOTAL = TEST_MESSAGES_PER_PARTITION * TEST_PARTITIONS.size();

    public static final Set<Message> MESSAGES_SENT_DURING_TEST = new HashSet<>(MESSAGES_TOTAL);

    private static void m(String key, String partition, String timestamp, String data) {
        MESSAGES_SENT_DURING_TEST.add(new Message(key, partition, timestamp, data));
    }

    static {
        TEST_PARTITIONS.forEach(p -> {
            for (int j = 0; j < TEST_MESSAGES_PER_PARTITION; j++) {
                String mesg_in_partition = String.valueOf(j);
                m("key".concat(mesg_in_partition), p, "ts".concat(mesg_in_partition), "d".concat(mesg_in_partition));
            }
        });
    }

    public static String getTestMessagesStringRepresentation() {
        StringBuilder builder = new StringBuilder("Messages I'm going to send to NXCALS: \n");
        TestMessageSet.MESSAGES_SENT_DURING_TEST.forEach(m -> builder.append(m).append('\n'));
        return builder.toString();
    }
}
