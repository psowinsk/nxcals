/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.inttests.ingestionapi;

import cern.nxcals.common.avro.BytesToGenericRecordDecoder;
import cern.nxcals.inttests.TestFailedException;
import cern.nxcals.inttests.datamodel.Message;
import cern.nxcals.inttests.datasource.MessageSchema;
import cern.nxcals.service.client.ServiceClientFactory;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Methods used for verification correctness of the behaviour of system under test (on various stages of the test) The
 * convention - in case of negative result of the verification, TestFailedException is thrown
 *
 * @author Krzysztof Nawara
 * @date Aug 11, 2016 3:16 PM
 */
public class Verifier {
    private static final Logger LOGGER = LoggerFactory.getLogger(Verifier.class);

    public static void verifyMessagesSentOrTimeout(Set<Future<Void>> results, long pollTimeout, long globalTimeout)
            throws TestFailedException {
        LOGGER.debug("Started: verification whether all messages were sent");

        long stopTime = System.currentTimeMillis() + globalTimeout;
        boolean foundAll = false;
        while (!foundAll && System.currentTimeMillis() < stopTime) {
            Future<Void> result;
            for (Iterator<Future<Void>> it = results.iterator(); it.hasNext() && !foundAll; ) {
                result = it.next();
                try {
                    result.get(pollTimeout, TimeUnit.MILLISECONDS);
                    it.remove();
                    LOGGER.trace("Sending was successful, {} left", results.size());
                    if (results.isEmpty())
                        foundAll = true;
                } catch (InterruptedException e) {
                    LOGGER.warn("Unexpected InterruptedException", e);
                } catch (TimeoutException e) {
                    // not really an error
                    LOGGER.trace("Timeout");
                } catch (ExecutionException e) {
                    throw new TestFailedException("Couldn't send message", e.getCause());
                }
            }
        }

        if (!foundAll) {
            throw new TestFailedException(results.size() + " messeges were not sent");
        }

        LOGGER.debug("Finished: verification whether all messages were sent");
    }

    /**
     * Checks: - whether all sent messages arrived - whether only sent messages arrived
     */
    public static void verifyOnlySentMessagesArrived(Map<String, Set<ConsumerRecord<byte[], byte[]>>> rawRecords,
            Set<Message> expected) throws TestFailedException {

        LOGGER.debug("Started: decoding of messages & verification whether all were retrieved");

        final BytesToGenericRecordDecoder decoder = new BytesToGenericRecordDecoder(
                id -> ServiceClientFactory.createSchemaService().findById(
                        id));
        /* I'm gonna remove things from this, so I make a copy */
        final Set<Message> expectedCopy = new HashSet<>(expected);
        rawRecords.entrySet().forEach(
                e -> {
                    final String topic = e.getKey();
                    e.getValue()
                            .stream()
                            .map(ConsumerRecord::value)
                            .map(decoder::apply)
                            .map(MessageSchema::avroToMessage)
                            .forEach(
                                    m -> {
                                        boolean removed = expectedCopy.remove(m);

                                        if (!removed) {
                                            throw new TestFailedException(
                                                    "Consumed record which shouldn't even be in Kafka in topic. "
                                                            + "Maybe from previously failed test?" + topic
                                                            + "\nRecord: " + m.toString());
                                        }
                                    });
                });

        if (expectedCopy.size() > 0) {
            StringBuilder message = new StringBuilder("Not all expected records were consumed from Kafka. Missing:");
            expectedCopy.stream().map(Message::toString).forEach(s -> message.append("\n").append(s));
            throw new TestFailedException(message.toString());
        }

        LOGGER.debug("Ended: decoding of messages & verification whether all were retrieved");
    }
}
