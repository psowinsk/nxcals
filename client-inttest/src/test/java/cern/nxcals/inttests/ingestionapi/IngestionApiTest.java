/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.inttests.ingestionapi;

import cern.nxcals.inttests.datasource.InttestDatasource;
import cern.nxcals.inttests.kafka.InttestKafkaClient;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;

/**
 * Integration test for NXCALS Ingestion API.
 * When test is successful, application returns successfully. Otherwise TestFailedException is thrown.
 * Message class - represents records that are going to be sent durnig test
 * Messages which are going to be sent to the cluster are created in TestMessageSet class
 * Stuff in datasource package takes Set<Message>, sends it to the NXCALS (as INTTEST client system) and returns
 * Set<Future<Void>>
 * Stuff in kafka package is responsbile for retrieving messages from Kafka
 * Verifier class has two methods, one for verification whether messages were sent, second for checking if messages
 * retrieved from Kafka match the ones we put there.
 * HsqlGenerator - generates import.sql file for Hibernate, containing definition of INTTEST client service.
 *
 * @author Krzysztof Nawara
 * @date Jul 26, 2016 9:55:08 PM
 */
public class IngestionApiTest {
    private static final int SEND_TIMEOUT = 10000; /* in ms */
    private static final int SEND_POLL_TIMEOUT = SEND_TIMEOUT / (TestMessageSet.MESSAGES_TOTAL * 10);
    private static final int RECEIVE_TIMEOUT = 10000; /* in ms */
    private static final Logger LOGGER = LoggerFactory.getLogger(IngestionApiTest.class);

    @Test
    public void messagesInsertedUsingIngestionApiShouldBeRetrivableFromKafka() throws Exception {
        LOGGER.debug(TestMessageSet.getTestMessagesStringRepresentation());

        Collection<String> topics = Collections.singletonList(InttestDatasource.SYSTEM_NAME);
        try (final InttestKafkaClient client = new InttestKafkaClient(topics)) {
            client.markOffset();
            Set<Future<Void>> results = InttestDatasource
                    .sendMessagesToSystem(TestMessageSet.MESSAGES_SENT_DURING_TEST);
            Verifier.verifyMessagesSentOrTimeout(results, SEND_POLL_TIMEOUT, SEND_TIMEOUT);
            Map<String, Set<ConsumerRecord<byte[], byte[]>>> rawRecords =
                    client.retrieveRecords(TestMessageSet.MESSAGES_TOTAL, RECEIVE_TIMEOUT);
            Verifier.verifyOnlySentMessagesArrived(rawRecords, TestMessageSet.MESSAGES_SENT_DURING_TEST);
        }
    }
}
