/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.inttests.datamodel;

/**
 * @author Krzysztof Nawara
 * @date Jul 26, 2016 9:55:08 PM
 */
public final class Message {
    private final String key;
    private final String partition;
    private final String timestamp;
    private final String data;

    public Message(/* @Nonnnull */ String key,
                   /* @Nonnnull */ String partition,
                   /* @Nonnnull */ String timestamp,
	               /* @Nonnnull */ String data) {
        this.key = key;
        this.partition = partition;
        this.timestamp = timestamp;
        this.data = data;
    }

    public String getKey() {
        return key;
    }

    public String getPartition() {
        return partition;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getData() {
        return data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Message message = (Message) o;

        if (!key.equals(message.key))
            return false;
        if (!partition.equals(message.partition))
            return false;
        if (!timestamp.equals(message.timestamp))
            return false;
        return data.equals(message.data);
    }

    @Override
    public int hashCode() {
        int result = key.hashCode();
        result = 31 * result + partition.hashCode();
        result = 31 * result + timestamp.hashCode();
        result = 31 * result + data.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Message(" + key + ',' + partition + ',' + timestamp + ',' + data + ')';
    }
}
