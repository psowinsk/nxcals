/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.inttests.hsqlgenerator;

import cern.nxcals.inttests.datasource.InttestDatasource;
import cern.nxcals.inttests.datasource.MessageSchema;

/**
 * @author Krzysztof Nawara
 * @date Aug 03, 2016 8:02 AM
 */
public class HsqlGenerator {
    public static final String SQL_TEMPLATE =
            "INSERT INTO systems(system_id, system_name, entity_key_defs, partition_key_defs, time_key_defs, rec_version) "
                    +
                    "VALUES (2,'%s','%s','%s','%s',0);";

    public static void main(String[] args) {
        String sql = String.format(SQL_TEMPLATE,
                InttestDatasource.SYSTEM_NAME,
                MessageSchema.KEY_SCHEMA.toString(),
                MessageSchema.PARTITION_SCHEMA.toString(),
                MessageSchema.TIME_SCHEMA.toString());

        System.out.println(sql);
    }
}
