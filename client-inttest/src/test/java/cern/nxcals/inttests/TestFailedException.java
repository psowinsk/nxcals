/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.inttests;

/**
 * @author Krzysztof Nawara
 * @date Aug 04, 2016 5:37 PM
 */
public class TestFailedException extends RuntimeException {
    public TestFailedException() {
    }

    public TestFailedException(String message) {
        super(message);
    }

    public TestFailedException(String message, Throwable cause) {
        super(message, cause);
    }

    public TestFailedException(Throwable cause) {
        super(cause);
    }

    public TestFailedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
