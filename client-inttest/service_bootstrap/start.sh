#!/usr/bin/env bash

export JAVA_OPTS=$JAVA_OPTS" -Dspring.profiles.active=inttest"
export CLASSPATH="."

if [ ! -z "$PID_DIR" ]; then
 PID_FILE=$PID_DIR/service_pid
else
 PID_FILE=service_pid
fi

nohup bin/nxcals-service 0<&- &> service.log & echo $! > $PID_FILE
