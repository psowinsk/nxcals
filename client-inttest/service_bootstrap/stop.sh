#!/usr/bin/env bash

if [ ! -z "$PID_DIR" ]; then
 PID_FILE=$PID_DIR/service_pid
else
 PID_FILE=service_pid
fi

kill -s 9 `cat $PID_FILE`
