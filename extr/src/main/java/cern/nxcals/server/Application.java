/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.server;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Marcin Sobieszek
 * @date Oct 3, 2016 12:17:53 PM
 */
@SpringBootApplication
public class Application {
    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);
    private final ExecutorService executor = Executors.newFixedThreadPool(10);

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public CommandLineRunner demo(SparkSession context) {
        return args -> {
            //            for (int i = 0; i < 10; i++) {
            //                String fileName = "/user/msobiesz/SkunkClusterPRO.log." + i;
            //                this.executor.submit(() -> {
            //                    LOGGER.info("Submiting task for {}", fileName);
            //                    JavaRDD<String> file = context.textFile(fileName);
            //                    LOGGER.info("File {} count {}", fileName, file.count());
            //                });
            //            }
            Dataset<Row> df = context.read().format("com.databricks.spark.avro")
                    //.load("hdfs://p01001532067275.cern.ch/tmp/cals2/XenericSampler/21060/Samples/2016-02-19/*.avro");
                    //.load("/tmp/cals2/XenericSampler/21060/Samples/*/*.avro");
                    //.load("/tmp/cals2/XTIM/2.0.0/Acquisition/2016-02-22/*.avro");
                    //.load("/tmp/cals2/*/*/*/*/*.avro");
                    //.load("hdfs://p01001532067275.cern.ch/tmp/cals2/XTIM/2.0.0/Acquisition/2016-02-22/merged/part-r-00000-39bc3633-cefd-4e10-8ce6-379a1b640bb6.gz.parquet");
                    //.load("hdfs://p01001532067275.cern.ch/tmp/cals2/merged/data2.parquet");
                    //.load("hdfs://p01001532067275.cern.ch/tmp/cals/MDB1/BFBLHCSettings/3.1.1/Logging/2016-2-29/data-merged-1456865231322.parquet");
                    //.load("/tmp/cals/MDB3/XenericSampler/*/Samples/*/*.parquet" /*,"/tmp/cals/MDB3/PowRs422/3.9.0/Acquisition/2016-03-03/data-merged-1457023280052.parquet"*/);
                    .load("hdfs://p01001532067275.cern.ch/tmp/acclog/staging/TEST-CMW/1/9/3/2016-09-16/*.avro");

            df.printSchema();

            String[] fieldNames = df.schema().fieldNames();

            //sqlContext.udf().register("arrEmpty", (WrappedArray<?> arr) -> arr.isEmpty(), DataTypes.BooleanType);
            //System.out.println("Count " + df.count() + " default partitions: "  + df.toJavaRDD().getNumPartitions());
        /*DataFrame filtered = df.select( df.col("device"), df.col("cyclestamp"), df.col("acqStamp"), df.col("selector"), df.col("samples"))
                .filter("selector != 'LN4.USER.MD1' and size(samples) != 0");*/

            df.show(10);

            df.repartition(1).write().parquet("/tmp/jwozniak/test");

        };
    }

}
