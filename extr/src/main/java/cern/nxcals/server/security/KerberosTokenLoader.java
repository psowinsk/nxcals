/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.server.security;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.security.UserGroupInformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author Marcin Sobieszek
 * @date Mar 11, 2016 11:54:34 AM
 */
@Component("kerberos")
public class KerberosTokenLoader {
    private static final Logger LOGGER = LoggerFactory.getLogger(KerberosTokenLoader.class);
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    @Value(value = "${hadoop.kerberos.principle}")
    private String principle;

    @Value(value = "${hadoop.kerberos.keytabLocation}")
    private String keytabLocation;

    @PostConstruct
    public void init() {
        this.logCredentials();
        this.scheduler.scheduleAtFixedRate(() -> this.logCredentials(), 1, 1, TimeUnit.HOURS);
        LOGGER.info("Started Kerberos re-login daemon");
    }

    @PreDestroy
    public void shutdown() {
        LOGGER.info("Shut down Kerberos re-login daemon");
        this.scheduler.shutdown();
    }

    private void logCredentials() {
        LOGGER.info("Kerberos - sending credentials");
        Configuration conf = new Configuration();
        conf.set("hadoop.security.authentication", "Kerberos");
        UserGroupInformation.setConfiguration(conf);
        try {
            UserGroupInformation.loginUserFromKeytab(this.principle, this.keytabLocation);
            LOGGER.info("Kerberos logged user = {}", UserGroupInformation.getLoginUser());
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.error("Cannot send Kerberos credentials", e);
            throw new UncheckedIOException(e);
        }
    }

    public void setPrinciple(String _principle) {
        this.principle = _principle;
    }

    public void setKeytabLocation(String _keytabLocation) {
        this.keytabLocation = _keytabLocation;
    }

}
