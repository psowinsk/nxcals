/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.server.config;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import java.util.Map;

@Configuration
public class SparkContext {

    @Bean
    @DependsOn("kerberos")
    public JavaSparkContext javaSparkContext() {
        SparkConf conf = getSparkConf();
        return new JavaSparkContext(conf);
    }

    private SparkConf getSparkConf() {
        SparkConfig config = this.sparkConfig();
        SparkConf conf = new SparkConf().setAppName(config.getAppName()).setMaster(config.getMasterType());
        config.getProperties().forEach((k, v) -> conf.set(k, v));
        conf.setJars(config.getJars());
        return conf;
    }

    @Bean
    @DependsOn("kerberos")
    public SparkSession createSparkSession() {
        return SparkSession.builder().config(getSparkConf()).getOrCreate();
    }

    @ConfigurationProperties(prefix = "spark")
    @Bean
    public SparkConfig sparkConfig() {
        return new SparkConfig();
    }

    public static class SparkConfig {
        private String appName;
        private String masterType;
        private String[] jars;
        private Map<String, String> properties;

        public Map<String, String> getProperties() {
            return this.properties;
        }

        public String getAppName() {
            return this.appName;
        }

        public void setAppName(String _appName) {
            this.appName = _appName;
        }

        public String getMasterType() {
            return this.masterType;
        }

        public void setMasterType(String _masterType) {
            this.masterType = _masterType;
        }

        public void setProperties(Map<String, String> _properties) {
            this.properties = _properties;
        }

        public String[] getJars() {
            return this.jars;
        }

        public void setJars(String[] _jars) {
            this.jars = _jars;
        }

    }
}
