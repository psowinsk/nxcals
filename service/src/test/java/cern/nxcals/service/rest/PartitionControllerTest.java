/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest;

import cern.nxcals.service.domain.Partition;
import cern.nxcals.service.domain.System;
import cern.nxcals.service.repository.PartitionRepository;
import cern.nxcals.service.repository.SystemRepository;
import com.google.common.base.Throwables;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Optional;

import static cern.nxcals.common.web.Endpoints.PARTITIONS;
import static cern.nxcals.service.rest.DomainTestConstants.OBJECT_MAPPER;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_DATA;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_ID;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_STRING_1;
import static cern.nxcals.service.rest.DomainTestConstants.SYSTEM_ID;
import static cern.nxcals.service.rest.DomainTestConstants.TEST_MESSAGE;
import static cern.nxcals.service.rest.GlobalControllerExceptionHandler.INTERNAL_ERROR_FORMAT;
import static cern.nxcals.service.rest.GlobalControllerExceptionHandler.NOT_FOUND_ERROR_FORMAT_PREFIX;
import static cern.nxcals.service.rest.PartitionController.ERROR_MESSAGE;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_SCHEMA_1;
import static java.lang.String.format;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class PartitionControllerTest {

    @Mock
    private PartitionRepository partitionRepository;

    @Mock
    private SystemRepository systemRepository;

    @InjectMocks
    private PartitionController partitionController;

    private MockMvc mockMvc;

    @Mock
    private Partition partition;

    @Mock
    private System system;

    @Before
    public void setup() {
        when(partition.toPartitionData()).thenReturn(PARTITION_DATA);
        when(partition.getId()).thenReturn(PARTITION_ID);
        when(partition.getKeyValues()).thenReturn(PARTITION_KEY_VALUES_STRING_1);
        when(systemRepository.findById(SYSTEM_ID)).thenReturn(Optional.of(system));
        when(system.getPartitionKeyDefs()).thenReturn(PARTITION_SCHEMA_1.toString());
        mockMvc = MockMvcBuilders.standaloneSetup(partitionController)
                .setControllerAdvice(new GlobalControllerExceptionHandler())
                .build();
    }

    @Test
    public void shouldGetPartitionBySystemIdAndKeyValues() throws Exception {
        Mockito.when(partitionRepository.findBySystemIdAndKeyValues(eq(SYSTEM_ID), eq(PARTITION_KEY_VALUES_STRING_1)))
                .thenReturn(Optional.of(partition));

        String expectedValue = OBJECT_MAPPER.writeValueAsString(PARTITION_KEY_VALUES_1);
        mockMvc.perform(post(PARTITIONS)
                .param("systemId", String.valueOf(SYSTEM_ID))
                .contentType(MediaType.APPLICATION_JSON)
                .content(expectedValue))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.id").value(PARTITION_ID))
                .andExpect(jsonPath("$.keyValues").value(is(PARTITION_KEY_VALUES_1)));
    }

    @Test
    public void shouldGet404WhenFindBySystemIdFindsNothing() throws Exception {

        Mockito.when(partitionRepository.findBySystemIdAndKeyValues(eq(SYSTEM_ID), eq(PARTITION_KEY_VALUES_STRING_1)))
                .thenReturn(Optional.empty());
        String expectedPrefix = format(NOT_FOUND_ERROR_FORMAT_PREFIX,
                format(ERROR_MESSAGE, SYSTEM_ID, PARTITION_KEY_VALUES_1));
        mockMvc.perform(post(PARTITIONS)
                .param("systemId", String.valueOf(SYSTEM_ID))
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsString(PARTITION_KEY_VALUES_1)))
                .andExpect(status().isNotFound())
                .andExpect(content().string(containsString(expectedPrefix)));
    }

    @Test
    public void shouldGet500WhenFindBySystemIdFailsUnexpectedly() throws Exception {
        RuntimeException runtimeException = new RuntimeException(TEST_MESSAGE);
        Mockito.when(partitionRepository.findBySystemIdAndKeyValues(eq(SYSTEM_ID), eq(PARTITION_KEY_VALUES_STRING_1)))
                .thenThrow(runtimeException);

        mockMvc.perform(post(PARTITIONS)
                .param("systemId", String.valueOf(SYSTEM_ID))
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsString(PARTITION_KEY_VALUES_1)))
                .andExpect(status().isInternalServerError())
                .andExpect(content().string(format(INTERNAL_ERROR_FORMAT, TEST_MESSAGE,
                        Throwables.getStackTraceAsString(runtimeException))));
    }

}
