/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest;

import cern.nxcals.service.domain.System;
import cern.nxcals.service.repository.SystemRepository;
import com.google.common.base.Throwables;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Optional;

import static cern.nxcals.common.web.Endpoints.SYSTEMS;
import static cern.nxcals.service.rest.DomainTestConstants.SYSTEM_DATA;
import static cern.nxcals.service.rest.DomainTestConstants.SYSTEM_ID;
import static cern.nxcals.service.rest.DomainTestConstants.SYSTEM_NAME;
import static cern.nxcals.service.rest.DomainTestConstants.TEST_MESSAGE;
import static cern.nxcals.service.rest.GlobalControllerExceptionHandler.INTERNAL_ERROR_FORMAT;
import static cern.nxcals.service.rest.GlobalControllerExceptionHandler.NOT_FOUND_ERROR_FORMAT;
import static java.lang.String.format;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class SystemControllerTest {

    @Mock
    private SystemRepository systemRepository;

    @InjectMocks
    private SystemController systemController;

    private MockMvc mockMvc;

    @Mock
    private System system;

    @Before
    public void setUp() {
        when(system.toSystemData()).thenReturn(SYSTEM_DATA);
        mockMvc = MockMvcBuilders.standaloneSetup(systemController)
                .setControllerAdvice(new GlobalControllerExceptionHandler())
                .build();
    }

    @Test
    public void shouldGetIdForSystemName() throws Exception {
        when(systemRepository.findByName(eq(SYSTEM_NAME))).thenReturn(Optional.of(system));

        mockMvc.perform(get(SYSTEMS)
                .param("name", SYSTEM_NAME))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(SYSTEM_ID))
                .andExpect(jsonPath("name").value(SYSTEM_NAME));
    }

    @Test
    public void shouldGetIdForSystemId() throws Exception {
        when(systemRepository.findById(eq(SYSTEM_ID))).thenReturn(Optional.of(system));

        mockMvc.perform(get(SYSTEMS + "/{id}", SYSTEM_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(SYSTEM_ID))
                .andExpect(jsonPath("name").value(SYSTEM_NAME));
    }

    @Test
    public void shouldGet404WhenFindsByNameFindsNothing() throws Exception {
        RuntimeException runtimeException =new NotFoundRuntimeException(TEST_MESSAGE);
        when(systemRepository.findByName(eq(SYSTEM_NAME))).thenThrow(runtimeException);

        mockMvc.perform(get(SYSTEMS)
                .param("name", SYSTEM_NAME))
                .andExpect(status().isNotFound())
                .andExpect(content().string(format(NOT_FOUND_ERROR_FORMAT, TEST_MESSAGE,Throwables.getStackTraceAsString(runtimeException))));
    }

    @Test
    public void shouldGet500WhenFindsByNameFailsUnexpectedly() throws Exception {
        RuntimeException runtimeException = new RuntimeException(TEST_MESSAGE);
        when(systemRepository.findByName(eq(SYSTEM_NAME))).thenThrow(runtimeException);

        mockMvc.perform(get(SYSTEMS)
                .param("name", SYSTEM_NAME))
                .andExpect(status().isInternalServerError())
                .andExpect(content().string(format(INTERNAL_ERROR_FORMAT, TEST_MESSAGE,
                        Throwables.getStackTraceAsString(runtimeException))));
    }

    @Test
    public void shouldGet404WhenFindsBySystemIdFindsNothing() throws Exception {
        RuntimeException  runtimeException = new NotFoundRuntimeException(TEST_MESSAGE);
        when(systemRepository.findById(eq(SYSTEM_ID))).thenThrow(runtimeException);

        mockMvc.perform(get(SYSTEMS + "/{id}", SYSTEM_ID))
                .andExpect(status().isNotFound())
                .andExpect(content().string(format(NOT_FOUND_ERROR_FORMAT, TEST_MESSAGE,Throwables.getStackTraceAsString(runtimeException))));
    }

    @Test
    public void shouldGet500WhenFindsByIdFailsUnexpectedly() throws Exception {
        RuntimeException runtimeException = new RuntimeException(TEST_MESSAGE);
        when(systemRepository.findById(eq(SYSTEM_ID))).thenThrow(runtimeException);

        mockMvc.perform(get(SYSTEMS + "/{id}", SYSTEM_ID))
                .andExpect(status().isInternalServerError())
                .andExpect(content().string(format(INTERNAL_ERROR_FORMAT, TEST_MESSAGE,
                        Throwables.getStackTraceAsString(runtimeException))));
    }
}
