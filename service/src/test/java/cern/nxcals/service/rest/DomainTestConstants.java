/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest;

import cern.nxcals.common.domain.FindOrCreateEntityRequest;
import cern.nxcals.common.domain.PartitionData;
import cern.nxcals.common.domain.SchemaData;
import cern.nxcals.common.domain.SystemData;
import cern.nxcals.common.domain.VariableConfigData;
import cern.nxcals.common.domain.VariableData;
import cern.nxcals.common.utils.KeyValuesUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;

import java.util.Collections;
import java.util.Map;

import static cern.nxcals.common.utils.KeyValuesUtils.convertMapIntoAvroSchemaString;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_DOUBLE_SCHEMA_KEY_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_DOUBLE_SCHEMA_KEY_2;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_STRING_SCHEMA_KEY_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_STRING_SCHEMA_KEY_2;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_DOUBLE_SCHEMA_KEY_1;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_DOUBLE_SCHEMA_KEY_2;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_SCHEMA_2;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_STRING_SCHEMA_KEY_1;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_STRING_SCHEMA_KEY_2;
import static cern.nxcals.service.rest.TestSchemas.RECORD_VERSION_SCHEMA;
import static cern.nxcals.service.rest.TestSchemas.TIME_SCHEMA;

public class DomainTestConstants {

    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    public static final long SYSTEM_ID = 10;
    public static final String SYSTEM_NAME = "SYSTEM_NAME";
    public static final String TEST_REGEX = "TEST_REGEX";
    public static final String TEST_MESSAGE = "TEST_MESSAGE";
    public static final long TIMESTAMP = 1;

    public static final long PARTITION_ID = 20;

    public static final Map<String, Object> PARTITION_KEY_VALUES_1 = ImmutableMap
            .of(PARTITION_STRING_SCHEMA_KEY_1, "string_1", PARTITION_DOUBLE_SCHEMA_KEY_1, 1d);

    public static final String PARTITION_KEY_VALUES_STRING_1 = convertMapIntoAvroSchemaString(PARTITION_KEY_VALUES_1,
            PARTITION_SCHEMA_1.toString());

    public static final Map<String, Object> PARTITION_KEY_VALUES_2 = ImmutableMap
            .of(PARTITION_STRING_SCHEMA_KEY_2, "string_2", PARTITION_DOUBLE_SCHEMA_KEY_2, 2d);

    public static final String PARTITION_KEY_VALUES_STRING_2 = convertMapIntoAvroSchemaString(PARTITION_KEY_VALUES_2,
            PARTITION_SCHEMA_2.toString());

    public static final PartitionData PARTITION_DATA = PartitionData.builder().id(PARTITION_ID)
            .keyValues(PARTITION_KEY_VALUES_1).build();

    public static final Map<String, Object> ENTITY_KEY_VALUES_1 = ImmutableMap
            .of(ENTITY_STRING_SCHEMA_KEY_1, "string", ENTITY_DOUBLE_SCHEMA_KEY_1, 1d);

    public static final Map<String, Object> ENTITY_KEY_VALUES_2 = ImmutableMap
            .of(ENTITY_STRING_SCHEMA_KEY_2, "string", ENTITY_DOUBLE_SCHEMA_KEY_2, 1d);

    public static final String ENTITY_KEY_VALUES_JSON = KeyValuesUtils
            .convertMapIntoAvroSchemaString(ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1.toString());
    public static final String PARTITION_KEY_VALUES_JSON_1 = KeyValuesUtils
            .convertMapIntoAvroSchemaString(PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1.toString());

    public static final String SCHEMA_VALUE = "SCHEMA_VALUE";
    public static final long ENTITY_ID = 40;
    public static final String KEY_VALUES = "KEY_VALUES";
    public static final long SCHEMA_ID = 30;
    public static final SchemaData SCHEMA_DATA = SchemaData.builder().id(SCHEMA_ID).schema(SCHEMA_VALUE).build();
    public static final String VARIABLE_NAME = "VARIABLE_NAME";
    public static final String VARIABLE_DESCRIPTION = "VARIABLE_DESCRIPTION";
    public static final String VARIABLE_CONFIG_FIELD_NAME = "VARIABLE_CONFIG_FIELD_NAME";
    public static final VariableConfigData VARIABLE_CONFIG_DATA = VariableConfigData.builder().entityId(ENTITY_ID)
            .fieldName(VARIABLE_CONFIG_FIELD_NAME).validFromStamp(TIMESTAMP).validToStamp(TIMESTAMP).build();
    public static final VariableData VARIABLE_DATA = VariableData.builder().name(VARIABLE_NAME)
            .description(VARIABLE_DESCRIPTION)
            .creationTimeUtc(TIMESTAMP)
            .variableConfigData(Sets.newTreeSet(Collections.singletonList(VARIABLE_CONFIG_DATA))).build();

    public static final SystemData SYSTEM_DATA = SystemData.builder().id(SYSTEM_ID).name(SYSTEM_NAME)
            .entityKeyDefinitions(ENTITY_SCHEMA_1.toString()).partitionKeyDefinitions(PARTITION_SCHEMA_1.toString())
            .timeKeyDefinitions(TIME_SCHEMA.toString())
            .recordVersionKeyDefinitions(RECORD_VERSION_SCHEMA.toString()).build();

    static String FIND_OR_CREATE_ENTITY_REQUEST_JSON;
    static String VARIABLE_DATA_JSON;

    static {
        try {
            FIND_OR_CREATE_ENTITY_REQUEST_JSON =
                    OBJECT_MAPPER.writeValueAsString(FindOrCreateEntityRequest.builder()
                            .entityKeyValues(ENTITY_KEY_VALUES_1)
                            .partitionKeyValues(PARTITION_KEY_VALUES_1)
                            .schema(ENTITY_SCHEMA_1.toString())
                            .build());

            VARIABLE_DATA_JSON = OBJECT_MAPPER.writeValueAsString(VARIABLE_DATA);
        } catch (Exception exception) {
            throw new RuntimeException("Error initializing necessary variables for testing.", exception);
        }
    }
}
