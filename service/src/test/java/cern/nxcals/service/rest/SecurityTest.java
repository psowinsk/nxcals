/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest;

import cern.nxcals.common.domain.EntityData;
import cern.nxcals.common.domain.FindOrCreateEntityRequest;
import cern.nxcals.common.domain.SystemData;
import cern.nxcals.common.domain.VariableData;
import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.Entity;
import cern.nxcals.service.domain.Partition;
import cern.nxcals.service.domain.System;
import cern.nxcals.service.domain.Variable;
import cern.nxcals.service.internal.InternalEntityService;
import cern.nxcals.service.internal.InternalVariableService;
import cern.nxcals.service.repository.EntityRepository;
import cern.nxcals.service.repository.SystemRepository;
import org.assertj.core.util.Lists;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.internal.matchers.Contains;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static cern.nxcals.common.web.Endpoints.ENTITIES;
import static cern.nxcals.common.web.Endpoints.ENTITY_EXTEND_FIRST_HISTORY;
import static cern.nxcals.common.web.Endpoints.ENTITY_UPDATE;
import static cern.nxcals.common.web.Endpoints.VARIABLE_REGISTER_OR_UPDATE;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_ID;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.OBJECT_MAPPER;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_DATA;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.SCHEMA_DATA;
import static cern.nxcals.service.rest.DomainTestConstants.SYSTEM_ID;
import static cern.nxcals.service.rest.DomainTestConstants.TIMESTAMP;
import static cern.nxcals.service.rest.DomainTestConstants.VARIABLE_DATA;
import static cern.nxcals.service.rest.EntityControllerTest.ENTITY_DATA;
import static cern.nxcals.service.rest.GlobalControllerExceptionHandler.FORBIDDEN_ERROR_FORMAT_PREFIX;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_1;
import static java.lang.String.format;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test suite that checks if the endpoints defined in {@link EntityController} follow the desired contract.
 */
@WebAppConfiguration
@Transactional(transactionManager = "jpaTransactionManager")
public class SecurityTest extends BaseTest {

    private static final SystemData SYSTEM_DATA2 = SystemData.builder().id(20).name(SYSTEM_NAME2)
            .entityKeyDefinitions(ENTITY_SCHEMA_1.toString()).partitionKeyDefinitions(ENTITY_SCHEMA_1.toString())
            .timeKeyDefinitions(ENTITY_SCHEMA_1.toString()).recordVersionKeyDefinitions(ENTITY_SCHEMA_1.toString())
            .build();

    static final EntityData ENTITY_DATA2 = EntityData.builder(20, ENTITY_KEY_VALUES_1, SYSTEM_DATA2,
            PARTITION_DATA, SCHEMA_DATA, Collections.emptySortedSet(), null, 1).build();

    @MockBean
    private SystemRepository systemRepository;

    @MockBean
    private EntityRepository entityRepository;

    @MockBean
    private InternalEntityService internalEntityService;

    @MockBean
    private InternalVariableService internalVariableService;

    @Mock
    private Entity entity;

    @Mock
    private Partition partition;

    @Mock
    private System system;

    @Mock
    private Variable variable;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private final static Matcher<String> ERROR_MESSAGE_CONTENT = new Contains(format(FORBIDDEN_ERROR_FORMAT_PREFIX, "Access is denied"));

    @Before
    public void setup() {
        when(entity.getId()).thenReturn(ENTITY_ID);

        when(internalEntityService.findOrCreateEntityFor(isA(Long.class), isA(Map.class), isA(Map.class),
                isA(String.class), isA(Long.class))).thenReturn(entity);

        when(internalEntityService.extendEntityFirstHistoryDataFor(isA(Long.class), isA(String.class), isA(Long.class)))
                .thenReturn(entity);

        when(internalVariableService.registerOrUpdateVariableFor(any(VariableData.class))).thenReturn(variable);

        when(system.getName()).thenReturn(SYSTEM_NAME);

        when(partition.getSystem()).thenReturn(system);

        when(entity.getPartition()).thenReturn(partition);

        when(systemRepository.findById(eq(SYSTEM_ID))).thenReturn(Optional.of(system));

        when(entityRepository.findById(eq(ENTITY_ID))).thenReturn(Optional.of(entity));

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }

    @Test
    @WithMockUser(authorities = BaseTest.AUTHORITY)
    public void shouldFindOrCreateEntity() throws Exception {
        mockMvc.perform(put(ENTITIES)
                .param("systemId", String.valueOf(SYSTEM_ID))
                .param("recordTimestamp", String.valueOf(TIMESTAMP))
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsString(FindOrCreateEntityRequest.builder()
                        .entityKeyValues(ENTITY_KEY_VALUES_1)
                        .partitionKeyValues(PARTITION_KEY_VALUES_1)
                        .schema(ENTITY_SCHEMA_1.toString()).build())))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = "SOME_AUTHORITY")
    public void shouldNotAuthorizeToFindOrCreateEntityFor() throws Exception {
        mockMvc.perform(put(ENTITIES)
                .param("systemId", String.valueOf(SYSTEM_ID))
                .param("recordTimestamp", String.valueOf(TIMESTAMP))
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsString(FindOrCreateEntityRequest.builder()
                        .entityKeyValues(ENTITY_KEY_VALUES_1)
                        .partitionKeyValues(PARTITION_KEY_VALUES_1)
                        .schema(ENTITY_SCHEMA_1.toString()).build())))
                .andExpect(status().isForbidden())
                .andExpect(content().string(ERROR_MESSAGE_CONTENT));
    }

    @Test
    @WithMockUser(authorities = BaseTest.VARIABLE_AUTHORITY)
    public void shouldRegisterOrUpdateVariable() throws Exception {
        mockMvc.perform(put(VARIABLE_REGISTER_OR_UPDATE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsString(VARIABLE_DATA)))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = "SOME_AUTHORITY")
    public void shouldNotAuthorizeToRegisterOrUpdateVariableFor() throws Exception {
        mockMvc.perform(put(VARIABLE_REGISTER_OR_UPDATE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsString(VARIABLE_DATA)))
                .andExpect(status().isForbidden())
                .andExpect(content().string(ERROR_MESSAGE_CONTENT));
    }

    @Test
    @WithMockUser(authorities = { BaseTest.AUTHORITY, BaseTest.AUTHORITY2 })
    public void shouldAuthorizeToUpdateEntities() throws Exception {
        List<Entity> entityList = Collections.singletonList(entity);
        List<EntityData> entityDataList = Lists.newArrayList();
        entityDataList.add(ENTITY_DATA);
        entityDataList.add(ENTITY_DATA2);

        when(internalEntityService.updateEntities(eq(entityDataList))).thenReturn(entityList);

        mockMvc.perform(put(ENTITY_UPDATE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsString(entityDataList)))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = { BaseTest.AUTHORITY, BaseTest.AUTHORITY2 })
    public void shouldAuthorizeToUpdateEntitiesWhenMoreAuthoritiesThanNecessary() throws Exception {
        List<Entity> entityList = Collections.singletonList(entity);
        List<EntityData> entityDataList = Lists.newArrayList();
        entityDataList.add(ENTITY_DATA);

        when(internalEntityService.updateEntities(eq(entityDataList))).thenReturn(entityList);

        mockMvc.perform(put(ENTITY_UPDATE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsString(entityDataList)))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = { BaseTest.AUTHORITY })
    public void shouldNotAuthorizeToUpdateEntities() throws Exception {
        List<Entity> entityList = Collections.singletonList(entity);
        List<EntityData> entityDataList = Lists.newArrayList();
        entityDataList.add(ENTITY_DATA);
        entityDataList.add(ENTITY_DATA2);

        when(internalEntityService.updateEntities(eq(entityDataList))).thenReturn(entityList);

        mockMvc.perform(put(ENTITY_UPDATE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsString(entityDataList)))
                .andExpect(content().string(ERROR_MESSAGE_CONTENT))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(authorities = BaseTest.AUTHORITY)
    public void shouldExtendEntityFirstHistoryDataFor() throws Exception {
        mockMvc.perform(put(ENTITY_EXTEND_FIRST_HISTORY)
                .param("entityId", String.valueOf(ENTITY_ID))
                .param("from", String.valueOf(TIMESTAMP))
                .contentType(MediaType.APPLICATION_JSON)
                .content(ENTITY_SCHEMA_1.toString()))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = "SOME_AUTHORITY")
    public void shouldNotAuthorizeToExtendEntityFirstHistoryDataFor() throws Exception {
        this.mockMvc
                .perform(put(ENTITY_EXTEND_FIRST_HISTORY)
                        .param("entityId", String.valueOf(ENTITY_ID))
                        .param("from", String.valueOf(TIMESTAMP))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(ENTITY_SCHEMA_1.toString()))
                .andExpect(status().isForbidden())
                .andExpect(content().string(ERROR_MESSAGE_CONTENT));
    }
}