/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest;

import cern.nxcals.service.domain.Variable;
import cern.nxcals.service.internal.InternalVariableService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;

import static cern.nxcals.common.web.Endpoints.VARIABLES;
import static cern.nxcals.service.rest.DomainTestConstants.TEST_MESSAGE;
import static cern.nxcals.service.rest.DomainTestConstants.TEST_REGEX;
import static cern.nxcals.service.rest.DomainTestConstants.TIMESTAMP;
import static cern.nxcals.service.rest.DomainTestConstants.VARIABLE_DATA;
import static cern.nxcals.service.rest.DomainTestConstants.VARIABLE_DESCRIPTION;
import static cern.nxcals.service.rest.DomainTestConstants.VARIABLE_NAME;
import static cern.nxcals.service.rest.GlobalControllerExceptionHandler.INTERNAL_ERROR_FORMAT;
import static cern.nxcals.service.rest.GlobalControllerExceptionHandler.NOT_FOUND_ERROR_FORMAT;
import static com.google.common.base.Throwables.getStackTraceAsString;
import static java.lang.String.format;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class VariableControllerTest {

    @Mock
    private InternalVariableService variableService;

    @InjectMocks
    private VariableController variableController;

    private MockMvc mockMvc;

    @Mock
    private Variable variable;

    @Before
    public void setUp() {
        when(variable.toVariableData()).thenReturn(VARIABLE_DATA);
        mockMvc = MockMvcBuilders.standaloneSetup(variableController)
                .setControllerAdvice(new GlobalControllerExceptionHandler())
                .build();
    }

    @Test
    public void shouldGetVariableByName() throws Exception {
        when(variableService.findByVariableName(eq(VARIABLE_NAME))).thenReturn(variable);

        mockMvc.perform(get(VARIABLES)
                .param("name", VARIABLE_NAME))
                .andExpect(status().isOk())
                .andExpect(jsonPath("variableName").value(VARIABLE_NAME))
                .andExpect(jsonPath("description").value(VARIABLE_DESCRIPTION))
                .andExpect(jsonPath("creationTimeUtc").value(TIMESTAMP));
    }

    @Test
    public void shouldGet500WhenFindVariableByNameFailsUnexpectedly() throws Exception {
        RuntimeException runtimeException = new RuntimeException(TEST_MESSAGE);
        when(variableService.findByVariableName(eq(VARIABLE_NAME))).thenThrow(runtimeException);

        mockMvc.perform(get(VARIABLES)
                .param("name", VARIABLE_NAME))
                .andExpect(status().isInternalServerError())
                .andExpect(content().string(format(INTERNAL_ERROR_FORMAT, TEST_MESSAGE,
                        getStackTraceAsString(runtimeException))));
    }

    @Test
    public void shouldGet404WhenFindVariableByNameFindsNothing() throws Exception {
        RuntimeException runtimeException = new NotFoundRuntimeException(TEST_MESSAGE);
        when(variableService.findByVariableName(eq(VARIABLE_NAME)))
                .thenThrow(runtimeException);

        mockMvc.perform(get(VARIABLES)
                .param("name", VARIABLE_NAME))
                .andExpect(status().isNotFound())
                .andExpect(content().string(format(NOT_FOUND_ERROR_FORMAT, TEST_MESSAGE,getStackTraceAsString(runtimeException))));
    }

    @Test
    public void shouldGetVariableByNameAndTimeWindow() throws Exception {
        when(variableService.findByVariableNameAndTimeWindow(eq(VARIABLE_NAME), eq(TIMESTAMP), eq(TIMESTAMP)))
                .thenReturn(variable);

        mockMvc.perform(get(VARIABLES)
                .param("name", VARIABLE_NAME)
                .param("startTime", String.valueOf(TIMESTAMP))
                .param("endTime", String.valueOf(TIMESTAMP)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("variableName").value(VARIABLE_NAME))
                .andExpect(jsonPath("description").value(VARIABLE_DESCRIPTION))
                .andExpect(jsonPath("creationTimeUtc").value(TIMESTAMP));
    }

    @Test
    public void shouldGet500WhenFindVariableByNameAndTimeWindowFailsUnexpectedly() throws Exception {
        RuntimeException runtimeException = new RuntimeException(TEST_MESSAGE);
        when(variableService.findByVariableNameAndTimeWindow(eq(VARIABLE_NAME), eq(TIMESTAMP), eq(TIMESTAMP)))
                .thenThrow(runtimeException);

        mockMvc.perform(get(VARIABLES)
                .param("name", VARIABLE_NAME)
                .param("startTime", String.valueOf(TIMESTAMP))
                .param("endTime", String.valueOf(TIMESTAMP)))
                .andExpect(status().isInternalServerError())
                .andExpect(content().string(format(INTERNAL_ERROR_FORMAT, TEST_MESSAGE,
                        getStackTraceAsString(runtimeException))));
    }

    @Test
    public void shouldGet404WhenFindVariableByNameAndTimeWindowFindsNothing() throws Exception {
        RuntimeException runtimeException = new NotFoundRuntimeException(TEST_MESSAGE);
        when(variableService.findByVariableNameAndTimeWindow(eq(VARIABLE_NAME), eq(TIMESTAMP), eq(TIMESTAMP)))
                .thenThrow(runtimeException);

        mockMvc.perform(get(VARIABLES)
                .param("name", VARIABLE_NAME)
                .param("startTime", String.valueOf(TIMESTAMP))
                .param("endTime", String.valueOf(TIMESTAMP)))
                .andExpect(status().isNotFound())
                .andExpect(content().string(format(NOT_FOUND_ERROR_FORMAT, TEST_MESSAGE, getStackTraceAsString(runtimeException))));
    }

    @Test
    public void shouldFindVariableByRegexName() throws Exception {
        when(variableService.findByNameLike(eq(TEST_REGEX))).thenReturn(Collections.singletonList(variable));

        mockMvc.perform(get(VARIABLES)
                .param("nameExpression", TEST_REGEX))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].variableName").value(VARIABLE_NAME))
                .andExpect(jsonPath("$[0].description").value(VARIABLE_DESCRIPTION))
                .andExpect(jsonPath("$[0].creationTimeUtc").value(TIMESTAMP));
    }

    @Test
    public void shouldGetEmptyListWhenFindByVariableByRegexNameFindsNothing() throws Exception {
        when(variableService.findByNameLike(eq(TEST_REGEX))).thenReturn(Collections.emptyList());

        mockMvc.perform(get(VARIABLES)
                .param("nameExpression", TEST_REGEX))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void shouldGet500WhenFindVariableByRegexNameFailsUnexpectedly() throws Exception {
        RuntimeException runtimeException = new RuntimeException(TEST_MESSAGE);
        when(variableService.findByNameLike(eq(TEST_REGEX)))
                .thenThrow(runtimeException);

        mockMvc.perform(get(VARIABLES)
                .param("nameExpression", TEST_REGEX))
                .andExpect(status().isInternalServerError())
                .andExpect(content().string(format(INTERNAL_ERROR_FORMAT, TEST_MESSAGE,
                        getStackTraceAsString(runtimeException))));
    }

    @Test
    public void shouldGetVariableByDescriptionRegex() throws Exception {
        when(variableService.findByDescriptionLike(eq(TEST_REGEX))).thenReturn(Collections.singletonList(variable));

        mockMvc.perform(get(VARIABLES)
                .param("descriptionExpression", TEST_REGEX))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].variableName").value(VARIABLE_NAME))
                .andExpect(jsonPath("$[0].description").value(VARIABLE_DESCRIPTION))
                .andExpect(jsonPath("$[0].creationTimeUtc").value(TIMESTAMP));
    }

    @Test
    public void shouldGetEmptyListWhenFindByVariableByDescriptionRegexFindsNothing() throws Exception {
        when(variableService.findByDescriptionLike(eq(TEST_REGEX))).thenReturn(Collections.emptyList());

        mockMvc.perform(get(VARIABLES)
                .param("descriptionExpression", TEST_REGEX))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void shouldGet500WhenFindVariableByRegexDescriptionFailsUnexpectedly() throws Exception {
        RuntimeException runtimeException = new RuntimeException(TEST_MESSAGE);
        when(variableService.findByDescriptionLike(eq(TEST_REGEX)))
                .thenThrow(runtimeException);

        mockMvc.perform(get(VARIABLES)
                .param("descriptionExpression", TEST_REGEX))
                .andExpect(status().isInternalServerError())
                .andExpect(content().string(format(INTERNAL_ERROR_FORMAT, TEST_MESSAGE,
                        getStackTraceAsString(runtimeException))));
    }
}
