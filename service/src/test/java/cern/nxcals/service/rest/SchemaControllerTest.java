/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest;

import cern.nxcals.service.domain.Schema;
import cern.nxcals.service.repository.SchemaRepository;
import com.google.common.base.Throwables;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Optional;

import static cern.nxcals.common.web.Endpoints.SCHEMA_WITH_ID;
import static cern.nxcals.service.rest.DomainTestConstants.SCHEMA_DATA;
import static cern.nxcals.service.rest.DomainTestConstants.SCHEMA_ID;
import static cern.nxcals.service.rest.DomainTestConstants.SCHEMA_VALUE;
import static cern.nxcals.service.rest.DomainTestConstants.TEST_MESSAGE;
import static cern.nxcals.service.rest.GlobalControllerExceptionHandler.INTERNAL_ERROR_FORMAT;
import static cern.nxcals.service.rest.GlobalControllerExceptionHandler.NOT_FOUND_ERROR_FORMAT;
import static java.lang.String.format;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class SchemaControllerTest {

    @Mock
    private SchemaRepository schemaRepository;

    @InjectMocks
    private SchemaController schemaController;

    private MockMvc mockMvc;

    @Mock
    private Schema schema;

    @Before
    public void setUp() {
        when(schema.toSchemaData()).thenReturn(SCHEMA_DATA);
        mockMvc = MockMvcBuilders.standaloneSetup(schemaController)
                .setControllerAdvice(new GlobalControllerExceptionHandler())
                .build();
    }

    @Test
    public void shouldGetSchemaById() throws Exception {
        when(schemaRepository.findById(eq(SCHEMA_ID))).thenReturn(Optional.of(schema));

        mockMvc.perform(get(SCHEMA_WITH_ID, SCHEMA_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(SCHEMA_ID))
                .andExpect(jsonPath("schemaJson").value(SCHEMA_VALUE));
    }

    @Test
    public void shouldGet404WhenFindByIdFindsNothing() throws Exception {
        RuntimeException runtimeException =new NotFoundRuntimeException(TEST_MESSAGE);
        when(schemaRepository.findById(eq(SCHEMA_ID))).thenThrow(runtimeException);

        mockMvc.perform(get(SCHEMA_WITH_ID, SCHEMA_ID))
                .andExpect(status().isNotFound())
                .andExpect(content().string(format(NOT_FOUND_ERROR_FORMAT, TEST_MESSAGE,Throwables.getStackTraceAsString(runtimeException))));
    }

    @Test
    public void shouldGet500WhenFindByIdFailsUnexpectedly() throws Exception {
        RuntimeException runtimeException = new RuntimeException(TEST_MESSAGE);
        when(schemaRepository.findById(eq(SCHEMA_ID))).thenThrow(runtimeException);

        mockMvc.perform(get(SCHEMA_WITH_ID, SCHEMA_ID))
                .andExpect(status().isInternalServerError())
                .andExpect(content().string(format(INTERNAL_ERROR_FORMAT, TEST_MESSAGE,
                        Throwables.getStackTraceAsString(runtimeException))));
    }
}
