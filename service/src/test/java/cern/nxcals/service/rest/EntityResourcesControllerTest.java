/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest;

import cern.nxcals.common.domain.EntityResources;
import cern.nxcals.common.domain.ResourceData;
import cern.nxcals.service.internal.InternalEntityResourcesService;
import com.google.common.base.Throwables;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.net.URI;

import static cern.nxcals.common.web.Endpoints.RESOURCES;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_ID;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.KEY_VALUES;
import static cern.nxcals.service.rest.DomainTestConstants.OBJECT_MAPPER;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_DATA;
import static cern.nxcals.service.rest.DomainTestConstants.SCHEMA_DATA;
import static cern.nxcals.service.rest.DomainTestConstants.SYSTEM_DATA;
import static cern.nxcals.service.rest.DomainTestConstants.SYSTEM_ID;
import static cern.nxcals.service.rest.DomainTestConstants.TEST_MESSAGE;
import static cern.nxcals.service.rest.DomainTestConstants.TIMESTAMP;
import static cern.nxcals.service.rest.GlobalControllerExceptionHandler.INTERNAL_ERROR_FORMAT;
import static cern.nxcals.service.rest.GlobalControllerExceptionHandler.NOT_FOUND_ERROR_FORMAT;
import static com.google.common.collect.Sets.newHashSet;
import static java.lang.String.format;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class EntityResourcesControllerTest {

    @Mock
    private InternalEntityResourcesService internalEntitiesResourceService;

    @InjectMocks
    private EntityResourcesController entityResourcesController;

    private MockMvc mockMvc;

    private static URI HDFS_PATH;

    static {
        try {
            HDFS_PATH = new URI("HDFS_PATH");
        } catch (Exception e) {
            throw new RuntimeException("Cannot initialize HDFS_PATH", e);
        }
    }

    private static final String HDFS_TABLE_NAME = "TABLE_NAME";
    private static final ResourceData RESOURCE_DATA = ResourceData.builder().hdfsPaths(newHashSet(HDFS_PATH))
            .hbaseTableNames(newHashSet(HDFS_TABLE_NAME)).build();
    private static final EntityResources ENTITY_RESOURCES = EntityResources.builder().id(SYSTEM_ID)
            .entityKeyValues(KEY_VALUES).systemData(SYSTEM_DATA)
            .partitionData(PARTITION_DATA).schemaData(SCHEMA_DATA).resourceData(RESOURCE_DATA).build();

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(entityResourcesController)
                .setControllerAdvice(new GlobalControllerExceptionHandler())
                .build();
    }

    @Test
    public void shouldFindBySystemIdKeyValuesAndTimeWindow() throws Exception {
        when(internalEntitiesResourceService
                .findBySystemIdKeyValuesAndTimeWindow(eq(SYSTEM_ID), eq(ENTITY_KEY_VALUES_1), eq(TIMESTAMP),
                        eq(TIMESTAMP)))
                .thenReturn(newHashSet(ENTITY_RESOURCES));

        mockMvc.perform(post(RESOURCES, SYSTEM_ID, TIMESTAMP, TIMESTAMP)
                .param("systemId", String.valueOf(SYSTEM_ID))
                .param("startTime", String.valueOf(TIMESTAMP))
                .param("endTime", String.valueOf(TIMESTAMP))
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsString(ENTITY_KEY_VALUES_1)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(ENTITY_RESOURCES.getId()))
                .andExpect(jsonPath("$[0].entityKeyValues").value(KEY_VALUES))
                .andExpect(jsonPath("$[0].systemData.id").value(SYSTEM_DATA.getId()))
                .andExpect(jsonPath("$[0].partitionData.id").value(PARTITION_DATA.getId()))
                .andExpect(jsonPath("$[0].schemaData.id").value(SCHEMA_DATA.getId()))
                .andExpect(jsonPath("$[0].resourcesData.hdfsPaths[0]").value(HDFS_PATH.toString()))
                .andExpect(jsonPath("$[0].resourcesData.hbaseTableNames[0]").value(HDFS_TABLE_NAME));
    }

    @Test
    public void shouldFindByEntityIdAndTimeWindow() throws Exception {
        when(internalEntitiesResourceService.findByEntityIdAndTimeWindow(eq(ENTITY_ID), eq(TIMESTAMP), eq(TIMESTAMP)))
                .thenReturn(newHashSet(ENTITY_RESOURCES));

        mockMvc.perform(get(RESOURCES, ENTITY_ID, TIMESTAMP, TIMESTAMP)
                .param("entityId", String.valueOf(ENTITY_ID))
                .param("startTime", String.valueOf(TIMESTAMP))
                .param("endTime", String.valueOf(TIMESTAMP)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(ENTITY_RESOURCES.getId()))
                .andExpect(jsonPath("$[0].entityKeyValues").value(KEY_VALUES))
                .andExpect(jsonPath("$[0].systemData.id").value(SYSTEM_DATA.getId()))
                .andExpect(jsonPath("$[0].partitionData.id").value(PARTITION_DATA.getId()))
                .andExpect(jsonPath("$[0].schemaData.id").value(SCHEMA_DATA.getId()))
                .andExpect(jsonPath("$[0].resourcesData.hdfsPaths[0]").value(HDFS_PATH.toString()))
                .andExpect(jsonPath("$[0].resourcesData.hbaseTableNames[0]").value(HDFS_TABLE_NAME));
    }

    @Test
    public void shouldGet404WhenFindByEntityIdAndTimeWindowFindsNothing() throws Exception {
        RuntimeException runtimeException = new NotFoundRuntimeException(TEST_MESSAGE);
        when(internalEntitiesResourceService.findByEntityIdAndTimeWindow(eq(ENTITY_ID), eq(TIMESTAMP), eq(TIMESTAMP)))
                .thenThrow(runtimeException);

        mockMvc.perform(get(RESOURCES, SYSTEM_ID, TIMESTAMP, TIMESTAMP)
                .param("entityId", String.valueOf(ENTITY_ID))
                .param("startTime", String.valueOf(TIMESTAMP))
                .param("endTime", String.valueOf(TIMESTAMP)))
                .andExpect(status().isNotFound())
                .andExpect(content().string(format(NOT_FOUND_ERROR_FORMAT, TEST_MESSAGE,Throwables.getStackTraceAsString(runtimeException))));
    }

    @Test
    public void shouldGet404WhenFindBySystemIdKyValuesAndTimeWindowFindsNothing() throws Exception {
        RuntimeException runtimeException = new NotFoundRuntimeException(TEST_MESSAGE);
        when(internalEntitiesResourceService
                .findBySystemIdKeyValuesAndTimeWindow(isA(Long.class), anyMapOf(String.class, Object.class),
                        isA(Long.class), isA(Long.class)))
                .thenThrow(runtimeException);

        mockMvc.perform(post(RESOURCES, SYSTEM_ID, TIMESTAMP, TIMESTAMP)
                .param("systemId", String.valueOf(SYSTEM_ID))
                .param("startTime", String.valueOf(TIMESTAMP))
                .param("endTime", String.valueOf(TIMESTAMP))
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsString(ENTITY_KEY_VALUES_1)))
                .andExpect(status().isNotFound())
                .andExpect(content().string(format(NOT_FOUND_ERROR_FORMAT, TEST_MESSAGE,Throwables.getStackTraceAsString(runtimeException))));
    }

    @Test
    public void shouldGet500WhenFindByEntityIdAndTimeWindowFailsUnexpectedly() throws Exception {
        RuntimeException runtimeException = new RuntimeException(TEST_MESSAGE);
        when(internalEntitiesResourceService
                .findByEntityIdAndTimeWindow(isA(Long.class), isA(Long.class), isA(Long.class)))
                .thenThrow(runtimeException);

        mockMvc.perform(get(RESOURCES, SYSTEM_ID, TIMESTAMP, TIMESTAMP)
                .param("entityId", String.valueOf(ENTITY_ID))
                .param("startTime", String.valueOf(TIMESTAMP))
                .param("endTime", String.valueOf(TIMESTAMP)))
                .andExpect(status().isInternalServerError())
                .andExpect(content().string(format(INTERNAL_ERROR_FORMAT, TEST_MESSAGE, Throwables.getStackTraceAsString(runtimeException)
                        )));
    }

    @Test
    public void shouldGet500WhenFindBySystemIdKyValuesAndTimeWindowFailsUnexpectedly() throws Exception {
        RuntimeException runtimeException = new RuntimeException(TEST_MESSAGE);
        when(internalEntitiesResourceService
                .findBySystemIdKeyValuesAndTimeWindow(isA(Long.class), anyMapOf(String.class, Object.class),
                        isA(Long.class), isA(Long.class)))
                .thenThrow(runtimeException);

        mockMvc.perform(post(RESOURCES, SYSTEM_ID, TIMESTAMP, TIMESTAMP)
                .param("systemId", String.valueOf(SYSTEM_ID))
                .param("startTime", String.valueOf(TIMESTAMP))
                .param("endTime", String.valueOf(TIMESTAMP))
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsString(ENTITY_KEY_VALUES_1)))
                .andExpect(status().isInternalServerError())
                .andExpect(content().string(format(INTERNAL_ERROR_FORMAT, TEST_MESSAGE,
                        Throwables.getStackTraceAsString(runtimeException))));
    }
}
