/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest;

import cern.nxcals.common.domain.EntityData;
import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.Entity;
import cern.nxcals.service.internal.InternalEntityService;
import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.persistence.OptimisticLockException;
import javax.validation.ConstraintViolationException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static cern.nxcals.common.web.Endpoints.ENTITIES;
import static cern.nxcals.common.web.Endpoints.ENTITY_UPDATE;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_ID;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_JSON;
import static cern.nxcals.service.rest.DomainTestConstants.FIND_OR_CREATE_ENTITY_REQUEST_JSON;
import static cern.nxcals.service.rest.DomainTestConstants.OBJECT_MAPPER;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_DATA;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_ID;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.SCHEMA_DATA;
import static cern.nxcals.service.rest.DomainTestConstants.SCHEMA_VALUE;
import static cern.nxcals.service.rest.DomainTestConstants.SYSTEM_DATA;
import static cern.nxcals.service.rest.DomainTestConstants.SYSTEM_ID;
import static cern.nxcals.service.rest.DomainTestConstants.TEST_MESSAGE;
import static cern.nxcals.service.rest.DomainTestConstants.TEST_REGEX;
import static cern.nxcals.service.rest.DomainTestConstants.TIMESTAMP;
import static cern.nxcals.service.rest.GlobalControllerExceptionHandler.CONSTRAIN_VIOLATION_ERROR_FORMAT;
import static cern.nxcals.service.rest.GlobalControllerExceptionHandler.DATA_CONFLICT_ERROR_FORMAT;
import static cern.nxcals.service.rest.GlobalControllerExceptionHandler.ILLEGAL_ARGUMENT_ERROR_FORMAT;
import static cern.nxcals.service.rest.GlobalControllerExceptionHandler.INTERNAL_ERROR_FORMAT;
import static cern.nxcals.service.rest.GlobalControllerExceptionHandler.NOT_FOUND_ERROR_FORMAT;
import static cern.nxcals.service.rest.GlobalControllerExceptionHandler.VERSION_MISMATCH_ERROR_FORMAT;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_DOUBLE_SCHEMA_KEY_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_STRING_SCHEMA_KEY_1;
import static java.lang.String.format;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.anySetOf;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test suite that checks if the endpoints defined in {@link EntityController} follow the desired contract.
 */
@RunWith(MockitoJUnitRunner.class)
public class EntityControllerTest {
    private static final long LOCK_UNTIL_EPOCH_NANOS = 1511278189185000000L;
    private static final long REC_VERSION = 0L;
    private static final long NEW_REC_VERSION = 1L;
    private static final Map<String, Object> NEW_KEY_VALUES = ImmutableMap
            .of(ENTITY_STRING_SCHEMA_KEY_1, "new_string", ENTITY_DOUBLE_SCHEMA_KEY_1, "new_double");

    static final EntityData ENTITY_DATA = EntityData.builder(ENTITY_ID, ENTITY_KEY_VALUES_1, SYSTEM_DATA,
            PARTITION_DATA, SCHEMA_DATA, Collections.emptySortedSet(), LOCK_UNTIL_EPOCH_NANOS, REC_VERSION).build();

    private static final EntityData NEW_ENTITY_DATA = EntityData.builder(ENTITY_ID + 1, NEW_KEY_VALUES, SYSTEM_DATA,
            PARTITION_DATA, SCHEMA_DATA, Collections.emptySortedSet(), LOCK_UNTIL_EPOCH_NANOS, NEW_REC_VERSION)
            .build();

    private static final String EMPTY_JSON_ARRAY = "[]";
    private static final String FIND_ALL_BY_ID_ENDPOINT = ENTITIES + "?ids=%s,%s";

    @InjectMocks
    private EntityController entityController;

    @Mock
    private InternalEntityService internalEntityService;

    @Mock
    private Entity entity;

    private MockMvc mockMvc;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setup() {
        when(entity.toEntityData()).thenReturn(ENTITY_DATA);
        when(entity.getId()).thenReturn(ENTITY_ID);
        mockMvc = MockMvcBuilders.standaloneSetup(entityController)
                .setControllerAdvice(new GlobalControllerExceptionHandler())
                .build();
    }

    @Test
    public void shouldFindBySystemIdAndKeyValues() throws Exception {
        when(internalEntityService.findByPartitionSystemIdAndKeyValues(eq(SYSTEM_ID), eq(ENTITY_KEY_VALUES_1)))
                .thenReturn(entity);

        mockMvc.perform(post(ENTITIES)
                .param("systemId", String.valueOf(SYSTEM_ID))
                .contentType(MediaType.APPLICATION_JSON)
                .content(ENTITY_KEY_VALUES_JSON))
                .andExpect(status().isOk()).andExpect(jsonPath("id").value(ENTITY_ID))
                .andExpect(jsonPath("schemaData.schemaJson").value(SCHEMA_VALUE));
    }

    @Test
    public void shouldReturn404WhenFindBySystemIdAndKeyValuesFindsNothing() throws Exception {
        RuntimeException runtimeException = new NotFoundRuntimeException(TEST_MESSAGE);
        when(internalEntityService
                .findByPartitionSystemIdAndKeyValues(isA(Long.class), anyMapOf(String.class, Object.class)))
                .thenThrow(runtimeException);
        mockMvc.perform(post(ENTITIES)
                .param("systemId", String.valueOf(SYSTEM_ID))
                .contentType(MediaType.APPLICATION_JSON)
                .content(ENTITY_KEY_VALUES_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().string(format(NOT_FOUND_ERROR_FORMAT, TEST_MESSAGE,Throwables.getStackTraceAsString(runtimeException))));
    }

    @Test
    public void shouldReturn500WhenFindBySystemIdAndKeyValuesFailsUnexpectedly() throws Exception {
        RuntimeException runtimeException = new RuntimeException(TEST_MESSAGE);
        when(internalEntityService
                .findByPartitionSystemIdAndKeyValues(isA(Long.class), anyMapOf(String.class, Object.class)))
                .thenThrow(runtimeException);

        mockMvc.perform(post(ENTITIES)
                .param("systemId", String.valueOf(SYSTEM_ID))
                .contentType(MediaType.APPLICATION_JSON)
                .content(ENTITY_KEY_VALUES_JSON))
                .andExpect(status().isInternalServerError())
                .andExpect(content().string(format(INTERNAL_ERROR_FORMAT, TEST_MESSAGE,
                        Throwables.getStackTraceAsString(runtimeException))));
    }

    @Test
    public void shouldFindOrCreateEntity() throws Exception {
        when(internalEntityService.findOrCreateEntityFor(eq(SYSTEM_ID), eq(ENTITY_KEY_VALUES_1), eq(
                PARTITION_KEY_VALUES_1),
                eq(ENTITY_SCHEMA_1.toString()), eq(TIMESTAMP))).thenReturn(entity);

        mockMvc.perform(put(ENTITIES)
                .param("systemId", String.valueOf(SYSTEM_ID))
                .param("recordTimestamp", String.valueOf(TIMESTAMP))
                .contentType(MediaType.APPLICATION_JSON)
                .content(FIND_OR_CREATE_ENTITY_REQUEST_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(ENTITY_ID))
                .andExpect(jsonPath("schemaData.schemaJson").value(SCHEMA_VALUE));
    }

    @Test
    @WithMockUser(authorities = BaseTest.AUTHORITY)
    public void shouldReturn500WhenFindOrCreateEntityFailsUnexpectedly() throws Exception {
        RuntimeException runtimeException = new RuntimeException(TEST_MESSAGE);
        when(internalEntityService.findOrCreateEntityFor(isA(Long.class), anyMapOf(String.class, Object.class),
                anyMapOf(String.class, Object.class),
                isA(String.class), isA(Long.class))).thenThrow(runtimeException);

        mockMvc.perform(put(ENTITIES)
                .param("systemId", String.valueOf(SYSTEM_ID))
                .param("recordTimestamp", String.valueOf(TIMESTAMP))
                .contentType(MediaType.APPLICATION_JSON)
                .content(FIND_OR_CREATE_ENTITY_REQUEST_JSON))
                .andExpect(status().isInternalServerError())
                .andExpect(content().string(format(INTERNAL_ERROR_FORMAT, TEST_MESSAGE,
                        Throwables.getStackTraceAsString(runtimeException))));
    }

    @Test
    public void shouldReturn409WhenFindOrCreateEntityFailsWithConfigDataConflictException() throws Exception {
        ConfigDataConflictException runtimeException = new ConfigDataConflictException(TEST_MESSAGE);
        when(internalEntityService.findOrCreateEntityFor(isA(Long.class), isA(Long.class), isA(Long.class),
                isA(String.class), isA(Long.class))).thenThrow(runtimeException);

        mockMvc.perform(put(ENTITIES)
                .param("systemId", String.valueOf(SYSTEM_ID))
                .param("entityId", String.valueOf(ENTITY_ID))
                .param("partitionId", String.valueOf(PARTITION_ID))
                .param("recordTimestamp", String.valueOf(TIMESTAMP))
                .contentType(MediaType.APPLICATION_JSON)
                .content(SCHEMA_VALUE))
                .andExpect(status().isConflict())
                .andExpect(content().string(format(DATA_CONFLICT_ERROR_FORMAT, TEST_MESSAGE,
                        Throwables.getStackTraceAsString(runtimeException))));
    }

    @Test
    public void shouldReturn409WhenFindOrCreateEntityFailsWithIllegalArgumentException() throws Exception {
        IllegalArgumentException runtimeException = new IllegalArgumentException(TEST_MESSAGE);
        when(internalEntityService.findOrCreateEntityFor(isA(Long.class), isA(Long.class), isA(Long.class),
                isA(String.class), isA(Long.class))).thenThrow(runtimeException);

        mockMvc.perform(put(ENTITIES)
                .param("systemId", String.valueOf(SYSTEM_ID))
                .param("entityId", String.valueOf(ENTITY_ID))
                .param("partitionId", String.valueOf(PARTITION_ID))
                .param("recordTimestamp", String.valueOf(TIMESTAMP))
                .contentType(MediaType.APPLICATION_JSON)
                .content(SCHEMA_VALUE))
                .andExpect(status().isConflict())
                .andExpect(content().string(format(ILLEGAL_ARGUMENT_ERROR_FORMAT, TEST_MESSAGE,
                        Throwables.getStackTraceAsString(runtimeException))));
    }

    @Test
    public void shouldFindBySystemIdKeyValuesAndTimeWindow() throws Exception {
        when(internalEntityService
                .findEntityWithHistForTimeWindow(eq(SYSTEM_ID), eq(ENTITY_KEY_VALUES_1), eq(TIMESTAMP),
                        eq(TIMESTAMP))).thenReturn(entity);

        mockMvc.perform(post(ENTITIES)
                .param("systemId", String.valueOf(SYSTEM_ID))
                .param("startTime", String.valueOf(TIMESTAMP))
                .param("endTime", String.valueOf(TIMESTAMP))
                .contentType(MediaType.APPLICATION_JSON)
                .content(ENTITY_KEY_VALUES_JSON))
                .andExpect(status().isOk()).andExpect(jsonPath("id").value(ENTITY_ID))
                .andExpect(jsonPath("schemaData.schemaJson").value(SCHEMA_VALUE));
    }

    @Test
    public void shouldReturn404WhenFindBySystemIdKeyValuesAndTimeWindowFindsNothing() throws Exception {
        RuntimeException runtimeException = new NotFoundRuntimeException(TEST_MESSAGE);
        when(internalEntityService
                .findEntityWithHistForTimeWindow(isA(Long.class), anyMapOf(String.class, Object.class), isA(Long.class),
                        isA(Long.class)))
                .thenThrow(runtimeException);

        mockMvc.perform(post(ENTITIES)
                .param("systemId", String.valueOf(SYSTEM_ID))
                .param("startTime", String.valueOf(TIMESTAMP))
                .param("endTime", String.valueOf(TIMESTAMP))
                .contentType(MediaType.APPLICATION_JSON)
                .content(ENTITY_KEY_VALUES_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().string(format(NOT_FOUND_ERROR_FORMAT, TEST_MESSAGE,Throwables.getStackTraceAsString(runtimeException))));
    }

    @Test
    public void shouldReturn500WhenFindBySystemIdKeyvaluesAndTimeWindowFailsUnexpectedly() throws Exception {
        RuntimeException runtimeException = new RuntimeException(TEST_MESSAGE);
        when(internalEntityService
                .findEntityWithHistForTimeWindow(isA(Long.class), anyMapOf(String.class, Object.class), isA(Long.class),
                        isA(Long.class)))
                .thenThrow(runtimeException);

        mockMvc.perform(post(ENTITIES)
                .param("systemId", String.valueOf(SYSTEM_ID))
                .param("startTime", String.valueOf(TIMESTAMP))
                .param("endTime", String.valueOf(TIMESTAMP))
                .contentType(MediaType.APPLICATION_JSON)
                .content(ENTITY_KEY_VALUES_JSON))
                .andExpect(status().isInternalServerError())
                .andExpect(content().string(format(INTERNAL_ERROR_FORMAT, TEST_MESSAGE,
                        Throwables.getStackTraceAsString(runtimeException))));
    }

    @Test
    public void shouldFindByExpression() throws Exception {
        when(internalEntityService.findByKeyValueLike(eq(TEST_REGEX))).thenReturn(ImmutableList.of(entity));

        mockMvc.perform(get(ENTITIES)
                .param("keyValuesExpression", TEST_REGEX))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].entityKeyValues").value(is(ENTITY_KEY_VALUES_1)));
    }

    @Test
    public void shouldReturn404WhenFindByExpressionFindsNothing() throws Exception {
        RuntimeException runtimeException = new NotFoundRuntimeException(TEST_MESSAGE);
        when(internalEntityService.findByKeyValueLike(isA(String.class)))
                .thenThrow(runtimeException);

        mockMvc.perform(get(ENTITIES)
                .param("keyValuesExpression", TEST_REGEX))
                .andExpect(status().isNotFound())
                .andExpect(content().string(format(NOT_FOUND_ERROR_FORMAT, TEST_MESSAGE,Throwables.getStackTraceAsString(runtimeException))));
    }

    @Test
    public void shouldReturn500WhenFindByExpressionFailsUnexpectedly() throws Exception {
        RuntimeException runtimeException = new RuntimeException(TEST_MESSAGE);
        when(internalEntityService.findByKeyValueLike(isA(String.class)))
                .thenThrow(runtimeException);

        mockMvc.perform(get(ENTITIES)
                .param("keyValuesExpression", TEST_REGEX))
                .andExpect(status().isInternalServerError())
                .andExpect(content().string(format(INTERNAL_ERROR_FORMAT, TEST_MESSAGE,
                        Throwables.getStackTraceAsString(runtimeException))));
    }

    @Test
    public void shouldRenameEntityUsingEntityRenameRequest() throws Exception {
        when(entity.toEntityData()).thenReturn(NEW_ENTITY_DATA);
        List<Entity> entityList = Collections.singletonList(entity);

        List<EntityData> entityDataList = Lists.newArrayList();
        entityDataList.add(ENTITY_DATA);

        when(internalEntityService.updateEntities(eq(entityDataList))).thenReturn(entityList);

        mockMvc.perform(put(ENTITY_UPDATE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsString(entityDataList)))
                .andDo(print())
                .andExpect(jsonPath("$[0].entityKeyValues").value(is(NEW_KEY_VALUES)));
    }

    @Test
    public void shouldUpdateEntityLockUntilUsingEntityUpdateRequest() throws Exception {
        when(entity.toEntityData()).thenReturn(NEW_ENTITY_DATA);

        List<EntityData> entityDataList = Collections.singletonList(ENTITY_DATA);
        when(internalEntityService.updateEntities(eq(entityDataList)))
                .thenReturn(Collections.singletonList(entity));

        mockMvc.perform(put(ENTITY_UPDATE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsString(entityDataList)))
                .andDo(print())
                .andExpect(jsonPath("$[0].lockUntilEpochNanos").value(LOCK_UNTIL_EPOCH_NANOS));
    }

    @Test
    public void shouldReturnUnlockedEntityWhenUnlockEntityUpdateRequest() throws Exception {
        EntityData nonLockedEntityData = EntityData.builder(NEW_ENTITY_DATA).unlock().build();

        when(entity.toEntityData()).thenReturn(nonLockedEntityData);
        when(entity.getLockedUntilStamp()).thenReturn(null);

        List<EntityData> entityDataList = Collections.singletonList(nonLockedEntityData);
        when(internalEntityService.updateEntities(eq(entityDataList))).thenReturn(Collections.singletonList(entity));

        mockMvc.perform(put(ENTITY_UPDATE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsString(entityDataList)))
                .andDo(print())
                .andExpect(jsonPath("$[0].lockUntilEpochNanos").isEmpty());
    }

    @Test
    public void shouldReturn409CodeWhenThereIsRecordVersionMismatch() throws Exception {
        RuntimeException runtimeException = new VersionMismatchException(TEST_MESSAGE);
        when(internalEntityService.updateEntities(anyListOf(EntityData.class)))
                .thenThrow(runtimeException);

        mockMvc.perform(put(ENTITY_UPDATE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(EMPTY_JSON_ARRAY))
                .andExpect(status().isConflict())
                .andExpect(content().string(format(VERSION_MISMATCH_ERROR_FORMAT, TEST_MESSAGE,Throwables.getStackTraceAsString(runtimeException))));
    }

    @Test
    public void shouldReturn409CodeWhenThereIsJPAOptimisticLockException() throws Exception {
        RuntimeException runtimeException =new OptimisticLockException(TEST_MESSAGE);
        when(internalEntityService.updateEntities(anyListOf(EntityData.class)))
                .thenThrow(runtimeException);

        mockMvc.perform(put(ENTITY_UPDATE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(EMPTY_JSON_ARRAY))
                .andExpect(status().isConflict())
                .andExpect(content().string(format(VERSION_MISMATCH_ERROR_FORMAT, TEST_MESSAGE,Throwables.getStackTraceAsString(runtimeException))));
    }

    @Test
    public void shouldReturn409CodeWhenThereIsConstraintViolationException() throws Exception {
        RuntimeException runtimeException = new ConstraintViolationException(TEST_MESSAGE, Sets.newHashSet());
        when(internalEntityService.updateEntities(anyListOf(EntityData.class)))
                .thenThrow(runtimeException);

        mockMvc.perform(put(ENTITY_UPDATE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(EMPTY_JSON_ARRAY))
                .andExpect(status().isConflict())
                .andExpect(content().string(format(CONSTRAIN_VIOLATION_ERROR_FORMAT, TEST_MESSAGE,Throwables.getStackTraceAsString(runtimeException))));
    }

    @Test
    public void shouldFindOneEntityById() throws Exception {
        when(internalEntityService.findById(eq(ENTITY_ID)))
                .thenReturn(entity);

        mockMvc.perform(get(ENTITIES + "/" + ENTITY_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(ENTITY_ID));
    }

    @Test
    public void shouldGetEntityNotFoundWhenFindOneEntityByNonExistingId() throws Exception {
        RuntimeException runtimeException = new NotFoundRuntimeException(TEST_MESSAGE);
        when(internalEntityService.findById(anyLong()))
                .thenThrow(runtimeException);

        long nonExistingId = 123456L;

        mockMvc.perform(get(ENTITIES + "/" + nonExistingId))
                .andExpect(status().isNotFound())
                .andExpect(content().string(format(NOT_FOUND_ERROR_FORMAT, TEST_MESSAGE,Throwables.getStackTraceAsString(runtimeException))));
    }

    @Test
    public void shouldFindAllEntitiesById() throws Exception {
        Entity anEntity = Mockito.mock(Entity.class);
        when(anEntity.toEntityData()).thenReturn(ENTITY_DATA);

        Entity otherEntity = Mockito.mock(Entity.class);
        when(otherEntity.toEntityData()).thenReturn(NEW_ENTITY_DATA);

        Set<Long> requestEntityIds = Sets.newHashSet(
                ENTITY_DATA.getId(),
                NEW_ENTITY_DATA.getId()
        );

        Set<Entity> fetchedEntities = Sets.newHashSet(
                anEntity,
                otherEntity
        );
        when(internalEntityService.findAllByIdIn(eq(requestEntityIds)))
                .thenReturn(fetchedEntities);

        String h = format(FIND_ALL_BY_ID_ENDPOINT, ENTITY_DATA.getId(), NEW_ENTITY_DATA.getId());

        mockMvc.perform(get(ENTITIES)
                .param("ids", String.valueOf(ENTITY_DATA.getId()), String.valueOf(NEW_ENTITY_DATA.getId())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").isNotEmpty())
                .andExpect(jsonPath("$[1].id").isNotEmpty());
    }

    @Test
    public void shouldGetEmptyListWhenFindOneEntityByNonExistingId() throws Exception {
        when(internalEntityService.findAllByIdIn(anySetOf(Long.class))).thenReturn(Collections.emptySet());

        String nonExistingId = String.valueOf(123456L);
        String otherNonExistingId = String.valueOf(654321L);

        mockMvc.perform(get(ENTITIES)
                .param("ids", nonExistingId, otherNonExistingId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$").isEmpty());
    }
}