/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest;

import cern.nxcals.service.ApplicationDev;
import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.Entity;
import cern.nxcals.service.domain.System;
import cern.nxcals.service.internal.InternalEntityService;
import cern.nxcals.service.repository.SystemRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Optional;

import static cern.nxcals.common.web.Endpoints.ENTITIES;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_ID;
import static cern.nxcals.service.rest.DomainTestConstants.FIND_OR_CREATE_ENTITY_REQUEST_JSON;
import static cern.nxcals.service.rest.DomainTestConstants.SYSTEM_ID;
import static cern.nxcals.service.rest.DomainTestConstants.SYSTEM_NAME;
import static cern.nxcals.service.rest.DomainTestConstants.TIMESTAMP;
import static cern.nxcals.service.rest.GlobalControllerExceptionHandler.FORBIDDEN_ERROR_FORMAT_PREFIX;
import static java.lang.String.format;
import static org.hamcrest.Matchers.containsString;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ApplicationDev.class)
@TestPropertySource(locations = "classpath:application.properties")
public class NonDevProfileResolverTest {
    @MockBean
    private SystemRepository systemRepository;

    @MockBean
    private InternalEntityService internalEntityService;

    @Mock
    private Entity entity;

    @Mock
    private System system;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() {
        when(entity.getId()).thenReturn(ENTITY_ID);

        when(internalEntityService.findOrCreateEntityFor(isA(Long.class), anyMapOf(String.class, Object.class),
                anyMapOf(String.class, Object.class), isA(String.class), isA(Long.class))).thenReturn(entity);

        when(system.getName()).thenReturn(SYSTEM_NAME);

        when(systemRepository.findById(eq(SYSTEM_ID))).thenReturn(Optional.of(system));

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }

    @Test
    @WithMockUser(authorities = "ALL")
    public void shouldNotAuthorizeToFindOrCreateEntityFor() throws Exception {
        String expectedString = format(FORBIDDEN_ERROR_FORMAT_PREFIX, "Access is denied");
        mockMvc.perform(put(ENTITIES)
                .param("systemId", String.valueOf(SYSTEM_ID))
                .param("recordTimestamp", String.valueOf(TIMESTAMP))
                .contentType(MediaType.APPLICATION_JSON)
                .content(FIND_OR_CREATE_ENTITY_REQUEST_JSON))
                .andExpect(status().isForbidden())
                .andExpect(content().string(containsString(expectedString)));
    }

    @Test
    @WithMockUser(authorities = BaseTest.AUTHORITY)
    public void shouldAuthorizeWithDefaultProfile() throws Exception {
        mockMvc.perform(put(ENTITIES)
                .param("systemId", String.valueOf(SYSTEM_ID))
                .param("recordTimestamp", String.valueOf(TIMESTAMP))
                .contentType(MediaType.APPLICATION_JSON)
                .content(FIND_OR_CREATE_ENTITY_REQUEST_JSON))
                .andExpect(status().isOk());
    }
}

