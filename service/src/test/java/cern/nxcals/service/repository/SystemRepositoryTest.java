/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.repository;

import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.System;
import org.junit.Test;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolationException;
import java.util.Optional;

import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.TIME_SCHEMA;
import static org.assertj.core.api.Assertions.assertThat;

@Transactional(transactionManager = "jpaTransactionManager")
public class SystemRepositoryTest extends BaseTest {

    @Test
    @Rollback
    public void shouldNotFindSystemWIthWrongName() {
        createAndPersistClientSystem(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA);

        Optional<System> foundSystem = systemRepository.findByName("");
        assertThat(foundSystem).isNotPresent();
    }

    @Test
    @Rollback
    public void shouldFindSystemWithCorrectName() {
        Long id = createAndPersistClientSystem(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA).getId();

        Optional<System> optionalSystem = systemRepository.findByName(TEST_NAME);

        assertThat(optionalSystem).isPresent();

        System foundSystem = optionalSystem.get();

        assertThat(foundSystem.getId()).isEqualTo(id);
        assertThat(foundSystem.getEntityKeyDefs()).isEqualTo(ENTITY_SCHEMA_1.toString());
    }

    @Test(expected = ConstraintViolationException.class)
    @Rollback
    public void shouldNotCreateSystemWithoutName() {
        createAndPersistClientSystem(null, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA);
        entityManager.flush();
    }

    @Test(expected = ConstraintViolationException.class)
    @Rollback
    public void shouldNotCreateSystemWithoutKeyDefinitions() {
        createAndPersistClientSystem(TEST_NAME, null, PARTITION_SCHEMA_1, TIME_SCHEMA);
        entityManager.flush();
    }

    @Test(expected = ConstraintViolationException.class)
    @Rollback
    public void shouldNotCreateSystemWithoutPartitionDefinitions() {
        createAndPersistClientSystem(TEST_NAME, ENTITY_SCHEMA_1, null, TIME_SCHEMA);
        entityManager.flush();
    }

    @Test(expected = ConstraintViolationException.class)
    @Rollback
    public void shouldNotCreateSystemWithoutTimeDefinitions() {
        createAndPersistClientSystem(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, null);
        entityManager.flush();
    }

    @Test
    @Rollback
    public void shouldCreateClientSystem() {

        Long id = createAndPersistClientSystem(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA).getId();

        Optional<System> optionalSystem = systemRepository.findById(id);

        assertThat(optionalSystem).isPresent();

        System foundSystem = optionalSystem.get();

        assertThat(foundSystem.getName()).isEqualTo(TEST_NAME);
        assertThat(foundSystem.getEntityKeyDefs()).isEqualTo(ENTITY_SCHEMA_1.toString());
        assertThat(foundSystem.getPartitionKeyDefs()).isEqualTo(PARTITION_SCHEMA_1.toString());
        assertThat(foundSystem.getTimeKeyDefs()).isEqualTo(TIME_SCHEMA.toString());
    }
}
