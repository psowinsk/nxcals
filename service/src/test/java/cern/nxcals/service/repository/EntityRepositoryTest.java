/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.repository;

import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.Entity;
import cern.nxcals.service.domain.Partition;
import cern.nxcals.service.domain.Schema;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolationException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_JSON;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_2;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_JSON_1;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_STRING_2;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_2;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_SCHEMA_2;
import static org.assertj.core.api.Assertions.assertThat;

@Transactional(transactionManager = "jpaTransactionManager")
public class EntityRepositoryTest extends BaseTest {

    private Entity newEntity;

    @Before
    public void setUp() {
        newEntity = createEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1, PARTITION_KEY_VALUES_1,
                PARTITION_SCHEMA_1);
    }

    @Test(expected = ConstraintViolationException.class)
    @Rollback
    public void shouldNotCreateEntityWithoutKeyValues() {
        newEntity.setKeyValues(null);
        persistEntity(newEntity);
        entityManager.flush();
    }

    @Test(expected = ConstraintViolationException.class)
    @Rollback
    public void shouldNotCreateEntityWithoutSchema() {
        newEntity.setSchema(null);
        persistEntity(newEntity);
        entityManager.flush();
    }

    @Test(expected = IllegalArgumentException.class)
    @Rollback
    public void shouldNotCreateEntityWithoutPartition() {
        newEntity.setPartition(null);
        entityRepository.save(newEntity);
        entityManager.flush();
    }

    @Test
    @Rollback
    public void shouldCreateEntity() {
        persistEntity(newEntity);
        Entity savedEntity = entityRepository.save(newEntity);
        assertThat(savedEntity).isNotNull();
        assertThat(savedEntity.getKeyValues()).isEqualTo(ENTITY_KEY_VALUES_JSON);
        assertThat(savedEntity.getSchema().getContent()).isEqualTo(ENTITY_SCHEMA_1.toString());
        assertThat(savedEntity.getPartition().getKeyValues()).isEqualTo(PARTITION_KEY_VALUES_JSON_1);
    }

    @Test
    @Rollback
    public void shouldChangeEntitySchema() {
        persistEntity(newEntity);
        Schema newSchema = createSchema(ENTITY_SCHEMA_2);
        schemaRepository.save(newSchema);
        newEntity.setSchema(newSchema);
        entityRepository.save(newEntity);
        entityManager.flush();

        Optional<Entity> optionalEntity = entityRepository.findById(newEntity.getId());

        assertThat(optionalEntity).isPresent();
        assertThat(optionalEntity.get().getSchema().getContent()).isEqualTo(ENTITY_SCHEMA_2.toString());
    }

    @Test
    @Rollback
    public void shouldChangeEntityPartition() {
        persistEntity(newEntity);
        Partition newPartition = createPartition(TEST_NAME, PARTITION_KEY_VALUES_2, PARTITION_SCHEMA_2);
        partitionRepository.save(newPartition);
        newEntity.setPartition(newPartition);
        entityRepository.save(newEntity);
        entityManager.flush();

        Optional<Entity> optionalEntity = entityRepository.findById(newEntity.getId());

        assertThat(optionalEntity).isPresent();
        assertThat(optionalEntity.get().getPartition().getKeyValues()).isEqualTo(PARTITION_KEY_VALUES_STRING_2);
    }

    //TODO Check why this test fails
    /*@Test
    @Rollback
    public void shouldChangeEntityKeyValues() {
        persistEntity(newEntity);
        newEntity.setKeyValues(getKeyValuesString("Test"));
        entityRepository.save(newEntity);

        Optional<Entity> optionalEntity = entityRepository.findById(newEntity.getId());

        assertThat(optionalEntity).isPresent();
        Entity foundEntity = optionalEntity.get();

        assertThat(foundEntity.getKeyValues()).isEqualTo(getKeyValuesString("Test"));
        assertThat(foundEntity.getSchema().getContent()).isEqualTo(getSchema());
        assertThat(foundEntity.getPartition().getKeyValues()).isEqualTo(getKeyValuesString());
        assertThat(foundEntity.getPartition().getSystem().getName()).isEqualTo(TEST_NAME);
    }*/

    @Test
    @Rollback
    public void shouldChangeEntityLockedUntilStamp() {
        persistEntity(newEntity);

        Instant lockUntilStamp = Instant.now().plus(2, ChronoUnit.HOURS);
        newEntity.setLockedUntilStamp(lockUntilStamp);
        entityRepository.save(newEntity);

        Optional<Entity> optionalEntity = entityRepository.findById(newEntity.getId());

        assertThat(optionalEntity).isPresent();

        Entity foundEntity = optionalEntity.get();

        assertThat(foundEntity.getLockedUntilStamp()).isEqualTo(lockUntilStamp);
        assertThat(foundEntity.getSchema().getContent()).isEqualTo(ENTITY_SCHEMA_1.toString());
        assertThat(foundEntity.getPartition().getKeyValues()).isEqualTo(PARTITION_KEY_VALUES_JSON_1);
        assertThat(foundEntity.getPartition().getSystem().getName()).isEqualTo(TEST_NAME);
    }

    @Test
    @Rollback
    public void shouldNotFindResult() {
        persistEntity(newEntity);

        Optional<Entity> entity = entityRepository.findByPartitionSystemIdAndKeyValues(-1, ENTITY_KEY_VALUES_JSON);

        assertThat(entity).isEmpty();
    }

    @Test
    @Rollback
    public void shouldNotFindResultWhileQueryingWithRegex() {
        persistEntity(newEntity);

        List<Entity> foundEntities = entityRepository.findByKeyValuesLike("%rsdfin%");

        assertThat(foundEntities).isEmpty();
    }

    @Test
    @Rollback
    public void shouldFindEntitiesWhileQueryingWithRegex() {
        persistEntity(newEntity);

        List<Entity> foundEntities = entityRepository.findByKeyValuesLike("%trin%");

        assertThat(foundEntities).containsExactly(newEntity);
    }

    @Test
    @Rollback
    public void shouldGetEntityForSystemAndKeyValues() {
        persistEntity(newEntity);
        createAndPersistEntityHist(newEntity, newEntity.getPartition(), newEntity.getSchema(), TEST_RECORD_TIME);

        Long systemId = newEntity.getPartition().getSystem().getId();
        Optional<Entity> optionalEntity = entityRepository
                .findByPartitionSystemIdAndKeyValues(systemId, ENTITY_KEY_VALUES_JSON);

        assertThat(optionalEntity).isPresent();

        Entity foundEntity = optionalEntity.get();

        assertThat(foundEntity.getKeyValues()).isEqualTo(ENTITY_KEY_VALUES_JSON);
        assertThat(foundEntity.getSchema().getContent()).isEqualTo(ENTITY_SCHEMA_1.toString());
        assertThat(foundEntity.getPartition().getKeyValues()).isEqualTo(PARTITION_KEY_VALUES_JSON_1);
        assertThat(foundEntity.getPartition().getSystem().getName()).isEqualTo(TEST_NAME);

        assertThat(foundEntity.getEntityHistories()).hasSize(1);
    }

    @Test
    @Rollback
    public void shouldCountOneEntityWithLockUntilStampGreaterThanProvided() {
        persistEntity(newEntity);

        Long partitionId = newEntity.getPartition().getId();
        Long systemId = newEntity.getPartition().getSystem().getId();
        Instant lockedUntilStamp = Instant.now().plus(4, ChronoUnit.HOURS);

        newEntity.setLockedUntilStamp(lockedUntilStamp);

        entityRepository.save(newEntity);

        Instant appliedActionOnEntityStamp = Instant.now();

        long lockedEntitiesNum = entityRepository
                .countByPartitionSystemIdAndPartitionIdAndLockedUntilStampGreaterThanEqual(
                        systemId, partitionId, appliedActionOnEntityStamp);
        assertThat(lockedEntitiesNum).isOne();
    }

    @Test
    @Rollback
    public void shouldCountOneEntityWithLockUntilStampEqualWithProvided() {
        persistEntity(newEntity);

        Long partitionId = newEntity.getPartition().getId();
        Long systemId = newEntity.getPartition().getSystem().getId();
        Instant lockedUntilStamp = Instant.now().plus(4, ChronoUnit.HOURS);

        newEntity.setLockedUntilStamp(lockedUntilStamp);

        Instant appliedActionOnEntityStamp = lockedUntilStamp;

        entityRepository.save(newEntity);

        long lockedEntitiesNum = entityRepository
                .countByPartitionSystemIdAndPartitionIdAndLockedUntilStampGreaterThanEqual(
                        systemId, partitionId, appliedActionOnEntityStamp);
        assertThat(lockedEntitiesNum).isOne();
    }

    @Test
    @Rollback
    public void shouldCountZeroEntitiesWithLockUntilStampGreaterOrEqualThanProvided() {
        persistEntity(newEntity);

        Long partitionId = newEntity.getPartition().getId();
        Long systemId = newEntity.getPartition().getSystem().getId();

        /**
         * Old time to ensure that no entity is locked for that timestamp and before
         * Like some months later... should be a good time for compaction!
         */
        Instant appliedActionOnEntityStamp = Instant.now().plus(300, ChronoUnit.DAYS);

        long lockedEntitiesNum = entityRepository
                .countByPartitionSystemIdAndPartitionIdAndLockedUntilStampGreaterThanEqual(
                        systemId, partitionId, appliedActionOnEntityStamp);
        assertThat(lockedEntitiesNum).isZero();
    }

    @Test
    @Rollback
    public void shouldWriteEntityLockedUntilStampAsNull() {
        persistEntity(newEntity);
        newEntity.setLockedUntilStamp(null);
        entityRepository.save(newEntity);

        Optional<Entity> optionalEntity = entityRepository.findById(newEntity.getId());

        assertThat(optionalEntity).isPresent();

        Entity foundEntity = optionalEntity.get();

        assertThat(foundEntity.getLockedUntilStamp()).isNull();
        assertThat(foundEntity.getSchema().getContent()).isEqualTo(ENTITY_SCHEMA_1.toString());
        assertThat(foundEntity.getPartition().getKeyValues()).isEqualTo(PARTITION_KEY_VALUES_JSON_1);
        assertThat(foundEntity.getPartition().getSystem().getName()).isEqualTo(TEST_NAME);
    }
}
