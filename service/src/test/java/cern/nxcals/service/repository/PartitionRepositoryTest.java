/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.repository;

import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.Partition;
import cern.nxcals.service.domain.System;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolationException;
import java.util.Optional;

import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_JSON_1;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_STRING_1;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_SCHEMA_1;
import static org.assertj.core.api.Assertions.assertThat;

@Transactional(transactionManager = "jpaTransactionManager")
public class PartitionRepositoryTest extends BaseTest {

    private Partition newPartition;

    @Before
    public void setUp() {
        newPartition = createPartition(TEST_NAME, PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
    }

    @Test
    @Rollback
    public void shouldNotFindPartition() {
        Optional<Partition> foundPartition = partitionRepository
                .findBySystemIdAndKeyValues(-1, PARTITION_KEY_VALUES_STRING_1);
        assertThat(foundPartition).isNotPresent();
    }

    @Test
    @Rollback
    public void shouldGetPartitionForSystemAndKeyValues() {
        partitionRepository.save(newPartition);
        Long systemId = newPartition.getSystem().getId();

        Partition foundPartition = partitionRepository
                .findBySystemIdAndKeyValues(systemId, PARTITION_KEY_VALUES_STRING_1).get();

        assertThat(foundPartition.getSystem().getName()).isEqualTo(TEST_NAME);
        assertThat(foundPartition.getKeyValues()).isEqualTo(PARTITION_KEY_VALUES_STRING_1);
    }

    @Test(expected = ConstraintViolationException.class)
    @Rollback
    public void shouldNotCreatePartitionWithoutSystem() {
        partitionRepository.save(createPartition((System) null, PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1));
        entityManager.flush();
    }

    @Test(expected = ConstraintViolationException.class)
    @Rollback
    public void shouldNotCreatePartitionWithoutKeyValues() {
        partitionRepository.save(createPartition(TEST_NAME, null, null));
        entityManager.flush();
    }

    @Test
    @Rollback
    public void shouldCreatePartitionKey() {
        Partition partition = createPartition(TEST_NAME, PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
        Long partitionId = partitionRepository.save(partition).getId();

        Optional<Partition> optionalPartition = partitionRepository.findById(partitionId);

        assertThat(optionalPartition).isPresent();

        Partition foundPartition = optionalPartition.get();

        assertThat(foundPartition.getKeyValues()).isEqualTo(PARTITION_KEY_VALUES_JSON_1);
        assertThat(foundPartition.getSystem().getName()).isEqualTo(TEST_NAME);
    }
}