package cern.nxcals.service.repository;

import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.Variable;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Transactional(transactionManager = "jpaTransactionManager")
public class VariableRepositoryTest extends BaseTest {

    @Autowired
    private VariableRepository variableRepository;
    private Variable firstVariable;
    private Variable secondVariable;

    @Before
    public void before() {
        firstVariable = createAndPersistVariable("TEST_NAME", "Long Description", Instant.now());
        secondVariable = createAndPersistVariable("TEST_NAME2", "Very Long Description", Instant.now());
    }

    @Test
    @Rollback
    public void shouldNotFindAnyResults() {
        List<Variable> results = variableRepository.findByVariableNameLike("TEST_%4");

        assertThat(results).hasSize(0);
    }

    @Test
    @Rollback
    public void shouldFindOneResultsByRegexThatIsUsingName() {
        List<Variable> results = variableRepository.findByVariableNameLike("%AME2");

        assertThat(results).containsExactly(secondVariable);
    }

    @Test
    @Rollback
    public void shouldFindAllResultsByRegexThatIsUsingName() {
        List<Variable> results = variableRepository.findByVariableNameLike("TEST_NAM%");

        assertThat(results).containsExactly(firstVariable, secondVariable);
    }

    @Test
    @Rollback
    public void shouldFindNoResultsByRegexThatIsUsingName() {
        List<Variable> results = variableRepository.findByVariableNameLike("TEST_NAM%22");

        assertThat(results).hasSize(0);
    }

    @Test
    @Rollback
    public void shouldFindNoResultsBasedOnDescription() {
        List<Variable> results = variableRepository.findByDescriptionLike("%ON4");

        assertThat(results).hasSize(0);
    }

    @Test
    @Rollback
    public void shouldFindOneResultBasedOnDescription() {
        List<Variable> results = variableRepository.findByDescriptionLike("Very%on%escription");

        assertThat(results).containsExactly(secondVariable);
    }

    @Test
    @Rollback
    public void shouldFindAllResultsByRegexThatIsUsingDescription() {
        List<Variable> results = variableRepository.findByDescriptionLike("%ong%");

        assertThat(results).containsExactly(firstVariable, secondVariable);
    }
}
