/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.repository;

import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.Schema;
import org.junit.Test;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolationException;
import java.util.Optional;

import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_1;
import static org.assertj.core.api.Assertions.assertThat;

@Transactional(transactionManager = "jpaTransactionManager")
public class SchemaRepositoryTest extends BaseTest {

    @Test
    @Rollback
    public void shouldNotFindSchemaWrongId() {
        Optional<Schema> optionalSchema = schemaRepository.findById(-1L);
        assertThat(optionalSchema).isNotPresent();
    }

    @Test(expected = ConstraintViolationException.class)
    @Rollback
    public void shouldNotCreateSchemaWithoutContent() {
        Schema schema = createSchema(null);
        schemaRepository.save(schema);
        entityManager.flush();
    }

    @Test
    @Rollback
    public void shouldCreateSchema() {
        Schema newSchema = createSchema(ENTITY_SCHEMA_1);
        schemaRepository.save(newSchema);

        Optional<Schema> optionalSchema = schemaRepository.findById(newSchema.getId());

        assertThat(optionalSchema).isPresent();

        Schema foundSchema = optionalSchema.get();

        assertThat(foundSchema.getContent()).isEqualTo(ENTITY_SCHEMA_1.toString());
    }
}
