/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.repository;

import cern.nxcals.common.utils.TimeUtils;
import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.Entity;
import cern.nxcals.service.domain.EntityHistory;
import cern.nxcals.service.domain.Partition;
import com.google.common.collect.Sets;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolationException;
import java.time.Instant;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_JSON;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_2;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_STRING_1;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_STRING_2;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_2;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_SCHEMA_2;
import static org.assertj.core.api.Assertions.assertThat;

@Transactional(transactionManager = "jpaTransactionManager")
public class EntityHistoryRepositoryTest extends BaseTest {

    @Autowired
    private EntityHistoryRepository entityHistoryRepository;

    @Test(expected = ConstraintViolationException.class)
    @Rollback
    public void shouldNotCreateHistoryWithoutPartition() {
        Entity entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1, PARTITION_KEY_VALUES_1,
                PARTITION_SCHEMA_1);
        createAndPersistEntityHistory(entity, null, entity.getSchema(), TEST_RECORD_TIME);
        entityManager.flush();
    }

    @Test(expected = ConstraintViolationException.class)
    @Rollback
    public void shouldNotCreateHistoryWithoutSchema() {
        Entity entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1, PARTITION_KEY_VALUES_1,
                PARTITION_SCHEMA_1);
        createAndPersistEntityHistory(entity, entity.getPartition(), null, TEST_RECORD_TIME);
        entityManager.flush();
    }

    @Test(expected = NullPointerException.class)
    @Rollback
    public void shouldNotCreateHistoryWithoutValidFromStamp() {
        Entity entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1, PARTITION_KEY_VALUES_1,
                PARTITION_SCHEMA_1);
        createAndPersistEntityHistory(entity, entity.getPartition(), entity.getSchema(), null);
        entityManager.flush();
    }

    @Test
    @Rollback
    public void shouldCreateEntityHist() {
        Entity entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1, PARTITION_KEY_VALUES_1,
                PARTITION_SCHEMA_1);
        Long historyId = createAndPersistEntityHist(entity, entity.getPartition(), entity.getSchema(), TEST_RECORD_TIME)
                .getId();
        Optional<EntityHistory> optionalEntityHistory = entityHistoryRepository.findById(historyId);

        assertThat(optionalEntityHistory).isPresent();
        EntityHistory entityHistory = optionalEntityHistory.get();
        assertThat(entityHistory.getEntity().getKeyValues()).isEqualTo(ENTITY_KEY_VALUES_JSON);
        assertThat(entityHistory.getPartition().getKeyValues()).isEqualTo(PARTITION_KEY_VALUES_STRING_1);
        assertThat(entityHistory.getSchema().getContent()).isEqualTo(ENTITY_SCHEMA_1.toString());
        assertThat(entityHistory.getValidFromStamp()).isEqualTo(TimeUtils.getInstantFromNanos(TEST_RECORD_TIME));
        assertThat(entityHistory.getValidToStamp()).isNull();
    }

    @Test
    @Rollback
    public void shouldUpdateEntityHistPartition() {
        Entity entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1, PARTITION_KEY_VALUES_1,
                PARTITION_SCHEMA_1);
        EntityHistory entityHistory = createAndPersistEntityHist(entity, entity.getPartition(), entity.getSchema(),
                TEST_RECORD_TIME);
        updateEntityHistoryPartition(entityHistory, TEST_NAME, PARTITION_KEY_VALUES_2, PARTITION_SCHEMA_2);

        Optional<EntityHistory> optionalEntityHistory = entityHistoryRepository.findById(entityHistory.getId());

        assertThat(optionalEntityHistory).isPresent();
        EntityHistory foundEntityHistory = optionalEntityHistory.get();
        assertThat(foundEntityHistory.getPartition().getSystem().getName()).isEqualTo(TEST_NAME);
        assertThat(foundEntityHistory.getPartition().getKeyValues()).isEqualTo(PARTITION_KEY_VALUES_STRING_2);
        assertThat(foundEntityHistory.getSchema().getContent()).isEqualTo(ENTITY_SCHEMA_1.toString());
        assertThat(foundEntityHistory.getValidFromStamp()).isEqualTo(TimeUtils.getInstantFromNanos(TEST_RECORD_TIME));
        assertThat(foundEntityHistory.getValidToStamp()).isNull();
    }

    @Test
    @Rollback
    public void shouldUpdateEntityHistSchema() {
        Entity entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1, PARTITION_KEY_VALUES_1,
                PARTITION_SCHEMA_1);
        EntityHistory entityHistory = createAndPersistEntityHist(entity, entity.getPartition(), entity.getSchema(),
                TEST_RECORD_TIME);
        updateEntityHistorySchema(entityHistory, ENTITY_SCHEMA_2);

        Optional<EntityHistory> optionalEntityHistory = entityHistoryRepository.findById(entityHistory.getId());

        assertThat(optionalEntityHistory).isPresent();
        EntityHistory foundEntityHistory = optionalEntityHistory.get();
        assertThat(foundEntityHistory.getPartition().getSystem().getName()).isEqualTo(TEST_NAME);
        assertThat(foundEntityHistory.getPartition().getKeyValues()).isEqualTo(PARTITION_KEY_VALUES_STRING_1);
        assertThat(foundEntityHistory.getSchema().getContent()).isEqualTo(ENTITY_SCHEMA_2.toString());
        assertThat(foundEntityHistory.getValidFromStamp()).isEqualTo(TimeUtils.getInstantFromNanos(TEST_RECORD_TIME));
        assertThat(foundEntityHistory.getValidToStamp()).isNull();
    }

    @Test
    @Rollback
    public void shouldUpdateEntityHistTimes() {
        EntityHistory entityHistory = createAndPersistDefaultTestEntityKeyHist();
        Instant testTime = TimeUtils.getInstantFromNanos(TEST_RECORD_TIME).plusSeconds(10);
        entityHistory.setValidFromStamp(testTime);
        entityHistory.setValidToStamp(testTime);
        entityHistoryRepository.save(entityHistory);

        Optional<EntityHistory> optionalUpdatedHistory = entityHistoryRepository.findById(entityHistory.getId());

        assertThat(optionalUpdatedHistory).isPresent();
        EntityHistory foundEntityHistory = optionalUpdatedHistory.get();
        assertThat(foundEntityHistory.getPartition().getSystem().getName()).isEqualTo(TEST_NAME);
        assertThat(foundEntityHistory.getPartition().getKeyValues()).isEqualTo(PARTITION_KEY_VALUES_STRING_1);
        assertThat(foundEntityHistory.getSchema().getContent()).isEqualTo(ENTITY_SCHEMA_1.toString());
        assertThat(foundEntityHistory.getValidFromStamp()).isEqualTo(testTime);
        assertThat(foundEntityHistory.getValidToStamp()).isEqualTo(testTime);
    }

    private EntityHistory updateEntityHistorySchema(EntityHistory entityHistory,
            org.apache.avro.Schema newSchemaContent) {
        cern.nxcals.service.domain.Schema newSchema = createSchema(newSchemaContent);
        schemaRepository.save(newSchema);
        entityHistory.setSchema(newSchema);
        return entityHistRepository.save(entityHistory);
    }

    private void updateEntityHistoryPartition(EntityHistory entityHistory, String systemName,
            Map<String, Object> partitionKeyValues, org.apache.avro.Schema partitionSchema) {
        Partition newPartition = createPartition(systemName, partitionKeyValues, partitionSchema);
        partitionRepository.save(newPartition);
        entityHistory.setPartition(newPartition);
        entityHistRepository.save(entityHistory);
    }

    @Test
    @Rollback
    public void shouldFindAllFirstEntriesByEntityInCollection() {
        Entity entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1, PARTITION_KEY_VALUES_1,
                PARTITION_SCHEMA_1);
        EntityHistory entityHistory = createAndPersistEntityHist(entity, entity.getPartition(), entity.getSchema(),
                TEST_RECORD_TIME);
        updateEntityHistorySchema(entityHistory, ENTITY_SCHEMA_2);

        Set<EntityHistory> historyEntries = entityHistoryRepository
                .findAllFirstEntriesByEntityIn(Sets.newHashSet(entity));

        assertThat(historyEntries).isNotNull();
        assertThat(historyEntries.size()).isEqualTo(1);
        assertThat(historyEntries).contains(entityHistory);
    }

    @Test
    @Rollback
    public void shouldFindAllLastEntriesByEntityInCollection() {
        Entity entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1, PARTITION_KEY_VALUES_1,
                PARTITION_SCHEMA_1);
        EntityHistory entityHistory = createAndPersistEntityHist(entity, entity.getPartition(), entity.getSchema(),
                TEST_RECORD_TIME);
        EntityHistory updatedEntityHistory = updateEntityHistorySchema(entityHistory, ENTITY_SCHEMA_2);

        Set<EntityHistory> historyEntries = entityHistoryRepository
                .findAllLatestEntriesByEntityIn(Sets.newHashSet(entity));

        assertThat(historyEntries).isNotNull();
        assertThat(historyEntries.size()).isEqualTo(1);
        assertThat(historyEntries).contains(updatedEntityHistory);
    }
}