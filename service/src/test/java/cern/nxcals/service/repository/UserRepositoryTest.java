package cern.nxcals.service.repository;

import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.security.Permission;
import cern.nxcals.service.domain.security.Role;
import cern.nxcals.service.domain.security.User;
import org.junit.Test;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by wjurasz on 11.10.17.
 */
@Transactional(transactionManager = "jpaTransactionManager")
public class UserRepositoryTest extends BaseTest {

    @Test
    @Rollback
    public void shouldGetAllPermissionsOfUser() {
        createAndPersistUser();
        User user = this.userRepository.findUserWithRealmWithRolesWithPermissions(TEST_USER_NAME, TEST_REALM_NAME);

        assertEquals(user.getUserName(), TEST_USER_NAME);
        List<Permission> permissions = user.getRoles().stream().flatMap(role -> role.getPermissions().stream())
                .collect(Collectors.toList());
        assertEquals(2, permissions.size());
        permissions.forEach(e -> {
            Optional<Role> first = e.getRoles().stream().findFirst();
            assertTrue(first.isPresent());
            assertTrue(TEST_PERMISSIONNS.contains(e.getPermissionName()));
        });
    }

    @Test
    @Rollback
    public void shouldGetNullIfUserDoesNotExist() {
        User user = this.userRepository.findUserWithRealmWithRolesWithPermissions("SOME_USER_THAT_DOES_NOT_EXIST", TEST_REALM_NAME);
        assertNull(user);
    }

}
