/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service;

import cern.nxcals.service.security.LocalKdc;
import cern.nxcals.service.security.resolvers.DevPermissionResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

/**
 * This class starts local instance of nxcals-service as well as local KDC server.
 * As service's endpoints are protected by Kerberos, client has to set following properties in order to use it locally.
 *
 * try {
 * System.setProperty("service.url", "https://" + InetAddress.getLocalHost().getHostName() + ":<service-port>"); //Do not use localhost here.
 * } catch (UnknownHostException e) {
 * throw new RuntimeException("Cannot acquire hostname programmatically, provide the name full name of localhost");
 * }
 *
 * //Name of the user. To change it go to {@link TEST_PRINCIPAL} variable.
 * //Also the same name must be used in application.properties in kerberos.principal property.
 * //Do not set this variable to your username if you have Kerberos ticket cached,
 * //this will cause conflicts during authentication with local KDC instance.
 * System.setProperty("kerberos.principal", "mock-system-user");
 *
 * //This keytab is created automatically. It contains local service principle
 * //and user principle (with username as above)
 * System.setProperty("kerberos.keytab", ".service.keytab");
 *
 * //This conf is created automatically.
 * //It contains configuration necessary to run local instance of KDC.
 * System.setProperty("java.security.krb5.conf", "build/LocalKdc/krb5.conf");
 *
 * Also in application.properties unit test database credentials are set. You probably want to change it to use your personal DB instance.
 */

@EnableJpaRepositories(transactionManagerRef = "jpaTransactionManager")
@SpringBootApplication
public class ApplicationDev {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationDev.class);
    private static final String TEST_PRINCIPAL = "mock-system-user";
    private static String KEYTAB_PATH = ".service.keytab";

    private static String HOST;
    private static String SERVICE_PRINCIPAL;
    private static LocalKdc kdc;
    private static File workDirParent;
    private static String workDir = "LocalKdc";
    private static Properties conf;
    private static boolean isKdcUp = false;

    static {
        try {
            HOST = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException ex) {
            LOGGER.error("Could not get localhost name.", ex);
        }
        SERVICE_PRINCIPAL = "HTTPS/" + HOST;
    }

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(ApplicationDev.class);
        app.setAdditionalProfiles("dev");
        app.addListeners(new ApplicationPidFileWriter());
        ConfigurableApplicationContext context = app.run(args);
        IdGeneratorFactory.setContext(context);
    }

    @PostConstruct
    public synchronized static void startMiniKdc() throws Exception {
        if (!isKdcUp) {
            createTestDir();
            createMiniKdcConf();

            kdc = new LocalKdc(conf, workDirParent, workDir);
            kdc.start();
            isKdcUp = true;
            kdc.createPrincipal(new File(KEYTAB_PATH), TEST_PRINCIPAL, SERVICE_PRINCIPAL);
        }
    }

    @PreDestroy
    public static void stopMiniKdc() {
        if (kdc != null) {
            kdc.stop();
        }
        File keytab = new File(KEYTAB_PATH);
        if (keytab.exists()) {
            keytab.delete();
        }
    }

    public static void createTestDir() {
        workDirParent = new File(java.lang.System.getProperty("test.dir", "build"));
        if (!workDirParent.exists()) {
            workDirParent.mkdirs();
        }
    }

    public static void createMiniKdcConf() {
        conf = LocalKdc.createConf();
        conf.setProperty("org.name", "CERN");
        conf.setProperty("org.domain", "CH");
        //        Uncomment to turn on debug messages.
        //        conf.setProperty("debug", "true");
    }

    @Configuration
    static class TestConfig {

        @Bean
        @Profile("dev")
        public DevPermissionResolver devPermissionResolver() {
            return new DevPermissionResolver();
        }
    }

}
