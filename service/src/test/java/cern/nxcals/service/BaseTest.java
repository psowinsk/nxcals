/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service;

import cern.nxcals.common.domain.VariableConfigData;
import cern.nxcals.common.domain.VariableData;
import cern.nxcals.common.utils.KeyValuesUtils;
import cern.nxcals.common.utils.TimeUtils;
import cern.nxcals.service.domain.Entity;
import cern.nxcals.service.domain.EntityHistory;
import cern.nxcals.service.domain.Partition;
import cern.nxcals.service.domain.Schema;
import cern.nxcals.service.domain.System;
import cern.nxcals.service.domain.Variable;
import cern.nxcals.service.domain.security.Permission;
import cern.nxcals.service.domain.security.Realm;
import cern.nxcals.service.domain.security.Role;
import cern.nxcals.service.domain.security.User;
import cern.nxcals.service.internal.InternalEntityService;
import cern.nxcals.service.repository.EntityHistoryRepository;
import cern.nxcals.service.repository.EntityRepository;
import cern.nxcals.service.repository.PartitionRepository;
import cern.nxcals.service.repository.SchemaRepository;
import cern.nxcals.service.repository.SystemRepository;
import cern.nxcals.service.repository.VariableRepository;
import cern.nxcals.service.repository.security.PermissionRepository;
import cern.nxcals.service.repository.security.RealmRepository;
import cern.nxcals.service.repository.security.RoleRepository;
import cern.nxcals.service.repository.security.UserRepository;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;

import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.TIME_SCHEMA;

//FIXME tests based on this class are integration tests and not really unit tests
// This makes it hard to understand, change and maintain. Also slows down significantly the test execution.
// It should be changed so that we actually mock the next layers instead of recreating the whole context every single time
@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ApplicationDev.class)
@TestPropertySource(locations = "classpath:application.properties")
@ActiveProfiles("test")
@Transactional
public abstract class BaseTest implements ApplicationContextAware {
    protected final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd")
            .withZone(ZoneOffset.UTC);

    @Value("${data.location.prefix}")
    protected String dataLocationPrefix;
    @Value("${data.location.suffix}")
    protected String dataLocationSuffix;
    @Value("${data.location.hbase.tablespace}")
    protected String hbaseTablespace;

    protected static final String TEST_REALM_NAME = "UNIT_TEST.CH";
    protected static final String TEST_USER_NAME = "UNIT_TEST_USER";
    protected static final String TEST_ROLE_NAME = "UNIT_TEST_ROLE";
    protected static final List<String> TEST_PERMISSIONNS = Lists
            .newArrayList("UNIT_TEST_PERMISSION:READ", "UNIT_TEST_PERMISSION:WRITE");
    public static final String TEST_NAME = "Test Name";
    protected static final String TEST_DEF_CONTENT = "Test Content";

    public static final long TEST_RECORD_TIME = 1000000000L;
    protected static final String WRITE_PERMISSION = "WRITE";
    public static final String SYSTEM_NAME = "SYSTEM_NAME";
    public static final String SYSTEM_NAME2 = "SYSTEM_NAME2";
    public static final String AUTHORITY = SYSTEM_NAME + ":" + WRITE_PERMISSION;
    public static final String AUTHORITY2 = SYSTEM_NAME2 + ":" + WRITE_PERMISSION;
    public static final String SYSTEMS_SEARCH_FIND_BY_NAME_ENDPOINT = "/systems/search/findByName?name=%s";
    public static final String PARTITION_FIND_BY_SYSTEM_ID_KEYVALUES_ENDPOINT =
            "/partitions/search/findBySystemIdAndKeyValues?"
                    + "systemId=%s&"
                    + "partitionKeyValues=%s";

    public static final String VARIABLE = "VARIABLE";
    public static final String VARIABLE_AUTHORITY = VARIABLE + ":" + WRITE_PERMISSION;

    @PersistenceContext
    protected EntityManager entityManager;
    @Autowired
    protected SystemRepository systemRepository;
    @Autowired
    protected EntityRepository entityRepository;
    @Autowired
    protected SchemaRepository schemaRepository;
    @Autowired
    protected PartitionRepository partitionRepository;
    @Autowired
    protected EntityHistoryRepository entityHistRepository;
    @Autowired
    protected UserRepository userRepository;
    @Autowired
    protected RoleRepository roleRepository;
    @Autowired
    protected PermissionRepository permissionRepository;
    @Autowired
    protected RealmRepository realmRepository;
    @Autowired
    protected VariableRepository variableRepository;
    @Autowired
    protected InternalEntityService service;

    @Override
    public void setApplicationContext(ApplicationContext context) throws BeansException {
        IdGeneratorFactory.setContext(context);
    }

    protected System createAndPersistClientSystem(String systemName, org.apache.avro.Schema entitySchema,
            org.apache.avro.Schema partitionSchema, org.apache.avro.Schema timeSchema) {
        System system = new System();
        if (entitySchema != null) {
            system.setEntityKeyDefs(entitySchema.toString());
        }
        if (partitionSchema != null) {
            system.setPartitionKeyDefs(partitionSchema.toString());
        }
        if (timeSchema != null) {
            system.setTimeKeyDefs(timeSchema.toString());
        }
        if (systemName != null) {
            system.setName(systemName);
        }
        return systemRepository.save(system);
    }

    protected Partition createPartition(System system, Map<String, Object> partitionKeyValues,
            org.apache.avro.Schema schema) {
        Partition partition = new Partition();
        partition.setSystem(system);
        String keyValuesContent = partitionKeyValues != null ?
                KeyValuesUtils.convertMapIntoAvroSchemaString(partitionKeyValues, schema.toString()) :
                null;
        partition.setKeyValues(keyValuesContent);
        return partition;
    }

    protected Partition createPartition(String systemName, Map<String, Object> partitionKeyValues,
            org.apache.avro.Schema schema) {
        System system = createAndPersistClientSystem(systemName, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA);
        return createPartition(system, partitionKeyValues, schema);
    }

    protected Schema createSchema(org.apache.avro.Schema schema) {
        Schema newSchema = new Schema();
        String schemaContent = schema != null ? schema.toString() : null;
        newSchema.setContent(schemaContent);
        if (schemaContent != null) {
            newSchema.setContentHash(DigestUtils.md5Hex(schemaContent));
        }
        return newSchema;
    }

    protected Entity createEntity(String systemName,
            Map<String, Object> entityKeyValues, org.apache.avro.Schema entitySchema,
            Map<String, Object> partitionKeyValues, org.apache.avro.Schema partitionSchema) {
        return createEntity(systemName, entityKeyValues, entitySchema, partitionKeyValues, partitionSchema, null);
    }

    protected Entity createEntity(String systemName,
            Map<String, Object> entityKeyValues, org.apache.avro.Schema entitySchema,
            Map<String, Object> partitionKeyValues, org.apache.avro.Schema partitionSchema, Instant lockedTimestamp) {
        Entity entity = new Entity();
        String entityKeyValuesString = entityKeyValues != null ?
                KeyValuesUtils.convertMapIntoAvroSchemaString(entityKeyValues, entitySchema.toString()) :
                null;
        entity.setKeyValues(entityKeyValuesString);
        entity.setSchema(createSchema(entitySchema));
        entity.setPartition(createPartition(systemName, partitionKeyValues, partitionSchema));
        if (lockedTimestamp != null) {
            entity.setLockedUntilStamp(lockedTimestamp);
        }
        return entity;
    }

    protected Entity createAndPersistEntity(String systemName,
            Map<String, Object> entityKeyValues, org.apache.avro.Schema entitySchema,
            Map<String, Object> partitionKeyValues, org.apache.avro.Schema partitionSchema) {
        return createAndPersistEntity(systemName, entityKeyValues, entitySchema, partitionKeyValues, partitionSchema,
                null);
    }

    protected Entity createAndPersistEntity(String systemName,
            Map<String, Object> entityKeyValues, org.apache.avro.Schema entitySchema,
            Map<String, Object> partitionKeyValues, org.apache.avro.Schema partitionSchema,
            Instant lockedTimestamp) {
        return persistEntity(createEntity(systemName, entityKeyValues, entitySchema, partitionKeyValues,
                partitionSchema, lockedTimestamp));
    }

    protected Entity persistEntity(Entity entity) {
        if (entity.getPartition() != null) {
            partitionRepository.save(entity.getPartition());
        }

        if (entity.getSchema() != null) {
            schemaRepository.save(entity.getSchema());
        }

        return entityRepository.save(entity);
    }

    protected Variable createAndPersistVariable(String variableName, String variableDescription, Instant creationTime) {
        Variable variable = new Variable(variableName, variableDescription, creationTime);
        return variableRepository.save(variable);
    }

    protected Entity createAndSaveDefaultTestEntityKey() {
        Entity entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1, PARTITION_KEY_VALUES_1,
                PARTITION_SCHEMA_1);
        createAndPersistEntityHist(entity, entity.getPartition(), entity.getSchema(), TEST_RECORD_TIME);
        return entity;
    }

    // FIXME entity already has partition and schema, do we use this to force different partitions ands schemas somewhere? If not receive only entity.
    protected EntityHistory createAndPersistEntityHist(Entity key, Partition part, Schema schema,
            Long recordTimestamp) {
        EntityHistory history = new EntityHistory();
        history.setEntity(key);
        history.setPartition(part);
        history.setSchema(schema);
        if (recordTimestamp != null) {
            history.setValidFromStamp(TimeUtils.getInstantFromNanos(recordTimestamp));
        }
        if (key != null) {
            key.addEntityHist(history);
        }
        return this.entityHistRepository.save(history);
    }

    protected EntityHistory createAndPersistEntityHist(Entity key, Partition part, Schema schema, Long fromTimestamp,
            Long toTimestamp) {
        EntityHistory history = new EntityHistory();
        history.setEntity(key);
        history.setPartition(part);
        history.setSchema(schema);
        if (fromTimestamp != null) {
            history.setValidFromStamp(TimeUtils.getInstantFromNanos(fromTimestamp));
        }
        if (toTimestamp != null) {
            history.setValidToStamp(TimeUtils.getInstantFromNanos(toTimestamp));
        }

        if (key != null) {
            key.addEntityHist(history);
        }
        return entityHistRepository.save(history);

    }
/*
    protected Entity createAndSaveDefaultTestEntityKeyWithRandomSchemaContent() {
        Partition part = this.createAndPersistPartitionKey(TEST_NAME, SCHEMA_VALUE);

        String randomScemaContent = String.format("{\"class\":\"%s\", \"property\":\"string\"}",
                UUID.randomUUID().toString());
        Schema schema = this.createAndPersistSchema(randomScemaContent);
        Entity key = this.createAndPersistEntity(schema, part, TEST_KEY_VALUE);
        createAndPersistEntityHistory(key, part, schema, TEST_RECORD_TIME, null);
        createAndPersistEntityHistory(key, part, schema, TEST_RECORD_TIME + 1000, TEST_RECORD_TIME + 3000);
        createAndPersistEntityHistory(key, part, schema, TEST_RECORD_TIME + 3000, TEST_RECORD_TIME + 5000);
        // this.entityManager.flush();

        return key;
    }
*/

    protected EntityHistory createAndPersistEntityHistory(Entity key, Partition part, Schema schema,
            Long recordTimestamp) {
        EntityHistory firstHistory = new EntityHistory();
        firstHistory.setEntity(key);
        firstHistory.setPartition(part);
        firstHistory.setSchema(schema);
        if (recordTimestamp != null) {
            firstHistory.setValidFromStamp(TimeUtils.getInstantFromNanos(recordTimestamp));
        }
        if (key != null) {
            key.addEntityHist(firstHistory);
        }
        return this.entityHistRepository.save(firstHistory);
    }

    protected EntityHistory createAndPersistDefaultTestEntityKeyHist() {
        Entity entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1, PARTITION_KEY_VALUES_1,
                PARTITION_SCHEMA_1);
        EntityHistory entityHistory = createAndPersistEntityHistory(entity, entity.getPartition(), entity.getSchema(),
                TEST_RECORD_TIME);
        this.entityRepository.save(entity);
        return entityHistory;
    }

    protected void createAndPersistUser() {
        Realm realm = new Realm();
        User user1 = new User();
        Role role1 = new Role();
        Permission permission = new Permission();
        Permission permission2 = new Permission();

        realm.setRealmName(TEST_REALM_NAME);
        realm.setUsers(Collections.singleton(user1));

        user1.setUserName(TEST_USER_NAME);
        user1.setRealm(realm);
        user1.setRoles(Collections.singleton(role1));

        role1.setRoleName(TEST_ROLE_NAME);
        role1.setUsers(Collections.singleton(user1));
        role1.setPermissions(Sets.newHashSet(permission, permission2));

        permission.setPermissionName(TEST_PERMISSIONNS.get(0));
        permission.setRoles(Collections.singleton(role1));

        permission2.setPermissionName(TEST_PERMISSIONNS.get(1));
        permission2.setRoles(Collections.singleton(role1));

        userRepository.save(user1);
    }

    protected VariableConfigData createVariableConfigData(long entityId, String fieldName, Long validFromStamp,
            Long validToStamp) {
        return VariableConfigData.builder().entityId(entityId).fieldName(fieldName).validFromStamp(validFromStamp)
                .validToStamp(validToStamp).build();
    }

    protected VariableData createVariableData(String variableName, String description, Long creationTimeUtc,
            SortedSet<VariableConfigData> variableConfigData) {
        return VariableData.builder()
                .name(variableName)
                .description(description)
                .creationTimeUtc(creationTimeUtc)
                .variableConfigData(variableConfigData)
                .build();
    }
}
