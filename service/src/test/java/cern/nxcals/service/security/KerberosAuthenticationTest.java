/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.security;

import cern.nxcals.common.domain.SystemData;
import cern.nxcals.service.ApplicationDev;
import cern.nxcals.service.BaseTest;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.security.kerberos.client.KerberosRestTemplate;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;

import java.net.InetAddress;
import java.net.UnknownHostException;

import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.TIME_SCHEMA;
import static java.lang.String.format;

@SpringBootTest(classes = ApplicationDev.class, webEnvironment = WebEnvironment.DEFINED_PORT)
@TestPropertySource(locations = "classpath:kerberos.application.properties")
@Ignore("Won't pass on jenkins or with junit db. Still useful for debugging security in local env.")
@Transactional(transactionManager = "jpaTransactionManager")
public class KerberosAuthenticationTest extends BaseTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(KerberosAuthenticationTest.class);

    @Value("${service.kerberos.keytab}")
    private String KEYTAB_PATH;
    private static final String TEST_PRINCIPAL = "mock-system-user";
    public static final String MOCK_SYSTEM_NAME = "MOCK-SYSTEM";
    private static String HOST;

    static {
        try {
            HOST = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException ex) {
            LOGGER.error("Could not get localhost name.", ex);
        }
    }

    @LocalServerPort
    private int port;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        //TODO: Move it to configuration
        java.lang.System.setProperty("javax.net.ssl.trustStore", "../selfsigned.jks");
    }

    @Test
    @Rollback
    public void shouldAuthenticateToFindSystem() {
        cern.nxcals.service.domain.System system = createAndPersistClientSystem(MOCK_SYSTEM_NAME,
                ENTITY_SCHEMA_1,
                PARTITION_SCHEMA_1, TIME_SCHEMA);
        String endpoint = format(SYSTEMS_SEARCH_FIND_BY_NAME_ENDPOINT, system.getName());

        KerberosRestTemplate restTemplate = new KerberosRestTemplate(KEYTAB_PATH, TEST_PRINCIPAL + "@CERN.CH");
        restTemplate.getForObject("https://" + HOST + ":" + port + endpoint,
                SystemData.class);
    }

    @Test
    public void shouldNotAuthenticate() {
        String endpoint = format(PARTITION_FIND_BY_SYSTEM_ID_KEYVALUES_ENDPOINT, "", "");

        thrown.expect(RestClientException.class);
        thrown.expectMessage("Unable to obtain password from user");

        KerberosRestTemplate restTemplate = new KerberosRestTemplate(KEYTAB_PATH, TEST_PRINCIPAL + "-fail@CERN.CH");
        restTemplate.getForObject("https://" + HOST + ":" + port + endpoint, String.class);
    }
}