package cern.nxcals.service.security.resolvers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class PermissionResolverTest {

    private static String PERMISSION_AREA = "AREA";
    private static String PERMISSION_TYPE = "TYPE";
    private static String ALL_PERMISSION_STRING = "ALL";

    private ConcatenatingPermissionResolver concatenatingPermissionResolver;

    private DevPermissionResolver devPermissionResolver;

    @Before
    public void setUp() {
        concatenatingPermissionResolver = new ConcatenatingPermissionResolver();
        devPermissionResolver = new DevPermissionResolver();
    }

    @Test
    public void shouldConcatenateStringWithColon() {
        assertEquals(PERMISSION_AREA + ":" + PERMISSION_TYPE,
                concatenatingPermissionResolver.getPermissionString(PERMISSION_AREA, PERMISSION_TYPE));
    }

    @Test
    public void shouldReturnAllPermissionString() {
        assertEquals(ALL_PERMISSION_STRING,
                devPermissionResolver.getPermissionString(PERMISSION_AREA, PERMISSION_TYPE));
    }
}
