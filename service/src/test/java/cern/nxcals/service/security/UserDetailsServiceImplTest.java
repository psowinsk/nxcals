package cern.nxcals.service.security;

import cern.nxcals.service.BaseTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author kpodsiad
 */
public class UserDetailsServiceImplTest extends BaseTest {

    @Autowired
    private UserDetailsServiceImpl service;

    @Test
    @Rollback
    @Transactional(transactionManager = "jpaTransactionManager")
    public void loadUserByUsername() throws Exception {
        createAndPersistUser();
        String kerberosUsername = TEST_USER_NAME + "@" + TEST_REALM_NAME;
        UserDetails userDetails = service.loadUserByUsername(kerberosUsername);

        assertEquals(kerberosUsername, userDetails.getUsername());
        assertTrue(userDetails.getAuthorities().stream().map(GrantedAuthority::getAuthority)
                .anyMatch(TEST_PERMISSIONNS::contains));
    }

}