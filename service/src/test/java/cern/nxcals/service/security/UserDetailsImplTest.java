/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.security;

import cern.nxcals.service.BaseTest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@Transactional(transactionManager = "jpaTransactionManager")
public class UserDetailsImplTest extends BaseTest {

    private static final String TEST_AUTHORITY_A = "AAA";
    private static final String TEST_AUTHORITY_B = "BBB";
    private static final String TEST_AUTHORITY_C = "CCC";
    private UserDetailsImpl user;

    @Before
    public void setup() {
        user = UserDetailsImpl.builder(TEST_NAME)
                .authorities(Arrays.asList(new SimpleGrantedAuthority(TEST_AUTHORITY_C),
                        new SimpleGrantedAuthority(TEST_AUTHORITY_A),
                        new SimpleGrantedAuthority(TEST_AUTHORITY_B)))
                .build();
    }

    @Test
    public void getAuthorities() {
        String[] actual = user.getAuthorities().stream().map(GrantedAuthority::getAuthority).toArray(String[]::new);
        String[] expected = { TEST_AUTHORITY_A, TEST_AUTHORITY_B, TEST_AUTHORITY_C };

        assertArrayEquals(expected, actual);
    }

    @Test
    public void getPassword() {
        assertNull(user.getPassword());
    }

    @Test
    public void equals() {
        UserDetailsImpl user2 = UserDetailsImpl.builder(TEST_NAME)
                .authorities(Arrays.asList(new SimpleGrantedAuthority(TEST_AUTHORITY_C),
                        new SimpleGrantedAuthority(TEST_AUTHORITY_A),
                        new SimpleGrantedAuthority(TEST_AUTHORITY_B)))
                .build();
        assertEquals(user2, user);
        assertEquals(user2.hashCode(), user.hashCode());
    }

    @Test
    public void equals2() {
        UserDetailsImpl user2 = UserDetailsImpl.builder("fail")
                .authorities(Arrays.asList(new SimpleGrantedAuthority(TEST_AUTHORITY_C),
                        new SimpleGrantedAuthority(TEST_AUTHORITY_A),
                        new SimpleGrantedAuthority(TEST_AUTHORITY_B)))
                .build();
        assertNotEquals(user2, user);
        assertNotEquals(user2.hashCode(), user.hashCode());
    }

    @Test
    public void withUsername() {
        String[] expectedAuthorities = { TEST_AUTHORITY_A, TEST_AUTHORITY_B, TEST_AUTHORITY_C };
        UserDetailsImpl userFromBuilder = UserDetailsImpl.builder(TEST_NAME).authorities(expectedAuthorities).build();
        assertEquals(TEST_NAME, userFromBuilder.getUsername());
        assertNull(userFromBuilder.getPassword());
        assertTrue(userFromBuilder.isAccountNonExpired());
        assertTrue(userFromBuilder.isAccountNonLocked());
        assertTrue(userFromBuilder.isCredentialsNonExpired());
        assertTrue(userFromBuilder.isEnabled());
        assertArrayEquals(expectedAuthorities,
                userFromBuilder.getAuthorities().stream().map(GrantedAuthority::getAuthority).toArray(String[]::new));
    }

}