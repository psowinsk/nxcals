/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.security;

import cern.nxcals.service.BaseTest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Transactional(transactionManager = "jpaTransactionManager")
public class RootChangeMethodSecurityExpressionHandlerTest extends BaseTest {

    private RootChangeMethodSecurityExpressionHandler expressionHandler;

    @Before
    public void setup() {
        expressionHandler = new RootChangeMethodSecurityExpressionHandler();
    }

    @Test
    @WithMockUser
    public void shouldCreateSecurityExpressionRoot() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        MethodSecurityExpressionOperations securityExpressionRoot = expressionHandler.createSecurityExpressionRoot(
                authentication, null);
        assertNotNull(securityExpressionRoot);
        assertEquals(authentication, securityExpressionRoot.getAuthentication());
    }

}