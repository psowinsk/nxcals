/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.security;

import cern.nxcals.common.domain.EntityData;
import cern.nxcals.common.domain.SystemData;
import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.Entity;
import cern.nxcals.service.domain.Partition;
import cern.nxcals.service.domain.System;
import cern.nxcals.service.repository.EntityRepository;
import cern.nxcals.service.repository.SystemRepository;
import cern.nxcals.service.security.resolvers.PermissionResolver;
import com.google.common.collect.Sets;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@Transactional(transactionManager = "jpaTransactionManager")
public class MethodSecurityExpressionRootTest extends BaseTest {

    private final static long SYSTEM_ID = 1L;

    private final static long ENTITY_ID = 2L;

    @Mock
    private SystemRepository systemRepository;

    @Mock
    private EntityRepository entityRepository;

    @Mock
    private PermissionResolver permissionResolver1;

    @Mock
    private PermissionResolver permissionResolver2;

    @Mock
    private PermissionResolver permissionResolver3;

    @Mock
    private EntityData entityData1;

    @Mock
    private EntityData entityData2;

    @Mock
    private EntityData entityData3;

    @Mock
    private SystemData systemData1;

    @Mock
    private SystemData systemData2;

    @Mock
    private SystemData systemData3;

    private MethodSecurityExpressionRoot securityExpressionRoot;

    @Before
    public void setup() {
        when(entityData1.getSystemData()).thenReturn(systemData1);
        when(entityData2.getSystemData()).thenReturn(systemData2);
        when(entityData3.getSystemData()).thenReturn(systemData3);
        this.securityExpressionRoot = new MethodSecurityExpressionRoot(
                SecurityContextHolder.getContext().getAuthentication(), systemRepository, entityRepository,
                Sets.newHashSet(permissionResolver1, permissionResolver2, permissionResolver3));
    }

    @Test
    @WithMockUser(authorities = BaseTest.AUTHORITY)
    public void shouldAcceptAfterSystemChecking() {
        when(permissionResolver1.getPermissionString(eq(SYSTEM_NAME), eq(WRITE_PERMISSION)))
                .thenReturn(SYSTEM_NAME + ":" + WRITE_PERMISSION);
        System system = new System();
        system.setName(SYSTEM_NAME);

        assertTrue(securityExpressionRoot.hasSystemPermission(system, WRITE_PERMISSION));
    }

    @Test
    @WithMockUser(authorities = BaseTest.AUTHORITY)
    public void shouldDenyAfterSystemWithWrongNameChecking() {
        String failName = "fail";
        when(permissionResolver1.getPermissionString(eq(failName), eq(WRITE_PERMISSION)))
                .thenReturn(failName + ":" + WRITE_PERMISSION);
        System system = new System();
        system.setName(failName);

        assertFalse(securityExpressionRoot.hasSystemPermission(system, WRITE_PERMISSION));
    }

    @Test
    @WithMockUser(authorities = BaseTest.AUTHORITY)
    public void shouldDenyAfterSystemCheckingWithWrongPermission() {
        when(permissionResolver1.getPermissionString(eq(SYSTEM_NAME), eq(WRITE_PERMISSION)))
                .thenReturn(SYSTEM_NAME + ":" + WRITE_PERMISSION);
        System system = new System();
        system.setName(SYSTEM_NAME);

        assertFalse(securityExpressionRoot.hasSystemPermission(system, "fail"));
    }

    @Test
    @WithMockUser(authorities = BaseTest.AUTHORITY)
    public void shouldAcceptAfterSystemIdChecking() {
        when(permissionResolver1.getPermissionString(eq(SYSTEM_NAME), eq(WRITE_PERMISSION)))
                .thenReturn(SYSTEM_NAME + ":" + WRITE_PERMISSION);
        System system = new System();
        system.setName(SYSTEM_NAME);
        when(systemRepository.findById(SYSTEM_ID)).thenReturn(Optional.of(system));

        assertTrue(securityExpressionRoot.hasSystemPermission(SYSTEM_ID, WRITE_PERMISSION));
    }

    @Test
    @WithMockUser(authorities = BaseTest.AUTHORITY)
    public void shouldDenyAfterSysteIdmWithWrongNameChecking() {
        String failName = "fail";
        when(permissionResolver1.getPermissionString(eq(failName), eq(WRITE_PERMISSION)))
                .thenReturn(failName + ":" + WRITE_PERMISSION);
        System system = new System();
        system.setName(failName);
        when(systemRepository.findById(SYSTEM_ID)).thenReturn(Optional.of(system));

        assertFalse(securityExpressionRoot.hasSystemPermission(SYSTEM_ID, WRITE_PERMISSION));
    }

    @Test
    @WithMockUser(authorities = BaseTest.AUTHORITY)
    public void shouldDenyAfterSystemIdCheckingWithWrongPermission() {
        when(permissionResolver1.getPermissionString(eq(SYSTEM_NAME), eq(WRITE_PERMISSION)))
                .thenReturn(SYSTEM_NAME + ":" + WRITE_PERMISSION);
        System system = new System();
        system.setName(SYSTEM_NAME);
        when(systemRepository.findById(SYSTEM_ID)).thenReturn(Optional.of(system));

        assertFalse(securityExpressionRoot.hasSystemPermission(SYSTEM_ID, "fail"));
    }

    @Test
    @WithMockUser(authorities = BaseTest.AUTHORITY)
    public void shouldAuthorizeIfAnyResolverReturnsMatchingString() {
        when(permissionResolver1.getPermissionString(eq(SYSTEM_NAME), eq(WRITE_PERMISSION))).thenReturn("STH ELSE");
        when(permissionResolver2.getPermissionString(eq(SYSTEM_NAME), eq(WRITE_PERMISSION)))
                .thenReturn(SYSTEM_NAME + ":" + WRITE_PERMISSION);
        when(permissionResolver3.getPermissionString(eq(SYSTEM_NAME), eq(WRITE_PERMISSION))).thenReturn("WRONG");
        System system = new System();
        system.setName(SYSTEM_NAME);

        assertTrue(securityExpressionRoot.hasSystemPermission(system, WRITE_PERMISSION));
    }

    @Test
    @WithMockUser(authorities = BaseTest.AUTHORITY)
    public void shouldFailIfAllResolverReturnsNotMatchingString() {
        when(permissionResolver1.getPermissionString(eq(SYSTEM_NAME), eq(WRITE_PERMISSION))).thenReturn("STH ELSE");
        when(permissionResolver2.getPermissionString(eq(SYSTEM_NAME), eq(WRITE_PERMISSION))).thenReturn("WRONG");
        when(permissionResolver3.getPermissionString(eq(SYSTEM_NAME), eq(WRITE_PERMISSION))).thenReturn("ALSO WRONG");
        System system = new System();
        system.setName(SYSTEM_NAME);

        assertFalse(securityExpressionRoot.hasSystemPermission(system, WRITE_PERMISSION));
    }

    @Test
    @WithMockUser(authorities = { BaseTest.AUTHORITY, BaseTest.AUTHORITY2 })
    public void shouldAuthorizeIfAllSystemsOfEntitiesArePermitted() {
        when(permissionResolver1.getPermissionString(eq(SYSTEM_NAME), eq(WRITE_PERMISSION))).thenReturn(AUTHORITY);
        when(permissionResolver2.getPermissionString(eq(SYSTEM_NAME2), eq(WRITE_PERMISSION))).thenReturn(AUTHORITY2);
        when(systemData1.getName()).thenReturn(SYSTEM_NAME);
        when(systemData2.getName()).thenReturn(SYSTEM_NAME2);
        when(systemData3.getName()).thenReturn(SYSTEM_NAME);
        assertTrue(securityExpressionRoot
                .hasEntityPermission(Lists.newArrayList(entityData1, entityData2, entityData3), WRITE_PERMISSION));
    }

    @Test
    @WithMockUser(authorities = { BaseTest.AUTHORITY, BaseTest.AUTHORITY2 })
    public void shouldAuthorizeIfAllSystemsOfEntitiesArePermitted2() {
        when(permissionResolver1.getPermissionString(eq(SYSTEM_NAME), eq(WRITE_PERMISSION))).thenReturn(AUTHORITY);
        when(permissionResolver2.getPermissionString(eq(SYSTEM_NAME2), eq(WRITE_PERMISSION))).thenReturn(AUTHORITY2);
        when(systemData1.getName()).thenReturn(SYSTEM_NAME);
        when(systemData2.getName()).thenReturn(SYSTEM_NAME);
        when(systemData3.getName()).thenReturn(SYSTEM_NAME);
        assertTrue(securityExpressionRoot
                .hasEntityPermission(Lists.newArrayList(entityData1, entityData2, entityData3), WRITE_PERMISSION));
    }

    @Test
    @WithMockUser(authorities = { BaseTest.AUTHORITY, BaseTest.AUTHORITY2 })
    public void shouldNotAuthorizeIfAnySystemOfEntitiesIsNotPermitted() {
        when(permissionResolver1.getPermissionString(eq(SYSTEM_NAME), eq(WRITE_PERMISSION))).thenReturn(AUTHORITY);
        when(systemData1.getName()).thenReturn(SYSTEM_NAME);
        when(systemData2.getName()).thenReturn(SYSTEM_NAME2);
        when(systemData3.getName()).thenReturn(SYSTEM_NAME);
        assertFalse(securityExpressionRoot
                .hasEntityPermission(Lists.newArrayList(entityData1, entityData2, entityData3), WRITE_PERMISSION));
    }

    @Test
    @WithMockUser(authorities = BaseTest.AUTHORITY)
    public void shouldAcceptAfterEntityIdChecking() {
        when(permissionResolver1.getPermissionString(eq(SYSTEM_NAME), eq(WRITE_PERMISSION)))
                .thenReturn(SYSTEM_NAME + ":" + WRITE_PERMISSION);
        System system = new System();
        system.setName(SYSTEM_NAME);
        Entity entity = new Entity();
        Partition partition = new Partition();
        partition.setSystem(system);
        entity.setPartition(partition);
        when(systemRepository.findById(SYSTEM_ID)).thenReturn(Optional.of(system));
        when(entityRepository.findById(ENTITY_ID)).thenReturn(Optional.of(entity));

        assertTrue(securityExpressionRoot.hasEntityPermission(ENTITY_ID, WRITE_PERMISSION));
    }

    @Test
    @WithMockUser(authorities = BaseTest.AUTHORITY)
    public void shouldDenyAfterEntityIdWithWrongNameChecking() {
        String failName = "fail";
        when(permissionResolver1.getPermissionString(eq(failName), eq(WRITE_PERMISSION)))
                .thenReturn(failName + ":" + WRITE_PERMISSION);
        System system = new System();
        system.setName(failName);
        Entity entity = new Entity();
        Partition partition = new Partition();
        partition.setSystem(system);
        entity.setPartition(partition);
        when(systemRepository.findById(SYSTEM_ID)).thenReturn(Optional.of(system));
        when(entityRepository.findById(ENTITY_ID)).thenReturn(Optional.of(entity));

        assertFalse(securityExpressionRoot.hasSystemPermission(1L, WRITE_PERMISSION));
    }

    @Test
    @WithMockUser(authorities = BaseTest.AUTHORITY)
    public void shouldDenyAfterEntityIdCheckingWithWrongPermission() {
        when(permissionResolver1.getPermissionString(eq(SYSTEM_NAME), eq(WRITE_PERMISSION)))
                .thenReturn(SYSTEM_NAME + ":" + WRITE_PERMISSION);
        System system = new System();
        system.setName(SYSTEM_NAME);
        Entity entity = new Entity();
        Partition partition = new Partition();
        partition.setSystem(system);
        entity.setPartition(partition);
        when(systemRepository.findById(SYSTEM_ID)).thenReturn(Optional.of(system));
        when(entityRepository.findById(ENTITY_ID)).thenReturn(Optional.of(entity));

        assertFalse(securityExpressionRoot.hasSystemPermission(1L, "fail"));
    }
}