/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.internal;

import cern.nxcals.common.utils.TimeUtils;
import cern.nxcals.service.config.CompactionConfigurationProperties;
import cern.nxcals.service.domain.Entity;
import cern.nxcals.service.domain.EntityHistory;
import cern.nxcals.service.domain.Partition;
import cern.nxcals.service.domain.Schema;
import cern.nxcals.service.domain.System;
import cern.nxcals.service.internal.InternalCompactionServiceImplTest.TestConfiguration;
import cern.nxcals.service.repository.EntityRepository;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource(locations = "classpath:application.properties")
@ActiveProfiles("test")
@Import({
        TestConfiguration.class
})
public class InternalCompactionServiceImplTest {

    @Profile("test")
    @Configuration
    static class TestConfiguration {
        @Bean
        public CompactionConfigurationProperties compactionConfigurationProperties(
                @Value(value = "${compaction.tolerance.period.hours}") long compactionToleranceHours,
                @Value(value = "${compaction.migration.tolerance.period.hours}") long compactionMigrationToleranceHours,
                @Value(value = "${compaction.min.file.modification.period.hours}") long minFileModificationBeforeCompactionHours) {
            return new CompactionConfigurationProperties(compactionToleranceHours, compactionMigrationToleranceHours,
                    minFileModificationBeforeCompactionHours);
        }

        @Bean
        public EntityRepository entityRepository() {
            return Mockito.mock(EntityRepository.class);
        }

        @Bean
        public InternalStorageService storageService() {
            return Mockito.mock(InternalStorageService.class);
        }

        @Bean
        public InternalCompactionService internalCompactionService(
                CompactionConfigurationProperties compactionConfigurationProperties) {
            return new InternalCompactionServiceImpl(entityRepository(), storageService(),
                    compactionConfigurationProperties);
        }

    }

    @Autowired
    private EntityRepository entityRepository;

    @Autowired
    private InternalStorageService storageService;

    @Autowired
    private InternalCompactionServiceImpl internalCompactionService;

    @After
    public void tearDown() {
        Mockito.reset(
                entityRepository,
                storageService
        );
    }

    @Test
    @Rollback
    public void shouldNotCompactExistingMigrationEntities() {
        //given
        List<Entity> entities = Arrays.asList(createDefaultTestEntity());
        Partition partition = entities.stream().map(Entity::getPartition).findFirst().get();
        String pathToTest = "system_dirs/" + partition.getSystem().getId() + "/" + partition.getId() + "/0/2017-01-01";

        //when
        when(entityRepository
                .countByPartitionSystemIdAndPartitionIdAndLockedUntilStampGreaterThanEqual(anyLong(), anyLong(),
                        any(Instant.class))).thenReturn(1L);
        when(storageService.countFilesUnderDirectoryThatModifiedLaterThan(eq(pathToTest), any()))
                .thenReturn(0L);

        boolean result = this.internalCompactionService.shouldCompact(pathToTest);

        //then
        Assert.assertNotNull(result);
        Assert.assertFalse(result);

        verify(entityRepository, times(1)).countByPartitionSystemIdAndPartitionIdAndLockedUntilStampGreaterThanEqual(
                anyLong(), anyLong(), any(Instant.class));
        verify(storageService, never()).countFilesUnderDirectoryThatModifiedLaterThan(eq(pathToTest), any());
    }

    @Test
    @Rollback
    public void shouldCompactNotExistingMigrationEntities() {
        //given
        List<Entity> entities = Collections.singletonList(createDefaultTestEntity());
        Partition partition = entities.stream().map(Entity::getPartition).findFirst().get();
        String pathToTest = "system_dirs/" + partition.getSystem().getId() + "/" + partition.getId() + "/0/2017-01-01";

        //when
        when(entityRepository
                .countByPartitionSystemIdAndPartitionIdAndLockedUntilStampGreaterThanEqual(anyLong(), anyLong(),
                        any(Instant.class))).thenReturn(0L);
        when(storageService.countFilesUnderDirectoryThatModifiedLaterThan(eq(pathToTest), any()))
                .thenReturn(0L);

        boolean result = this.internalCompactionService.shouldCompact(pathToTest);

        //then
        Assert.assertNotNull(result);
        Assert.assertTrue(result);

        verify(entityRepository, times(1)).countByPartitionSystemIdAndPartitionIdAndLockedUntilStampGreaterThanEqual(
                anyLong(), anyLong(), any(Instant.class));
        verify(storageService, only()).countFilesUnderDirectoryThatModifiedLaterThan(eq(pathToTest), any());
    }

    @Test
    @Rollback
    public void shouldNotCompactPathWithTheCurrentDate() {
        //given
        List<Entity> entities = Arrays.asList(createDefaultTestEntity());
        Partition partition = entities.stream().map(Entity::getPartition).findFirst().get();
        Path currentDate = Paths
                .get(ZonedDateTime.now(ZoneId.of("UTC")).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
                        .toString());
        String pathToTest =
                "system_dirs/" + partition.getSystem().getId() + "/" + partition.getId() + "/0/" + currentDate;

        //when
        when(entityRepository
                .countByPartitionSystemIdAndPartitionIdAndLockedUntilStampGreaterThanEqual(anyLong(), anyLong(),
                        any(Instant.class))).thenReturn(0L);
        when(storageService.countFilesUnderDirectoryThatModifiedLaterThan(eq(pathToTest), any()))
                .thenReturn(0L);
        boolean result = this.internalCompactionService.shouldCompact(pathToTest);

        //then
        Assert.assertNotNull(result);
        Assert.assertFalse(result);

        verify(entityRepository, never()).countByPartitionSystemIdAndPartitionIdAndLockedUntilStampGreaterThanEqual(
                anyLong(), anyLong(), any(Instant.class));
        verify(storageService, never()).countFilesUnderDirectoryThatModifiedLaterThan(eq(pathToTest), any());
    }

    @Test
    @Rollback
    public void shouldNotCompactPathWithRecentlyModifiedFiles() {
        //given
        List<Entity> entities = Collections.singletonList(createDefaultTestEntity());
        Partition partition = entities.stream().map(Entity::getPartition).findFirst().get();
        String pathToTest = "system_dirs/" + partition.getSystem().getId() + "/" + partition.getId() + "/0/2017-01-01";

        //when
        when(entityRepository
                .countByPartitionSystemIdAndPartitionIdAndLockedUntilStampGreaterThanEqual(anyLong(), anyLong(),
                        any(Instant.class))).thenReturn(0L);
        when(storageService.countFilesUnderDirectoryThatModifiedLaterThan(eq(pathToTest), any()))
                .thenReturn(3L);

        boolean result = this.internalCompactionService.shouldCompact(pathToTest);

        //then
        Assert.assertNotNull(result);
        Assert.assertFalse(result);

        verify(entityRepository, times(1)).countByPartitionSystemIdAndPartitionIdAndLockedUntilStampGreaterThanEqual(
                anyLong(), anyLong(), any(Instant.class));
        verify(storageService, only()).countFilesUnderDirectoryThatModifiedLaterThan(eq(pathToTest), any());
    }

    //  test helper methods
    protected Entity createDefaultTestEntity() {
        Partition part = this.createPartition("Test name", "Test Key Value");
        Schema schema = this.createNonPersistedSchema("{\"class\":\"string\", \"property\":\"string\"}");

        Entity entity = new Entity();
        entity.setId(20L);
        entity.setKeyValues("Test Key Value");
        entity.setSchema(schema);
        entity.setPartition(part);

        createAndAssociateNonPersistedEntityHist(entity, part, schema, 1000000000L);
        return entity;
    }

    protected Partition createPartition(String systemName, String hashValue) {
        System cs = this.createClientSystem(systemName);
        Partition part = new Partition();
        part.setId(22L);
        part.setSystem(cs);
        part.setKeyValues(hashValue);
        return part;
    }

    private System createClientSystem(String systemName) {
        System cs = new System();
        cs.setId(23L);
        cs.setEntityKeyDefs("{\"class\":\"string\", \"property\":\"string\"}");
        cs.setPartitionKeyDefs("{\"class\":\"string\", \"property\":\"string\"}");
        cs.setTimeKeyDefs("Test Content");
        cs.setName(systemName);
        return cs;
    }

    private Schema createNonPersistedSchema(String schemaContent) {
        Schema schema = new Schema();
        schema.setId(24L);
        schema.setContent(schemaContent);
        if (schemaContent != null) {
            schema.setContentHash(DigestUtils.md5Hex(schemaContent));
        }
        return schema;
    }

    private EntityHistory createAndAssociateNonPersistedEntityHist(Entity entity, Partition part, Schema schema,
            Long recordTimestamp) {
        EntityHistory history = new EntityHistory();
        history.setId(22L);
        history.setEntity(entity);

        history.setRecVersion(0L);
        history.setPartition(part);
        history.setSchema(schema);
        if (recordTimestamp != null) {
            history.setValidFromStamp(TimeUtils.getInstantFromNanos(recordTimestamp));
        }
        if (entity != null) {
            entity.addEntityHist(history);
        }
        return history;
    }

}
