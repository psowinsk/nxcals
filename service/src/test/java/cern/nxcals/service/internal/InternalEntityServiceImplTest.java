/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.internal;

import cern.nxcals.common.domain.EntityData;
import cern.nxcals.common.utils.TimeUtils;
import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.Entity;
import cern.nxcals.service.domain.EntityHistory;
import cern.nxcals.service.domain.Partition;
import cern.nxcals.service.domain.Schema;
import cern.nxcals.service.domain.System;
import cern.nxcals.service.rest.ConfigDataConflictException;
import cern.nxcals.service.rest.NotFoundRuntimeException;
import com.google.common.collect.Sets;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;
import java.util.stream.Collectors;

import static cern.nxcals.common.utils.KeyValuesUtils.convertMapIntoAvroSchemaString;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_2;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_2;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_2;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_STRING_SCHEMA_KEY_1;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_SCHEMA_2;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_STRING_SCHEMA_KEY_1;
import static cern.nxcals.service.rest.TestSchemas.TIME_SCHEMA;
import static cern.nxcals.service.rest.TestSchemas.createRandomSchema;
import static cern.nxcals.service.rest.TestSchemas.getFullSchema;
import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

@Transactional(transactionManager = "jpaTransactionManager")
public class InternalEntityServiceImplTest extends BaseTest {

    @Test(expected = NotFoundRuntimeException.class)
    @Rollback
    public void shouldNotCreateEntityKeyForNonExistingSystem() {
        Entity entity = service.findOrCreateEntityFor(-1L, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME);
        assertThat(entity).isNotNull();
    }

    @Test(expected = ConfigDataConflictException.class)
    @Rollback
    public void shouldNotGetResultForLateEntityWithSchemaNotPresentedInTheHistory() {
        System system = createAndPersistClientSystem(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA);
        Long systemId = system.getId();

        service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME);
        service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                ENTITY_SCHEMA_2.toString(), 100 * TEST_RECORD_TIME);
        service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                ENTITY_SCHEMA_2.toString(), TEST_RECORD_TIME + 10);
    }

    @Test(expected = ConfigDataConflictException.class)
    @Rollback
    public void shouldNotGetResultForLateEntityWithPartitionNotPresentedInTheHistory() {

        long systemId = createAndPersistClientSystem(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA)
                .getId();

        HashMap<String, Object> newPartitionKeyValues = new HashMap<>(PARTITION_KEY_VALUES_1);
        newPartitionKeyValues.put(PARTITION_STRING_SCHEMA_KEY_1, "new_value");

        service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME);

        service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, newPartitionKeyValues,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), 100 * TEST_RECORD_TIME);

        service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, newPartitionKeyValues,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME + 10);
    }

    @Test(expected = ConfigDataConflictException.class)
    @Rollback
    public void shouldRejectHistoryWithDifferentSchemaAndSameCreationTime() {
        Long systemId = createAndPersistClientSystem(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA)
                .getId();

        service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME);
        service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                ENTITY_SCHEMA_2.toString(), TEST_RECORD_TIME);
    }

    @Test
    @Rollback
    public void shouldCreateEntity() {
        Long systemId = createAndPersistClientSystem(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA)
                .getId();

        Long schemaId = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME).getSchema().getId();
        Optional<Schema> optionalSchema = schemaRepository.findById(schemaId);

        assertThat(optionalSchema).isPresent();

        Schema foundSchema = optionalSchema.get();
        assertThat(foundSchema.getContent()).isEqualTo(getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString());
    }

    @Test
    @Rollback
    public void shouldFindOrCreateEntityWithId() {
        Entity entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
        createAndPersistEntityHist(entity, entity.getPartition(), entity.getSchema(), TEST_RECORD_TIME);

        Entity foundEntity = service
                .findOrCreateEntityFor(entity.getPartition().getSystem().getId(), entity.getId(),
                        entity.getPartition().getId(), ENTITY_SCHEMA_1.toString(), TEST_RECORD_TIME);

        assertThat(foundEntity.getSchema().getContent()).isEqualTo(ENTITY_SCHEMA_1.toString());
        assertThat(foundEntity.getId()).isEqualTo(entity.getId());
    }

    @Test
    @Rollback
    public void shouldFindOrCreateEntityWithIdForNextTimestamp() {
        Entity entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
        createAndPersistEntityHist(entity, entity.getPartition(), entity.getSchema(), TEST_RECORD_TIME);

        Entity foundEntity = service.findOrCreateEntityFor(entity.getPartition().getSystem().getId(), entity.getId(),
                entity.getPartition().getId(), ENTITY_SCHEMA_1.toString(), TEST_RECORD_TIME + TEST_RECORD_TIME);

        assertThat(foundEntity.getSchema().getContent()).isEqualTo(ENTITY_SCHEMA_1.toString());
    }

    @Test
    @Rollback
    public void shouldFindOrCreateEntityWithIdForDifferentSchema() {
        Entity entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
        createAndPersistEntityHist(entity, entity.getPartition(), entity.getSchema(), TEST_RECORD_TIME);

        Entity foundEntity = service.findOrCreateEntityFor(entity.getPartition().getSystem().getId(), entity.getId(),
                entity.getPartition().getId(), ENTITY_SCHEMA_2.toString(), TEST_RECORD_TIME + TEST_RECORD_TIME);

        assertThat(foundEntity.getSchema().getContent()).isEqualTo(ENTITY_SCHEMA_2.toString());
        assertThat(foundEntity.getEntityHistories()).hasSize(1);
        EntityHistory entityHistory = foundEntity.getEntityHistories().first();

        assertThat(entityHistory.getValidFromStamp())
                .isEqualTo(TimeUtils.getInstantFromNanos(TEST_RECORD_TIME + TEST_RECORD_TIME));
        assertThat(entityHistory.getValidToStamp()).isNull();
    }

    private void verifyTest(String testSchemaContent) {
        Entity entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
        createAndPersistEntityHist(entity, entity.getPartition(), entity.getSchema(), TEST_RECORD_TIME);

        HashMap<String, Object> newPartitionKeyValues = new HashMap<>(PARTITION_KEY_VALUES_1);
        newPartitionKeyValues.put(PARTITION_STRING_SCHEMA_KEY_1, "new_value");

        Partition newPartition = createPartition(entity.getPartition().getSystem(), newPartitionKeyValues,
                PARTITION_SCHEMA_1);
        partitionRepository.save(newPartition);

        Entity foundEntity = service
                .findOrCreateEntityFor(entity.getPartition().getSystem().getId(), entity.getId(), newPartition.getId(),
                        testSchemaContent, TEST_RECORD_TIME + TEST_RECORD_TIME);

        assertThat(foundEntity.getSchema().getContent()).isEqualTo(testSchemaContent);
        assertThat(foundEntity.getEntityHistories()).hasSize(1);

        EntityHistory entityHistory = foundEntity.getEntityHistories().first();

        assertThat(entityHistory.getValidFromStamp())
                .isEqualTo(TimeUtils.getInstantFromNanos(TEST_RECORD_TIME + TEST_RECORD_TIME));
        assertThat(entityHistory.getValidToStamp()).isNull();
    }

    @Test
    @Rollback
    public void shouldFindOrCreateEntityWithIdForDifferentPartition() {
        verifyTest(ENTITY_SCHEMA_1.toString());
    }

    @Test
    @Rollback
    public void shouldFindOrCreateEntityWithIdForDifferentPartitionAndSchema() {
        verifyTest(ENTITY_SCHEMA_2.toString());
    }

    @Test(expected = ConfigDataConflictException.class)
    @Rollback
    public void shouldFailToFindOrCreateEntityWithIdForDifferentPartitionAndSchemaWithUsedTimestamp() {
        Entity entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
        createAndPersistEntityHist(entity, entity.getPartition(), entity.getSchema(), TEST_RECORD_TIME);

        HashMap<String, Object> newPartitionKeyValues = new HashMap<>(PARTITION_KEY_VALUES_1);
        newPartitionKeyValues.put(PARTITION_STRING_SCHEMA_KEY_1, "new_value");

        Partition newPartition = createPartition(entity.getPartition().getSystem(), newPartitionKeyValues,
                PARTITION_SCHEMA_1);
        partitionRepository.save(newPartition);

        service.findOrCreateEntityFor(entity.getPartition().getSystem().getId(), entity.getId(), newPartition.getId(),
                ENTITY_SCHEMA_2.toString(), TEST_RECORD_TIME);
    }

    @Test(expected = IllegalArgumentException.class)
    @Rollback
    public void shouldFailOnFindOrCreateEntityWithIdWithWrongSystem() {
        Entity entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
        createAndPersistEntityHist(entity, entity.getPartition(), entity.getSchema(), TEST_RECORD_TIME);

        //This is another system
        System system = createAndPersistClientSystem(TEST_NAME, ENTITY_SCHEMA_1, ENTITY_SCHEMA_1, ENTITY_SCHEMA_1);
        Long systemId = system.getId();

        Entity foundEntity = service.findOrCreateEntityFor(systemId, entity.getId(), entity.getPartition().getId(),
                ENTITY_SCHEMA_1.toString(), TEST_RECORD_TIME);
    }

    @Test
    @Rollback
    public void shouldUpdateEntitySchema() {
        Long systemId = createAndPersistClientSystem(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA)
                .getId();
        Entity entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1, PARTITION_KEY_VALUES_1,
                PARTITION_SCHEMA_1);

        createAndPersistEntityHist(entity, entity.getPartition(), entity.getSchema(), TEST_RECORD_TIME);

        // TODO: Confirm why in the previous test there was no need to increment the recordTimestamp, it should not be possible to send the same as far as I know?
        Long schemaId = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME + 10).getSchema()
                .getId();

        Optional<Schema> optionalSchema = schemaRepository.findById(schemaId);

        assertThat(optionalSchema).isPresent();

        Schema foundSchema = optionalSchema.get();

        assertThat(foundSchema.getContent()).isEqualTo(getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString());
        assertThat(foundSchema.getId()).isEqualTo(schemaId);
    }

    @Test
    @Rollback
    public void shouldUpdateEntityPartition() {
        Entity entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1, PARTITION_KEY_VALUES_1,
                PARTITION_SCHEMA_1);

        createAndPersistEntityHist(entity, entity.getPartition(), entity.getSchema(), TEST_RECORD_TIME);

        Map<String, Object> newPartitionKeyValues = new HashMap<>(PARTITION_KEY_VALUES_1);
        newPartitionKeyValues.put(PARTITION_STRING_SCHEMA_KEY_1, "new_value");

        Long systemId = entity.getPartition().getSystem().getId();

        Long entityId = service
                .findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, newPartitionKeyValues,
                        getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME + 10).getId();

        Optional<Entity> optionalEntity = entityRepository.findById(entityId);

        assertThat(optionalEntity).isPresent();

        Entity foundEntity = optionalEntity.get();

        assertThat(foundEntity.getPartition().getKeyValues())
                .isEqualTo(convertMapIntoAvroSchemaString(newPartitionKeyValues, PARTITION_SCHEMA_1.toString()));
    }

    @Test(expected = NotFoundRuntimeException.class)
    @Rollback
    public void shouldThrowExceptionOnUpdateWhenEntityDoesNotExists() {
        Entity entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1, PARTITION_KEY_VALUES_1,
                PARTITION_SCHEMA_1);

        Map<String, Object> newEntityKeyValues = new HashMap<>(ENTITY_KEY_VALUES_1);
        newEntityKeyValues.put(ENTITY_STRING_SCHEMA_KEY_1, "new_value");

        EntityData updatedEntityData = EntityData.builder(entity.toEntityData())
                .withNewEntityKeyValues(newEntityKeyValues)
                .build();

        Entity nonExistingEntity = createEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1, PARTITION_KEY_VALUES_1,
                PARTITION_SCHEMA_1);
        nonExistingEntity.setRecVersion(0L);
        EntityData nonExistingEntityData = EntityData.builder(nonExistingEntity.toEntityData())
                .build();

        service.updateEntities(Arrays.asList(updatedEntityData, nonExistingEntityData));
    }

    @Test(expected = org.springframework.orm.ObjectOptimisticLockingFailureException.class)
    @Rollback
    public void shouldThrowExceptionWhenProvidedUpdateEntityIsOlderVersion() {
        Entity entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1, PARTITION_KEY_VALUES_1,
                PARTITION_SCHEMA_1);

        Map<String, Object> oldEntityKeyValues = new HashMap<>(ENTITY_KEY_VALUES_1);
        oldEntityKeyValues.put(ENTITY_STRING_SCHEMA_KEY_1, "old_value");

        Map<String, Object> newEntityKeyValues = new HashMap<>(ENTITY_KEY_VALUES_1);
        newEntityKeyValues.put(ENTITY_STRING_SCHEMA_KEY_1, "new_value");

        EntityData oldEntityData = EntityData.builder(entity.toEntityData())
                .withNewEntityKeyValues(oldEntityKeyValues)
                .build();

        EntityData updatedEntityData = EntityData.builder(entity.toEntityData())
                .withNewEntityKeyValues(newEntityKeyValues)
                .build();

        service.updateEntities(Collections.singletonList(updatedEntityData));

        service.updateEntities(Collections.singletonList(oldEntityData));
    }

    @Test
    @Rollback
    public void shouldUpdateAndLockMoreThan1kEntities() {
        // This is mandatory to constantly ensure that we overcome the oracle limitations described by
        // issue: "ORA-01795: maximum number of expressions in a list is 1000"
        int items = 1234;

        List<EntityData> toUpdateEntities = new ArrayList<>();
        for (int i = 0; i < items; i++) {
            org.apache.avro.Schema randomSchema = createRandomSchema("entity_type_" + String.valueOf(i));
            Entity entity = createAndPersistEntity(TEST_NAME + i, ENTITY_KEY_VALUES_1, randomSchema,
                    PARTITION_KEY_VALUES_1,
                    PARTITION_SCHEMA_1);

            long lockUntilEpochNanos = TimeUtils.getNanosFromInstant(Instant.now().plus(2, ChronoUnit.HOURS));

            EntityData updatedEntityData = EntityData.builder(entity.toEntityData())
                    .lockUntil(lockUntilEpochNanos)
                    .build();

            toUpdateEntities.add(updatedEntityData);
        }
        List<Long> updatedEntityIds = new LinkedList<>(
                service.updateEntities(toUpdateEntities)).stream().map(Entity::getId).collect(Collectors.toList());

        Assert.assertEquals(items, updatedEntityIds.size());
    }

    @Test
    @Rollback
    public void shouldUpdateAndLockOneEntity() {

        Entity entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1, PARTITION_KEY_VALUES_1,
                PARTITION_SCHEMA_1);

        long lockUntilEpochNanos = TimeUtils.getNanosFromInstant(Instant.now().plus(2, ChronoUnit.HOURS));

        EntityData updatedEntityData = EntityData.builder(entity.toEntityData())
                .lockUntil(lockUntilEpochNanos)
                .build();

        LinkedList<Entity> updatedEntities = new LinkedList<>(
                service.updateEntities(Collections.singletonList(updatedEntityData)));

        assertThat(updatedEntities).isNotNull();
        assertThat(updatedEntities).hasSize(1);

        Entity updatedEntity = updatedEntities.getFirst();

        assertThat(updatedEntity.getId()).isEqualTo(entity.getId());
        assertThat(TimeUtils.getNanosFromInstant(updatedEntity.getLockedUntilStamp())).isEqualTo(lockUntilEpochNanos);
    }

    @Test
    @Rollback
    public void shouldUpdateAndUnlockOneEntity() {
        Entity entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1, PARTITION_KEY_VALUES_1,
                PARTITION_SCHEMA_1, Instant.now().plus(2, ChronoUnit.HOURS));

        EntityData updatedEntityData = EntityData.builder(entity.toEntityData())
                .unlock()
                .build();

        LinkedList<Entity> updatedEntities = new LinkedList<>(
                service.updateEntities(Collections.singletonList(updatedEntityData)));

        assertThat(updatedEntities).hasSize(1);
        Entity updatedEntity = updatedEntities.getFirst();
        assertThat(updatedEntity.getId()).isEqualTo(entity.getId());
        assertThat(updatedEntity.getLockedUntilStamp()).isNull();
    }

    @Test
    @Rollback
    public void shouldCreateEntityWithNewKeyValues() {
        Entity entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1, PARTITION_KEY_VALUES_1,
                PARTITION_SCHEMA_1);

        Long systemId = entity.getPartition().getSystem().getId();

        Map<String, Object> newEntityKeysValues = new HashMap<>(ENTITY_KEY_VALUES_1);
        newEntityKeysValues.put(ENTITY_STRING_SCHEMA_KEY_1, "new_value");

        service.findOrCreateEntityFor(systemId, newEntityKeysValues, PARTITION_KEY_VALUES_1,
                ENTITY_SCHEMA_1.toString(), TEST_RECORD_TIME);

        String newEntityKeyValuesString = convertMapIntoAvroSchemaString(newEntityKeysValues,
                ENTITY_SCHEMA_1.toString());

        Optional<Entity> optionalEntity = entityRepository
                .findByPartitionSystemIdAndKeyValues(systemId, newEntityKeyValuesString);

        assertThat(optionalEntity).isPresent();
        Entity newEntity = optionalEntity.get();
        assertThat(newEntity.getKeyValues()).isEqualTo(newEntityKeyValuesString);
        assertThat(newEntity.getSchema().getId()).isEqualTo(entity.getSchema().getId());
    }

    @Test
    @Rollback
    public void shouldCreateEntityHistory() {
        Entity entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1, PARTITION_KEY_VALUES_1,
                PARTITION_SCHEMA_1);

        createAndPersistEntityHist(entity, entity.getPartition(), entity.getSchema(), TEST_RECORD_TIME);

        Map<String, Object> newPartitionKeyValues = new HashMap<>(PARTITION_KEY_VALUES_1);
        newPartitionKeyValues.put(PARTITION_STRING_SCHEMA_KEY_1, "new_value");

        Long systemId = entity.getPartition().getSystem().getId();

        Entity entityWithChangedPartition = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1,
                newPartitionKeyValues, getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(),
                TEST_RECORD_TIME + 1);

        assertThat(entityWithChangedPartition.getId()).isEqualTo(entity.getId());
        assertThat(entityWithChangedPartition.getEntityHistories()).hasSize(1);
    }

    @Test
    @Rollback
    public void shouldGetLatestHistoryFromExistingEntity() {
        Long systemId = createAndPersistClientSystem(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA)
                .getId();

        Entity entity1 = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME);

        Entity found1 = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME);

        Entity found2 = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME + 1_000_000);

        assertThat(found1).isNotNull();
        assertThat(found2).isNotNull();
        assertThat(found1).isEqualTo(entity1);
        assertThat(found2).isEqualTo(found2);
        assertThat(found1.getEntityHistories()).hasSize(1);
        assertThat(found2.getEntityHistories()).hasSize(1);
    }

    @Test
    @Rollback
    public void shouldGetHistoryForExistingEntityWithDifferentSchemaFromThePast() {
        Long systemId = createAndPersistClientSystem(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA)
                .getId();

        Entity entity1 = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME);

        service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1, ENTITY_SCHEMA_2.toString(),
                100 * TEST_RECORD_TIME);
        Entity found = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME);

        assertThat(found).isNotNull();
        assertThat(found).isEqualTo(entity1);
        assertThat(found.getSchema().getContent()).isEqualTo(ENTITY_SCHEMA_2.toString());
        assertThat(found.getEntityHistories()).hasSize(1);
        assertThat(found.getEntityHistories().first().getSchema().getContent())
                .isEqualTo(getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString());
    }

    @Test
    @Rollback
    public void shouldGetHistoryForExistingEntityWithDifferentPartitionFromThePast() {
        Long systemId = createAndPersistClientSystem(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA)
                .getId();

        Map<String, Object> newPartitionKeyValues = new HashMap<>(PARTITION_KEY_VALUES_1);
        newPartitionKeyValues.put(PARTITION_STRING_SCHEMA_KEY_1, "new_value");

        Entity entity1 = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME);
        //this creates a new historical entry with new partition
        service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, newPartitionKeyValues,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), 100 * TEST_RECORD_TIME);
        Entity found = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME);

        assertThat(found).isNotNull();
        assertThat(found).isEqualTo(entity1);
        assertThat(found.getPartition().getKeyValues())
                .isEqualTo(convertMapIntoAvroSchemaString(newPartitionKeyValues, PARTITION_SCHEMA_1.toString()));
        assertThat(found.getEntityHistories()).hasSize(1);
        assertThat(found.getEntityHistories().first().getValidFromStamp())
                .isEqualTo(TimeUtils.getInstantFromNanos(TEST_RECORD_TIME));
        assertThat(found.getEntityHistories().first().getValidToStamp())
                .isEqualTo(TimeUtils.getInstantFromNanos(100 * TEST_RECORD_TIME));
        assertThat(found.getEntityHistories().first().getPartition().getKeyValues())
                .isEqualTo(convertMapIntoAvroSchemaString(PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1.toString()));
        assertThat(found.getEntityHistories().first().getSchema().getContent())
                .isEqualTo(getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString());
    }

    @Test(expected = ConfigDataConflictException.class)
    public void shouldNotCreateHistoryForLateDataWithDifferentSchemaBeforeEntityCreationTime() {
        Long systemId = createAndPersistClientSystem(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA)
                .getId();

        service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), 100 * TEST_RECORD_TIME);
        service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                ENTITY_SCHEMA_2.toString(), TEST_RECORD_TIME);
    }

    @Test
    @Rollback
    public void shouldExtendHistoryForLateDataWithTheSameSchemaBeforeEntityCreationTime() {
        Long systemId = createAndPersistClientSystem(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA)
                .getId();

        Entity entity1 = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), 100 * TEST_RECORD_TIME);
        Entity entity2 = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME);

        assertThat(entity1).isNotNull();
        assertThat(entity2).isNotNull();
        assertThat(entity1).isEqualTo(entity2);
        assertThat(entity2.getSchema().getContent())
                .isEqualTo(getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString());
        assertThat(entity2.getEntityHistories()).hasSize(1);
        assertThat(entity2.getEntityHistories().first().getValidFromStamp())
                .isEqualTo(TimeUtils.getInstantFromNanos(TEST_RECORD_TIME));
        assertThat(entity2.getEntityHistories().first().getValidToStamp()).isNull();
    }

    @Test
    @Rollback
    public void shouldExtendFirstHistoryForMigration() {
        Long systemId = createAndPersistClientSystem(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA)
                .getId();
        Long timestamp = 100 * TEST_RECORD_TIME;

        Entity entity1 = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), timestamp);

        Entity entity2 = service.extendEntityFirstHistoryDataFor(entity1.getId(), ENTITY_SCHEMA_2.toString(), 0);

        assertThat(entity1).isNotNull();
        assertThat(entity2).isNotNull();
        assertThat(entity2.getEntityHistories()).hasSize(1);
        assertThat(entity2.getEntityHistories().first().getValidFromStamp())
                .isEqualTo(TimeUtils.getInstantFromNanos(0));
        assertThat(entity2.getEntityHistories().first().getValidToStamp())
                .isEqualTo(TimeUtils.getInstantFromNanos(timestamp));
    }

    @Test
    @Rollback
    public void shouldAcceptEntityWithPartitionNotPresentedInTheHistory() {
        Long systemId = createAndPersistClientSystem(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA)
                .getId();

        Map<String, Object> newPartitionKeyValues = new HashMap<>(PARTITION_KEY_VALUES_1);
        newPartitionKeyValues.put(PARTITION_STRING_SCHEMA_KEY_1, "new_value");

        service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME);
        Entity entity = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, newPartitionKeyValues,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), 100 * TEST_RECORD_TIME);

        assertThat(entity).isNotNull();
        assertThat(entity.getEntityHistories()).hasSize(1);
    }

    @Test
    @Rollback
    public void shouldUpdateHistoryForLateDataWithSameSchemaBeforeEntityCreationTime() {
        Long systemId = createAndPersistClientSystem(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA)
                .getId();

        Entity entity1 = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), 100 * TEST_RECORD_TIME);
        Entity entity2 = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME);

        assertThat(entity1).isNotNull();
        assertThat(entity2).isNotNull();
        assertThat(entity1).isEqualTo(entity2);
        assertThat(entity2.getSchema().getContent())
                .isEqualTo(getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString());
        assertThat(entity2.getEntityHistories()).hasSize(1);
    }

    @Test
    public void shouldFindEntityHistForAdvancingTimeWindow() {
        //given
        Long systemId = createAndPersistClientSystem(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA)
                .getId();

        Map<String, Object> newPartitionKeyValues1 = new HashMap<>(PARTITION_KEY_VALUES_1);
        newPartitionKeyValues1.put(PARTITION_STRING_SCHEMA_KEY_1, "value1");
        Map<String, Object> newPartitionKeyValues2 = new HashMap<>(PARTITION_KEY_VALUES_1);
        newPartitionKeyValues2.put(PARTITION_STRING_SCHEMA_KEY_1, "value2");
        Map<String, Object> newPartitionKeyValues3 = new HashMap<>(PARTITION_KEY_VALUES_1);
        newPartitionKeyValues3.put(PARTITION_STRING_SCHEMA_KEY_1, "value3");
        Map<String, Object> newPartitionKeyValues4 = new HashMap<>(PARTITION_KEY_VALUES_1);
        newPartitionKeyValues4.put(PARTITION_STRING_SCHEMA_KEY_1, "value4");
        Map<String, Object> newPartitionKeyValues5 = new HashMap<>(PARTITION_KEY_VALUES_1);
        newPartitionKeyValues5.put(PARTITION_STRING_SCHEMA_KEY_1, "value5");

        Entity entity = service
                .findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, newPartitionKeyValues1,
                        getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(),
                        TEST_RECORD_TIME + 100);
        checkEntityHistory(entity, TEST_RECORD_TIME + 100, null);

        entity = service
                .findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, newPartitionKeyValues2,
                        getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(),
                        TEST_RECORD_TIME + 200);
        checkEntityHistory(entity, TEST_RECORD_TIME + 200, null);

        entity = service
                .findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, newPartitionKeyValues3,
                        getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(),
                        TEST_RECORD_TIME + 300);
        checkEntityHistory(entity, TEST_RECORD_TIME + 300, null);

        entity = service
                .findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, newPartitionKeyValues4,
                        getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(),
                        TEST_RECORD_TIME + 400);
        checkEntityHistory(entity, TEST_RECORD_TIME + 400, null);

        entity = service
                .findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, newPartitionKeyValues5,
                        getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(),
                        TEST_RECORD_TIME + 500);
        checkEntityHistory(entity, TEST_RECORD_TIME + 500, null);

        //when
        Entity entityWithHistForTimeWindow = service
                .findEntityWithHistForTimeWindow(systemId, ENTITY_KEY_VALUES_1, TEST_RECORD_TIME + 100,
                        TEST_RECORD_TIME + 300);
        SortedSet<EntityHistory> filteredHistory = entityWithHistForTimeWindow.getEntityHistories();

        //then
        assertThat(filteredHistory).hasSize(3);

        EntityHistory[] entityHistsArray = filteredHistory.toArray(new EntityHistory[0]);
        assertThat(entityHistsArray[0].getPartition().getKeyValues())
                .isEqualTo(convertMapIntoAvroSchemaString(newPartitionKeyValues3, PARTITION_SCHEMA_1.toString()));
        assertThat(entityHistsArray[1].getPartition().getKeyValues())
                .isEqualTo(convertMapIntoAvroSchemaString(newPartitionKeyValues2, PARTITION_SCHEMA_1.toString()));
        assertThat(entityHistsArray[2].getPartition().getKeyValues())
                .isEqualTo(convertMapIntoAvroSchemaString(newPartitionKeyValues1, PARTITION_SCHEMA_1.toString()));
    }

    @Test
    @Rollback
    public void shouldFetchEntityWithoutHistoryInThatTimeWindow() {
        //given
        Entity entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1, PARTITION_KEY_VALUES_1,
                PARTITION_SCHEMA_1);

        //when
        Entity entityWithoutHistory = service
                .findEntityWithHistForTimeWindow(entity.getPartition().getSystem().getId(), ENTITY_KEY_VALUES_1,
                        TEST_RECORD_TIME, TEST_RECORD_TIME);

        //then
        assertThat(entityWithoutHistory).isNotNull();
        assertThat(entityWithoutHistory.getEntityHistories()).isEmpty();
    }

    @Test(expected = InvalidDataAccessApiUsageException.class)
    public void shouldThrowExceptionWhenTryingToRegexWithNull() {
        service.findByKeyValueLike(null);
    }

    @Test
    @Rollback
    public void shouldFindEntityWithGivenRegex() {
        Entity entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1, PARTITION_KEY_VALUES_1,
                PARTITION_SCHEMA_1);
        createAndPersistEntityHist(entity, entity.getPartition(), entity.getSchema(), TEST_RECORD_TIME);

        Long systemId = entity.getPartition().getSystem().getId();

        Entity newEntity = service.findOrCreateEntityFor(systemId, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME + 1);

        String regex = "%trin%";
        List<Entity> foundKeys = entityRepository.findByKeyValuesLike(regex);

        assertThat(foundKeys).containsExactly(newEntity);
    }

    @Test
    @Rollback
    public void shouldFindEntityById() {
        Entity entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1, PARTITION_KEY_VALUES_1,
                PARTITION_SCHEMA_1);

        createAndPersistEntityHist(entity, entity.getPartition(), entity.getSchema(), TEST_RECORD_TIME);

        Entity fetchedEntity = service.findById(entity.getId());

        assertThat(fetchedEntity).isNotNull();
        assertThat(fetchedEntity.getEntityHistories()).hasSize(1);
    }

    @Test(expected = NotFoundRuntimeException.class)
    public void shouldThrowExceptionWhenTryingToFindOneEntityById() {
        service.findById(12345L);
    }

    @Test
    @Rollback
    public void shouldFindAllEntitiesById() {
        Long updatedTestRecordTime = TEST_RECORD_TIME + 500000L;
        Entity entity1 = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1, PARTITION_KEY_VALUES_1,
                PARTITION_SCHEMA_1);

        createAndPersistEntityHist(entity1, entity1.getPartition(), entity1.getSchema(), TEST_RECORD_TIME);

        Entity entity2 = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_2, ENTITY_SCHEMA_2, PARTITION_KEY_VALUES_2,
                PARTITION_SCHEMA_2);

        createAndPersistEntityHist(entity2, entity2.getPartition(), entity2.getSchema(), TEST_RECORD_TIME);

        Set<Entity> fetchedEntities = service.findAllByIdIn(Sets.newHashSet(entity1.getId(), entity2.getId()));

        assertThat(fetchedEntities).hasSize(2);
        assertThat(fetchedEntities).doesNotContainNull();

        List<Long> fetchedIds = fetchedEntities.stream().map(Entity::getId).collect(toList());
        assertThat(fetchedIds).contains(entity1.getId(), entity2.getId());

        LinkedList<SortedSet<EntityHistory>> fetchedHistories = fetchedEntities.stream().map(Entity::getEntityHistories)
                .collect(toCollection(LinkedList::new));

        assertThat(fetchedHistories.getFirst()).hasSize(1);
        assertThat(fetchedHistories.getLast()).hasSize(1);
    }

    @Test
    public void shouldFetchEmptyListWhenTryingToFindAllNonExistingEntitiesById() {
        Set<Entity> fetchedEntities = service.findAllByIdIn(Sets.newHashSet(12345L, 123456L));
        assertThat(fetchedEntities).hasSize(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenGivenNullListOfIds() {
        service.findAllByIdIn(null);
    }

    private void checkEntityHistory(Entity entity, long start, Long stop) {
        assertThat(entity.getEntityHistories()).hasSize(1);
        assertThat(entity.getEntityHistories().first().getValidFromStamp())
                .isEqualTo(TimeUtils.getInstantFromNanos(start));
        if (stop != null) {
            assertThat(entity.getEntityHistories().first().getValidToStamp())
                    .isEqualTo(TimeUtils.getInstantFromNanos(stop));
        } else {
            assertThat(entity.getEntityHistories().first().getValidToStamp()).isNull();
        }
    }
}
