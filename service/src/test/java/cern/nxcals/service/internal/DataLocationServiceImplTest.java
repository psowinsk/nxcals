/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.internal;

import cern.nxcals.common.domain.EntityResources;
import cern.nxcals.common.utils.HdfsFileUtils;
import cern.nxcals.common.utils.TimeUtils;
import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.Entity;
import cern.nxcals.service.domain.EntityHistory;
import cern.nxcals.service.domain.Partition;
import cern.nxcals.service.domain.Schema;
import cern.nxcals.service.domain.System;
import cern.nxcals.service.repository.EntityHistoryRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.net.URI;
import java.time.Instant;
import java.time.ZoneId;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.StringJoiner;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_JSON;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_JSON_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_2;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.RECORD_VERSION_SCHEMA;
import static cern.nxcals.service.rest.TestSchemas.TIME_SCHEMA;
import static java.time.temporal.ChronoUnit.DAYS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@Transactional(transactionManager = "jpaTransactionManager")
public class DataLocationServiceImplTest extends BaseTest {
    private long startTime;
    private long endTime;

    @Mock
    private EntityHistoryRepository histRepoMocked;
    @Mock
    private InternalEntityService entityRepoMocked;
    private InternalEntityResourcesServiceImpl entityResourcesService;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
        entityResourcesService = new InternalEntityResourcesServiceImpl(dataLocationPrefix, dataLocationSuffix,
                hbaseTablespace,
                entityRepoMocked);

        //given
        // Time window: Start time: 1970-01-01T00:00:01 End time: 1970-01-03T00:00:01Z
        startTime = TEST_RECORD_TIME;
        Instant endTimeInstant = TimeUtils.getInstantFromNanos(startTime).plus(2, DAYS);
        endTime = TimeUtils.getNanosFromInstant(endTimeInstant);

    }

    @Rollback
    @Test(expected = IllegalArgumentException.class)
    public void shouldNotGetEntityWithoutHistory() {
        // given
        Entity entity = createEntity();

        when(entityRepoMocked.findEntityWithHistForTimeWindow(anyLong(), eq(ENTITY_KEY_VALUES_1), anyLong(), anyLong()))
                .thenReturn(entity);
        // when
        entityResourcesService.findBySystemIdKeyValuesAndTimeWindow(entity.getPartition().getSystem().getId(),
                ENTITY_KEY_VALUES_1, startTime, endTime);
    }

    @Test
    @Rollback
    public void shouldGetResourcesForCurrentEntity() {
        // given
        Entity entity = createEntity();
        Instant startTimeInstant = TimeUtils.getInstantFromNanos(startTime);

        EntityHistory entityHistory = createEntityHistory(entity, entity.getSchema(), startTimeInstant, null);

        SortedSet<EntityHistory> listEntityHistory = new TreeSet<>();
        listEntityHistory.add(entityHistory);
        entity.setEntityHistories(listEntityHistory);

        when(entityRepoMocked.findEntityWithHistForTimeWindow(anyLong(), eq(ENTITY_KEY_VALUES_1), anyLong(), anyLong()))
                .thenReturn(entity);

        Map<Long, Set<URI>> paths = entity.getEntityHistories().stream()
                .filter(h -> h.getValidFromStamp().truncatedTo(DAYS).equals(startTimeInstant.truncatedTo(DAYS)))
                .collect(Collectors.toMap(h -> h.getSchema().getId(), h -> buildDataLocationPath(h)));

        // when
        Set<EntityResources> resourceData = this.entityResourcesService.findBySystemIdKeyValuesAndTimeWindow(
                entity.getPartition().getSystem().getId(), ENTITY_KEY_VALUES_1,
                startTime, endTime);

        // then
        assertNotNull(resourceData);
        assertEquals(1, resourceData.size());
        assertEquals(3,
                (int) resourceData.stream().map(rd -> rd.getResourcesData().getHdfsPaths().size()).findFirst().get());
        assertEquals(1,
                (int) resourceData.stream().map(rd -> rd.getResourcesData().getHbaseTableNames().size()).findFirst()
                        .get());
        assertEquals(1,
                resourceData.stream().filter(rd -> rd.getResourcesData().getHdfsPaths().containsAll(paths.get(3L)))
                        .count());
        assertEquals(1,
                resourceData.stream().filter(rd -> rd.getResourcesData().getHbaseTableNames().containsAll(
                        Stream.of("1__2__3").collect(Collectors.toSet())
                )).count());
    }

    @Test
    @Rollback
    public void shouldGetResourcesForEntityFromHistory() {
        // given
        // Time window: Start time: 1970-01-01T00:00:01 End time: 1970-01-03T00:00:01
        Entity entity = createEntity();
        Instant startTimeInstant = TimeUtils.getInstantFromNanos(startTime);

        EntityHistory entityHistory = createEntityHistory(entity, entity.getSchema(), TimeUtils.getInstantFromNanos(0L),
                startTimeInstant.plusSeconds(1000));

        Schema new_schema = new Schema();
        new_schema.setId(5L);
        new_schema.setContent(ENTITY_SCHEMA_2.toString());

        EntityHistory entityHistory2 = createEntityHistory(entity, new_schema, startTimeInstant, null);

        SortedSet<EntityHistory> listEntityHistory = new TreeSet<>();
        listEntityHistory.add(entityHistory);
        listEntityHistory.add(entityHistory2);
        entity.setEntityHistories(listEntityHistory);

        when(entityRepoMocked.findEntityWithHistForTimeWindow(anyLong(), eq(ENTITY_KEY_VALUES_1), anyLong(), anyLong()))
                .thenReturn(entity);

        Map<Long, Set<URI>> paths = entity.getEntityHistories().stream()
                .filter(h -> h.getValidFromStamp().truncatedTo(DAYS).equals(startTimeInstant.truncatedTo(DAYS)))
                .collect(Collectors.toMap(h -> h.getSchema().getId(), h -> buildDataLocationPath(h)));

        // when
        Set<EntityResources> resourceData = entityResourcesService
                .findBySystemIdKeyValuesAndTimeWindow(1L, ENTITY_KEY_VALUES_1,
                        startTime, endTime);

        // then
        assertNotNull(resourceData);
        assertEquals(2, resourceData.size());
        assertEquals(1, (int) resourceData.stream().filter(rd -> rd.getSchemaData().getId() == 3L)
                .map(rd -> rd.getResourcesData().getHdfsPaths().size()).findFirst().get());
        assertEquals(1, (int) resourceData.stream().filter(rd -> rd.getSchemaData().getId() == 3L)
                .map(rd -> rd.getResourcesData().getHbaseTableNames().size()).findFirst().get());
        assertEquals(3, (int) resourceData.stream().filter(rd -> rd.getSchemaData().getId() == 5L)
                .map(rd -> rd.getResourcesData().getHdfsPaths().size()).findFirst().get());
        assertEquals(1, (int) resourceData.stream().filter(rd -> rd.getSchemaData().getId() == 5L)
                .map(rd -> rd.getResourcesData().getHbaseTableNames().size()).findFirst().get());
        assertEquals(1, resourceData.stream().filter(rd -> rd.getSchemaData().getId() == 3L &&
                rd.getResourcesData().getHdfsPaths().containsAll(paths.get(3L))).count());
        assertEquals(1, resourceData.stream().filter(rd -> rd.getSchemaData().getId() == 3L &&
                rd.getResourcesData().getHbaseTableNames().containsAll(
                        Stream.of("1__2__3").collect(Collectors.toSet())
                )).count());
        assertEquals(1, resourceData.stream().filter(rd -> rd.getSchemaData().getId() == 5L &&
                rd.getResourcesData().getHdfsPaths().containsAll(paths.get(5L))).count());
        assertEquals(1, resourceData.stream().filter(rd -> rd.getSchemaData().getId() == 5L &&
                rd.getResourcesData().getHbaseTableNames().containsAll(
                        Stream.of("1__2__5").collect(Collectors.toSet())
                )).count());
    }

    private Set<URI> buildDataLocationPath(EntityHistory entityHistory) {
        StringJoiner joiner = new StringJoiner("/", dataLocationPrefix, dataLocationSuffix);
        joiner.add(entityHistory.getPartition().getSystem().getId().toString())
                .add(entityHistory.getPartition().getId().toString()).add(entityHistory.getSchema().getId().toString())
                .add(HdfsFileUtils.expandDateToNestedPaths(TimeUtils.getInstantFromNanos(TEST_RECORD_TIME).atZone(
                        ZoneId.of("UTC")).format(HdfsFileUtils.STAGE_DIRECTORY_DATE_FORMATTER)));

        Set<URI> paths = new HashSet<>();
        paths.add(URI.create(joiner.toString()));
        return paths;
    }

    private Entity createEntity() {
        // System
        System system = new System();
        system.setId(1L);
        system.setName(TEST_NAME);
        system.setEntityKeyDefs(ENTITY_SCHEMA_1.toString());
        system.setPartitionKeyDefs(PARTITION_SCHEMA_1.toString());
        system.setRecordVersionKeyDefs(RECORD_VERSION_SCHEMA.toString());
        system.setTimeKeyDefs(TIME_SCHEMA.toString());

        // Partition
        Partition part = new Partition();
        part.setId(2L);
        part.setSystem(system);
        part.setKeyValues(PARTITION_KEY_VALUES_JSON_1);

        // Schema
        Schema schema = new Schema();
        schema.setId(3L);
        schema.setContent(ENTITY_SCHEMA_1.toString());

        // Entity
        Entity entity = new Entity();
        entity.setId(4L);
        entity.setKeyValues(ENTITY_KEY_VALUES_JSON);
        entity.setPartition(part);
        entity.setSchema(schema);
        return entity;
    }

    private EntityHistory createEntityHistory(Entity entity, Schema schema, Instant startTime, Instant endTime) {
        EntityHistory entityHistory = new EntityHistory();
        entityHistory.setEntity(entity);
        entityHistory.setSchema(schema);
        entityHistory.setPartition(entity.getPartition());
        entityHistory.setValidFromStamp(startTime);
        if (endTime != null) {
            entityHistory.setValidToStamp(endTime);
        }
        return entityHistory;
    }
}
