/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.internal;

import cern.nxcals.common.domain.VariableConfigData;
import cern.nxcals.common.domain.VariableData;
import cern.nxcals.common.utils.TimeUtils;
import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.Entity;
import cern.nxcals.service.domain.Variable;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Collections;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import static org.assertj.core.api.Assertions.assertThat;

@Transactional(transactionManager = "jpaTransactionManager")
public class InternalVariableServiceImplTest extends BaseTest {

    @Autowired
    private InternalVariableService internalVariableService;

    @Rollback
    @Test
    public void shouldCreateNewVariable() {
        //given
        Entity entity = this.createAndSaveDefaultTestEntityKey();
        VariableConfigData varConfData = createVariableConfigData(entity.getId(), "test", null, null);
        SortedSet<VariableConfigData> varConfSet = new TreeSet<>();
        varConfSet.add(varConfData);
        VariableData variableData = createVariableData("Test Variable", null, null, varConfSet);

        //when
        Variable newVariable = this.internalVariableService.registerOrUpdateVariableFor(variableData);

        //then
        Assert.assertNotNull(newVariable);
        Assert.assertEquals("Test Variable", newVariable.getVariableName());
        Assert.assertEquals(1, newVariable.getVariableConfigs().size());
        Assert.assertEquals(Long.valueOf(varConfData.getEntityId()),
                newVariable.getVariableConfigs().first().getEntity().getId());
        Assert.assertEquals(varConfData.getFieldName(), newVariable.getVariableConfigs().first().getFieldName());
        Assert.assertNull(newVariable.getVariableConfigs().first().getValidFromStamp());
        Assert.assertNull(newVariable.getVariableConfigs().first().getValidToStamp());
    }

    @Rollback
    @Test
    public void shouldCheckOrderedConfigData() {
        //given
        Entity entity = this.createAndSaveDefaultTestEntityKey();
        long t1 = TimeUtils.getNanosFromInstant(LocalDateTime.of(2017, 03, 11, 20, 30, 30).toInstant(ZoneOffset.UTC));
        long t2 = TimeUtils.getNanosFromInstant(LocalDateTime.of(2017, 03, 12, 21, 00, 45).toInstant(ZoneOffset.UTC));

        VariableConfigData varConfData1 = createVariableConfigData(entity.getId(), "field1", null, t1);
        VariableConfigData varConfData2 = createVariableConfigData(entity.getId(), "field2", t1, t2);
        VariableConfigData varConfData3 = createVariableConfigData(entity.getId(), "field3", t2, null);

        SortedSet<VariableConfigData> varConfSet = new TreeSet<>();
        varConfSet.add(varConfData1);
        varConfSet.add(varConfData2);
        varConfSet.add(varConfData3);

        VariableData variableData = createVariableData("Test Variable", "Description", null, varConfSet);

        //when
        Variable newVariable = this.internalVariableService.registerOrUpdateVariableFor(variableData);

        //then
        Assert.assertNotNull(newVariable);
        Assert.assertEquals("Test Variable", newVariable.getVariableName());
        Assert.assertEquals(3, newVariable.getVariableConfigs().size());
        Assert.assertEquals(varConfData3.getFieldName(), newVariable.getVariableConfigs().first().getFieldName());
        Assert.assertEquals((long) varConfData3.getValidFromStamp(),
                TimeUtils.getNanosFromInstant(newVariable.getVariableConfigs().first().getValidFromStamp()));
        Assert.assertNull(newVariable.getVariableConfigs().first().getValidToStamp());
    }

    @Rollback
    @Test
    public void shouldCheckUnorderedConfigData() {
        //given
        Entity entity = this.createAndSaveDefaultTestEntityKey();
        long t1 = TimeUtils.getNanosFromInstant(LocalDateTime.of(2017, 03, 11, 20, 30, 30).toInstant(ZoneOffset.UTC));
        long t2 = TimeUtils.getNanosFromInstant(LocalDateTime.of(2017, 03, 12, 21, 00, 45).toInstant(ZoneOffset.UTC));

        VariableConfigData varConfData1 = createVariableConfigData(entity.getId(), "test1", null, t1);
        VariableConfigData varConfData2 = createVariableConfigData(entity.getId(), "test2", t1, t2);
        VariableConfigData varConfData3 = createVariableConfigData(entity.getId(), "test3", t2, null);

        SortedSet<VariableConfigData> varConfSet = new TreeSet<>();
        varConfSet.add(varConfData3);
        varConfSet.add(varConfData2);
        varConfSet.add(varConfData1);

        VariableData variableData = createVariableData("Test Variable", "Description", null, varConfSet);

        //when
        Variable newVariable = this.internalVariableService.registerOrUpdateVariableFor(variableData);

        //then
        Assert.assertNotNull(newVariable);
        Assert.assertEquals("Test Variable", newVariable.getVariableName());
        Assert.assertEquals(3, newVariable.getVariableConfigs().size());
        Assert.assertEquals(varConfData3.getFieldName(), newVariable.getVariableConfigs().first().getFieldName());
        Assert.assertEquals((long) varConfData3.getValidFromStamp(),
                TimeUtils.getNanosFromInstant(newVariable.getVariableConfigs().first().getValidFromStamp()));
        Assert.assertNull(newVariable.getVariableConfigs().first().getValidToStamp());

    }

    @Rollback
    @Test
    public void shouldReplaceOldConfig() {
        //given
        Entity entity = this.createAndSaveDefaultTestEntityKey();
        long t1 = TimeUtils.getNanosFromInstant(LocalDateTime.of(2017, 03, 11, 20, 30, 30).toInstant(ZoneOffset.UTC));

        VariableConfigData varConfData1 = createVariableConfigData(entity.getId(), "field1", null, t1);
        VariableConfigData varConfData2 = createVariableConfigData(entity.getId(), "field2", t1, null);
        VariableConfigData varConfData3 = createVariableConfigData(entity.getId(), "field1", null, null);

        SortedSet<VariableConfigData> varConfSet = new TreeSet<>();
        varConfSet.add(varConfData1);
        varConfSet.add(varConfData2);

        VariableData variableData = createVariableData("Test Variable", "Description", null, varConfSet);
        VariableData initialVaraible = this.internalVariableService.registerOrUpdateVariableFor(variableData)
                .toVariableData();

        SortedSet<VariableConfigData> varConfSetNew = new TreeSet<>();
        varConfSetNew.add(varConfData3);
        VariableData variableData1 = createVariableData("Test Variable", "Description", null, varConfSetNew);

        //when
        VariableData resultVariable = this.internalVariableService.registerOrUpdateVariableFor(variableData1)
                .toVariableData();

        //then
        Assert.assertNotNull(initialVaraible);
        Assert.assertNotNull(resultVariable);
        Assert.assertEquals(initialVaraible.getVariableName(), resultVariable.getVariableName());
        Assert.assertEquals(2, initialVaraible.getVariableConfigData().size());
        Assert.assertEquals(1, resultVariable.getVariableConfigData().size());
        Assert.assertNull(resultVariable.getVariableConfigData().first().getValidFromStamp());
        Assert.assertNull(resultVariable.getVariableConfigData().first().getValidToStamp());
        Assert.assertNull(initialVaraible.getVariableConfigData().first().getValidToStamp());
        Assert.assertEquals((Long) t1, initialVaraible.getVariableConfigData().first().getValidFromStamp());
    }

    @Rollback
    @Test
    public void shouldReturnMatchedVariablesByName() {
        //given
        String nameExpression = "R%VARIABLE_THAT_WILL_NEVER%";
        Variable variable = createAndPersistVariable("REGEX_VARIABLE_THAT_WILL_NEVER_BE_IN_DATABASE",
                "DESC", Instant.now());

        //when
        final List<Variable> returnedVariables = internalVariableService.findByNameLike(nameExpression);

        //then
        final List<Variable> expectedVariables = Collections.singletonList(variable);
        assertThat(returnedVariables).isEqualTo(expectedVariables);
    }

    @Rollback
    @Test
    public void shouldReturnMatchedVariablesByDesc() {
        //given
        String descriptionExpression = "V%L_n%n";
        Variable variable = createAndPersistVariable("REGEX", "Very Long Distinctive Expression", Instant.now());

        //when
        final List<Variable> returnedVariables = internalVariableService.findByDescriptionLike(descriptionExpression);

        //then
        final List<Variable> expectedVariables = Collections.singletonList(variable);
        assertThat(returnedVariables).isEqualTo(expectedVariables);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailToVerifyConfigDataWithTimeHoles() {
        //given
        Entity entity = this.createAndSaveDefaultTestEntityKey();
        long t1 = TimeUtils.getNanosFromInstant(LocalDateTime.of(2017, 03, 11, 20, 30, 30).toInstant(ZoneOffset.UTC));
        long t2 = TimeUtils.getNanosFromInstant(LocalDateTime.of(2017, 03, 12, 21, 00, 45).toInstant(ZoneOffset.UTC));

        VariableConfigData varConfData1 = createVariableConfigData(entity.getId(), "test", null, t1);
        VariableConfigData varConfData2 = createVariableConfigData(entity.getId(), "test", t2, null);

        SortedSet<VariableConfigData> varConfSet = new TreeSet<>();
        varConfSet.add(varConfData1);
        varConfSet.add(varConfData2);

        VariableData variableData = createVariableData("Test Variable", "Description", null, varConfSet);

        //when
        this.internalVariableService.registerOrUpdateVariableFor(variableData);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailtoVerifyConfigDataWithClosedEndInterval() {
        //given
        Entity entity = this.createAndSaveDefaultTestEntityKey();
        long t1 = TimeUtils.getNanosFromInstant(LocalDateTime.of(2017, 03, 11, 20, 30, 30).toInstant(ZoneOffset.UTC));
        long t2 = TimeUtils.getNanosFromInstant(LocalDateTime.of(2017, 03, 12, 21, 00, 45).toInstant(ZoneOffset.UTC));

        VariableConfigData varConfData1 = createVariableConfigData(entity.getId(), "test1", null, t1);

        VariableConfigData varConfData2 = createVariableConfigData(entity.getId(), "test2", t1, t2);

        SortedSet<VariableConfigData> varConfSet = new TreeSet<>();
        varConfSet.add(varConfData1);
        varConfSet.add(varConfData2);

        VariableData variableData = createVariableData("Test Variable", "Description", null, varConfSet);

        //when
        this.internalVariableService.registerOrUpdateVariableFor(variableData);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailToVerifyConfigDataWithClosedBothInterval() {
        //when
        Entity entity = this.createAndSaveDefaultTestEntityKey();
        long t1 = TimeUtils.getNanosFromInstant(LocalDateTime.of(2017, 03, 11, 20, 30, 30).toInstant(ZoneOffset.UTC));
        long t2 = TimeUtils.getNanosFromInstant(LocalDateTime.of(2017, 03, 12, 21, 00, 45).toInstant(ZoneOffset.UTC));
        long t3 = TimeUtils.getNanosFromInstant(LocalDateTime.of(2017, 03, 13, 12, 00, 45).toInstant(ZoneOffset.UTC));

        VariableConfigData varConfData1 = createVariableConfigData(entity.getId(), "test1", t1, t2);
        VariableConfigData varConfData2 = createVariableConfigData(1, "test1", t2, t3);

        SortedSet<VariableConfigData> varConfSet = new TreeSet<>();
        varConfSet.add(varConfData1);
        varConfSet.add(varConfData2);
        VariableData variableData = createVariableData("Test Variable", "Description", null, varConfSet);

        //when
        this.internalVariableService.registerOrUpdateVariableFor(variableData);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailToVerifyConfigDataWithClosedStartInterval() {
        //given
        Entity entity = this.createAndSaveDefaultTestEntityKey();
        long t1 = TimeUtils.getNanosFromInstant(LocalDateTime.of(2017, 03, 11, 20, 30, 30).toInstant(ZoneOffset.UTC));
        long t2 = TimeUtils.getNanosFromInstant(LocalDateTime.of(2017, 03, 12, 21, 00, 45).toInstant(ZoneOffset.UTC));
        long t3 = TimeUtils.getNanosFromInstant(LocalDateTime.of(2017, 03, 13, 12, 00, 45).toInstant(ZoneOffset.UTC));

        VariableConfigData varConfData1 = createVariableConfigData(entity.getId(), "test1", t1, t2);
        VariableConfigData varConfData2 = createVariableConfigData(entity.getId(), "test1", t2, t3);
        VariableConfigData varConfData3 = createVariableConfigData(entity.getId(), "test1", t3, null);

        SortedSet<VariableConfigData> varConfSet = new TreeSet<>();
        varConfSet.add(varConfData1);
        varConfSet.add(varConfData2);
        varConfSet.add(varConfData3);

        VariableData variableData = createVariableData("Test Variable", "Description", null, varConfSet);

        //when
        this.internalVariableService.registerOrUpdateVariableFor(variableData);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailToVerifyConfigDataForOverlappingPeriods() {
        //given
        Entity entity = this.createAndSaveDefaultTestEntityKey();
        long t1 = TimeUtils.getNanosFromInstant(LocalDateTime.of(2017, 03, 11, 20, 30, 30).toInstant(ZoneOffset.UTC));
        long t2 = TimeUtils.getNanosFromInstant(LocalDateTime.of(2017, 03, 12, 21, 00, 45).toInstant(ZoneOffset.UTC));

        VariableConfigData varConfData1 = createVariableConfigData(entity.getId(), "test1", null, t2);
        VariableConfigData varConfData2 = createVariableConfigData(1, "test1", t1, null);

        SortedSet<VariableConfigData> varConfSet = new TreeSet<>();
        varConfSet.add(varConfData1);
        varConfSet.add(varConfData2);

        VariableData variableData = createVariableData("Test Variable", "Description", null, varConfSet);

        //when
        this.internalVariableService.registerOrUpdateVariableFor(variableData);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailToVerifyConfigDataForWrongTimeWindow() {
        //given
        Entity entity = this.createAndSaveDefaultTestEntityKey();
        long t1 = TimeUtils.getNanosFromInstant(LocalDateTime.of(2017, 03, 11, 20, 30, 30).toInstant(ZoneOffset.UTC));
        long t2 = TimeUtils.getNanosFromInstant(LocalDateTime.of(2017, 03, 12, 21, 00, 45).toInstant(ZoneOffset.UTC));

        VariableConfigData varConfData1 = createVariableConfigData(entity.getId(), "test1", null, t2);
        VariableConfigData varConfData2 = createVariableConfigData(1, "test1", t2, t1);
        VariableConfigData varConfData3 = createVariableConfigData(1, "test1", t1, null);

        SortedSet<VariableConfigData> varConfSet = new TreeSet<>();
        varConfSet.add(varConfData1);
        varConfSet.add(varConfData2);
        varConfSet.add(varConfData3);

        VariableData variableData = createVariableData("Test Variable", "Description", null, varConfSet);

        //when
        this.internalVariableService.registerOrUpdateVariableFor(variableData);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionEntityNull() {
        //given
        VariableConfigData varConfData1 = createVariableConfigData(Long.valueOf(null), "test1", null, null);
        SortedSet<VariableConfigData> varConfSet = new TreeSet<>();
        varConfSet.add(varConfData1);
        VariableData variableData = createVariableData("Test Variable", "Description", null, varConfSet);

        //when
        this.internalVariableService.registerOrUpdateVariableFor(variableData);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionEntityDoesNotExist() {
        //given
        VariableConfigData varConfData1 = createVariableConfigData(0, "test1", null, null);
        SortedSet<VariableConfigData> varConfSet = new TreeSet<>();
        varConfSet.add(varConfData1);
        VariableData variableData = createVariableData("Test Variable", "Description", null, varConfSet);

        //when
        this.internalVariableService.registerOrUpdateVariableFor(variableData);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailCreatingVariableWithNullConfig() {
        //given
        Entity entity = this.createAndSaveDefaultTestEntityKey();
        VariableData variableData = createVariableData("Test Variable", "Description", null, null);

        //when
        this.internalVariableService.registerOrUpdateVariableFor(variableData);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailCreatingVariableWithEmptyConfig() {
        //given
        Entity entity = this.createAndSaveDefaultTestEntityKey();
        VariableData variableData = createVariableData("Test Variable", "Description", null,
                Collections.emptySortedSet());

        //when
        this.internalVariableService.registerOrUpdateVariableFor(variableData);
    }

}
