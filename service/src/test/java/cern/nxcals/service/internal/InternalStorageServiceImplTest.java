package cern.nxcals.service.internal;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class InternalStorageServiceImplTest {

    private final FileSystem hadoopFileSystem = Mockito.mock(FileSystem.class);

    private InternalStorageService hdfsService;

    @Before
    public void setUp() throws Exception {
        this.hdfsService = new InternalStorageServiceImpl(hadoopFileSystem);
    }

    @After
    public void tearDown() throws Exception {
        reset(
                hadoopFileSystem
        );
    }

    @Test
    public void shouldCountFilesThatModifiedAfterAGivenTime() throws Exception {
        Instant modificationReferenceTime = Instant.now().minus(4, ChronoUnit.DAYS);

        String directoryPath = "/user/super/directory/path";
        Path hdfsPath = new Path(directoryPath);

        Path filePath = new Path(directoryPath + "/super.avro");
        LocatedFileStatus fileStatus = Mockito.mock(LocatedFileStatus.class);
        when(fileStatus.getPath()).thenReturn(filePath);
        when(fileStatus.isFile()).thenReturn(true);
        when(fileStatus.getModificationTime())
                .thenReturn(Instant.now().minus(1, ChronoUnit.HOURS).toEpochMilli());

        Path filePathOther = new Path(directoryPath + "test.other");
        LocatedFileStatus fileStatusOther = Mockito.mock(LocatedFileStatus.class);
        when(fileStatusOther.getPath()).thenReturn(filePathOther);
        when(fileStatusOther.isFile()).thenReturn(true);
        when(fileStatusOther.getModificationTime())
                .thenReturn(Instant.now().toEpochMilli());

        final List<LocatedFileStatus> fileStatusList = new ArrayList<>();
        fileStatusList.add(fileStatus);
        fileStatusList.add(fileStatusOther);

        RemoteIterator<LocatedFileStatus> fileStatusesIterator = new RemoteIterator<LocatedFileStatus>() {
            private Iterator<LocatedFileStatus> statusIterator = fileStatusList.iterator();

            @Override
            public boolean hasNext() throws IOException {
                return statusIterator.hasNext();
            }

            @Override
            public LocatedFileStatus next() throws IOException {
                return statusIterator.next();
            }
        };

        when(hadoopFileSystem.exists(hdfsPath)).thenReturn(true);
        when(hadoopFileSystem.listFiles(hdfsPath, false)).thenReturn(fileStatusesIterator);

        long numOfModifiedFiles = hdfsService
                .countFilesUnderDirectoryThatModifiedLaterThan(directoryPath, modificationReferenceTime);

        Assert.assertEquals(1, numOfModifiedFiles);

        verify(hadoopFileSystem, times(1)).exists(hdfsPath);
        verify(hadoopFileSystem, times(1)).listFiles(hdfsPath, false);
        verifyNoMoreInteractions(hadoopFileSystem);
    }

}
