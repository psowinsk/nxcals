package cern.nxcals.service.internal.component;

import org.assertj.core.util.Sets;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.Set;
import java.util.function.Function;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SplitBulkActionTest {

    private final Function<Set<String>, Set<String>> mockOperation = mock(Function.class);

    private BulkAction<Set<String>, Set<String>> splitBulkAction = new SplitBulkAction<>();

    @Before
    public void setUp() throws Exception {
        reset(
                mockOperation
        );
    }

    @After
    public void tearDown() throws Exception {
        verifyNoMoreInteractions(
                mockOperation
        );
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionWhenReceivesNullInput() {
        Set<String> inputElements = null;

        splitBulkAction.apply(inputElements, mockOperation);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionWhenReceivesNullOperation() {
        Set<String> inputElements = Sets.newLinkedHashSet("Test", "Testy", "Testify");

        splitBulkAction.apply(inputElements, null);
    }

    @Test(expected = IllegalStateException.class)
    public void shouldPropagateExceptionWhenOperationThrowsOne() {
        Set<String> inputElements = Sets.newLinkedHashSet("Test", "Testy", "Testify");

        when(mockOperation.apply(inputElements)).thenThrow(new IllegalStateException("Testing is for the weak!!"));

        try {
            splitBulkAction.apply(inputElements, mockOperation);
        } catch (Exception e) {
            verify(mockOperation).apply(inputElements);
            throw e;
        }
    }

    @Test(expected = IllegalStateException.class)
    public void shouldThrowExceptionWhenOperationResultIsNull() {
        Set<String> inputElements = Sets.newLinkedHashSet("Test", "Testy", "Testify");

        when(mockOperation.apply(inputElements)).thenReturn(null);

        try {
            splitBulkAction.apply(inputElements, mockOperation);
        } catch (Exception e) {
            verify(mockOperation).apply(inputElements);
            throw e;
        }
    }

    @Test
    public void shouldContinueWhenBatchOperationResultIsEmpty() {
        Set<String> inputElements = Sets.newLinkedHashSet("Test", "Testy", "Testify");

        when(mockOperation.apply(inputElements)).thenReturn(Collections.emptySet());

        Set<String> result = splitBulkAction.apply(inputElements, mockOperation);

        assertNotNull(result);

        verify(mockOperation).apply(inputElements);
    }

    @Test
    public void shouldSplitIn2ChunksAndApplyBulkAction() {
        Set<String> elements = Sets.newLinkedHashSet("Test", "Testy", "Testify");

        when(mockOperation.apply(any())).thenAnswer(invocation -> {
            Set<String> invocationElemets = (Set<String>) invocation.getArguments()[0];
            String result = null;
            if (invocationElemets.size() == 2) {
                result = "Duper";
            } else {
                result = "Super";
            }
            return Collections.singleton(result);
        });

        splitBulkAction = new SplitBulkAction<>(2);

        Set<String> actionResult = splitBulkAction.apply(elements, mockOperation);

        assertNotNull(actionResult);
        Assert.assertEquals(2, actionResult.size());
        Assert.assertTrue(actionResult.contains("Super"));
        Assert.assertTrue(actionResult.contains("Duper"));

        verify(mockOperation, times(2)).apply(any());
    }

}
