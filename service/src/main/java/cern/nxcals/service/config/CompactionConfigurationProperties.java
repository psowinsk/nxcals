package cern.nxcals.service.config;

import cern.nxcals.service.internal.InternalCompactionService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Represents a grouping component for all {@link InternalCompactionService} related
 * application properties.
 */
@Data
@Component
public class CompactionConfigurationProperties {

    private final long compactionToleranceHours;
    private final long compactionMigrationToleranceHours;
    private final long minFileModificationBeforeCompactionHours;

    public CompactionConfigurationProperties(
            @Value(value = "${compaction.tolerance.period.hours}") long compactionToleranceHours,
            @Value(value = "${compaction.migration.tolerance.period.hours}") long compactionMigrationToleranceHours,
            @Value(value = "${compaction.min.file.modification.period.hours}") long minFileModificationBeforeCompactionHours) {
        this.compactionToleranceHours = compactionToleranceHours;
        this.compactionMigrationToleranceHours = compactionMigrationToleranceHours;
        this.minFileModificationBeforeCompactionHours = minFileModificationBeforeCompactionHours;
    }

}
