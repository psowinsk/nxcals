package cern.nxcals.service.config;

import cern.nxcals.service.security.KerberosAuthenticationFailureHandler;
import cern.nxcals.service.security.UserDetailsServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.kerberos.authentication.KerberosAuthenticationProvider;
import org.springframework.security.kerberos.authentication.KerberosServiceAuthenticationProvider;
import org.springframework.security.kerberos.authentication.sun.SunJaasKerberosClient;
import org.springframework.security.kerberos.authentication.sun.SunJaasKerberosTicketValidator;
import org.springframework.security.kerberos.web.authentication.ResponseHeaderSettingKerberosAuthenticationSuccessHandler;
import org.springframework.security.kerberos.web.authentication.SpnegoAuthenticationProcessingFilter;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Configuration class with definition of all the Spring beams necessary to secure application with Kerberos.
 *
 * @author wjurasz
 */
@Configuration
@EnableWebSecurity
@Slf4j
public class KerberosConfiguration {


    @Value("${service.kerberos.keytab}")
    private String keytabPath;

    @Value("${service.kerberos.realm}")
    private String realm;

    @Value("${service.kerberos.type}")
    private String kerberosType;

    @Value("${service.host:#{null}}")
    private String serviceHost;

    private String servicePrincipal = null;

    /**
     * Spring implementation of AuthenticationProvider using Kerberos, for clients.
     *
     * @param userDetailsServiceImpl Authorization service.
     * @return Instance of {@link KerberosAuthenticationProvider}
     */
    @Bean
    @Autowired
    public KerberosAuthenticationProvider kerberosAuthenticationProvider(
            UserDetailsServiceImpl userDetailsServiceImpl) {
        KerberosAuthenticationProvider provider = new KerberosAuthenticationProvider();
        SunJaasKerberosClient client = new SunJaasKerberosClient();
        provider.setKerberosClient(client);
        provider.setUserDetailsService(userDetailsServiceImpl);
        return provider;
    }

    /**
     * Spring implementation of AuthenticationProvider using Kerberos, for service.
     *
     * @param sunJaasKerberosTicketValidator Kerberos ticker validator implemented for SUN JRE
     * @param userDetailsServiceImpl         Authorization service.
     * @return Instance of {@link KerberosAuthenticationProvider}
     */
    @Bean
    @Autowired
    public KerberosServiceAuthenticationProvider kerberosServiceAuthenticationProvider(
            SunJaasKerberosTicketValidator sunJaasKerberosTicketValidator,
            UserDetailsServiceImpl userDetailsServiceImpl) {
        KerberosServiceAuthenticationProvider provider = new KerberosServiceAuthenticationProvider();
        provider.setTicketValidator(sunJaasKerberosTicketValidator);
        provider.setUserDetailsService(userDetailsServiceImpl);
        return provider;
    }

    /**
     * Bean that validate Kerberos ticker from incoming request.
     * It requires principal of service that it's running in and keytab file path for this service.
     *
     * @return Instance of {@link SunJaasKerberosTicketValidator}
     */
    @Bean
    public SunJaasKerberosTicketValidator sunJaasKerberosTicketValidator() {
        SunJaasKerberosTicketValidator ticketValidator = new SunJaasKerberosTicketValidator();
        ticketValidator.setServicePrincipal(getServicePrincipal());
        ticketValidator.setKeyTabLocation(new FileSystemResource(keytabPath));
        return ticketValidator;
    }

    /**
     * Creates bean responsible for gathering SPNEGO token from HTTP request and transforming it into kerberos token.
     *
     * @param authenticationManager Spring @AuthenticationManager instance provided by @AuthenticationManagerBuilder
     * @return Instance of {@link SpnegoAuthenticationProcessingFilter}
     */
    @Bean
    public SpnegoAuthenticationProcessingFilter spnegoAuthenticationProcessingFilter(
            AuthenticationManager authenticationManager) {
        SpnegoAuthenticationProcessingFilter filter = new SpnegoAuthenticationProcessingFilter();
        filter.setAuthenticationManager(authenticationManager);
        filter.setSuccessHandler(new ResponseHeaderSettingKerberosAuthenticationSuccessHandler());
        filter.setFailureHandler(new KerberosAuthenticationFailureHandler());
        return filter;
    }

    /**
     * Service responsible for user authorization.
     *
     * @return Instance of {@link UserDetailsServiceImpl}
     */
    @Bean
    public UserDetailsServiceImpl nxcalsUserDetailsService() {
        return new UserDetailsServiceImpl();
    }

    private String getServicePrincipal() {
        if (servicePrincipal == null) {
            if (this.serviceHost == null) {
                log.warn("service.host property not found, trying to obtain hostname programmatically");
                try {
                    String host = InetAddress.getLocalHost().getHostName();
                    servicePrincipal = kerberosType + "/" + host + "@" + realm;
                    log.info("Host name obtained: {}", host);
                } catch (UnknownHostException ex) {
                    log.error(
                            "Couldn't find service.host property. Backup host name resolving also failed. Cannot create service principal.",
                            ex);
                    throw new IllegalStateException(ex);
                }
            } else {
                servicePrincipal = kerberosType + "/" + serviceHost + "@" + realm;
            }
        }
        return servicePrincipal;
    }

}
