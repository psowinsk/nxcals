package cern.nxcals.service.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.kerberos.authentication.KerberosAuthenticationProvider;
import org.springframework.security.kerberos.authentication.KerberosServiceAuthenticationProvider;
import org.springframework.security.kerberos.web.authentication.SpnegoAuthenticationProcessingFilter;
import org.springframework.security.kerberos.web.authentication.SpnegoEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

/**
 * Class containing authentication configuration. It set up kerberos based authentication for NXCALS service.
 *
 * @author wjurasz
 * @author kpodsiad
 */
@Configuration
public class AuthConfigProvider extends WebSecurityConfigurerAdapter {

    @Autowired
    private SpnegoAuthenticationProcessingFilter spnegoAuthenticationProcessingFilter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .exceptionHandling()
                .authenticationEntryPoint(spnegoEntryPoint())
                .and()
                .authorizeRequests()
                .anyRequest().authenticated()
                .and()
                .addFilterBefore(
                        spnegoAuthenticationProcessingFilter,
                        BasicAuthenticationFilter.class);
        //The service won't be used in browser, so we don't need CSRF protection.
        //Reference: Point 18.3 in https://docs.spring.io/spring-security/site/docs/current/reference/html/csrf.html
        http.csrf().disable();
    }

    /**
     * Method adding necessary Kerberos related beams to @AuthenticationManagerBuilder.
     *
     * @param auth                                  Spring builder that creates @AuthenticationManager
     * @param kerberosAuthenticationProvider        Spring implementation of AuthenticationProvider using Kerberos, for clients.
     * @param kerberosServiceAuthenticationProvider Spring implementation of AuthenticationProvider using Kerberos, for services.
     */
    @Autowired
    public void configureGlobal(final AuthenticationManagerBuilder auth,
            KerberosAuthenticationProvider kerberosAuthenticationProvider,
            KerberosServiceAuthenticationProvider kerberosServiceAuthenticationProvider) {
        auth.authenticationProvider(kerberosAuthenticationProvider)
                .authenticationProvider(kerberosServiceAuthenticationProvider);
    }

    private SpnegoEntryPoint spnegoEntryPoint() {
        return new SpnegoEntryPoint();
    }

}