package cern.nxcals.service.config;

import cern.nxcals.common.security.KerberosRelogin;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.FileSystem;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import java.io.IOException;
import java.io.UncheckedIOException;

@Configuration
@Slf4j
public class SpringConfig {

    @Bean(name = "kerberos", initMethod = "start")
    public KerberosRelogin kerberosRelogin(@Value("${kerberos.principal}") String principal,
            @Value("${kerberos.keytab}") String keytab,
            @Value("${kerberos.relogin}") boolean enableRelogin) {
        return new KerberosRelogin(principal, keytab, enableRelogin);
    }

    @Bean
    @DependsOn("kerberos")
    public FileSystem hadoopFileSystem() {
        log.info("Creating HDFS FileSystem");
        org.apache.hadoop.conf.Configuration conf = new org.apache.hadoop.conf.Configuration();
        try {
            return FileSystem.get(conf);
        } catch (IOException e) {
            throw new UncheckedIOException("Cannot access filesystem", e);
        }
    }

}
