/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.internal;

import cern.nxcals.common.domain.EntityResources;

import java.util.Map;
import java.util.Set;

public interface InternalEntityResourcesService {

    Set<EntityResources> findBySystemIdKeyValuesAndTimeWindow(long systemId, Map<String, Object> entityKeyValues,
            long startTime, long endTime);

    Set<EntityResources> findByEntityIdAndTimeWindow(long entityId, long startTime, long endTime);
}
