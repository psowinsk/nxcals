package cern.nxcals.service.internal.component;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@Slf4j
public class SplitBulkAction<T, R> implements BulkAction<Set<T>, Set<R>> {

    private static final int DEFAULT_MAX_BATCH_SIZE = 1000;

    private final int maxBatchSize;

    public SplitBulkAction() {
        log.debug("Using the default batch size: {}", DEFAULT_MAX_BATCH_SIZE);
        this.maxBatchSize = DEFAULT_MAX_BATCH_SIZE;
    }

    public SplitBulkAction(int maxBatchSize) {
        log.debug("Using the provided batch size: {}", maxBatchSize);
        this.maxBatchSize = maxBatchSize;
    }

    @Override
    public Set<R> apply(Set<T> elements, Function<Set<T>, Set<R>> operation) {
        Objects.requireNonNull(elements, "Input elements can not be null!");
        Objects.requireNonNull(operation, "Operation to apply can not be null!");

        List<T> sanitizedElements = elements.stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        log.debug("Received elements for split bulk operation with total size: {}", elements.size());
        List<Set<T>> elementBatches = Lists.partition(sanitizedElements, maxBatchSize).stream()
                .map(HashSet::new)
                .collect(Collectors.toList());

        log.debug("Elements separated into {} batches with max size of {} elements each", elementBatches.size(),
                maxBatchSize);

        Set<R> bulkResult = new HashSet<>();
        for (Set<T> elementBatch : elementBatches) {
            log.debug("Applying operation on batch with size: {}", elementBatch.size());
            Set<R> batchResult = operation.apply(elementBatch);
            if (batchResult == null) {
                throw new IllegalStateException("The batch operation result can not be null!");
            }
            bulkResult.addAll(batchResult);
        }
        return bulkResult;
    }

    public long getMaxBatchSize() {
        return maxBatchSize;
    }
}
