package cern.nxcals.service.internal;

import java.time.Instant;

/**
 * Defines behaviour for services that can interact with file storage systems.
 */
public interface InternalStorageService {

    /**
     * Counts all the files located under a directory path, that have been modified after
     * the given time instant.
     *
     * @param directoryPath         a {@link String} representation of the directory path
     * @param beginModificationTime an {@link Instant} that points to a time that will be used
     *                              as reference to check if any files exist that have been modified after it.
     * @return the number of files, located under the provided directory path, that have been modified later than
     * the given time
     */
    long countFilesUnderDirectoryThatModifiedLaterThan(String directoryPath, Instant beginModificationTime);

}
