package cern.nxcals.service.internal;

import cern.nxcals.common.utils.HdfsPathDecoder;
import cern.nxcals.service.config.CompactionConfigurationProperties;
import cern.nxcals.service.repository.EntityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Clock;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

/**
 * Implementation of @see {@link InternalCompactionService } interface.
 *
 * @author ntsvetko
 */
@Service
@Slf4j
public class InternalCompactionServiceImpl implements InternalCompactionService {

    private final long compactionToleranceHours;
    private final long compactionMigrationToleranceHours;
    private final long minFileModificationBeforeCompactionHours;
    private final Clock clock;
    private final EntityRepository entityRepository;

    private final InternalStorageService storageService;

    @Autowired
    public InternalCompactionServiceImpl(EntityRepository entityRepository,
            InternalStorageService storageService,
            CompactionConfigurationProperties compactionConfigurationProperties) {
        this(entityRepository, storageService, Clock.systemDefaultZone(), compactionConfigurationProperties);
    }

    InternalCompactionServiceImpl(EntityRepository entityRepository,
            InternalStorageService storageService, Clock clock,
            CompactionConfigurationProperties compactionConfigurationProperties) {
        this.entityRepository = entityRepository;
        this.storageService = storageService;
        this.clock = clock;
        this.compactionToleranceHours = compactionConfigurationProperties.getCompactionToleranceHours();
        this.compactionMigrationToleranceHours = compactionConfigurationProperties
                .getCompactionMigrationToleranceHours();
        this.minFileModificationBeforeCompactionHours = compactionConfigurationProperties
                .getMinFileModificationBeforeCompactionHours();
    }

    @Override
    public boolean shouldCompact(String path) {
        try {
            Path hdfsPath = Paths.get(path);
            Instant compactionDateTime = buildCompactionDateTime(HdfsPathDecoder.findDateFrom(hdfsPath));
            log.debug("HDFS Path = {} with Compaction Time = {}", path, compactionDateTime);
            return isDateBeforeEdgeDateTime(compactionDateTime) &&
                    hasNoEntitiesLocked(HdfsPathDecoder.findSystemIdFrom(hdfsPath),
                            HdfsPathDecoder.findPartitionIdFrom(hdfsPath), compactionDateTime) &&
                    hasNoRecentlyModifiedFiles(path);
        } catch (InvalidPathException e) {
            throw new IllegalArgumentException("Cannot convert provided string to PATH: " + path, e);
        }
    }

    private boolean isDateBeforeEdgeDateTime(Instant compactionDateTime) {
        //we give a grace time, of externally configured, additional hours for processing of previous day
        Instant edgeTime = this.clock.instant().minus(compactionToleranceHours, ChronoUnit.HOURS);
        log.debug("Edge Time = {}", edgeTime);
        return compactionDateTime.isBefore(edgeTime);
    }

    private Instant buildCompactionDateTime(String date) {
        ZonedDateTime folderDateTime = ZonedDateTime
                .parse(date + "T00:00Z[UTC]", DateTimeFormatter.ISO_ZONED_DATE_TIME);
        return folderDateTime.toInstant();
    }

    private boolean hasNoEntitiesLocked(long systemId, long partitionId, Instant compactionTime) {
        //We add 1 HOUR of tolerance to be sure that all the data has been written to HDFS
        Instant watermarkUpdateTime = this.clock.instant().minus(compactionMigrationToleranceHours, ChronoUnit.HOURS);
        long totalEntitiesLocked = this.entityRepository
                .countByPartitionSystemIdAndPartitionIdAndLockedUntilStampGreaterThanEqual(
                        systemId, partitionId, compactionTime);
        log.debug(
                "Number of locked entities for operations = {} for systemId={}, partitionId={}, compactionTime={}, " +
                        "watermarkUpdateTime={} ", totalEntitiesLocked, systemId, partitionId,
                compactionTime, watermarkUpdateTime);
        return totalEntitiesLocked == 0;
    }

    private boolean hasNoRecentlyModifiedFiles(String hdfsDirectoryPath) {
        final Instant minimumLastFileModificationTime = Instant.now().minus(
                minFileModificationBeforeCompactionHours, ChronoUnit.HOURS);

        long numOfRecentlyModifiedFiles = storageService.countFilesUnderDirectoryThatModifiedLaterThan(
                hdfsDirectoryPath, minimumLastFileModificationTime);
        log.info("Found {} recently modified files, under HDFS directory: {}", numOfRecentlyModifiedFiles,
                hdfsDirectoryPath);

        return numOfRecentlyModifiedFiles == 0;
    }

}
