/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.internal;

import cern.nxcals.common.domain.EntityData;
import cern.nxcals.service.domain.Entity;
import cern.nxcals.service.domain.Partition;
import cern.nxcals.service.domain.Schema;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface InternalEntityService {

    /**
     * Creates all objects necessary to persist the Entity state in the NXCALS system.
     * In this case only the history might be changed. Otherwise the entity & partition must exist before.
     * @param systemId an identification number that corresponds to the
     *                         target {@link cern.nxcals.service.domain.System}
     * @param entityId an id of an existing entity
     * @param partitionId an id of an existing partition
     * @param recordTimestamp a record timestamp for which the action is done
     * @param recordDefinition a json map of key names and @see Data types (a schema)
     *
     * @see Partition
     * @see Schema
     * @see Entity
     * @see cern.nxcals.service.domain.System
     *
     * @return {@link Entity)
     */
    Entity findOrCreateEntityFor(long systemId, long entityId, long partitionId, String recordDefinition, long recordTimestamp );

    /**
     * Creates all objects necessary to persist the Entity state in the NXCALS system.
     *
     * @param systemId         an identification number that corresponds to the
     *                         target {@link cern.nxcals.service.domain.System}
     * @param keyValues        a {@link Map} of keys and values
     * @param partitionValues  a {@link Map} of keys and values
     * @param recordDefinition a {@link Map} of key names and @see Data types.
     * @param recordTimestamp  timestamp value to be used as craetion time and history reference
     * @return an {@link Entity} with all corresponding {@link Partition} {@link Schema} and @see
     * @see Partition
     * @see Schema
     * @see Entity
     * @see cern.nxcals.service.domain.System
     */
    Entity findOrCreateEntityFor(long systemId, Map<String, Object> keyValues, Map<String, Object> partitionValues,
            String recordDefinition, long recordTimestamp);

    /**
     * Finds an entity for given system & keys with history limited to the time range <startTime, endTime>.
     *
     * @param systemId        the identification number that corresponds to the target {@link cern.nxcals.service.domain.System}
     * @param entityKeyValues a {@link Map} of properties that belong to an {@link Entity}
     * @param startTime       the search start time in epoch milliseconds
     * @param endTime         the search end time in epoch milliseconds
     * @return an {@link Entity} instance that matches the provided search criteria
     */
    Entity findEntityWithHistForTimeWindow(long systemId, Map<String, Object> entityKeyValues, long startTime,
            long endTime);

    /**
     * Finds an entity by the given system id and it's key-value properties.
     *
     * @param systemId        the identification number that corresponds to the target {@link cern.nxcals.service.domain.System}
     * @param entityKeyValues a {@link Map} of properties that belong to an {@link Entity}
     * @return an {@link Entity} instance that matches the given search criteria
     */
    Entity findByPartitionSystemIdAndKeyValues(long systemId, Map<String, Object> entityKeyValues);

    /**
     * Finds an entity by the given entity id for a specified time window
     *
     * @param entityId  the identification number that corresponds to the target {@link Entity}
     * @param startTime the search start time in epoch milliseconds
     * @param endTime   the search end time in epoch milliseconds
     * @return an {@link Entity} instance that matches the provided search criteria
     */
    Entity findByEntityIdAndTimeWindow(long entityId, long startTime, long endTime);

    Entity extendEntityFirstHistoryDataFor(long entityId, String schema, long from);

    /**
     * Finds all entities that their key-value properties are following the given pattern.
     *
     * @param regex a {@link String} representation of the key-values expression
     * @return a {@link List} of all the {@link Entity} instances that match the provided expression
     */
    List<Entity> findByKeyValueLike(String regex);

    /**
     * Renames a list of entities.
     *
     * @param entityDataList a {@link List} of {@link EntityData}s that identify the entities to be updated.
     *                       We only allow change of the keyValues
     * @return a {@link List} of updated entities.
     */
    List<Entity> updateEntities(List<EntityData> entityDataList);

    /**
     * Fetches an {@link Entity} instance based on the provided entity identification number.
     * The fetched entity's history data is adjusted to contain <b>only</b> the latest registered entry.
     *
     * @param entityId a number that corresponds to the target entity's id
     * @return the requested {@link Entity} instance, queried by the provided id
     */
    Entity findById(long entityId);

    /**
     * Fetches a {@link Set<Entity>} that contains all found (if any) {@link Entity} instances, based on
     * the provided collection of entity identification numbers.
     * Each fetched entity's history data is adjusted to contain <b>only</b> the latest registered entry.
     *
     * @param entityIds a {@link Set<Long>} of ids that correspond to the target entities
     * @return a {@link Set<Entity>} that contains all target entities queried by the provided ids,
     * or an empty collection, if nothing was found.
     */
    Set<Entity> findAllByIdIn(Set<Long> entityIds);
}
