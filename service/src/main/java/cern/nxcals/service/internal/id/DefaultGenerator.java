/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.internal.id;

import cern.nxcals.common.concurrent.AutoCloseableLock;
import cern.nxcals.service.domain.SequenceType;
import cern.nxcals.service.internal.IdGenerator;
import com.google.common.util.concurrent.Striped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;

/**
 * Default implementation of id generator. It uses the underlying db based pool to obtain a range of id numbers to be
 * used. A number from the range is then served on each request until the range gets fully drained; the pool gets then
 * called for the next range.
 *
 * @author Marcin Sobieszek
 * @date Jul 25, 2016 3:41:07 PM
 */
@Service
public class DefaultGenerator implements IdGenerator {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultGenerator.class);
    private final Striped<Lock> locks = Striped.lazyWeakLock(1000);
    private final ConcurrentMap<SequenceType, AtomicLong> ids = new ConcurrentHashMap<>();
    private final ConcurrentMap<SequenceType, Long> sequenceLastValue = new ConcurrentHashMap<>();

    @Value("${id.generator.batch.size:10}")
    private int batchSize = 10;

    @Autowired
    private DbBasedIdPool idPool;

    @PostConstruct
    public void init() {
        for (SequenceType st : SequenceType.values()) {
            this.ids.putIfAbsent(st, new AtomicLong(0));
            this.sequenceLastValue.putIfAbsent(st, -1L);
        }
    }

    @Override
    public long getIdFor(SequenceType type) {
        Objects.requireNonNull(type);
        try (AutoCloseableLock aLock = this.getLockFor(type)) {
            AtomicLong current = this.ids.get(type);
            long firstIdExceedingSequence = sequenceLastValue.get(type);
            long value = current.get();
            if (this.notValid(firstIdExceedingSequence, value)) {
                LOGGER.info("Calling db pool for the next {} ids, current value = {}", this.batchSize, value);
                long offset = this.idPool.getNextIdAndReserveQuantityLongSequence(type, this.batchSize);
                sequenceLastValue.put(type, offset + batchSize);
                current.set(offset);
                LOGGER.info("Obtained pool offset {} for sequenceType {}", offset, type);
            }
            return current.getAndIncrement();
        }
    }

    private boolean notValid(long firstIdExceedingSequence, long value) {
        return firstIdExceedingSequence == -1 || value == firstIdExceedingSequence;
    }

    private AutoCloseableLock getLockFor(SequenceType name) {
        return AutoCloseableLock.getFor(this.locks.get(name.getSeqName()));
    }
}
