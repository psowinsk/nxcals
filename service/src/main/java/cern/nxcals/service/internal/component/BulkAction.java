package cern.nxcals.service.internal.component;

import java.util.Collection;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Defines behaviour for a bulk action that applies an operation to a given collection of elements.
 *
 * @param <T> the type of the collection
 * @param <R> the type of the resulted collection
 */
public interface BulkAction<T extends Collection<?>, R extends Collection<?>>
        extends BiFunction<T, Function<T, R>, R> {

    /**
     * Applies a given operation to a collection of elements and provides the result.
     *
     * @param elements  the elements to be used as input of the action
     * @param operation a {@link Function} that contains the operation to be applied on the elements
     * @return a collection, containing the result of the bulk operation
     */
    @Override
    R apply(T elements, Function<T, R> operation);

}
