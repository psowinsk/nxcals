package cern.nxcals.service.internal;

import cern.nxcals.common.utils.HdfsFileUtils;
import cern.nxcals.service.domain.SupportedFileTypePredicate;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

@Service
@Slf4j
public class InternalStorageServiceImpl implements InternalStorageService {

    private static final Predicate<LocatedFileStatus> SUPPORTED_FILE_TYPES_PREDICATE =
            SupportedFileTypePredicate.AVRO.or(SupportedFileTypePredicate.PARQUET);

    private final FileSystem hadoopFileSystem;

    @Autowired
    public InternalStorageServiceImpl(FileSystem hadoopFileSystem) {
        this.hadoopFileSystem = Objects.requireNonNull(hadoopFileSystem, "Hadoop File System can not be null!");
    }

    @Override
    public long countFilesUnderDirectoryThatModifiedLaterThan(String directoryPath, Instant beginModificationTime) {
        final Path hdfsPath = getHadoopPathFromString(directoryPath);

        List<LocatedFileStatus> fileStatuses = HdfsFileUtils.findFilesInPath(
                hadoopFileSystem, hdfsPath, HdfsFileUtils.SearchType.NONRECURSIVE, SUPPORTED_FILE_TYPES_PREDICATE);

        log.debug("Found status information for {} filtered files under HDFS directory: {}",
                fileStatuses.size(), directoryPath);
        return fileStatuses.stream()
                .filter(fileStatus -> {
                    Instant lastModification = Instant.ofEpochMilli(fileStatus.getModificationTime());
                    return lastModification.isAfter(beginModificationTime);
                }).count();
    }

    /**
     * Transforms a string path to an actual HDFS {@link Path}.
     *
     * @param hdfsPathString the {@link String} representation of a resource path in HDFS
     * @return the actual path in hadoop's HDFS tha corresponds to the provided reference
     */
    private Path getHadoopPathFromString(final String hdfsPathString) {
        if (StringUtils.isBlank(hdfsPathString)) {
            throw new IllegalArgumentException("Provided HDFS directory path, can not be null or blank string!");
        }
        return new Path(hdfsPathString);
    }

}
