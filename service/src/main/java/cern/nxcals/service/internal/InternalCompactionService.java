package cern.nxcals.service.internal;

/**
 * @author ntsvetko
 */
public interface InternalCompactionService {

    /**
     * Checks whether the requested HDFS path should be compacted
     *
     * @param path NXCALS HDFS path
     * @return true if the tested path can be compacted, otherwise it returns false
     */
    boolean shouldCompact(String path);
}
