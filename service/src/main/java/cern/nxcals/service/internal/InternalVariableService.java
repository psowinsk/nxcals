/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.internal;

import cern.nxcals.common.domain.VariableData;
import cern.nxcals.service.domain.Variable;

import java.util.List;

public interface InternalVariableService {

    Variable registerOrUpdateVariableFor(VariableData variableData);

    Variable findByVariableName(String variableName);

    Variable findByVariableNameAndTimeWindow(String variableName, long startTime, long endTime);

    List<Variable> findByNameLike(String regex);

    List<Variable> findByDescriptionLike(String regex);
}
