/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.internal;

import cern.nxcals.common.domain.EntityResources;
import cern.nxcals.common.domain.ResourceData;
import cern.nxcals.common.domain.TimeWindow;
import cern.nxcals.common.utils.HdfsFileUtils;
import cern.nxcals.common.utils.TimeUtils;
import cern.nxcals.service.domain.Entity;
import cern.nxcals.service.domain.EntityHistory;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.time.temporal.ChronoUnit.DAYS;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import static org.apache.commons.lang3.ObjectUtils.max;
import static org.apache.commons.lang3.ObjectUtils.min;

@Service
@Slf4j
public class InternalEntityResourcesServiceImpl implements InternalEntityResourcesService {
    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd").withZone(ZoneOffset.UTC);

    private static final String HBASE_TABLE_NAME_DELIMITER = "__";

    private final String dataLocationPrefix;
    private final String dataLocationSuffix;
    private final String hbaseNamespace;

    private final InternalEntityService entityService;

    @Autowired
    public InternalEntityResourcesServiceImpl(@Value("${data.location.prefix}") String dataLocationPrefix,
            @Value("${data.location.suffix}") String dataLocationSuffix,
            @Value("${data.location.hbase.tablespace}") String hbaseNamespace,
                                              InternalEntityService entityService) {

        this.dataLocationPrefix = dataLocationPrefix;
        this.dataLocationSuffix = dataLocationSuffix;
        this.hbaseNamespace = hbaseNamespace;
        this.entityService = entityService;
    }

    @Override
    public Set<EntityResources> findBySystemIdKeyValuesAndTimeWindow(long systemId, Map<String, Object> entityKeyValues,
            long startTime, long endTime) {
        Entity entity = entityService.findEntityWithHistForTimeWindow(systemId, entityKeyValues, startTime, endTime);
        return getEntityResourcesData(startTime, endTime, entity);
    }

    @Override
    public Set<EntityResources> findByEntityIdAndTimeWindow(long entityId, long startTime, long endTime) {
        Entity entity = entityService.findByEntityIdAndTimeWindow(entityId, startTime, endTime);
        return getEntityResourcesData(startTime, endTime, entity);
    }

    private Set<EntityResources> getEntityResourcesData(long startTime, long endTime, Entity entity) {
        Map<Long, ResourceData> schemaResources = new HashMap<>();
        if (entity.getEntityHistories() == null) {
            throw new IllegalArgumentException(
                    "There is no entity history for the selected time window for entityId=" + entity.getId()
                            + " startTime=" + startTime + " endTime=" + endTime);
        }

        entity.getEntityHistories().forEach(h -> schemaResources.compute(h.getSchema().getId(), (k, v) -> getResourceData(h, v, startTime, endTime)));


        return entity.getEntityHistories().stream()
                .map(entityHistory -> EntityResources.builder()
                        .id(entity.getId())
                        .entityKeyValues(entity.getKeyValues())
                        .systemData(entityHistory.getPartition().getSystem().toSystemData())
                        .partitionData(entityHistory.getPartition().toPartitionData())
                        .schemaData(entityHistory.getSchema().toSchemaData())
                        .resourceData(schemaResources.get(entityHistory.getSchema().getId())).build())
                .collect(Collectors.toSet());
    }

    private ResourceData getResourceData(EntityHistory entityHistory, ResourceData resourceData, long startTime,
                                         long endTime) {
        Set<URI> hdfsPaths = new HashSet<>();
        Set<String> hbaseTables = new HashSet<>();

        if (resourceData != null) {
            hdfsPaths.addAll(resourceData.getHdfsPaths());
            hbaseTables.addAll(resourceData.getHbaseTableNames());
        }

        hdfsPaths.addAll(getFileLocationsFor(entityHistory, startTime, endTime));
        hbaseTables.add(getHBaseTableNameFor(entityHistory));
        return ResourceData.builder()
                .hdfsPaths(hdfsPaths)
                .hbaseNamespace(hbaseNamespace)
                .hbaseTableNames(hbaseTables)
                .build();
    }

    private Set<URI> getFileLocationsFor(EntityHistory entityHistory, long startTime, long endTime) {
        TimeWindow timeWindowPerLocation = this
                .getTimeWindowPerLocation(entityHistory, TimeUtils.getInstantFromNanos(startTime),
                        TimeUtils.getInstantFromNanos(endTime));
        return Stream.iterate(timeWindowPerLocation.getStartTime(), date -> date.plus(1, DAYS)).limit(DAYS
                .between(timeWindowPerLocation.getStartTime().truncatedTo(DAYS),
                        timeWindowPerLocation.getEndTime().truncatedTo(DAYS)) + 1)
                .map(time -> this.buildPathFor(time, entityHistory)).collect(Collectors.toSet());
    }

    private TimeWindow getTimeWindowPerLocation(EntityHistory entityHistory, Instant startTime, Instant endTime) {
        Instant validToStamp = firstNonNull(entityHistory.getValidToStamp(), endTime);

        return TimeWindow.between(
                max(entityHistory.getValidFromStamp(), startTime),
                min(validToStamp, endTime));
    }

    private URI buildPathFor(Instant time, EntityHistory entityHistory) {
        String date = time.atZone(ZoneId.of("UTC")).format(HdfsFileUtils.STAGE_DIRECTORY_DATE_FORMATTER);

        StringJoiner joiner = new StringJoiner(Path.SEPARATOR, dataLocationPrefix, dataLocationSuffix);
        joiner.add(entityHistory.getPartition().getSystem().getId().toString())
                .add(entityHistory.getPartition().getId().toString()).add(entityHistory.getSchema().getId().toString())
                .add(HdfsFileUtils.expandDateToNestedPaths(date));
        try {
            return new URI(joiner.toString());
        } catch (URISyntaxException e) {
            log.error(String.format("Wrongly build resource path for entity with key %s and date %s",
                    entityHistory.getEntity().getKeyValues(), dateTimeFormatter.format(time)), e);
            return null;
        }
    }

    private String getHBaseTableNameFor(EntityHistory entityHistory) {
        StringJoiner joiner = new StringJoiner(HBASE_TABLE_NAME_DELIMITER);

        joiner.add(entityHistory.getPartition().getSystem().getId().toString())
                .add(entityHistory.getPartition().getId().toString())
                .add(entityHistory.getSchema().getId().toString());

        return joiner.toString();
    }
}