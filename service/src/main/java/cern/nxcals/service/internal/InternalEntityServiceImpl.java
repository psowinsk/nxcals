/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.internal;

import cern.nxcals.common.concurrent.AutoCloseableLock;
import cern.nxcals.common.domain.EntityData;
import cern.nxcals.common.utils.TimeUtils;
import cern.nxcals.service.domain.Entity;
import cern.nxcals.service.domain.EntityHistory;
import cern.nxcals.service.domain.Partition;
import cern.nxcals.service.domain.Schema;
import cern.nxcals.service.domain.System;
import cern.nxcals.service.internal.component.BulkAction;
import cern.nxcals.service.repository.EntityHistoryRepository;
import cern.nxcals.service.repository.EntityRepository;
import cern.nxcals.service.repository.PartitionRepository;
import cern.nxcals.service.repository.SchemaRepository;
import cern.nxcals.service.repository.SystemRepository;
import cern.nxcals.service.rest.ConfigDataConflictException;
import cern.nxcals.service.rest.NotFoundRuntimeException;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collectors;

import static cern.nxcals.common.utils.KeyValuesUtils.convertMapIntoAvroSchemaString;
import static cern.nxcals.common.utils.TimeUtils.getInstantFromNanos;
import static java.lang.String.format;

/**
 * The implementation of the @see {@link cern.nxcals.service.internal.InternalEntityService} interface.
 *
 * @author ntsvetko
 * @author jwozniak
 * Remarks (msobiesz):
 * <ul>
 * <li>please make sure you always deal with suppliers for your optionals</li>
 * <li>I guess we should also handle here concurrent access (non-unique inserts) by other service instances simply by
 * catching apropriate DataAccessException and retrying the operation</li>
 * </ul>
 */

@Service
@Slf4j
public class InternalEntityServiceImpl extends BaseService implements InternalEntityService {

    private static final String ENTITY_NOT_FOUND_ERROR_FORMAT = "No system found for id %s";

    @Autowired
    private EntityRepository entityRepository;
    @Autowired
    private PartitionRepository partitionRepository;
    @Autowired
    private SystemRepository systemRepository;
    @Autowired
    private SchemaRepository schemaRepository;

    @Autowired
    private EntityHistoryRepository entityHistoryRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private BulkAction<Set<Long>, Set<Entity>> splitBatchAction;

    @Override
    @Transactional(transactionManager = "jpaTransactionManager")
    public Entity findOrCreateEntityFor(long systemId, Map<String, Object> entityKeyValues,
            Map<String, Object> partitionKeyValues,
            String recordFieldsWithTypes, long recordTimestamp) {

        log.info("findOrCreateEntityFor system={}, entity={} partition={} schema={} recordTimestamp={}", systemId,
                entityKeyValues, partitionKeyValues, recordFieldsWithTypes, recordTimestamp);

        System clientSystem = systemRepository.findById(systemId).orElseThrow(() -> {
            log.warn("There is no system coresponding to systemId = {}", systemId);
            return new NotFoundRuntimeException(format(ENTITY_NOT_FOUND_ERROR_FORMAT, systemId));
        });

        return internalFindOrCreateEntityFor(clientSystem, entityKeyValues, partitionKeyValues, recordFieldsWithTypes,
                recordTimestamp);
    }

    @Override
    public Entity findEntityWithHistForTimeWindow(long systemId, Map<String, Object> entityKeyValues, long startTime,
            long endTime) {
        if (startTime > endTime) {
            throw new IllegalArgumentException(
                    "Start time must be less or equal end time for systemId=" + systemId + " entityKey="
                            + entityKeyValues + " startTime=" + startTime + " endTime=" + endTime);
        }

        Entity entity = findEntityBySystemIdAndKeyValuesOrThrow(systemId, entityKeyValues);
        SortedSet<EntityHistory> entityHistory = getEntityHist(startTime, endTime, entity);
        entity.setEntityHistories(entityHistory);
        return entity;
    }

    private SortedSet<EntityHistory> getEntityHist(long startTime, long endTime, Entity entity) {
        return entityHistoryRepository.findByEntityAndTimestamps(entity, TimeUtils.getInstantFromNanos(startTime),
                TimeUtils.getInstantFromNanos(endTime));
    }

    @Override
    public Entity findByPartitionSystemIdAndKeyValues(long systemId, Map<String, Object> entityKeyValues) {
        Entity entity = findEntityBySystemIdAndKeyValuesOrThrow(systemId, entityKeyValues);
        EntityHistory entityHistory = entityHistoryRepository.findByEntityAndValidToStamp(entity, null)
                .orElseThrow(() -> new NotFoundRuntimeException("Cannot find history for entityId=" + entity.getId()));

        entity.clearHistory();
        entity.addEntityHist(entityHistory);
        return entity;
    }

    @Override
    public Entity findByEntityIdAndTimeWindow(long entityId, long startTime, long endTime) {
        if (startTime > endTime) {
            throw new IllegalArgumentException(
                    "Start time must be less or equal end time for entityId=" + entityId +
                            " startTime=" + startTime + " endTime=" + endTime);
        }
        Entity entity = findEntityByIdOrThrow(entityId);
        SortedSet<EntityHistory> entityHistory = getEntityHist(startTime, endTime, entity);
        entity.setEntityHistories(entityHistory);
        return entity;
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    @Override
    public Entity extendEntityFirstHistoryDataFor(long entityId, String schemaAsString, long fromNanos) {

        //This "ignored" is not really ignored but sonar complains here so we need to stick to this name.
        //I didn't want to put SuppressWarnings since it would suppress them all in this method (jwozniak)
        try (AutoCloseableLock ignored = this.getLockFor(String.valueOf(entityId))) {
            // extend entity history for the given system & entity key values.
            Entity entity = findEntityByIdOrThrow(entityId);
            entity.clearHistory();

            EntityHistory firstEntityHistoryElement = getFirstEntityHistoryEntryOrThrow(entity);

            // Perform validations
            long firstValidFromNanos = TimeUtils.getNanosFromInstant(firstEntityHistoryElement.getValidFromStamp());

            if (fromNanos < firstValidFromNanos) {
                Partition partition = entity.getPartition();
                Schema schema = findOrCreateSchema(DigestUtils.md5Hex(schemaAsString), schemaAsString);
                return extendEntityHistoryFor(entity, partition, schema, fromNanos, firstValidFromNanos);
            } else if ((firstEntityHistoryElement.getValidToStamp() == null || fromNanos < TimeUtils
                    .getNanosFromInstant(firstEntityHistoryElement.getValidToStamp()))) {
                //we ask to extend the history with some timestamp that is already in the first history time range but not ahead of it.
                String firstSchema = firstEntityHistoryElement.getSchema().getContent();
                if (firstSchema.equals(schemaAsString)) {
                    entity.addEntityHist(firstEntityHistoryElement);
                    return entity;
                } else {
                    throw new ConfigDataConflictException(
                            "Schema not matching for first history record=" + firstEntityHistoryElement
                                    + " and given timestamp=" + fromNanos + " for entity=" + entity);
                }
            } else {
                throw new ConfigDataConflictException(
                        "The timestamp=" + fromNanos + " is not before or within the first history record for entity"
                                + entity);
            }
        }
    }

    @Override
    public List<Entity> findByKeyValueLike(String keyValueExpression) {
        final List<Entity> entitiesMatched = entityRepository.findByKeyValuesLike(keyValueExpression);
        for (Entity entity : entitiesMatched) {
            Optional<EntityHistory> entityHist = entityHistoryRepository.findByEntityAndValidToStamp(entity, null);
            if (!entityHist.isPresent()) {
                throw new NotFoundRuntimeException(
                        format("Cannot find history for entityId=%d", entity.getId()));
            }
            entity.setEntityHistories(Sets.newTreeSet(Collections.singletonList(entityHist.get())));
        }
        return entitiesMatched;
    }

    @Override
    @Transactional(transactionManager = "jpaTransactionManager")
    public List<Entity> updateEntities(List<EntityData> entityDataList) {
        if (entityDataList == null) {
            throw new IllegalArgumentException("Provided entity information list can not be null!");
        }

        Map<Long, Entity> fetchedEntitiesMap = fetchEntityIdToEntityMapOrThrow(entityDataList);
        for (EntityData entityData : entityDataList) {
            Entity fetchedEntity = fetchedEntitiesMap.get(entityData.getId());

            // Needs to detach entity to allow explicit version data passing!
            // This can not be achieved differently, cause one can not modify
            // the version property (explicit version changes are not propagated to database).
            entityManager.detach(fetchedEntity);
            fetchedEntity.setRecVersion(entityData.getRecVersion());

            fetchedEntity.setKeyValues(convertMapIntoAvroSchemaString(entityData.getEntityKeyValues(),
                    fetchedEntity.getPartition().getSystem().getEntityKeyDefs()));

            Instant lockedUntilStamp = entityData.getLockUntilEpochNanos() == null ?
                    null : TimeUtils.getInstantFromNanos(entityData.getLockUntilEpochNanos());
            fetchedEntity.setLockedUntilStamp(lockedUntilStamp);
        }
        return Lists.newArrayList(entityRepository.save(fetchedEntitiesMap.values()));
    }

    private Map<Long, Entity> fetchEntityIdToEntityMapOrThrow(List<EntityData> entityDataList) {
        Set<Long> entityDataIds = entityDataList.stream()
                .map(EntityData::getId)
                .collect(Collectors.toSet());

        Set<Entity> fetchedEntities = splitBatchAction.apply(entityDataIds, entityIdsChunk ->
                Sets.newHashSet(entityRepository.findAll(entityIdsChunk)));

        Map<Long, Entity> fetchedEntitiesMap = fetchedEntities.stream()
                .collect(Collectors.toMap(Entity::getId, Function.identity()));

        if (fetchedEntitiesMap.size() < entityDataList.size()) {
            entityDataIds.removeAll(fetchedEntitiesMap.keySet());
            throw new NotFoundRuntimeException(format("Entities by the following ids were not found: [ %s ]",
                    StringUtils.join(entityDataIds, ", ")));
        }

        return fetchedEntitiesMap;
    }

    @Override
    public Entity findById(final long entityId) {
        Entity entity = findEntityByIdOrThrow(entityId);

        EntityHistory latestEntityHistory = getLatestEntityHistoryEntryOrThrow(entity);
        entity.setEntityHistories(new TreeSet<>(Collections.singleton(latestEntityHistory)));
        return entity;
    }

    @Override
    public Set<Entity> findAllByIdIn(final Set<Long> entityIds) {
        if (entityIds == null || entityIds.isEmpty()) {
            throw new IllegalArgumentException("Provided entity ids can not be null or empty!");
        }

        return splitBatchAction.apply(entityIds, entityIdsChunk -> {
            Set<Entity> fetchedEntitiesChunk = entityRepository.findAllByIdIn(entityIdsChunk);
            return mapEntitiesWithLatestHistoryEntries(fetchedEntitiesChunk);
        });
    }

    private Set<Entity> mapEntitiesWithLatestHistoryEntries(final Set<Entity> entities) {
        if (entities == null || entities.isEmpty()) {
            return Collections.emptySet();
        }

        Map<Entity, EntityHistory> entityToFirstHistoryEntryMap = entityHistoryRepository
                .findAllLatestEntriesByEntityIn(entities).stream()
                .collect(Collectors.toMap(EntityHistory::getEntity, Function.identity()));

        for (Entity entity : entities) {
            EntityHistory firstEntityHistoryEntry = entityToFirstHistoryEntryMap.get(entity);
            entity.setEntityHistories(new TreeSet<>(Collections.singleton(firstEntityHistoryEntry)));
        }
        return entities;
    }

    private Entity findEntityBySystemIdAndKeyValuesOrThrow(long systemId, Map<String, Object> keyValues) {
        String entityKeyDefinitions = findSystemOrThrowError(systemId).getEntityKeyDefs();
        String stringKeyValues = convertMapIntoAvroSchemaString(keyValues, entityKeyDefinitions);
        return entityRepository.findByPartitionSystemIdAndKeyValues(systemId,stringKeyValues)
                .orElseThrow(() -> new NotFoundRuntimeException(
                        String.format("Entity of system with id %s and key values %s not found.", systemId,
                                stringKeyValues)));
    }

    private System findSystemOrThrowError(long id) {
        return systemRepository.findById(id)
                .orElseThrow(() -> new NotFoundRuntimeException(format("System with id %s not found", id)));
    }

    private Entity findEntityByIdOrThrow(long id) {
        return entityRepository.findById(id)
                .orElseThrow(() -> new NotFoundRuntimeException(format("Entity with id %s not found.", id)));
    }

    private EntityHistory getFirstEntityHistoryEntryOrThrow(Entity entity) {
        EntityHistory firstEntityHistoryEntry = entityHistoryRepository.findFirstByEntityOrderByValidToStampAsc(entity);
        if (firstEntityHistoryEntry == null) {
            throw new NotFoundRuntimeException(
                    format("Entity history for entity with id %s not found.", entity.getId()));
        }
        return firstEntityHistoryEntry;
    }

    private EntityHistory getLatestEntityHistoryEntryOrThrow(Entity entity) {
        EntityHistory latestEntityHistoryEntry = entityHistoryRepository
                .findFirstByEntityOrderByValidToStampDesc(entity);
        if (latestEntityHistoryEntry == null) {
            throw new NotFoundRuntimeException(
                    format("Entity history for entity with id %s not found.", entity.getId()));
        }
        return latestEntityHistoryEntry;
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    @Override
    public Entity findOrCreateEntityFor(long systemId, long entityId, long partitionId, String recordFieldsWithTypes,
            long timestamp) {
        Entity entity = this.findEntityByIdOrThrow(entityId);
        try (AutoCloseableLock ignored = this.getLockFor(systemId + entity.getKeyValues())) {
            Partition partition = this.partitionPartitionByIdOrThrow(partitionId);
            checkSystem(systemId, entity, partition);
            Schema schema = getSchema(recordFieldsWithTypes, entity);
            return updateEntityHistoryFor(entity, partition, schema, timestamp);
        }
    }

    private void checkSystem(long systemId, Entity entity, Partition partition) {
        if (systemId != entity.getPartition().getSystem().getId() || systemId != partition.getSystem().getId()) {
            throw new IllegalArgumentException(
                    "System id " + systemId + " does not match system from entityId=" + entity.getId()
                            + " or system from partitionId=" + partition.getId());
        }
    }

    private Partition partitionPartitionByIdOrThrow(long partitionId) {
        return this.partitionRepository.findById(partitionId)
                .orElseThrow(() -> new IllegalArgumentException("No such partition with id=" + partitionId));
    }

    private Entity internalFindOrCreateEntityFor(System system, Map<String, Object> entityKeyValues,
            Map<String, Object> partitionKeyValues, String recordFieldsWithTypes, long recordTimestamp) {

        String entityKeyValuesString = convertMapIntoAvroSchemaString(entityKeyValues, system.getEntityKeyDefs());
        String partitionKeyValuesString = convertMapIntoAvroSchemaString(partitionKeyValues,
                system.getPartitionKeyDefs());

        try (AutoCloseableLock ignored = this.getLockFor(system.getId() + entityKeyValuesString)) {
            // find or create entity for the given system & entity key values.
            Entity entity = getEntity(system, entityKeyValuesString);
            Partition partition = getPartition(system, partitionKeyValuesString, entity);
            Schema schema = getSchema(recordFieldsWithTypes, entity);
            return updateEntityHistoryFor(entity, partition, schema, recordTimestamp);
        }
    }

    private Schema getSchema(String schemaAsString, Entity entity) {
        // find or create a schema for a given record content.
        String md5Hash = DigestUtils.md5Hex(schemaAsString);
        Schema schema;
        if (entity.getSchema() == null || !md5Hash.equals(entity.getSchema().getContentHash())) {
            schema = findOrCreateSchema(md5Hash, schemaAsString);
        } else {
            schema = entity.getSchema();
        }
        return schema;
    }

    private Partition getPartition(System system, String partitionKeyValues, Entity entity) {
        Partition partition;
        // find or create partition for a given system & partition key values
        if (entity.getPartition() == null || !partitionKeyValues.equals(entity.getPartition().getKeyValues())) {
            // This is different partition than on the entity, maybe changed partition
            partition = findOrCreatePartition(system, partitionKeyValues);
        } else {
            partition = entity.getPartition();
        }
        return partition;
    }

    private Entity getEntity(System system, String entityKeyValues) {
        return entityRepository.findByPartitionSystemIdAndKeyValues(system.getId(), entityKeyValues)
                .orElseGet(() -> createEntity(entityKeyValues));
    }

    private Entity extendEntityHistoryFor(Entity entity, Partition newPartition, Schema newSchema, long fromNanos,
            long toNanos) {
        EntityHistory entityHistory = new EntityHistory();
        entityHistory.setEntity(entity);
        entityHistory.setPartition(newPartition);
        entityHistory.setSchema(newSchema);
        entityHistory.setValidFromStamp(getInstantFromNanos(fromNanos));
        entityHistory.setValidToStamp(getInstantFromNanos(toNanos));
        entity.addEntityHist(entityHistoryRepository.save(entityHistory));
        return entity;
    }

    private Entity updateEntityHistoryFor(Entity entity, Partition newPartition, Schema newSchema,
            long recordTimestamp) {
        Instant recordTime = getInstantFromNanos(recordTimestamp);
        EntityHistory entityHistory;

        //new entity will not have the partition set
        if (entity.getPartition() == null) {
            entityHistory = handleNewEntity(entity, newPartition, newSchema, recordTime);
        } else {
            entityHistory = handleExistingEntity(entity, newPartition, newSchema, recordTime);
        }

        //Save the entity, make sure we return only one history item.
        return saveEntity(entity, entityHistory);

    }

    private EntityHistory handleExistingEntity(Entity entity, Partition newPartition, Schema newSchema,
            Instant recordTime) {
        //This is not a new entity so we search in the history
        Optional<EntityHistory> entityHistOptional = entityHistoryRepository
                .findByEntityAndTimestamp(entity, recordTime);
        return entityHistOptional
                .map(hist -> handleExistingEntityHist(entity, newPartition, newSchema, recordTime, hist))
                .orElseGet(() -> handleAbsentEntityHist(entity, newPartition, newSchema, recordTime));
    }

    private EntityHistory handleAbsentEntityHist(Entity entity, Partition newPartition, Schema newSchema,
            Instant recordTime) {
        //The recordTime must be before all known history, we have to add this to the begining of the history...
        //This can only work if we assume that the schema of this one is the same as the first entry in the history.
        //We cannot accept records which come with different schema than the one from the first entry.
        //The reasoning is that this would create an entry in the history linking the time of this record to the first known history which this new schema.
        //This is not necessarly good as the next bunch of records might come with yet different schema and get rejected.
        //The only good solution to this is to create a "good" history somehow by hand and than send data or send late forgotten data with the schema of the first history.
        EntityHistory entityHistory = getFirstEntityHistoryEntryOrThrow(entity);
        if (isEntityHistEqualToNewPartitionAndSchema(entityHistory, newPartition, newSchema)) {
            entityHistory.setValidFromStamp(recordTime);
            entityHistoryRepository.save(entityHistory);
        } else {
            log.warn("Late data ambiguity conflict for entity={} recordTimestamp={} historyId={}",
                    entity.getKeyValues(), recordTime, entityHistory.getId());
            throw new ConfigDataConflictException(formatMessage("We received late data before the known history with different schema or partition that was not used before, ", entity, recordTime, entityHistory));
        }
        return entityHistory;
    }

    private static String formatMessage(String prefix, Entity entity, Instant recordTime, EntityHistory entityHistory) {
        return String.format("%s entityId=%s recordTime=%s historyId=%s",prefix,entity.getId(), recordTime, entityHistory.getId());
    }

    private EntityHistory handleExistingEntityHist(Entity entity, Partition newPartition, Schema newSchema,
            Instant recordTime, EntityHistory entityHistory) {

        EntityHistory history = entityHistory;
        boolean historyMatchesNewRecord = isEntityHistEqualToNewPartitionAndSchema(history, newPartition,
                newSchema);

        if (history.getValidToStamp() == null) {
            //This is the latest history.

            if (!historyMatchesNewRecord && history.getValidFromStamp().equals(recordTime)) {
                log.warn("Data history conflict for entity={} recordTimestamp={} historyId={}",
                        entity.getKeyValues(), recordTime, history.getId());
                throw new ConfigDataConflictException(formatMessage( "We received late data with different schema or partition that was not used before, " ,entity, recordTime, history));
            }

            if (!historyMatchesNewRecord) {
                // This entity has different state than the last history so we must finish the open history record and create a new one.
                // We have to finish the last history state and open a new one, set the new partition & schema to the entity
                entity.setPartition(newPartition);
                entity.setSchema(newSchema);

                history.setValidToStamp(recordTime);
                entityHistoryRepository.save(history); //save the changed history record

                // Need to flush in order to preserve the order statements are executed in the DB
                // This is because JPA first executes the insert of the new history entry and next the update of the
                // last one.
                // In that case (temporary after the insert) we have two records with null validToStamp which triggers
                // the constraint.
                this.entityManager.flush();

                history = createAndPersistEntityHistory(entity, newPartition, newSchema, recordTime
                ); //this one adds this new history to entity.
            }
            //else nothing to do as the entity state matches the history, just add to the entity history

        } else if (!historyMatchesNewRecord) {
            //this is some old data with already closed history record.
            // Late data - the timestamp is in the past, before the last record

            // Late data that has different schema or partition for time period already in history - this is an
            // invalid case, we do not allow it.
            log.warn("Data history conflict for entity={} recordTimestamp={} historyId={}", entity.getKeyValues(),
                    recordTime, history.getId());
            throw new ConfigDataConflictException(formatMessage("We received late data with different schema or partition that was not used before, ",entity, recordTime, history));

        }

        return history;
    }

    private Entity saveEntity(Entity entity, EntityHistory entityHistory) {
        Entity newEntity = entityRepository.save(entity);
        newEntity.clearHistory();
        newEntity.addEntityHist(entityHistory);
        return newEntity;
    }

    private EntityHistory handleNewEntity(Entity entity, Partition newPartition, Schema newSchema, Instant recordTime) {
        entity.setPartition(newPartition);
        entity.setSchema(newSchema);
        return createAndPersistEntityHistory(entityRepository.save(entity), newPartition, newSchema,
                recordTime);
    }

    /**
     * Create new history record for a given entity with the corresponding validFromStamp and validToStamp timestamps
     */
    private EntityHistory createAndPersistEntityHistory(Entity entity, Partition partition, Schema schema,
            Instant validFromStamp) {
        EntityHistory newEntityHistory = new EntityHistory();
        newEntityHistory.setEntity(entity);
        newEntityHistory.setSchema(schema);
        newEntityHistory.setPartition(partition);
        newEntityHistory.setValidFromStamp(validFromStamp);
        newEntityHistory.setValidToStamp(null);
        return entityHistoryRepository.save(newEntityHistory);
    }

    private boolean isEntityHistEqualToNewPartitionAndSchema(EntityHistory lastHistory, Partition newPartition,
            Schema newSchema) {
        return lastHistory.getPartition().equals(newPartition) && lastHistory.getSchema().equals(newSchema);
    }

    private Schema findOrCreateSchema(String md5Hash, String schemaAsString) {
        try (AutoCloseableLock ignored = this.getLockFor(md5Hash)) {
            return schemaRepository.findByContentHash(md5Hash)
                    .orElseGet(() -> createAndPersistSchema(md5Hash, schemaAsString));
        }
    }

    private Partition findOrCreatePartition(System system, String partitionKeyValues) {
        try (AutoCloseableLock ignored = getLockFor(system.getId() + partitionKeyValues)) {
            return partitionRepository.findBySystemIdAndKeyValues(system.getId(), partitionKeyValues)
                    .orElseGet(() -> createAndPersistPartition(system, partitionKeyValues));
        }
    }

    private Entity createEntity(String entityKeyValues) {
        Entity entity = new Entity();
        entity.setKeyValues(entityKeyValues);
        return entity;
    }

    private Schema createAndPersistSchema(String md5Hash, String recordFieldsWithTypes) {
        Schema schema = new Schema();
        schema.setContent(recordFieldsWithTypes);
        schema.setContentHash(md5Hash);
        return schemaRepository.save(schema);
    }

    private Partition createAndPersistPartition(System system, String partitionKeyValues) {
        Partition partition = new Partition();
        partition.setKeyValues(partitionKeyValues);
        partition.setSystem(system);
        return partitionRepository.save(partition);
    }
}
