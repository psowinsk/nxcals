/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.internal;

import cern.nxcals.common.domain.VariableConfigData;
import cern.nxcals.common.domain.VariableData;
import cern.nxcals.common.utils.TimeUtils;
import cern.nxcals.service.domain.Entity;
import cern.nxcals.service.domain.Variable;
import cern.nxcals.service.domain.VariableConfig;
import cern.nxcals.service.repository.EntityRepository;
import cern.nxcals.service.repository.VariableConfigRepository;
import cern.nxcals.service.repository.VariableRepository;
import cern.nxcals.service.rest.NotFoundRuntimeException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.constraints.NotEmpty;
import java.time.Instant;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.stream.Collectors;

@Service
@Slf4j
public class InternalVariableServiceImpl implements InternalVariableService {

    @Autowired
    private VariableRepository variableRepository;
    @Autowired
    private VariableConfigRepository variableConfigRepository;
    @Autowired
    private EntityRepository entityRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Variable findByVariableName(String variableName) {
        Variable variable = variableRepository.findByVariableName(variableName)
                .orElseThrow(() -> new NotFoundRuntimeException("No variable found with name = " + variableName));
        SortedSet<VariableConfig> varConfigs = variableConfigRepository
                .findByVariableIdOrderByValidFromStampDesc(variable.getId());
        variable.setVariableConfigs(varConfigs);
        return variable;
    }

    @Override
    public Variable findByVariableNameAndTimeWindow(String variableName, long startTime, long endTime) {
        Variable variable = variableRepository.findByVariableName(variableName)
                .orElseThrow(() -> new NotFoundRuntimeException("No variable found with name = " + variableName));
        SortedSet<VariableConfig> varConfigs = variableConfigRepository.findByVariableIdAndTimeWindow(variable,
                TimeUtils.getInstantFromNanos(startTime), TimeUtils.getInstantFromNanos(endTime));
        variable.setVariableConfigs(varConfigs);
        return variable;
    }

    @Override
    public List<Variable> findByNameLike(@NotEmpty String nameExpression) {
        List<Variable> variablesFound = variableRepository.findByVariableNameLike(nameExpression);
        appendVariablesConfigs(variablesFound);
        return variablesFound;
    }

    @Override
    public List<Variable> findByDescriptionLike(@NotEmpty String descriptionExpression) {
        List<Variable> variablesFound = variableRepository.findByDescriptionLike(descriptionExpression);
        appendVariablesConfigs(variablesFound);
        return variablesFound;
    }

    private void appendVariablesConfigs(List<Variable> variablesFound) {
        for (Variable variable : variablesFound) {
            SortedSet<VariableConfig> varConfigs = variableConfigRepository
                    .findByVariableIdOrderByValidFromStampDesc(variable.getId());
            variable.setVariableConfigs(varConfigs);
        }
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    @Override
    public Variable registerOrUpdateVariableFor(VariableData variableData) {
        log.debug("registerOrUpdateVariableFor: " + variableData);
        Variable variable = variableRepository.findByVariableName(variableData.getVariableName()).orElseGet(() ->
                registerNewVariable(variableData));
        return updateVariableConfig(variable, variableData);
    }

    private Variable updateVariableConfig(Variable variable, VariableData variableData) {
        if (variableData.getVariableConfigData() == null || variableData.getVariableConfigData().isEmpty())
            throw new IllegalArgumentException("Variable cannot be reistered/updated with empty configuration !");

        if (!validateConfigData(variableData.getVariableConfigData()))
            throw new IllegalArgumentException(
                    "Variable configuration data is not valid! There are uncovered time windows :"
                            + variableData.getVariableConfigData().toString());

        removeOldVariableConfigs(variable);
        variableData.getVariableConfigData().stream()
                .map(c -> createVariableConfig(c, variable)).sorted(VariableConfig::compareTo)
                .forEach(variable::addConfig);
        return variableRepository.save(variable);
    }

    private Variable registerNewVariable(VariableData variableData) {
        Variable variable = new Variable();
        variable.setVariableName(variableData.getVariableName());
        variable.setDescription(variableData.getDescription());
        variable.setCreationTimeUtc(Instant.ofEpochMilli(System.currentTimeMillis()));
        return variableRepository.save(variable);
    }

    private VariableConfig createVariableConfig(VariableConfigData variableConfigData, Variable variable) {
        Entity entity = entityRepository.findById(variableConfigData.getEntityId())
                .orElseThrow(() -> new IllegalArgumentException(
                        "There is no existing entity with entityId=" + variableConfigData.getEntityId()));

        return createNewConfigRecord(variable, entity, variableConfigData.getFieldName(),
                variableConfigData.getValidFromStamp(),
                variableConfigData.getValidToStamp());
    }

    private boolean validateConfigData(Set<VariableConfigData> varConfs) {
        List<Long> sortedTimeStamps = varConfs.stream()
                .sorted(Comparator.comparing(
                        rec -> (rec.getValidFromStamp() == null ? Long.valueOf(0) : rec.getValidFromStamp())))
                .map(rec -> new Long[] { rec.getValidFromStamp(), rec.getValidToStamp() }).flatMap(Arrays::stream)
                .collect(Collectors.toList());

        return firstAndLastTimeStampsAreBothNull(sortedTimeStamps) && allTimeStampsAreInPairs(sortedTimeStamps)
                && timeStampsDecrease(sortedTimeStamps);
    }

    private boolean firstAndLastTimeStampsAreBothNull(List<Long> sortedTimeStamps) {
        return sortedTimeStamps.get(0) == null && sortedTimeStamps.get(sortedTimeStamps.size() - 1) == null;
    }

    private boolean timeStampsDecrease(List<Long> sortedTimeStamps) {
        // check whether the timestamps in the next configuration decrease and has later time/date
        for (int i = 1; i < sortedTimeStamps.size() - 2; i++) {
            if (sortedTimeStamps.get(i) > sortedTimeStamps.get(i + 1))
                return false;
        }
        return true;
    }

    /**
     * This is checking if the space of timestamp is continuous.
     */
    private boolean allTimeStampsAreInPairs(List<Long> sortedTimeStamps) {
        // checks whether the next timestamp is equal as we should have equal pairs after flatting them in a sorted list
        for (int i = 1; i < sortedTimeStamps.size() - 1; i += 2) {
            if (!sortedTimeStamps.get(i).equals(sortedTimeStamps.get(i + 1))) {
                return false;
            }
        }
        return true;
    }

    private VariableConfig createNewConfigRecord(Variable variable, Entity entity, String fieldName,
            Long validFromStamp, Long validToStamp) {
        VariableConfig variableConfig = new VariableConfig();
        variableConfig.setVariable(variable);
        variableConfig.setEntity(entity);
        variableConfig.setFieldName(fieldName);
        variableConfig.setValidFromStamp(validFromStamp == null ? null : TimeUtils.getInstantFromNanos(validFromStamp));
        variableConfig.setValidToStamp(validToStamp == null ? null : TimeUtils.getInstantFromNanos(validToStamp));
        return variableConfigRepository.save(variableConfig);
    }

    private void removeOldVariableConfigs(Variable variable) {
        List<VariableConfig> removedConfigs = variableConfigRepository.removeByVariable(variable);
        removedConfigs.forEach(variable::removeConfig);

        log.debug(removedConfigs.stream().map(l -> l.getFieldName() + " from:" + l.getValidFromStamp() +
                " to:" + l.getValidToStamp()).collect(Collectors.toList()).toString());
        // Need to flush in order to preserve the order of executing the statements to the DB
        // 1. remove existing configuration
        // 2. create new variables configuration
        variableRepository.save(variable);
        entityManager.flush();
    }
}
