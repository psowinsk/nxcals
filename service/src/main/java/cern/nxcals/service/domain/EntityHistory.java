/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.domain;

import cern.nxcals.common.domain.EntityHistoryData;
import cern.nxcals.common.utils.TimeUtils;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Comparator;

import static cern.nxcals.service.domain.SequenceType.ENTITY_HIST;

/**
 * @author ntsvetko
 * @author jwozniak
 */
@javax.persistence.Entity
@Table(name = "ENTITIES_HIST")
@ToString
@SuppressWarnings("squid:S2160") //override equals while we do it in the base class - jwozniak
public class EntityHistory extends StandardPersistentEntityWithVersion implements Comparable<EntityHistory> {

    private static final long serialVersionUID = -4406604195344770819L;

    @NotNull
    private Partition partition;
    private System system;
    @NotNull
    private transient Instant validFromStamp;
    private transient Instant validToStamp;

    @NotNull
    private Schema schema;

    @NotNull
    private Entity entity;

    @Id
    @Column(name = "ENTITY_HIST_ID")
    @Override
    public Long getId() {
        return super.getId();
    }

    @Override
    protected SequenceType sequenceType() {
        return ENTITY_HIST;
    }

    @ManyToOne
    @JoinColumn(name = "partition_id")
    public Partition getPartition() {
        return this.partition;
    }

    public void setPartition(Partition value) {
        this.partition = value;
    }

    public Instant getValidFromStamp() {
        return validFromStamp;
    }

    public void setValidFromStamp(Instant validFromStamp) {
        this.validFromStamp = validFromStamp;
    }

    public Instant getValidToStamp() {
        return validToStamp;
    }

    public void setValidToStamp(Instant validToStamp) {
        this.validToStamp = validToStamp;
    }

    @ManyToOne
    @JoinColumn(name = "entity_id")
    public Entity getEntity() {
        return this.entity;
    }

    public void setEntity(Entity value) {
        this.entity = value;
    }

    @ManyToOne
    @JoinColumn(name = "schema_id")
    public Schema getSchema() {
        return this.schema;
    }

    public void setSchema(Schema calsSchema) {
        this.schema = calsSchema;
    }

    @ManyToOne
    @JoinColumn(name = "system_id")
    public System getSystem() {
        if (this.partition != null) {
            return this.partition.getSystem();
        }
        return null;
    }

    public void setSystem(System system) {
        // set through entity
    }

    /**
     * Remarks (msobiesz): the same as in the similar method in Entity
     */
    public EntityHistoryData toEntityHistData() {
        return EntityHistoryData.builder()
                .id(this.getId())
                .schemaData(this.schema.toSchemaData())
                .partitionData(this.partition.toPartitionData())
                .validFromStamp(TimeUtils.getNanosFromInstant(this.validFromStamp))
                .validToStamp(this.validToStamp == null ? null : TimeUtils.getNanosFromInstant(this.validToStamp))
                .build();
    }

    @Override
    @SuppressWarnings("squid:S1210") // override equals to comply with compareTo contract (we do it in base class)
    public int compareTo(EntityHistory otherHistory) {
        return Comparator.<Instant>reverseOrder().compare(this.getValidFromStamp(), otherHistory.getValidFromStamp());
    }
}
