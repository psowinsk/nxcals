/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.domain;

import cern.nxcals.common.domain.VariableConfigData;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Comparator;

import static cern.nxcals.common.utils.TimeUtils.getNanosFromInstant;

/**
 * Created by ntsvetko on 1/12/17.
 */
@javax.persistence.Entity
@Table(name = "VARIABLE_CONFIGS")
@SuppressWarnings("squid:S2160") //override equals while we do it in the base class - jwozniak
public class VariableConfig extends StandardPersistentEntityWithVersion implements Comparable<VariableConfig> {

    private static final long serialVersionUID = 3665546057719893699L;

    @NotNull
    private Variable variable;

    @NotNull
    private Entity entity;

    private String fieldName;

    private transient Instant validFromStamp;
    private transient Instant validToStamp;

    @Id
    @Column(name = "VARIABLE_CONFIG_ID")
    @Override
    public Long getId() {
        return super.getId();
    }

    @ManyToOne
    @JoinColumn(name = "variable_id")
    public Variable getVariable() {
        return variable;
    }

    public void setVariable(Variable variable) {
        this.variable = variable;
    }

    @ManyToOne
    @JoinColumn(name = "entity_id")
    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public Instant getValidFromStamp() {
        return validFromStamp;
    }

    public void setValidFromStamp(Instant validFromStamp) {
        this.validFromStamp = validFromStamp;
    }

    public Instant getValidToStamp() {
        return validToStamp;
    }

    public void setValidToStamp(Instant validToStamp) {
        this.validToStamp = validToStamp;
    }

    public VariableConfigData toVariableConfigData() {
        return VariableConfigData.builder()
                .entityId(this.getEntity().getId())
                .fieldName(this.getFieldName())
                .validFromStamp(this.getValidFromStamp() == null ? null : getNanosFromInstant(this.getValidFromStamp()))
                .validToStamp(this.getValidToStamp() == null ? null : getNanosFromInstant(this.getValidToStamp()))
                .build();
    }

    @SuppressWarnings("squid:S1210") // override equals to comply with compareTo contract (we do it in base class)
    @Override
    public int compareTo(VariableConfig o) {
        return Comparator.<Instant>reverseOrder()
                .compare(this.validFromStamp == null ? Instant.EPOCH : this.validFromStamp,
                        o.validFromStamp == null ? Instant.EPOCH : o.validFromStamp);
    }
}
