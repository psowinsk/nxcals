package cern.nxcals.service.domain.security;

import cern.nxcals.service.domain.SequenceType;
import cern.nxcals.service.domain.StandardPersistentEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.CascadeType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * Created by wjurasz on 10.08.17.
 */
@javax.persistence.Entity
@Table(name = "ROLES")
@Immutable
@SuppressWarnings("squid:S2160") //override equals while we do it in the base class - jwozniak
public class Role extends StandardPersistentEntity {
    private static final long serialVersionUID = -4793867515072384266L;

    @NotNull
    @Immutable
    private String roleName;

    @Immutable
    private Set<User> users;

    @Immutable
    private Set<Permission> permissions;

    @Id
    @Override
    public Long getId() {
        return super.getId();
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @ManyToMany(mappedBy = "roles")
    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    //Cascade necessary for tests.
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            inverseJoinColumns = @JoinColumn(name = "PERMISSION_ID", referencedColumnName = "ID"),
            joinColumns = @JoinColumn(name = "ROLE_ID", referencedColumnName = "ID")
    )
    public Set<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<Permission> permissions) {
        this.permissions = permissions;
    }

    @Override
    protected SequenceType sequenceType() {
        return SequenceType.ROLE;
    }
}
