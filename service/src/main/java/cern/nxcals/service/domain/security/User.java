package cern.nxcals.service.domain.security;

import cern.nxcals.service.domain.SequenceType;
import cern.nxcals.service.domain.StandardPersistentEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.CascadeType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * Created by wjurasz on 10.08.17.
 */
@javax.persistence.Entity
@Table(name = "USERS")
@Immutable
@SuppressWarnings("squid:S2160") //override equals while we do it in the base class - jwozniak
public class User extends StandardPersistentEntity {
    private static final long serialVersionUID = -8151614125621402723L;

    @NotNull
    @Immutable
    private String userName;

    @Immutable
    private Set<Role> roles;

    @Immutable
    private Realm realm;

    @Id
    @Override
    public Long getId() {
        return super.getId();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    //Cascade necessary for tests.
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(inverseJoinColumns = @JoinColumn(name = "ROLE_ID", referencedColumnName = "ID"),
            joinColumns = @JoinColumn(name = "USER_ID", referencedColumnName = "ID"))
    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    //Cascade necessary for tests.
    @ManyToOne(cascade = CascadeType.ALL)
    public Realm getRealm() {
        return realm;
    }

    public void setRealm(Realm realm) {
        this.realm = realm;
    }

    @Override
    protected SequenceType sequenceType() {
        return SequenceType.USER;
    }
}



