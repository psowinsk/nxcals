package cern.nxcals.service.domain.security;

import cern.nxcals.service.domain.SequenceType;
import cern.nxcals.service.domain.StandardPersistentEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * Created by wjurasz on 10.10.17.
 */
@javax.persistence.Entity
@Table(name = "REALMS")
@Immutable
@SuppressWarnings("squid:S2160") //override equals while we do it in the base class - jwozniak
public class Realm extends StandardPersistentEntity {

    private static final long serialVersionUID = -3585589675913286999L;

    @NotNull
    @Immutable
    private String realmName;

    @Immutable
    private Set<User> users;

    @Id
    @Override
    public Long getId() {
        return super.getId();
    }

    public String getRealmName() {
        return realmName;
    }

    public void setRealmName(String realmName) {
        this.realmName = realmName;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    @OneToMany(mappedBy = "realm")
    public Set<User> getUsers() {
        return users;
    }

    @Override
    protected SequenceType sequenceType() {
        return SequenceType.REALM;
    }

}
