/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.domain;

import cern.nxcals.common.domain.PartitionData;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

import static cern.nxcals.common.utils.KeyValuesUtils.convertKeyValuesStringIntoMap;
import static cern.nxcals.service.domain.SequenceType.PARTITION;

@javax.persistence.Entity
@Table(name = "PARTITIONS")
/**
 * @author ntsvetko
 * @author jwozniak
 */
@SuppressWarnings("squid:S2160") //override equals while we do it in the base class - jwozniak
public class Partition extends StandardPersistentEntityWithVersion {

    private static final long serialVersionUID = 6293229615370043778L;

    @NotNull
    private String keyValues;

    @NotNull
    private System system;
    private Set<Entity> entities;

    @Id
    @Column(name = "PARTITION_ID")
    @Override
    public Long getId() {
        return super.getId();
    }

    @Override
    protected SequenceType sequenceType() {
        return PARTITION;
    }

    public String getKeyValues() {
        return this.keyValues;
    }

    public void setKeyValues(String value) {
        this.keyValues = value;
    }

    @OneToMany(mappedBy = "partition")
    public Set<Entity> getEntities() {
        return this.entities;
    }

    public void setEntities(Set<Entity> entityKeys) {
        this.entities = entityKeys;
    }

    @ManyToOne
    @JoinColumn(name = "system_id")
    public System getSystem() {
        return this.system;
    }

    public void setSystem(System clientSystem) {
        this.system = clientSystem;
    }

    public PartitionData toPartitionData() {
        return PartitionData.builder()
                .id(this.getId())
                .keyValues(convertKeyValuesStringIntoMap(keyValues))
                .build();
    }

    public boolean addEntity(Entity e) {
        if (this.entities == null) {
            this.entities = new HashSet<>();
        }
        e.setPartition(this);
        return this.entities.add(e);
    }

    public boolean removeEntity(Entity e) {
        e.setPartition(null);
        if (this.entities == null) {
            return false;
        }
        return this.entities.remove(e);
    }

}
