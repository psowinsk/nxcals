/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.domain;

import cern.nxcals.common.domain.EntityData;
import cern.nxcals.common.domain.EntityHistoryData;
import cern.nxcals.common.utils.TimeUtils;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Collections;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static cern.nxcals.common.utils.KeyValuesUtils.convertKeyValuesStringIntoMap;
import static cern.nxcals.service.domain.SequenceType.ENTITY;

@javax.persistence.Entity
@Table(name = "ENTITIES")
@SuppressWarnings("squid:S2160") //override equals while we do it in the base class - jwozniak
public class Entity extends StandardPersistentEntityWithVersion {

    private static final long serialVersionUID = -6802891913563005210L;

    @NotNull
    private String keyValues;

    @NotNull
    private Partition partition;

    @NotNull
    private Schema schema;

    private SortedSet<EntityHistory> entityHistories;

    private transient Instant lockedUntilStamp;

    @Id
    @Column(name = "ENTITY_ID")
    @Override
    public Long getId() {
        return super.getId();
    }

    @Override
    protected SequenceType sequenceType() {
        return ENTITY;
    }

    public String getKeyValues() {
        return this.keyValues;
    }

    public void setKeyValues(String values) {
        this.keyValues = values;
    }

    @ManyToOne
    @JoinColumns({ @JoinColumn(name = "system_id", referencedColumnName = "system_id"),
            @JoinColumn(name = "partition_id", referencedColumnName = "partition_id") })
    public Partition getPartition() {
        return this.partition;
    }

    public void setPartition(Partition value) {
        if (value == null) {
            throw new IllegalArgumentException("Partition cannot be null");
        }
        this.partition = value;
    }

    @ManyToOne
    @JoinColumn(name = "schema_id")
    public Schema getSchema() {
        return this.schema;
    }

    public void setSchema(Schema value) {
        this.schema = value;
    }

    @Transient
    public SortedSet<EntityHistory> getEntityHistories() {
        return this.entityHistories;
    }

    public void setEntityHistories(SortedSet<EntityHistory> values) {
        this.entityHistories = values;
    }

    public boolean addEntityHist(EntityHistory entityHistory) {
        if (this.entityHistories == null) {
            this.entityHistories = new TreeSet<>();
        }
        entityHistory.setEntity(this);
        return this.entityHistories.add(entityHistory);
    }

    public boolean removeEntityHist(EntityHistory entityHistory) {
        entityHistory.setEntity(null);
        return this.entityHistories != null && this.entityHistories.remove(entityHistory);
    }

    public EntityData toEntityData() {
        return EntityData.builder(getId(), convertKeyValuesStringIntoMap(keyValues),
                getPartition().getSystem() != null ? getPartition().getSystem().toSystemData() : null,
                partition != null ? partition.toPartitionData() : null,
                schema != null ? schema.toSchemaData() : null,
                createEntityHistoryData(this),
                lockedUntilStamp != null ? TimeUtils.getNanosFromInstant(lockedUntilStamp) : null,
                getRecVersion())
                .build();
    }

    public EntityData toEntityDataWithHistory(SortedSet<EntityHistoryData> entityHistoryData) {
        return EntityData.builder(getId(), convertKeyValuesStringIntoMap(keyValues),
                getPartition().getSystem() != null ? getPartition().getSystem().toSystemData() : null,
                partition != null ? partition.toPartitionData() : null,
                schema != null ? schema.toSchemaData() : null, entityHistoryData,
                lockedUntilStamp != null ? TimeUtils.getNanosFromInstant(lockedUntilStamp) : null,
                getRecVersion()).build();
    }

    private static SortedSet<EntityHistoryData> createEntityHistoryData(Entity entity) {
        SortedSet<EntityHistory> entityHistories = entity.getEntityHistories();
        if (entityHistories == null) {
            return Collections.emptySortedSet();
        }

        return entityHistories.stream()
                .map(EntityHistory::toEntityHistData)
                .collect(Collectors.toCollection(TreeSet::new));
    }

    public void clearHistory() {
        if (this.entityHistories != null) {
            this.entityHistories.clear();
        }
    }

    public Instant getLockedUntilStamp() {
        return lockedUntilStamp;
    }

    public void setLockedUntilStamp(Instant lockedUntilStamp) {
        this.lockedUntilStamp = lockedUntilStamp;
    }

    @Override
    public String toString() {
        return "Entity{" +
                "id=" + getId() +
                ", keyValues='" + keyValues + '\'' +
                ", partition=" + partition +
                ", schema=" + schema +
                ", lockedUntilStamp=" + lockedUntilStamp +
                ", recVersion=" + getRecVersion() +
                '}';
    }
}
