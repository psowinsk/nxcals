/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.domain;

/**
 * @author Marcin Sobieszek
 * @date Jul 25, 2016 3:50:22 PM
 */
public enum SequenceType {
    SCHEMA("schema_seq"),
    ENTITY("entity_seq"),
    ENTITY_HIST("entity_hist_seq"),
    PARTITION("partition_seq"),
    SYSTEM("system_seq"),
    VARIABLE("variable_seq"),
    DEFAULT("default_seq"),
    PERMISSION("permission_seq"),
    ROLE("role_seq"),
    USER("user_seq"),
    REALM("realm_seq");
    private final String seqName;

    private SequenceType(String sName) {
        this.seqName = sName;
    }

    public String getSeqName() {
        return this.seqName;
    }

}
