package cern.nxcals.service.domain.security;

import cern.nxcals.service.domain.SequenceType;
import cern.nxcals.service.domain.StandardPersistentEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * Created by wjurasz on 10.08.17.
 */
@javax.persistence.Entity
@Table(name = "PERMISSIONS")
@Immutable
@SuppressWarnings("squid:S2160") //override equals while we do it in the base class - jwozniak
public class Permission extends StandardPersistentEntity {
    private static final long serialVersionUID = 1456864434602355259L;

    @NotNull
    @Immutable
    private String permissionName;

    @Immutable
    private Set<Role> roles;

    @Id
    @Override
    public Long getId() {
        return super.getId();
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    @ManyToMany(mappedBy = "permissions")
    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    @Override
    protected SequenceType sequenceType() {
        return SequenceType.PERMISSION;
    }
}
