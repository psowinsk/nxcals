/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.domain;

import cern.nxcals.common.domain.SchemaData;
import cern.nxcals.service.exception.SchemaOverrideException;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import static cern.nxcals.service.domain.SequenceType.SCHEMA;

/**
 * @author ntsvetko
 */
@javax.persistence.Entity
@Table(name = "SCHEMAS")
@SuppressWarnings("squid:S2160") //override equals while we do it in the base class - jwozniak
public class Schema extends StandardPersistentEntityWithVersion {

    private static final long serialVersionUID = 39304950565223841L;
    private String content;
    private String contentHash;

    @Id
    @Column(name = "SCHEMA_ID")
    @Override
    public Long getId() {
        return super.getId();
    }

    @Override
    protected SequenceType sequenceType() {
        return SCHEMA;
    }

    @NotNull
    @Column(name = "SCHEMA_CONTENT", columnDefinition = "CLOB", nullable = false)
    public String getContent() {
        return this.content;
    }

    public void setContent(String value) {
        if (this.content == null) {
            this.content = value;
        } else if (!this.content.equals(value)) {
            throw new SchemaOverrideException("Cannot change the schema content for schema " + this.getId());
        }
    }

    @NotNull
    @Column(name = "SCHEMA_CONTENT_HASH", nullable = false)
    public String getContentHash() {
        return this.contentHash;
    }

    public void setContentHash(String hash) {
        if (this.contentHash == null) {
            this.contentHash = hash;
        } else if (!this.contentHash.equals(hash)) {
            throw new SchemaOverrideException("Cannot change the schema content hash for schema " + this.getId());
        }
    }

    /**
     * Converts to DAO representation of Schema
     *
     * @return DAO object
     */
    public SchemaData toSchemaData() {
        return SchemaData.builder()
                .id(this.getId())
                .schema(this.getContent())
                .build();
    }
}
