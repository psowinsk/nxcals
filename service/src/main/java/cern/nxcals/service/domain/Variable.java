/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.domain;

import cern.nxcals.common.domain.VariableConfigData;
import cern.nxcals.common.domain.VariableData;
import cern.nxcals.common.utils.TimeUtils;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Created by ntsvetko on 1/12/17.
 */
@javax.persistence.Entity
@Table(name = "VARIABLES")
@SuppressWarnings("squid:S2160") //override equals while we do it in the base class - jwozniak
public class Variable extends StandardPersistentEntityWithVersion {

    private static final long serialVersionUID = -8581950832501916844L;

    @NotNull
    private String variableName;
    private String description;

    @NotNull
    private transient Instant creationTimeUtc;

    private SortedSet<VariableConfig> variableConfigs;

    public Variable() {
    }

    public Variable(String variableName, String description, Instant creationTimeUtc) {
        this.variableName = variableName;
        this.description = description;
        this.creationTimeUtc = creationTimeUtc;
    }

    public Variable(String variableName, String description, Instant creationTimeUtc,
            SortedSet<VariableConfig> variableConfigs) {
        this.variableName = variableName;
        this.description = description;
        this.creationTimeUtc = creationTimeUtc;
        this.variableConfigs = variableConfigs;
    }

    @Id
    @Column(name = "VARIABLE_ID")
    @Override
    public Long getId() {
        return super.getId();
    }

    public String getVariableName() {
        return variableName;
    }

    public void setVariableName(String variableName) {
        this.variableName = variableName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Instant getCreationTimeUtc() {
        return creationTimeUtc;
    }

    public void setCreationTimeUtc(Instant creationTimeUtc) {
        this.creationTimeUtc = creationTimeUtc;
    }

    //    @OneToMany(mappedBy = "variable", cascade = {CascadeType.MERGE,CascadeType.PERSIST})

    @Transient
    public SortedSet<VariableConfig> getVariableConfigs() {
        return variableConfigs;
    }

    public void setVariableConfigs(SortedSet<VariableConfig> variableConfigs) {
        this.variableConfigs = variableConfigs;
    }

    public void addConfig(VariableConfig variableConfig) {
        if (this.variableConfigs == null) {
            this.variableConfigs = new TreeSet<>();
        }
        variableConfig.setVariable(this);
        this.variableConfigs.add(variableConfig);
    }

    public void removeConfig(VariableConfig variableConfig) {
        variableConfig.setVariable(null);
        if (this.variableConfigs != null) {
            this.variableConfigs.remove(variableConfig);
        }
    }

    public VariableData toVariableData() {
        return VariableData.builder()
                .name(this.getVariableName())
                .description(this.getDescription())
                .creationTimeUtc(TimeUtils.getNanosFromInstant(this.getCreationTimeUtc()))
                .variableConfigData(this.getVariableConfigData(this))
                .build();
    }

    private SortedSet<VariableConfigData> getVariableConfigData(Variable variable) {
        SortedSet<VariableConfigData> varConfData = new TreeSet<>();
        for (VariableConfig conf : variable.getVariableConfigs()) {
            varConfData.add(conf.toVariableConfigData());
        }
        return varConfData;
    }
}
