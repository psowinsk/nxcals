/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.domain;

import java.io.Serializable;

/**
 * Base class for all persistent objects. It is considered to be the root of all persistent objects.
 * On this level of abstraction we force every persistent object to have an identity.
 *
 * @author Marcin Sobieszek
 * @date Jul 19, 2016 10:22:55 AM
 */
public interface PersistentEntity extends Serializable {

    Serializable getId();
}
