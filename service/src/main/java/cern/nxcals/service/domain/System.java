/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.domain;

import cern.nxcals.common.domain.SystemData;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

import static cern.nxcals.service.domain.SequenceType.SYSTEM;

@javax.persistence.Entity
@Table(name = "SYSTEMS")
/**
 * Remarks (msobiesz):
 * <ul>
 * <li>same comment for property one2many relations handling as in Entity</li>
 * <li>maybe we could call it differently as it colides with java.lang.System</li>
 * </ul>
 */
@SuppressWarnings("squid:S2160") //override equals while we do it in the base class - jwozniak
public class System extends StandardPersistentEntityWithVersion {

    private static final long serialVersionUID = 1205018780230518529L;

    @NotNull
    private String name;

    @NotNull
    private String entityKeyDefs;

    @NotNull
    private String partitionKeyDefs;

    @NotNull
    private String timeKeyDefs;

    private String recordVersionKeyDefs;

    private Set<Partition> partitions;

    @Id
    @Column(name = "SYSTEM_ID")
    @Override
    public Long getId() {
        return super.getId();
    }

    @Override
    protected SequenceType sequenceType() {
        return SYSTEM;
    }

    @Column(name = "SYSTEM_NAME")
    public String getName() {
        return this.name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getEntityKeyDefs() {
        return this.entityKeyDefs;
    }

    @Column(nullable = false)
    public void setEntityKeyDefs(String value) {
        this.entityKeyDefs = value;
    }

    public String getPartitionKeyDefs() {
        return this.partitionKeyDefs;
    }

    public void setPartitionKeyDefs(String value) {
        this.partitionKeyDefs = value;
    }

    public String getTimeKeyDefs() {
        return this.timeKeyDefs;
    }

    public void setTimeKeyDefs(String value) {
        this.timeKeyDefs = value;
    }

    public String getRecordVersionKeyDefs() {
        return this.recordVersionKeyDefs;
    }

    public void setRecordVersionKeyDefs(String value) {
        this.recordVersionKeyDefs = value;
    }

    @OneToMany(mappedBy = "system")
    public Set<Partition> getPartitions() {
        return this.partitions;
    }

    public void setPartitions(Set<Partition> value) {
        this.partitions = value;
    }

    public boolean addPartition(Partition p) {
        if (this.partitions == null) {
            this.partitions = new HashSet<>();
        }
        p.setSystem(this);
        return this.partitions.add(p);
    }

    public boolean removePartition(Partition p) {
        p.setSystem(null);
        if (this.partitions == null) {
            return false;
        }
        return this.partitions.remove(p);
    }

    /**
     * Converts to DAO representation of System
     *
     * @return DAO object
     */

    public SystemData toSystemData() {
        return SystemData.builder()
                .id(this.getId())
                .name(this.getName())
                .entityKeyDefinitions(this.getEntityKeyDefs())
                .partitionKeyDefinitions(this.getPartitionKeyDefs())
                .timeKeyDefinitions(this.getTimeKeyDefs())
                .recordVersionKeyDefinitions(this.getRecordVersionKeyDefs())
                .build();
    }

}
