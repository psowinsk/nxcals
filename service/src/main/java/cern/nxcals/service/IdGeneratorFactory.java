/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service;

import cern.nxcals.service.internal.IdGenerator;
import org.springframework.context.ApplicationContext;

/**
 * @author Marcin Sobieszek
 * @date Jul 26, 2016 12:08:49 PM
 */
public class IdGeneratorFactory {
    private static ApplicationContext context;
    private static IdGenerator generator;

    private IdGeneratorFactory() {
        //private, cannot instantiate
    }

    static void setContext(ApplicationContext ctx) {
        context = ctx;
    }

    public static IdGenerator getGenerator() {
        if (generator == null) {
            generator = context.getBean(IdGenerator.class);
        }
        return generator;
    }

}
