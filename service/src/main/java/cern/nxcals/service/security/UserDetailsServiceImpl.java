package cern.nxcals.service.security;

import cern.nxcals.service.domain.security.User;
import cern.nxcals.service.repository.security.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.stream.Collectors;

/**
 * Nxcals specific implementation of {@link UserDetailsService}
 * {@inheritDoc}
 *
 * @author wjurasz
 */
@Slf4j
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String kerberosUsername)  {
        if (StringUtils.isEmpty(kerberosUsername)) {
            logAndThrowUserNotFoundException("Username has to be provided in order to proceed with authorization", kerberosUsername);
        }

        int lastAtIndex = kerberosUsername.lastIndexOf('@');

        if (lastAtIndex == -1) {
            logAndThrowUserNotFoundException("Invalid username. Kerberos realm have to be provided.", kerberosUsername);
        }
        String username = kerberosUsername.substring(0, lastAtIndex);
        String realm = kerberosUsername.substring(lastAtIndex + 1);

        User user = userRepository.findUserWithRealmWithRolesWithPermissions(username, realm);
        if (user == null) {
            logAndThrowUserNotFoundException("Not such user is registered", kerberosUsername);
        }


        return UserDetailsImpl.builder(kerberosUsername).authorities(user.getRoles()
                    .stream()
                    .flatMap(role -> role.getPermissions().stream())
                    .map(permission -> new SimpleGrantedAuthority(permission.getPermissionName()))
                    .collect(Collectors.toList()))
                    .build();

    }

    private static void logAndThrowUserNotFoundException(String message, String userName){
        log.error("Unsuccessful authentication from user: {}. Reason: {} ", userName, message);
        throw new UsernameNotFoundException(message);
    }
}
