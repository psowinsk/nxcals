/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.security.resolvers;

import org.springframework.stereotype.Component;

@Component
public class ConcatenatingPermissionResolver implements PermissionResolver {
    @Override
    public String getPermissionString(String permissionArea, String permissionType) {
        return permissionArea + ":" + permissionType;
    }
}
