package cern.nxcals.service.security;

import com.google.common.collect.ImmutableSortedSet;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;
import org.apache.commons.lang.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityCoreVersion;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Created by wjurasz on 03.11.17.
 */
@ToString
@EqualsAndHashCode(of = "username")
public class UserDetailsImpl implements UserDetails {

    private static final long serialVersionUID = -4806613463844346321L;

    private static final AuthorityComparator AUTHORITY_COMPARATOR = new AuthorityComparator();

    private final String username;
    private final ImmutableSortedSet<GrantedAuthority> authorities;
    private final boolean accountNonExpired;
    private final boolean accountNonLocked;
    private final boolean enabled;

    private UserDetailsImpl(@NonNull String username,
            @NonNull Collection<? extends GrantedAuthority> authorities,
            boolean accountNonExpired,
            boolean accountNonLocked,
            boolean enabled) {
        if (StringUtils.isEmpty(username)) {
            throw new IllegalArgumentException(
                    "Username has to be provided");
        }
        this.username = username;
        this.authorities = createSortedSet(authorities);
        this.accountNonExpired = accountNonExpired;
        this.accountNonLocked = accountNonLocked;
        this.enabled = enabled;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    private static ImmutableSortedSet<GrantedAuthority> createSortedSet(
            Collection<? extends GrantedAuthority> authorities) {
        // Ensure array iteration order is predictable (as per
        // UserDetails.getAuthorities() contract and SEC-717)
        SortedSet<GrantedAuthority> sortedAuthorities = new TreeSet<>(AUTHORITY_COMPARATOR);

        for (GrantedAuthority grantedAuthority : authorities) {
            Objects.requireNonNull(grantedAuthority,
                    "GrantedAuthority list cannot contain any null elements");
            sortedAuthorities.add(grantedAuthority);
        }

        return ImmutableSortedSet.copyOfSorted(sortedAuthorities);
    }

    //Since this class implements serializable (by inheritance) this comparator also has to be serializable, so
    //the contract will not be broken.
    private static class AuthorityComparator implements Comparator<GrantedAuthority>,
            Serializable {
        private static final long serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID;

        public int compare(GrantedAuthority g1, GrantedAuthority g2) {
            // Neither should ever be null as each entry is checked before adding it to
            // the set.
            // If the authority is null, it is a custom authority and should precede
            // others.
            if (g2.getAuthority() == null) {
                return -1;
            }
            if (g1.getAuthority() == null) {
                return 1;
            }
            return g1.getAuthority().compareTo(g2.getAuthority());
        }
    }

    /**
     * Creates an instance of {@link Builder}.
     *
     * @param username the username. Cannot be null.
     * @return new Builder object.
     */
    public static Builder builder(String username) {
        return new Builder(username);
    }

    /**
     * Builds the user to be added. At minimum the username, and authorities
     * should provided. The remaining attributes have reasonable defaults.
     */
    public static class Builder {
        private String username;
        private List<? extends GrantedAuthority> authorities;
        private boolean accountNonExpired;
        private boolean accountNonLocked;
        private boolean enabled;

        /**
         * Creates a new instance
         *
         * @param username the username. Cannot be null.
         */
        private Builder(String username) {
            Objects.requireNonNull(username, "username cannot be null");
            this.username = username;
            this.accountNonExpired = true;
            this.accountNonLocked = true;
            this.enabled = true;
        }

        /**
         * Populates the authorities. This attribute is required.
         *
         * @param authorities the authorities for this user. Cannot be null, or contain
         *                    null values
         * @return the {@link Builder} for method chaining (i.e. to populate
         * additional attributes for this user)
         */
        public Builder authorities(List<? extends GrantedAuthority> authorities) {
            if (authorities == null) {
                this.authorities = new ArrayList<>();
            } else {
                this.authorities = authorities;
            }
            return this;
        }

        /**
         * Populates the authorities. This attribute is required.
         *
         * @param authorities the authorities for this user (i.e. ROLE_USER, ROLE_ADMIN,
         *                    etc). Cannot be null, or contain null values
         * @return the {@link Builder} for method chaining (i.e. to populate
         * additional attributes for this user)
         */
        public Builder authorities(String... authorities) {
            if (authorities == null) {
                return authorities(new ArrayList<>());
            }
            return authorities(AuthorityUtils.createAuthorityList(authorities));
        }

        /**
         * Defines if the account is expired or not. Default is false.
         *
         * @param accountNonExpired true if the account is expired, false otherwise
         * @return the {@link Builder} for method chaining (i.e. to populate
         * additional attributes for this user)
         */
        public Builder accountNonExpired(boolean accountNonExpired) {
            this.accountNonExpired = accountNonExpired;
            return this;
        }

        /**
         * Defines if the account is locked or not. Default is true.
         *
         * @param accountNonLocked true if the account is locked, false otherwise
         * @return the {@link Builder} for method chaining (i.e. to populate
         * additional attributes for this user)
         */
        public Builder accountNonLocked(boolean accountNonLocked) {
            this.accountNonLocked = accountNonLocked;
            return this;
        }

        /**
         * Defines if the account is enabled or not. Default is true.
         *
         * @param enabled true if the account is disabled, false otherwise
         * @return the {@link Builder} for method chaining (i.e. to populate
         * additional attributes for this user)
         */
        public Builder enabled(boolean enabled) {
            this.enabled = enabled;
            return this;
        }

        /**
         * Builds an object.
         *
         * @return New Instance of @UserDetailsImpl.
         */
        public UserDetailsImpl build() {
            return new UserDetailsImpl(username, authorities, accountNonExpired,
                    accountNonLocked, enabled);
        }
    }

}
