package cern.nxcals.service.security;

import cern.nxcals.common.domain.EntityData;
import cern.nxcals.service.domain.Entity;
import cern.nxcals.service.domain.System;
import cern.nxcals.service.repository.EntityRepository;
import cern.nxcals.service.repository.SystemRepository;
import cern.nxcals.service.security.resolvers.PermissionResolver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.expression.SecurityExpressionRoot;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * NXCALS implementation for expression evaluation in Spring Security.
 *
 * @author wjurasz
 * @author kpodsiad
 */
@Slf4j
public class MethodSecurityExpressionRoot
        extends SecurityExpressionRoot implements MethodSecurityExpressionOperations {

    /**
     * This variable holds value for permissions type WRITE.
     * It is accessible by Spring-EL when evaluating expressions that contains public methods from this class.
     * ex. {@link MethodSecurityExpressionRoot#hasSystemPermission(long, String)} can be called link "hasSystemPermission(1L, ACCESS_WRITE)"
     */
    public static final String ACCESS_WRITE = "WRITE";

    /**
     * This variable holds value for permissions type READ.
     * It is accessible by Spring-EL when evaluating expressions that contains public methods from this class.
     * ex. {@link MethodSecurityExpressionRoot#hasSystemPermission(long, String)} can be called link "hasSystemPermission(1L, ACCESS_READ)"
     */
    public static final String ACCESS_READ = "READ";

    private Object filterObject;
    private Object returnObject;

    private final SystemRepository systemRepository;
    private final EntityRepository entityRepository;
    private final Set<PermissionResolver> permissionResolvers;

    /**
     * Creates a new instance
     *
     * @param authentication the {@link Authentication} to use. Cannot be null.
     */
    MethodSecurityExpressionRoot(Authentication authentication, SystemRepository systemRepository,
                                 EntityRepository entityRepository, Set<PermissionResolver> permissionResolvers) {
        super(authentication);
        this.systemRepository = systemRepository;
        this.entityRepository = entityRepository;
        this.permissionResolvers = permissionResolvers;
    }

    /**
     * Evaluates if user has {@param permissionType} to access given {@param system}
     *
     * @param system     NXCALS system.
     * @param permissionType Level of permissionType (ex. read, write).
     * @return Boolean which states if user has necessary permissions.
     */

    public boolean hasSystemPermission(System system, String permissionType) {
        return system != null && hasPermission(system.getName(), permissionType);
    }

    /**
     * Evaluates if user has {@param permissionType} to access system identified by {@param systemId}
     *
     * @param systemId NXCALS systemId
     * @param permissionType Level of permissionType (ex. read, write).
     * @return Boolean which states if user has necessary permissions.
     */
    public boolean hasSystemPermission(long systemId, String permissionType) {
        Optional<System> systemOptional = systemRepository.findById(systemId);
        return systemOptional.isPresent() && hasSystemPermission(systemOptional.get(), permissionType);
    }

    /**
     * Evaluates if user has {@param permission} to access ALL systems assigned to {@param entityDataList}.
     * This method will fail to authorize the user if ANY of system is not permitted to access.
     *
     * @param entityDataList List of entities asked to edit.
     * @param permissionType Level of permission (ex. read, write).
     * @return Boolean which states if user has necessary permissions.
     */
    public boolean hasEntityPermission(List<EntityData> entityDataList, String permissionType) {
        return entityDataList.stream()
                .allMatch(entityData -> hasPermission(entityData.getSystemData().getName(), permissionType));
    }

    /**
     * Evaluates if user has {@param permissionType} to access system in which entity identyfied by {@param entityId} exists.
     *
     * @param entityId NXCALS entityId
     * @param permissionType Level of permission (ex. read, write).
     * @return Boolean which states if user has necessary permissions.
     */
    public boolean hasEntityPermission(long entityId, String permissionType) {
        Optional<Entity> entityOptional = entityRepository.findById(entityId);
        return entityOptional.isPresent() && hasSystemPermission(entityOptional.get().getPartition().getSystem(), permissionType);
    }

    /**
     * Evaluates if user has {@param permissionType} to access given {@param permissionArea} (ex. VARIABLE)
     * It's achieved by iteration through {@link MethodSecurityExpressionRoot#permissionResolvers}.
     * User is authorized if ANY of {@link cern.nxcals.service.security.resolvers.PermissionResolver} returns matching permission.
     *
     * @param permissionArea String matching permission area in NXCALS.
     * @param permissionType Level of permission (ex. read, write).
     * @return Boolean which states if user has necessary permissions.
     */
    public boolean hasPermission(String permissionArea, String permissionType) {
        return permissionResolvers.stream()
                .anyMatch(resolver -> hasPrivileges(resolver.getPermissionString(permissionArea, permissionType)));
    }

    private boolean hasPrivileges(String permission) {
        boolean isUserPermitted = authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority)
                .anyMatch(authority -> authority.equals(permission));
        if (!isUserPermitted) {
            log.warn("User {} has failed to authorize against permission {}. It is not assigned to the user", authentication.getName(), permission);
        } else {
            log.debug("User {}, successfully authorized with {} permission", authentication.getName(), permission);
        }
        return isUserPermitted;
    }

    @Override
    public void setFilterObject(Object filterObject) {
        this.filterObject = filterObject;
    }

    @Override
    public Object getFilterObject() {
        return filterObject;
    }

    @Override
    public void setReturnObject(Object returnObject) {
        this.returnObject = returnObject;
    }

    @Override
    public Object getReturnObject() {
        return returnObject;
    }

    @Override
    public Object getThis() {
        return this;
    }
}