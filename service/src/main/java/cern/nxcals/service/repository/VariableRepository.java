/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.repository;

import cern.nxcals.service.domain.Variable;

import java.util.List;
import java.util.Optional;

/**
 * Created by ntsvetko on 1/12/17.
 */
public interface VariableRepository extends BaseRepository<Variable> {
    Optional<Variable> findByVariableName(String variableName);

    List<Variable> findByVariableNameLike(String regex);

    List<Variable> findByDescriptionLike(String regex);
}
