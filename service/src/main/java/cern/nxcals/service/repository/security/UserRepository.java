package cern.nxcals.service.repository.security;

import cern.nxcals.service.domain.security.User;
import cern.nxcals.service.repository.BaseRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by wjurasz on 10.08.17.
 */
public interface UserRepository extends BaseRepository<User> {

    /**
     * Performs database query which result in retrieving user with all roles and permissions.
     *
     * @param userName  Name of user.
     * @param realmName Name of realm.
     * @return List of all permissions of user.
     */
    @Query("SELECT u from User u JOIN FETCH u.realm r LEFT OUTER JOIN FETCH u.roles ro LEFT OUTER JOIN FETCH ro.permissions p WHERE u.userName = :userName AND r.realmName = :realmName")
    User findUserWithRealmWithRolesWithPermissions(@Param("userName") String userName, @Param("realmName") String realmName);

}
