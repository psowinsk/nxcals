/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Optional;
import java.util.Set;

@NoRepositoryBean
public interface BaseRepository<T> extends CrudRepository<T, Long> {

    Optional<T> findById(Long id);

    Set<T> findAllByIdIn(Set<Long> ids);

}
