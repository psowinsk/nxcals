/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.repository;

import cern.nxcals.service.domain.Variable;
import cern.nxcals.service.domain.VariableConfig;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.Instant;
import java.util.List;
import java.util.SortedSet;

/**
 * Created by ntsvetko on 1/12/17.
 */
public interface VariableConfigRepository extends BaseRepository<VariableConfig> {

    public SortedSet<VariableConfig> findByVariableIdOrderByValidFromStampDesc(long variableId);

    @Query("select c from VariableConfig c where c.variable = :variable " +
            "and ((c.validFromStamp <= :end or c.validFromStamp is null) " +
            "and (c.validToStamp > :start or c.validToStamp is null)) " +
            "order by c.validFromStamp desc"
    )
    public SortedSet<VariableConfig> findByVariableIdAndTimeWindow(@Param("variable") Variable variable,
            @Param("start") Instant validFromStamp,
            @Param("end") Instant validToStamp);

    public List<VariableConfig> removeByVariable(Variable variable);
}
