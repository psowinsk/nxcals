package cern.nxcals.service.repository.security;

import cern.nxcals.service.domain.security.Permission;
import cern.nxcals.service.repository.BaseRepository;

/**
 * Created by wjurasz on 10.08.17.
 */
public interface PermissionRepository extends BaseRepository<Permission> {

}
