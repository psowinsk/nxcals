package cern.nxcals.service.repository.security;

import cern.nxcals.service.domain.security.Role;
import cern.nxcals.service.repository.BaseRepository;

/**
 * Created by wjurasz on 10.08.17.
 */
public interface RoleRepository extends BaseRepository<Role> {
}
