/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.repository;

import cern.nxcals.service.domain.Entity;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

public interface EntityRepository extends BaseRepository<Entity> {
    Optional<Entity> findByPartitionSystemIdAndKeyValues(long systemId, String keyValues);

    List<Entity> findByPartitionSystemIdAndPartitionId(long systemId, long partitionId);

    List<Entity> findByKeyValuesLike(String keyValuesExpression);

    long countByPartitionSystemIdAndPartitionIdAndLockedUntilStampGreaterThanEqual(long systemId, long partitionId,
            Instant lockedUntilStamp);

}
