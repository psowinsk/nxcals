/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.repository;

import cern.nxcals.service.domain.Entity;
import cern.nxcals.service.domain.EntityHistory;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.Instant;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;

public interface EntityHistoryRepository extends BaseRepository<EntityHistory> {

    /**
     * Finds all history elements that fall into the specified time ranges.
     *
     * @param entity           an {@link Entity} instance to be associated with history
     * @param instantFromNanos the time {@link Instant} that corresponds to the beginning of the enty's validity
     * @param instantToNanos   the time {@link Instant} that corresponds to the end of the entry's validity
     * @return a {@link SortedSet} that contains the matched entities, based on the given search criteria
     */
    @Query("select h from EntityHistory h where h.entity = :entity  and (h.validFromStamp <= :end and (h.validToStamp > :start or h.validToStamp is null)) order by h.validFromStamp desc")
    SortedSet<EntityHistory> findByEntityAndTimestamps(@Param("entity") Entity entity,
            @Param("start") Instant instantFromNanos, @Param("end") Instant instantToNanos);

    /**
     * Finds an entity that it's validity ending time is exactly the same as the provided time instant.
     *
     * @param entity       an {@link Entity} instance to be associated with history
     * @param validToStamp the time {@link Instant} that corresponds to the validity ending of the provided entity
     * @return an {@link EntityHistory} instance that is valid until the provided validity ending time
     */
    Optional<EntityHistory> findByEntityAndValidToStamp(Entity entity, Instant validToStamp);

    /**
     * Finds the entity history that is valid for this given timestamp (the timestamp is inside the history <validFrom;validToStamp) range.
     *
     * @param entity     an {@link Entity} instance to be associated with history
     * @param recordTime the time {@link Instant} that corresponds to a point of time that the entry is valid
     * @return an {@link EntityHistory} instance that is valid in the provided point of time
     */
    @Query("select h from EntityHistory h where h.entity = :entity  and (h.validFromStamp <= :timestamp and (h.validToStamp > :timestamp or h.validToStamp is null))")
    Optional<EntityHistory> findByEntityAndTimestamp(@Param("entity") Entity entity,
            @Param("timestamp") Instant recordTime);

    /**
     * Finds the first (the oldest) element in the history.
     *
     * @param entity an {@link Entity} instance to be associated with history
     * @return an {@link EntityHistory} instance that represents the fist history entry for the given entity
     */
    EntityHistory findFirstByEntityOrderByValidToStampAsc(Entity entity);

    /**
     * Finds the latest (the newest) element in the history.
     *
     * @param entity an {@link Entity} instance to be associated with history
     * @return an {@link EntityHistory} instance that represents the latest history entry for the given entity
     */
    EntityHistory findFirstByEntityOrderByValidToStampDesc(Entity entity);

    /**
     * Finds the last (the newest) element in the history.
     *
     * @param entity an {@link Entity} instance to be associated with history
     * @return an {@link EntityHistory} instance that represents the newest history entry for the given entity
     */
    EntityHistory findByEntityAndValidToStampIsNull(Entity entity);

    /**
     * Finds the first (the oldest) element in the history for each of the provided entities.
     * The expected result is one history entry per each provided entity that exists in the system.
     *
     * @param entities a collection of {@link Entity} instances to fetch the history for.
     * @return a {@link Set} of {@link EntityHistory} instances that correspond to the provided entities.
     */
    @Query("select h from EntityHistory h where h.validFromStamp = (select min(hh.validFromStamp) from EntityHistory hh where hh.entity = h.entity) and h.entity in :entities")
    Set<EntityHistory> findAllFirstEntriesByEntityIn(@Param("entities") Set<Entity> entities);

    /**
     * Finds the latest (the newest) element in the history for each of the provided entities.
     * The expected result is one history entry per each provided entity that exists in the system.
     *
     * @param entities a collection of {@link Entity} instances to fetch the history for.
     * @return a {@link Set} of {@link EntityHistory} instances that correspond to the provided entities.
     */
    @Query("select h from EntityHistory h where h.validFromStamp = (select max(hh.validFromStamp) from EntityHistory hh where hh.entity = h.entity) and h.entity in :entities")
    Set<EntityHistory> findAllLatestEntriesByEntityIn(@Param("entities") Set<Entity> entities);

}
