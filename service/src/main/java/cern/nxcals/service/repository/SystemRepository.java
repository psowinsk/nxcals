/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.repository;

import java.util.Optional;

public interface SystemRepository extends BaseRepository<cern.nxcals.service.domain.System> {

    Optional<cern.nxcals.service.domain.System> findByName(String name);

}
