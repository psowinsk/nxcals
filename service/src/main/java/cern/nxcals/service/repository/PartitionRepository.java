/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.repository;

import cern.nxcals.service.domain.Partition;

import java.util.Optional;

public interface PartitionRepository extends BaseRepository<Partition> {

    Optional<Partition> findBySystemIdAndKeyValues(long systemId, String keyHash);

}
