/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.repository;

import cern.nxcals.service.domain.Schema;

import java.util.Optional;

public interface SchemaRepository extends BaseRepository<Schema> {

    /**
     * @param hash - md5 hash from the content
     */
    Optional<Schema> findByContentHash(String hash);

}
