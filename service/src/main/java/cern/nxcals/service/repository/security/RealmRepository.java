package cern.nxcals.service.repository.security;

import cern.nxcals.service.domain.security.Realm;
import cern.nxcals.service.repository.BaseRepository;

public interface RealmRepository extends BaseRepository<Realm> {
}
