/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author ntsvetko
 */
@SpringBootApplication
@EnableJpaRepositories(transactionManagerRef = "jpaTransactionManager")
@Slf4j
public class Application {

    public static void main(String[] args) {
        try {
            SpringApplication app = new SpringApplication(Application.class);
            app.addListeners(new ApplicationPidFileWriter());
            ConfigurableApplicationContext context = app.run(args);
            IdGeneratorFactory.setContext(context);
        } catch (Exception ex) {
            log.error("Cannot start service appplication", ex);
            System.exit(-1);
        }
    }

}
