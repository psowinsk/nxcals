/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest;

/**
 * Indicates for inconsistent or duplicated configuration data
 *
 * @author ntsvetko
 */

public class ConfigDataConflictException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ConfigDataConflictException(String message) {
        super(message);
    }

    public ConfigDataConflictException(String message, Throwable cause) {
        super(message, cause);
    }

}
