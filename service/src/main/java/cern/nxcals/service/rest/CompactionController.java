/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest;

import cern.nxcals.service.internal.InternalCompactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static cern.nxcals.common.web.Endpoints.COMPACTION_SHOULD_COMPACT;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class CompactionController {

    @Autowired
    private InternalCompactionService internalCompactionService;

    @RequestMapping(value = COMPACTION_SHOULD_COMPACT, method = POST)
    public boolean shouldCompact(@RequestBody String path) {
        return internalCompactionService.shouldCompact(path);
    }
}
