/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.rest;

import cern.nxcals.common.domain.SchemaData;
import cern.nxcals.service.domain.Schema;
import cern.nxcals.service.repository.SchemaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static cern.nxcals.common.web.Endpoints.SCHEMA_WITH_ID;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Schema Controller API.
 */
@RestController
public class SchemaController {

    @Autowired
    private SchemaRepository schemaService;

    @RequestMapping(value = SCHEMA_WITH_ID, method = GET)
    public SchemaData findById(@PathVariable(value = "schemaId") long schemaId) {
        Schema schema = schemaService.findById(schemaId)
                .orElseThrow(() -> new NotFoundRuntimeException("Schema for id " + schemaId + " not found"));
        return schema.toSchemaData();
    }
}
