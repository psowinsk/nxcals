/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.rest;

import cern.nxcals.common.domain.EntityResources;
import cern.nxcals.service.internal.InternalEntityResourcesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Set;

import static cern.nxcals.common.web.Endpoints.RESOURCES;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class EntityResourcesController {
    @Autowired
    private InternalEntityResourcesService entityResourcesService;

    @RequestMapping(value = RESOURCES, method = POST, params = { "systemId", "startTime", "endTime" },
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public Set<EntityResources> findBySystemIdKeyValuesAndTimeWindow(@RequestParam long systemId,
            @RequestBody Map<String, Object> entityKeyValues, @RequestParam long startTime,
            @RequestParam long endTime) {
        return entityResourcesService
                .findBySystemIdKeyValuesAndTimeWindow(systemId, entityKeyValues, startTime, endTime);
    }

    @RequestMapping(value = RESOURCES, method = GET, params = { "entityId", "startTime", "endTime" })
    public Set<EntityResources> findByEntityIdAndTimeWindow(@RequestParam long entityId,
            @RequestParam long startTime,
            @RequestParam long endTime) {
        return entityResourcesService.findByEntityIdAndTimeWindow(entityId, startTime, endTime);
    }
}
