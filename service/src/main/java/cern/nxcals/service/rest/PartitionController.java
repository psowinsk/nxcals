/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.rest;

import cern.nxcals.common.domain.PartitionData;
import cern.nxcals.service.domain.Partition;
import cern.nxcals.service.domain.System;
import cern.nxcals.service.repository.PartitionRepository;
import cern.nxcals.service.repository.SystemRepository;
import com.google.common.annotations.VisibleForTesting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import static cern.nxcals.common.utils.KeyValuesUtils.convertMapIntoAvroSchemaString;
import static cern.nxcals.common.web.Endpoints.PARTITIONS;
import static java.lang.String.format;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class PartitionController {

    @Autowired
    private PartitionRepository partitionRepository;

    @Autowired
    private SystemRepository systemRepository;

    @VisibleForTesting
    static final String ERROR_MESSAGE = "Partition for system %s and %s not found";

    @RequestMapping(value = PARTITIONS, method = POST, params = "systemId", consumes = MediaType.APPLICATION_JSON_VALUE)
    public PartitionData findBySystemIdAndKeyValues(@RequestParam long systemId,
            @RequestBody Map<String, Object> partitionKeyValuesMap) {

        System system = systemRepository.findById(systemId)
                .orElseThrow(() -> new NotFoundRuntimeException(format("System with id: %s not found", systemId)));
        String partitionKeyValues = convertMapIntoAvroSchemaString(partitionKeyValuesMap, system.getPartitionKeyDefs());

        Partition partition = partitionRepository.findBySystemIdAndKeyValues(systemId, partitionKeyValues)
                .orElseThrow(
                        () -> new NotFoundRuntimeException(format(ERROR_MESSAGE, systemId, partitionKeyValuesMap)));
        return partition.toPartitionData();
    }
}
