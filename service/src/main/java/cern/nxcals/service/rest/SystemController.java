/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.rest;

import cern.nxcals.common.domain.SystemData;
import cern.nxcals.service.domain.System;
import cern.nxcals.service.repository.SystemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static cern.nxcals.common.web.Endpoints.SYSTEMS;
import static cern.nxcals.common.web.Endpoints.SYSTEM_WITH_ID;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class SystemController {

    @Autowired
    private SystemRepository systemRepository;

    @RequestMapping(value = SYSTEMS, method = GET, params = "name")
    public SystemData findByName(@RequestParam String name) {
        System system = systemRepository.findByName(name)
                .orElseThrow(() -> new NotFoundRuntimeException("System for name " + name + " not found"));
        return system.toSystemData();
    }

    @RequestMapping(value = SYSTEM_WITH_ID, method = GET)
    public SystemData findById(@PathVariable long id) {
        System system = systemRepository.findById(id)
                .orElseThrow(() -> new NotFoundRuntimeException("System for id " + id + " not found"));
        return system.toSystemData();
    }

}
