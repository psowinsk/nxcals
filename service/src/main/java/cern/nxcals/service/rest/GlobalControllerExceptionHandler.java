package cern.nxcals.service.rest;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Throwables;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.persistence.OptimisticLockException;
import javax.validation.ConstraintViolationException;

@RestControllerAdvice
@Slf4j
public class GlobalControllerExceptionHandler {


    @VisibleForTesting
    static final String INTERNAL_ERROR_FORMAT = "500 Internal Server Error: %s. Stacktrace: %s";
    @VisibleForTesting
    static final String VERSION_MISMATCH_ERROR_FORMAT = "409 Version Mismatch Error: %s. Stacktrace: %s";
    @VisibleForTesting
    static final String CONSTRAIN_VIOLATION_ERROR_FORMAT = "409 Constraint Violation Error: %s. Stacktrace: %s";
    @VisibleForTesting
    private static final String STACKTRACE = "Stacktrace: %s";
    static final String NOT_FOUND_ERROR_FORMAT_PREFIX = "404 Resource Not Found: %s. ";

    static final String NOT_FOUND_ERROR_FORMAT = NOT_FOUND_ERROR_FORMAT_PREFIX + STACKTRACE;
    @VisibleForTesting
    static final String FORBIDDEN_ERROR_FORMAT_PREFIX = "403 You do not have a permission to access this path: %s. ";
    static final String FORBIDDEN_ERROR_FORMAT = FORBIDDEN_ERROR_FORMAT_PREFIX + STACKTRACE;

    @VisibleForTesting
    static final String DATA_CONFLICT_ERROR_FORMAT = "409 Inconsistent or duplicated configuration data: %s. Stacktrace: %s";

    @VisibleForTesting
    static final String ILLEGAL_ARGUMENT_ERROR_FORMAT = "409 Illegal argument: %s. Stacktrace: %s";


    @ExceptionHandler(value = { RuntimeException.class })
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String handleBasicExpression(Exception exception) {
        String message = exception.getMessage();
        log.error(message, exception);
        return String.format(INTERNAL_ERROR_FORMAT, message, Throwables.getStackTraceAsString(exception));
    }


    @ExceptionHandler(value = { NotFoundRuntimeException.class })
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleNoContent(NotFoundRuntimeException exception) {
        String message = exception.getMessage();
        log.warn(message, exception);
        return String.format(NOT_FOUND_ERROR_FORMAT, message, Throwables.getStackTraceAsString(exception));
    }

    @ExceptionHandler(value = { AccessDeniedException.class })
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public String handleAccessDenied(AccessDeniedException exception) {
        String message = exception.getMessage();
        log.warn(message, exception);
        return String.format(FORBIDDEN_ERROR_FORMAT, message, Throwables.getStackTraceAsString(exception));
    }

    @ExceptionHandler(value = { VersionMismatchException.class, OptimisticLockException.class })
    @ResponseStatus(HttpStatus.CONFLICT)
    public String handleVersionMismatchException(Exception exception) {
        String message = exception.getMessage();
        log.error(message, exception);
        return String.format(VERSION_MISMATCH_ERROR_FORMAT, message, Throwables.getStackTraceAsString(exception));
    }

    @ExceptionHandler(value = { ConstraintViolationException.class })
    @ResponseStatus(HttpStatus.CONFLICT)
    public String handleConstraintViolationException(Exception exception) {
        String message = exception.getMessage();
        log.error(message, exception);
        return String.format(CONSTRAIN_VIOLATION_ERROR_FORMAT, message, Throwables.getStackTraceAsString(exception));
    }

    @ExceptionHandler(value = { ConfigDataConflictException.class })
    @ResponseStatus(HttpStatus.CONFLICT)
    public String handleConfigDataConflictException(Exception exception) {
        String message = exception.getMessage();
        log.warn(message, exception);
        return String.format(DATA_CONFLICT_ERROR_FORMAT, message, Throwables.getStackTraceAsString(exception));
    }

    @ExceptionHandler(value = { IllegalArgumentException.class })
    @ResponseStatus(HttpStatus.CONFLICT)
    public String handleIllegalArgumentException(Exception exception) {
        String message = exception.getMessage();
        log.error(message, exception);
        return String.format(ILLEGAL_ARGUMENT_ERROR_FORMAT, message, Throwables.getStackTraceAsString(exception));
    }


}
