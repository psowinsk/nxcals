/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest;

import cern.nxcals.common.domain.VariableData;
import cern.nxcals.service.domain.Variable;
import cern.nxcals.service.internal.InternalVariableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static cern.nxcals.common.web.Endpoints.VARIABLES;
import static cern.nxcals.common.web.Endpoints.VARIABLE_REGISTER_OR_UPDATE;
import static java.util.stream.Collectors.toList;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@RestController
public class VariableController {

    @Autowired
    private InternalVariableService internalVariableService;

    @RequestMapping(value = VARIABLES, method = GET, params = "name")
    public VariableData findByVariableName(@RequestParam String name) {
        return internalVariableService.findByVariableName(name).toVariableData();
    }

    @RequestMapping(value = VARIABLES, method = GET, params = { "name", "startTime", "endTime" })
    public VariableData findByVariableNameAndTimeWindow(@RequestParam String name,
            @RequestParam Long startTime,
            @RequestParam Long endTime) {
        return internalVariableService.findByVariableNameAndTimeWindow(name, startTime, endTime)
                .toVariableData();
    }

    @RequestMapping(value = VARIABLE_REGISTER_OR_UPDATE, method = PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission('VARIABLE', ACCESS_WRITE)")
    public VariableData registerOrUpdateVariableFor(@RequestBody VariableData variableData) {
        return internalVariableService.registerOrUpdateVariableFor(variableData).toVariableData();
    }

    @RequestMapping(value = VARIABLES, method = GET, params = { "nameExpression" })
    public List<VariableData> findByNameLike(@RequestParam String nameExpression) {
        return internalVariableService.findByNameLike(nameExpression).stream().map(Variable::toVariableData)
                .collect(toList());
    }

    @RequestMapping(value = VARIABLES, method = GET, params = { "descriptionExpression" })
    public List<VariableData> findByDescriptionLike(@RequestParam String descriptionExpression) {
        return internalVariableService.findByDescriptionLike(descriptionExpression).stream()
                .map(Variable::toVariableData).collect(toList());
    }
}
