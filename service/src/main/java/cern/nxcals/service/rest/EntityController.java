/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.rest;

import cern.nxcals.common.domain.EntityData;
import cern.nxcals.common.domain.FindOrCreateEntityRequest;
import cern.nxcals.service.domain.Entity;
import cern.nxcals.service.internal.InternalEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static cern.nxcals.common.web.Endpoints.ENTITIES;
import static cern.nxcals.common.web.Endpoints.ENTITY_EXTEND_FIRST_HISTORY;
import static cern.nxcals.common.web.Endpoints.ENTITY_UPDATE;
import static java.util.stream.Collectors.toList;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

/**
 * Remarks (msobiesz): what if a repository's method throws an exception? certainly we get 500 as http error code - do
 * we want that?
 * Remarks (mamajews): http error codes can now be customized by using {@link cern.nxcals.service.rest.GlobalControllerExceptionHandler}
 */
@RestController
public class EntityController {

    @Autowired
    private InternalEntityService entityService;

    @RequestMapping(value = ENTITIES, method = POST, params = "systemId", consumes = MediaType.APPLICATION_JSON_VALUE)
    public EntityData findBySystemIdAndKeyValues(@RequestParam("systemId") long systemId,
            @RequestBody Map<String, Object> entityKeyValues) {
        return entityService.findByPartitionSystemIdAndKeyValues(systemId, entityKeyValues).toEntityData();
    }

    @RequestMapping(value = ENTITIES, method = PUT, params = { "systemId", "recordTimestamp" })
    @PreAuthorize("hasSystemPermission(#systemId, ACCESS_WRITE)")
    public EntityData findOrCreateEntityFor(@RequestParam long systemId,
            @RequestParam long recordTimestamp,
            @RequestBody FindOrCreateEntityRequest findOrCreateEntityRequest) {
        return entityService
                .findOrCreateEntityFor(systemId, findOrCreateEntityRequest.getEntityKeyValues(),
                        findOrCreateEntityRequest.getPartitionKeyValues(), findOrCreateEntityRequest.getSchema(),
                        recordTimestamp).toEntityData();
    }

    @RequestMapping(value = ENTITIES, method = PUT, params = { "systemId", "entityId", "partitionId",
            "recordTimestamp" })
    @PreAuthorize("hasSystemPermission(#systemId, ACCESS_WRITE)")
    public EntityData findOrCreateEntityFor(@RequestParam long systemId,
            @RequestParam long entityId,
            @RequestParam long partitionId,
            @RequestBody String recordFieldDefinitions,
            @RequestParam long recordTimestamp) {
        return entityService.findOrCreateEntityFor(systemId, entityId, partitionId, recordFieldDefinitions,
                recordTimestamp).toEntityData();
    }

    @RequestMapping(value = ENTITY_EXTEND_FIRST_HISTORY, method = PUT, params = { "entityId", "from" })
    @PreAuthorize("hasEntityPermission(#entityId, ACCESS_WRITE)")
    public EntityData extendEntityFirstHistoryDataFor(@RequestParam long entityId,
            @RequestParam long from,
            @RequestBody String schema) {
        return entityService.extendEntityFirstHistoryDataFor(entityId, schema, from).toEntityData();
    }

    @RequestMapping(value = ENTITIES, method = POST, params = { "systemId", "startTime", "endTime" },
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public EntityData findBySystemIdKeyValuesAndTimeWindow(@RequestParam long systemId,
            @RequestBody Map<String, Object> entityKeyValues, @RequestParam long startTime,
            @RequestParam long endTime) {
        return entityService.findEntityWithHistForTimeWindow(systemId, entityKeyValues, startTime, endTime)
                .toEntityData();
    }

    @RequestMapping(value = ENTITIES, method = GET, params = { "entityId", "startTime", "endTime" })
    public EntityData findByEntityIdAndTimeWindow(@RequestParam long entityId,
            @RequestParam long startTime, @RequestParam long endTime) {
        return entityService.findByEntityIdAndTimeWindow(entityId, startTime, endTime).toEntityData();
    }

    @RequestMapping(value = ENTITIES, method = GET, params = "keyValuesExpression")
    public List<EntityData> findByKeyValuesLike(@RequestParam String keyValuesExpression) {
        return entityService.findByKeyValueLike(keyValuesExpression).stream().map(Entity::toEntityData).collect(
                toList());
    }

    @RequestMapping(value = ENTITY_UPDATE, method = PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasEntityPermission(#entityDataList, ACCESS_WRITE)")
    public List<EntityData> updateEntities(@NotEmpty @RequestBody List<EntityData> entityDataList) {
        return entityService.updateEntities(entityDataList).stream().map(Entity::toEntityData).collect(toList());
    }

    @RequestMapping(value = ENTITIES + "/{entityId}", method = GET)
    public EntityData findOneById(@PathVariable("entityId") long entityId) {
        return entityService.findById(entityId).toEntityData();
    }

    @RequestMapping(value = ENTITIES, method = GET, params = "ids")
    public Set<EntityData> findAllById(@NotEmpty @RequestParam Set<Long> ids) {
        return entityService.findAllByIdIn(ids).stream()
                .map(Entity::toEntityData)
                .collect(Collectors.toSet());
    }
}
