package cern.nxcals.common.config;

import cern.nxcals.common.spark.SparkUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.SparkSession;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

/**
 * A default Spark Spring config that can be reused.
 * Created by jwozniak on 05/04/17.
 */
@Configuration
public class SparkContext {

    /**
     * Creates Spark configuration
     * @param config Spark properties config
     * @return Configuration for a Spark application
     */
    @Bean
    @DependsOn("kerberos")
    public SparkConf createSparkConf(SparkPropertiesConfig config) {
        return SparkUtils.createSparkConf(config);
    }

    /**
     * Creates Spark session
     * @param conf Configuration for a Spark application
     * @return The entry point to programming Spark with the Dataset and DataFrame API.
     */
    @Bean
    @DependsOn("kerberos")
    public SparkSession createSparkSession(SparkConf conf) {
        //FIXME - currently there are problems with
        return SparkSession.builder().config(conf)./*enableHiveSupport().*/
                //FIXME - to be understood what is this directory really???
                        // Use spark.sql.warehouse.dir Spark property to change the location of Hive's
                        // hive.metastore.warehouse.dir property, i.e. the location of the Hive local/embedded metastore database (using Derby).
                        config("spark.sql.warehouse.dir", "/tmp/nxcals/spark/warehouse-" + System.currentTimeMillis()).
                        getOrCreate();
    }

    /**
     * Creates Spark properties config
     * from application name, master type, list of jars and list of properties
     * @return Spark properties config
     */
    @ConfigurationProperties(prefix = "spark")
    @Bean
    public SparkPropertiesConfig createSparkPropertiesConfig() {
        return new SparkPropertiesConfig();
    }

}
