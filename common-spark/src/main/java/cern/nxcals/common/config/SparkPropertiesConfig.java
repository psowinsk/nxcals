package cern.nxcals.common.config;

import java.util.Map;

/**
 * Used to configure Spark from Spring.
 * Created by jwozniak on 05/04/17.
 */
public class SparkPropertiesConfig {
    private String appName;
    private String masterType;
    private String[] jars;
    private Map<String, String> properties;

    public Map<String, String> getProperties() {
        return this.properties;
    }

    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }

    public String getAppName() {
        return this.appName;
    }

    public void setAppName(String newAppName) {
        this.appName = newAppName;
    }

    public String getMasterType() {
        return this.masterType;
    }

    public void setMasterType(String newMasterType) {
        this.masterType = newMasterType;
    }

    public String[] getJars() {
        return this.jars;
    }

    public void setJars(String[] newJars) {
        this.jars = newJars;
    }
}
