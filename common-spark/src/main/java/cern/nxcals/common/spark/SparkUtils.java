package cern.nxcals.common.spark;

import cern.nxcals.common.config.SparkPropertiesConfig;
import org.apache.spark.SparkConf;

/**
 * Created by jwozniak on 05/04/17.
 */
public final class SparkUtils {

    private SparkUtils() {
        throw new IllegalStateException("SparkUtils class");
    }

    /**
     * Creates Spark configuration
     * @param config Spark properties config
     * @return Configuration for a Spark application
     */
    public static SparkConf createSparkConf(SparkPropertiesConfig config) {
        SparkConf conf = new SparkConf().setAppName(config.getAppName()).setMaster(config.getMasterType());
        config.getProperties().forEach(conf::set);
        final String[] jars = config.getJars();
        if (jars != null && jars.length > 0) {
            conf.setJars(jars);
        }
        return conf;
    }
}
