package cern.nxcals.monitoring.grok.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Periodically looks for files in given locations.
 * Created by jwozniak on 6/4/17.
 */
@Service
@Slf4j
class FileScanner {
    List<Path> scan(String basePath, String glob, int depth) {
        log.trace("Scanning for files in path={}, glob={}, depth={}", basePath, glob, depth);
        PathMatcher pathMatcher = FileSystems.getDefault().getPathMatcher("glob:" + glob);
        Path path = FileSystems.getDefault().getPath(basePath);
        try (Stream<Path> files = Files.find(path, depth, (p, a) -> {
            log.trace("Path found {}", p);
            return p.toFile().exists() && pathMatcher.matches(p);
        })) {
            return files.collect(Collectors.toList());
        } catch (UncheckedIOException ex) {
            if (ex.getCause() instanceof NoSuchFileException) {
                //This happens if the file gets deleted while iterating over them, not sure why this
                //API is not immune to that. Anyway it is safe to ignore it (jwozniak)
                log.trace("No such file exception, can be ignored: {}", ex.getMessage());
                return Collections.emptyList();
            } else {
                throw ex;
            }
        } catch (IOException e) {
            log.error("Exception while scanning for files in basePath={}, glob={}", basePath, glob, e);
            throw new RuntimeException(e);
        }
    }

}
