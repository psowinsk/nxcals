package cern.nxcals.monitoring.grok.service;

import cern.nxcals.monitoring.grok.domain.Configuration;
import cern.nxcals.monitoring.grok.domain.RuleConfig;
import cern.nxcals.monitoring.grok.tailer.Tailer;
import cern.nxcals.monitoring.grok.tailer.TailerListenerAdapter;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.boot.actuate.metrics.CounterService;
import org.springframework.boot.actuate.metrics.GaugeService;
import org.springframework.stereotype.Service;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.File;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ScheduledExecutorService;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * The main file processor that periodically scans the paths for files and creates Tailers for them.
 * Those tailers search for pattern matching and if matches it increments the CounterService counters.
 * Created by jwozniak on 6/4/17.
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class FileProcessor implements DisposableBean {
    public static final int FILE_CHANGES_CHECK_DELAY_MILLIS = 2000;
    @NonNull
    private final FileScanner fileScanner;
    @NonNull
    private final ScheduledExecutorService executorService;
    @NonNull
    private final CounterService counterService;
    @NonNull
    private final GaugeService gaugeService;
    @NonNull
    private final Configuration config;

    //For testing...
    @Setter(AccessLevel.PACKAGE)
    private Function<Path, String> metricsPrefixGenerator = this::defaultMetricPrefix;

    private final Map<Path, Tailer> tailers = new HashMap<>();
    private final ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");

    private String defaultMetricPrefix(Path path) {
        //FIXME - this should be somehow configurable. This is meant to create the significant metric name.
        Pattern pattern = Pattern.compile("([a-zA-Z1-9\\-_]*)-(.*)");
        String name = path.getName(2).toString();
        Matcher matcher = pattern.matcher(name);
        if (matcher.matches()) {
            name = matcher.group(1);
        } else {
            log.warn("Module name {} does not match pattern {}! Please fix module naming to conform with the pattern.",
                    name, pattern.pattern());
        }
        return name + "___" + path.getFileName().toString();
    }

    public synchronized void start() {
        executorService.scheduleAtFixedRate(this::processDirs,
                0, config.getSearchEvery(), config.getSearchEveryTimeUnit()
        );

    }

    private synchronized void processDirs() {
        try {
            List<Path> paths = fileScanner.scan(config.getBasePath(), config.getFileGlob(), config.getDepth());
            List<File> newFiles = paths.stream()
                    //remove the files for which we have already tailers created.
                    .filter(path -> !tailers.containsKey(path))
                    .map(Path::toFile).collect(Collectors.toList());

            for (File file : newFiles) {
                //create tailer for the file if we have matching rule for this file.
                Optional<Tailer> tailerOptional = createTailer(file);
                if (tailerOptional.isPresent()) {
                    tailers.put(tailerOptional.get().getFile().toPath(), tailerOptional.get());
                } else {
                    log.warn("No rules matching file=", file);
                }
            }

        } catch (Exception ex) {
            log.error("Error while processing basePath={}, glob={}", config.getBasePath(), config.getFileGlob(), ex);
        }
    }

    private Optional<Tailer> createTailer(File file) {
        List<RuleConfig> ruleConfigs = config.getRules().stream()
                //This selects only those rules for which this file path matches the specified files patterns.
                .filter(rule -> rule.getFilePatternsInclude().stream()
                        .anyMatch(filePattern -> Pattern.matches(filePattern, file.getAbsolutePath())))
                //but filter out those that are on the exclude list
                .filter(rule -> rule.getFilePatternsExclude().stream()
                        .noneMatch(filePattern -> Pattern.matches(filePattern, file.getAbsolutePath())))
                .collect(Collectors.toList());
        //create the Tailer for a file only with matched rules
        if (ruleConfigs.isEmpty()) {
            return Optional.empty();
        }
        log.debug("Creating Tailer for file={} with rules={}", file.getAbsolutePath(),
                ruleConfigs.stream().map(RuleConfig::getName).collect(Collectors.toList()));
        return Optional.of(Tailer.create(file,
                new PatternTailerListener(file.toPath(), ruleConfigs, metricsPrefixGenerator.apply(file.toPath())),
                FILE_CHANGES_CHECK_DELAY_MILLIS, true));
    }

    @Override
    public synchronized void destroy() throws Exception {
        log.debug("Destroy called");
        executorService.shutdown();
        tailers.values().forEach(Tailer::stop);
    }

    @Data
    private static class CompiledRule {
        @NonNull
        private final RuleConfig ruleConfig;
        @NonNull
        private final Pattern[] patterns;

        Predicate<String> firstMatcher() {
            return line -> patterns[0].matcher(line).matches() &&
                    Arrays.stream(Arrays.copyOfRange(patterns, 1, patterns.length))
                            .noneMatch(pattern -> pattern.matcher(line).matches());
        }
    }

    private class PatternTailerListener extends TailerListenerAdapter {
        private final Path path;
        private final List<RuleConfig> rules;
        private final List<CompiledRule> ruleWithPatterns;
        private final Bindings bindings;

        PatternTailerListener(Path path, List<RuleConfig> rules, String metricNamePrefix) {
            this.path = Objects.requireNonNull(path);
            this.rules = Objects.requireNonNull(rules);

            this.ruleWithPatterns = this.rules.stream().map(this::createRuleWithPatterns).collect(Collectors.toList());
            this.bindings = engine.createBindings();
            this.bindings.put("counterService", counterService);
            this.bindings.put("gaugeService", gaugeService);
            this.bindings.put("log", log);
            this.bindings.put("metricNamePrefix", metricNamePrefix);
            this.bindings.put("path", path);
            this.bindings.put("metricPrefix", Objects.requireNonNull(metricNamePrefix));
            initRules();
        }

        private void initRules() {
            this.ruleWithPatterns.forEach(rule -> {
                try {
                    bindings.put("ruleConfig", rule.getRuleConfig());
                    engine.eval(rule.getRuleConfig().getInitScript(), bindings);
                } catch (ScriptException ex) {
                    log.error("Error running init script for rule={}", rule.getRuleConfig(), ex);
                }
            });
        }

        private CompiledRule createRuleWithPatterns(RuleConfig rule) {
            List<Pattern> patterns = rule.getLinePatterns().stream().map(Pattern::compile).collect(Collectors.toList());
            return new CompiledRule(rule, patterns.toArray(new Pattern[0]));
        }

        @Override
        public void endOfFileReached() {
            log.trace("End of file in path={}", path);
        }

        @Override
        public void handle(Exception ex) {
            log.error("Exception in processing file in path={}", path, ex);
        }

        @Override
        public void fileRotated() {
            log.trace("File rotated in path={}", path);
        }

        @Override
        public void fileNotFound() {
            log.trace("File not found path={}", path);
        }

        @Override
        public void handle(String line) {
            this.ruleWithPatterns.forEach(rule -> executeRuleForLine(rule, line));
        }

        private void executeRuleForLine(CompiledRule rule, String line) {
            bindings.put("patterns", rule.getPatterns());
            bindings.put("ruleConfig", rule.getRuleConfig());
            bindings.put("firstMatcher", rule.firstMatcher());
            bindings.put("line", line);
            try {
                engine.eval(rule.getRuleConfig().getConditionScript(), bindings);
            } catch (ScriptException ex) {
                log.error("Exception while processing rule {} for line {}", rule.getRuleConfig().getName(), line, ex);
            }
        }
    }

}
