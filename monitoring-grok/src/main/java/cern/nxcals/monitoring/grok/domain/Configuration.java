package cern.nxcals.monitoring.grok.domain;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by jwozniak on 6/4/17.
 */
@ConfigurationProperties("nxcals.monitoring.grok")
@Data
public class Configuration {
    private String basePath;
    private String fileGlob;
    private int depth;
    private List<RuleConfig> rules;
    private int searchEvery;
    private TimeUnit searchEveryTimeUnit;
}
