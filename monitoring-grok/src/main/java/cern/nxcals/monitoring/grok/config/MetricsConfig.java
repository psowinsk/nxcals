package cern.nxcals.monitoring.grok.config;

import com.codahale.metrics.JmxReporter;
import com.codahale.metrics.MetricRegistry;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by jwozniak on 17/11/16.
 */
@Configuration
public class MetricsConfig {
    @Value("${nxcals.monitoring.grok.metrics.domain}")
    private String metricsDomain;

    /**
     * This is to export all metrics as JMX. Those can later be exported to Prometheus.
     *
     * @return
     */
    @Bean
    public JmxReporter jmxReporter(MetricRegistry registry) {
        JmxReporter reporter = JmxReporter.forRegistry(registry).inDomain(metricsDomain).build();

        reporter.start();
        return reporter;
    }

}
