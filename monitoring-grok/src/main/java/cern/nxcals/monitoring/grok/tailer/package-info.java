/**
 * This is a temporary copy of the Apache Commons IO Tailer class due to a bug https://issues.apache.org/jira/browse/IO-530.
 * This affects version 2.5 of the lib and 2.6 has not been released yet.
 * Due to the bug Tailer uses 100% of the CPU.
 * Once 2.6 is release we can remove it.
 * Created by jwozniak on 6/5/17.
 */
package cern.nxcals.monitoring.grok.tailer;