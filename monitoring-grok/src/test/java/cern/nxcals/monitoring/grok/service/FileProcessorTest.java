package cern.nxcals.monitoring.grok.service;

import cern.nxcals.monitoring.grok.domain.Configuration;
import cern.nxcals.monitoring.grok.domain.RuleConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.actuate.metrics.CounterService;
import org.springframework.boot.actuate.metrics.GaugeService;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by jwozniak on 6/6/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class FileProcessorTest {

    FileScanner fileScanner = new FileScanner();
    @Mock
    CounterService counterService;
    @Mock
    GaugeService gaugeService;

    @Test
    public void testProcessor() throws Exception {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        Configuration config = new Configuration();
        config.setBasePath(".");
        config.setFileGlob("**/tailer-test.log");
        config.setDepth(1);
        config.setSearchEvery(1);
        config.setSearchEveryTimeUnit(TimeUnit.MILLISECONDS);
        RuleConfig ruleConfig = new RuleConfig();
        ruleConfig.setFilePatternsInclude(Arrays.asList(".*"));
        ruleConfig.setFilePatternsExclude(Collections.emptyList());
        ruleConfig.setInitScript("counterService.decrement('test')");
        //ruleConfig.setInitScript("");
        ruleConfig.setConditionScript("if(patterns[0].matcher(line).matches()) {counterService.increment('test');}" +
                "var matcher = patterns[1].matcher(line);" +
                "if(matcher.matches()) {" +
                "gaugeService.submit('test1', java.lang.Integer.parseInt(matcher.group(1)));" +
                "}");
        ruleConfig.setLinePatterns(Arrays.asList(".* ERR .*", ".*value=([0-9]+).*"));
        config.setRules(Arrays.asList(ruleConfig));
        FileProcessor processor = new FileProcessor(fileScanner, executor, counterService, gaugeService, config);
        processor.setMetricsPrefixGenerator(p -> "prefix");
        processor.start(); //file is not yet created
        TimeUnit.SECONDS.sleep(2); // wait for the processor to initialize and start scanning.

        writeToFile(); //write the file
        TimeUnit.SECONDS
                .sleep(3); //wait for the processor to process the lines, 3 secs should do, if not please increase.
        Mockito.verify(counterService, Mockito.times(1)).decrement("test");
        Mockito.verify(counterService, Mockito.times(3)).increment("test");
        Mockito.verify(gaugeService, Mockito.times(1)).submit("test1", 20);

        processor.destroy();
    }

    @Test
    public void testProcessorWithFirstMatcher() throws Exception {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        Configuration config = new Configuration();
        config.setBasePath(".");
        config.setFileGlob("**/tailer-test.log");
        config.setDepth(1);
        config.setSearchEvery(1);
        config.setSearchEveryTimeUnit(TimeUnit.MILLISECONDS);
        RuleConfig ruleConfig = new RuleConfig();
        ruleConfig.setFilePatternsInclude(Arrays.asList(".*"));
        ruleConfig.setFilePatternsExclude(Collections.emptyList());
        ruleConfig.setInitScript("");
        //ruleConfig.setInitScript("");
        ruleConfig.setConditionScript("if(firstMatcher.test(line)) {counterService.increment('test');}");
        ruleConfig.setLinePatterns(Arrays.asList(".* ERR .*", ".*dont count.*"));
        config.setRules(Arrays.asList(ruleConfig));
        FileProcessor processor = new FileProcessor(fileScanner, executor, counterService, gaugeService, config);
        processor.setMetricsPrefixGenerator(p -> "prefix");
        processor.start(); //file is not yet created
        TimeUnit.SECONDS.sleep(2); // wait for the processor to initialize and start scanning.

        writeToFile(); //write the file
        TimeUnit.SECONDS
                .sleep(3); //wait for the processor to process the lines, 3 secs should do, if not please increase.
        Mockito.verify(counterService, Mockito.times(2)).increment("test");

        processor.destroy();
    }

    private void writeToFile() {
        //create a file
        try {
            File temp = new File("./tailer-test.log");
            temp.createNewFile();
            temp.deleteOnExit();

            //write it
            BufferedWriter bw = new BufferedWriter(new FileWriter(temp));
            bw.write("START: This is the temporary file content\n");
            bw.flush();
            TimeUnit.MILLISECONDS.sleep(500); //here we should have the file ready to be read by the tailer.
            //The tailer starts from the end of the file so it must find it before we start writing any content to it for this test.
            bw.write("This is the ERR temporary file content\n");
            bw.write("This is the temporary  ERR file content\n");
            bw.write("This is the temporary file ERR content dont count\n");
            bw.write("This is the value=20 temporary file content\n");
            bw.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
