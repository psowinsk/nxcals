/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.client.demo;

import cern.nxcals.client.Publisher;
import cern.nxcals.client.PublisherFactory;
import cern.nxcals.common.avro.BytesToGenericRecordDecoder;
import cern.nxcals.ds.cmw.converter.ApvToDataConverter;
import cern.japc.AcquiredParameterValue;
import cern.japc.Parameter;
import cern.japc.ParameterException;
import cern.japc.ParameterValueListener;
import cern.japc.SubscriptionHandle;
import cern.japc.factory.ParameterFactory;
import cern.japc.spi.SelectorImpl;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static java.util.Arrays.asList;

/**
 * Demonstrates data flow japc -> cals ingestion API -> kafka -> consumer.
 *
 * @author Marcin Sobieszek
 * @date Jul 18, 2016 5:31:08 PM
 */
public class PutAndGetFromKafka {
    private static final Logger LOGGER = LoggerFactory.getLogger(PutAndGetFromKafka.class);
    private final Set<SubscriptionHandle> handlers = new HashSet<>();

    public static void main(String[] args) throws Exception {
        PutAndGetFromKafka s = new PutAndGetFromKafka();
        s.subscribe();
        System.exit(1);
    }

    private void subscribe() throws Exception {
        ParameterFactory parameterFactory = ParameterFactory.newInstance();
        List<String> params = Arrays.asList("LARGER/loggingBct3");
        List<String> selectors = Arrays.asList("SPS.USER.ALL");
        JapcListener listener = new JapcListener();
        for (int i = 0; i < params.size(); ++i) {
            Parameter parameter = parameterFactory.newParameter(params.get(i));
            SubscriptionHandle handle = parameter.createSubscription(new SelectorImpl(selectors.get(i)), listener);
            LOGGER.info("Starting subscription for {}", params.get(i));
            handle.startMonitoring();
            handlers.add(handle);
        }
        SimpleKafkaConsumer test = new SimpleKafkaConsumer();
        test.start();
        TimeUnit.SECONDS.sleep(100);
        this.handlers.stream().forEach(SubscriptionHandle::stopMonitoring);
        listener.stop();
        test.shutdown();
        LOGGER.info("Stopped monitoring");
    }

    private static class JapcListener implements ParameterValueListener {
        private final Publisher<AcquiredParameterValue> producer = PublisherFactory.createPublisher("MOCK-SYSTEM",
                ApvToDataConverter.get());

        @Override
        public void valueReceived(String parameterName, AcquiredParameterValue value) {
            LOGGER.info("Sending data to cals {}", value);
            this.producer.publish(value);
            LOGGER.info("Data to cals sent");
        }

        @Override
        public void exceptionOccured(String parameterName, String _description, ParameterException exception) {
            LOGGER.error("Exception for {}", parameterName, exception);
        }

        public void stop() throws IOException {
            this.producer.close();
        }
    }

    private static class SimpleKafkaConsumer {
        private final ExecutorService executor = Executors.newFixedThreadPool(1);
        private final Task task = new Task();

        public void start() {
            LOGGER.info("Starting consumer thread");
            this.executor.submit(this.task);
        }

        public void shutdown() {
            this.task.stop = true;
            this.task.consumer.close();
            this.executor.shutdown();
        }

        private static class Task implements Runnable {
            private final BytesToGenericRecordDecoder decoder = new BytesToGenericRecordDecoder();
            private final KafkaConsumer<byte[], byte[]> consumer = new KafkaConsumer<>(getConsumerProps());
            private volatile boolean stop;

            @Override
            public void run() {
                // TODO: this works as long as we subscribe to the given device and property
                String topic = "BCTSDC-Acquisition";
                List<TopicPartition> partitions = asList(new TopicPartition(topic, 0), new TopicPartition(topic, 1));
                this.consumer.assign(partitions);
                try {
                    while (!stop) {
                        LOGGER.info("Querying topic {} with partitions {}", topic, partitions);
                        ConsumerRecords<byte[], byte[]> data = consumer.poll(200);
                        LOGGER.info("Got data size = {}", data.count());
                        data.forEach(record -> {
                            GenericRecord r = this.decoder.apply(record.value());
                            LOGGER.info("Decoded record from Kafka {}", r);
                        });
                        TimeUnit.SECONDS.sleep(5);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            // TODO: would be good to take kafka/zk coordinates from the client configuration
            private static Properties getConsumerProps() {
                Properties props = new Properties();
                props.put("bootstrap.servers", "137.138.166.238:9092");
                props.put("group.id", "test");
                props.put("enable.auto.commit", "false");
                props.put("session.timeout.ms", "30000");
                props.put("key.deserializer", "org.apache.kafka.common.serialization.ByteArrayDeserializer");
                props.put("value.deserializer", "org.apache.kafka.common.serialization.ByteArrayDeserializer");
                return props;
            }
        }
    }

}
