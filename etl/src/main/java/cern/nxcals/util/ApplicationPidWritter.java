package cern.nxcals.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.lang.management.ManagementFactory;
import java.nio.file.Paths;

/**
 * Created by msobiesz on 01/12/16.
 */
public final class ApplicationPidWritter {
    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationPidWritter.class);
    private static final String DEFAULT_FILE_NAME = "application.pid";

    public static void writePid() {
        writePidAt(System.getProperty("user.dir"), DEFAULT_FILE_NAME);
    }

    public static void writePidAt(String location, String fileName) {
        File file = new File(Paths.get(location, fileName).toString());
        file.deleteOnExit();
        try (FileWriter writer = new FileWriter(file)) {
            String pid = getPid();
            LOGGER.info("Storing pid {} under {}", pid, file.getAbsolutePath());
            writer.append(pid);
        } catch (IOException e) {
            LOGGER.error("Wrror while writting a file {}", e);
            throw new UncheckedIOException(e);
        }
    }

    private static String getPid() {
        try {
            return ManagementFactory.getRuntimeMXBean().getName().split("@")[0];
        } catch (Throwable t) {
            throw new RuntimeException("No PID available");
        }
    }
}
