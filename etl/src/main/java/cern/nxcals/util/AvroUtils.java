package cern.nxcals.util;

import org.apache.avro.Schema;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.io.BinaryEncoder;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.io.EncoderFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by msobiesz on 29/11/16.
 */
public final class AvroUtils {

    private AvroUtils() {
        throw new RuntimeException("No instancess allowed");
    }

    public static byte[] recordToByteArray(Object record, Schema schema) {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            BinaryEncoder encoder = EncoderFactory.get().directBinaryEncoder(out, null);
            DatumWriter<Object> writer = new GenericDatumWriter<>(schema);
            writer.write(record, encoder);
            return out.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException("Exception while creating byte array", e);
        }
    }

    public static byte[] toBytes(Schema schema, Object value) {
        if (value == null) {
            return null;
        }
        return AvroUtils.recordToByteArray(value, schema);
    }

}
