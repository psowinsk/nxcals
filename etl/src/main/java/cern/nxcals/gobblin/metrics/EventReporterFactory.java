package cern.nxcals.gobblin.metrics;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.ScheduledReporter;
import gobblin.metrics.CustomCodahaleReporterFactory;
import gobblin.metrics.GobblinTrackingEvent;
import gobblin.metrics.RootMetricContext;
import gobblin.metrics.reporter.EventReporter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Queue;
import java.util.function.Consumer;

import static org.apache.commons.collections.CollectionUtils.isEmpty;

/**
 * Created by msobiesz on 10/05/17.
 */
public class EventReporterFactory implements CustomCodahaleReporterFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(EventReporterFactory.class);
    private static Map<String, Collection<Consumer<Map<String, String>>>> listeners = new HashMap<>();

    @Override
    public ScheduledReporter newScheduledReporter(MetricRegistry registry, Properties properties) throws IOException {
        LOGGER.info("Created simple event reporter");
        return new SimpleEventReporter();
    }

    // there's no way to obtain a reference to the event reporter so we have to resort to using statics ... argh!
    static void addListener(String eventName, Consumer<Map<String, String>> listener) {
        LOGGER.debug("Adding listener {} for event {}", listener, eventName);
        listeners.computeIfAbsent(eventName, k -> new ArrayList<>()).add(listener);
    }

    static void removeListener(String eventName, Consumer<Map<String, String>> listener) {
        LOGGER.debug("Removing listener {} for event {}", listener, eventName);
        Collection<Consumer<Map<String, String>>> consumers = listeners.get(eventName);
        if (!isEmpty(consumers)) {
            consumers.remove(listener);
        }
    }

    private static class SimpleEventReporter extends EventReporter {

        public SimpleEventReporter() {
            this(new BuilderImpl());
        }

        public SimpleEventReporter(Builder builder) {
            super(builder);
        }

        @Override
        public void reportEventQueue(Queue<GobblinTrackingEvent> queue) {
            if (queue.size() <= 0) {
                LOGGER.info("Obtained empty queue");
                return;
            }
            GobblinTrackingEvent nextEvent;
            while ((nextEvent = queue.poll()) != null) {
                Collection<Consumer<Map<String, String>>> consumers = listeners.get(nextEvent.getName());
                if (!isEmpty(consumers)) {
                    for (Consumer<Map<String, String>> consumer : consumers) {
                        consumer.accept(nextEvent.getMetadata());
                    }
                }
            }
        }
    }

    private static class BuilderImpl extends EventReporter.Builder<BuilderImpl> {
        protected BuilderImpl() {
            super(RootMetricContext.get());
        }

        @Override
        protected BuilderImpl self() {
            return this;
        }
    }

}

