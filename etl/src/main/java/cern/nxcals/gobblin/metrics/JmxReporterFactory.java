package cern.nxcals.gobblin.metrics;

import com.codahale.metrics.Counter;
import com.codahale.metrics.Gauge;
import com.codahale.metrics.Histogram;
import com.codahale.metrics.JmxReporter;
import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricFilter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.ScheduledReporter;
import com.codahale.metrics.Timer;
import gobblin.metrics.CustomCodahaleReporterFactory;
import gobblin.metrics.metric.filter.MetricNameRegexFilter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.Instant;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;

import static cern.nxcals.gobblin.metrics.EventReporterFactory.addListener;
import static cern.nxcals.gobblin.metrics.EventReporterFactory.removeListener;
import static gobblin.metrics.event.TimingEvent.LauncherTimings.JOB_COMPLETE;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;

/**
 * Created by msobiesz on 06/04/17.
 */
public class JmxReporterFactory implements CustomCodahaleReporterFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(JmxReporterFactory.class);

    @Override
    public ScheduledReporter newScheduledReporter(MetricRegistry metricRegistry, Properties properties)
            throws IOException {
        return new JmxScheduledReporter(metricRegistry, "jmxScheduledReporter", this.getMetricFilter(), SECONDS,
                MILLISECONDS);
    }

    private MetricFilter getMetricFilter() {
        return new MetricNameRegexFilter("^JOB.*|^gobblin.writer.*|^gobblin.extractor.*");
    }

    private static class JmxScheduledReporter extends ScheduledReporter {
        private static final String COUNTER_JOB_PREFIX = "JOB.";
        private static final String JMX_METRIC_JOB_PREFIX = "gobblin.job";
        private static final String RECORDS_SUFFIX = ".records";
        private static final String BYTES_SUFFIX = ".bytes";
        private final Consumer<Map<String, String>> eventListener = this::onEvent;
        private final MetricRegistry registry = new MetricRegistry();
        private final Map<String, DynamicGauge> metrics = new HashMap<>();
        private final Set<String> completedJobs = new HashSet<>();
        private final JmxReporter jmxReporter;
        private SortedMap<String, MetricWrapper> toPublish;
        private String lastPublishedJob;

        protected JmxScheduledReporter(MetricRegistry registry, String name, MetricFilter filter, TimeUnit rateUnit,
                TimeUnit durationUnit) {
            super(registry, name, filter, rateUnit, durationUnit);
            this.jmxReporter = JmxReporter.forRegistry(this.registry).convertRatesTo(TimeUnit.SECONDS)
                    .convertDurationsTo(TimeUnit.MILLISECONDS).build();
            this.jmxReporter.start();
            addListener(JOB_COMPLETE, this.eventListener);
        }

        @Override
        public synchronized void report(SortedMap<String, Gauge> gauges, SortedMap<String, Counter> counters,
                SortedMap<String, Histogram> historgrams, SortedMap<String, Meter> metters,
                SortedMap<String, Timer> timers) {
            LOGGER.trace("Collecting metrics for counters: {} and metters: {}", counters, metters);
            this.registerCounters(counters);
            this.registerMeters(metters);
        }

        @Override
        public void close() {
            removeListener(JOB_COMPLETE, this.eventListener);
            super.close();
        }

        private synchronized void onEvent(Map<String, String> metadata) {
            String jobId = metadata.get("jobId");
            if (StringUtils.isEmpty(jobId)) {
                LOGGER.warn("Cannot obtain job id from event {} metadata {}", JOB_COMPLETE, metadata);
                return;
            }
            Instant timestamp = Instant.now();
            if (!isEmpty(this.toPublish)) {
                MetricWrapper metric = this.toPublish.get(jobId);
                // due to asynchronous nature of metric/event update we can publish here only the metric which has just
                // been populated not the partial ones
                if (metric != null && timestamp.getEpochSecond() - metric.timestamp.getEpochSecond() <= 1) {
                    this.publishMetric(jobId, metric);
                    this.toPublish.remove(jobId);
                    this.lastPublishedJob = jobId;
                    return;
                }
            }
            this.completedJobs.add(jobId);
        }

        private void registerMeters(SortedMap<String, Meter> meters) {
            if (this.isEmpty(meters)) {
                LOGGER.debug("No meter metrics recorded");
                return;
            }
            meters.entrySet().stream().filter(e -> e.getKey().startsWith("gobblin"))
                    .forEach(e -> this.registerMetric(e.getKey(), e.getValue().getCount()));
        }

        private void registerCounters(SortedMap<String, Counter> counters) {
            if (this.isEmpty(counters)) {
                LOGGER.debug("No counter metrics recorded");
                return;
            }
            Instant timestamp = Instant.now();
            SortedMap<String, MetricWrapper> countersByJob = new TreeMap(counters.entrySet().stream().collect(
                    groupingBy(e -> e.getKey().replace(COUNTER_JOB_PREFIX, "").replace(RECORDS_SUFFIX, "")
                            .replace(BYTES_SUFFIX, ""), toMap(Entry::getKey, Entry::getValue))).entrySet().stream()
                    .collect(toMap(Entry::getKey, e -> new MetricWrapper(e.getValue(), timestamp))));
            // tailMap's form param is inclusive
            if (this.lastPublishedJob != null) {
                countersByJob.remove(this.lastPublishedJob);
            }
            this.toPublish =
                    this.lastPublishedJob == null ? countersByJob : countersByJob.tailMap(this.lastPublishedJob);

            toPublish.forEach((jobId, metric) -> {
                if (this.completedJobs.contains(jobId)) {
                    this.publishMetric(jobId, metric);
                    this.completedJobs.remove(jobId);
                    this.lastPublishedJob = jobId;
                }
            });
        }

        private void publishMetric(String jobId, MetricWrapper metric) {
            LOGGER.debug("Publishing metric for jobId {}", jobId);
            Counter records = metric.counters.get(COUNTER_JOB_PREFIX + jobId + RECORDS_SUFFIX);
            if (records != null) {
                this.registerMetric(JMX_METRIC_JOB_PREFIX + RECORDS_SUFFIX, records.getCount());
            }
            Counter bytes = metric.counters.get(COUNTER_JOB_PREFIX + jobId + BYTES_SUFFIX);
            if (bytes != null) {
                this.registerMetric(JMX_METRIC_JOB_PREFIX + BYTES_SUFFIX, bytes.getCount());
            }
        }

        private void registerMetric(String name, long value) {
            DynamicGauge metric = this.metrics.computeIfAbsent(name, k -> new DynamicGauge(new AtomicLong(0)));
            metric.value.set(value);
            if (!this.registry.getNames().contains(name)) {
                this.registry.register(name, metric);
            }
        }

        private <K, V> boolean isEmpty(SortedMap<K, V> map) {
            return map == null || map.isEmpty();
        }
    }

    private static class DynamicGauge implements Gauge<Long> {
        private final AtomicLong value;

        DynamicGauge(AtomicLong value) {
            this.value = value;
        }

        @Override
        public Long getValue() {
            return value.get();
        }
    }

    private static class MetricWrapper {
        private final Map<String, Counter> counters;
        private final Instant timestamp;

        public MetricWrapper(Map<String, Counter> counters, Instant timestamp) {
            this.counters = counters;
            this.timestamp = timestamp;
        }

        @Override
        public String toString() {
            return "MetricWrapper{" + "counters=" + counters + ", timestamp=" + timestamp + '}';
        }
    }

}
