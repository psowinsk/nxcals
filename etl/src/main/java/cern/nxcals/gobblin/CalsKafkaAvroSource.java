/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.gobblin;

import gobblin.configuration.WorkUnitState;
import gobblin.source.extractor.Extractor;
import gobblin.source.extractor.extract.kafka.KafkaSource;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericRecord;

import java.io.IOException;

/**
 * @author Marcin Sobieszek
 * @date Aug 2, 2016 3:25:59 PM
 */
public class CalsKafkaAvroSource extends KafkaSource<Schema, GenericRecord> {

    @Override
    public Extractor<Schema, GenericRecord> getExtractor(WorkUnitState state) throws IOException {
        return new CalsKafkaAvroExtractor(state);
    }

}
