/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.gobblin;

import cern.nxcals.common.avro.BytesToGenericRecordDecoder;
import cern.nxcals.service.client.providers.InternalServiceClientFactory;
import com.google.common.base.Optional;
import gobblin.configuration.WorkUnitState;
import gobblin.kafka.client.ByteArrayBasedKafkaRecord;
import gobblin.source.extractor.extract.kafka.KafkaAvroExtractor;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.Decoder;

import java.io.IOException;

/**
 * @author Marcin Sobieszek
 * @date Aug 2, 2016 3:23:07 PM
 */
public class CalsKafkaAvroExtractor extends KafkaAvroExtractor<String> {
    private final BytesToGenericRecordDecoder decoder = new BytesToGenericRecordDecoder(
            id -> InternalServiceClientFactory.createSchemaService().findById(id));

    public CalsKafkaAvroExtractor(WorkUnitState state) {
        super(state);
    }

    @Override
    protected Schema getRecordSchema(byte[] payload) {
        throw new RuntimeException("Not supported");
    }

    @Override
    protected Decoder getDecoder(byte[] payload) {
        throw new RuntimeException("Not supported");
    }

    @Override
    protected GenericRecord decodeRecord(ByteArrayBasedKafkaRecord messageAndOffset) throws IOException {
        byte[] payload = messageAndOffset.getMessageBytes();
        return this.decoder.apply(payload);
    }

    @Override
    protected Optional<Schema> getExtractorSchema() {
        return Optional.of(DEFAULT_SCHEMA);
    }

}
