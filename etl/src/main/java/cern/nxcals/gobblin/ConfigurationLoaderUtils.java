/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.gobblin;

import org.apache.commons.configuration.CombinedConfiguration;
import org.apache.commons.configuration.ConfigurationConverter;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.SystemConfiguration;
import org.apache.commons.configuration.tree.MergeCombiner;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;
import java.util.Properties;

/**
 * Allows to load the configuration in a specific order:
 * <ul>
 * <li>System properties
 * <li>Specific file properties
 * <li>Generic file properties
 * </ul>
 *
 * @author jwozniak
 */
class ConfigurationLoaderUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationLoaderUtils.class);

    private ConfigurationLoaderUtils() {
    }

    static Properties loadProperties(String defaultConf, Optional<String> customConf) {
        try {
            CombinedConfiguration combinedConfig = new CombinedConfiguration();
            combinedConfig.setNodeCombiner(new MergeCombiner());

            // order of adding matters - the system takes precedence, next the specific, finally the general config
            combinedConfig.addConfiguration(new SystemConfiguration());
            if (customConf.isPresent()) {
                combinedConfig.addConfiguration(new PropertiesConfiguration(customConf.get()));
            }
            if (!StringUtils.isEmpty(defaultConf)) {
                combinedConfig.addConfiguration(new PropertiesConfiguration(defaultConf));
            }

            // Load default framework configuration properties
            Properties properties = ConfigurationConverter.getProperties(combinedConfig);
            printProperties(properties);

            return properties;
        } catch (Exception ex) {
            throw new RuntimeException("Cannot load properties", ex);
        }
    }

    private static void printProperties(Properties properties) {
        LOGGER.debug("All properties:");
        properties.entrySet().stream()
                .forEach(entry -> LOGGER.debug("Property {}={}", entry.getKey(), entry.getValue()));
    }

}
