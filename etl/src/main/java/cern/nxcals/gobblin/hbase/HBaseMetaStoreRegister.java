/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.gobblin.hbase;

import cern.nxcals.common.concurrent.AutoCloseableLock;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableExistsException;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static org.apache.hadoop.hbase.io.compress.Compression.Algorithm.SNAPPY;
import static org.apache.hadoop.hbase.io.encoding.DataBlockEncoding.FAST_DIFF;

/**
 * @author Marcin Sobieszek
 * @date Nov 1, 2016 4:33:11 PM
 */
public final class HBaseMetaStoreRegister {
    public static final String CF_DEFAULT = "data";
    public static final Duration TTL = Duration.of(5, ChronoUnit.DAYS);
    private static final ConcurrentMap<String, HBaseMetaStoreRegister> cache = new ConcurrentHashMap<>();
    private static final Logger LOGGER = LoggerFactory.getLogger(HBaseMetaStoreRegister.class);
    private final ConcurrentMap<String, TableName> createdTables = new ConcurrentHashMap<>();
    private final TableLock locks = new TableLock();
    private final HBaseProps props;

    private HBaseMetaStoreRegister(HBaseProps props) {
        this.props = props;
    }

    public static HBaseMetaStoreRegister get(HBaseProps props) {
        return cache.computeIfAbsent(props.id(), k -> new HBaseMetaStoreRegister(props));
    }

    public String createTableIfNeeded(String name, String namespace) {
        if (this.createdTables.containsKey(name)) {
            return this.createdTables.get(name).getNameAsString();
        }
        TableName tableName = TableName.valueOf(namespace, name);
        try (AutoCloseableLock lock = this.locks.getTableLockFor(name);
             Admin admin = HBaseConnectionPool.getConnection(this.props).getAdmin()) {
            if (admin.tableExists(tableName)) {
                LOGGER.debug("Table {} exists", tableName);
                return tableName.getNameAsString();
            }
            LOGGER.info("Table {} does not exist, so creating one.", tableName);
            HTableDescriptor descriptor = this.prepareTableFor(tableName);
            admin.createTable(descriptor);
            this.createdTables.put(name, tableName);
            LOGGER.info("Table {} has been created.", tableName);
            return tableName.getNameAsString();
        } catch (TableExistsException e) {
            LOGGER.error("Table {} has been meanwhile created, this must not happen as it means a developer error!",
                    name);
            return tableName.getNameAsString();
        } catch (IOException e) {
            LOGGER.error("Exception while registering new HBase table", e);
            throw new UncheckedIOException(e);
        }
    }

    private HTableDescriptor prepareTableFor(TableName tableName) {
        HTableDescriptor table = new HTableDescriptor(tableName);
        table.setConfiguration("hbase.table.sanity.checks", "false");
        HColumnDescriptor cDesc = new HColumnDescriptor(CF_DEFAULT);
        cDesc.setCompressionType(SNAPPY);
        cDesc.setDataBlockEncoding(FAST_DIFF);
        cDesc.setTimeToLive((int) TTL.get(ChronoUnit.SECONDS));
        table.addFamily(cDesc);
        return table;
    }
}
