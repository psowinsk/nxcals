/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.gobblin.hbase;

import cern.nxcals.common.concurrent.AutoCloseableLock;
import com.google.common.util.concurrent.Striped;
import org.apache.commons.lang3.StringUtils;

import java.util.concurrent.locks.Lock;

/**
 * @author Marcin Sobieszek
 * @date Nov 1, 2016 11:54:38 AM
 */
class TableLock {
    private final Striped<Lock> locks = Striped.lazyWeakLock(1000);

    public AutoCloseableLock getTableLockFor(String tableName) {
        if (StringUtils.isEmpty(tableName)) {
            throw new RuntimeException("Table name must not be empty");
        }
        return AutoCloseableLock.getFor(this.locks.get(tableName));
    }

}
