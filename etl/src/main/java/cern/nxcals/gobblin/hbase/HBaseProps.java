/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.gobblin.hbase;

import gobblin.configuration.State;
import org.apache.commons.lang3.StringUtils;

import java.util.EnumMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Marcin Sobieszek
 * @date Nov 2, 2016 11:13:49 AM
 */
public class HBaseProps {
    public enum Conf {
        /**
         * @formatter:off
         */
        ZOOKEPER_QUORUM("hbase.zookeeper.quorum"),
        SECURITY_AUTH("hbase.security.authentication"),
        MASTER_PRINCIPAL("hbase.master.kerberos.principal"),
        REGION_PRINCIPAL("hbase.regionserver.kerberos.principal"),
        HBASE_CLIENT_BUFFER("hbase.client.write.buffer"),
        ;
        /**
         * @formatter:on
         */

        private final String param;

        private Conf(String param) {
            this.param = param;
        }

        public String getParam() {
            return this.param;
        }
    }

    private final Map<Conf, String> props = new EnumMap<>(Conf.class);

    public HBaseProps(State state) {
        for (Conf c : Conf.values()) {
            String prop = state.getProp(c.getParam());
            if (!StringUtils.isEmpty(prop)) {
                this.props.put(c, prop);
            }
        }
    }

    public String getPropOf(Conf conf) {
        return this.props.get(conf);
    }

    public String id() {
        return this.props.values().stream().collect(Collectors.joining("#"));
    }

}
