/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.gobblin.hbase;

import cern.nxcals.gobblin.hbase.HBaseProps.Conf;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * TOOD: close the cached connections on system shutdown
 *
 * @author Marcin Sobieszek
 * @date Nov 1, 2016 4:32:16 PM
 */
@Slf4j
public final class HBaseConnectionPool {
    private static final ConcurrentMap<String, Connection> cache = new ConcurrentHashMap<>();

    public static Connection getConnection(HBaseProps props) {
        return cache.computeIfAbsent(props.id(), k -> createConnection(props));
    }

    private static Connection createConnection(HBaseProps props) {
        try {
            return ConnectionFactory.createConnection(getConfig(props));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public static Configuration getConfig(HBaseProps props) {
        Configuration config = HBaseConfiguration.create();
        for (Conf c : Conf.values()) {
            String prop = props.getPropOf(c);
            if (!StringUtils.isEmpty(prop)) {
                log.info("Setting {} to {}", c.getParam(), prop);
                config.set(c.getParam(), prop);
            }
        }
        return config;
    }

}
