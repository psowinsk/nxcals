/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.gobblin.writer.partitioner;

import cern.nxcals.common.domain.SystemData;
import cern.nxcals.service.client.api.SystemService;
import cern.nxcals.service.client.providers.ServiceClientFactory;
import gobblin.configuration.State;
import gobblin.writer.partitioner.WriterPartitioner;
import org.apache.avro.Schema;
import org.apache.avro.Schema.Field;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.concurrent.TimeUnit;

import static cern.nxcals.common.Schemas.PARTITION_ID;
import static cern.nxcals.common.Schemas.SCHEMA_ID;
import static cern.nxcals.common.Schemas.SYSTEM_ID;
import static gobblin.util.DatePartitionType.DAY;
import static org.apache.avro.Schema.Type.LONG;
import static org.apache.avro.Schema.Type.STRING;
import static org.apache.avro.Schema.create;
import static org.apache.avro.SchemaBuilder.record;

/**
 * Partitions cals data accordingly to {@code systemId/partitionId/schemaId/date}
 *
 * @author Marcin Sobieszek
 * @date Aug 2, 2016 4:19:48 PM
 */
public class CalsPartitioner implements WriterPartitioner<GenericRecord> {
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern("yyyy-MM-dd");
    private static final String JOB_SYSTEM_NAME_PROP = "job.nxcals.system.name";
    private static final String PARTITION = PARTITION_ID.getFieldName();
    private static final String SCHEMA = SCHEMA_ID.getFieldName();
    private static final String SYSTEM = SYSTEM_ID.getFieldName();
    private static final Schema DEFAULT_SCHEMA = record("partition").namespace("cern.cals.partition").fields()
            .name(SYSTEM).type(create(LONG)).noDefault().name(PARTITION).type(create(LONG)).noDefault().name(SCHEMA)
            .type(create(LONG)).noDefault().name(DAY.toString()).type(create(STRING)).noDefault().endRecord();
    private final SystemService systemProvider = ServiceClientFactory.createSystemService();
    private final String systemName;
    private String timestampField;

    public CalsPartitioner(State state, int numBranches, int branchId) {
        this.systemName = state.getProp(JOB_SYSTEM_NAME_PROP, StringUtils.EMPTY);
        if (StringUtils.isEmpty(this.systemName)) {
            throw new IllegalStateException("Missing system configuration property " + JOB_SYSTEM_NAME_PROP);
        }
    }

    @Override
    public Schema partitionSchema() {
        return DEFAULT_SCHEMA;
    }

    @Override
    public GenericRecord partitionForRecord(GenericRecord record) {
        GenericRecord partition = new GenericData.Record(this.partitionSchema());
        partition.put(SYSTEM, record.get(SYSTEM));
        partition.put(PARTITION, record.get(PARTITION));
        partition.put(SCHEMA, record.get(SCHEMA));
        Object time = record.get(this.getSystemTimestamp());
        DateTime partTime = time != null ? new DateTime(TimeUnit.NANOSECONDS.toMillis((long) time)) : new DateTime();
        partition.put(DAY.toString(), DATE_FORMATTER.print(partTime.withZone(DateTimeZone.UTC)));
        return partition;
    }

    private String getSystemTimestamp() {
        if (StringUtils.isEmpty(this.timestampField)) {
            SystemData systemData = this.systemProvider.findByName(this.systemName);
            Schema timeKyeDefSchema = new Schema.Parser().parse(systemData.getTimeKeyDefinitions());
            this.timestampField = timeKyeDefSchema.getFields().stream().map(Field::name).findFirst()
                    .orElseThrow(() -> new RuntimeException("Cannot obtain time schema for system " + this.systemName));
        }
        return this.timestampField;
    }
}
