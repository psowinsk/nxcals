/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.gobblin.writer;

import gobblin.writer.DataWriter;
import gobblin.writer.PartitionAwareDataWriterBuilder;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericRecord;

import java.io.IOException;

public class HBaseDataWriterBuilder extends PartitionAwareDataWriterBuilder<Schema, GenericRecord> {

    @Override
    public boolean validatePartitionSchema(Schema partitionSchema) {
        return true;
    }

    @Override
    public DataWriter<GenericRecord> build() throws IOException {
        return new HBaseDataWriter(this.partition, this.destination.getProperties());
    }

}
