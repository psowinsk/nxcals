/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.gobblin.writer;

import cern.nxcals.common.domain.SystemData;
import cern.nxcals.gobblin.hbase.HBaseConnectionPool;
import cern.nxcals.gobblin.hbase.HBaseMetaStoreRegister;
import cern.nxcals.gobblin.hbase.HBaseProps;
import cern.nxcals.service.client.api.SystemService;
import cern.nxcals.service.client.providers.ServiceClientFactory;
import com.google.common.base.Joiner;
import com.google.common.base.Optional;
import gobblin.configuration.State;
import gobblin.writer.DataWriter;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Schema;
import org.apache.avro.Schema.Field;
import org.apache.avro.generic.GenericRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.BufferedMutator;
import org.apache.hadoop.hbase.client.Put;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import static cern.nxcals.common.SystemFields.NXC_ENTITY_ID;
import static cern.nxcals.common.SystemFields.NXC_PARTITION_ID;
import static cern.nxcals.common.SystemFields.NXC_SCHEMA_ID;
import static cern.nxcals.common.SystemFields.NXC_SYSTEM_ID;
import static cern.nxcals.gobblin.hbase.HBaseMetaStoreRegister.CF_DEFAULT;
import static cern.nxcals.gobblin.hbase.HBaseMetaStoreRegister.TTL;
import static cern.nxcals.util.AvroUtils.toBytes;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.apache.hadoop.hbase.util.Bytes.toBytes;

@Slf4j
public class HBaseDataWriter implements DataWriter<GenericRecord> {
    private static final Joiner JOINER = Joiner.on("__").skipNulls();
    private static final byte[] COLUMN_FAMILY = toBytes(CF_DEFAULT);
    private static final String JOB_SYSTEM_NAME_PROP = "job.nxcals.system.name";
    private static final String JOB_HBASE_NAMESPACE = "hbase.namespace";
    private static final AtomicReference<HBaseProps> HBASE_PROPS = new AtomicReference<>();
    private final SystemService systemProvider = ServiceClientFactory.createSystemService();
    private final AtomicLong recordCounter = new AtomicLong();
    private final AtomicLong sizeCounter = new AtomicLong();
    private final AtomicLong rejectedCounter = new AtomicLong();
    private final AtomicLong writeTime = new AtomicLong();
    private final AtomicLong putTime = new AtomicLong();
    private final BufferedMutator mutator;
    private final String timestampField;

    public HBaseDataWriter(Optional<GenericRecord> partition, State state) throws IOException {
        if (!partition.isPresent()) {
            throw new RuntimeException("Partition is required");
        }
        this.initializePropsIfNeeded(state);

        String hbNs = state.getProp(JOB_HBASE_NAMESPACE) != null ? state.getProp(JOB_HBASE_NAMESPACE) : "";
        String name = this.getTableNameFrom(partition.get());

        String tableName = HBaseMetaStoreRegister.get(HBASE_PROPS.get()).createTableIfNeeded(name, hbNs);
        this.mutator = HBaseConnectionPool.getConnection(HBASE_PROPS.get())
                .getBufferedMutator(TableName.valueOf(tableName));

        String systemName = state.getProp(JOB_SYSTEM_NAME_PROP, StringUtils.EMPTY);
        if (StringUtils.isEmpty(systemName)) {
            throw new IllegalStateException("Missing system configuration property " + JOB_SYSTEM_NAME_PROP);
        }
        this.timestampField = this.getSystemTimestamp(systemName);
    }

    private void initializePropsIfNeeded(State state) {
        if (HBASE_PROPS.get() == null) {
            synchronized (HBASE_PROPS) {
                if (HBASE_PROPS.get() == null) {
                    HBASE_PROPS.set(new HBaseProps(state));
                }
            }
        }
    }

    private String getTableNameFrom(GenericRecord _partition) {
        return JOINER.join(_partition.get(NXC_SYSTEM_ID.getValue()), _partition.get(NXC_PARTITION_ID.getValue()),
                _partition.get(NXC_SCHEMA_ID.getValue()));
    }

    private Put convertToPut(GenericRecord record) {
        long start = System.currentTimeMillis();
        long timestampNanos = (long) record.get(this.timestampField);
        if (!this.isRecordAcceptableFor(timestampNanos)) {
            return null;
        }
        Schema schema = record.getSchema();
        long key = Long.MAX_VALUE - timestampNanos;

        String rowKey = JOINER.join(record.get(NXC_ENTITY_ID.getValue()).toString(), key);
        byte[] keyAsBytes = toBytes(rowKey);
        Put put = new Put(keyAsBytes);

        for (Field f : schema.getFields()) {
            Object object = record.get(f.name());
            put.addColumn(COLUMN_FAMILY, toBytes(f.name()), toBytes(f.schema(), object));
        }
        putTime.addAndGet(System.currentTimeMillis() - start);
        return put;
    }

    private boolean isRecordAcceptableFor(long timestampNanos) {
        return timestampNanos > (MILLISECONDS.toNanos(System.currentTimeMillis()) - TTL.toNanos());
    }

    private String getSystemTimestamp(String systemName) {
        SystemData systemData = this.systemProvider.findByName(systemName);
        Schema timeKyeDefSchema = new Schema.Parser().parse(systemData.getTimeKeyDefinitions());
        return timeKyeDefSchema.getFields().stream().map(Field::name).findFirst()
                .orElseThrow(() -> new RuntimeException("Cannot obtain time schema for system " + systemName));
    }

    @Override
    public void close() throws IOException {
        log.info(
                "Closing mutator to {}, records written={}, records rejected={}, time spent in put={}, " + "writing={}",
                this.mutator.getName(), this.recordsWritten(), this.rejectedCounter.get(), this.putTime.get(),
                this.writeTime.get());
        long start = System.currentTimeMillis();
        this.mutator.close();
        log.info("Close took: {}", System.currentTimeMillis() - start);
    }

    @Override
    public void write(GenericRecord record) throws IOException {
        Put put = this.convertToPut(record);
        long start = System.currentTimeMillis();
        if (put != null) {
            this.recordCounter.incrementAndGet();
            this.sizeCounter.addAndGet(put.getRow().length);
            this.mutator.mutate(put);
        } else {
            this.rejectedCounter.incrementAndGet();
        }
        this.writeTime.addAndGet(System.currentTimeMillis() - start);
    }

    @Override
    public void commit() throws IOException {
        long start = System.currentTimeMillis();
        this.mutator.flush();
        log.info("Flushing to {} took {} ms", this.mutator.getName(), (System.currentTimeMillis() - start));
    }

    @Override
    public void cleanup() throws IOException {
        this.mutator.close();
        this.writeTime.set(0);
        this.putTime.set(0);
        this.rejectedCounter.set(0);
        this.recordCounter.set(0);
        this.sizeCounter.set(0);
    }

    @Override
    public long recordsWritten() {
        return this.recordCounter.get();
    }

    @Override
    public long bytesWritten() {
        return this.sizeCounter.get();
    }

}
