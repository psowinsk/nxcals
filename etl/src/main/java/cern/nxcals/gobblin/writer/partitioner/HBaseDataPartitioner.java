/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.gobblin.writer.partitioner;

import gobblin.configuration.State;
import gobblin.writer.partitioner.WriterPartitioner;
import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;

import static cern.nxcals.common.Schemas.PARTITION_ID;
import static cern.nxcals.common.Schemas.SCHEMA_ID;
import static cern.nxcals.common.Schemas.SYSTEM_ID;
import static cern.nxcals.common.SystemFields.NXC_PARTITION_ID;
import static cern.nxcals.common.SystemFields.NXC_SCHEMA_ID;
import static cern.nxcals.common.SystemFields.NXC_SYSTEM_ID;

/**
 * @author Marcin Sobieszek
 * @date Apr 22, 2016 2:34:54 PM
 */
public class HBaseDataPartitioner implements WriterPartitioner<GenericRecord> {
    private static final Schema SCHEMA = SchemaBuilder.record("HbasePartition").namespace("cern.nxcals.hbase.partition")
            .fields().name(NXC_SYSTEM_ID.getValue()).type(SYSTEM_ID.getSchema()).noDefault()
            .name(NXC_PARTITION_ID.getValue()).type(PARTITION_ID.getSchema()).noDefault().name(NXC_SCHEMA_ID.getValue())
            .type(SCHEMA_ID.getSchema()).noDefault().endRecord();

    public HBaseDataPartitioner(State state, int numBranches, int branchId) {
    }

    @Override
    public Schema partitionSchema() {
        return SCHEMA;
    }

    @Override
    public GenericRecord partitionForRecord(GenericRecord record) {
        GenericRecord partition = new GenericData.Record(this.partitionSchema());
        partition.put(NXC_SYSTEM_ID.getValue(), record.get(NXC_SYSTEM_ID.getValue()));
        partition.put(NXC_PARTITION_ID.getValue(), record.get(NXC_PARTITION_ID.getValue()));
        partition.put(NXC_SCHEMA_ID.getValue(), record.get(NXC_SCHEMA_ID.getValue()));
        return partition;
    }

}
