/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.gobblin.writer;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import gobblin.writer.DataWriter;
import gobblin.writer.Destination.DestinationType;
import gobblin.writer.FsDataWriterBuilder;
import gobblin.writer.WriterOutputFormat;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericRecord;

import java.io.IOException;

/**
 * @author Marcin Sobieszek
 * @date Aug 9, 2016 11:31:37 AM
 */
public class CalsAvroDataWriterBuilder extends FsDataWriterBuilder<Schema, GenericRecord> {

    @Override
    public DataWriter<GenericRecord> build() throws IOException {
        Preconditions.checkNotNull(this.destination);
        Preconditions.checkArgument(!Strings.isNullOrEmpty(this.writerId));
        //Preconditions.checkNotNull(this.schema);
        Preconditions.checkArgument(this.format == WriterOutputFormat.AVRO);
        this.withSchema(null);
        if (this.destination.getType() == DestinationType.HDFS) {
            return new CalsAvroHdfsDataWriter(this, this.partition, this.destination.getProperties());
        }
        throw new RuntimeException("Unsupported destination type: " + this.destination.getType());
    }
}
