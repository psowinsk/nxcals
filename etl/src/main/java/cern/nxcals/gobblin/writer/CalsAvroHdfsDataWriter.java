/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.gobblin.writer;

import cern.nxcals.common.domain.SchemaData;
import cern.nxcals.service.client.api.internal.InternalSchemaService;
import cern.nxcals.service.client.providers.InternalServiceClientFactory;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import gobblin.configuration.ConfigurationKeys;
import gobblin.configuration.State;
import gobblin.util.ForkOperatorUtils;
import gobblin.util.WriterUtils;
import gobblin.writer.FsDataWriter;
import gobblin.writer.FsDataWriterBuilder;
import org.apache.avro.Schema;
import org.apache.avro.file.CodecFactory;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumWriter;
import org.apache.hadoop.fs.FileSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.atomic.AtomicLong;

import static cern.nxcals.common.Schemas.SCHEMA_ID;

/**
 * @author Marcin Sobieszek
 * @date Aug 9, 2016 11:32:18 AM
 */
public class CalsAvroHdfsDataWriter extends FsDataWriter<GenericRecord> {
    private static final Logger LOGGER = LoggerFactory.getLogger(CalsAvroHdfsDataWriter.class);
    private static final String SCHEMA = SCHEMA_ID.getFieldName();
    private final InternalSchemaService schemaProvider = InternalServiceClientFactory.createSchemaService();
    private final OutputStream stagingFileOutputStream;
    private final DatumWriter<GenericRecord> datumWriter;
    private final DataFileWriter<GenericRecord> writer;
    // Number of records successfully written
    private final AtomicLong count = new AtomicLong(0);
    private final Schema schema;

    public CalsAvroHdfsDataWriter(FsDataWriterBuilder<Schema, GenericRecord> builder,
            Optional<GenericRecord> partition, State state) throws IOException {
        super(builder, state);

        CodecFactory codecFactory = WriterUtils.getCodecFactory(Optional.fromNullable(properties
                .getProp(ForkOperatorUtils.getPropertyNameForBranch(ConfigurationKeys.WRITER_CODEC_TYPE,
                        this.numBranches, this.branchId))), Optional.fromNullable(properties.getProp(ForkOperatorUtils
                .getPropertyNameForBranch(ConfigurationKeys.WRITER_DEFLATE_LEVEL, this.numBranches, this.branchId))));

        Schema partitionSchema = this.getSchemaFrom(partition);
        this.schema = partitionSchema != null ? partitionSchema : builder.getSchema();
        if (this.schema == null) {
            throw new IllegalStateException("Schema is null for partition " + partition + " and builder " + builder);
        }
        this.stagingFileOutputStream = createStagingFileOutputStream();
        this.datumWriter = new GenericDatumWriter<>();
        this.writer = this.closer.register(createDataFileWriter(codecFactory));

        setStagingFileGroup();
    }

    private Schema getSchemaFrom(Optional<GenericRecord> partition) {
        if (!partition.isPresent()) {
            LOGGER.warn("No partition set for writting, skipping to default schema");
            return null;
        }
        try {
            Long schemaId = (Long) partition.get().get(SCHEMA);
            SchemaData schemaData = schemaProvider.findById(schemaId);
            if (schemaData == null) {
                LOGGER.error("Cannot get schema for id {}", schemaId);
                return null;
            }
            return new Schema.Parser().parse(schemaData.getSchemaJson());
        } catch (Exception e) {
            LOGGER.error("Exception while reading schema", e);
            return null;
        }
    }

    public FileSystem getFileSystem() {
        return this.fs;
    }

    @Override
    public void write(GenericRecord record) throws IOException {
        Preconditions.checkNotNull(record);

        this.writer.append(record);
        // Only increment when write is successful
        this.count.incrementAndGet();
    }

    @Override
    public long recordsWritten() {
        return this.count.get();
    }

    @Override
    public synchronized long bytesWritten() throws IOException {
        if (!this.fs.exists(this.outputFile)) {
            return 0;
        }

        return this.fs.getFileStatus(this.outputFile).getLen();
    }

    /**
     * Create a new {@link DataFileWriter} for writing Avro records.
     *
     * @param codecFactory a {@link CodecFactory} object for building the compression codec
     * @throws IOException if there is something wrong creating a new {@link DataFileWriter}
     */
    private DataFileWriter<GenericRecord> createDataFileWriter(CodecFactory codecFactory) throws IOException {
        @SuppressWarnings("resource")
        DataFileWriter<GenericRecord> w = new DataFileWriter<>(this.datumWriter);
        w.setCodec(codecFactory);
        // Open the file and return the DataFileWriter
        return w.create(this.schema, this.stagingFileOutputStream);
    }
}