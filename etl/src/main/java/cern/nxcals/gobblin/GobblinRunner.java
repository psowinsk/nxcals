/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.gobblin;

import cern.nxcals.kerberos.KerberosRelogin;
import cern.nxcals.util.ApplicationPidWritter;
import gobblin.runtime.app.ServiceBasedAppLauncher;
import gobblin.scheduler.JobScheduler;
import gobblin.scheduler.SchedulerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;
import java.util.Properties;
import java.util.UUID;

/**
 * The main Gobblin runner. Adds the possibility to override the file based properties with the system ones. It is meant
 * to be run from the script and configured from outside with properties. Please see the gobblin-start.sh script in
 * src/dist.
 *
 * @author jwozniak
 */
public class GobblinRunner extends ServiceBasedAppLauncher {
    private static final Logger LOGGER = LoggerFactory.getLogger(GobblinRunner.class);
    static final String KERBEROS_REQUIRED = "kerberos.required";
    static final String KEYTAB_PROPERTY = "kerberos.keytab";
    static final String PRINCIPAL_PROPERTY = "kerberos.principal";

    public GobblinRunner(Properties properties) throws Exception {
        super(properties,
                properties.getProperty(ServiceBasedAppLauncher.APP_NAME, "GobblinRunner-" + UUID.randomUUID()));
        SchedulerService schedulerService = new SchedulerService(properties);
        this.addService(schedulerService);
        this.addService(new JobScheduler(properties, schedulerService));

    }

    public static void main(String[] args) {
        if (args == null || args.length < 1 || args.length > 2) {
            LOGGER.error(
                    "Usage: GobblinRunner <default configuration properties file> [custom configuration properties file]");
            System.exit(1);
        }
        Properties properties = ConfigurationLoaderUtils.loadProperties(args[0],
                args.length == 2 ? Optional.of(args[1]) : Optional.empty());
        startKerberos(properties);
        startGobblin(properties);
    }

    /**
     * Resource leak is handled as hook in the base class.
     */
    private static void startGobblin(Properties properties) {
        try {
            new GobblinRunner(properties).start();
            ApplicationPidWritter.writePid();
        } catch (Exception e) {
            throw new RuntimeException("Cannot start GobblinRunner, please check the configuration", e);
        }
    }

    private static void startKerberos(Properties properties) {
        String kerberosRequired = System.getProperty(KERBEROS_REQUIRED, "true");
        if (Boolean.valueOf(kerberosRequired)) {
            KerberosRelogin kerberos = new KerberosRelogin();
            kerberos.setKeytab(properties.getProperty(KEYTAB_PROPERTY));
            kerberos.setPrincipal(properties.getProperty(PRINCIPAL_PROPERTY));
            kerberos.start();
        }
    }
}
