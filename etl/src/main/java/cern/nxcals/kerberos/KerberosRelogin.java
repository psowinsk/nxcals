/*
 * Copyright (C) 2014-2016 LinkedIn Corp. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package cern.nxcals.kerberos;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.security.UserGroupInformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * A class that updates Kerberos token.
 *
 * @author jwozniak
 */
public class KerberosRelogin {

    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1, runnable -> {
        Thread thread = new Thread(runnable);
        thread.setDaemon(true);
        return thread;
    });

    private static final Logger LOGGER = LoggerFactory.getLogger(KerberosRelogin.class);

    private String keytab = "/data1/cals/acclog/.keytab";
    private String principal = "acclog@CERN.CH";
    private boolean enableRelogin = true;

    public boolean getEnableRelogin() {
        return enableRelogin;
    }

    public void setEnableRelogin(boolean enableRelogin) {
        this.enableRelogin = enableRelogin;
    }

    public String getKeytab() {
        return keytab;
    }

    public void setKeytab(String keytab) {
        this.keytab = keytab;
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    /**
     * Start this scheduler daemon.
     */
    @PostConstruct
    public void start() {
        if (enableRelogin) {

            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    scheduler.shutdown();
                }
            });

            LOGGER.info("Starting the Kerberos re-login daemon");
            loginFromKeytab();
            this.scheduler.scheduleAtFixedRate(this::relogin, 1, 30, TimeUnit.SECONDS);
        } else {
            LOGGER.info("Not starting the Kerberos re-login daemon, just logedin once");
            loginFromKeytab();

        }
    }

    /**
     * Login the user from a given keytab file.
     */
    private void loginFromKeytab() {
        try {
            if (!UserGroupInformation.isSecurityEnabled()) {
                Configuration conf = new Configuration(false);
                conf.set("hadoop.security.authentication", "kerberos");
                UserGroupInformation.setConfiguration(conf);
            }
            UserGroupInformation.loginUserFromKeytab(principal, keytab);
            UserGroupInformation loginUser = UserGroupInformation.getLoginUser();

            LOGGER.info("Logged in from keytab to hadoop as {}", loginUser);
        } catch (Throwable e) {
            LOGGER.error("Error while logging user from keytab ", e);
        }
    }

    private void relogin() {
        try {
            UserGroupInformation loginUser = UserGroupInformation.getLoginUser();
            LOGGER.info("Re-Login attempted from keytab to hadoop as {}", loginUser);
            loginUser.checkTGTAndReloginFromKeytab();
        } catch (Throwable e) {
            LOGGER.error("Error while relogging user from keytab ", e);
        }
    }
}
