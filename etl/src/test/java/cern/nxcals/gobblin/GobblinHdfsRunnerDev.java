/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.gobblin;

import static cern.nxcals.gobblin.GobblinRunner.KEYTAB_PROPERTY;
import static cern.nxcals.gobblin.GobblinRunner.PRINCIPAL_PROPERTY;

/**
 * Those variables have to be set to run this class locally:
 * <ul>
 * <li>-Djobconf.dir=src/dist/jobs/dev/hdfs
 * <li>-Dgobblin.work_dir=/tmp/something
 * </ul>
 */
public class GobblinHdfsRunnerDev {
    private static String USER = System.getProperty("user.name");
    private static String BASE_DIR = "/opt/" + USER;
    private static String CONFIG_DIR = BASE_DIR + "/idea-projects/nxcals/etl/src/test/resources/config";
    private static String JOBS_DIR = CONFIG_DIR + "/jobs/hdfs";

    static {
        System.setProperty(PRINCIPAL_PROPERTY, "acclog@CERN.CH");
        System.setProperty(KEYTAB_PROPERTY, BASE_DIR + "/.keytab-acclog");

        System.setProperty("jobconf.dir", JOBS_DIR);
        System.setProperty("gobblin.work_dir", "/tmp/nxcals/gobblin/hdfs/" + System.currentTimeMillis());
        System.setProperty("jobconf.fullyQualifiedPath", JOBS_DIR);

        System.setProperty("log4j.debug", "true");
        System.setProperty("log4j.configuration", "log4j-standalone-test.xml");
        System.setProperty("service.url", "http://nxcals-" + USER + "1:19093");
        //        System.setProperty(GobblinRunner.KERBEROS_REQUIRED, "false");
    }

    public static void main(String[] args) throws Exception {
        GobblinRunner.main(new String[] { CONFIG_DIR + "/gobblin-standalone.properties" });

    }

}
