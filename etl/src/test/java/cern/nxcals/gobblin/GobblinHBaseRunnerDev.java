/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.gobblin;

import java.util.Random;

import static cern.nxcals.gobblin.GobblinRunner.KEYTAB_PROPERTY;
import static cern.nxcals.gobblin.GobblinRunner.PRINCIPAL_PROPERTY;

public class GobblinHBaseRunnerDev {
    private static String USER = System.getProperty("user.name");
    private static String USER_HOME = System.getProperty("user.home");
    private static String BASE_DIR = "/opt/" + USER;
    private static String CONFIG_DIR = BASE_DIR + "/idea-projects/nxcals/etl/src/test/resources/config";
    private static String JOBS_DIR = CONFIG_DIR + "/jobs/hbase";

    static {

        System.setProperty("kafka.brokers",
                "nxcals-" + USER + "3:9092,nxcals-" + USER + "4:9092,nxcals-" + USER + "5:9092");
        System.setProperty(PRINCIPAL_PROPERTY, USER);
        System.setProperty(KEYTAB_PROPERTY, USER_HOME + "/" + USER + ".keytab");

        System.setProperty("jobconf.dir", JOBS_DIR);
        System.setProperty("gobblin.work_dir", "/tmp/nxcals/gobblin/hbase/" + new Random().nextLong());
        System.setProperty("jobconf.fullyQualifiedPath", JOBS_DIR);

        System.setProperty("log4j.debug", "true");
        System.setProperty("log4j.configuration", "log4j-standalone-test.xml");
        System.setProperty("service.url", "http://nxcals-" + USER + "6:19093");
    }

    public static void main(String[] args) throws Exception {
        GobblinRunner.main(new String[] { CONFIG_DIR + "/gobblin-standalone.properties" });
    }

}
