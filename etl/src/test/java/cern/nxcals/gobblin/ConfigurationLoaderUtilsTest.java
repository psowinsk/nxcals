/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.gobblin;

import org.junit.Test;

import java.util.Optional;
import java.util.Properties;

import static org.junit.Assert.assertEquals;

public class ConfigurationLoaderUtilsTest {

    static {
        System.setProperty("property6", "${property1}");
        System.setProperty("property7", "value7");
        System.setProperty("property1", "value1");
    }

    @Test
    public void testLoadConfiguration() {
        Properties properties = ConfigurationLoaderUtils
                .loadProperties("base-test.properties", Optional.of("specific-test.properties"));
        // properties.list(System.out);
        assertEquals("value1", properties.getProperty("property1"));
        assertEquals("false", properties.getProperty("property2"));
        assertEquals("value-specific", properties.getProperty("property3"));
        assertEquals("myValue", properties.getProperty("property4"));
        assertEquals("value7", properties.getProperty("property5"));
        assertEquals("value1", properties.getProperty("property6"));
        assertEquals("value7", properties.getProperty("property7"));
        assertEquals("myValue", properties.getProperty("property8"));
        assertEquals("value7", properties.getProperty("property9"));

    }

}
